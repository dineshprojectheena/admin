
<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->


                    <!-- main content data start here -->
                    <?php
                    
                    foreach ($ress2 as $get_res)                        
                    ?>                       
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Wheel
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form action="<?php echo site_url("wheel_upload"); ?>" class="form-horizontal" method="POST" method="POST" enctype="multipart/form-data" role="form" novalidate="novalidate">
                                            <div class="span6">
                                                <?php
                                                $results = validation_errors();
                                                if(!empty($results)) {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>


                                                    </ul>


                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="tire_size">Tire Size</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="tire_size" id="tire_size" value="<?php
									if (!isset($_POST['tire_size'])) {			
                                                                            if ($get_res['tire_size'] != '') {
                                                                                echo $get_res['tire_size'];
                                                                            }} else if (isset($_POST['tire_size'])) {
                                                                                echo $_POST['tire_size'];
                                                                            }

                                                                        ?>" class="col-xs-10 col-sm-5">  
                                                                        <span class="error"><?php echo form_error('tire_size'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="wheel_size">Wheel Size</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="wheel_size" id="wheel_size" value="<?php
									if (!isset($_POST['wheel_size'])) {			
                                                                            if ($get_res['wheel_size'] != '') {
                                                                                echo $get_res['wheel_size'];
                                                                            }} else if (isset($_POST['wheel_size'])) {
                                                                                echo $_POST['wheel_size'];
                                                                            }
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">  
                                                                        <input type="hidden" name="wheel_id" id="wheel_id" value="<?php
                                                                        if ($get_res['wheel_id'] != '') {
                                                                            echo $get_res['wheel_id'];
                                                                        } else if (isset($_POST['wheel_id'])) {
                                                                            echo $_POST['wheel_id'];
                                                                        }
                                                                        ?>" class="col-xs-10 col-sm-5">  
                                                                        <span class="error"><?php echo form_error('wheel_size'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="wheel_design">Wheel Design</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">    
                                                                        <input type="file" name="wheel_design" id="wheel_design" value="" class="col-xs-10 col-sm-5">  
									<input type="hidden" name="wheel_design2" id="wheel_design2" value="<?php 
									    	
									    if (!isset($_POST['wheel_design2'])) {			
                                                                            if ($get_res['wheel_design'] != '') {
                                                                                echo $get_res['wheel_design'];
                                                                            }} else if (isset($_POST['wheel_design2'])) {
                                                                                echo $_POST['wheel_design2'];
                                                                            }
									?>" class="col-xs-10 col-sm-5"> 
                                                                        <span class="error"><?php echo form_error('wheel_design'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="wheel_description">Wheel Description</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <textarea type="file" name="wheel_description" id="wheel_description"  class="col-xs-10 col-sm-5"><?php if (!isset($_POST['wheel_description'])) {			
                                                                            if ($get_res['wheel_description'] != '') {
                                                                                echo $get_res['wheel_description'];
                                                                            }} else if (isset($_POST['wheel_description'])) {
                                                                                echo $_POST['wheel_description'];
                                                                            }                                                                        
                                                                        ?></textarea>
                                                                        <span class="error"><?php echo form_error('wheel_description'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="wheel_price">Wheel Price</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="wheel_price" id="wheel_price" value="<?php
									if(!isset($_POST['wheel_price'])) {			
                                                                            if ($get_res['wheel_price'] != '') {
                                                                                echo $get_res['wheel_price'];
                                                                            }} else if (isset($_POST['wheel_price'])) {
                                                                                echo $_POST['wheel_price'];
                                                                            }
                                                                        ?>" class="col-xs-10 col-sm-5">  
                                                                        <span class="error"><?php echo form_error('wheel_price'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <?php
                                                                        $status = $get_res['status'];                                                                        
                                                                        ?>                                             
                                                                        <select name="status" id="status" style="width:340px;float:left;">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php if(!isset($_POST['status'])) {if($status==1){ echo 'selected';}} else { echo '';}  ?> value="1">Enabled</option>    
                                                                        <option <?php if(!isset($_POST['status'])) {if($status==2){ echo 'selected';}} else { echo '';}  ?> value="2">Disabled</option>    
                                                                        </select>  
                                                                        <span class="error"><?php echo form_error('status'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>




                                                        </div>                                            
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($ress2)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="form-group">
                                    <div class="form-group">
                                        <?php
                                                                                    
                                        if (isset($res1)):
                                            ?>                                    
                                            <div class="table-header">
                                                Results for "Wheel"
                                            </div>
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Tire Size</th>
                                                        <th>Wheel Size</th>
                                                        <th>Wheel Design</th>
                                                        <th>Wheel Description</th>
                                                        <th>Wheel Price</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
                                                    $i = 1;
                                                    foreach ($res1 as $r) :
                                                        ?>
                                                        <tr>                                                
                                                            <td>
                                                                <?php echo $i; ?>                                                        
                                                            </td>
                                                            <td>
                                                                <?php echo $r['tire_size']; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $r['wheel_size']; ?>
                                                            </td>
                                                            <td> 
                                                                <img src="<?php echo base_url().'uploads/wheel/'.$r['wheel_design']; ?>" width="100px"/>   
                                                            </td>
                                                            <td>
                                                                <?php echo $r['wheel_description']; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $r['wheel_price']; ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                if ($r['status'] == '1') {
                                                                    echo 'Enable';
                                                                } else {
                                                                    echo 'Disable';
                                                                };
                                                                ?>
                                                            </td>


                                                    <form action="<?php echo 'wheel_upload'; ?>" method="POST">
                                                        <input type="hidden" name="wheel_id"  value="<?php echo $r['wheel_id']; ?>" ?>
                                                        <td class="align_tble">
                                                            <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                                Edit
                                                            </button>                                                        
                                                        </td>
                                                    </form>
                                                    <form action="<?php echo 'wheel_upload'; ?>" method="POST" >
                                                        <input type="hidden" name="wheel_id"  value="<?php echo $r['wheel_id']; ?>" ?>
                                                        <td>
                                                            <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                delete
                                                            </button>

                                                        </td>                                                    
                                                    </form>
                                                    </tr>
                                                    <?php $i++; ?>    
                                                <?php endforeach; ?>


                                                </tbody>
                                            </table>
                                            <?php
                                        else:
//echo '<p>No Task Exist</p>';
                                        endif;
                                        ?>
                                    </div>


                                </div>
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



