<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                <script>
                    $(document).ready(function() {
                        $('.table').dataTable({
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": '<?php echo base_url() . 'top_selling_cars/top_selling_result'; ?>',
                            "aaSorting": [[4, "desc"]],
                            "fnServerData": function(sSource, aoData, fnCallback) {
                                $.ajax({
                                    "dataType": 'json',
                                    "type": "POST",
                                    "url": sSource,
                                    "data": aoData,
                                    "success": fnCallback
                                });
                            }
                        });
                    });
                </script>

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                    <!--<script type="text/javascript" src="<?php echo base_url(); ?>content/jquery-1.8.0.min.js"></script>-->
                    <style type="text/css">
                        .content{
                            width:900px;
                            margin:0 auto;
                        }
                        #searchid
                        {
                            width:300px;                                                                        
                        }
                        #result
                        {
                            position:absolute;
                            width:300px;
                            padding:3px;
                            display:none;
                            margin-top:-1px;
                            border-top:0px;
                            overflow:hidden;
                            border:1px #CCC solid;
                            background-color: white;
                            z-index:9999; 
                        }
                        #result2
                        {
                            position:absolute;
                            width:300px;
                            padding:3px;
                            display:none;
                            margin-top:-1px;
                            border-top:0px;
                            overflow:hidden;
                            border:1px #CCC solid;
                            background-color: white;
                            z-index:9999;
                        }
                        .show
                        {
                            padding:10px; 
                            border-bottom:1px #999 dashed;
                            font-size:15px; 
                            height:50px;
                        }
                        .show:hover
                        {
                            background:#4c66a4;
                            color:#FFF;
                            cursor:pointer;
                        }
                    </style>
                    <script type="text/javascript">
                        function search_product(id)
                        {
//                                alert(id);
                            if (id == 1)
                            {
                                var searchid = $('#searchid').val();
                                var dataString = 'searchid=' + searchid + '&id=' + id;
                            }
                            if (id == 2)
                            {
                                var searchid2 = $('#searchid2').val();
                                var dataString = 'searchid2=' + searchid2 + '&id=' + id;
                            }
                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url('home_struture/product_load'); ?>",
                                data: dataString,
                                cache: false,
                                success: function(html)
                                {
//                                    alert(html);
                                    if (id == 1)
                                    {
                                        $("#result").html(html).show();
                                    }
                                    if (id == 2)
                                    {
                                        $("#result2").html(html).show();
                                    }

                                }
                            });
                            return false;
                        }
                        function setdata(res, id, pro_id)
                        {
                            if (id == 1)
                            {
                                $('#searchid').val(res);
                                $('#pro_1').val(pro_id);
                                $('#result').css('display', 'none');
                            }
                            if (id == 2)
                            {
                                $('#searchid2').val(res);
                                $('#pro_2').val(pro_id);
                                $('#result2').css('display', 'none');
                            }
                        }
                    </script>

                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Group Deals 
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form action="<?php echo site_url("top_selling_cars_upload"); ?>"  id="validation-form" autocomplete="off" class="form-horizontal" method="POST">
                                            <div class="span6">
                                                <div class="tabbable">
                                                    <ul id="myTab" class="nav nav-tabs">
                                                        <li class="active">
                                                            <a>
                                                                <i class="green icon-home bigger-110"></i>
                                                                Group Deals
                                                            </a>                                                        
                                                        </li>    
                                                    </ul>
                                                    <?php
                                                    foreach ($featured_res as $feat_res)
//                                                    print_r($feat_res);
                                                        
                                                        ?>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="car_comparison" style="height:auto;">
                                                            <form action="<?php echo 'car_comparison'; ?>" method="POST">  
                                                                <div class="clearfix">
                                                                    <label class="col-sm-3 control-label no-padding-right" style="text-align: left;width:10%; " for="status">Status</label>    
                                                                    <div class="content">
                                                                        <select name="status" id="status" style="width:300px;float:left;">
                                                                            <option  value="">Select Status</option>
                                                                            <option 
                                                                            <?php
                                                                            if ($feat_res['status'] == 1) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>
                                                                                value="1">Enabled</option>    
                                                                            <option 
                                                                            <?php
                                                                            if ($feat_res['status'] == 2) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>
                                                                                value="2">Disabled</option>    
                                                                        </select>  


                                                                        <span class="error"><?php echo form_error('status'); ?></span>
                                                                    </div>
                                                                </div>
                                                                <br><br>

                                                                <div class="clearfix">
                                                                    <label class="col-sm-3 control-label no-padding-right" style="text-align: left;width:10%; " for="car_type">Top Selling Variant</label>    
                                                                    <div class="content">
                                                                        <input type="text" class="search" style="float:left;" 
                                                                               value="<?php
                                                                               echo $feat_res['pro_name'];
                                                                               ?>" id="searchid" name="searchid" onkeyup="search_product('1');" placeholder="Search for first product" />
                                                                        <input type="hidden" id="pro_1" value="<?php echo $feat_res['prd_id_1']; ?>" name="pro_1"/>
                                                                        <input type="hidden" id="featured_id" value="<?php echo $feat_res['featured_id']; ?>" name="featured_id"/>
                                                                        <span class="error" style="float:left;"><?php echo form_error('searchid'); ?></span>
                                                                        <div id="result"></div>                                                                    
                                                                    </div>
                                                                </div>
                                                                <br><br>
                                                                <div class="clearfix">
                                                                    <?php
                                                                    if (empty($feat_res)) {
                                                                        ?>
                                                                        <div class="clearfix form-actions" >
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" name="top_selling_cars_submit" class="btn btn-info">                                                
                                                                                    Submit
                                                                                </button>

                                                                                &nbsp; &nbsp; &nbsp;
                                                                                <button class="btn" type="reset">
                                                                                    <i class="icon-undo bigger-110"></i>
                                                                                    Reset
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <div class="clearfix form-actions">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" name="top_selling_cars_update" class="btn btn-info">                                                
                                                                                    Update
                                                                                </button>

                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?> 
                                                                </div>    


                                                        </div>
                                                        </form>
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Group Deals</th>
                                                                    <th>Status</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                        </table>  



                                                        <!--                                                            <div class="form-group">
                                                        <?php
                                                        if (isset($load_res)):
                                                            ?>                                    
                                                                                                                                    <div class="table-header">
                                                                                                                                        Results for "Top Selling Cars"
                                                                                                                                    </div>
                                                                                                                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                                                                                                        <thead>
                                                                                                                                            <tr>
                                                                                                                                                <th>#</th>
                                                                                                                                                <th>Top Selling Cars</th>
                                                                                                                                                <th>Status</th>
                                                                                                                                                <th></th>
                                                                                                                                                <th></th>
                                                                                                                                            </tr>
                                                                                                                                        </thead>                                                                        
                                                                                                                                        <thead>
                                                            <?php
                                                            $i = 1;
                                                            $j = 0;
                                                            foreach ($load_res as $r) :
                                                                ?>
                                                                                                                                                        <tr>                                                
                                                                                                                                                            <td>
                                                                <?php echo $i; ?>                                                        
                                                                                                                                                            </td>
                                                                                                                                                            <td>
                                                                <?php echo $r['pro_name']; ?>
                                                                                                                                                            </td>                                                                                    
                                                                                                                                                            <td>
                                                                <?php
                                                                if ($r['status'] == 1) {
                                                                    echo 'enable';
                                                                } else {
                                                                    echo 'Disable';
                                                                }
                                                                ?>
                                                                                                                                                            </td>                                                                                    
                                                                                                                                                    <form action="<?php echo 'top_selling_cars_upload'; ?>" method="POST" >
                                                                                                                                                        <td class="align_tble">
                                                                                                                                                            <input type="hidden" name="featured_id"  value="<?php echo $r['featured_id']; ?>" ?>
                                                                                                                                                            <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                                                                                                                                Edit
                                                                                                                                                            </button>                                                        
                                                                        
                                                                                                                                                        </td>
                                                                                                                                                    </form>
                                                                        
                                                                                                                                                    <form action="<?php echo 'top_selling_cars_upload'; ?>" method="POST">                                                                                    
                                                                                                                                                        <td>
                                                                                                                                                            <input type="hidden" name="featured_id"  value="<?php echo $r['featured_id']; ?>" ?>
                                                                                                                                                            <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                                                                                                                delete
                                                                                                                                                            </button>
                                                                                                                                                        </td>
                                                                                                                                                    </form> 
                                                                        
                                                                                                                                                    </tr>
                                                                        
                                                                <?php $i++; ?>    
                                                            <?php endforeach; ?>
                                                                
                                                                
                                                                                                                                        </thead>
                                                                
                                                                                                                                    </table>
                                                            <?php
                                                        else:
                                                        endif;
                                                        ?>
                                                                                                                    </div>-->


                                                    </div>
                                                </div>
                                            </div>
                                    </div>

                                    <div class="space-4"></div>



                                    </form>
                                </div>
                            </div>




                        </div>
                    </div>





                    <!-- main content data end here -->

                </div><!-- /.main-content -->                

            </div><!-- /.main-container-inner -->

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!-- footer js file links start here -->
        <?php $this->load->view('templates/js') ?>   
        <script src="<?php echo base_url() . 'assets/js/modules/validate_home_structure.js'; ?>"></script>
        <!-- footer js file links end here -->

        <!-- footer data links start here -->
        <?php $this->load->view('templates/footer') ?>    
        <!-- footer data links end here -->  

    </body>
</html>



