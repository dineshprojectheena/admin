<div class="page-content">
                        <div class="page-header">
                            <h1>
                                Dashboardssas
                                <small>
                                    <i class="icon-double-angle-right"></i>
                                    overview &amp; stats
                                </small>
                            </h1>
                        </div><!-- /.page-header -->

                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->

                                <div class="alert alert-block alert-success">
                                    <button type="button" class="close" data-dismiss="alert">
                                        <i class="icon-remove"></i>
                                    </button>

                                    <i class="icon-ok green"></i>

                                    Welcome to 
                                    <strong class="green">
                                        MNC 
                                    </strong>
                                    Admin Panel
                                </div>

                                <div class="row">
                                    <div class="space-6"></div>

                                    <div class="col-sm-7 infobox-container">
                                        <div class="infobox infobox-green  ">
                                            <div class="infobox-icon">
                                                <i class="icon-comments"></i>
                                            </div>

                                            <div class="infobox-data">
                                                <span class="infobox-data-number">32</span>
                                                <div class="infobox-content">comments + 2 reviews</div>
                                            </div>
                                            <div class="stat stat-success">8%</div>
                                        </div>

                                        <div class="infobox infobox-blue  ">
                                            <div class="infobox-icon">
                                                <i class="icon-twitter"></i>
                                            </div>

                                            <div class="infobox-data">
                                                <span class="infobox-data-number">11</span>
                                                <div class="infobox-content">new followers</div>
                                            </div>

                                            <div class="badge badge-success">
                                                +32%
                                                <i class="icon-arrow-up"></i>
                                            </div>
                                        </div>

                                        <div class="infobox infobox-pink  ">
                                            <div class="infobox-icon">
                                                <i class="icon-shopping-cart"></i>
                                            </div>

                                            <div class="infobox-data">
                                                <span class="infobox-data-number">8</span>
                                                <div class="infobox-content">new orders</div>
                                            </div>
                                            <div class="stat stat-important">4%</div>
                                        </div>

                                        <div class="infobox infobox-red  ">
                                            <div class="infobox-icon">
                                                <i class="icon-beaker"></i>
                                            </div>

                                            <div class="infobox-data">
                                                <span class="infobox-data-number">7</span>
                                                <div class="infobox-content">experiments</div>
                                            </div>
                                        </div>

                                        <div class="infobox infobox-orange2  ">
                                            <div class="infobox-chart">
                                                <span class="sparkline" data-values="196,128,202,177,154,94,100,170,224"></span>
                                            </div>

                                            <div class="infobox-data">
                                                <span class="infobox-data-number">6,251</span>
                                                <div class="infobox-content">pageviews</div>
                                            </div>

                                            <div class="badge badge-success">
                                                7.2%
                                                <i class="icon-arrow-up"></i>
                                            </div>
                                        </div>

                                        <div class="infobox infobox-blue2  ">
                                            <div class="infobox-progress">
                                                <div class="easy-pie-chart percentage" data-percent="42" data-size="46">
                                                    <span class="percent">42</span>%
                                                </div>
                                            </div>

                                            <div class="infobox-data">
                                                <span class="infobox-text">traffic used</span>

                                                <div class="infobox-content">
                                                    <span class="bigger-110">~</span>
                                                    58GB remaining
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-6"></div>

                                        <div class="infobox infobox-green infobox-small infobox-dark">
                                            <div class="infobox-progress">
                                                <div class="easy-pie-chart percentage" data-percent="61" data-size="39">
                                                    <span class="percent">61</span>%
                                                </div>
                                            </div>

                                            <div class="infobox-data">
                                                <div class="infobox-content">Order</div>
                                                <div class="infobox-content">Completed</div>
                                            </div>
                                        </div>

                                        <div class="infobox infobox-blue infobox-small infobox-dark">
                                            <div class="infobox-chart">
                                                <span class="sparkline" data-values="3,4,2,3,4,4,2,2"></span>
                                            </div>

                                            <div class="infobox-data">
                                                <div class="infobox-content">Earnings</div>
                                                <div class="infobox-content">$32,000</div>
                                            </div>
                                        </div>

                                        <div class="infobox infobox-grey infobox-small infobox-dark">
                                            <div class="infobox-icon">
                                                <i class="icon-download-alt"></i>
                                            </div>

                                            <div class="infobox-data">
                                                <div class="infobox-content">Downloads</div>
                                                <div class="infobox-content">1,205</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="vspace-sm"></div>

                                    <div class="col-sm-5">
                                        <div class="widget-box">
                                            <div class="widget-header widget-header-flat widget-header-small">
                                                <h5>
                                                    <i class="icon-signal"></i>
                                                    Traffic Sources
                                                </h5>

                                                <div class="widget-toolbar no-border">
                                                    <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        This Week
                                                        <i class="icon-angle-down icon-on-right bigger-110"></i>
                                                    </button>

                                                    <ul class="dropdown-menu pull-right dropdown-125 dropdown-lighter dropdown-caret">
                                                        <li class="active">
                                                            <a href="#" class="blue">
                                                                <i class="icon-caret-right bigger-110">&nbsp;</i>
                                                                This Week
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Last Week
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                This Month
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Last Month
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div id="piechart-placeholder"></div>

                                                    <div class="hr hr8 hr-double"></div>

                                                    <div class="clearfix">
                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-facebook-sign icon-2x blue"></i>
                                                                &nbsp; likes
                                                            </span>
                                                            <h4 class="bigger pull-right">1,255</h4>
                                                        </div>

                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-twitter-sign icon-2x purple"></i>
                                                                &nbsp; tweets
                                                            </span>
                                                            <h4 class="bigger pull-right">941</h4>
                                                        </div>

                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-pinterest-sign icon-2x red"></i>
                                                                &nbsp; pins
                                                            </span>
                                                            <h4 class="bigger pull-right">1,050</h4>
                                                        </div>
                                                    </div>
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div><!-- /widget-box -->
                                    </div><!-- /span -->
                                </div><!-- /row -->
                                <?php
                                /*
                                ?>
                                <div class="row">

                                    <div class="space-6"></div>

                                    <div class="col-sm-6">
                                        <div class="widget-box">
                                            <div class="widget-header widget-header-flat widget-header-small">
                                                <h5>
                                                    <i class="icon-signal"></i>
                                                    Group Statistics
                                                </h5>

                                                <div class="widget-toolbar no-border">
                                                    <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        This Week
                                                        <i class="icon-angle-down icon-on-right bigger-110"></i>
                                                    </button>

                                                    <ul class="dropdown-menu pull-right dropdown-125 dropdown-lighter dropdown-caret">
                                                        <li class="active">
                                                            <a href="#" class="blue">
                                                                <i class="icon-caret-right bigger-110">&nbsp;</i>
                                                                This Week
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Last Week
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                This Month
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Last Month
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div class="clearfix">
                                                        <div id="grp-statics" class="pull-left" ></div>
                                                        <p id="choices"  class="pull-right"></p>
                                                    </div>

                                                    <div class="hr hr8 hr-double"></div>

                                                    <div class="clearfix">
                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-facebook-sign icon-2x blue"></i>
                                                                &nbsp; likes
                                                            </span>
                                                            <h4 class="bigger pull-right">1,255</h4>
                                                        </div>

                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-twitter-sign icon-2x purple"></i>
                                                                &nbsp; tweets
                                                            </span>
                                                            <h4 class="bigger pull-right">941</h4>
                                                        </div>

                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-pinterest-sign icon-2x red"></i>
                                                                &nbsp; pins
                                                            </span>
                                                            <h4 class="bigger pull-right">1,050</h4>
                                                        </div>
                                                    </div>
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div><!-- /widget-box -->
                                    </div><!-- /span -->


                                    <div class="col-sm-6">
                                        <div class="widget-box">
                                            <div class="widget-header widget-header-flat widget-header-small">
                                                <h5>
                                                    <i class="icon-signal"></i>
                                                    Group Statistics
                                                </h5>

                                                <div class="widget-toolbar no-border">
                                                    <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        This Week
                                                        <i class="icon-angle-down icon-on-right bigger-110"></i>
                                                    </button>

                                                    <ul class="dropdown-menu pull-right dropdown-125 dropdown-lighter dropdown-caret">
                                                        <li class="active">
                                                            <a href="#" class="blue">
                                                                <i class="icon-caret-right bigger-110">&nbsp;</i>
                                                                This Week
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Last Week
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                This Month
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Last Month
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <div id="bar-chart"></div>

                                                    <div class="hr hr8 hr-double"></div>

                                                    <div class="clearfix">
                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-facebook-sign icon-2x blue"></i>
                                                                &nbsp; likes
                                                            </span>
                                                            <h4 class="bigger pull-right">1,255</h4>
                                                        </div>

                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-twitter-sign icon-2x purple"></i>
                                                                &nbsp; tweets
                                                            </span>
                                                            <h4 class="bigger pull-right">941</h4>
                                                        </div>

                                                        <div class="grid3">
                                                            <span class="grey">
                                                                <i class="icon-pinterest-sign icon-2x red"></i>
                                                                &nbsp; pins
                                                            </span>
                                                            <h4 class="bigger pull-right">1,050</h4>
                                                        </div>
                                                    </div>
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div><!-- /widget-box -->
                                    </div><!-- /span -->
                                </div>

                                <div class="space-6"></div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="widget-box ">
                                            <div class="widget-header">
                                                <h4 class="lighter smaller">
                                                    <i class="icon-user blue"></i>
                                                    Members
                                                </h4>
                                                <div class="widget-toolbar no-border">
                                                    <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Renault
                                                        <i class="icon-angle-down icon-on-right bigger-110"></i>
                                                    </button>

                                                    <ul class="dropdown-menu pull-right dropdown-125 dropdown-lighter dropdown-caret">
                                                        <li class="active">
                                                            <a href="#" class="blue">
                                                                <i class="icon-caret-right bigger-110">&nbsp;</i>
                                                                Audi
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Chevrolet
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Ford
                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="#">
                                                                <i class="icon-caret-right bigger-110 invisible">&nbsp;</i>
                                                                Maruti
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                    <div class="clearfix">
                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Bob Doe's avatar" src="assets/avatars/user.jpg" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Bob Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">20 min</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-warning label-sm">pending</span>

                                                                    <div class="inline position-relative">
                                                                        <button class="btn btn-minier bigger btn-yellow btn-no-border dropdown-toggle" data-toggle="dropdown">
                                                                            <i class="icon-angle-down icon-only bigger-120"></i>
                                                                        </button>

                                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                            <li>
                                                                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Approve">
                                                                                    <span class="green">
                                                                                        <i class="icon-ok bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-warning" data-rel="tooltip" title="Reject">
                                                                                    <span class="orange">
                                                                                        <i class="icon-remove bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                                                    <span class="red">
                                                                                        <i class="icon-trash bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Joe Doe's avatar" src="assets/avatars/avatar2.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Joe Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">1 hour</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-warning label-sm">pending</span>

                                                                    <div class="inline position-relative">
                                                                        <button class="btn btn-minier bigger btn-yellow btn-no-border dropdown-toggle" data-toggle="dropdown">
                                                                            <i class="icon-angle-down icon-only bigger-120"></i>
                                                                        </button>

                                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                            <li>
                                                                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Approve">
                                                                                    <span class="green">
                                                                                        <i class="icon-ok bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-warning" data-rel="tooltip" title="Reject">
                                                                                    <span class="orange">
                                                                                        <i class="icon-remove bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                                                    <span class="red">
                                                                                        <i class="icon-trash bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Jim Doe's avatar" src="assets/avatars/avatar.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Jim Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">2 hour</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-warning label-sm">pending</span>

                                                                    <div class="inline position-relative">
                                                                        <button class="btn btn-minier bigger btn-yellow btn-no-border dropdown-toggle" data-toggle="dropdown">
                                                                            <i class="icon-angle-down icon-only bigger-120"></i>
                                                                        </button>

                                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                            <li>
                                                                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Approve">
                                                                                    <span class="green">
                                                                                        <i class="icon-ok bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-warning" data-rel="tooltip" title="Reject">
                                                                                    <span class="orange">
                                                                                        <i class="icon-remove bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                                                    <span class="red">
                                                                                        <i class="icon-trash bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Alex Doe's avatar" src="assets/avatars/avatar5.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Alex Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">3 hour</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-danger label-sm">blocked</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Bob Doe's avatar" src="assets/avatars/avatar2.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Bob Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">6 hour</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-success label-sm arrowed-in">approved</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Susan's avatar" src="assets/avatars/avatar3.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Susan</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">yesterday</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-success label-sm arrowed-in">approved</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Phil Doe's avatar" src="assets/avatars/avatar4.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Phil Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">2 days ago</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-info label-sm arrowed-in arrowed-in-right">online</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Alexa Doe's avatar" src="assets/avatars/avatar1.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Alexa Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">3 days ago</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-success label-sm arrowed-in">approved</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix">
                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Bob Doe's avatar" src="assets/avatars/user.jpg" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Bob Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">20 min</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-warning label-sm">pending</span>

                                                                    <div class="inline position-relative">
                                                                        <button class="btn btn-minier bigger btn-yellow btn-no-border dropdown-toggle" data-toggle="dropdown">
                                                                            <i class="icon-angle-down icon-only bigger-120"></i>
                                                                        </button>

                                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                            <li>
                                                                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Approve">
                                                                                    <span class="green">
                                                                                        <i class="icon-ok bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-warning" data-rel="tooltip" title="Reject">
                                                                                    <span class="orange">
                                                                                        <i class="icon-remove bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                                                    <span class="red">
                                                                                        <i class="icon-trash bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Joe Doe's avatar" src="assets/avatars/avatar2.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Joe Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">1 hour</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-warning label-sm">pending</span>

                                                                    <div class="inline position-relative">
                                                                        <button class="btn btn-minier bigger btn-yellow btn-no-border dropdown-toggle" data-toggle="dropdown">
                                                                            <i class="icon-angle-down icon-only bigger-120"></i>
                                                                        </button>

                                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                            <li>
                                                                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Approve">
                                                                                    <span class="green">
                                                                                        <i class="icon-ok bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-warning" data-rel="tooltip" title="Reject">
                                                                                    <span class="orange">
                                                                                        <i class="icon-remove bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                                                    <span class="red">
                                                                                        <i class="icon-trash bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Jim Doe's avatar" src="assets/avatars/avatar.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Jim Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">2 hour</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-warning label-sm">pending</span>

                                                                    <div class="inline position-relative">
                                                                        <button class="btn btn-minier bigger btn-yellow btn-no-border dropdown-toggle" data-toggle="dropdown">
                                                                            <i class="icon-angle-down icon-only bigger-120"></i>
                                                                        </button>

                                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                            <li>
                                                                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Approve">
                                                                                    <span class="green">
                                                                                        <i class="icon-ok bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-warning" data-rel="tooltip" title="Reject">
                                                                                    <span class="orange">
                                                                                        <i class="icon-remove bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                                                    <span class="red">
                                                                                        <i class="icon-trash bigger-110"></i>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Alex Doe's avatar" src="assets/avatars/avatar5.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Alex Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">3 hour</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-danger label-sm">blocked</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Bob Doe's avatar" src="assets/avatars/avatar2.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Bob Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">6 hour</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-success label-sm arrowed-in">approved</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Susan's avatar" src="assets/avatars/avatar3.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Susan</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">yesterday</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-success label-sm arrowed-in">approved</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Phil Doe's avatar" src="assets/avatars/avatar4.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Phil Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">2 days ago</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-info label-sm arrowed-in arrowed-in-right">online</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv memberdiv">
                                                            <div class="user">
                                                                <img alt="Alexa Doe's avatar" src="assets/avatars/avatar1.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="name">
                                                                    <a href="#">Alexa Doe</a>
                                                                </div>

                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">3 days ago</span>
                                                                </div>

                                                                <div>
                                                                    <span class="label label-success label-sm arrowed-in">approved</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="widget-box ">
                                            <div class="widget-header">
                                                <h4 class="lighter smaller">
                                                    <i class="icon-comment blue"></i>
                                                    Latest Comments
                                                </h4>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                    <div class="dialogs">
                                                        <div class="itemdiv dialogdiv">
                                                            <div class="user">
                                                                <img alt="Alexa's Avatar" src="assets/avatars/avatar1.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">4 sec</span>
                                                                </div>

                                                                <div class="name">
                                                                    <a href="#">Alexa</a>
                                                                </div>
                                                                <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>

                                                                <div class="tools">
                                                                    <a href="#" class="btn btn-minier btn-info">
                                                                        <i class="icon-only icon-share-alt"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv dialogdiv">
                                                            <div class="user">
                                                                <img alt="John's Avatar" src="assets/avatars/avatar.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="blue">38 sec</span>
                                                                </div>

                                                                <div class="name">
                                                                    <a href="#">John</a>
                                                                </div>
                                                                <div class="text">Raw denim you probably haven&#39;t heard of them jean shorts Austin.</div>

                                                                <div class="tools">
                                                                    <a href="#" class="btn btn-minier btn-info">
                                                                        <i class="icon-only icon-share-alt"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv dialogdiv">
                                                            <div class="user">
                                                                <img alt="Bob's Avatar" src="assets/avatars/user.jpg" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="orange">2 min</span>
                                                                </div>

                                                                <div class="name">
                                                                    <a href="#">Bob</a>
                                                                    <span class="label label-info arrowed arrowed-in-right">admin</span>
                                                                </div>
                                                                <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>

                                                                <div class="tools">
                                                                    <a href="#" class="btn btn-minier btn-info">
                                                                        <i class="icon-only icon-share-alt"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv dialogdiv">
                                                            <div class="user">
                                                                <img alt="Jim's Avatar" src="assets/avatars/avatar4.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="grey">3 min</span>
                                                                </div>

                                                                <div class="name">
                                                                    <a href="#">Jim</a>
                                                                </div>
                                                                <div class="text">Raw denim you probably haven&#39;t heard of them jean shorts Austin.</div>

                                                                <div class="tools">
                                                                    <a href="#" class="btn btn-minier btn-info">
                                                                        <i class="icon-only icon-share-alt"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="itemdiv dialogdiv">
                                                            <div class="user">
                                                                <img alt="Alexa's Avatar" src="assets/avatars/avatar1.png" />
                                                            </div>

                                                            <div class="body">
                                                                <div class="time">
                                                                    <i class="icon-time"></i>
                                                                    <span class="green">4 min</span>
                                                                </div>

                                                                <div class="name">
                                                                    <a href="#">Alexa</a>
                                                                </div>
                                                                <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>

                                                                <div class="tools">
                                                                    <a href="#" class="btn btn-minier btn-info">
                                                                        <i class="icon-only icon-share-alt"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <form>
                                                        <div class="form-actions">
                                                            <div class="input-group">
                                                                <input placeholder="Type your message here ..." type="text" class="form-control" name="message" />
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-sm btn-info no-radius" type="button">
                                                                        <i class="icon-share-alt"></i>
                                                                        Send
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div><!-- /widget-main -->
                                            </div><!-- /widget-body -->
                                        </div><!-- /widget-box -->
                                    </div><!-- /span -->
                                </div>
                                
                                <?php
                                
                                */
                                ?>
                                
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->