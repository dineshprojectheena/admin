<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here --><div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->

                    <script>
                        $(document).ready(function () {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'stitcher/affiliate_result'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function (sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });
                    </script>
                    <!--Landing page master... 
                    Id, name, email, City, preferred brand model variant, bg color, bg image, 
                    submit button one, submit button two and their tex
                    -->

                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Affiliate Page
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <form  id="validation-form" action="<?php echo site_url("upload_affiliate"); ?>" method="POST" enctype="multipart/form-data" role="form" class="form-horizontal" style="overflow:hidden;">
                                            <div class="span6">
                                                <?php
//                                                print_r($query);
                                                $message = $this->session->flashdata('message');


                                                if (($message) == 'updated') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Updated:
                                                        </strong>
                                                        Data Updated Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                else if (($message) == 'validate') {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please fill all mendatory details!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                else if (($message) == 'error') {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                else if (($message) == 'exist') {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Code Exist please try new code details!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                else if (($message) == 'success') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>
                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Successful:
                                                        </strong>
                                                        Data Uploaded Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                else if (($message) == 'delete') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>
                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Successful:
                                                        </strong>
                                                        Data Deleted Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
//                                                print_r($affi_data);
                                                ?>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="user">User/Site</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="user" value="<?php echo $affi_data['user']; ?>" id="user" class="col-md-3" placeholder="">                                                                         
                                                                        <input type="hidden" name="affiliate_id" value="<?php echo $affi_data['affiliate_id']; ?>" id="user" class="col-md-3" placeholder="">                                                                         
                                                                        <span class="error"><?php echo form_error('user'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="code">Code</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="code" value="<?php echo $affi_data['code']; ?>" id="user" class="col-md-3" placeholder="">                                                                         
                                                                        <span class="error"><?php echo form_error('code'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="email_id">Email Id</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="email_id" value="<?php echo $affi_data['email_id']; ?>" id="email_id" class="col-md-3" placeholder="">                                                                         
                                                                        <span class="error"><?php echo form_error('email_id'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="comments">Comments</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <textarea name="comments" id="comments" class="col-md-3" placeholder=""><?php echo $affi_data['comments']; ?></textarea>                                                                         
                                                                        <span class="error"><?php echo form_error('comments'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="status_data">Status</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <select name="status_data" id="status_data" class="col-md-3" >
                                                                            <option  value="">Select status</option>
                                                                            <option <?php if($affi_data['status']=='1') { echo 'selected';} ?> value="1">Enable</option>
                                                                            <option <?php if($affi_data['status']=='2') { echo 'selected';} ?> value="2">Disable</option>
                                                                                                                                                                                                                        
                                                                        </select>
                                                                        <span class="error"><?php echo form_error('status_data'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            
                                                        </div>                                                                                                 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <div class="clearfix form-actions">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php
                                                    if (!empty($affi_data)) {
                                                        ?>
                                                        <button type="submit" name="Update" class="btn btn-info">                                                
                                                            Update
                                                        </button>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>

                        <?php
//                        print_r($coupon);
                        ?>


                        <div class="row">
                            <div class="col-xs-12">

                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Affiliate Id</th>
                                            <th>User</th>
                                            <th>Code</th>
                                            <th>Email Id</th>
                                            <th>Comments</th>
                                            <th>Status</th>
                                            <th>Added Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>
            <script src="<?php echo base_url() . 'assets/js/modules/validate_brand.js' ?>"></script>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>   

            <!-- footer data links end here -->  
            <style>
                #brand_id
                {
                    width:100px;    
                }
            </style>

    </body>
</html>



