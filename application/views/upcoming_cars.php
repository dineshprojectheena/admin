<!DOCTYPE html>
<html lang="en">
    <?php
    $this->load->view('templates/header');
    ?>    
    <style>
        #commentForm {
            width: 500px;
        }
        #commentForm label {
            width: 250px;
        }
        #commentForm label.error, #commentForm input.submit {
            margin-left: 253px;
        }
        #signupForm {
            width: 670px;
        }
        #signupForm label.error {
            margin-left: 10px;
            width: auto;
            display: inline;
        }
        #newsletter_topics label.error {
            display: none;
            margin-left: 103px;
        }
    </style>
    <style>
        .col-sm-3
        {
            width:20% !important;    
        }
        .tab-content
        {
            overflow:hidden !important;    
        }
    </style>  
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">
                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                <div class="main-content">
                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <script>

                        $(document).ready(function () {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'product/upcoming_results'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function (sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });

                    </script>
                    <!-- breadcrumbs data links end here -->
                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Upcoming Cars
                            </div>
                        </div>
                        <?php
                        $status = $this->session->flashdata('status');
                        if (!empty($status)):
                            ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <?php echo $status ?>
                                <br>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active">
                                        <a data-toggle="tab" onclick="search_data('Active');" href="#tab-1" aria-expanded="true" >
                                            <i class="blue ace-icon fa fa-question-circle bigger-120"></i>
                                            Activated
                                        </a>
                                    </li>

                                    <li>
                                        <a data-toggle="tab" onclick="search_data('Deactive');" href="#tab-2" aria-expanded="false">
                                            <i class="green ace-icon fa fa-user bigger-120"></i>
                                            Deactivated
                                        </a>
                                    </li>

                                </ul>
                                <script>
                                    function search_data(a)
                                    {
                                        exit;
                                        setTimeout(function () {
                                            $('.dataTables_filter input').val(a);
                                            $('.dataTables_filter input').focus();
                                            var e = jQuery.Event("keyup");
                                            e.which = 13;
                                            e.keyCode = 13;
                                            $('.dataTables_filter input').trigger(e);
                                            $('.dataTables_filter input').select();
                                        }, 1000);

                                    }
                                </script>
                                <div class="tab-content">
                                    <div id="tab-1" class="tab-pane fade active in">
                                        <div class="col-xs-12" style="padding:0px;margin:20px 0px;">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Variant Name</th>
                                                        <th>Launching Date</th>
                                                        <th>Status</th>
                                                        <th >Pre-Order Button Status</th>
                                                        <th>#</th>                                            
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="3" >
                                                            <button class="btn btn-xs btn-success" style="float:right" onclick="upcoming_status('1');" name="deleteall" type="submit">
                                                                Activate Upcoming
                                                            </button>
                                                            <button class="btn btn-xs btn-danger" style="float:right" onclick="upcoming_status('2');" name="deleteall" type="submit">
                                                                De-Activate Upcoming
                                                            </button>
                                                        </th>
                                                        <th colspan="3" >
                                                            <button class="btn btn-xs btn-success" style="float:left" onclick="upcoming_status('3');" name="deleteall" type="submit">
                                                                Activate Sale Button
                                                            </button>
                                                            <button class="btn btn-xs btn-danger" style="float:left" onclick="upcoming_status('4');" name="deleteall" type="submit">
                                                                De-Activate Sale Button
                                                            </button>
                                                        </th>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>     
                                    </div>
                                    <div id="tab-1" class="tab-pane fade active in">

                                    </div>    
                                </div>    
                            </div>
                        </div>




                        <!-- main content data end here -->
                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->
            <script>
                function upcoming_status(a)
                {
//                    alert(a);

                    var val = [];
                    $(':checkbox:checked').each(function (i) {
                        val[i] = $(this).val();
//                        alert(val[i]);
                    });
//                    if(a==3)
//                    {
                    window.location.href = "<?php echo base_url('product/upcoming_variant_update/') ?>/?id=" + val + "&status=" + a;
//                    }    
//                    
//                    alert(val);

                }
                get_expert_model();
                function get_expert_model()
                {
                    var model_id = "<?php
                        if (!empty($single_res['model_id'])) {
                            echo $single_res['model_id'];
                        } else if (!empty($_POST['model_id'])) {
                            echo $_POST['model_id'];
                        }
                        ?>";
//                            alert(model_id);
                    var brand_id = $('#brand_id').val();
                    if (brand_id != '')
                    {

                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('stitcher/get_exp_brand_wise_model') ?>",
                            data: {brand_id: brand_id, model_id: model_id}
                        }).done(function (html) {
                            $("#model_res").html(html);
                            get_exp_variant();
                        });
                    }
                }
                function get_exp_variant()
                {

                    var variant_id = "<?php
                        if (!empty($single_res['variant_id'])) {
                            echo $single_res['variant_id'];
                        } else if (!empty($_POST['all_product_name'])) {
                            echo $_POST['all_product_name'];
                        }
                        ?>";
                    var brand_id = $('#brand_id').val();
                    var model_id = $('#model_id').val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('stitcher/get_exp_model_wise_variant') ?>",
                        data: {model_id: model_id, brand_id: brand_id, variant_id: variant_id}
                    }).done(function (html) {
                        $("#variant_results").html(html);
                    });
                }
            </script>
            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <script src="<?php echo base_url() . 'assets/js/modules/validate_video.js' ?>" ></script>
            <?php $this->load->view('templates/footer') ?>    

    </body>
</html>







