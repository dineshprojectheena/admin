<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">
                <?php $this->load->view('templates/sidebar') ?>  
                <style>
                    ul.token-input-list-facebook
                    {
                        width:100% !important;    
                        clear:both !important;    
                    }
                </style>
                <script>
                    $(document).ready(function () {
                        $('.table').dataTable({
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": '<?php echo base_url() . 'dealer_prices/dealer_result'; ?>',
                            "aaSorting": [[4, "desc"]],
                            "fnServerData": function (sSource, aoData, fnCallback) {
                                $.ajax({
                                    "dataType": 'json',
                                    "type": "POST",
                                    "url": sSource,
                                    "data": aoData,
                                    "success": fnCallback
                                });
                            }
                        });
                    });
                </script>
                <div class="main-content">
                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Dealer
                            </div>
                        </div>
                        <?php
                        echo $err;
                        ?>
                        <?php if (isset($error_msg)): ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <?php echo $error_msg ?>
                                <br>
                            </div>
                        <?php endif; ?>

                        <?php if (isset($success_msg)): ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <?php echo $success_msg ?>
                                <br>
                            </div>
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                foreach ($results_up as $get_res)
                                    
                                    ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form" action="<?php echo site_url("dealer_upload"); ?>" class="form-horizontal" class="form-horizontal" method="POST" enctype="multipart/form-data" role="form">
                                            <div class="span6">
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>

                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <h4 class="header blue bolder smaller">Dealer Details</h4>
                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="dealer_name">Dealer Company Name</label>
                                                                <input type="text" name="dealer_name" required id="dealer_name" value="<?php
                                                                if (isset($_POST['dealer_name'])) {
                                                                    echo $_POST['dealer_name'];
                                                                } else {
                                                                    echo $results_up['dealer_name'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('dealer_name'); ?></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="contact_no">Phone Number</label>
                                                                <input type="text" name="contact_no" id="contact_no"  onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                if (isset($_POST['contact_no'])) {
                                                                    echo $_POST['contact_no'];
                                                                } else {
                                                                    echo $results_up['contact_no'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                       <?php
//                                                                print_r($results_up);
                                                                       ?>
                                                                <span class="error"><?php echo form_error('contact_no'); ?></span>    
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="fax">Fax Number</label>

                                                                <div >
                                                                    <input type="text" name="fax" id="fax" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                    if (isset($_POST['fax'])) {
                                                                        echo $_POST['fax'];
                                                                    } else {
                                                                        echo $results_up['fax'];
                                                                    }
                                                                    ?>" class="form-control col-md-12">                                                                
                                                                    <span class="error"><?php echo form_error('fax'); ?></span>
                                                                </div>
                                                            </div>





                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="email">Email</label>
                                                                <input type="text" name="email" id="email" required value="<?php
                                                                if (isset($_POST['email'])) {
                                                                    echo $_POST['email'];
                                                                } else {
                                                                    echo $results_up['email'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('email'); ?></span> 
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="profiles">Profile</label>
                                                                <input type="file" name="profiles" accept="image/*" id="profiles" class="form-control col-md-12">                                                                
                                                                <input type="hidden" name="profile_pic" id="profile_pic" value="<?php
                                                                if(isset($_POST['profile_pic'])) {
                                                                    echo $_POST['profile_pic'];
                                                                } else {
                                                                    echo $user_images['profile_pic'];
                                                                }
                                                                ?>"  class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('profiles'); ?></span>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="others_brand">Brands</label>
                                                                <input type="text" name="others_brand" id="others_brand" value="<?php
                                                                if (isset($_POST['others_brand'])) {
                                                                    echo $_POST['others_brand'];
                                                                } else {
                                                                    echo $results_up['others_brand'];
                                                                }
                                                                ?>" class="form-control col-md-12">   
                                                                <span class="error"><?php echo form_error('others_brand'); ?></span>
                                                            </div>




                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="no_of_showrooms">Number of Showrooms </label>
                                                                <input type="text" name="no_of_showrooms" id="no_of_showrooms" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" value="<?php
                                                                if (isset($_POST['no_of_showrooms'])) {
                                                                    echo $_POST['no_of_showrooms'];
                                                                } else {
                                                                    echo $results_up['no_of_showrooms'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('no_of_showrooms'); ?></span>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="no_of_workshops">Number of Workshops</label>
                                                                <input type="text" name="no_of_workshops" id="no_of_workshops" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" value="<?php
                                                                if (isset($_POST['no_of_workshops'])) {
                                                                    echo $_POST['no_of_workshops'];
                                                                } else {
                                                                    echo $results_up['no_of_workshops'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('no_of_workshops'); ?></span>
                                                            </div>

                                                            <div class="clear"></div>

                                                            <h4 class="header blue bolder smaller" style="padding:5px;">SPOC Details</h4>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="cont_person">Name</label>
                                                                <input type="text" name="cont_person" id="cont_person" value="<?php
                                                                if (isset($_POST['cont_person'])) {
                                                                    echo $_POST['cont_person'];
                                                                } else {
                                                                    echo $results_up['cont_person'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('cont_person'); ?></span>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="spoc_number">Mobile Number</label>
                                                                <input type="text" name="spoc_number" id="spoc_number" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                if (isset($_POST['spoc_number'])) {
                                                                    echo $_POST['spoc_number'];
                                                                } else {
                                                                    echo $results_up['spoc_number'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('spoc_number'); ?></span>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="spoc_email">Email</label>
                                                                <input type="text" name="spoc_email" id="spoc_email" value="<?php
                                                                if (isset($_POST['spoc_email'])) {
                                                                    echo $_POST['spoc_email'];
                                                                } else {
                                                                    echo $results_up['spoc_email'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('spoc_email'); ?></span>
                                                            </div>




                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="alternate_contact">Alternate Contact</label>
                                                                <input type="text" name="alternate_contact" id="alternate_contact" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                if (isset($_POST['alternate_contact'])) {
                                                                    echo $_POST['alternate_contact'];
                                                                } else {
                                                                    echo $results_up['alternate_contact'];
                                                                }
                                                                ?>" class="form-control col-md-12">                                                                
                                                                <span class="error"><?php echo form_error('alternate_contact'); ?></span>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="spoc_manager">Manager</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="spoc_manager" id="spoc_manager" value="<?php
                                                                        if (isset($_POST['spoc_manager'])) {
                                                                            echo $_POST['spoc_manager'];
                                                                        } else {
                                                                            echo $results_up['spoc_manager'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('spoc_manager'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="manager_cellphone_no">Manager Cellphone Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="manager_cellphone_no" id="manager_cellphone_no" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['manager_cellphone_no'])) {
                                                                            echo $_POST['manager_cellphone_no'];
                                                                        } else {
                                                                            echo $results_up['manager_cellphone_no'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('manager_cellphone_no'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="manager_email">Manager Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="manager_email" id="manager_email" value="<?php
                                                                        if (isset($_POST['manager_email'])) {
                                                                            echo $_POST['manager_email'];
                                                                        } else {
                                                                            echo $results_up['manager_email'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('manager_email'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div style="clear:both;"></div>

                                                            <h4 class="header blue bolder smaller">Decision Maker Details</h4>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="decision_maker_name">Name</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="decision_maker_name" id="decision_maker_name" value="<?php
                                                                        if (isset($_POST['decision_maker_name'])) {
                                                                            echo $_POST['decision_maker_name'];
                                                                        } else {
                                                                            echo $results_up['decision_maker_name'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('decision_maker_name'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="mobile_number">Mobile Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="mobile_number" id="mobile_number" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['mobile_number'])) {
                                                                            echo $_POST['mobile_number'];
                                                                        } else {
                                                                            echo $results_up['mobile_number'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('mobile_number'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="decision_maker_email">Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="decision_maker_email" id="decision_maker_email" value="<?php
                                                                        if (isset($_POST['decision_maker_email'])) {
                                                                            echo $_POST['decision_maker_email'];
                                                                        } else {
                                                                            echo $results_up['decision_maker_email'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('decision_maker_email'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="decision_maker_designation">Designation</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="decision_maker_designation" id="decision_maker_designation" value="<?php
                                                                        if (isset($_POST['decision_maker_designation'])) {
                                                                            echo $_POST['decision_maker_designation'];
                                                                        } else {
                                                                            echo $results_up['decision_maker_designation'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('decision_maker_designation'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div style="clear:both;"></div>

                                                            <h4 class="header blue bolder smaller">Showroom Details</h4>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_name1">Showroom1 Address</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_name1" id="showroom_name1" value="<?php
                                                                        if (isset($_POST['showroom_name1'])) {
                                                                            echo $_POST['showroom_name1'];
                                                                        } else {
                                                                            echo $results_up['showroom_name1'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_name1'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_contact_name1">Showroom1 Contact Name</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_contact_name1" id="showroom_contact_name1" value="<?php
                                                                        if (isset($_POST['showroom_contact_name1'])) {
                                                                            echo $_POST['showroom_contact_name1'];
                                                                        } else {
                                                                            echo $results_up['showroom_contact_name1'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_contact_name1'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_email1">Showroom1 Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_email1" id="showroom_email1" value="<?php
                                                                        if (isset($_POST['showroom_email1'])) {
                                                                            echo $_POST['showroom_email1'];
                                                                        } else {
                                                                            echo $results_up['showroom_email1'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_email1'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_phone_number1">Showroom1 Phone Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_phone_number1" id="showroom_phone_number1" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['showroom_phone_number1'])) {
                                                                            echo $_POST['showroom_phone_number1'];
                                                                        } else {
                                                                            echo $results_up['showroom_phone_number1'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_phone_number1'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <hr/>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_name2">Showroom2 Address</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_name2" id="showroom_name2" value="<?php
                                                                        if (isset($_POST['showroom_name2'])) {
                                                                            echo $_POST['showroom_name2'];
                                                                        } else {
                                                                            echo $results_up['showroom_name2'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_name2'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_contact_name2">Showroom2 Contact Name</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_contact_name2" id="showroom_contact_name2" value="<?php
                                                                        if (isset($_POST['showroom_contact_name2'])) {
                                                                            echo $_POST['showroom_contact_name2'];
                                                                        } else {
                                                                            echo $results_up['showroom_contact_name2'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_contact_name2'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_email2">Showroom2 Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_email2" id="showroom_email2" value="<?php
                                                                        if (isset($_POST['showroom_email2'])) {
                                                                            echo $_POST['showroom_email2'];
                                                                        } else {
                                                                            echo $results_up['showroom_email2'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_email2'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_phone_number2">Showroom2 Phone Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_phone_number2" id="showroom_phone_number2" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['showroom_phone_number2'])) {
                                                                            echo $_POST['showroom_phone_number2'];
                                                                        } else {
                                                                            echo $results_up['showroom_phone_number2'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_phone_number2'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <hr>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_name3">Showroom3 Address</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_name3" id="showroom_name3" value="<?php
                                                                        if (isset($_POST['showroom_name3'])) {
                                                                            echo $_POST['showroom_name3'];
                                                                        } else {
                                                                            echo $results_up['showroom_name3'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_name3'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_contact_name3">Showroom3 Contact Name</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_contact_name3" id="showroom_contact_name3" value="<?php
                                                                        if (isset($_POST['showroom_contact_name3'])) {
                                                                            echo $_POST['showroom_contact_name3'];
                                                                        } else {
                                                                            echo $results_up['showroom_contact_name3'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_contact_name3'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_email3">Showroom3 Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_email3" id="showroom_email3" value="<?php
                                                                        if (isset($_POST['showroom_email3'])) {
                                                                            echo $_POST['showroom_email3'];
                                                                        } else {
                                                                            echo $results_up['showroom_email3'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_email3'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_phone_number3">Showroom2 Phone Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_phone_number3" id="showroom_phone_number3" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['showroom_phone_number3'])) {
                                                                            echo $_POST['showroom_phone_number3'];
                                                                        } else {
                                                                            echo $results_up['showroom_phone_number3'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_phone_number3'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <hr>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_name4">Showroom2 Address</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_name4" id="showroom_name4" value="<?php
                                                                        if (isset($_POST['showroom_name4'])) {
                                                                            echo $_POST['showroom_name4'];
                                                                        } else {
                                                                            echo $results_up['showroom_name4'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_name4'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_contact_name4">Showroom4 Contact Name</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_contact_name4" id="showroom_contact_name4" value="<?php
                                                                        if (isset($_POST['showroom_contact_name4'])) {
                                                                            echo $_POST['showroom_contact_name4'];
                                                                        } else {
                                                                            echo $results_up['showroom_contact_name4'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_contact_name4'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_email4">Showroom4 Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_email4" id="showroom_email4" value="<?php
                                                                        if (isset($_POST['showroom_email4'])) {
                                                                            echo $_POST['showroom_email4'];
                                                                        } else {
                                                                            echo $results_up['showroom_email4'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_email4'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_phone_number4">Showroom2 Phone Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_phone_number4" id="showroom_phone_number4" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['showroom_phone_number4'])) {
                                                                            echo $_POST['showroom_phone_number4'];
                                                                        } else {
                                                                            echo $results_up['showroom_phone_number4'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_phone_number4'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <hr>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_name5">Showroom5 Address</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_name5" id="showroom_name5" value="<?php
                                                                        if (isset($_POST['showroom_name5'])) {
                                                                            echo $_POST['showroom_name5'];
                                                                        } else {
                                                                            echo $results_up['showroom_name5'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_name5'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_contact_name5">Showroom5 Contact Name</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_contact_name5" id="showroom_contact_name5" value="<?php
                                                                        if (isset($_POST['showroom_contact_name5'])) {
                                                                            echo $_POST['showroom_contact_name5'];
                                                                        } else {
                                                                            echo $results_up['showroom_contact_name5'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_contact_name5'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_email5">Showroom4 Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_email5" id="showroom_email5" value="<?php
                                                                        if (isset($_POST['showroom_email5'])) {
                                                                            echo $_POST['showroom_email5'];
                                                                        } else {
                                                                            echo $results_up['showroom_email5'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_email5'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="showroom_phone_number5">Showroom5 Phone Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="showroom_phone_number5" id="showroom_phone_number5" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['showroom_phone_number5'])) {
                                                                            echo $_POST['showroom_phone_number5'];
                                                                        } else {
                                                                            echo $results_up['showroom_phone_number5'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('showroom_phone_number5'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div style="clear:both;"></div>

                                                            <h4 class="header blue bolder smaller">EDP Team Details</h4>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="edp_team_name">Name</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="edp_team_name" id="edp_team_name" value="<?php
                                                                        if (isset($_POST['edp_team_name'])) {
                                                                            echo $_POST['edp_team_name'];
                                                                        } else {
                                                                            echo $results_up['edp_team_name'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('edp_team_name'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="edp_team_mobile_number">Mobile Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="edp_team_mobile_number" id="edp_team_mobile_number" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['edp_team_mobile_number'])) {
                                                                            echo $_POST['edp_team_mobile_number'];
                                                                        } else {
                                                                            echo $results_up['edp_team_mobile_number'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('edp_team_mobile_number'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="edp_team_email">Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="edp_team_email" id="edp_team_email" value="<?php
                                                                        if (isset($_POST['edp_team_email'])) {
                                                                            echo $_POST['edp_team_email'];
                                                                        } else {
                                                                            echo $results_up['edp_team_email'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('edp_team_email'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div style="clear:both;"></div>

                                                            <h4 class="header blue bolder smaller">Dealer Contact Information</h4>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="address">Address</label>
                                                                <div >
                                                                    <div >
                                                                        <textarea  name="address" id="address" style="height:30px;"  class="form-control col-md-12"><?php
                                                                            if (isset($_POST['address'])) {
                                                                                echo $_POST['address'];
                                                                            } else {
                                                                                echo $results_up['address'];
                                                                            }
                                                                            ?></textarea>                                                                
                                                                        <span class="error"><?php echo form_error('address'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="state">State</label>
                                                                <div >
                                                                    <div >
                                                                        <?php
//                                                                        echo $results_up['state']
                                                                        ?>
                                                                        <select name="state" id="state" class="state_data form-control col-md-12" onchange="getcity();">
                                                                            <option value="">Select State</option>    
                                                                            <?php
                                                                            foreach ($state as $states) {
                                                                                ?>
                                                                                <option 
                                                                                <?php
                                                                                if (!empty($results_up['state']) && $results_up['state']==$states->state_id) {
                                                                                    echo $res1 = 'selected="selected"';
                                                                                }
                                                                                else if (isset($_POST['state']) == $states->state_id) {
                                                                                    echo $res1 = 'selected="selected"';
                                                                                }
                                                                                    
//                                                                                if (!isset($_POST['state'])) {
//                                                                                    if (isset($_POST['state']) == $states->state_id) {
//                                                                                    echo $res1 = 'selected="selected"';
//                                                                                }
//                                                                                }
//                                                                                else if (!empty($results_up['state'])) {
//                                                                                    echo $res1 = 'selected="selected"';
//                                                                                }
                                                                                ?>

                                                                                    value="<?php echo $states->state_id; ?>"><?php echo $states->name; ?></option>    
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                        </select>
                                                                        <span class="error"><?php echo form_error('state'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>




                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="city">City</label>
                                                                <div >
                                                                    <div >
                                                                        <select name="city" id="city" class="form-control col-md-12">
                                                                            <option value="">Select City</option>    
                                                                            <?php
                                                                            foreach ($city as $citys) {
                                                                                ?>
                                                                                <option 
                                                                                <?php
                                                                                if (!empty($results_up['city']) && $results_up['city']==$citys->city_id) {
                                                                                    echo $res1 = 'selected="selected"';
                                                                                }
                                                                                else if (isset($_POST['city']) == $citys->city_id) {
                                                                                    echo $res1 = 'selected="selected"';
                                                                                }
//                                                                                if (isset($_POST['city']) == $results_up['city']) {
//                                                                                    echo $res = 'selected';
//                                                                                }
                                                                                ?>

                                                                                    value="<?php echo $citys->city_id; ?>"><?php echo $citys->name; ?></option>    
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                        </select>
                                                                        <span class="error"><?php echo form_error('city'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div>






                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="post_code">Post Code</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="post_code" maxlength="6" id="post_code" onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['post_code'])) {
                                                                            echo $_POST['post_code'];
                                                                        } else {
                                                                            echo $results_up['post_code'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                
                                                                        <span class="error"><?php echo form_error('post_code'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div style="clear:both;"></div>


                                                            <h4 class="header blue bolder smaller">Financial Details</h4>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="pan_card">Pan Card</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="pan_card" id="pan_card"  class="form-control col-md-12"   value="<?php
                                                                        if (isset($_POST['pan_card'])) {
                                                                            echo $_POST['pan_card'];
                                                                        } else {
                                                                            echo $results_up['pan_card'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('pan_card'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="vat_number">VAT Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="vat_number" id="vat_number"  class="form-control col-md-12"   value="<?php
                                                                        if (isset($_POST['vat_number'])) {
                                                                            echo $_POST['vat_number'];
                                                                        } else {
                                                                            echo $results_up['vat_number'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('vat_number'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="service_tax_number">Service Tax Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="service_tax_number" id="service_tax_number"  class="form-control col-md-12"   value="<?php
                                                                        if (isset($_POST['service_tax_number'])) {
                                                                            echo $_POST['service_tax_number'];
                                                                        } else {
                                                                            echo $results_up['service_tax_number'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('service_tax_number'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="bank_details">Bank Details</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="bank_details" id="bank_details"  class="form-control col-md-12"   value="<?php
                                                                        if (isset($_POST['bank_details'])) {
                                                                            echo $_POST['bank_details'];
                                                                        } else {
                                                                            echo $results_up['bank_details'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('bank_details'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="bank_account_no">Bank Account Number</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="bank_account_no" id="bank_account_no"  class="form-control col-md-12"   onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" value="<?php
                                                                        if (isset($_POST['bank_account_no'])) {
                                                                            echo $_POST['bank_account_no'];
                                                                        } else {
                                                                            echo $results_up['bank_account_no'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('bank_account_no'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="ifsc_code">IFSC Code</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="ifsc_code" id="ifsc_code"  class="form-control col-md-12"   value="<?php
                                                                        if (isset($_POST['ifsc_code'])) {
                                                                            echo $_POST['ifsc_code'];
                                                                        } else {
                                                                            echo $results_up['ifsc_code'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('ifsc_code'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="bank_account_type">Bank Account Type</label>
                                                                <?php
                                                                if ($_POST['bank_account_type'] == '1' || $results_up['bank_account_type'] == '1') {
                                                                    $chk01 = 'selected';
                                                                }

                                                                if ($_POST['bank_account_type'] == '2' || $results_up['bank_account_type'] == '2') {
                                                                    $chk02 = 'selected';
                                                                }
                                                                ?>
                                                                <select name="bank_account_type" id="bank_account_type" class="form-control col-md-12">
                                                                    <option  value="">Select Bank Account Type</option>
                                                                    <option <?php echo $chk01; ?> value="1">Savings</option>    
                                                                    <option <?php echo $chk02; ?> value="2">Currrent</option>   
                                                                </select>                                                                      

                                                                <span class="error"><?php echo form_error('bank_account_type'); ?></span>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="payment_methods">Payment methods</label>
                                                                <div >
                                                                    <div >
                                                                        <?php
                                                                        $payment_methods = $results_up['payment_methods'];
//                                                                        $payment_methods = '';
                                                                        if ($payment_methods == '1' || $_POST['payment_methods'] == '1') {
                                                                            $chk11 = 'selected';
                                                                        } else {
                                                                            $chk11 = '';
                                                                        }
                                                                        if ($payment_methods == '2' || $_POST['payment_methods'] == '2') {
                                                                            $chk12 = 'selected';
                                                                        } else {
                                                                            $chk12 = '';
                                                                        }
                                                                        if ($payment_methods == '3' || $_POST['payment_methods'] == '3') {
                                                                            $chk13 = 'selected';
                                                                        } else {
                                                                            $chk13 = '';
                                                                        }
                                                                        if ($payment_methods == '4' || $_POST['payment_methods'] == '4') {
                                                                            $chk14 = 'selected';
                                                                        } else {
                                                                            $chk14 = '';
                                                                        }
                                                                        ?>
                                                                        <select name="payment_methods" id="payment_methods" class="form-control col-md-12">
                                                                            <option  value="">Select Payment Methods</option>
                                                                            <option <?php echo $chk11; ?> value="1">NEFT/RTGS</option>    
                                                                            <option <?php echo $chk12; ?> value="2">Credit Card</option>   
                                                                            <option <?php echo $chk13; ?> value="3">Cash</option>   
                                                                            <option <?php echo $chk14; ?> value="4">Cheque</option>   
                                                                        </select>                                                                      

                                                                        <span class="error"><?php echo form_error('payment_methods'); ?></span>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="account_contact_person">Accounts Contact Person</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="account_contact_person" id="account_contact_person"  class="form-control col-md-12"   value="<?php
                                                                        if (isset($_POST['account_contact_person'])) {
                                                                            echo $_POST['account_contact_person'];
                                                                        } else {
                                                                            echo $results_up['account_contact_person'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('account_contact_person'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="account_contact_phone">Accounts Contact Phone</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="account_contact_phone" id="account_contact_phone"  class="form-control col-md-12"    onkeyup="this.value = this.value.replace(/[^1234567890]/g, '');" max-length="10" value="<?php
                                                                        if (isset($_POST['account_contact_phone'])) {
                                                                            echo $_POST['account_contact_phone'];
                                                                        } else {
                                                                            echo $results_up['account_contact_phone'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('account_contact_phone'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="account_contact_email">Accounts Contact Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input name="account_contact_email" id="account_contact_email"  class="form-control col-md-12"   value="<?php
                                                                        if (isset($_POST['account_contact_email'])) {
                                                                            echo $_POST['account_contact_email'];
                                                                        } else {
                                                                            echo $results_up['account_contact_email'];
                                                                        }
                                                                        ?>"/>                                                                
                                                                        <span class="error"><?php echo form_error('account_contact_email'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div style="clear:both;"></div>

                                                            <h4 class="header blue bolder smaller">Dealer Access Crediential</h4>

                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="username">Username</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" onchange="checkuser_name();" required name="username" id="username" value="<?php
                                                                        if (isset($_POST['username'])) {
                                                                            echo $_POST['username'];
                                                                        } else {
                                                                            echo $results_up['username'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                                                                              
                                                                        <input type="hidden" name="dealer_login_id"  id="dealer_login_id" value="<?php 
                                                                        if (isset($_POST['dealer_login_id'])) {
                                                                            echo $_POST['dealer_login_id'];
                                                                        } else {
                                                                            echo $results_up['dealer_id'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                                                                              
                                                                        <span class="error"><?php echo form_error('username'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="user_password">Passoword</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="user_password" required  id="user_password" value="<?php
                                                                        if (isset($_POST['user_password'])) {
                                                                            echo $_POST['user_password'];
                                                                        } else {
                                                                            echo $results_up['user_password'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                                                                              
                                                                        <span class="error"><?php echo form_error('user_password'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="user_email">Email</label>
                                                                <div >
                                                                    <div >
                                                                        <input type="text" name="user_email" required id="user_email" value="<?php
                                                                        if (isset($_POST['user_email'])) {
                                                                            echo $_POST['user_email'];
                                                                        } else {
                                                                            echo $results_up['user_email'];
                                                                        }
                                                                        ?>" class="form-control col-md-12">                                                                                                                              
                                                                        <span class="error"><?php echo form_error('user_email'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>




                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" required for="status">Status</label>
                                                                <div >
                                                                    <div >
                                                                        <?php
                                                                        $status = $results_up['status'];
                                                                        if ($status == '1' || $_POST['status'] == '1') {
                                                                            $chk1 = 'selected';
                                                                        } else {
                                                                            $chk1 = '';
                                                                        }

                                                                        if ($status == '2' || $_POST['status'] == '2') {
                                                                            $chk2 = 'selected';
                                                                        } else {
                                                                            $chk2 = '';
                                                                        }
                                                                        ?>
                                                                        <select name="status" id="status" class="form-control col-md-12">
                                                                            <option  value="">Select Status</option>
                                                                            <option <?php echo $chk1; ?> value="1">Enabled</option>    
                                                                            <option <?php echo $chk2; ?> value="2">Disabled</option>    
                                                                        </select>                                                                      

                                                                        <span class="error"><?php echo form_error('status'); ?></span>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div style="clear:both;"></div>
                                                            <!--
                                                            <h4 class="header blue bolder smaller">Documents Upload</h4>
                                                            <?php
                                                            if(count($document_status)>0)
                                                            {    
                                                            foreach($document_status as $document_status_data)
                                                            {
                                                            if($document_status_data['file_type']=='mou_pic')
                                                            {
                                                            if($document_status_data['status']=='1')
                                                            {
                                                            $mou_message='Document Verifed';    
                                                            $mou_message_color='success';    
                                                            }
                                                            else if($document_status_data['status']=='2')
                                                            {
                                                            $mou_message='Document Rejected';    
                                                            $mou_message_color='danger';    
                                                            }
                                                            else
                                                            {
                                                            $mou_message='Document Pending for upload';    
                                                            $mou_message_color='';        
                                                            }    
                                                            }
                                                            
                                                            if($document_status_data['file_type']=='agreement_pic')
                                                            {
                                                            if($document_status_data['status']=='1')
                                                            {
                                                            $agreement_message='Document Verifed';    
                                                            $agreement_message_color='success';    
                                                            }
                                                            else if($document_status_data['status']=='2')
                                                            {
                                                            $agreement_message='Document Rejected';    
                                                            $agreement_message_color='danger';    
                                                            }
                                                            else
                                                            {
                                                            $agreement_message='Document Pending for upload';    
                                                            $agreement_message_color='';        
                                                            }    
                                                            }
                                                            
                                                            if($document_status_data['file_type']=='comission_pic')
                                                            {
                                                            if($document_status_data['status']=='1')
                                                            {
                                                            $comission_message='Document Verifed';    
                                                            $comission_message_color='success';    
                                                            }
                                                            else if($document_status_data['status']=='2')
                                                            {
                                                            $comission_message='Document Rejected';    
                                                            $comission_message_color='danger';    
                                                            }
                                                            else
                                                            {
                                                            $comission_message='Document Pending for upload';    
                                                            $comission_message_color='';        
                                                            }    
                                                            }
                                                            
                                                            
                                                            if($document_status_data['file_type']=='process_flow_chart_pic')
                                                            {
                                                            if($document_status_data['status']=='1')
                                                            {
                                                            $process_message='Document Verifed';    
                                                            $process_message_color='success';    
                                                            }
                                                            else if($document_status_data['status']=='2')
                                                            {
                                                            $process_message='Document Rejected';    
                                                            $process_message_color='danger';    
                                                            }
                                                            else
                                                            {
                                                            $process_message='Document Pending for upload';    
                                                            $process_message_color='';        
                                                            }    
                                                            }
                                                            
                                                            if($document_status_data['file_type']=='dealer_location_map_pic')
                                                            {
                                                            if($document_status_data['status']=='1')
                                                            {
                                                            $dealer_message='Document Verifed';    
                                                            $dealer_message_color='success';    
                                                            }
                                                            else if($document_status_data['status']=='2')
                                                            {
                                                            $dealer_message='Document Rejected';    
                                                            $dealer_message_color='danger';    
                                                            }
                                                            else
                                                            {
                                                            $dealer_message='Document Pending for upload';    
                                                            $dealer_message_color='';        
                                                            }    
                                                            }
                                                            
                                                            }
                                                            }
                                                            else {
                                                            $dealer_message='Document Pending for upload';        
                                                            $process_message='Document Pending for upload';
                                                            $comission_message='Document Pending for upload';    
                                                            $agreement_message='Document Pending for upload';    
                                                            $mou_message='Document Pending for upload';    
                                                            }
                                                            ?>
                                                            
                                                            
                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="mou_copy">MOU copy</label>
                                                                <input type="file" name="mou_copy" id="mou_copy" class="form-control">                                                                                                                              
                                                                <span class="error">
                                                                    <?php echo $mou_message;?>
                                                                    <?php echo form_error('mou_copy'); ?></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="agreement_copy">Agreement copy</label>
                                                                <div >
                                                                    <input type="file"  class="form-control" name="agreement_copy" id="agreement_copy">                                                                                                                              
                                                                    <span class="error">
                                                                        <?php echo $agreement_message;?>
                                                                        <?php echo form_error('agreement_copy'); ?></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="commision_slab">Commission Slabs</label>
                                                                <div >
                                                                    <input type="file"  class="form-control" name="commision_slab" id="commision_slab">                                                                                                                              
                                                                    <span class="error">
                                                                        <?php echo $comission_message;?>
                                                                        <?php echo form_error('commision_slab'); ?></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="process_flow_Chart">Process Flow Chart</label>
                                                                <div >
                                                                    <input type="file"  class="form-control" name="process_flow_Chart" id="process_flow_Chart">                                                                                                                              
                                                                    <span class="error">
                                                                        <?php echo $process_message;?>
                                                                        <?php echo form_error('process_flow_Chart'); ?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label class="control-label pull-left" for="dealer_location_map">Dealer Location Map</label>
                                                                <div >
                                                                    <input type="file"  class="form-control" name="dealer_location_map" id="dealer_location_map">                                                                                                                              
                                                                    <span class="error">
                                                                        <?php echo $dealer_message;?>
                                                                        <?php echo form_error('dealer_location_map'); ?></span>
                                                                </div>
                                                            </div>
                                                            -->
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                            if (empty($results_up)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Dealer Name</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <!--<th>Brand</th>-->
                                            <th>City</th>                                                    
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>

                            </div>     

                        </div>
                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->
            <script>
                function checkuser_name()
                {
                    var username = $("#username").val();

                    if (username != '')
                    {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url() . 'dealer_prices/check_dealer'; ?>",
                            data: {username: username}
                        }).done(function (html) {
                            if (html == 0)
                            {
                                return true;
                            } else
                            {
                                alert('Username already exist,please try some other username');
                                $("#username").val("");
                            }
                        });
                    }
                }

                prePopulate_data2 = [];
                prePopulate_data = [];
            </script>            
            <?php
            if (!empty($ext_brands)) {
                $data = '';
                foreach ($ext_brands as $ext_brands_data) {
                    $brand_id = $ext_brands_data['brand_id'];
                    $brand_name = $ext_brands_data['brand_name'];
                    $data = '{id:"' . $brand_id . '",name:"' . $brand_name . '"},' . $data;
                }
                $otherbrands = substr($data, 0, -1);
                ?>
                <script>
                    var prePopulate_data2 = [<?php echo $otherbrands; ?>];
                </script>
                <?php
            }
            ?>
            <script>
                var prePopulate_data = [<?php if ($get_res->primary_brand != '') { ?>{id:"<?php echo $get_res->primary_brand; ?>", name:"<?php echo $get_res->brand_name; ?>"}<?php } ?>
                ];</script>
            <script src="<?php echo base_url() . 'assets/js/modules/validate_dealer.js' ?>" ></script>
            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



