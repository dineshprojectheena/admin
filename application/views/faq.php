<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->

                    <script>
                        $(document).ready(function() {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'stitcher/faq_result'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function(sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });
                    </script>
                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Faq
                            </div>
                            <?php
//                            print_r($banner_get);
                            foreach ($faq_get as $res)
                                
                                ?>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <form action="<?php echo site_url("faq_upload"); ?>" class="form-horizontal" id="validation-form" method="POST" enctype="multipart/form-data" role="form" novalidate="novalidate">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="main_heading">Top Heading</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" value="<?php
                                                if (isset($_POST['main_heading'])) {
                                                    echo $_POST['main_heading'];
                                                } else {
                                                    echo $res->main_heading;
                                                }
                                                ?>" name="main_heading" id="main_heading" class="col-xs-10 col-sm-5">
                                                <input type="hidden" value="<?php echo $res->faq_id; ?>" name="faq_id" id="faq_id" >
                                                <input type="hidden" value="<?php echo $res->faq_id; ?>" name="count2" id="count2" >

                                            </div>
                                            <span class="error"><?php echo form_error('main_heading'); ?></span>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>


                                    <div class="form-group">
                                        <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="s2id_autogen1">Status</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <?php
                                            $status = $res->status;
                                            if ($status == 1) {
                                                $chk1 = 'selected';
                                            } else if ($status == 2) {
                                                $chk2 = 'selected';
                                            }
                                            ?>
                                            <select name="status" id="status">
                                                <option <?php
                                                if ($status == '1') {
                                                    echo $chk1;
                                                }
                                                ?> value="1">Enabled</option>    
                                                <option <?php
                                                if ($status == '2') {
                                                    echo $chk2;
                                                }
                                                ?> value="2">Disabled</option>    
                                            </select> 
                                        </div>
                                        <span class="error"><?php echo form_error('status'); ?></span>
                                    </div>

                                    <div class="space-4"></div>


                                    <div class="form-group">
                                        <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="type">Type</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <?php
                                            $type = $res->type;
                                            ?>
                                            <select name="type" id="type">
                                                <option <?php
                                                if ($type == 1) {
                                                    echo 'selected';
                                                }
                                                ?> value="1">Buyer FAQs</option>    
                                                <option <?php
                                                if ($type == 2) {
                                                    echo 'selected';
                                                }
                                                ?> value="2">MNC FAQs</option>    
                                                <option <?php
                                                if ($type == 3) {
                                                    echo 'selected';
                                                }
                                                ?> value="3">Dealer FAQs</option>    
                                            </select> 
                                        </div>
                                        <span class="error"><?php echo form_error('type'); ?></span>
                                    </div>

                                    <div class="space-4"></div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="banner_desc">Descrtiption</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <!--<textarea name="banner_desc" id="banner_desc" class="col-xs-10 col-sm-5"><?php echo $res->descrtiption; ?></textarea>-->
                                                <div class="wysiwyg-editor" id="editor1" name="details" onkeyup="passdata();"></div>
                                                <textarea style="display:none;" type="hidden" id="banner_desc" name="banner_desc"><?php
                                                if(isset($_POST['banner_desc'])){
                                                echo $_POST['banner_desc'];
                                                } else {
                                                echo $res->descrtiption;
                                                }
                                                ?></textarea>
                                                <script>
                                                function passdata()
                                                {
                                                var editor1 = $('#editor1').html();
                                                $('#banner_desc').val(editor1);
                                                }
                                                </script>    

                                            </div>
                                            <span class="error"><?php echo form_error('banner_desc'); ?></span>
                                        </div>
                                    </div>


                                    <div class="space-4"></div>




                                    <?php
//                                    print_r($faq_get);
                                    if (empty($faq_get)) {
                                        ?>
                                        <div class="clearfix form-actions">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="submit" class="btn btn-info">                                                
                                                    Submit
                                                </button>

                                                &nbsp; &nbsp; &nbsp;
                                                <button class="btn" type="reset">
                                                    <i class="icon-undo bigger-110"></i>
                                                    Reset
                                                </button>
                                            </div>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="clearfix form-actions">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="update" class="btn btn-info">                                                
                                                    Update
                                                </button>

                                            </div>
                                        </div>


                                        <?php
                                    }
                                    ?>   

                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Heading</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table> 


                            <!--                            <div class="form-group">
                            <?php
                            if (isset($results)):
                                ?>                                    
                                                                                        <div class="table-header">
                                                                                            Results for "Brand"
                                                                                        </div>
                                                                                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>#</th>
                                                                                                    <th>Heading</th>
                                                                                                    <th>Description</th>
                                                                                                    <th>Type</th>
                                                                                                    <th>Status</th>
                                                                                                    <th></th>
                                                                                                    <th></th>
                                                                                                </tr>
                                                                                            </thead>
                                                    
                                                                                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($results as $r) :
                                    ?>
                                                                                                                            <tr>                                                
                                                                                                                                <td>
                                    <?php echo $i; ?>                                                        
                                                                                                                                </td>
                                                                                                                                <td class="align_tble">
                                    <?php echo $r->main_heading; ?>
                                                                                                                                </td>
                                                                                                                                <td class="align_tble">
                                    <?php echo $r->descrtiption; ?>
                                                                                                                                </td>
                                                                                                                                <td class="align_tble">
                                    <?php
                                    $type = $r->type;
                                    if ($type == 1) {
                                        echo 'Buyer FAQs';
                                    } else if ($type == 2) {
                                        echo 'MNC FAQs';
                                    } else if ($type == 3) {
                                        echo 'Dealers FAQs';
                                    }
                                    ?>
                                                                                                                                </td>
                                                                                                                                <td class="align_tble">
                                    <?php
                                    $status = $r->status;
                                    if ($status == 1) {
                                        echo 'Enable';
                                    } else if ($status == 2) {
                                        echo 'Disable';
                                    }
                                    ?>
                                                                                                                                </td>
                                                                                                                        <form action="<?php echo 'faq_upload'; ?>" method="POST">
                                                                                                                            <input type="hidden" name="count2"  value="<?php echo $r->faq_id; ?>" ?>
                                                                                                                            <td class="align_tble">
                                                                                                                                <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="edit">
                                                                                                                                    Edit
                                                                                                                                </button>                                                        
                                                                                                                            </td>
                                                                                                                        </form>
                                                                                                                        <form action="<?php echo 'faq_upload'; ?>" method="POST" >
                                                                                                                            <input type="hidden" name="count"  value="<?php echo $r->faq_id; ?>" ?>
                                                                                                                            <td>
                                                                                                                                <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                                                                                    delete
                                                                                                                                </button>
                                                                            
                                                                                                                            </td>                                                    
                                                                                                                        </form>
                                                                                                                        </tr>
                                    <?php $i++; ?>    
                                <?php endforeach; ?>
                                                    
                                                    
                                                                                            </tbody>
                                                                                        </table>
                                <?php
                            else:
//echo '<p>No Task Exist</p>';
                            endif;
                            ?>
                                                        </div>-->


                            <!--div class="col-xs-12">

                                <div class="form-group">
                                    <div class="table-header">
                                        Results for "Banners"
                                    </div>
                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Title</th>
                                                <th>Image</th>
                                                <th>Description</th>
                                                <th>Link</th>                                                    
                                                <th>Status</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                            <?php
                            $i = 1;
                            if (isset($results)):foreach ($results as $r) :
                                    ?>

                                                                                                                                            <tr>	
                                                                                                                                        <form action="<?php echo 'stitcher/delete_update'; ?>" method="POST" enctype="multipart/form-data">
                                                                                                                                            <td class="align_tble"><?php echo $i++; ?>
                                                                                                                                                <input type="hidden" name="count"  value="<?php echo $r->banner_id; ?>" ?>
                                                                                                                                                <input type="hidden" name="count2"  value="<?php echo $i; ?>" ?>
                                                                                                                                            </td>
                                                                                                                                            <td class="align_tble">
                                                                                                                                                <input type="text" name="<?php echo 'name' . $i; ?>" id="<?php echo 'name' . $i; ?>" value="<?php echo $r->name; ?>" readonly="false" class="removeborder"/>
                                                                                                                                            </td>
                                                                                                                                            <td class="align_tble">
                                                                                                                                                <input type="text" name="<?php echo 'title' . $i; ?>" id="<?php echo 'title' . $i; ?>" value="<?php echo $r->title; ?>" readonly="false" class="removeborder"/>
                                                                                                                                            </td>
                                                                                                                                            <td class="align_tble">
                                                                                                                                                <input type="file" name="<?php echo 'image' . $i; ?>" id="<?php echo 'image' . $i; ?>"  class="removeborder"/>
                                                                                                                                            </td>
                                                                                                                                            <td class="align_tble">
                                                                                                                                                <input type="text" name="<?php echo 'description' . $i; ?>" id="<?php echo 'description' . $i; ?>" value="<?php echo $r->description; ?>" readonly="false" class="removeborder"/>
                                                                                                                                            </td>
                                                                                                                                            <td class="align_tble">
                                                                                                                                                <input type="text" name="<?php echo 'link' . $i; ?>" id="<?php echo 'link' . $i; ?>" value="<?php echo $r->link; ?>" readonly="false" class="removeborder"/>
                                                                                                                                            </td>
                                                                                                                                            <td class="align_tble">
                                    <?php
                                    $status = $r->status;
                                    if ($status == 1) {
                                        $chk1 = 'selected';
                                    } else if ($status == 2) {
                                        $chk2 = 'selected';
                                    }
                                    ?>
                                                                                                                                                <select name="<?php echo 'status' . $i; ?>" id="<?php echo 'status' . $i; ?>" disabled class="removeborder">
                                                                                                                                                    <option <?php
                                    if ($status == '1') {
                                        echo $chk1;
                                    }
                                    ?> value="1">Enabled</option>    
                                                                                                                                                    <option <?php
                                    if ($status == '2') {
                                        echo $chk2;
                                    }
                                    ?> value="2">Disabled</option>    
                                                                                                                                                </select>    

                                                                                                                                                <input type="hidden" value="<?php echo $r->status; ?>"  />
                                                                                                                                            </td>
                                                                                                                                            <td class="align_tble">
                                                                                                                                                <button type="button" class="btn btn-xs btn-success" style="display:block;" id="edit<?php echo $i; ?>" onclick="update_all('<?php echo $i; ?>');" name="update">
                                                                                                                                                    Edit
                                                                                                                                                </button>
                                                                                                                                                <button type="submit" class="btn btn-xs btn-success"  style="display:none;" id="update<?php echo $i; ?>" name="update">
                                                                                                                                                    Update
                                                                                                                                                </button>                                                                
                                                                                                                                            </td>
                                                                                                                                            <td>
                                                                                                                                                <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                                                                                                    delete
                                                                                                                                                </button>

                                                                                                                                            </td>   
                                                                                                                                        </form>
                                                                                                                                        </tr>

                                <?php endforeach; ?>
                                <?php
                            else:
                                echo '<p>No Task Exist</p>';
                            endif;
                            ?>

                                        </tbody>
                                    </table>

                                </div-->
                        </div>     

                    </div>




                    <!-- main content data end here -->

                </div><!-- /.main-content -->                

            </div><!-- /.main-container-inner -->

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!-- footer js file links start here -->
        <?php $this->load->view('templates/js') ?>    
        <script src="<?php echo base_url() . 'assets/js/modules/validate_faq.js'; ?>"></script>
        <!-- footer js file links end here -->

        <!-- footer data links start here -->
        <?php $this->load->view('templates/footer') ?>    
        <!-- footer data links end here -->  

    </body>
</html>



