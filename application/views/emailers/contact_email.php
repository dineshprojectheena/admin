Dear Mr/Ms. <?php echo $firstname.' '.$lastname;?>,
<br>
Greetings from MYNEWCAR.in!!
<br>
Congratulations on booking your new car!!
<br>
As a next step our designated partner dealer will get in touch with you in the next 72 hours to proceed with your booking Invoice No. <?php echo $invoice_no;?> 
<br>
Communication, Discipline, Trust and Customer Satisfaction are our core values and we are constantly in touch with our partners to make sure they share the same values when dealing with our customers.
<br>
Please feel free to contact us via email or by calling on 022-30071555 for any assistance. (Mon – Sat 10.00hrs -19.00hrs)
<br>
Congratulations once again!
<br><br>
Regards,<br>
Team MYNEWCAR.in<br>
<img src="<?php echo base_url().'uploads/logo.png';?>"/><br>

Phone: 91 22 300 71 555 | Email: contact@mynewcar.in<br>
www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br><br>
---------------------------------------------------------------------------------------------------------------------------------
<br><br>
DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>
Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>
Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>