<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here --><div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                                <script>
                                        $(document).ready(function() {
                                            $('.table').dataTable({
                                                "bProcessing": true,
                                                "bServerSide": true,
                                                "sAjaxSource": '<?php echo base_url().'stitcher/brand_result';?>',
                                                "aaSorting": [[4, "desc"]],
                                                "fnServerData": function(sSource, aoData, fnCallback) {
                                                    $.ajax({
                                                        "dataType": 'json',
                                                        "type": "POST",
                                                        "url": sSource,
                                                        "data": aoData,
                                                        "success": fnCallback
                                                    });
                                                }
                                            });
                                        });
                                    </script>
                    

                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Brand
                            </div>                            
                        </div>
                        <?php 
                        $status = $this->session->flashdata('status');                        
                        if (!empty($status)): ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <?php echo $status ?>
                                <br>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                foreach ($results_ups as $get_res)
                                    
                                    ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form  id="validation-form" novalidate="novalidate" style="display: block;" action="<?php echo site_url("upload_brand_group"); ?>" method="POST" enctype="multipart/form-data" role="form" class="form-horizontal">
                                            <div class="span6">
                                                <?php
                                                $valid = validation_errors();
                                                if (!empty($valid)) {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                        <li style="display:none;">
                                                            <a data-toggle="tab" href="#profile3">
                                                                <i class="blue icon-user bigger-110"></i>
                                                                Seo
                                                            </a>
                                                        </li>                                            
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <!--div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="brand_group">Brand Group</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <select id="brand_group" name="brand_group" data-placeholder="Click to Choose..." style="float:left;width:337px;">
                                                                            <option value="">select brand</option>
                                                            <?php
                                                            foreach($results_brand as $brand_res){
                                                                if($brand_res->brand_group_id == $get_res->brand_group_id || $_POST['brand_group'] == $brand_res->brand_group_id) {
                                                                    $res = 'selected';
                                                                }else{
                                                                    $res = '';
                                                                }
                                                                ?>
                                                                <option <?php echo $res; ?> value="<?php echo $brand_res->brand_group_id; ?>"><?php echo $brand_res->group_name; ?></option>
                                                                <?php
                                                            }
                                                            ?>

                                                                        </select>
                                                                        <span class="error" style="float:left;"><?php echo form_error('brand_group'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div-->
                                                            <div class="space-4"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="brand_name">Brand Name</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="brand_name" id="brand_name" data-rel="tooltip"  value="<?php
                                                                        if (!isset($_POST['brand_name'])) {
                                                                            if ($get_res->brand_name != '') {
                                                                                echo str_replace("-"," ", $get_res->brand_name);                                                                                
                                                                            }
                                                                        } else if (isset($_POST['brand_name'])) {
                                                                            echo str_replace("-"," ", $_POST['brand_name']);
//                                                                            echo $_POST['brand_name'];
                                                                        }
                                                                        ?>" class="col-xs-10 col-sm-5"> 
                                                                        
                                                                        <!--<span class="sample_data">(eg. Audi)</span>-->                                                                

                                                                        <input type="hidden" name="brand_id" id="brand_id" value="<?php
                                                                        if ($get_res->brand_id != '') {
                                                                            echo $get_res->brand_id;
                                                                        } else if (isset($_POST['brand_id'])) {
                                                                            echo $_POST['brand_id'];
                                                                        }
                                                                        ?>">                                                                
                                                                        <span class="error"><?php echo form_error('brand_name'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="brand_description">Description</label>
                                                                <div class="col-sm-9">
                                                                    <?php
//                                                                    print_r($get_res);
                                                                    ?>
                                                                    <div class="clearfix">
                                                                        <!--div class="wysiwyg-editor" id="editor1" onblur="pass_desc_txt();" name="editor1" style="height:100px;width:99%;"><?php echo $get_res->meta_description; ?></div-->
                                                                        <textarea name="brand_description" id="brand_description" required style="width:42%;"><?php
                                                                            if ($get_res->description != '') {
                                                                                echo $get_res->description;
                                                                            } else {
                                                                                echo $results_ups['description'];
                                                                            }
                                                                            ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="brand_images">Brand Pic</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
<!--                                                                    <input type="file" name="brand_images" id="brand_images" value="<?php
                                                                        if (!isset($_POST['brand_images'])) {
                                                                            if ($get_res->brand_image != '') {
                                                                                echo $get_res->brand_image;
                                                                            }
                                                                        } else if (isset($_POST['brand_images'])) {
                                                                            echo $_POST['brand_images'];
                                                                        }
                                                                        ?>" class="col-xs-10 col-sm-5">-->
                                                                        <?php
                                                                        if (!isset($_POST['brand_images'])) {
                                                                            if ($get_res->brand_image != '') {
                                                                                $brand_image = $get_res->brand_image;
                                                                            }
                                                                        } else if (isset($_POST['brand_images'])) {
                                                                            $brand_image = $_POST['brand_images'];
                                                                        }
                                                                        ?>
                                                                        <?php
                                                                        /*if (!empty($brand_image)) {
                                                                            ?>
                                                                            <img src="<?php echo base_url() . 'uploads/brand/' . $brand_image ?>" width="100" style="float:right;margin-right:-100px;"/>
                                                                            <?php
                                                                        }*/
                                                                        ?>
                                                                        <input type="file" style="float:left;" name="brand_images" accept="image/*" id="brand_images"value="<?php
                                                                        if (!isset($_POST['brand_image2'])) {
                                                                            if ($get_res->brand_image != '') {
                                                                                echo $get_res->brand_image;
                                                                            }
                                                                        } else if (isset($_POST['brand_image2'])) {
                                                                            echo $_POST['brand_image2'];
                                                                        }
                                                                        ?>" />

                                                                        <input type="hidden" name="brand_image2" id="brand_image2" accept="image/*" value="<?php
                                                                        if (!isset($_POST['brand_image2'])) {
                                                                            if ($get_res->brand_image != '') {
                                                                                echo $get_res->brand_image;
                                                                            }
                                                                        } else if (isset($_POST['brand_image2'])) {
                                                                            echo $_POST['brand_image2'];
                                                                        }
                                                                        ?>" class="col-xs-10 col-sm-5">
                                                                        <span class="error"><?php echo form_error('brand_images'); ?></span>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                            
<!--                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="variant_showcase_image">Variant Showcase Image</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <?php
                                                                        if (!isset($_POST['variant_showcase_image'])){
                                                                            if($get_res->variant_showcase_image!=''){
                                                                            $variant_showcase_image=$get_res->variant_showcase_image;
                                                                            }
                                                                        } else if (isset($_POST['variant_showcase_image'])) {
                                                                            $variant_showcase_image=$_POST['variant_showcase_image'];
                                                                        }
                                                                        ?>
                                                                        <?php
                                                                        /*
                                                                        if (!empty($variant_showcase_image)) {
                                                                            ?>
                                                                            <img src="<?php echo base_url() . 'uploads/brand_variant_showcase/' . $variant_showcase_image ?>" width="100" style="float:right;margin-right:-100px;"/>
                                                                            <?php
                                                                        }*/
                                                                        ?>
                                                                        <input type="file" style="float:left;" name="variant_showcase_image" id="variant_showcase_image"value="<?php
                                                                        if(!isset($_POST['variant_showcase_image'])) {
                                                                            if ($get_res->variant_showcase_image!='') {
                                                                                echo $get_res->variant_showcase_image;
                                                                        }
                                                                        } else if (isset($_POST['variant_showcase_image'])) {
                                                                            echo $_POST['variant_showcase_image'];
                                                                        }
                                                                        ?>" />

                                                                        <input type="hidden" name="variant_showcase_image2" id="variant_showcase_image2" value="<?php
                                                                        if (!isset($_POST['variant_showcase_image2'])) {
                                                                            if ($get_res->variant_showcase_image!= '') {
                                                                                echo $get_res->variant_showcase_image;
                                                                            }
                                                                        } else if (isset($_POST['variant_showcase_image'])) {
                                                                            echo $_POST['variant_showcase_image'];
                                                                        }
                                                                        ?>" class="col-xs-10 col-sm-5">
                                                                        <span class="error"><?php echo form_error('variant_showcase_image'); ?></span>
                                                                    </div>

                                                                </div>

                                                            </div>-->
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="details">Brand Details</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <textarea name="details" id="details" style="width:42%;" class="col-xs-10 col-sm-5"><?php
                                                                            if (!isset($_POST['details'])) {
                                                                                if ($get_res->details != '') {
                                                                                    echo $get_res->details;
                                                                                }
                                                                            } else if (isset($_POST['details'])) {
                                                                                echo $_POST['details'];
                                                                            }
                                                                            ?></textarea>                                                                
                                                                        <span class="error"><?php echo form_error('details'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            


                                                            <div class="form-group">
                                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>
                                                                <div class="col-xs-12 col-sm-9">
                                                                    <?php
                                                                    $status = $get_res->status;
                                                                    ?>
                                                                    <select name="status" id="status" style="width:42%;float:left;">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php
                                                                        if (!isset($_POST['status'])) {
                                                                            if ($status == 1) {
                                                                                echo 'selected';
                                                                            }
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> value="1">Enabled</option>    
                                                                        <option <?php
                                                                        if (!isset($_POST['status'])) {
                                                                            if ($status == 2) {
                                                                                echo 'selected';
                                                                            }
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> value="2">Disabled</option>    
                                                                    </select>   
                                                                    <span class="error"><?php echo form_error('status'); ?></span>	
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div id="profile3" class="tab-pane" style="display:none;">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="title">Seo Title</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="title" id="title" value="<?php
                                                                        if ($get_res->cust_title != '') {
                                                                            echo $get_res->cust_title;
                                                                        } else {
                                                                            echo $results_ups['cust_title'];
                                                                        }
                                                                        ?>" class="col-xs-10 col-sm-5">
                                                                    </div>
                                                                    <span class="error"><?php echo form_error('status'); ?></span>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="editor1">Seo Description</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <!--div class="wysiwyg-editor" id="editor1" onblur="pass_desc_txt();" name="editor1" style="height:100px;width:99%;"><?php echo $get_res->meta_description; ?></div-->
                                                                        <textarea  name="description" id="description" style="width:340px;"><?php
                                                                            if ($get_res->meta_description != '') {
                                                                                echo $get_res->meta_description;
                                                                            } else {
                                                                                echo $results_ups['meta_description'];
                                                                            }
                                                                            ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="editor2">Seo Keyword</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <!--div class="wysiwyg-editor" id="editor2" onblur="pass_key_txt();" name="editor2" style="height:100px;width:99%;"><?php echo $get_res->meta_keyword; ?></div>
                                                                        <input type="hidden" name="keyword" id="keyword" value="<?php echo $get_res->meta_keyword; ?>" /-->
                                                                        <textarea  name="keyword" id="keyword" style="width:340px;"><?php
                                                                            if ($get_res->meta_keyword != '') {
                                                                                echo $get_res->meta_keyword;
                                                                            } else {
                                                                                echo $results_ups['meta_keyword'];
                                                                            }

                                                                            //echo $get_res->meta_keyword; 
                                                                            ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                        </div>                                            
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($results_ups)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                
                                <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Brand Id</th>
                                                <th>Brand Name</th>
                                                <!--<th>Brand Group Name</th>-->
                                                <th>Details</th>
                                                <th>Status</th>
                                                <th>Action</th>
<!--                                            <th>Download</th>-->
                                            </tr>
                                        </thead>
                                    </table>

<!--                                <div class="form-group">
                                    <?php
                                    if (isset($results)):
                                        ?>                                    
                                        <div class="table-header">
                                            Results for "Brand"
                                        </div>
                                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Brand Name</th>
                                                    th>Brand Group Name</th
                                                    <th>Details</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($results as $r) :
                                                    ?>
                                                    <tr>                                                
                                                        <td>
                                                            <?php echo $i; ?>                                                        
                                                        </td>
                                                        <td>
                                                            <?php 
                                                            echo str_replace("-"," ", $r->brand_name);
                                                            ?>
                                                        </td>
                                                        td>
                                                        <?php echo $r->group_name; ?>
                                                        </td
                                                        <td>
                                                            <?php echo $r->details; ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $status = $r->status;
                                                            if ($status == 1) {
                                                                echo 'Enable';
                                                            } else if ($status == 2) {
                                                                echo 'Disable';
                                                            }
                                                            ?>                                                            
                                                        </td>
                                                <form action="<?php echo 'stitcher/get_brand'; ?>" method="POST">
                                                    <input type="hidden" name="get_brandgrp_id"  value="<?php echo $r->brand_id; ?>" ?>
                                                    <td class="align_tble">
                                                        <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                            Edit
                                                        </button>                                                        
                                                    </td>
                                                </form>
                                                <form action="<?php echo 'stitcher/del_brand'; ?>" method="POST" >
                                                    <input type="hidden" name="brandgrp_id"  value="<?php echo $r->brand_id; ?>" ?>
                                                    <td>
                                                        <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                            delete
                                                        </button>

                                                    </td>                                                    
                                                </form>
                                                </tr>
                                                <?php $i++; ?>    
                                            <?php endforeach; ?>


                                            </tbody>
                                        </table>
                                        <?php
                                    else:

                                    endif;
                                    ?>
                                </div>-->
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>
            <script src="<?php echo base_url().'assets/js/modules/validate_brand.js' ?>"></script>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



