<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->


                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Information Page
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                foreach ($info_get as $get_res)
                                    //print_r($get_res);
                                    
                                    ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form action="<?php echo site_url("stitcher/info_upload"); ?>" class="form-horizontal" method="POST">
                                            <div class="span6">
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                        <li class="">
                                                            <a data-toggle="tab" href="#profile3">
                                                                <i class="blue icon-user bigger-110"></i>
                                                                Seo
                                                            </a>
                                                        </li>                                            
                                                    </ul>

                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="info_title">Information Title</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="info_title" id="info_title" value="<?php echo $get_res->title; ?>" class="col-xs-10 col-sm-5">                                                                
                                                                        
                                                                        <input type="hidden" name="information_id" id="information_id" value="<?php echo $get_res->information_id; ?>">                                                                
                                                                    </div>
                                                                    <span class="error"><?php echo form_error('info_title');?></span>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="space-4"></div>

                                                           <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="sort_order">Description</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">                                                                        
                                                                        <div class="wysiwyg-editor" id="editor3" onblur="pass_desc_txt2();" name="editor3" style="height:100px;width:99%;"><?php echo $get_res->meta_description; ?></div>
                                                                        <input type="hidden" name="details" id="details" value="<?php echo $get_res->description; ?>"/>
                                                                    </div>
                                                                    <span class="error"><?php echo form_error('details');?></span>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="space-4"></div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="sort_order">Sort Order</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="sort_order" id="sort_order" value="<?php echo $get_res->sort_order; ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        
                                                                    </div>
                                                                    <span class="error"><?php echo form_error('sort_order');?></span>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="space-4"></div>
                                                           
                                                            
                                                            <div class="form-group">
                                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>
                                                                <div class="col-xs-12 col-sm-9">
                                                                    <?php
                                                                    $status = $get_res->status;
                                                                    if ($status == 1) {
                                                                        $chk1 = 'selected';
                                                                    } else if ($status == 2) {
                                                                        $chk2 = 'selected';
                                                                    }
                                                                    ?>
                                                                    <select name="status" id="status" >
                                                                        <option <?php
                                                                        if ($status == '1') {
                                                                            echo $chk1;
                                                                        }
                                                                        ?> value="1">Enabled</option>    
                                                                        <option <?php
                                                                        if ($status == '2') {
                                                                            echo $chk2;
                                                                        }
                                                                        ?> value="2">Disabled</option>    
                                                                    </select>  
                                                                </div>
                                                                 <span class="error"><?php echo form_error('status');?></span>
                                                            </div>
                                                        </div>

                                                        <div id="profile3" class="tab-pane">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="title">Title</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="title" id="title" value="<?php echo $get_res->cust_title; ?>" class="col-xs-10 col-sm-5">
                                                                    </div>
                                                                     <span class="error"><?php echo form_error('status');?></span>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="editor1">Description</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <div class="wysiwyg-editor" id="editor1" onblur="pass_desc_txt();" name="editor1" style="height:100px;width:99%;"><?php echo $get_res->meta_description; ?></div>
                                                                        <input type="hidden" name="description" id="description" value="<?php echo $get_res->meta_description; ?>"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="editor2">Keyword</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <div class="wysiwyg-editor" id="editor2" onblur="pass_key_txt();" name="editor2" style="height:100px;width:99%;"><?php echo $get_res->meta_keyword; ?></div>
                                                                        <input type="hidden" name="keyword" id="keyword" value="<?php echo $get_res->meta_keyword; ?>" />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                        </div>                                            
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($info_get)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="form-group">
                                    <?php
                                    if (isset($results)):
                                        ?>                                    
                                        <div class="table-header">
                                            Results for "Information"
                                        </div>
                                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Information Title</th>
                                                    <th>Sort Order</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($results as $r) :
                                                    ?>
                                                    <tr>                                                
                                                        <td>
                                                            <?php echo $i; ?>                                                        
                                                        </td>
                                                        <td>
                                                            <?php echo $r->title; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $r->sort_order; ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $status = $r->status;
                                                            if ($status == 1) {
                                                                $chk1 = 'selected';
                                                            } else if ($status == 2) {
                                                                $chk2 = 'selected';
                                                            }
                                                            ?>
                                                            <select name="<?php echo 'status' . $i; ?>" id="<?php echo 'status' . $i; ?>" disabled class="removeborder">
                                                                <option <?php
                                                                if ($status == '1') {
                                                                    echo $chk1;
                                                                }
                                                                ?> value="1">Enabled</option>    
                                                                <option <?php
                                                                if ($status == '2') {
                                                                    echo $chk2;
                                                                }
                                                                ?> value="2">Disabled</option>    
                                                            </select>                                                            
                                                        </td>
                                                <form action="<?php echo 'stitcher/info_upload'; ?>" method="POST">
                                                    <input type="hidden" name="information_id"  value="<?php echo $r->information_id; ?>" ?>
                                                    <td class="align_tble">
                                                        <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                            Edit
                                                        </button>                                                        
                                                    </td>
                                                </form>
                                                <form action="<?php echo 'stitcher/info_upload'; ?>" method="POST" >
                                                    <input type="hidden" name="information_id"  value="<?php echo $r->information_id; ?>" ?>
                                                    <td>
                                                        <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                            delete
                                                        </button>

                                                    </td>                                                    
                                                </form>
                                                </tr>
                                                <?php $i++; ?>    
                                            <?php endforeach; ?>


                                            </tbody>
                                        </table>
                                        <?php
                                    else:
//echo '<p>No Task Exist</p>';
                                    endif;
                                    ?>
                                </div>
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



