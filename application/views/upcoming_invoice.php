<style>
ul
{
display: block;list-style-type: disc;margin-top: 1em;margin-bottom: 1 em;margin-left: 0;margin-right: 0;padding-left: 40px;    
}
</style>
<div style="width:80%;margin:10%;">
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px;margin-top:30px;">
        <thead>
            <tr>
                <td style="width:33.33%;font-size:12px;border-right:0px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <div style="float:left;">
                        <a href="" style="text-align:center" target="_blank">
                            <img class="CToWUd" src="<?php echo base_url(); ?>uploads/logo2.png" style="margin-bottom:10px;border:none;width:30%;">
                        </a> 
                    </div >    

                </td>
                <td style="width:33.33%;font-size:12px;border-right:0px solid #dddddd;border-bottom:1px solid #dddddd;text-align:center;padding:7px">
                    <div style="font-size:16px;text-transform:uppercase;font-weight:bold;" align="center">Booking Confirmation</div>   
                </td>
                <td style="width:33.33%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    <a href="" title="" style="text-align:center" target="_blank">
                        <img class="CToWUd" src="<?php echo base_url(); ?>assets/img/home/contact-img.png" alt="" style="margin-top:15px;border:none;width:30%;;">
                    </a>    
                </td>
            </tr>
        </thead>
    </table> 
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
    
        <thead>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222" colspan="2">
                    <span class="il">Order</span> Details</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <b><span class="il">Pre-Order Booking No:</span></b> <?php echo $upcoming_details['invoice_no']; ?><br>
                    
                </td>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <b>Customer Name:</b> <?php echo $customer_details[0]['firstname'] . ' ' . $customer_details[0]['lastname']; ?><br>
                </td>
            </tr>
            <tr>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <b><span class="il">Date of Booking:</span></b> <?php 
                    $added_date=substr($upcoming_details['added_date'], 0, 10); 
                    $exp=explode('-',$added_date);
                    echo $exp[2].'-'.$exp[1].'-'.$exp[0];
                    ?><br>
                </td>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px" >
                    <b>Registration City:</b><span style="text-transform:capitalize;"> <?php echo $upcoming_details['city_name']; ?></span><br>
                </td>
            </tr>
        </tbody>


    </table>


    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Vehicle Information</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <div style="float:left;width:100%;">
                        <?php echo $upcoming_details['product_name']; ?><br>
                        
                    </div>                   
                    <div style="float:left;width:100%;">
                        <img style="margin-bottom:10px;border:none;width:20%;" src="<?php echo $pro_image = img_url('/uploads/product/' . $upcoming_details['pro_image']); ?>" class="CToWUd">    
                    </div>
                    
                </td>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <p>Refundable Booking Amount: Rs. <?php echo $upcoming_details['booking_amount']; ?></p>   
                    <p>Comments: <?php echo $upcoming_details['comments']; ?></p>   
                </td>
            </tr>
            
        </tbody>
        <thead>
            
        </thead>
    </table>


    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td  style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    <span class="il">Correspondence</span>  Address</td>
                <td  style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    <span class="il">Registration</span>  Address</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:50%%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <?php echo $customer_details[0]['firstname'].' '.$customer_details[0]['lastname'];?>
                    <br>
                    <?php echo $customer_details[0]['address'];?>
                    <br>                    
                    <?php echo $customer_details[0]['city'];?>
                    <br>
                    <?php echo $customer_details[0]['state'];?>
                    <br>                    
                    <?php echo $customer_details[0]['pincode'];?>
                    <br>                    
                    <?php echo $customer_details[0]['email'];?>
                    <br>                    
                    <?php echo $customer_details[0]['telephone'];?>
                    <br>                                        
                </td>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <?php echo $registeration_details[0]['name'];?>
                    <br>
                    <?php echo $registeration_details[0]['address'];?>
                    <br>                    
                    <?php echo $registeration_details[0]['city'];?>
                    <br>
                    <?php echo $registeration_details[0]['state'];?>
                    <br>                    
                    <?php echo $registeration_details[0]['pincode'];?>
                    <br>                    
                    <?php echo $registeration_details[0]['email'];?>
                    <br>                    
                    <?php echo $registeration_details[0]['telephone'];?>
                    <br>         
                </td>
            </tr>

        </tbody>


    </table>

    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td  colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    <span class="il">Important </span> Information</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2" style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <ul >
                        <li style="font-size:10px;">Full Manufacturer Warranty</li>    
                        <li style="font-size:10px;">After Sales Service provided by our partnered authorized dealer</li>    
                        <li style="font-size:10px;">Special Deals on car accessories,insurance,finance,car exchange and other related services can be availed only on purchase of a new car through www.mynewcar.in</li>    
                        <li style="font-size:10px;">Tentative delivery period of 2 - 5 weeks is based on variant and color availability</li>    
                        <li style="font-size:10px;">All the offers are promoted by our respective partners</li>    
                        <li style="font-size:10px;">It is mandatory to have a Photo Identity along with Address Proof for your vehicle registration.This can include Passport, Adhar Card, Ration Card, Telephone Bill,
                    Deed, Pan Card or any other Identification Proof issued by The Government of India.</li>    
                        <li style="font-size:10px;">Color change may attract additional charges as applicable by manufacturers</li>    
                        <li style="font-size:10px;">Specification and features are subject to change without any notice</li>    
                        <li style="font-size:10px;">Accessories shown are not part of standard equipment. Features vary from model to model. Colours are indicative only.</li>    
                        <li style="font-size:10px;">Please refer to our website for Terms of Offer for Sale.</li>    
                        
                    </ul>
                    <div style="margin-left:10px;">
                        1. Taxes & other charges as applicable.<br>
                        2. On-Road Price indicated is based on on Taxes & Local Levies as applicable (subject to change)<br>
                        <!--3. Additional Savings via Group Discount can be availed in case the number of car bookings for the specified car brand reaches its target within the specified deal period<br>-->
                        <!--3. Total Savings include the Additional Group Discount Option<br>-->
                        3. Fully refundable within 48 hours of booking<br>
                    </div>    
                </td>

            </tr>

        </tbody>


    </table>
<!--<pagebreak />-->
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td  style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222;">

                    <img style="margin-bottom:10px;border:none;width:100%;" alt="Prrems" src="<?php echo base_url() . 'uploads/booking_flow.png'; ?>"  class="CToWUd">    
                    <!--<img style="margin-bottom:10px;border:none;" alt="Prrems" src="<?php echo $pro_image = base_url() . 'uploads/booking_flow.png' . $car_into_details[0]['pro_image']; ?>" class="CToWUd">-->    


                </td>                
            </tr>
        </thead>
        
    </table>
    
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222;">
                    <b>Our customer care will get in touch with you within the next 48 hours to confirm the delivery details along with offers on other related services and
                        accessories requested by you. You could also reach our customer care for any clarification by emailing at contact@mynewcar.in or on +91 22 45
                        02 03 04 (Daily l 10 am - 7 pm)</b>
                </td>
            </tr>
        </thead>
    </table>




    

        <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">MYNEWCAR.IN Support</td>

            </tr>
        </thead>
        
        
        <tbody>
            <tr>
                <td colspan="2" style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    Web: https://mynewcar.in/contact-us
                    <br>
                    FAQ's: https://mynewcar.in/faq
                    <br>
                    Telephone: +022 45 02 03 04  (Daily l 10 am - 7 pm)
                    <br>
                    Email: contact@mynewcar.in
                    <br>
                    Visit Us on Facebook or Follow Us on Twitter
                    <br>
                    DREAMZ MYNEWCAR INDIA Pvt. Ltd. Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.

                </td>

            </tr>
        </tbody>
    </table>


</div>
