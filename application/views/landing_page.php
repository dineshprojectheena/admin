<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here --><div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->

                    <script>
                        $(document).ready(function () {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'stitcher/landing_result'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function (sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });
//                        function edit_record(id)
//                        {
//                            alert(id);
//                            $.ajax({
//                                type: "POST",
//                                url: "<?php echo site_url('stitcher/load_edit_record') ?>",
//                                data: {trimline: trimline, engine: engine, transmission: transmission, pro_id: pro_id, selected_vairant: selected_vairant}
//                            }).done(function (html) {
//                                $("#result").html(html);
////            $("#compare-variant2").css("display","block");
//                            });
//                        }


                    </script>
                    <!--Landing page master... 
                    Id, name, email, City, preferred brand model variant, bg color, bg image, 
                    submit button one, submit button two and their tex
                    -->

                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Landing Page
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <form  id="validation-form" action="<?php echo site_url("upload_landing_data"); ?>" method="POST" enctype="multipart/form-data" role="form" class="form-horizontal">
                                            <div class="span6">
                                                <?php
//                                                print_r($query);
                                                $message = $this->session->flashdata('message');

                                                
                                                if(($message)=='Updated') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Updated:
                                                        </strong>
                                                        Data Updated Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                } 
                                                if(($message)=='error') {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                } 
                                                else if(($message)=='Submited') {
                                                   
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>
                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Successful:
                                                        </strong>
                                                        Data Uploaded Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="name">Landing Page SEO Url</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="name" value="<?php echo $query['name']; ?>" id="name"class="col-md-3"> 
                                                                        <input type="hidden" name="main_id" value="<?php echo $query['landing_page_id']; ?>" id="main_id"class="col-md-3"> 
                                                                        <span class="error"><?php echo form_error('name'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="page_heading">Landing Page Title</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="page_heading" id="page_heading" class="col-md-3" value="<?php echo $query['title']; ?>"> 
                                                                        <span class="error"><?php echo form_error('page_heading'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>                                                            
                                                            <div class="space-4"></div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="page_heading_color">Landing Page Title Color</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="page_heading_color" id="page_heading_color" class="col-md-3" value="<?php echo $query['page_heading_color']; ?>"> 
                                                                        <span class="error"><?php echo form_error('page_heading_color'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <div class="space-4"></div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="page_heading_Size">Landing Page Title Size</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="page_heading_Size" id="page_heading_Size" class="col-md-3" value="<?php echo $query['page_heading_Size']; ?>"> 
                                                                        <span class="error"><?php echo form_error('page_heading_Size'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="back_color">Background Color</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="back_color" id="back_color" value="<?php echo $query['background_color']; ?>" class="col-md-3"> 
                                                                        <span class="error"><?php echo form_error('back_color'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="back_image">Background Image</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="file" name="back_image" id="back_image" accept="image/*"  value="<?php echo $query['background_image']; ?>" class="col-md-3"> 
                                                                        <input type="hidden" name="back_image1" id="back_image1" value="<?php echo $query['background_image']; ?>" class="col-md-3"> 
                                                                        <span class="error"><?php echo form_error('back_image'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="main_image">Foreground Image</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="file" name="main_image" id="main_image" accept="image/*"  value="<?php echo $query['main_image']; ?>" class="col-md-3"> 
                                                                        <input type="hidden" name="main_image1" id="main_image1" value="<?php echo $query['main_image']; ?>" class="col-md-3"> 
                                                                        <span class="error"><?php echo form_error('main_image'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="menu">Menu Status</label>
                                                                <div class="col-xs-12 col-sm-9">
                                                                    <select name="menu" id="menu" class="col-md-3">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php if($query['menu_status']=='1'){ echo 'selected'; }?>  value="1">Enable</option>
                                                                        <option <?php if($query['menu_status']=='2'){ echo 'selected'; }?> value="2">Disable</option>                                                                        
                                                                    </select>   
                                                                    <span class="error"><?php echo form_error('menu'); ?></span>	
                                                                </div>

                                                            </div>
                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="video_status">Video Status</label>
                                                                <div class="col-xs-12 col-sm-9">
                                                                    <select name="video_status" id="video_status" class="col-md-3">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php if($query['video_status']=='1'){ echo 'selected'; }?>  value="1">Enable</option>
                                                                        <option <?php if($query['video_status']=='2'){ echo 'selected'; }?> value="2">Disable</option>                                                                        
                                                                    </select>   
                                                                    <span class="error"><?php echo form_error('video_status'); ?></span>	
                                                                </div>

                                                            </div>
                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="testimonials">Testimonials Status</label>
                                                                <div class="col-xs-12 col-sm-9">
                                                                    <select name="testimonials" id="testimonials" class="col-md-3">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php if($query['testimonials_status']=='1'){ echo 'selected'; }?>  value="1">Enable</option>
                                                                        <option <?php if($query['testimonials_status']=='2'){ echo 'selected'; }?>  value="2">Disable</option>                                                                        
                                                                    </select>   
                                                                    <span class="error"><?php echo form_error('testimonials'); ?></span>	
                                                                </div>

                                                            </div>
                                                            <div class="space-4"></div>
                                                            <div class="form-group">
                                                                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="timmer">Timer Status</label>
                                                                <div class="col-xs-12 col-sm-9">
                                                                    <select name="timmer" id="timmer" class="col-md-3">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php if($query['timmer']=='1'){ echo 'selected'; }?>  value="1">Enable</option>
                                                                        <option <?php if($query['timmer']=='2'){ echo 'selected'; }?>  value="2">Disable</option>                                                                        
                                                                    </select>   
                                                                    <span class="error"><?php echo form_error('timmer'); ?></span>	
                                                                </div>

                                                            </div>
                                                            <div class="space-4"></div>




                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="main_video">Foreground Video</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="main_video" id="main_video" value="<?php echo $query['main_video']; ?>" class="col-md-3"> 
                                                                        <span class="error"><?php echo form_error('main_video'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="Description">Description</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <textarea type="text" name="Description" id="Description" class="col-md-3"><?php echo $query['Description']; ?></textarea> 
                                                                        <span class="error"><?php echo form_error('Description'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <hr/>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="form_heading">Form Heading</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="input" name="form_heading" id="form_heading" class="col-md-3"  value="<?php echo $query['form_heading']; ?>"> 
                                                                        <span class="error"><?php echo form_error('form_heading'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="text_fields">Text Fields</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <?php
                                                                        $exp=explode(',',$query['text_fields']);  
//                                                                        print_r($exp);
                                                                        
                                                                        $exp2=explode('--',$query['button_fields']);  
//                                                                        print_r($exp2);
                                                                        ?>                                                                        
                                                                        <select name="text_fields[]"  style="height:80px;" class="col-md-3" multiple>
                                                                            <option <?php if (in_array("1", $exp)) {    echo "selected"; }  ?>  value="1">First Name</option>
                                                                            <option  <?php if (in_array("2", $exp)) {    echo "selected"; } ?> value="2">Last Name</option>                                                                        
                                                                            <option <?php if (in_array("3", $exp)) {    echo "selected"; } ?>  value="3">Number</option>                                                                        
                                                                            <option  <?php if (in_array("4", $exp)) {    echo "selected"; } ?> value="4">Email</option>                                                                        
                                                                            <option  <?php if (in_array("5", $exp)) {    echo "selected"; } ?> value="5">Book A Test Drive</option>                                                                        
                                                                            
                                                                        </select>   
                                                                        <span class="error"><?php echo form_error('text_fields'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right">Submit Text</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        
                                                                        <input type="input" name="button_fields[]" class="col-md-3"  value="<?php echo $exp2[0]; ?>"> 

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right">Cancel Text</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="input" name="button_fields[]" id="text_fields[]" class="col-md-3"  value="<?php echo $exp2[1]; ?>"> 
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right">End Date</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="input" name="end_date" id="end_date" class="col-md-3"  value="<?php echo $query['end_date']; ?>"> 
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            


                                                            <hr/>

                                                            <div class="form-group">
                                                                <!--<input type="text" name="icons1" id="icons1" value="<?php echo $query['icons']; ?>" class="col-md-3">--> 
                                                                <?php
                                                                $exp3=explode(',',$query['icons']);                                                                        
                                                                ?>
                                                                <label class="col-sm-3 control-label no-padding-right"  for="icons">Icons</label>
                                                                <div class="col-sm-9">
                                                                    <div class="col-md-2">
                                                                        Car Insurance Offer
                                                                        <input type="checkbox" name="icons[]" id="icons"  <?php if (in_array("1", $exp3)) {    echo "checked='true'"; }  ?> value="1" style="padding:10px;">                                                                         
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        Car Finance Offer
                                                                        <input type="checkbox" name="icons[]" id="icons" <?php if (in_array("2", $exp3)) {    echo "checked='true'"; }  ?> value="2" style="padding:10px;">                                                                         
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        Car Exchange Offer
                                                                        <input type="checkbox" name="icons[]" id="icons" <?php if (in_array("3", $exp3)) {    echo "checked='true'"; }  ?> value="3"  style="padding:10px;">                                                                         
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        Car Gifts
                                                                        <input type="checkbox" name="icons[]" id="icons" <?php if (in_array("4", $exp3)) {    echo "checked='true'"; }  ?> value="4"  style="padding:10px;">                                                                         
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                        </div>                                                                                                 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <div class="clearfix form-actions">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php
                                                    if(!empty($query))
                                                    {
                                                    ?>
                                                    <button type="submit" name="Update" class="btn btn-info">                                                
                                                        Update
                                                    </button>
                                                    <?php
                                                    }
                                                    else
                                                    {    
                                                    ?>
                                                    <button type="submit" name="submit" class="btn btn-info">                                                
                                                        Submit
                                                    </button>

                                                    &nbsp; &nbsp; &nbsp;
                                                    <button class="btn" type="reset">
                                                        <i class="icon-undo bigger-110"></i>
                                                        Reset
                                                    </button>
                                                    <?php
                                                    }    
                                                    ?>
                                                </div>
                                            </div>


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>

                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Feature</h4>
                                    </div>
                                    <div class="modal-body">
                                        ...
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary select-feature">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-xs-12">

                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Landing Id</th>
                                            <th>Landing Page SEO Url</th>
                                            <th>Landing Page Title</th>
                                            <!--<th>Email</th>-->
                                            <th>Background Color</th>
                                            <th>Background Image</th>
                                            <th>Description</th>
                                            <th>Action</th>
<!--                                            <th>Download</th>-->
                                        </tr>
                                    </thead>
                                </table>

                                <!--                                <div class="form-group">
                                <?php
                                if (isset($results)):
                                    ?>                                    
                                                                                            <div class="table-header">
                                                                                                Results for "Brand"
                                                                                            </div>
                                                                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>#</th>
                                                                                                        <th>Brand Name</th>
                                                                                                        th>Brand Group Name</th
                                                                                                        <th>Details</th>
                                                                                                        <th>Status</th>
                                                                                                        <th></th>
                                                                                                        <th></th>
                                                                                                    </tr>
                                                                                                </thead>
                                                    
                                                                                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($results as $r) :
                                        ?>
                                                                                                                            <tr>                                                
                                                                                                                                <td>
                                        <?php echo $i; ?>                                                        
                                                                                                                                </td>
                                                                                                                                <td>
                                        <?php
                                        echo str_replace("-", " ", $r->brand_name);
                                        ?>
                                                                                                                                </td>
                                                                                                                                td>
                                        <?php echo $r->group_name; ?>
                                                                                                                                </td
                                                                                                                                <td>
                                        <?php echo $r->details; ?>
                                                                                                                                </td>
                                                                                                                                <td>
                                        <?php
                                        $status = $r->status;
                                        if ($status == 1) {
                                            echo 'Enable';
                                        } else if ($status == 2) {
                                            echo 'Disable';
                                        }
                                        ?>                                                            
                                                                                                                                </td>
                                                                                                                        <form action="<?php echo 'stitcher/get_brand'; ?>" method="POST">
                                                                                                                            <input type="hidden" name="get_brandgrp_id"  value="<?php echo $r->brand_id; ?>" ?>
                                                                                                                            <td class="align_tble">
                                                                                                                                <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                                                                                                    Edit
                                                                                                                                </button>                                                        
                                                                                                                            </td>
                                                                                                                        </form>
                                                                                                                        <form action="<?php echo 'stitcher/del_brand'; ?>" method="POST" >
                                                                                                                            <input type="hidden" name="brandgrp_id"  value="<?php echo $r->brand_id; ?>" ?>
                                                                                                                            <td>
                                                                                                                                <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                                                                                    delete
                                                                                                                                </button>
                                                                        
                                                                                                                            </td>                                                    
                                                                                                                        </form>
                                                                                                                        </tr>
                                        <?php $i++; ?>    
                                    <?php endforeach; ?>
                                                    
                                                    
                                                                                                </tbody>
                                                                                            </table>
                                    <?php
                                else:

                                endif;
                                ?>
                                                                </div>-->
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>
            <script src="<?php echo base_url() . 'assets/js/modules/validate_landing.js' ?>"></script>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>   

            <!-- footer data links end here -->  
            <style>
                #brand_id
                {
                    width:100px;    
                }
            </style>

    </body>
</html>



