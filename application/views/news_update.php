<?php
foreach ($results_news as $r)
//    print_r($r);
$res_count=count($results_news);    
?>

<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <script type="text/javascript">
        function update_all(id)
        {
//        alert(id);    
            document.getElementById('update' + id).style.display = 'block';
            document.getElementById('edit' + id).style.display = 'none';
            document.getElementById('main_heading' + id).className = "";
            document.getElementById('main_heading' + id).readOnly = false;
            document.getElementById('descrtiption' + id).className = "";
            document.getElementById('descrtiption' + id).readOnly = false;
            document.getElementById('status' + id).className = "";
            document.getElementById('status' + id).disabled = false;

        }
    </script>

    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->


                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                News
                            </div>
                            <?php //echo validation_errors(); ?>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <form action="<?php echo site_url("stitcher/news_update"); ?>" class="form-horizontal" id="create-accessory" method="POST" enctype="multipart/form-data" role="form" novalidate="novalidate">
                                    <div class="span6">
                                        <div class="tabbable tabs-left">
                                            <ul class="nav nav-tabs" id="myTab3">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#home3">
                                                        <i class="pink icon-dashboard bigger-110"></i>
                                                        Details
                                                    </a>
                                                </li>

                                                <li class="">
                                                    <a data-toggle="tab" href="#profile3">
                                                        <i class="blue icon-user bigger-110"></i>
                                                        Seo
                                                    </a>
                                                </li>                                            
                                            </ul>
                                            
                                            <div class="tab-content">
                                                <div id="home3" class="tab-pane active">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="news_title">News Heading</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input type="text" name="news_title" value="<?php echo $r->news_title;?>" id="news_title" class="col-xs-10 col-sm-5">
                                                                <input type="hidden" name="new_id" value="<?php echo $r->latest_news_id;?>" id="new_id" class="col-xs-10 col-sm-5">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="details">Details</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <div class="wysiwyg-editor" id="editor1" name="details" onkeyup="passdata();"><?php
                                                                    if(isset($_POST['news_main_heading'])) {
                                                                    echo $_POST['news_main_heading'];
                                                                    }
                                                                    else{
                                                                    echo $r->details;    
                                                                    }
                                                                    ?></div>
                                                                <textarea style="display:none;" type="hidden" id="news_titles" name="news_titles"><?php 
                                                                if(isset($_POST['news_titles']))
                                                                { 
                                                                echo $_POST['news_titles'];
                                                                }
                                                                else{
                                                                echo $r->details;    
                                                                }
                                                                ?></textarea>
                                                                <script> 
                                                                function passdata()
                                                                {
                                                                var editor1=$('#editor1').html();                                                                          
                                                                $('#news_titles').val(editor1);
                                                                }
                                                                </script>    
                                                                
                                                                <!--<div class="wysiwyg-editor" id="editor1" name="details"><?php echo $r->details;?></div>-->
                                                                <!--<textarea name="details" id="details" class="col-xs-10 col-sm-5"><?php echo $r->details;?></textarea>-->
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="pub_date">Publish Date</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input class="span10 date-picker" name="id-date-picker-1" id="id-date-picker-1" value="<?php echo $r->publish_date;?>"  type="text" data-date-format="dd-mm-yyyy" />
                                                                <span class="add-on">
                                                                    <i class="icon-calendar"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="news_title">Author</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input type="text" name="Author" id="Author" class="col-xs-10 col-sm-5" value="<?php echo $r->author;?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="pic">Pic</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input type="file" name="news_pic" id="news_pic" class="col-xs-10 col-sm-5" value="<?php echo $r->image;?>">
                                                                <input type="hidden" name="news_pic2" id="news_pic2" class="col-xs-10 col-sm-5" value="<?php echo $r->image;?>">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>




                                                    <div class="form-group">
                                                        <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>
                                                        <div class="col-xs-12 col-sm-9">
                                                             <?php
                                                        $status = $r->status;
                                                        if ($status == 1) {
                                                            $chk1 = 'selected';
                                                        } else if ($status == 2) {
                                                            $chk2 = 'selected';
                                                        }
                                                        ?>
                                                        <select id="status" name="status">
                                                            <option <?php
                                                            if ($status == '1') {
                                                                echo $chk1;
                                                            }
                                                            ?> value="1">Enabled</option>    
                                                            <option <?php
                                                            if ($status == '2') {
                                                                echo $chk2;
                                                            }
                                                            ?> value="2">Disabled</option>    
                                                        </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="profile3" class="tab-pane">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="title">Title</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input type="text" name="title" value="<?php echo $r->cust_title;?>" id="title" class="col-xs-10 col-sm-5">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="editor1">Description</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <div class="wysiwyg-editor" id="editor1" onblur="pass_desc_txt();" name="editor1" style="height:100px;width:99%;"><?php echo $r->meta_description;?></div>
                                                                <input type="hidden" name="description" value="<?php echo $r->meta_description;?>" id="description" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="editor2">Keyword</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <div class="wysiwyg-editor" id="editor2" onblur="pass_key_txt();" name="editor2" style="height:100px;width:99%;"><?php echo $r->meta_keyword;?></div>
                                                                <input type="hidden" name="keyword" value="<?php echo $r->meta_keyword;?>" id="keyword" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                </div>                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-4"></div>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" name="Update" class="btn btn-info">                                                
                                                Update
                                            </button>

                                            &nbsp; &nbsp; &nbsp;
                                            
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->
            <!--[if !IE]>-->
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
            <!--<![endif]-->		
            <script src="<?php echo base_url(); ?>assets/js/jquery.hotkeys.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.min.js"></script>
            <script type="text/javascript">
                                                    function pass_desc_txt()
                                                    {
                                                        var editor1 = $('#editor1').html();
                                                        $('#description').val(editor1);
                                                    }
                                                    function pass_key_txt()
                                                    {
                                                        var editor2 = $('#editor2').html();
                                                        $('#keyword').val(editor2);
                                                    }
                                                    $(function() {

                                                        function showErrorAlert(reason, detail) {
                                                            var msg = '';
                                                            if (reason === 'unsupported-file-type') {
                                                                msg = "Unsupported format " + detail;
                                                            }
                                                            else {
                                                                console.log("error uploading file", reason, detail);
                                                            }
                                                            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                                                    '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                                                        }

                                                        //but we want to change a few buttons colors for the third style
                                                        $('#editor1').ace_wysiwyg({
                                                            toolbar:
                                                                    [
                                                                        'font',
                                                                        null,
                                                                        'fontSize',
                                                                        null,
                                                                        {name: 'bold', className: 'btn-info'},
                                                                        {name: 'italic', className: 'btn-info'},
                                                                        {name: 'strikethrough', className: 'btn-info'},
                                                                        {name: 'underline', className: 'btn-info'},
                                                                        null,
                                                                        {name: 'insertunorderedlist', className: 'btn-success'},
                                                                        {name: 'insertorderedlist', className: 'btn-success'},
                                                                        {name: 'outdent', className: 'btn-purple'},
                                                                        {name: 'indent', className: 'btn-purple'},
                                                                        null,
                                                                        {name: 'justifyleft', className: 'btn-primary'},
                                                                        {name: 'justifycenter', className: 'btn-primary'},
                                                                        {name: 'justifyright', className: 'btn-primary'},
                                                                        {name: 'justifyfull', className: 'btn-inverse'},
                                                                        null,
                                                                        {name: 'createLink', className: 'btn-pink'},
                                                                        {name: 'unlink', className: 'btn-pink'},
                                                                        null,
                                                                        {name: 'insertImage', className: 'btn-success'},
                                                                        null,
                                                                        'foreColor',
                                                                        null,
                                                                        {name: 'undo', className: 'btn-grey'},
                                                                        {name: 'redo', className: 'btn-grey'}
                                                                    ],
                                                            'wysiwyg': {
                                                                fileUploadError: showErrorAlert
                                                            }
                                                        }).prev().addClass('wysiwyg-style2');






                                                        $('[data-toggle="buttons-radio"]').on('click', function(e) {
                                                            var target = $(e.target);
                                                            var which = parseInt($.trim(target.text()));
                                                            var toolbar = $('#editor1').prev().get(0);
                                                            if (which == 1 || which == 2 || which == 3) {
                                                                toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
                                                                if (which == 1)
                                                                    $(toolbar).addClass('wysiwyg-style1');
                                                                else if (which == 2)
                                                                    $(toolbar).addClass('wysiwyg-style2');
                                                            }
                                                        });

                                                        $('#editor2').ace_wysiwyg({
                                                            toolbar:
                                                                    [
                                                                        'font',
                                                                        null,
                                                                        'fontSize',
                                                                        null,
                                                                        {name: 'bold', className: 'btn-info'},
                                                                        {name: 'italic', className: 'btn-info'},
                                                                        {name: 'strikethrough', className: 'btn-info'},
                                                                        {name: 'underline', className: 'btn-info'},
                                                                        null,
                                                                        {name: 'insertunorderedlist', className: 'btn-success'},
                                                                        {name: 'insertorderedlist', className: 'btn-success'},
                                                                        {name: 'outdent', className: 'btn-purple'},
                                                                        {name: 'indent', className: 'btn-purple'},
                                                                        null,
                                                                        {name: 'justifyleft', className: 'btn-primary'},
                                                                        {name: 'justifycenter', className: 'btn-primary'},
                                                                        {name: 'justifyright', className: 'btn-primary'},
                                                                        {name: 'justifyfull', className: 'btn-inverse'},
                                                                        null,
                                                                        {name: 'createLink', className: 'btn-pink'},
                                                                        {name: 'unlink', className: 'btn-pink'},
                                                                        null,
                                                                        {name: 'insertImage', className: 'btn-success'},
                                                                        null,
                                                                        'foreColor',
                                                                        null,
                                                                        {name: 'undo', className: 'btn-grey'},
                                                                        {name: 'redo', className: 'btn-grey'}
                                                                    ],
                                                            'wysiwyg': {
                                                                fileUploadError: showErrorAlert
                                                            }
                                                        }).prev().addClass('wysiwyg-style2');






                                                        $('[data-toggle="buttons-radio"]').on('click', function(e) {
                                                            var target = $(e.target);
                                                            var which = parseInt($.trim(target.text()));
                                                            var toolbar = $('#editor2').prev().get(0);
                                                            if (which == 1 || which == 2 || which == 3) {
                                                                toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
                                                                if (which == 1)
                                                                    $(toolbar).addClass('wysiwyg-style1');
                                                                else if (which == 2)
                                                                    $(toolbar).addClass('wysiwyg-style2');
                                                            }
                                                        });

                                                        if (typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase())) {

                                                            var lastResizableImg = null;
                                                            function destroyResizable() {
                                                                if (lastResizableImg == null)
                                                                    return;
                                                                lastResizableImg.resizable("destroy");
                                                                lastResizableImg.removeData('resizable');
                                                                lastResizableImg = null;
                                                            }

                                                            var enableImageResize = function() {
                                                                $('.wysiwyg-editor')
                                                                        .on('mousedown', function(e) {
                                                                            var target = $(e.target);
                                                                            if (e.target instanceof HTMLImageElement) {
                                                                                if (!target.data('resizable')) {
                                                                                    target.resizable({
                                                                                        aspectRatio: e.target.width / e.target.height,
                                                                                    });
                                                                                    target.data('resizable', true);

                                                                                    if (lastResizableImg != null) {//disable previous resizable image
                                                                                        lastResizableImg.resizable("destroy");
                                                                                        lastResizableImg.removeData('resizable');
                                                                                    }
                                                                                    lastResizableImg = target;
                                                                                }
                                                                            }
                                                                        })
                                                                        .on('click', function(e) {
                                                                            if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                                                                                destroyResizable();
                                                                            }
                                                                        })
                                                                        .on('keydown', function() {
                                                                            destroyResizable();
                                                                        });
                                                            }

                                                            enableImageResize();


                                                        }


                                                    });
            </script>

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



