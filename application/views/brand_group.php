<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->


                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Brand Group
                            </div>
                            <?php //echo validation_errors(); ?>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                foreach ($results_up as $get_res)
                                    
                                    ?>
                                <form action="<?php echo site_url("group_upload"); ?>" class="form-horizontal" method="POST">
                                    <?php
                                    $valid = validation_errors();
                                    if (!empty($valid)) {
                                        ?>
                                        <div class="def_error">
                                            <button type="button" class="close" data-dismiss="alert">
                                                <i class="icon-remove"></i>
                                            </button>

                                            <strong>
                                                <i class="icon-remove"></i>
                                                Warning:
                                            </strong>
                                            Please check the form carefully for errors!                                
                                            <br>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="form-group">

                                        <label class="col-sm-3 control-label no-padding-right" for="group_name">Group Name</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" name="group_name" id="group_name" value="<?php
                                                if ($get_res->group_name != '') {
                                                    echo $get_res->group_name;
                                                } else if (isset($_POST['group_name'])) {
                                                    echo $_POST['group_name'];
                                                }
                                                ?>" class="col-xs-10 col-sm-5">

                                                <input type="hidden" name="group_id" id="group_id" value="<?php
                                                if ($get_res->brand_group_id != '') {
                                                    echo $get_res->brand_group_id;
                                                } else if (isset($_POST['group_id'])) {
                                                    echo $_POST['group_id'];
                                                }
                                                ?>">
                                                <span class="error"><?php echo form_error('group_name'); ?></span>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="group_size">Group Size</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" name="group_size" id="group_size" value="<?php
                                                if ($get_res->group_size != '') {
                                                    echo $get_res->group_size;
                                                } else if (isset($_POST['group_size'])) {
                                                    echo $_POST['group_size'];
                                                }
                                                ?>" class="col-xs-10 col-sm-5">
                                                <span class="error"><?php echo form_error('group_size'); ?></span> 
                                            </div>

                                        </div>
                                    </div>

                                    <div class="space-4"></div>                                   

                                    <div class="form-group">
                                        <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <?php
                                            
                                            if ($status == '1' || $_POST['status'] == '1') {
                                                $chk1 = 'selected';
                                            } else {
                                                $chk1 = '';
                                            }

                                            if ($status == '2' || $_POST['status'] == '2') {
                                                $chk2 = 'selected';
                                            } else {
                                                $chk2 = '';
                                            }
                                            ?>                                             
                                            <select name="status" id="status" style="width:370px;float:left;">
                                                <option  value="">Select Status</option>
                                                <option <?php echo $chk1; ?> value="1">Enabled</option>    
                                                <option <?php echo $chk2; ?> value="2">Disabled</option>    
                                            </select>   
                                            <span class="error" style="float:left;"><?php echo form_error('status'); ?></span>
                                        </div>

                                    </div>

                                    <div class="space-4"></div>
                                    <?php
                                    if (empty($results_up)) {
                                        ?>
                                        <div class="clearfix form-actions">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="submit" class="btn btn-info">                                                
                                                    Submit
                                                </button>

                                                &nbsp; &nbsp; &nbsp;
                                                <button class="btn" type="reset">
                                                    <i class="icon-undo bigger-110"></i>
                                                    Reset
                                                </button>
                                            </div>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="clearfix form-actions">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="update" class="btn btn-info">                                                
                                                    Update
                                                </button>

                                            </div>
                                        </div>


                                        <?php
                                    }
                                    ?>

                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="form-group">
                                    <?php
                                    if (isset($results)):
                                        ?>                                    
                                        <div class="table-header">
                                            Results for "Brand Group"
                                        </div>
                                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Group Name</th>
                                                    <th>Group Size</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($results as $r) :
                                                    ?>
                                                    <tr>                                                
                                                        <td>
                                                            <?php echo $i; ?>                                                        
                                                        </td>
                                                        <td>
                                                            <?php echo $r->group_name; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $r->group_size; ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $status = $r->status;
                                                            if ($status == 1) {
                                                                $chk1 = 'selected';
                                                            } else if ($status == 2) {
                                                                $chk2 = 'selected';
                                                            }
                                                            ?>
                                                            <select name="<?php echo 'status' . $i; ?>" id="<?php echo 'status' . $i; ?>" disabled class="removeborder">
                                                                <option <?php
                                                                if ($status == '1') {
                                                                    echo $chk1;
                                                                }
                                                                ?> value="1">Enabled</option>    
                                                                <option <?php
                                                                if ($status == '2') {
                                                                    echo $chk2;
                                                                }
                                                                ?> value="2">Disabled</option>    
                                                            </select>                                                            
                                                        </td>
                                                <form action="<?php echo 'stitcher/get_brand_group'; ?>" method="POST">
                                                    <input type="hidden" name="get_brandgrp_id"  value="<?php echo $r->brand_group_id; ?>" ?>
                                                    <td class="align_tble">
                                                        <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                            Edit
                                                        </button>                                                        
                                                    </td>
                                                </form>
                                                <form action="<?php echo 'stitcher/del_brand_group'; ?>" method="POST" >
                                                    <input type="hidden" name="brandgrp_id"  value="<?php echo $r->brand_group_id; ?>" ?>
                                                    <td>
                                                        <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                            delete
                                                        </button>

                                                    </td>                                                    
                                                </form>
                                                </tr>
                                                <?php $i++; ?>    
                                            <?php endforeach; ?>


                                            </tbody>
                                        </table>
                                        <?php
                                    else:
//echo '<p>No Task Exist</p>';
                                    endif;
                                    ?>
                                </div>
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



