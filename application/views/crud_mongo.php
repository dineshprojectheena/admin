<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <style>
        .col-sm-3
        {
            width:20% !important;    
        }
        .tab-content
        {
            overflow:hidden !important;    
        }
    </style>  
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->
        <div class="main-container" id="main-container">
            <div class="main-container-inner">
                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                <div class="main-content">
                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Mongo Crud
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <form  id="validation-form" novalidate="novalidate" style="display: block;" action="<?php echo site_url("mgo_upload"); ?>" method="POST" enctype="multipart/form-data" role="form" class="form-horizontal">
                                                <div class="span6">

                                                    <div class="tabbable tabs-left">
                                                        <ul class="nav nav-tabs" id="myTab3">
                                                            <li class="active">
                                                                <a data-toggle="tab" href="#home3">
                                                                    <i class="pink icon-dashboard bigger-110"></i>
                                                                    Details
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <?php if ($this->session->flashdata('error_message')) { ?>
                                                            <div class="error">
                                                                <?php echo $this->session->flashdata('error_message'); ?>
                                                            </div>
                                                        <?php }
//                                                        foreach($single as $single_data)
//                                                            print_r($single['name']);
                                                        ?>
                                                        <div class="tab-content">
                                                            <div id="home3" class="tab-pane active">
                                                                <div class="space-4"></div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label no-padding-right"  for="brand_name">Name</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="clearfix">
                                                                            <input type="text" name="name" id="name" value="<?php if($_POST['name']) { echo $_POST['name'];} else { echo $single['name']; }  ?>"  class="col-xs-10 col-sm-5" required="required"> 
                                                                            <input type="hidden" name="id" id="id" value="<?php if($_POST['id']) { echo $_POST['id'];} else { echo $single['_id']; }  ?>" data-rel="tooltip"  class="col-xs-10 col-sm-5"> 

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="space-4"></div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label no-padding-right"  for="brand_name">Dob</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="clearfix">
                                                                            <input type="text" name="dob" id="dob" value="<?php if($_POST['dob']) { echo $_POST['dob'];} else { echo $single['dob']; }  ?>" data-rel="tooltip"  class="col-xs-10 col-sm-5"  required="required"> 
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="space-4"></div>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                      <?php
                                                      if(!empty($user))
                                                      {
                                                      ?>
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>
                                                      <?php
                                                      }
                                                      else {
                                                      ?>
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>
                                                      <?php
                                                      }
                                                      ?>
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>





                                </div>
                                <?php
                                if(!empty($user))
                                {    
                                ?>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Date of Birth</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($user as $list) { ?>
                                                    <tr>
                                                        <td><?php echo $list['name']; ?></td>
                                                        <td><?php echo $list['dob']; ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url();?>crud/?id=<?php echo $list['_id'];?>" class="btn sbtn">Edit</a>
                                                            <a href="<?php echo base_url();?>crud/?del_id=<?php echo $list['_id'];?>" class="btn sbtn">Delete</a>
                                                            <?php 
                                                            /*
                                                            echo anchor('crud/' . $list['_id'], 'Edit', array('class' => 'btn sbtn')); ?> <?php echo anchor('blog/delete_author/' . $list['_id'], 'Delete', array('class' => 'btn sbtn'));
                                                             * 
                                                             *  
                                                             */
                                                            ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                                <?php
                                }    
                                ?>
                                
                                

                            </div>




                            
                                




                        </div>

                    </div><!-- /.main-container-inner -->

                    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                        <i class="icon-double-angle-up icon-only bigger-110"></i>
                    </a>
                </div><!-- /.main-container -->

                <!-- footer js file links start here -->
                <?php $this->load->view('templates/js') ?>    

                <script src="<?php echo base_url() . 'assets/js/modules/validate_bulk_product.js' ?>"></script>
                <!-- footer js file links end here -->

                <!-- footer data links start here -->
                <?php $this->load->view('templates/footer') ?>    
                <!-- footer data links end here -->  

                </body>
                </html>







