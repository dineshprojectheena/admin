<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here --><div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->

                    <script>
                        $(document).ready(function () {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'stitcher/coupon_result'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function (sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });
//                        function edit_record(id)
//                        {
//                            alert(id);
//                            $.ajax({
//                                type: "POST",
//                                url: "<?php echo site_url('stitcher/load_edit_record') ?>",
//                                data: {trimline: trimline, engine: engine, transmission: transmission, pro_id: pro_id, selected_vairant: selected_vairant}
//                            }).done(function (html) {
//                                $("#result").html(html);
////            $("#compare-variant2").css("display","block");
//                            });
//                        }


                    </script>
                    <!--Landing page master... 
                    Id, name, email, City, preferred brand model variant, bg color, bg image, 
                    submit button one, submit button two and their tex
                    -->

                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Coupon Page
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <form  id="validation-form" action="<?php echo site_url("upload_coupon_data"); ?>" method="POST" enctype="multipart/form-data" role="form" class="form-horizontal" style="overflow:hidden;">
                                            <div class="span6">
                                                <?php
//                                                print_r($query);
                                                $message = $this->session->flashdata('message');


                                                if (($message) == 'Updated') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Updated:
                                                        </strong>
                                                        Data Updated Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                if (($message) == 'error') {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                } else if (($message) == 'Submited') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>
                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Successful:
                                                        </strong>
                                                        Data Uploaded Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="start_date">Start Date</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="start_date" value="<?php echo $query['start_date']; ?>" id="start_date"class="col-md-3" placeholder="YYYY-MM-DD">                                                                         
                                                                        <span class="error"><?php echo form_error('start_date'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="end_date">End Date</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="end_date" value="<?php echo $query['end_date']; ?>" id="end_date"class="col-md-3" placeholder="YYYY-MM-DD">                                                                         
                                                                        <span class="error"><?php echo form_error('end_date'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="no_of_coupon">No of Coupon Time</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="no_of_coupon" value="<?php echo $query['no_of_coupon']; ?>" id="no_of_coupon" class="col-md-3" >                                                                         
                                                                        <span class="error"><?php echo form_error('no_of_coupon'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                            
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="coupon_type">Coupon Type</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <select name="coupon_type" id="coupon_type" class="col-md-3" onchange="coupon_type_sel();" >
                                                                            <option  value="">Select Coupon Type</option>
                                                                            <option  value="Money">Money</option>
                                                                            <option  value="Material">Material</option>                                                                                                                                                    
                                                                        </select>
                                                                        <span class="error"><?php echo form_error('apply_for'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            <div class="form-group" style="display:none" id="money_id">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="amount_percent">Amount/Percent</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="amount_percent" value="<?php echo $query['amount_percent']; ?>" id="amount_percent" class="col-md-3" placeholder="eg... 5000 or 5%" >                                                                         
                                                                        <span class="error"><?php echo form_error('amount_percent'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group" style="display:none" id="matreial_id">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="material">Material Detail</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="material" value="<?php echo $query['material']; ?>" id="material" class="col-md-3" placeholder="matrerial detail" >                                                                         
                                                                        <span class="error"><?php echo form_error('material'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="coupon_code">Coupon Code</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="coupon_code" value="<?php echo $query['coupon_code']; ?>" id="coupon_code" class="col-md-3">                                                                         
                                                                                                                                                 
                                                                        <span class="error"><?php echo form_error('coupon_code'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="description">Description</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <textarea type="text" name="description"  id="description" class="col-md-3"><?php echo $query['description']; ?></textarea>                                                                                                                                                 
                                                                        <span class="error"><?php echo form_error('description'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="comment">Comment</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <textarea type="text" name="comment"  id="comment" class="col-md-3"><?php echo $query['comment']; ?></textarea>                                                                                                                                                 
                                                                        <span class="error"><?php echo form_error('comment'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="faq">Faq</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <textarea type="text" name="faq"  id="faq" class="col-md-3"><?php echo $query['faq']; ?></textarea>                                                                                                                                                 
                                                                        <span class="error"><?php echo form_error('faq'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="apply_for">Apply Type</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <select name="apply_for" id="apply_for" class="col-md-3" onchange="select_type();" multiple="multiple">
                                                                            <option  value="">Select Coupon Apply Type</option>
                                                                            <option  value="1">City</option>
                                                                            <option  value="2">Product</option>                                                                        
                                                                            <option  value="3">Brand</option>                                                                        
                                                                            <option  value="4">Color</option>                                                                        
                                                                        </select>
                                                                        <input type="hidden" name="apply_type" value="" id="apply_type" class="col-md-3">
                                                                        <span class="error"><?php echo form_error('apply_for'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                            <div class="form-group" id="city_wise" style="display:none;">

                                                                <h4 class="header blue bolder smaller" style="text-align: left;  width: 30%;  margin-left: 200px;"> City Select Here</h4>
                                                                <label class="col-sm-3 control-label no-padding-right"  for="city">City</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <select name="city" id="city" onchange="select_type();" class="col-md-3" multiple="multiple">
                                                                            <option  value="">Select City</option>
                                                                            <?php
                                                                            foreach ($city as $city_res) {
                                                                                ?>
                                                                                <option  value="<?php echo $city_res['city_id']; ?>"><?php echo $city_res['name']; ?></option>                                                                        
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <input type="hidden" name="city_type" value="" id="city_type" class="col-md-3">
                                                                        <span class="error"><?php echo form_error('apply_for'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>
                                                            <div class="form-group" id="product_wise" style="display:none;">

                                                                <h4 class="header blue bolder smaller" style="text-align: left;  width: 30%;  margin-left: 200px;"> Select Product Here</h4>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                    <div class="col-sm-9" >
                                                                        <select name="brand_id" id="brand_id" onchange="getmodel();" style="width:255px;">
                                                                            <option  value="">Select Brand</option>
                                                                            <?php
                                                                            foreach ($brand as $brand_res) {
                                                                                ?>
                                                                                <option value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>  
                                                                        <br> <span class="error"><?php echo form_error('brand_id'); ?></span>

                                                                    </div> 
                                                                </div> 
                                                                <div class="space-4"></div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                                                    <div class="col-sm-9" style="float:left;" id="model_res" >
                                                                        <select name="model_id" id="model_id" style="width: 255px;">
                                                                            <option  value="">Select model</option>                                                                                                                                                    
                                                                        </select>  
                                                                        <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                                                    </div>


                                                                </div>
                                                                <div class="space-4"></div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                                                    <div class="col-sm-9" style="float:left;" id="variant_results" >
                                                                        <select name="all_product_name" id="all_product_name" style="width: 255px;">
                                                                            <option  value="">Select Variant</option>                                                                            
                                                                        </select>  
                                                                        <span class="error"><?php echo form_error('product_name'); ?></span>
                                                                    </div>
                                                                </div> 
                                                                <div class="space-4"></div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group" id="brand_wise" style="display:none;">

                                                                <h4 class="header blue bolder smaller" style="text-align: left;  width: 30%;  margin-left: 200px;"> Select Brand Here</h4>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="brand_id2">Brand</label>
                                                                    <div class="col-sm-9" >
                                                                        <select name="brand_id2" id="brand_id2" style="width:255px;">
                                                                            <option  value="">Select Brand</option>
                                                                            <?php
                                                                            foreach ($brand as $brand_res) {
                                                                                ?>
                                                                                <option value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>  
                                                                        <br> <span class="error"><?php echo form_error('brand_id2'); ?></span>

                                                                    </div> 
                                                                </div> 
                                                                <div class="space-4"></div>                                                                
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group" id="color_wise" style="display:none;">

                                                                <h4 class="header blue bolder smaller" style="text-align: left;  width: 30%;  margin-left: 200px;"> Select Color Here</h4>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="color">Color</label>
                                                                    <div class="col-sm-9" >
                                                                        <select name="color" id="color" style="width:255px;" onchange="color();">
                                                                            <option  value="">Select Color</option>
                                                                            <?php
                                                                            foreach ($color as $color_res) {
                                                                                ?>
                                                                                <option value="<?php echo $color_res['color']; ?>"><?php echo $color_res['color']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>  
                                                                        <br> <span class="error"><?php echo form_error('color'); ?></span>

                                                                    </div> 
                                                                </div> 
                                                                <div class="space-4"></div>                                                                
                                                            </div>
                                                            <div class="space-4"></div>




                                                        </div>                                                                                                 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <div class="clearfix form-actions">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php
                                                    if (!empty($query)) {
                                                        ?>
                                                        <button type="submit" name="Update" class="btn btn-info">                                                
                                                            Update
                                                        </button>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <script>
                                                function coupon_type_sel()
                                                {                                                
                                                $('#amount_percent').val('');     
                                                $('#material').val('');     
                                                $('#money_id').css('display', 'none');     
                                                $('#matreial_id').css('display', 'none');     
                                                var coupon_type=$('#coupon_type').val();   
                                                
                                                if(coupon_type=='Money')
                                                {
                                                $('#money_id').css('display', 'block');    
                                                }    
                                                else if(coupon_type=='Material')
                                                {
                                                $('#matreial_id').css('display', 'block');    
                                                }    
                                                }
                                                
                                                
                                                function select_type()
                                                {
                                                    $('#city_wise').css('display', 'none');
                                                    $('#product_wise').css('display', 'none');
                                                    $('#brand_wise').css('display', 'none');
                                                    $('#color_wise').css('display', 'none');
                                                    var app = $('#apply_for').val();
                                                    $('#apply_type').val(app);
                                                    
//                                                    alert(app);
                                                    var res = String(app).split(',');
//                                                    alert(res);
                                                    for (var i = 0, len = res.length; i < len; i++) {
                                                    if (res[i] == 1)
                                                    {
                                                        $('#city_wise').css('display', 'block');
                                                    }
                                                    if (res[i] == 2)
                                                    {
                                                        $('#product_wise').css('display', 'block');
                                                    }
                                                    if (res[i] == 3)
                                                    {
                                                        $('#brand_wise').css('display', 'block');
                                                    }
                                                    if (res[i] == 4)
                                                    {
                                                        $('#color_wise').css('display', 'block');
                                                    }
                                                    }
                                                    
                                                    var city = $('#city').val();
                                                    $('#city_type').val(city);
                                                    


                                                    
                                                    
                                                }
                                            </script>


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>

                        <?php
//                        print_r($coupon);
                        ?>


                        <div class="row">
                            <div class="col-xs-12">

                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Coupon Id</th>
                                            <th>Coupon Start Date</th>
                                            <th>Coupon End Date</th>

<!--<th>Email</th>-->
                                            <th>No of Coupon</th>
                                            <th>Coupon Used</th>
                                            <th>Amount/Percent</th>
                                            <th>Coupon Material Detail</th>
                                            <th>Coupon Code</th>
                                            <th>Description</th>
                                            <th>Comment</th>
                                            <th>Action</th>
<!--                                            <th>Download</th>-->
                                        </tr>
                                    </thead>
                                </table>

                                <!--                                <div class="form-group">
                                <?php
                                if (isset($results)):
                                    ?>                                    
                                                                                                                    <div class="table-header">
                                                                                                                        Results for "Brand"
                                                                                                                    </div>
                                                                                                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                                                                                        <thead>
                                                                                                                            <tr>
                                                                                                                                <th>#</th>
                                                                                                                                <th>Brand Name</th>
                                                                                                                                th>Brand Group Name</th
                                                                                                                                <th>Details</th>
                                                                                                                                <th>Status</th>
                                                                                                                                <th></th>
                                                                                                                                <th></th>
                                                                                                                            </tr>
                                                                                                                        </thead>
                                                                            
                                                                                                                        <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($results as $r) :
                                        ?>
                                                                                                                                                                            <tr>                                                
                                                                                                                                                                                <td>
                                        <?php echo $i; ?>                                                        
                                                                                                                                                                                </td>
                                                                                                                                                                                <td>
                                        <?php
                                        echo str_replace("-", " ", $r->brand_name);
                                        ?>
                                                                                                                                                                                </td>
                                                                                                                                                                                td>
                                        <?php echo $r->group_name; ?>
                                                                                                                                                                                </td
                                                                                                                                                                                <td>
                                        <?php echo $r->details; ?>
                                                                                                                                                                                </td>
                                                                                                                                                                                <td>
                                        <?php
                                        $status = $r->status;
                                        if ($status == 1) {
                                            echo 'Enable';
                                        } else if ($status == 2) {
                                            echo 'Disable';
                                        }
                                        ?>                                                            
                                                                                                                                                                                </td>
                                                                                                                                                                        <form action="<?php echo 'stitcher/get_brand'; ?>" method="POST">
                                                                                                                                                                            <input type="hidden" name="get_brandgrp_id"  value="<?php echo $r->brand_id; ?>" ?>
                                                                                                                                                                            <td class="align_tble">
                                                                                                                                                                                <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                                                                                                                                                    Edit
                                                                                                                                                                                </button>                                                        
                                                                                                                                                                            </td>
                                                                                                                                                                        </form>
                                                                                                                                                                        <form action="<?php echo 'stitcher/del_brand'; ?>" method="POST" >
                                                                                                                                                                            <input type="hidden" name="brandgrp_id"  value="<?php echo $r->brand_id; ?>" ?>
                                                                                                                                                                            <td>
                                                                                                                                                                                <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                                                                                                                                    delete
                                                                                                                                                                                </button>
                                                                                                                        
                                                                                                                                                                            </td>                                                    
                                                                                                                                                                        </form>
                                                                                                                                                                        </tr>
                                        <?php $i++; ?>    
                                    <?php endforeach; ?>
                                                                            
                                                                            
                                                                                                                        </tbody>
                                                                                                                    </table>
                                    <?php
                                else:

                                endif;
                                ?>
                                                                </div>-->
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>
            <script src="<?php echo base_url() . 'assets/js/modules/validate_brand.js' ?>"></script>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>   

            <!-- footer data links end here -->  
            <style>
                #brand_id
                {
                    width:100px;    
                }
            </style>

    </body>
</html>



