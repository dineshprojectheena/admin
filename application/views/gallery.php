<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <style>
        .col-sm-3
        {
            width:20% !important;    
        }
        .tab-content
        {
            overflow:hidden !important;    
        }
    </style>  
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                    <script>
                        $(document).ready(function() {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'product/gallery_result'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function(sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });
                    </script>


                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Gallery
                            </div>

                        </div>
                        <?php
                        $results = validation_errors();
                        if (!empty($results)) {
                            ?>
                            <div class="def_error">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="icon-remove"></i>
                                </button>

                                <strong>
                                    <i class="icon-remove"></i>
                                    Warning:
                                </strong>
                                Please check the form carefully for errors!                                
                                <br>
                            </div>
                            <?php
                        }
                        if (isset($sucess)) {
                            ?>    
                            <div class="def_error">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="icon-remove"></i>
                                </button>

                                <strong>
                                    <i class="icon-remove"></i>
                                    Thanks:
                                </strong>
                                <?php print_r($sucess); ?>                                
                                <br>
                            </div>
                            <?php
                        }
                        foreach ($load_products as $load_products_res)
                            
                            ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form"  action="<?php echo site_url("gallery_upload"); ?>" enctype="multipart/form-data" role="form" class="form-horizontal" method="POST">
                                            <div class="span6">                                                
                                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Body Color</h4>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>

                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">



                                                            <h4 class="header blue bolder smaller">Variant Gallery Details</h4>
                                                            <?php
//                                                            print_r($single_res_detail);
//                                                            echo $single_res_detail[0]['brand_id'];
//                                                            print_r($single_res_detail[0]['brand_id']);    
                                                            ?>
                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="brand_id" id="brand_id" onchange="getmodel();" style="width: 340px;">
                                                                        <option  value="">Select Brand</option>
                                                                        <?php
                                                                        foreach($brand as $brand_res){
                                                                        ?>
                                                                            <option 
                                                                            <?php
//                                                                            if($single_res_detail[0]['brand_id'] == $brand_res['brand_id']) {
//                                                                                echo 'selected';
//                                                                            }
                                                                            if ($single_res_detail[0]['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }else if($_POST['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?> value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('brand_id'); ?></span>

                                                                </div> 
                                                            </div> 
                                                            <?php
//                                                            print_r($single_res_detail);
                                                            ?>
                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                                                <div class="col-sm-9" style="float:left;" id="model_res" >
                                                                    <select name="model_id" id="model_id" style="width: 340px;">
                                                                        <option  value="">Select model</option>                                                                        
                                                                        <?php
                                                                        if (!empty($single_res_detail[0]['model_name'])) {
                                                                            ?>
                                                                            <option selected="selected" value="<?php echo $sing_res_data['model_id'] ?>"><?php echo $single_res_detail[0]['model_name']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                                                </div>


                                                            </div>
                                                            <?php
//                                                            print_r($single_res_detail);
                                                            ?>
                                                            <div class="form-group" id="" >
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                                                <div class="col-sm-9" style="float:left;" id="variant_results" >
                                                                    <select name="all_product_name" id="all_product_name">
                                                                        <option  value="">Select Variant</option>
                                                                        <?php
                                                                        if (!empty($single_res_detail[0]['pro_name'])) {
                                                                            ?>
                                                                            <option selected="selected" value="<?php echo $sing_res_data['variant_id'] ?>"><?php echo $single_res_detail[0]['pro_name']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>  
                                                                    <input type="hidden" name="expert_rating_ids" id="expert_rating_ids" value="<?php
                                                                    if ($get_res->expert_rating_id != '') {
                                                                        echo $get_res->expert_rating_id;
                                                                    } else if (isset($_POST['expert_rating_ids'])) {
                                                                        echo $_POST['expert_rating_ids'];
                                                                    }
                                                                    ?>" class="col-xs-10 col-sm-5">                                                                
                                                                    <br> <span class="error"><?php echo form_error('product_name'); ?></span>
                    <!--                                                <input type="text" name="product_names" id="product_names" value="<?php
                                                                    if (!isset($_POST['product_names'])) {
                                                                        if ($sing_res_data['pro_name'] != '') {
                                                                            echo $sing_res_data['pro_name'];
                                                                        }
                                                                    } else {
                                                                        echo $_POST['product_names'];
                                                                    }
                                                                    ?>" class="col-xs-10 col-sm-5"/>    -->
                                                                </div>

                                                            </div>
                                                            <input type="hidden" name="product_gallery_id"  value="<?php echo $load_products_res['product_gallery_id']; ?>" />
                                                            <input type="hidden" name="product_gallery_img"  value="<?php echo $load_products_res['image']; ?>" />
<!--                                                        <span class="error">
                                                            <?php echo form_error('product_name'); ?>
</span>-->
                                                            <!--                                                            <div class="form-group">
                                                                                                                            <label class="col-sm-1" for="product_name">Variant Name</label>
                                                                                                                            <div class="col-sm-3" style="float:left;">
                                                                                                                                <input type="text" name="product_name" id="product_names" value="<?php
                                                            if (!isset($_POST['product_name'])) {
                                                                if ($load_products_res['product_name'] != '') {
                                                                    echo $load_products_res['prduct_id'];
                                                                }
                                                            } else if (isset($_POST['product_name'])) {
                                                                echo $_POST['product_name'];
                                                            }
                                                            ?>" >
                                                                                                                                <input type="hidden" name="product_gallery_id"  value="<?php echo $load_products_res['product_gallery_id']; ?>" ?>
                                                                                                                                <input type="hidden" name="product_gallery_img"  value="<?php echo $load_products_res['image']; ?>" ?>
                                                                                                                                <span class="error">
                                                            <?php echo form_error('product_name'); ?>
                                                                                                                                </span>
                                                            
                                                                                                                            </div>
                                                            
                                                                                                                        </div>-->
                                                            <h4 class="header blue bolder smaller">Add <button type="button" class="btn btn-minier btn-yellow">Gallery</button></h4>
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <label class="col-sm-1" for="pro_name">Variant Image</label>
                                                                    <div class="col-sm-3">                                                                            
                                                                        <input type="file" id="pro_images" style="width: 340px;" name="pro_images">                                                                            
                                                                    </div>

                                                                </div></div>           
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <label class="col-sm-1" for="link">Link</label>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" id="link" name="link" style="width: 340px;" value="<?php
                                                                        if (!isset($_POST['link'])) {
                                                                            if ($load_products[0]['video'] != '') {
                                                                                echo $load_products[0]['video'];
                                                                            }
                                                                        } else if (isset($_POST['link'])) {
                                                                            echo $_POST['link'];
                                                                        }
                                                                        ?>">                                                                            
                                                                    </div>                                                                    

                                                                </div></div>
                                                            <br>
                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <label class="col-sm-1" for="page">Page</label>
                                                                    <div class="col-sm-3">

                                                                        <select name="page" id="page" style="width: 340px;" class="span3 valid">
                                                                            <option value="">----- Select Page -----</option>
                                                                            <?php
                                                                            $types = array('Overview', 'Variant', 'Exterior', 'Interior', 'Accessories', 'Booking', 'Video');
                                                                            for ($i = 0; $i <= 6; $i++) {
                                                                                ?>
                                                                                <option <?php
                                                                                if ($load_products_res['page'] == $types[$i]) {
                                                                                    echo 'selected';
                                                                                }
                                                                                ?> value="<?php echo $types[$i] ?>"><?php echo $types[$i] ?></option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                        </select>
                                                                    </div>   
                                                                    <span class="error" style="float: left;">
                                                                        <?php echo form_error('link'); ?>
                                                                        <?php echo form_error('pro_images'); ?>
                                                                    </span>                                                                 


                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($load_products)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Variant Name</th>
                                            <th>Page</th>  
<!--                                                    <th>Gallery Image</th>
                                            <th>Variant Video</th>                                                    -->

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>


                                <!--                                <div class="form-group">
                                <?php
                                if (isset($load_product)):
                                    ?>                                    
                                                                                                    <div class="table-header">
                                                                                                        Results for "Variant"
                                                                                                    </div>
                                                                                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>#</th>
                                                                                                                <th>Variant Name</th>
                                                                                                                <th>Gallery Image</th>
                                                                                                                <th>Variant Video</th>                                                    
                                                                                                                <th>Page</th>                                                    
                                                                                                                <th></th>
                                                                                                                <th></th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                            
                                                                                                        <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($load_product as $r) :
                                        ?>
                                                                                                                                            <tr> 
                                                                                                                                        <form action="<?php echo 'gallery_upload'; ?>" method="POST">
                                                                                                                                            <td>
                                        <?php echo $i; ?>                                                        
                                                                                                                                            </td>
                                                                                                                                            <td>
                                        <?php echo $r['pro_name']; ?>
                                                                                                                                            </td>
                                                                                        
                                                                                                                                            <td>
                                        <?php
                                        if ($r['image'] != '' && $r['image'] != '0') {
                                            ?>
                                                                                                                                                                                <img src="<?php echo base_url() . 'uploads/gallery/' . $r['image']; ?>" style="width:100px;"/>
                                            <?php
//                                                            echo $r['image']; 
                                        }
                                        ?>
                                                                                                                                            </td>
                                                                                                                                            <td>  
                                        <?php
                                        if ($r['video'] != '') {
                                            $video = $r['video'];
                                            $exp_v = explode('?v=', $video);
                                            ?>
                                                                                                                                                                                <iframe  src="<?php echo 'https://www.youtube.com/embed/' . $exp_v[1]; ?>" width="230" height="245" frameborder="0" allowfullscreen></iframe>        
                                            <?php
                                            ?>
                                                                                                                                                                            <iframe width="300" height="300" src="//www.youtube.com/embed/wrEp4QpE1wI?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                                            <?php // echo $r['video'];  ?>
                                                                                                                                                                                            <iframe width="120" height="120"
                                                                                                                                                                                    src="<?php echo $r['video']; ?>">
                                                                                                                                                                                    </iframe>	
                                                                                                                                                                                iframe width="100" height="100"
                                                                                                                                                                                        src="<?php echo trim($r['video']); ?>">
                                                                                                                                                                                </iframe
                                        <?php }
                                        ?>
                                                                                                                                            </td>
                                                                                                                                            <td>
                                        <?php echo $r['page']; ?>
                                                                                                                                            </td>
                                                                                        
                                                                                                                                            <td class="align_tble">
                                                                                                                                                <input type="hidden" name="product_gallery_id"  value="<?php echo $r['product_gallery_id']; ?>" ?>    
                                                                                                                                                <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                                                                                                                    Edit
                                                                                                                                                </button>                                                        
                                                                                                                                            </td>
                                                                                                                                            <td>
                                                                                                                                                <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                                                                                                    delete
                                                                                                                                                </button>
                                                                                                                                            </td>                                                    
                                                                                                                                        </form>
                                                                                                                                        </tr>
                                        <?php $i++; ?>    
                                    <?php endforeach; ?>
                                                            
                                                                                                        </tbody>
                                                                                                    </table>
                                    <?php
                                else:
//echo '<p>No Task Exist</p>';
                                endif;
                                ?>
                                                                </div>-->
                            </div>     

                        </div>
                        <?php
                        if (!empty($load_products_res)) {
                            $variant_id = $load_products_res['variant_id'];
                            $pro_name = $load_products_res['pro_name'];
                            $prepopulate = $data = '{id:"' . $variant_id . '",name:"' . $pro_name . '"}';
                        }
//                        echo $prepopulate;
                        ?>


                        <!-- main content data end here -->
                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->
                <script>

                    $(document).ready(function() {
                        var prepolulate_autocomplete;
                        <?php if (isset($prepopulate)) { ?>
                                                    prepolulate_autocomplete = [<?php echo $prepopulate ?>];
                        <?php } else { ?>
                                                    prepolulate_autocomplete = [];
                        <?php } ?>

                        $("#product_names").tokenInput("<?php echo base_url() . 'price/search_product'; ?>", {
                            theme: "facebook",
                            searchingText: 'Searching..',
                            tokenLimit: 1,
                            minChars: 3,
                            prePopulate: prepolulate_autocomplete
                        });
                    });

                    function s1() {
                        $('#img').css('display', 'block');
                        $('#link').css('display', 'none');
                    }
                    function s2() {
                        $('#img').css('display', 'none');
                        $('#link').css('display', 'block');
                    }
                    document.getElementById('brand_id').value = '';
                </script>
                <style>
                    #model_id
                    {
                        width: 340px !important;    
                    }
                    #all_product_name
                    {
                        width: 340px !important;    
                    }
                </style>


                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <!-- footer js file links end here -->

            <script src="<?php echo base_url() . 'assets/js/modules/validate_product_gallery.js' ?>" ></script>

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>







