<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                <script>
                    $(document).ready(function () {
                        $('.table').dataTable({
                            "bProcessing": true,
                            "bServerSide": true,
                            "sAjaxSource": '<?php echo base_url() . 'stitcher/slider_result_data'; ?>',
                            "aaSorting": [[4, "desc"]],
                            "fnServerData": function (sSource, aoData, fnCallback) {
                                $.ajax({
                                    "dataType": 'json',
                                    "type": "POST",
                                    "url": sSource,
                                    "data": aoData,
                                    "success": fnCallback
                                });
                            }
                        });
                    });
                </script>

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Slider
                            </div>
                        </div>
                        <?php
                        echo $err;
                        ?>
                        <?php if (isset($error_msg)): ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <?php echo $error_msg ?>
                                <br>
                            </div>
                        <?php endif; ?>

                        <?php if (isset($success_msg)): ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <?php echo $success_msg ?>
                                <br>
                            </div>
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                foreach ($results_up as $get_res)
//                                print_r($results_up);    
                                    
                                    ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form"  action="<?php echo site_url("stitcher/slider_upload_data"); ?>" class="form-horizontal" class="form-horizontal" method="POST" enctype="multipart/form-data" role="form">
                                            <div class="span6">
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>

                                                    <div class="tab-content">



                                                        <div id="home3" class="tab-pane active">
                                                            <h4 class="header blue bolder smaller">Slider Details</h4>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="app_type">Type</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <select name="app_type" onchange="change();" id="app_type" style="width:335px;">
                                                                            <option value="">Select App Type</option>    
                                                                            <option value="web">Web</option>    
                                                                            <option value="app">app</option>   

                                                                        </select>
                                                                        <span class="error"><?php echo form_error('app_type'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="profiles">Slider Image</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="file" accept="image/*"  name="profiles" id="profiles" class="col-xs-10 col-sm-5">                                                                
                                                                        <span class="error"><?php echo form_error('profiles'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="city">City</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <select name="city" id="city" style="width:335px;">
                                                                            <option value="">Select City</option>    
                                                                            <option value="All">All City</option>    
                                                                            <?php
                                                                            foreach ($city as $citys) {
                                                                                ?>
                                                                                <option 

                                                                                    value="<?php echo $citys->name; ?>"><?php echo $citys->name; ?></option>    
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                        </select>
                                                                        <span class="error"><?php echo form_error('city'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>



                                                            <div class="form-group" id="web1" style="display:none;">
                                                                <label class="col-sm-3 control-label no-padding-right" for="link">Link</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="link" id="link" class="col-md-3">                                                                
                                                                        <span class="error"><?php echo form_error('link'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="sort">Sort Order</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="sort" id="sort" class="col-md-3">                                                                
                                                                        <span class="error"><?php echo form_error('sort'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group" id="app1" style="display:none;">
                                                                <label class="col-sm-3 control-label no-padding-right" for="link_type">Link Type</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <select name="link_type" onchange="link_type_change();" id="link_type" style="width:335px;">
                                                                            <option value="">Select Link Type</option>    
                                                                            <option value="product">Product</option>    
                                                                            <option value="hot_deal">Hot Deal (with/without)</option>    
                                                                            <option value="search">Search (with/without)</option>   
                                                                            <option value="href">Href</option>                                                                               
                                                                        </select>
                                                                        <span class="error"><?php echo form_error('link_type'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="form-group" id="product_wise" style="display:none;overflow:hidden;">
                                                                <h4 class="header blue bolder smaller" style="text-align: left;  width: 30%;  margin-left: 200px;"> Select Product Here</h4>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                    <div class="col-sm-9" >
                                                                        <select name="brand_id" id="brand_id" onchange="getmodel();" style="width:255px;">
                                                                            <option  value="">Select Brand</option>
                                                                            <?php
                                                                            foreach ($brand as $brand_res) {
                                                                                ?>
                                                                                <option value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>  
                                                                        <br> <span class="error"><?php echo form_error('brand_id'); ?></span>

                                                                    </div> 
                                                                </div> 
                                                                <div class="space-4"></div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                                                    <div class="col-sm-9" style="float:left;" id="model_res" >
                                                                        <select name="model_id" id="model_id" style="width: 255px;">
                                                                            <option  value="">Select model</option>                                                                                                                                                    
                                                                        </select>  
                                                                        <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                                                    </div>


                                                                </div>
                                                                <div class="space-4"></div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                                                    <div class="col-sm-9" style="float:left;" id="variant_results" >
                                                                        <select name="all_product_name"  id="all_product_name" style="width: 255px;">
                                                                            <option  value="">Select Variant</option>                                                                            
                                                                        </select>  
                                                                        <span class="error"><?php echo form_error('product_name'); ?></span>
                                                                    </div>
                                                                </div> 
                                                                <div class="space-4"></div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                            <div class="form-group" id="hot_deal_wise" style="display:none;overflow:hidden;">
                                                                <h4 class="header blue bolder smaller" style="text-align: left;  width: 30%;  margin-left: 200px;"> Select Hot Deals Here</h4>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                    <div class="col-sm-9" >
                                                                        <select name="hot_deal_brand_id" id="hot_deal_brand_id" style="width:255px;">
                                                                            <option  value="">Select Brand</option>
                                                                            <?php
                                                                            foreach ($brand as $brand_res) {
                                                                                ?>
                                                                                <option value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                        </select>  
                                                                        <br> <span class="error"><?php echo form_error('brand_id'); ?></span>

                                                                    </div> 
                                                                </div>
                                                                <div class="form-group">
                                                                    <label  class="col-sm-3 control-label no-padding-right" for="body_type">Body Type</label>
                                                                    <div class="col-sm-9" >
                                                                        <select  id="body_type" name="body_type" class="form-control" style="width:255px;">
                                                                            <option value="">Select Body Type</option>
                                                                            <?php
                                                                            foreach ($all_car_type as $all_car_type_res) {
                                                                                ?>
                                                                                <option value="<?php echo $all_car_type_res['pro_type_id']; ?>" ><?php echo $all_car_type_res['pro_type_name']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>  
                                                                        <br> <span class="error"><?php echo form_error('body_type'); ?></span>

                                                                    </div> 
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group" id="href_wise" style="display:none;overflow:hidden;">
                                                                <h4 class="header blue bolder smaller" style="text-align: left;  width: 30%;  margin-left: 200px;"> Select HREF Here</h4>
                                                                <div class="form-group" >
                                                                    <label class="col-sm-3 control-label no-padding-right" for="app_data">Data</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="clearfix">
                                                                            <input type="text" name="app_data" id="app_data">                                                                
                                                                            <span class="error"><?php echo form_error('app_data'); ?></span>
                                                                        </div>

                                                                    </div>
                                                                </div>                                                     

                                                            </div>
                                                            <div class="space-4"></div>


                                                            <div class="space-4"></div>







                                                        </div>  



                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($results_up)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Upload
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Upload
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>City</th>
                                            <th>Slider Image</th>
                                            <th>Link</th>                                                    
                                            <th>Type</th>                                                    
                                            <th>Sort</th>                                                    
                                            <th>Added Date</th>                                                    
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th colspan="7" >
                                                <button class="btn btn-xs btn-danger" style="float:right" onclick="delete_all_slider();" name="deleteall" type="submit">
                                                    Delete All
                                                </button>

                                            </th>

                                        </tr>
                                    </tfoot>
                                </table>
                            </div>     

                        </div>
                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <script>
                function delete_all_slider()
                {

                    var val = [];
                    $(':checkbox:checked').each(function (i) {
                        val[i] = $(this).val();
//                        alert(val[i]);
                    });
                    window.location.href="<?php echo base_url('stitcher/slider_delete_data/')?>/?id="+val;
//                    alert(val);

                }
                function link_type_change()
                {
                    $('#product_wise').css("display", "none");
                    $('#hot_deal_wise').css("display", "none");
                    $('#href_wise').css("display", "none");
                    var link_type = $('#link_type').val();
//                alert(link_type);
                    if (link_type == 'product')
                    {
                        $('#product_wise').css("display", "block");
                    }
                    if (link_type == 'hot_deal')
                    {
                        $('#hot_deal_wise').css("display", "block");
                    }
                    if (link_type == 'href')
                    {
                        $('#href_wise').css("display", "block");
                    }
                }

                function change()
                {
                    var blank = '';
                    $('#web1').val(blank);
                    $('#app1').val(blank);
                    $('#app2').val(blank);
                    var app_type = $('#app_type').val();
                    if (app_type == 'web')
                    {
                        $('#web1').css("display", "block");
                        $('#app1').css("display", "none");
                        $('#app2').css("display", "none");
                    }
                    if (app_type == 'app')
                    {
                        $('#web1').css("display", "none");
                        $('#app1').css("display", "block");
//                                                            $('#app2').css("display","block");    
                    }
                }


            </script>


            <script src="<?php echo base_url() . 'assets/js/modules/validate_slider.js' ?>" ></script>
            <?php $this->load->view('templates/js') ?>
            <?php $this->load->view('templates/footer') ?>    

    </body>
</html>



