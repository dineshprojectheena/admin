<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <style>
        .col-sm-3
        {
            width:20% !important;    
        }
        .tab-content
        {
            overflow:hidden !important;    
        }
    </style>  
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->
        <div class="main-container" id="main-container">
            <div class="main-container-inner">
                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                <div class="main-content">
                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <?php
                            if(!empty($this->session->flashdata('Total_Result')) || !empty($this->session->flashdata('Total_Success')))
                            {
                            ?>
                            <div class="alert alert-block alert-success" style="height:200px;">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="icon-remove"></i>
                                </button>
                                <?php
                                if($this->session->flashdata('Total_Result')!='')
                                {    
                                ?>
                                <strong class="green" style="font-size: 11px;">
                                Total Results:-    
                                <?php
                                echo $this->session->flashdata('Total_Result');
                                ?>
                                </strong>
                                <br>
                                <?php
                                }
                                ?>
                             
                                <?php
                                if($this->session->flashdata('Total_Success')!='')
                                {    
                                ?>
                                <strong class="green" style="font-size: 11px;">
                                Total Success:-    
                                <?php
                                echo $this->session->flashdata('Total_Success');
                                ?>
                                </strong>
                                <br>
                                <?php
                                }
                                ?>
                             
                                <?php
                                if($this->session->flashdata('Total_Fail')!='')
                                {    
                                ?>
                                <strong class="green" style="font-size: 11px;">
                                Total Results:-    
                                <?php
                                echo $this->session->flashdata('Total_Result');
                                ?>
                                </strong>
                                <br>
                                <?php
                                }
                                ?>
                             
                                <?php
                                if($this->session->flashdata('Total_Result')!='')
                                {    
                                ?>
                                <strong class="green" style="font-size: 11px;">
                                Total Results:-    
                                <?php
                                echo $this->session->flashdata('Total_Result');
                                ?>
                                </strong>
                                <br>
                                <?php
                                }
                                ?>
                             
                                
                            </div>
                            <?php
                            }
                            ?>
                            <div class="table-header">
                                Bulk Accessory
                            </div>
                        </div>
                        <?php
                        foreach ($load_products as $load_products_res)
                        ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form" action="<?php echo site_url("bulkaccessoryupload"); ?>" enctype="multipart/form-data" role="form" class="form-horizontal" method="POST">
                                            <div class="span6">                                                
                                                      <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>

                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">



                                                            <h4 class="header blue bolder smaller">Upload Bulk Accessory CSV Here...</h4>
                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <label class="col-md-3" for="pro_name">Bulk Accessory Upload</label>
                                                                    <div class="col-sm-9">                                                                            
                                                                        <input type="file"  accept=".csv" id="pro_images" style="width: 340px;" name="pro_images">                                                                            
                                                                    </div>

                                                             </div>
                                                            
                                                             <br><br>
<!--                                                                <div class="col-md-12">
                                                                    <label class="col-md-3" for="pro_name">Delete Existing Records</label>
                                                                    <div class="col-sm-9">                                                                            
                                                                        <select id="del_record" name="del_record">
                                                                            <option value="">- - - Select - - - </option>    
                                                                            <option value="1">Yes</option>    
                                                                            <option value="2">No</option>    
                                                                        </select>    
                                                                    </div>

                                                                </div>    -->
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($load_products)){
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Upload
                                                        </button>
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                        
                                                        
<!--                                                        <button type="submit" name="download" class="btn btn-info">                                                
                                                            Download Coupon CSV
                                                        </button>-->

                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="form-group">
                                    <?php
                                    if (isset($load_product)):
                                        ?>                                    
                                        <div class="table-header">
                                            Results for "Variant"
                                        </div>
                                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Variant Name</th>
                                                    <th>Gallery Image</th>
                                                    <th>Variant Video</th>                                                    
                                                    <th>Page</th>                                                    
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($load_product as $r) :
                                                    ?>
                                                    <tr> 
                                                <form action="<?php echo 'gallery_upload'; ?>" method="POST">
                                                    <td>
                                                        <?php echo $i; ?>                                                        
                                                    </td>
                                                    <td>
                                                        <?php echo $r['pro_name']; ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        if ($r['image'] != '' && $r['image'] != '0') {
                                                            ?>
                                                            <img src="<?php echo base_url() . 'uploads/gallery/' . $r['image']; ?>" style="width:100px;"/>
                                                            <?php
//                                                            echo $r['image']; 
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>  
                                                        <?php
                                                        if ($r['video'] != '') {
                                                            $video = $r['video'];
                                                            $exp_v = explode('?v=', $video);
                                                            ?>
                                                            <iframe  src="<?php echo 'https://www.youtube.com/embed/' . $exp_v[1]; ?>" width="230" height="245" frameborder="0" allowfullscreen></iframe>        
                                                            <?php
                                                            ?>
                                                        <!--<iframe width="300" height="300" src="//www.youtube.com/embed/wrEp4QpE1wI?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>-->
                                                            <?php // echo $r['video'];  ?>
            <!--							   	<iframe width="120" height="120"
                                                                src="<?php echo $r['video']; ?>">
                                                                </iframe>	-->
                                                            <!--iframe width="100" height="100"
                                                                    src="<?php echo trim($r['video']); ?>">
                                                            </iframe-->
                                                        <?php }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $r['page']; ?>
                                                    </td>

                                                    <td class="align_tble">
                                                        <input type="hidden" name="product_gallery_id"  value="<?php echo $r['product_gallery_id']; ?>" ?>    
                                                        <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                            Edit
                                                        </button>                                                        
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                            delete
                                                        </button>
                                                    </td>                                                    
                                                </form>
                                                </tr>
                                                <?php $i++; ?>    
                                            <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                        <?php
                                    else:
//echo '<p>No Task Exist</p>';
                                    endif;
                                    ?>
                                </div>
                            </div>     

                        </div>
                        <?php
                        if (!empty($load_products_res)) {
                            $variant_id = $load_products_res['variant_id'];
                            $pro_name = $load_products_res['pro_name'];
                            $prepopulate = $data = '{id:"' . $variant_id . '",name:"' . $pro_name . '"}';
                        }
//                        echo $prepopulate;
                        ?>


                        <!-- main content data end here -->
                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->
                <script>

                    $(document).ready(function() {
                        var prepolulate_autocomplete;
<?php if (isset($prepopulate)) { ?>
                            prepolulate_autocomplete = [<?php echo $prepopulate ?>];
<?php } else { ?>
                            prepolulate_autocomplete = [];
<?php } ?>

                        $("#product_names").tokenInput("<?php echo base_url() . 'price/search_product'; ?>", {
                            theme: "facebook",
                            searchingText: 'Searching..',
                            tokenLimit: 1,
                            minChars: 3,
                            prePopulate: prepolulate_autocomplete
                        });
                    });

                    function s1() {
                        $('#img').css('display', 'block');
                        $('#link').css('display', 'none');
                    }
                    function s2() {
                        $('#img').css('display', 'none');
                        $('#link').css('display', 'block');
                    }
                    document.getElementById('brand_id').value = '';
                </script>
                <style>
                    #model_id
                    {
                        width: 340px !important;    
                    }
                    #all_product_name
                    {
                        width: 340px !important;    
                    }
                </style>


                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            
            
            <script src="<?php echo base_url() . 'assets/js/modules/validate_bulk_product.js' ?>"></script>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>







