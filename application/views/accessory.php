<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->
        <script>
            function listbox_move(listID, direction) {
                var listbox = document.getElementById(listID);
                var selIndex = listbox.selectedIndex;
                if (-1 == selIndex) {
                    alert("Please select an option to move.");
                    return;
                }
                var increment = -1;
                if (direction == 'up')
                    increment = -1;
                else
                    increment = 1;
                if ((selIndex + increment) < 0 || (selIndex + increment) > (listbox.options.length - 1)) {
                    return;
                }
                var selValue = listbox.options[selIndex].value;
                var selText = listbox.options[selIndex].text;
                listbox.options[selIndex].value = listbox.options[selIndex + increment].value
                listbox.options[selIndex].text = listbox.options[selIndex + increment].text
                listbox.options[selIndex + increment].value = selValue;
                listbox.options[selIndex + increment].text = selText;
                listbox.selectedIndex = selIndex + increment;
            }
            function listbox_moveacross(sourceID, destID) {
                var src = document.getElementById(sourceID);
                var dest = document.getElementById(destID);
                for (var count = 0; count < src.options.length; count++) {
                    if (src.options[count].selected == true) {
                        var option = src.options[count];
                        var newOption = document.createElement("option");
                        newOption.value = option.value;
                        newOption.text = option.text;
                        newOption.selected = true;
                        try {
                            dest.add(newOption, null);
                            src.remove(count, null);
                        } catch (error) {
                            dest.add(newOption);
                            src.remove(count);
                        }
                        count--;
                    }
                }
            }
            function listbox_selectall(listID, isSelect) {
                var listbox = document.getElementById(listID);
                for (var count = 0; count < listbox.options.length; count++) {
                    listbox.options[count].selected = isSelect;
                }
            }
        </script>
        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here --><div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->

                    <script>
                        $(document).ready(function () {
//                            alert('sdsdsdsd');
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'stitcher/accessory_master_result'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function (sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });
                    </script>
                    <!--Landing page master... 
                    Id, name, email, City, preferred brand model variant, bg color, bg image, 
                    submit button one, submit button two and their tex
                    -->

                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Accessory Master Page
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <form  id="validation-form" action="<?php echo site_url("upload_accessory_master"); ?>" method="POST" enctype="multipart/form-data" role="form" class="form-horizontal" style="overflow:hidden;">
                                            <div class="span6">
                                                <?php
//                                                print_r($query);
                                                $res = $this->session->flashdata('res');
                                                
                                                $acc_variant_data2=$this->session->flashdata('acc_variant_data');
                                                
//                                                print_r($acc_variant_data2);
                                                
                                                $message = $this->session->flashdata('message');
                                                if (($message) == 'updated') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Updated:
                                                        </strong>
                                                        Data Updated Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                } else if (($message) == 'validate') {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please fill all mendatory details!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                } else if (($message) == 'error') {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                } else if (($message) == 'exist') {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Code Exist please try new code details!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                } else if (($message) == 'success') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>
                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Successful:
                                                        </strong>
                                                        Data Uploaded Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                } else if (($message) == 'delete') {
                                                    ?>
                                                    <div class="alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>
                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Successful:
                                                        </strong>
                                                        Data Deleted Successfully!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
//                                                print_r($data_set);
                                                ?>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="brand_id" id="acc_brand_id" onchange="getaccmodel();" style="width: 340px;">
                                                                        <option  value="">Select Brand</option>
                                                                        <option <?php
                                                                        if ($res['brand_id'] == 0) {
                                                                            echo 'selected';
                                                                        } else if ($acc_variant_data[0]['brand_id'] == 0) {
                                                                            echo 'selected';
                                                                        }
                                                                        ?>  value="0">All Brands</option>
                                                                            <?php foreach ($brand as $brand_res) { ?>
                                                                            <option 
                                                                            <?php
                                                                            if ($acc_variant_data[0]['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            } else if ($res['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?> value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                    </select>  
                                                                    <br> 
                                                                    <span class="error"><?php echo form_error('brand_id'); ?></span>

                                                                </div> 
                                                            </div> 
                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                                                <div class="col-sm-9" style="float:left;" id="model_res" >
                                                                    <select name="model_id" id="model_id" style="width: 340px;" onchange="get_acc_variant();">
                                                                        <option  value="">Select model</option>                                                                        
                                                                        <?php
                                                                        if (!empty($single_res_detail[0]['model_name'])) {
                                                                            ?>
                                                                            <option selected="selected" value="<?php echo $sing_res_data['model_id'] ?>"><?php echo $single_res_detail[0]['model_name']; ?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>  


                                                                    <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                                                </div>


                                                            </div>
                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                                                <div class="col-sm-9" style="float:left;"  >
                                                                    <table>
                                                                        <tr valign="top">
                                                                            <td>
                                                                                <div id="variant_results1">
                                                                                    <select name="all_product_name[]" id="all_product_name" style="width:170px;" multiple>
                                                                                        <?php
                                                                                        /*
                                                                                          foreach($variant as $variant_res)
                                                                                          {
                                                                                          ?>
                                                                                          <option value="<?php echo $variant_res['variant_id'];?>"><?php echo $variant_res['pro_name_comp'];?></option>
                                                                                          <?php
                                                                                          }
                                                                                         * 
                                                                                         */
                                                                                        ?>
                                                                                    </select>

                                                                                </div>
                                                                            </td>
                                                                            <td valign="center">
                                                                                <a href="#" onclick="listbox_moveacross('s', 'd')">&gt;</a>
                                                                                <br/>
                                                                                <a href="#" onclick="listbox_moveacross('d', 's')">&lt;</a>
                                                                            </td>
                                                                            <td>
                                                                                <SELECT id="d" name="all_product_name[]" style="width:170px;" multiple>
                                                                                    <?php
                                                                                    foreach($acc_variant_data as $acc_variant_data_res) {
                                                                                        ?>
                                                                                        <option selected="selected"  value="<?php echo $acc_variant_data_res['variant_id']; ?>"><?php echo $acc_variant_data_res['pro_name_comp']; ?></option>    
                                                                                        <?php
                                                                                    }
                                                                                    foreach($acc_variant_data2 as $acc_variant_data_res2) {
                                                                                        ?>
                                                                                        <option selected="selected"  value="<?php echo $acc_variant_data_res2['variant_id']; ?>"><?php echo $acc_variant_data_res2['pro_name_comp']; ?></option>    
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </SELECT>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br>

                                                                    <span class="error"><?php echo form_error('product_name'); ?></span>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="category">Category</label>
                                                                <div class="col-sm-9" >
                                                                    <input type="hidden" name="selected_variant" value="<?php
                                                                    if (!empty($res['selected_variant'])) {
                                                                        echo $res['selected_variant'];
                                                                    } else {
                                                                        foreach ($acc_variant_data as $acc_variant_data_res) {
                                                                            echo $acc_variant_data_res['variant_id'] . '-';
                                                                        }
                                                                    }
                                                                    ?>" id="selected_variant" class="col-md-3">
                                                                    <input type="hidden" name="acc_id" value="<?php
                                                                    if (!empty($res['acc_id'])) {
                                                                        echo $res['acc_id'];
                                                                    } else {
                                                                        echo $acc_data['car_accessory_id'];
                                                                    }
                                                                    ?>" id="acc_id" class="col-md-3">
                                                                    <select name="category" id="category" style="width: 340px;">
                                                                        <option  value="">Select Category</option>
                                                                        <?php
                                                                        foreach ($accessory_category as $accessory_category_res) {
                                                                            ?>
                                                                            <option 
                                                                            <?php
                                                                            if ($acc_data['category'] == $accessory_category_res['accessory_category_id']) {
                                                                                echo 'selected';
                                                                            } else if ($res['category'] == $accessory_category_res['accessory_category_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?> value="<?php echo $accessory_category_res['accessory_category_id']; ?>" ><?php echo $accessory_category_res['accessory_category_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('brand_id'); ?></span>

                                                                </div> 
                                                            </div>

                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="accessory_sub_cat">Sub Category</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="accessory_sub_cat" id="accessory_sub_cat" style="width: 340px;">
                                                                        <option  value="">Select Sub Category</option>
                                                                        <?php
                                                                        foreach ($accessory_sub_category as $accessory_sub_category_res) {
                                                                            ?>
                                                                            <option 
                                                                            <?php
                                                                            if ($acc_data['accessory_sub_cat'] == $accessory_sub_category_res['accessory_sub_category_id']) {
                                                                                echo 'selected';
                                                                            } else if ($res['accessory_sub_cat'] == $accessory_sub_category_res['accessory_sub_category_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?> value="<?php echo $accessory_sub_category_res['accessory_sub_category_id']; ?>" ><?php echo $accessory_sub_category_res['accessory_sub_category_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('accessory_sub_cat'); ?></span>

                                                                </div> 
                                                            </div>

                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="cat_type">Category Type</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="cat_type" id="cat_type" style="width: 340px;">
                                                                        <option  value="">Select Category Type</option>
                                                                        <?php
                                                                        foreach ($accessory_type as $accessory_type_res) {
                                                                            ?>
                                                                            <option 
                                                                            <?php
                                                                            if ($acc_data['accessory_type'] == $accessory_type_res['accessory_type_id']) {
                                                                                echo 'selected';
                                                                            } else if ($res['cat_type'] == $accessory_type_res['accessory_type_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?> value="<?php echo $accessory_type_res['accessory_type_id']; ?>" ><?php echo $accessory_type_res['accessory_type']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('cat_type'); ?></span>

                                                                </div> 
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="image">Image</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="file" name="image" value="<?php echo $single_res_detail[0]['image']; ?>" id="image" class="col-md-3" style="width: 340px;">                                                                         
                                                                        <input type="hidden" name="image_name" value="<?php echo $acc_data['image']; ?>" id="image" class="col-md-3" style="width: 340px;">                                                                         
                                                                        <span class="error"><?php echo form_error('image'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="name">Name</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="name" value="<?php
                                                                        if (!empty($res['name'])) {
                                                                            echo $res['name'];
                                                                        } else {
                                                                            echo $acc_data['name'];
                                                                        }
                                                                        ?>" id="name" class="col-md-3" style="width: 340px;">                                                                         
                                                                        <span class="error"><?php echo form_error('name'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="accessory_part_no">Part Number</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="accessory_part_no" value="<?php
                                                                        if(!empty($res['accessory_part_no'])) {
                                                                            echo $res['accessory_part_no'];
                                                                        } else {
                                                                            echo $acc_data['accessory_part_no'];
                                                                        }
                                                                        ?>" id="accessory_part_no" class="col-md-3" style="width: 340px;">                                                                         
                                                                        <span class="error"><?php echo form_error('accessory_part_no'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="price">Price</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="price" value="<?php
                                                                        if (!empty($res['price'])) {
                                                                            echo $res['price'];
                                                                        } else {
                                                                            echo $acc_data['price'];
                                                                        }
                                                                        ?>" id="price" class="col-md-3" style="width: 340px;">                                                                         
                                                                        <span class="error"><?php echo form_error('price'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="discount_price">Discount</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="discount_price" value="<?php
                                                                        if (!empty($res['discount_price'])) {
                                                                            echo $res['discount_price'];
                                                                        } else {
                                                                            echo $acc_data['discount_price'];
                                                                        }
                                                                        ?>" id="discount_price" class="col-md-3" style="width: 340px;">                                                                         
                                                                        <span class="error"><?php echo form_error('discount_price'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="description">Description</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <textarea type="text" name="description" id="description" class="col-md-3" style="width: 340px;"><?php
                                                                            if (!empty($res['description'])) {
                                                                                echo $res['description'];
                                                                            } else {
                                                                                echo $acc_data['description'];
                                                                            }
                                                                            ?><?php echo $acc_data['description']; ?></textarea>                                                                         
                                                                        <span class="error"><?php echo form_error('price'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right"  for="stock">Stock</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="stock" id="stock" value="<?php
                                                                        if (!empty($res['stock'])) {
                                                                            echo $res['stock'];
                                                                        } else {
                                                                            echo $acc_data['stock'];
                                                                        }
                                                                        ?>" class="col-md-3" style="width: 340px;">
                                                                        <span class="error"><?php echo form_error('stock'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="status">Status</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="status" id="status" style="width: 340px;">
                                                                        <option  value="">Select status</option>

                                                                        <option <?php
                                                                        if ($acc_data['status'] == '1') {
                                                                            echo 'selected';
                                                                        } else if ($res['status'] == '1') {
                                                                            echo 'selected';
                                                                        }
                                                                        ?> value="1"> Enable </option>    

                                                                        <option <?php
                                                                        if ($acc_data['status'] == '2') {
                                                                            echo 'selected';
                                                                        } else if ($res['status'] == '2') {
                                                                            echo 'selected';
                                                                        }
                                                                        ?> value="2"> Disable </option>                                                                                  
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('status'); ?></span>
                                                                </div> 
                                                            </div>
                                                            
                                                            <div class="space-4"></div>
                                                            <hr>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="percent">Hot Deal Heading</label>
                                                                <div class="col-sm-9" >
                                                                    <input type="text" name="percent" id="percent" value="<?php
                                                                        if (!empty($acc_hot_deals['percent'])) {
                                                                            echo $acc_hot_deals['percent'];
                                                                        } else {
                                                                            echo $_POST['percent'];
                                                                        }
                                                                        ?>" class="col-md-3" style="width: 340px;">
                                                                    <br> <span class="error"><?php echo form_error('percent'); ?></span>
                                                                </div> 
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="valid_until">Valid Until</label>
                                                                <div class="col-sm-9" >
                                                                    <input type="text" name="valid_until" id="valid_until" value="<?php
                                                                        if (!empty($acc_hot_deals['valid_until'])) {
                                                                            echo $acc_hot_deals['valid_until'];
                                                                        } else {
                                                                            echo $_POST['valid_until'];
                                                                        }
                                                                        ?>" class="col-md-3" style="width: 340px;">
                                                                    <br> <span class="error"><?php echo form_error('valid_until'); ?></span>
                                                                </div> 
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="product_type">Accessory Type</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="product_type" id="product_type" style="width: 340px;">
                                                                        <option  value="">Accessory Type</option>

                                                                        <option <?php
                                                                        if ($acc_data['type'] == '1') {
                                                                            echo 'selected';
                                                                        } else if ($res['type'] == '1') {
                                                                            echo 'selected';
                                                                        }
                                                                        ?> value="1"> Aftermarket </option>    

                                                                        <option <?php
                                                                        if ($acc_data['type'] == '2') {
                                                                            echo 'selected';
                                                                        } else if ($res['type'] == '2') {
                                                                            echo 'selected';
                                                                        }
                                                                        ?> value="2"> Genuine </option>                                                                                  
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('product_type'); ?></span>
                                                                </div> 
                                                            </div>
                                                            

                                                        </div>                                                                                                 
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <div class="clearfix form-actions">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php
                                                    if (!empty($acc_data)) {
                                                        ?>
                                                        <button type="submit" name="Update" class="btn btn-info">                                                
                                                            Update
                                                        </button>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>



                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
<!--                                            <th>Product Name</th>
                                            <th>Category</th>-->
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Description</th>
                                            <!--<th>Stock</th>-->

                                            <th>Added Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>    

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>
            <!--<script src="<?php echo base_url() . 'assets/js/modules/validate_brand.js' ?>"></script>-->
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>   
            <input type="hidden" id='post_model' value="<?php echo $res['model_id']; ?>" />
            <input type="hidden" id='post_variant' value="<?php 
            $res_datas=''; 
            foreach($res['all_product_name'] as $res_data) 
                { 
                $res_datas= $res_data.'-'.$res_datas; 
                } 
                echo $res_datas;
                ?>" />
            <?php
            print_r($res);
            if (!empty($res)) {
                ?>
                <script>
                    getaccmodel();
                </script>
                <?php
            }
            ?>
            <script>
<?php
if (!empty($acc_variant_data)) {
    ?>
                    getaccmodel();
    <?php
}
?>
            </script> 
            <!-- footer data links end here -->  
            <style>
                #brand_id
                {
                    width:100px;    
                }
            </style>

    </body>
</html>



