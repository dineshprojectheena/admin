<style>
    ul
    {
        display: block;list-style-type: disc;margin-top: 1em;margin-bottom: 1 em;margin-left: 0;margin-right: 0;padding-left: 40px;    
    }
</style>
<?php

function no_to_words($no) {
    $words = array('0' => '', '1' => 'one', '2' => 'two', '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six', '7' => 'seven', '8' => 'eight', '9' => 'nine', '10' => 'ten', '11' => 'eleven', '12' => 'twelve', '13' => 'thirteen', '14' => 'fouteen', '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen', '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty', '30' => 'thirty', '40' => 'fourty', '50' => 'fifty', '60' => 'sixty', '70' => 'seventy', '80' => 'eighty', '90' => 'ninty', '100' => 'hundred &', '1000' => 'thousand', '100000' => 'lakh', '10000000' => 'crore');
    if ($no == 0)
        return ' ';
    else {
        $novalue = '';
        $highno = $no;
        $remainno = 0;
        $value = 100;
        $value1 = 1000;
        while ($no >= 100) {
            if (($value <= $no) && ($no < $value1)) {
                $novalue = $words["$value"];
                $highno = (int) ($no / $value);
                $remainno = $no % $value;
                break;
            }
            $value = $value1;
            $value1 = $value * 100;
        }
        if (array_key_exists("$highno", $words))
            return $words["$highno"] . "  " . $novalue . " " . no_to_words($remainno);
        else {
            $unit = $highno % 10;
            $ten = (int) ($highno / 10) * 10;
            return $words["$ten"] . " " . $words["$unit"] . " " . $novalue . "  " . no_to_words($remainno);
        }
    }
}
?>


<div style="width:80%;margin:10%;">
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px;margin-top:30px;">
        <thead>
            <tr>
                <td style="width:33.33%;font-size:12px;border-right:0px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <div style="float:left;">
                        <a href="" style="text-align:center" target="_blank">
                            <img class="CToWUd" src="<?php echo base_url(); ?>uploads/logo2.png" style="margin-bottom:10px;border:none;width:30%;">
                        </a> 
                    </div >    

                </td>
                <td style="width:33.33%;font-size:12px;border-right:0px solid #dddddd;border-bottom:1px solid #dddddd;text-align:center;padding:7px">
                    <div style="font-size:16px;text-transform:uppercase;font-weight:bold;" align="center">Booking Confirmation</div>   
                </td>
                <td style="width:33.33%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    <a href="" title="" style="text-align:center" target="_blank">
                        <img class="CToWUd" src="<?php echo base_url(); ?>assets/img/home/contact-img.png" alt="" style="margin-top:15px;border:none;width:30%;;">
                    </a>    
                </td>
            </tr>
        </thead>
    </table>  

    <table style="border-collapse:collapse;width:100%;margin-bottom:10px">
        <thead>
            <tr>
                <td style="font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222;text-align:center;text-transform:uppercase;" colspan="3">
                    <span class="il">Accessory Details</span></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:50%;font-size:14px;border:1px solid #dddddd;text-align:left;padding:7px">
                    <b><span class="il">Booking Order No:</span></b> 
                    <?php
                    echo $acc_config['invoice_no'];
                    ?>
                    <br>
                </td>
                <td style="width:50%;font-size:14px;border:1px solid #dddddd;text-align:left;padding:7px">
                    <b>Customer Name:</b> <?php echo $customer_details[0]['name'] . ' ' . $customer_details[0]['lastname']; ?><br>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width:100%;font-size:14px;border:1px solid #dddddd;text-align:left;padding:7px">
                    <b><span class="il">Date of Booking:</span></b>
                    <?php
                    echo substr(($accessory_details[0]['added_date']), 0, 10);
                    ?>
                    <br>
                </td>                
            </tr>
        </tbody>    
        <tbody>
            <tr>
                <td style="width:50%;font-size:14px;border:1px solid #dddddd;text-align:left;padding:7px">
                    <table>
                        <tr>
                            <td>
                                <p><b>Correspondence Address</b></p>   
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p><?php echo $customer_details[0]['name'] . ' ' . $customer_details[0]['lastname']; ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p><?php echo $customer_details[0]['address']; ?>,<?php echo $customer_details[0]['city']; ?>,</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p><?php echo $customer_details[0]['state']; ?>,<?php echo $customer_details[0]['pincode']; ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>Email Id: <?php echo $customer_details[0]['email']; ?></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>Phone No: <?php echo $customer_details[0]['telephone']; ?></p>
                            </td>
                        </tr>
                    </table>
                </td>  

                <td style="width:50%;font-size:14px;border:1px solid #dddddd;text-align:left;padding:7px;">

                    <table>
                        <tr>
                            <td>
                                <p><b>Delivery Address</b></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>Accessories can be picked up or fitted in your vehicle at our authorised partner dealer. Our customer care will get in touch with you within the next 48 hours to discuss this further.</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p><b><br></b></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p><b><br></b></p>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>

        </tbody>


    </table>
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
<!--        <thead>
            <tr>
                <td colspan="7" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"><div align="center">Accessorry Information</div></td>
            </tr>
        </thead>-->
        <thead>
            <tr>
                <td style="width:5%;font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Sr No
                </td>

                <td style="width:20%;font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Part Number
                </td>

                <td style="width:35%;font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Part Name
                </td>

<!--                <td style="font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Image
                </td>-->

                <td style="width:10%;font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Quantity
                </td>

                <td style="width:10%;font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    MRP (Rs.)
                </td>

                <td style="width:10%;font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Discount (Rs.)
                </td>                
                <td style="width:10%;font-size:16px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    Total Amount (Rs.)
                </td>                
            </tr>
        </thead>   
        <tbody>
            <?php
            $i = 1;
            $grand_total = '';
            foreach ($accessory_details as $acc_details_data) {
                ?>
                <tr>
                    <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                        <?php
//                    print_r($acc_details_data);
                        $dis_price = 0;
                        echo $i++;
                        ?>
                    </td>
                    <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                        <?php echo $acc_details_data['accessory_part_no']; ?>
                        <?php // echo $acc_details_data['name']; ?>
                    </td>
                    <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                        <?php echo $acc_details_data['name']; ?>

                    </td>
                    <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">

                        <?php echo $acc_details_data['quantity']; ?>
                    </td>
                    <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                        <?php
                        if (empty($acc_details_data['discount_price'])) {
                            $price = $acc_details_data['price'];
                            $price2 = $acc_details_data['discount_price'];
                            echo indianFormatNumber($price);
                        } else if (!empty($acc_details_data['discount_price'])) {
                            $price2 = $acc_details_data['price'];
                            $price = $acc_details_data['discount_price'];
                            echo indianFormatNumber($price2);
                        }

                        $mrp_total = $mrp_total + $price;
                        ?>

                        <?php
                        if (!empty($price2)) {
                            $dis_price = ($price2 - $price) * $acc_details_data['quantity'];
                            $total = $price2 * $acc_details_data['quantity'];
                        } else {
                            $total = $price * $acc_details_data['quantity'];
                        }-
                        $grand_discount = $grand_discount + $dis_price;
                        ?>
                    </td>
                    <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                        <?php
                        echo indianFormatNumber($dis_price);

                        $dis_total = $dis_total + $dis_price;
                        ?>
                    </td>
                    <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">

                        <?php
//                        echo indianFormatNumber($total);
                        if($dis_price>0)
                        {
                        $total=$price*$acc_details_data['quantity'];    
                        }    
                        else {
                        $total;    
                        }
                        echo indianFormatNumber($total);
                        $total_amount = $total_amount + $total;

                        $grand_total = $total + $grand_total;
                        ?>
                    </td>

                </tr>
                <?php
            }
            ?>
        </tbody>    
        <tfoot> 
<!--            <tr> 
                <td colspan="2" style="font-size:14px;border-right:1px solid #dddddd;font-weight:bold;text-align:left;padding:7px;color:#222222">
                </td>
                <td colspan="2" style="font-size:14px;border-right:1px solid #dddddd;font-weight:bold;text-align:right;padding:7px;color:#222222">
                    <p><b><span class="il">Sub Total Amount (Rs.)</span></b></p>
                <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;font-weight:bold;text-align:right;padding:7px;color:#222222">
                    <?php echo indianFormatNumber($mrp_total); ?>

                </td>
                <td style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;font-weight:bold;text-align:right;padding:7px;color:#222222">
                    <p><?php echo indianFormatNumber($dis_total); ?></p>

                </td>
                <td  style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;font-weight:bold;text-align:right;padding:7px;color:#222222">
                    <p><?php echo indianFormatNumber($total_amount); ?></p>

                </td>
            </tr>
            <tr> 
                <td colspan="2" style="font-size:14px;border-right:1px solid #dddddd;font-weight:bold;text-align:left;padding:7px;color:#222222">
                </td>
                <td colspan="2" style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;font-weight:bold;text-align:right;padding:7px;color:#222222">
                    <p><b><span class="il">Total Discount (Rs.)</span></b></p>
                </td>
                <td colspan="3" style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;font-weight:bold;text-align:right;padding:7px;color:#222222">

                    <p><?php echo indianFormatNumber($dis_total); ?></p>

                </td>
            </tr>-->
            <tr> 
                <td colspan="2"  style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;font-weight:bold;text-align:left;padding:7px;color:#222222">
<!--                    <div  style="text-align:left;"><b><span class="il" style="float:left;">(Customer Signature)</span></b>
                        <div>    
                            <div  style="text-align:right;">    
                                <b><span class="il">(Authorized Signatory)</span></b>
                            </div>-->

                </td>

                <td colspan="2" style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;font-weight:bold;text-align:right;padding:7px;color:#222222">
                    <p style="text-align:right;"><b><span class="il">Total Payable Amount (Rs.)</span></b></p>
                    <p><b><span class="il"><p><?php echo no_to_words($grand_total); ?></p></span></b></p>
                </td>
                <td colspan="3" style="font-size:14px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;font-weight:bold;text-align:right;padding:7px;color:#222222">
                    <p><?php echo indianFormatNumber($grand_total); ?></p>
                    <br>
                </td>

            </tr>

        </tfoot>        
    </table>
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td  colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    <span class="il">Important </span> Information</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2" style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <ul >
                        <li style="font-size:12px;">Genunie Accessories with Manufacturer Warranty as applicable</li>    
                        <li style="font-size:12px;">Accessories will be picked up from our partner dealer</li>    
                        <li style="font-size:12px;">All the offers are promoted by our respective partners</li>    
                        <li style="font-size:12px;">After Sales Service will be provided by our partner dealer</li>    
                        <li style="font-size:12px;">Please refer to our website for Terms of Offer for Sale</li>    
                        <li style="font-size:12px;">Taxes & other charges as applicable</li>    
                        <li style="font-size:12px;">Total Price indicated is based on on Taxes & Local Levies as applicable (subject to change)</li>    
                        <li style="font-size:12px;">Fittment charges to be paid to authorised dealer if applicable.</li>    
                        <li style="font-size:12px;">Fully refundable within 48 hours of booking</li>    

                    </ul>
                </td>

            </tr>

        </tbody>


    </table>
    
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td  style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222;">

                    <img style="margin-bottom:10px;border:none;width:100%;" src="<?php echo base_url() . 'uploads/955X165-process-order.png'; ?>"  class="CToWUd">    
                    
                </td>                
            </tr>
        </thead>
        
    </table>

    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222;">
                    <b>
                        Our customer care will get in touch with you within the next 48 hours to confirm the delivery details along with offers on other related services and accessories requested by you. You could also reach our customer care for any clarification by emailing at contact@mynewcar.in or on +91 22 45 02 03 04 (Daily l 10 am - 7 pm)
                    </b>
                </td>
            </tr>
        </thead>
    </table>
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">MYNEWCAR.IN Support</td>

            </tr>
        </thead>


        <tbody>
            <tr>
                <td colspan="2" style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    Web: https://mynewcar.in/contact-us
                    <br>
                    FAQ's: https://mynewcar.in/faq
                    <br>
                    Telephone: +022 45 02 03 04  (Daily | 10 am - 7 pm)
                    <br>
                    Email: contact@mynewcar.in
                    <br>
                    Visit Us on Facebook or Follow Us on Twitter
                    <br>
                    DREAMZ MYNEWCAR INDIA Pvt. Ltd. Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.

                </td>

            </tr>
        </tbody>
    </table>

</div>
