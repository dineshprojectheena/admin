<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
     <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->


                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                           <div class="table-header">
                                        Banners
                                    </div>
                            <?php 
//                            print_r($banner_get);
                            foreach ($banner_get as $res)
                            ?>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <form action="<?php echo site_url("banner_update"); ?>" class="form-horizontal" id="create-accessory" method="POST" enctype="multipart/form-data" role="form" novalidate="novalidate">
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="accessory_name">Banner Name</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" value="<?php echo $res-> name;?>" name="banner_name" id="banner_name" class="col-xs-10 col-sm-5">
                                                <input type="hidden" value="<?php echo $res-> banner_id;?>" name="banner_id" id="banner_id" >
                                                
                                            </div>
                                            <span class="error"><?php echo form_error('banner_name');?></span>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>


                                    <div class="form-group">
                                        <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="s2id_autogen1">Status</label>
                                        <div class="col-xs-12 col-sm-9">
                                             <?php
                                                            $status = $res->status;
                                                            if ($status == 1) {
                                                                $chk1 = 'selected';
                                                            } else if ($status == 2) {
                                                                $chk2 = 'selected';
                                                            }
                                                            ?>
                                                            <select name="status" id="status">
                                                                <option <?php
                                                                if ($status == '1') {
                                                                    echo $chk1;
                                                                }
                                                                ?> value="1">Enabled</option>    
                                                                <option <?php
                                                                if ($status == '2') {
                                                                    echo $chk2;
                                                                }
                                                                ?> value="2">Disabled</option>    
                                                            </select> 
                                        </div>
                                         <span class="error"><?php echo form_error('status');?></span>
                                    </div>

                                    <div class="space-4"></div>

                                    
                                     <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="accessory_pic">Banner Pic</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="file" name="accessory_pic"  id="accessory_pic" class="col-xs-10 col-sm-5">
                                                
                                            </div>
                                            <span class="error"><?php echo form_error('accessory_pic');?></span>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="space-4"></div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="title">Title</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" name="title" value="<?php echo $res-> title;?>" id="title" class="col-xs-10 col-sm-5">
                                                
                                            </div>
                                            <span class="error"><?php echo form_error('title');?></span>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="link">Link</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" name="link" value="<?php echo $res-> link;?>" id="link" class="col-xs-10 col-sm-5">
                                                
                                            </div>
                                            <span class="error"><?php echo form_error('link');?></span>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>



                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="banner_desc">Banner Description</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <textarea name="banner_desc" id="banner_desc"  class="col-xs-10 col-sm-5"><?php echo $res-> description;?></textarea>
                                                
                                            </div>
                                            <span class="error"><?php echo form_error('banner_desc');?></span>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>
                                    
                                    
                                    <?php
                                            if (empty($banner_get)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   

                                </form>
                            </div>
                        </div>
                        
                            
                            
                          
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
<?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



