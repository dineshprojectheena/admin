<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php
    $this->load->view('templates/header');
//    print_r($results_up);
    ?>    
    <style>
        #commentForm {
            width: 500px;
        }
        #commentForm label {
            width: 250px;
        }
        #commentForm label.error, #commentForm input.submit {
            margin-left: 253px;
        }
        #signupForm {
            width: 670px;
        }
        #signupForm label.error {
            margin-left: 10px;
            width: auto;
            display: inline;
        }
        #newsletter_topics label.error {
            display: none;
            margin-left: 103px;
        }
    </style>
    <style>
        .col-sm-3
        {
            width:20% !important;    
        }
        .tab-content
        {
            overflow:hidden !important;    
        }
    </style>  

    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">
                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                <div class="main-content">
                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <script>

                        $(document).ready(function () {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'product/video_results'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function (sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });

                    </script>
                    <!-- breadcrumbs data links end here -->
                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Variant Video
                            </div>
                        </div>
                        <?php 
                        $status = $this->session->flashdata('status');                        
                        if (!empty($status)): ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <?php echo $status ?>
                                <br>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form"  action="<?php echo site_url("product/video_upload"); ?>"  enctype="multipart/form-data" role="form" class="form-horizontal" method="POST">
                                            <div class="span6">                                                
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <h4 class="header blue bolder smaller">Video Details</h4>

                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="brand_id" id="brand_id" onchange="get_expert_model();" style="width: 340px;">
                                                                        <option  value="">Select Brand</option>
                                                                        <?php
                                                                        foreach ($brand as $brand_res) {
                                                                            ?>
                                                                            <option value="<?php echo $brand_res['brand_id']; ?>"  <?php
                                                                            if ($brand_res['brand_id'] == $single_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>  ><?php echo $brand_res['brand_name']; ?></option>    
                                                                                    <?php
                                                                                }
                                                                                ?>  
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('brand_id'); ?></span>
                                                                </div> 
                                                            </div>

                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                                                <div class="col-sm-9" style="float:left;" id="model_res" >
                                                                    <select name="model_id" id="model_id" style="width: 340px;">
                                                                        <option  value="">Select model</option>                                                                                                                                              
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                                                <div class="col-sm-9" style="float:left;" id="variant_results" >
                                                                    <select name="all_product_name" id="all_product_name">
                                                                        <option  value="">Select Variant</option>
                                                                    </select>  
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="video">Video Link</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="video" style="width:335px;" class="span10"
                                                                               value="<?php
                                                                               if (!isset($_POST['video'])) {
                                                                                   if ($single_res['variant_video'] != '') {
                                                                                       echo $single_res['variant_video'];
                                                                                   }
                                                                               } else if (isset($_POST['video'])) {
                                                                                   echo $_POST['video'];
                                                                               }
                                                                               ?>" id="video" value="" class="col-xs-10 col-sm-5">                                                                
                                                                        <input type="hidden" name="variant_video_id" style="width:335px;" class="span10"
                                                                               value="<?php
                                                                               if (!isset($_POST['variant_video_id'])) {
                                                                                   if ($single_res['variant_video_id'] != '') {
                                                                                       echo $single_res['variant_video_id'];
                                                                                   }
                                                                               } else if (isset($_POST['variant_video_id'])) {
                                                                                   echo $_POST['variant_video_id'];
                                                                               }
                                                                               ?>" id="variant_video_id" class="col-xs-10 col-sm-5">                                                                
                                                                        <span class="error"><?php echo form_error('video'); ?></span>    
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>













                                                        </div>                                                   



                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($single_res)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Variant Name</th>
                                            <th>Video Link</th>
                                            <!--<th>Status</th>-->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th colspan="7" >
                                                <button class="btn btn-xs btn-danger" style="float:right" onclick="delete_all_video();" name="deleteall" type="submit">
                                                    Delete All
                                                </button>
                                            </th>

                                        </tr>
                                    </tfoot>
                                </table>

                            </div>     

                        </div>




                        <!-- main content data end here -->
                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->
            <script>
                function delete_all_video()
                {

                    var val = [];
                    $(':checkbox:checked').each(function (i) {
                        val[i] = $(this).val();
//                        alert(val[i]);
                    });
                    window.location.href = "<?php echo base_url('product/video_delete/') ?>/?id=" + val;
//                    alert(val);

                }
                get_expert_model();
                function get_expert_model()
                {
                    var model_id = "<?php
                                            if (!empty($single_res['model_id'])) {
                                                echo $single_res['model_id'];
                                            } else if (!empty($_POST['model_id'])) {
                                                echo $_POST['model_id'];
                                            }
                                            ?>";
//                            alert(model_id);
                    var brand_id = $('#brand_id').val();
                    if (brand_id != '')
                    {

                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('stitcher/get_exp_brand_wise_model') ?>",
                            data: {brand_id: brand_id, model_id: model_id}
                        }).done(function (html) {
                            $("#model_res").html(html);
                            get_exp_variant();
                        });
                    }
                }
                function get_exp_variant()
                {

                    var variant_id = "<?php
                                            if (!empty($single_res['variant_id'])) {
                                                echo $single_res['variant_id'];
                                            } else if (!empty($_POST['all_product_name'])) {
                                                echo $_POST['all_product_name'];
                                            }
                                            ?>";
                    var brand_id = $('#brand_id').val();
                    var model_id = $('#model_id').val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('stitcher/get_exp_model_wise_variant') ?>",
                        data: {model_id: model_id, brand_id: brand_id, variant_id: variant_id}
                    }).done(function (html) {
                        $("#variant_results").html(html);
                    });
                }
            </script>
            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <script src="<?php echo base_url() . 'assets/js/modules/validate_video.js' ?>" ></script>
            <?php $this->load->view('templates/footer') ?>    

    </body>
</html>







