
<!-- main content data start here -->
<div class="page-content">
    <div class="page-header">
        <div class="table-header">
            Order to dealer map
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">

                    <div class="span6">
                        <div class="tabbable tabs-left">
                            <ul id="myTab3" class="nav nav-tabs">
                                <li class="active">
                                    <a href="#home3" data-toggle="tab">
                                        <i class="pink icon-dashboard bigger-110"></i>
                                        Order Details
                                    </a>
                                </li>



                                <li class="">
                                    <a href="#dropdown13" data-toggle="tab">
                                        <i class="icon-rocket"></i>
                                        Product
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#dropdown14" data-toggle="tab">
                                        <i class="icon-rocket"></i>
                                        Dealer Mapping
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="home3">
                                    <div class="profile-user-info profile-user-info-striped">
                                        <?php
                                        foreach ($order_details as $order_details_data)
                                            
                                            ?>
                                        <div class="profile-info-row">
                                            <div class="profile-info-name"> Order Id </div>

                                            <div class="profile-info-value">
                                                <span id="username" class="editable editable-click">
                                                    <?php echo $order_details_data['order_id']; ?>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="profile-info-row">
                                            <div class="profile-info-name"> Invoice No</div>

                                            <div class="profile-info-value">
                                                <?php echo $order_details_data['invoice_no']; ?>
<!--                                                    <i class="icon-map-marker light-orange bigger-110"></i>
                                                <span id="country" class="editable editable-click">Netherlands</span>
                                                <span id="city" class="editable editable-click">Amsterdam</span>-->
                                            </div>
                                        </div>



                                        <div class="profile-info-row">
                                            <div class="profile-info-name"> First Name</div>

                                            <div class="profile-info-value">
                                                <span id="username" class="editable editable-click">
                                                    <?php
                                                    if ($order_details_data['firstname'] == '') {
                                                        echo '-';
                                                    } else {
                                                        echo $order_details_data['firstname'];
                                                    }
                                                    ?>

                                                </span>
                                            </div>
                                        </div>
                                        <div class="profile-info-row">
                                            <div class="profile-info-name"> Last Name</div>

                                            <div class="profile-info-value">
                                                <span id="username" class="editable editable-click">
                                                    <?php
                                                    if ($order_details_data['lastname'] == '') {
                                                        echo '-';
                                                    } else {
                                                        echo $order_details_data['lastname'];
                                                    }
                                                    ?>

                                                </span>
                                            </div>
                                        </div>
                                        <div class="profile-info-row">
                                            <div class="profile-info-name"> Address1</div>

                                            <div class="profile-info-value">
                                                <span id="username" class="editable editable-click">
                                                    <?php
                                                    if ($order_details_data['address'] == '') {
                                                        echo '-';
                                                    } else {
                                                        echo $order_details_data['address'];
                                                    }
                                                    ?>                                                    
                                                </span>
                                            </div>


                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Email </div>

                                                <div class="profile-info-value">
                                                    <span id="signup" class="editable editable-click">
                                                        <?php echo $order_details_data['email']; ?>

                                                    </span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Telephone </div>

                                                <div class="profile-info-value">
                                                    <span>
                                                        <?php
                                                        if ($order_details_data['telephone'] == '') {
                                                            echo '-';
                                                        } else {
                                                            echo $order_details_data['telephone'];
                                                        }
                                                        ?>

                                                    </span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Date Added </div>

                                                <div class="profile-info-value">
                                                    <?php
                                                    if ($order_details_data['added_date'] == '') {
                                                        echo '-';
                                                    } else {
                                                        echo $order_details_data['added_date'];
                                                    }
                                                    ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane" id="dropdown13">
                                    <table class="table table-striped table-bordered table-hover" id="sample-table-1">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Image</th>

                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            foreach ($pro_details as $pro_details_data)
                                                
                                                ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    print_r($pro_details_data['pro_name']);
                                                    ?>
                                                </td>
                                                <td>
                                                    <img width="100px" src="<?php echo base_url() . 'uploads/product/' . $pro_details_data['pro_image']; ?>" />   
                                                </td>

                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="dropdown14">

                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td class="left"><b>Date Added</b></td>
                                                <td class="left"><b>Comment</b></td>
                                                <td class="left"><b>Status</b></td>                                                
                                                <td class="left"><b>Dealer Notified</b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tot=count($order_history);
                                            if ($tot > 0) {
                                                foreach ($order_history as $order_history_data) {
                                                    ?>
                                                    <tr>
                                                        <td ><?php echo $order_history_data['added_date']; ?></td>
                                                        <td ><?php echo $order_history_data['cust_message']; ?></td>
                                                        <td ><?php echo $order_history_data['order_status_name']; ?></td>
                                                        
                                                        <td ><?php
                                                            if ($order_history_data['notify_customer'] == 1) {
                                                                echo 'Yes';
                                                            } else {
                                                                echo 'No';
                                                            }
                                                            ;
                                                            ?></td>

                                                    </tr>        
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="4" class="center">No results!</td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <form action="<?php echo site_url("update-dealer-status"); ?>" class="form-horizontal" method="POST">
                                        <div class="tab-pane active" id="home3" style="float:left;width:100%;">

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label no-padding-right" for="dealer_id">Dealer Name</label>
                                                <div class="col-sm-9">
                                                    <div class="clearfix">
                                                        <input type="text" id="dealer_id" name="dealer_id" class="col-xs-10 col-sm-5" />
                                                    </div>                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="status" class="control-label col-xs-12 col-sm-3 no-padding-right">Order Status</label>
                                                <div class="col-xs-12 col-sm-9">                                                                                                                                        
                                                    <select style="width:337px;float:left;" id="status" name="status">
                                                        <option value="">Select Status</option>
                                                        <?php
                                                        foreach ($order_status as $order_status_data) {
                                                            ?>
                                                            <option value="<?php echo $order_status_data['order_status_id']; ?>"><?php echo $order_status_data['order_status_name']; ?></option>    
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>   

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="notify" class="col-sm-3 control-label no-padding-right">Notify Dealer</label>
                                                <input type="checkbox"  style="margin-left:10px; " value="1" id="notify" name="notify[]">                                                                
                                                <input type="hidden"  style="margin-left:10px;" value="<?php echo $id; ?>" id="order_id" name="order_id">                                                                
                                            </div>                                                            
                                            <div class="space-4"></div>
                                            <div class="form-group">
                                                <label for="comments" class="col-sm-3 control-label no-padding-right">Comments</label>
                                                <div class="col-sm-9">
                                                    <div class="clearfix">
                                                        <textarea class="col-xs-10 col-sm-5" id="comments" name="comments"></textarea>                                                                
                                                        <span class="error"></span>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group" align="center">
                                                <button class="btn btn-small btn-warning"  id="button-history">Add History</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="space-4"></div>


                </div>
            </div>
        </div>
    </div>
    <!-- main content data end here -->

</div><!-- /.main-content -->                

</div><!-- /.main-container-inner -->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

