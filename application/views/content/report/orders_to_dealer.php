
<!-- main content data start here -->
<div class="page-content">
    <div class="page-header">
        <div class="table-header">
            Latest Orders
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-2" aria-describedby="sample-table-2_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 182px;" aria-label="Domain: activate to sort column ascending">Order Id</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Customer</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Product</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Date Added</th>
                                                        <th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 177px;" aria-label="Status: activate to sort column ascending">Action</th><th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 170px;" aria-label="">Download</th>
                                                    </tr>
                                                </thead>


                                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                    <?php
                                                    foreach($report as $report_data)
                                                    {    
                                                    ?>                                                  
                                                    <tr class="odd">
                                                        
                                                        <td class=""><?php echo $report_data['order_id']; ?></td>
                                                        <td class=""><?php echo $report_data['firstname']; ?></td>
                                                        <td class=""><?php echo $report_data['pro_name'].' '.$report_data['variant']; ?></td>                                                        
                                                        <td class=""><?php echo $report_data['added_date']; ?></td>
 
                                                        <td class="td-actions ">
                                                            <div class="hidden-phone visible-desktop action-buttons">
                                                                <a href="<?php echo base_url().'order-to-dealer-map/'.$report_data['order_id'];?>" class="btn btn-mini btn-danger" style="width:100px;padding:1px;">
											<i class="icon-bolt"></i>
                                                                                        Dealer
											<i class="icon-arrow-right  icon-on-right"></i>
                                                                </a>

                                                            </div>

                                                            
                                                        </td>
							<td class="td-actions ">  
                                                                <a class="green" href="<?php echo base_url().'stitcher/download_pdf/'.$report_data['order_id'];?>">
                                                                    <i class="icon-pencil bigger-130"></i>
                                                                </a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    }    
                                                    ?>
                                                </tbody>
                                                </table>       
                </div>
            </div>
        </div>
    </div>
    <!-- main content data end here -->

</div><!-- /.main-content -->                

</div><!-- /.main-container-inner -->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

