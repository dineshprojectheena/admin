
<!-- main content data start here -->
<div class="page-content">
    <div class="page-header">
        <div class="table-header">
            Latest Orders
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                <table class="table table-striped table-bordered table-hover dataTable" id="sample-table-2" aria-describedby="sample-table-2_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Domain: activate to sort column ascending">Order Id</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Invoice No</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Product</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Customer</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Email</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Affiliate Code</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Price: activate to sort column ascending">Date Added</th>
                                                        <!--<th class="hidden-480 sorting" role="columnheader" tabindex="0" aria-controls="sample-table-2" rowspan="1" colspan="1" style="width: 131px;" aria-label="Status: activate to sort column ascending"></th>-->
                                                        <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 131px;" aria-label="">Action</th>
                                                    </tr>
                                                </thead>


                                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                    <?php
                                                    foreach($report as $report_data)
                                                    {    
                                                    ?>                                                  
                                                    <tr class="odd">                                                        
                                                        <td class=""><?php echo $report_data['order_id']; ?></td>
                                                        <td class=""><?php echo $report_data['invoice_no']; ?></td>
                                                        <td class=""><?php echo $report_data['pro_name_comp']; ?></td>                                                        
                                                                                                                
                                                        <td class=""><?php echo $report_data['firstname']; ?></td>
                                                        <td class=""><?php echo $report_data['email']; ?></td>
                                                        <td class=""><?php echo $report_data['code']; ?></td>
                                                        
                                                        <td class=""><?php echo $report_data['added_date']; ?></td>                                                        
                                                        <td class="td-actions">
                                                            <div class="hidden-phone visible-desktop action-buttons">
                                                                <a href="<?php echo base_url().'order-detail/'.$report_data['order_id'];?>" class="blue">
                                                                    <i class="icon-zoom-in bigger-130"></i>
                                                                </a>
                                                            </div>
                                                            <div class="hidden-phone visible-desktop action-buttons">
                                                                <a class="green" href="<?php echo base_url().'stitcher/download_pdf/'.$report_data['order_id'].'/2';?>" target="_blank">
                                                                    <i class="icon-pencil bigger-130"></i>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    }    
                                                    ?>
                                                </tbody>
                                                </table>       
                </div>
            </div>
        </div>
    </div>
    <!-- main content data end here -->

</div><!-- /.main-content -->                

</div><!-- /.main-container-inner -->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

