<script>
    $(document).ready(function () {
        $('.table').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": '<?php echo base_url() . 'order/brand_result'; ?>',
            "aaSorting": [[4, "desc"]],
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                });
            }
        });
    });
</script>
<!-- main content data start here -->
<div class="page-content">
    <div class="page-header">
        <div class="table-header">
            Latest Upcoming Orders
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                        <thead>
                            <tr role="row">
                                <th >Order Id</th>
                                <th >Invoice No</th>
                                <th >Product</th>
                                <th >First Name</th>
                                <th >Last Name</th>
                                <th >Email</th>
                                <th >Booking Amount</th>
                                <th >City</th>
                                <th >Date Added</th>
                                <!--<th >Action</th>-->
                            </tr>
                        </thead>


                    </table>       
                </div>
            </div>
        </div>
    </div>
    <!-- main content data end here -->

</div><!-- /.main-content -->                

</div><!-- /.main-container-inner -->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

