<div class="page-content">
    <div class="page-header">
        <h1>
            Dealer
            <small>
                <i class="icon-double-angle-right"></i>
                Join Us Enquiry
            </small>
        </h1>
    </div>
    <script>
        $(document).ready(function () {
            $('.table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'dealer_prices/datatables_dealer_join_us'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
        });
    </script>
    <div class="modal fade" id="loginModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="mess_heading">View & Sync</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row" id="result">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $status = $this->session->flashdata('success_msg');
    if (!empty($status)):
        ?>
        <div class="alert alert-warning">
            <button data-dismiss="alert" class="close" type="button">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <?php echo $status ?>
            <br>
        </div>
    <?php endif; ?>
    <?php
    $status2 = $this->session->flashdata('error_msg');
    if (!empty($status2)):
        ?>
        <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <?php echo $status2 ?>
            <br>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-xs-12">


            <div class="span6">
                <div id="recent-box" class="widget-box transparent">

                    <div class="widget-body">
                        <div class="widget-main">

                            <!--                            <div class="smaller lighter green" style="height:100px;">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">
                                                                    <i class="icon-list"></i>
                                                                    Dealer Name
                                                                    <select size="1" name="supplier" id="supplier" onchange="send_data();"> 
                                                                        <option value="">Select Dealer</option>                                            
                            <?php
                            foreach ($dealer_data as $dealer_data_res) {
                                ?>
                                                                                            <option <?php
                                if ($_GET['supplier'] == $dealer_data_res['dealer_id']) {
                                    echo 'selected="selected"';
                                }
                                ?> value="<?php echo $dealer_data_res['dealer_id']; ?>"><?php echo $dealer_data_res['dealer_name']; ?></option>
                                <?php
                            }
                            ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <i class="icon-list"></i>
                                                                    Date By Sort
                                                                    <input  name="sdate" id="sdate" value="<?php echo $_GET['sdate']; ?>" placeholder="Start Date (yyyy-mm-dd)" />                                                 
                                                                    <input  name="edate" id="edate" value="<?php echo $_GET['edate']; ?>" placeholder="END Date (yyyy-mm-dd)" onchange="send_data();"/>                                                 
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <i class="icon-list"></i>
                                                                    Sync Status
                                                                    <select size="1" name="sync_status" id="sync_status" onchange="send_data();"> 
                                                                        <option <?php
                            if ($_GET['sync_status'] == '') {
                                echo 'selected="selected"';
                            }
                            ?> value="">Select Status</option>                                            
                                                                        <option <?php
                            if ($_GET['sync_status'] == '1') {
                                echo 'selected="selected"';
                            }
                            ?> value="1">Synced</option>                                            
                                                                        <option <?php
                            if ($_GET['sync_status'] == '2') {
                                echo 'selected="selected"';
                            }
                            ?> value="2">Rejected</option>                                            
                                                                        <option <?php
                            if ($_GET['sync_status'] == '0') {
                                echo 'selected="selected"';
                            }
                            ?> value="0">Pending</option>                                            
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <br><br>
                                                            <form action="<?php echo base_url() . 'dealer_prices/download' ?>" method="POST">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-12" align="left">
                                                                        <button type="submit" name="download_all"  onclick="download('1');" class="btn btn-info" style="float:left;margin:5px; ">                                                
                                                                            Download All
                                                                        </button>
                            
                                                                        <button type="submit" name="download_rejected"  onclick="download('2');" class="btn btn-info" style="float:left;margin:5px; ">                                                
                                                                            Download Rejected
                                                                        </button>
                                                                        <button type="submit" name="download_active"  onclick="download('3');" class="btn btn-info" style="float:left;margin:5px; ">                                                
                                                                            Download Active
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                            
                                                                                                    <div class="col-md-4">
                                                            
                                                            
                                                                                                    </div>
                                                        </div>-->

                            <div class="tab-content padding-8 overflow-visible">

                                <form action="<?php echo base_url() . 'dealer_prices' ?>" method="get" id="data_sort">


                                    <div class="tab-pane active" id="task-tab" >

                                        <table id="find-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Dealership Name</th>
                                                    <th>Title</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>hear about us?</th>
                                                    <th>Message</th>
                                                    <!--<th>Added</th>-->
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                    <th>Document</th>
                                                    <th>Credential</th>
                                                </tr>
                                            </thead>                                            
                                        </table>
                                        <!--<input type="submit" value="submit" id="submit" style="display: none;"/>-->
                                    </div>
                                </form>

                            </div>
                        </div><!--/widget-main-->
                    </div><!--/widget-body-->
                </div><!--/widget-box-->
            </div>
            <div class="space-6"></div>            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->