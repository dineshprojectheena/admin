<div class="page-content">
    <div class="page-header">
        <h1>
            Dealer
            <small>
                <i class="icon-double-angle-right"></i>
                Hot Deals
            </small>
        </h1>
    </div>
    <script>
        $(document).ready(function () {
            var dealer = "<?php echo $_GET['supplier']; ?>";
            var sdate = "<?php echo $_GET['sdate']; ?>";
            var edate = "<?php echo $_GET['edate']; ?>";
            var sync_status = "<?php echo $_GET['sync_status']; ?>";
            
//            alert(dealer);
            $('.table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'dealer_prices/datatables_allhotdeal?dealer='; ?>' + dealer + "&sdate=" + sdate + "&edate=" + edate + "&sync_status=" + sync_status,
                "aaSorting": [[4, "desc"]],
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
//            alert(fnCallback);
        });
    </script>
    <div class="row">
        <div class="col-xs-12">


            <div class="span6">
                <div id="recent-box" class="widget-box transparent">
                    <div class="widget-body">
                        <div class="widget-main padding-4">
                            <div class="smaller lighter green" style="height:100px;">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <i class="icon-list"></i>
                                        Dealer Name
                                        <select size="1" name="supplier" id="supplier" onchange="send_data();"> 
                                            <option value="">Select Dealer</option>                                            
                                            <?php
                                            foreach ($dealer_data as $dealer_data_res) {
                                                ?>
                                                <option <?php
                                                if ($_GET['supplier'] == $dealer_data_res['dealer_id']) {
                                                    echo 'selected="selected"';
                                                }
                                                ?> value="<?php echo $dealer_data_res['dealer_id']; ?>"><?php echo $dealer_data_res['dealer_name']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <i class="icon-list"></i>
                                        Date By Sort
                                        <input  name="sdate" id="sdate" value="<?php echo $_GET['sdate']; ?>" placeholder="Start Date (yyyy-mm-dd)" />                                                 
                                        <input  name="edate" id="edate" value="<?php echo $_GET['edate']; ?>" placeholder="END Date (yyyy-mm-dd)" onchange="send_data();"/>                                                 
                                    </div>
                                    <div class="col-md-3">
                                        <i class="icon-list"></i>
                                        Sync Status
                                        <select size="1" name="sync_status" id="sync_status" onchange="send_data();"> 
                                            <option <?php
                                            if ($_GET['sync_status'] == '') {
                                                echo 'selected="selected"';
                                            }
                                            ?> value="">Select Status</option>                                            
                                            <option <?php
                                            if ($_GET['sync_status'] == '1') {
                                                echo 'selected="selected"';
                                            }
                                            ?> value="1">Active</option>                                            
                                            <option <?php
                                            if ($_GET['sync_status'] == '2') {
                                                echo 'selected="selected"';
                                            }
                                            ?> value="2">Rejected</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <br><br>
                                <form action="<?php echo base_url() . 'dealer_prices/download_hot_deal' ?>" method="POST">
                                    <div class="col-md-12">
                                        <div class="col-md-12" align="left">
                                            <button type="submit" name="download_all"  onclick="download('1');" class="btn btn-info" style="float:left;margin:5px; ">                                                
                                                Download All
                                            </button>

                                            <button type="submit" name="download_rejected"  onclick="download('2');" class="btn btn-info" style="float:left;margin:5px; ">                                                
                                                Download Rejected
                                            </button>
                                            <button type="submit" name="download_active"  onclick="download('3');" class="btn btn-info" style="float:left;margin:5px; ">                                                
                                                Download Active
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-content padding-8 overflow-visible">
                                <form action="<?php echo base_url() . 'dealer_hot_deal' ?>" method="get" id="data_send">
                                    <div class="tab-pane active" id="task-tab" >
                                        <table id="find-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Product Name</th>
                                                    <th>Hot Deal Percent</th>
                                                    <th>Valid Untill</th>
                                                    <th>Added Date</th>
                                                    <th>Action</th>
    <!--                                                <th>Download</th>-->

                                                </tr>
                                            </thead>

                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th colspan="6" >
                                                        <button type="button" name="submit" id="merge_button" onclick="sync_live();" class="btn btn-info" style="float:right;margin:5px;">                                                
                                                            Sync Live
                                                        </button>
                                                        <button type="button" name="submit" id="reject_button" onclick="sync_live();" class="btn btn-info" style="float:right;margin:5px; ">                                                
                                                            Reject Live
                                                        </button>

                                                    </th> 
                                            <script>
                                                $("#merge_button").click(function (event) {
                                                    event.preventDefault();
                                                    var searchIDs = [];

                                                    $("#find-table input:checkbox:checked").map(function () {
                                                        searchIDs.push($(this).val());
                                                    });
                                                    
                                                    alert(searchIDs);
                                                    exit;
                                                    window.location.href = "<?php echo base_url() . 'dealer_prices/sync_hot_deal?data='; ?>" + searchIDs;
//                                                    alert(searchIDs);VV
//                                                    console.log(searchIDs);
                                                });
                                                function send_data()
                                                {
                                                    var supplier = $("#supplier").val();
                                                    var sdate = $("#sdate").val();
                                                    var edate = $("#edate").val();
                                                    var sync_status = $("#sync_status").val();
                                                    var url = "<?php echo base_url() . 'dealer_hot_deal?supplier='; ?>" + supplier + "&sdate=" + sdate + "&edate=" + edate + "&sync_status=" + sync_status;
                                                    window.location.href = url;
                                                }
                                                $("#reject_button").click(function (event) {
                                                    event.preventDefault();
                                                    var searchIDs = [];
                                                    $("#find-table input:checkbox:checked").map(function () {
                                                        searchIDs.push($(this).val());
                                                    });
                                                    window.location.href = "<?php echo base_url() . 'dealer_prices/reject_hot_deal?data='; ?>" + searchIDs;
                                                });

                                            </script>

                                            </tr>
                                            </tfoot>

                                        </table>

                                    </div>
                                </form>

                            </div>
                        </div><!--/widget-main-->
                    </div><!--/widget-body-->
                </div><!--/widget-box-->
            </div>
            <?php
//            exit;
            ?>


            <div class="space-6"></div>

            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->