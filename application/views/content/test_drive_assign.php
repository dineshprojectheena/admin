<div class="page-content">
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                Assign Test Drive
            </small>
        </h1>
    </div>
    <div class="row">
        <div class="col-xs-12">            
            <div class="span6">
                <div id="recent-box" class="widget-box transparent">
                    <div class="widget-header">
                        <h4 class="lighter smaller">
                            <i class="icon-rss orange"></i>
                            Assign Test Drive to dealer
                        </h4>

                        <div class="widget-toolbar no-border">
                            <ul id="recent-tab" class="nav nav-tabs">
                                <li class="active">
                                    <a href="#task-tab" data-toggle="tab">Test Drive</a>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form" action="<?php echo site_url("model_upload"); ?>" class="form-horizontal" method="POST">
                                            <div class="span6">
                                                <?php
                                                $results = validation_errors();
                                                if (!empty($results)) {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="tabbable tabs-left" >
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>


                                                    </ul>


                                                    <div class="tab-content" >
                                                        <div id="home3" class="tab-pane active">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="dealer">Dealer</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                         <select name="brand" id="brand" style="width:340px;float:left;">
                                                                            <option  value="">Select dealer</option>
                                                                            <?php
                                                                            foreach ($brand as $brand_res) {
                                                                                ?>
                                                                                <option 
                                                                                <?php
                                                                                if ($get_res['brand_id'] == $brand_res['brand_id']) {
                                                                                    echo 'selected';
                                                                                }
                                                                                ?>   value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                    <?php
                                                                                }
                                                                                ?>                                                                                                                        
                                                                        </select>  
                                                                        <span class="error"><?php echo form_error('brand'); ?></span>
                                                                        
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="dealer">Assign</label>
                                                                <div class="col-md-9">
                                                                <div class="col-sm-9">
                                                                <label class="col-sm-3 control-label no-padding-right">
                                                                <input type="radio" name="assign" id="assign1"/>Assign
                                                                </label>
                                                                
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            

                                                        </div>                                            
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($get_res)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                </div><!--/widget-box-->
            </div>
            <?php
//            exit;
            ?>


            <div class="space-6"></div>

            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->