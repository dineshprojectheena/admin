<div class="page-content">
    <script>
        $(document).ready(function () {
            $('.table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'stitcher/datatables_affiliates_count'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
        });
    </script>
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                Top Affiliates
            </small>
        </h1>
    </div><!-- /.page-header -->

    <div class="row">
        <div class="col-xs-12">
            <div class="span6">
                <div id="recent-box" class="widget-box transparent">
                    <div class="widget-header">
                        <h4 class="lighter smaller">
                            <i class="icon-rss orange"></i>
                            Latest Affiliates Count 
                        </h4>

                        <div class="widget-toolbar no-border">
                            <ul id="recent-tab" class="nav nav-tabs">
                                <li class="active">
                                    <a href="#task-tab" data-toggle="tab">Counts</a>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main padding-4">
                            <div class="tab-content padding-8 overflow-visible">
                                <div class="tab-pane active" id="task-tab">
                                    <h4 class="smaller lighter green">
                                        <i class="icon-list"></i>
                                        Sortable Lists
                                    </h4>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Affiliates User</th>
                                                <th>Affiliates Code</th>
                                                <th>Affiliates Sale Count</th>                                                
                                            </tr>
                                        </thead>
                                    </table>

                                </div>

                            </div>
                        </div><!--/widget-main-->
                    </div><!--/widget-body-->
                </div><!--/widget-box-->
            </div>
            
            
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->