<div class="page-content">
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                accessory
            </small>
        </h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $error_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <?php if (isset($success_msg)): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $success_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <form class="form-horizontal" id="validation-form" method="post" enctype="multipart/form-data" role="form" action="<?php echo base_url() . 'accessory/edit_accessory' ?>" id="create-feature" role="form" >

                <div class="form-group <?php echo (strlen( form_error('accessory_name') ) > 0 )? 'has-error'  : '' ; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_name">Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="hidden" name="accessory_id" value="<?php echo $accessory_details['accessory_id'] ?>" />
                            <input type="text" name="accessory_name" value="<?php echo set_value('accessory_name', $accessory_details['accessory_name']); ?>" class="col-xs-10 col-sm-5" />
                        </div>
                        <?php echo form_error('accessory_name'); ?>
                    </div>
                </div>
                
                <div class="space-4"></div>

<!--                <div class="form-group <?php echo (strlen(form_error('accessory_type')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_type">Accessory Type</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="accessory_type" name="accessory_type" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('accessory_type', '1', ($accessory_details['accessory_type'] == '1')? TRUE : FALSE); ?> value="1">Single</option>
                                <option <?php echo set_select('accessory_type', '2', ($accessory_details['accessory_type'] == '2')? TRUE : FALSE); ?> value="2">Package</option>
                            </select>
                        </div>
                        <?php echo form_error('accessory_type'); ?>
                    </div>
                </div>
                
                <div class="space-4"></div>

                <div class="form-group dependencies <?php echo (strlen(form_error('positive_dependencies')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="positive_dependencies">Positive Dependency</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" name="positive_dependencies" class="get_positive_dependency" />
                        </div>
                        <?php echo form_error('positive_dependencies'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group dependencies <?php echo (strlen(form_error('negative_dependencies')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="negative_dependencies">Accessory Type</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" name="negative_dependencies" class="get_negative_dependency" />
                        </div>
                        <?php echo form_error('negative_dependencies'); ?>
                    </div>
                </div>

                <div class="space-4"></div>-->

                <div class="form-group <?php echo (strlen( form_error('accessory_cat') ) > 0 )? 'has-error'  : '' ; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_cat">Accessory Type</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="car_type" class="select2" name="accessory_cat" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('accessory_cat', '1', ($accessory_details['accessory_cat'] == '1')? TRUE : FALSE); ?> value="1">Exteriors</option>
                                <option <?php echo set_select('accessory_cat', '2', ($accessory_details['accessory_cat'] == '2')? TRUE : FALSE); ?> value="2">Interiors</option>
                                <option <?php echo set_select('accessory_cat', '3', ($accessory_details['accessory_cat'] == '3')? TRUE : FALSE); ?> value="3">Safety & Technology</option>
                                <option <?php echo set_select('accessory_cat', '4', ($accessory_details['accessory_cat'] == '4')? TRUE : FALSE); ?> value="4">Audio & Communication</option>
                                <!--<option <?php echo set_select('accessory_cat', '5', ($accessory_details['accessory_cat'] == '5')? TRUE : FALSE); ?> value="5">Services</option>-->
                                <option <?php echo set_select('accessory_cat', '6', ($accessory_details['accessory_cat'] == '6')? TRUE : FALSE); ?> value="6">Equipment Package</option>
                                <option <?php echo set_select('accessory_cat', '7'); ?> value="7">Is-Services</option>
                            </select>
                        </div>
                        <?php echo form_error('accessory_cat'); ?>
                    </div>
                </div>
                
                <div class="space-4"></div>
                <div class="form-group <?php echo (strlen( form_error('accessory_sub_cat') ) > 0 )? 'has-error'  : '' ; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_sub_cat">Accessory Sub Type</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="car_type" class="select2" name="accessory_sub_cat" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('accessory_sub_cat', '1', ($accessory_details['accessory_sub_cat'] == '1')? TRUE : FALSE); ?> value="1">Headlights</option>
                                <option <?php echo set_select('accessory_sub_cat', '2', ($accessory_details['accessory_sub_cat'] == '2')? TRUE : FALSE); ?> value="2">Sunroofs</option>
                                <option <?php echo set_select('accessory_sub_cat', '3', ($accessory_details['accessory_sub_cat'] == '3')? TRUE : FALSE); ?> value="3">Tyres</option>
                                <option <?php echo set_select('accessory_sub_cat', '4', ($accessory_details['accessory_sub_cat'] == '4')? TRUE : FALSE); ?> value="4">Steering Wheels</option>
                                <option <?php echo set_select('accessory_sub_cat', '5', ($accessory_details['accessory_sub_cat'] == '5')? TRUE : FALSE); ?> value="5">Seats</option>
                                <option <?php echo set_select('accessory_sub_cat', '6', ($accessory_details['accessory_sub_cat'] == '6')? TRUE : FALSE); ?> value="6">Navigation System</option>
                                <option <?php echo set_select('accessory_sub_cat', '7', ($accessory_details['accessory_sub_cat'] == '7')? TRUE : FALSE); ?> value="7">Audio System</option>
                                <option <?php echo set_select('accessory_sub_cat', '8', ($accessory_details['accessory_sub_cat'] == '8')? TRUE : FALSE); ?> value="8">Speakers</option>
                                <option <?php echo set_select('accessory_sub_cat', '9', ($accessory_details['accessory_sub_cat'] == '9')? TRUE : FALSE); ?> value="9">Telephone</option>
                                <!--<option <?php echo set_select('accessory_sub_cat', '10', ($accessory_details['accessory_sub_cat'] == '10')? TRUE : FALSE); ?> value="10">Others</option>-->
                                
                                <option <?php echo set_select('accessory_sub_cat', '11', ($accessory_details['accessory_sub_cat'] == '11')? TRUE : FALSE); ?> value="11">Finance</option>
                                <option <?php echo set_select('accessory_sub_cat', '12', ($accessory_details['accessory_sub_cat'] == '12')? TRUE : FALSE); ?> value="12">AMC Contract</option>
                                <option <?php echo set_select('accessory_sub_cat', '13', ($accessory_details['accessory_sub_cat'] == '13')? TRUE : FALSE); ?> value="13">Insurance for car</option>
                                <option <?php echo set_select('accessory_sub_cat', '14', ($accessory_details['accessory_sub_cat'] == '14')? TRUE : FALSE); ?> value="14">Extended Warranty</option>
                                <option <?php echo set_select('accessory_sub_cat', '15', ($accessory_details['accessory_sub_cat'] == '15')? TRUE : FALSE); ?> value="15">Used Cars</option>
                                <option <?php echo set_select('accessory_sub_cat', '16', ($accessory_details['accessory_sub_cat'] == '16')? TRUE : FALSE); ?>  value="16">Home Delivery</option>
                                <option <?php echo set_select('accessory_sub_cat', '10', ($accessory_details['accessory_sub_cat'] == '10')? TRUE : FALSE); ?> value="10">Others</option> 
                            </select>
                        </div>
                        <?php echo form_error('accessory_sub_cat'); ?>
                    </div>
                </div>
                
                <div class="space-4"></div>

<!--                <div class="form-group <?php echo (strlen( form_error('accessory_price') ) > 0 )? 'has-error'  : '' ; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_price">Price</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="number" name="accessory_price" value="<?php echo set_value('accessory_price', $accessory_details['accessory_price']); ?>" class="col-xs-10 col-sm-5" />
                        </div>
                        <?php echo form_error('accessory_price'); ?>
                    </div>
                </div>

                <div class="space-4"></div>-->

                <div class="form-group <?php echo (strlen( form_error('status') ) > 0 )? 'has-error'  : '' ; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="car_type" class="select2" name="status" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('status', '1', ($accessory_details['status'] == '1')? TRUE : FALSE); ?> value="1">Active</option>
                                <option <?php echo set_select('status', '0', ($accessory_details['status'] == '0')? TRUE : FALSE); ?> value="0">Inactive</option>
                            </select>
                        </div>
                         <?php echo form_error('status'); ?>
                    </div>
                </div>
                
                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen( form_error('accessory_information') ) > 0 )? 'has-error'  : '' ; ?>">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_information">Accessory Information</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <?php
//                            print_r($accessory_details);                            
                            ?>
                            <textarea name="accessory_information" style="height:120px;" class="col-xs-10 col-sm-5"><?php echo set_value('accessory_information', $accessory_details['accessory_information']); ?> </textarea>
                        </div>
                        <?php echo form_error('accessory_information'); ?>
                    </div>
                </div>

                <div class="space-4"></div>
                
                 <div class="form-group <?php echo (strlen( form_error('accessory_img') ) > 0 )? 'has-error'  : '' ; ?>">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_img">Image</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="file" name="accessory_img" class="col-xs-10 col-sm-5" >
                            <input type="hidden" name="feature_img2" value="<?php echo set_value('feature_img',$accessory_details['accessory_img']) ?>" class="col-xs-10 col-sm-5" >
                        </div>                        
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info">
                            <i class="icon-ok bigger-110"></i>
                            Update
                        </button>
<!--
                        &nbsp; &nbsp; &nbsp;
                        <button class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            Reset
                        </button>-->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url().'assets/js/modules/validate_accessory.js';?>"></script>
<script>
jQuery(document).ready(function($){
    var prepolulate_positive_dependency;
    var prepolulate_negative_dependencies;
<?php if (isset($positive_dependencies)): ?>
            prepolulate_positive_dependency = <?php echo $positive_dependencies ?>;
<?php else: ?> 
            prepolulate_positive_dependency = [];
<?php endif; ?>
<?php if (isset($negative_dependencies)): ?>
            prepolulate_negative_dependencies = <?php echo $negative_dependencies ?>;
<?php else: ?> 
            prepolulate_negative_dependencies = [];
<?php endif; ?>
        $(".get_positive_dependency").tokenInput("<?php echo base_url() . 'accessory/get_accessory'; ?>", {
            theme: "facebook",
            searchingText: 'Searching..',
            minChars: 3,
            preventDuplicates: true,
            prePopulate: prepolulate_positive_dependency
        });
        $(".get_negative_dependency").tokenInput("<?php echo base_url() . 'accessory/get_accessory'; ?>", {
            theme: "facebook",
            searchingText: 'Searching..',
            minChars: 3,
            preventDuplicates: true,
            prePopulate: prepolulate_negative_dependencies
        });
})

</script>

<!--<script type="text/javascript" src="<?php echo base_url().'assets/js/validation/feature.js' ?>"></script>-->