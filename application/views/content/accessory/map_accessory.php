<div class="page-content">
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                Variant Accessory Mapping 
            </small>
        </h1>
    </div>
    <script>
        $(document).ready(function() {
            $('.table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'accessory/all_map_accessory_result'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function(sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
        });
    </script>

    <div class="row">
        <div class="col-xs-12">

            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $error_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <?php if (isset($success_msg)): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $success_msg ?>
                    <br>
                </div>
            <?php endif;?>
            <form class="form-horizontal" id="validation-form" method="post" action="<?php echo base_url() . 'accessory/insert_accessory' ?>" id="create-feature" role="form" >
                <div class="form-group">
                    <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                    <div class="col-sm-9" >
                        <select name="brand_id" id="brand_id" onchange="getmodel();" style="width: 340px;">
                            <option  value="">Select Brand</option>
                            <?php
                            foreach ($brand as $brand_res) {
                                ?>
                                <option 
                                <?php
                                if ($results_up_copy[0]['brand_id'] == $brand_res['brand_id']) {
                                    echo 'selected';
                                } else if ($results_up[0]['brand_id'] == $brand_res['brand_id']) {
                                    echo 'selected';
                                }
                                ?>   value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                    <?php
                                }
                                ?>  
                        </select>  
                        <br> <span class="error"><?php echo form_error('brand_id'); ?></span>
                    </div> 
                </div> 

                <div class="form-group">
                    <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                    <div class="col-sm-9" style="float:left;" id="model_res" >
                        <select name="model_id" id="model_id" style="width: 340px;">
                            <option  value="">Select model</option>                                                                        
                        </select>  
                        <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                    </div>


                </div>

                <div class="form-group" id="" >
                    <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                    <div class="col-sm-9" style="float:left;" id="variant_results" >
                        <select name="all_product_name" id="all_product_name">
                            <option  value="">Select Variant</option>
                            <?php
                            if (!empty($sing_res_data)) {
                                ?>
                                <option selected="selected" value="<?php echo $sing_res_data['product_id'] ?>"><?php echo $sing_res_data['pro_name'] ?></option>
                                <?php
                            }
                            ?>
                        </select>  
                        <br> <span class="error"><?php echo form_error('product_name'); ?></span>
                    </div>
                </div>


<!--                <div class="form-group <?php echo (strlen(form_error('variant_id')) > 0 ) ? 'has-error' : ''; ?>"">
    <label class="col-sm-3 control-label no-padding-right" for="variant_id">Variant</label>
    <div class="col-sm-9">
        <div class="clearfix">
            <input type="text" id="product_name" name="variant_id" class="col-xs-10 col-sm-5" />
        </div>
                <?php echo form_error('variant_id'); ?>
    </div>
</div>-->

                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('feature_names')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_names">Select Accessory</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <textarea name="feature_names" class="feature_names"><?php echo set_value('feature_names'); ?></textarea>
                            <input name="feature_ids" value="<?php echo set_value('feature_ids'); ?>" type="hidden" class="feature_id" />

                            <button type="button" class="btn btn-primary select-variant-features">Select</button>
                        </div>
                        <?php echo form_error('feature_names'); ?>
                    </div>
                </div>

                <div class="space-4"></div>
                <div class="form-group <?php echo (strlen(form_error('feature_names')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="price">Price/Unit (Rs)</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" value="" id="price" name="price" placeholder=""  />    
                        </div>
                        <?php // echo form_error('feature_names'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info">
                            <i class="icon-ok bigger-110"></i>
                            Submit
                        </button>

                        &nbsp; &nbsp; &nbsp;
                        <button class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            Reset
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Variant Name</th>
                        <!--<th>Brand Group Name</th>-->
                        <th>Accessory</th>
                        <!--<th>Accessory Description</th>-->
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table> 




            <!--            <div class="form-group">
                                <div class="table-header">
                                    Results for "Map Accessory"
                                </div>
                                <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Variant Name</th>
                                            th>Brand Group Name</th
                                            <th>Accessory</th>
                                            <th>Accessory Description</th>
                                            <th>Price</th>
                                            
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
            <?php
            $i = 1;
            foreach ($all_features as $r) :
//                                print_r($r);
                ?>
                                                <tr>   
                                                    <form action="<?php echo base_url() . 'accessory/del_accessory_mapping'; ?>" method="POST">
                                                    <td>
                <?php echo $i; ?>                                                        
                                                    </td>
                                                    <td>
                <?php echo $r['pro_name']; ?>
                                                        
                                                        
                                                    </td>
                                                    td>
                <?php echo $r->group_name; ?>
                                                    </td
                                                    <td>
                <?php echo $r['accessory_name']; ?>
                                                    </td>
                                                    <td>
                <?php echo $r['accessory_information']; ?>
                                                    </td>
                                                    <td>
                <?php echo $r['price']; ?>
                                                    </td>
                                            
                                                <input type="hidden" name="accessory_id"  value="<?php echo $r['id']; ?>" />
                                                <input type="hidden" name="url"  value="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" />
                                                <td>
                                                    <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                        delete
                                                    </button>
                
                                                </td>                                                    
                                                </form>
                                            </tr>
                <?php $i++; ?>    
            <?php endforeach; ?>
                                    </tbody>
                                </table>                    
                        </div>-->
        </div>     
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Accessory</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary select-feature">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . 'assets/js/modules/validate_map_accessory.js'; ?>"></script>
<script>
                                                    $(document).ready(function() {
                                                        $(".select-variant-features").click(function() {
                                                            var product_id = $('#product_name').val();
                                                            if (product_id != '') {
                                                                $.ajax({
                                                                    url: '<?php echo base_url() . 'accessory/get_all_accessory' ?>',
                                                                    data: {product_id: product_id},
                                                                    type: 'post'
                                                                }).done(function(html) {
                                                                    $('#myModal').modal('show')
                                                                    $('.modal-body').html(html);
                                                                });
                                                            } else {
                                                                alert('Please Select Variant First');
                                                            }
                                                        })
                                                    });
                                                    document.getElementById('brand_id').value = '';
</script>
<style>
    #model_id
    {
        width: 340px !important;    
    }
    #all_product_name
    {
        width: 340px !important;    
    }
</style>