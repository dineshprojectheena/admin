<div class="page-content">
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                accessory depencies
            </small>
        </h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $error_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <?php if (isset($success_msg)): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $success_msg ?>
                    <br>
                </div>
            <?php endif; ?>
            <form class="form-horizontal" id="validation-form" enctype="multipart/form-data" role="form" method="post" action="<?php echo base_url().'accessory/upload_acc_dependency' ?>" id="create-feature" role="form" >
                <div class="form-group" >
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_id">Accessory Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" class="col-xs-10 col-sm-5 get_accessory" name="accessory_id" id="accessory_id" style="display:block;">
                        </div>
                        <?php echo form_error('accessory_id'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group" >
                    <label class="col-sm-3 control-label no-padding-right" for="dependent_accessory">Dependent Accessory Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" class="col-xs-10 col-sm-5 get_accessory" name="dependent_accessory" id="dependent_accessory" style="display:block;">
                        </div>
                        <?php echo form_error('dependent_accessory'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('type')) > 0) ? 'has-error' : '';?>";" >
                    <label class="col-sm-3 control-label no-padding-right" for="type">Dependent Accessory Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select class="col-xs-10 col-sm-5" id="type" name="type">
                                <option value="">Select Accessory Name</option>    
                                <option value="1">Positive</option>    
                                <option value="0">Negative</option>                                    
                            </select>    
                        </div>
                        <?php echo form_error('type'); ?>
                    </div>
                </div>

                <div class="space-4"></div>


                              


                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info">
                            <i class="icon-ok bigger-110"></i>
                            Submit
                        </button>
                        &nbsp; &nbsp; &nbsp;
                        <button class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            Reset
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?php if (isset($all_accessories)): ?>                                    
                <div class="table-header">
                    Results for "Accessory Depencies"
                </div>
                <?php
//                echo '<pre>';
//                print_r($all_accessories);
//                echo '</pre>';
                ?>
                <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Accessory</th>
                            <th>Dependant Accessory</th>
                            <th>Type</th>
                            <th></th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $i=1;
                        foreach($all_accessories as $a) : ?>
                            <tr>                                                
                                <td>
                                    <?php echo $i; ?>                                                    
                                </td>
                                <td>
                                    <?php echo $a ["accessory_name"]; ?>
                                </td>

                                <td>
                                    <?php echo $a["dependent_accessory_name"]; ?>
                                </td>
                                <td>
                                    <?php 
                                    if($a["dependency_type"]==1)
                                    {
                                    echo 'Positive';    
                                    }    
                                    else {
                                    echo 'Negative';        
                                    }
                                    ?>
                                </td>
                                <td>
                                    <form action="<?php echo base_url() . 'accessory/delete_acc_dependency'; ?>" method="POST">
                                        <input type="hidden" name="accessory_id"  value="<?php echo $a['id']; ?>" ?>                                        
                                        <button type="submit" class="btn btn-xs btn-danger delete-accessory"> <i class="icon-trash"></i></button>
                                    </form>
                                </td>
                                <td>
                                    
                                </td>

                            </tr>
                        <?php 
                        $i++;
                        endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
<script src="<?php echo base_url() . 'assets/js/modules/validate_map_depenency_accessory.js'; ?>"></script>
<script>
    function check_dep()
    {
        var select_acc = $('#select_acc').val();
        var result = select_acc.replace(/[^0-9;]/g, "-");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'accessory/check_accessory' ?>",
            data: {result: result}
        }).done(function(html) {
            alert(html);
        });       
    }


    jQuery(document).ready(function($) {
        var prepolulate_autocomplete;
<?php if (isset($prepopulate)): ?>
            prepolulate_autocomplete = [<?php echo $prepopulate ?>];
<?php else: ?>
            prepolulate_autocomplete = [];
<?php endif; ?>
        $(".get_accessory").tokenInput("<?php echo base_url() . 'accessory/get_accessory'; ?>", {
            theme: "facebook",
            searchingText: 'Searching..',
            minChars: 2,
            tokenLimit:1,
            preventDuplicates: true,
            prePopulate: prepolulate_autocomplete
        });

        $('.delete-accessory').click(function() {
            var accessory_id = $(this).data('accessory-id');
            var self = $(this);
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '<?php echo base_url() . 'accessory/delete_accessory' ?>',
                data: {accessory_id: accessory_id}
            }).done(function(html) {
                if (html.sts == 1) {
                    self.closest('tr').remove();
                } else {
                    self.closest('td').html(html.msg)
                }
            });
        });
    });
</script>