<div class="page-content">
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                accessory
            </small>
        </h1>
    </div>
    <script>
        $(document).ready(function() {
            $('.table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'accessory/all_accessory_result'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function(sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
        });
    </script>
    
    
    <div class="row">
        <div class="col-xs-12">
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $error_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <?php if (isset($success_msg)): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $success_msg ?>
                    <br>
                </div>
            <?php endif; ?>
            <form class="form-horizontal" id="validation-form" enctype="multipart/form-data" role="form" method="post" action="<?php echo base_url() . 'accessory/create_accessory' ?>" id="create-feature" role="form" >
                <div class="form-group <?php echo (strlen(form_error('accessory_name')) > 0) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_name">Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" name="accessory_name" value="<?php echo set_value('accessory_name'); ?>" class="col-xs-10 col-sm-5" />
                        </div>
                        <?php echo form_error('accessory_name'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <!--div class="form-group <?php echo (strlen(form_error('accessory_type')) > 0) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_type">Accessory Type</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="accessory_type" name="accessory_type" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('accessory_type', '1'); ?> value="1">Single</option>
                                <option <?php echo set_select('accessory_type', '2'); ?> value="2">Package</option>
                            </select>
                        </div>
                <?php echo form_error('accessory_type'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group dependencies <?php echo (strlen(form_error('positive_dependencies')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="positive_dependencies">Positive Dependency</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" name="positive_dependencies" class="get_accessory" />
                        </div>
                <?php echo form_error('positive_dependencies'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group dependencies <?php echo (strlen(form_error('negative_dependencies')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="negative_dependencies">Accessory Type</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" name="negative_dependencies" class="get_accessory" />
                        </div>
                <?php echo form_error('negative_dependencies'); ?>
                    </div>
                </div>

                <div class="space-4"></div-->

                <div class="form-group <?php echo (strlen(form_error('accessory_cat')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_cat">Accessory Category</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="accessory_cat" class="select2" name="accessory_cat" data-placeholder="Click to Choose...">

                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('accessory_type', '1'); ?> value="1">Exteriors</option>
                                <option <?php echo set_select('accessory_type', '2'); ?> value="2">Interiors</option>
                                <option <?php echo set_select('accessory_type', '3'); ?> value="3">Safety & Technology</option>
                                <option <?php echo set_select('accessory_cat', '4'); ?> value="4">Audio & Communication</option>
                                <!--<option <?php echo set_select('accessory_cat', '5'); ?> value="5">Services</option>-->
                                <option <?php echo set_select('accessory_cat', '6'); ?> value="6">Equipment Package</option>
                                <option <?php echo set_select('accessory_cat', '7'); ?> value="7">Is-Services</option>
                            </select>
                        </div>
                        <?php echo form_error('accessory_cat'); ?>
                    </div>
                </div>

                <div class="space-4"></div>
                <div class="form-group <?php echo (strlen(form_error('accessory_sub_cat')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_sub_cat">Accessory Sub Category</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="accessory_sub_cat" class="select2" name="accessory_sub_cat" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('accessory_sub_cat', '1'); ?> value="1">Headlights</option>
                                <option <?php echo set_select('accessory_sub_cat', '2'); ?> value="2">Sunroofs</option>
                                <option <?php echo set_select('accessory_sub_cat', '3'); ?> value="3">Tyres</option>
                                <option <?php echo set_select('accessory_sub_cat', '4'); ?> value="4">Steering Wheels</option>
                                <option <?php echo set_select('accessory_sub_cat', '5'); ?> value="5">Seats</option>
                                <option <?php echo set_select('accessory_sub_cat', '6'); ?> value="6">Navigation System</option>
                                <option <?php echo set_select('accessory_sub_cat', '7'); ?> value="7">Audio System</option>
                                <option <?php echo set_select('accessory_sub_cat', '8'); ?> value="8">Speakers</option>
                                <option <?php echo set_select('accessory_sub_cat', '9'); ?> value="9">Telephone</option>
                                <option <?php echo set_select('accessory_sub_cat', '10'); ?> value="10">Others</option>
                                <option <?php echo set_select('accessory_sub_cat', '11'); ?> value="11">Finance</option>
                                <option <?php echo set_select('accessory_sub_cat', '12'); ?> value="12">AMC Contract</option>
                                <option <?php echo set_select('accessory_sub_cat', '13'); ?> value="13">Insurance for car</option>
                                <option <?php echo set_select('accessory_sub_cat', '14'); ?> value="14">Extended Warranty</option>
                                <option <?php echo set_select('accessory_sub_cat', '15'); ?> value="15">Used Cars</option>
                                <option <?php echo set_select('accessory_sub_cat', '16'); ?> value="16">Home Delivery</option>

                            </select>
                        </div>
                        <?php echo form_error('accessory_sub_cat'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <!--div class="form-group <?php echo (strlen(form_error('accessory_price')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_price">Price</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="number" name="accessory_price" value="<?php echo set_value('accessory_price'); ?>" class="col-xs-10 col-sm-5" />
                        </div>
                <?php echo form_error('accessory_price'); ?>
                    </div>
                </div>

                <div class="space-4"></div-->

                <div class="form-group <?php echo (strlen(form_error('status')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="status" class="select2" name="status" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('status', '1', TRUE); ?> value="1">Active</option>
                                <option <?php echo set_select('status', '0'); ?> value="0">Inactive</option>
                            </select>
                        </div>
                        <?php echo form_error('status'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('accessory_information')) > 0 ) ? 'has-error' : ''; ?>">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_information">Accessory Information</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <textarea name="accessory_information" style="height:120px;" class="col-xs-10 col-sm-5"><?php echo set_value('accessory_information'); ?> </textarea>
                        </div>
                        <?php echo form_error('accessory_information'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('accessory_img')) > 0 ) ? 'has-error' : ''; ?>">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_img">Image</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="file" accept="image/*"  name="accessory_img" class="col-xs-10 col-sm-5" >
                        </div>                        
                    </div>
                </div>

                <div class="space-4"></div>


                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info">
                            <i class="icon-ok bigger-110"></i>
                            Submit
                        </button>

                        &nbsp; &nbsp; &nbsp;
                        <button class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            Reset
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">


        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Sr</th>
                    <th>Accessory Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table> 
        <!--        <div class="col-xs-12">
        <?php if (isset($all_accessories)): ?>                                    
                            <div class="table-header">
                                Results for "Features"
                            </div>
                        <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                        <table class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Accessory Name</th>
                                        <th>Description</th>
                                        <th></th>                            
                                    </tr>
                                </thead>
            
                                <tbody>
            <?php foreach ($all_accessories as $a) : ?>
                                            <tr>                                                
                                                <td>
                <?php echo $a['accessory_id']; ?>                                                    
                                                </td>
                                                <td>
                <?php echo $a ["accessory_name"]; ?>
                                                </td>
                
                                                <td>
                <?php echo $a["accessory_information"]; ?>
                                                </td>
                                                <td>
                                                    <form action="<?php echo base_url() . 'accessory/update_accessory'; ?>" method="POST">
                                                        <input type="hidden" name="accessory_id"  value="<?php echo $a['accessory_id']; ?>" ?>
                                                        <button type="submit" class="btn btn-xs btn-success"> <i class="icon-pencil"></i> </button>
                                                        <button type="button" class="btn btn-xs btn-danger delete-accessory" data-accessory-id="<?php echo $a['accessory_id']; ?>"> <i class="icon-trash"></i></button>
                                                    </form>
                                                </td> 
                                                <td>
                <?php //echo $a["accessory_information"]; ?>
                                                </td>
                                                
                                            </tr>
            <?php endforeach; ?>
                                </tbody>
                            </table>
        <?php endif; ?>
                </div>-->
    </div>
</div>
<script src="<?php echo base_url().'assets/js/modules/validate_accessory.js';?>"></script>
<script>
    jQuery(document).ready(function($) {
        var prepolulate_autocomplete;
<?php if (isset($prepopulate)): ?>
            prepolulate_autocomplete = [<?php echo $prepopulate ?>];
<?php else: ?>
            prepolulate_autocomplete = [];
<?php endif; ?>
        $(".get_accessory").tokenInput("<?php echo base_url() . 'accessory/get_accessory'; ?>", {
            theme: "facebook",
            searchingText: 'Searching..',
            minChars: 3,
            preventDuplicates: true,
            prePopulate: prepolulate_autocomplete
        });

        $('.delete-accessory').click(function() {
            var accessory_id = $(this).data('accessory-id');
            var self = $(this);
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '<?php echo base_url() . 'accessory/delete_accessory' ?>',
                data: {accessory_id: accessory_id}
            }).done(function(html) {
                if (html.sts == 1) {
                    self.closest('tr').remove();
                } else {
                    self.closest('td').html(html.msg)
                }
            });
        });
    });
</script>