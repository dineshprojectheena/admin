<div class="page-content">
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                accessory
            </small>
        </h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $error_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <?php if (isset($success_msg)): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $success_msg ?>
                    <br>
                </div>
            <?php endif; ?>
            <form class="form-horizontal" id="feature-form" enctype="multipart/form-data" role="form" method="post" action="<?php echo base_url() . 'accessory/check_accessory' ?>" id="create-feature" role="form" >
                <div class="form-group <?php echo (strlen(form_error('accessory_name')) > 0) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="accessory_name">Product Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" class="col-xs-10 col-sm-5" name="variant_id" id="product_name" style="display:block;">
                        </div>
                        <?php echo form_error('product_name'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('group_name')) > 0) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="group_name">Group Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" name="group_name" value="<?php echo set_value('group_name'); ?>" class="col-xs-10 col-sm-5" />
                        </div>
                        <?php echo form_error('group_name'); ?>
                    </div>
                </div>

                <div class="space-4"></div>



                <div class="form-group <?php echo (strlen(form_error('select_acc')) > 0 ) ? 'has-error' : ''; ?>">
                    <label class="col-sm-3 control-label no-padding-right" for="select_acc">Accessory</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" class="col-xs-10 col-sm-5 get_accessory" name="select_acc"  id="select_acc" style="display:block;">
                            <input type="text" class="col-xs-10 col-sm-5" name="select_acc2"  id="select_acc2" >
                        </div>
                        <?php echo form_error('select_acc'); ?>
                    </div>
                </div>

                <div class="space-4"></div>                


                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info" type="button" onclick="check_dep();">
                            <i class="icon-ok bigger-110"></i>
                            Submit
                        </button>
                        &nbsp; &nbsp; &nbsp;
                        <button class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            Reset
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?php if (isset($all_accessories)): ?>                                    
                <div class="table-header">
                    Results for "Features"
                </div>
                <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Accessory Name</th>
                            <th>Description</th>
                            <th></th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($all_accessories as $a) : ?>
                            <tr>                                                
                                <td>
                                    <?php echo $a['accessory_id']; ?>                                                    
                                </td>
                                <td>
                                    <?php echo $a ["accessory_name"]; ?>
                                </td>

                                <td>
                                    <?php echo $a["accessory_information"]; ?>
                                </td>
                                <td>
                                    <form action="<?php echo base_url() . 'accessory/update_accessory'; ?>" method="POST">
                                        <input type="hidden" name="accessory_id"  value="<?php echo $a['accessory_id']; ?>" ?>
                                        <button type="submit" class="btn btn-xs btn-success"> <i class="icon-pencil"></i> </button>
                                        <button type="button" class="btn btn-xs btn-danger delete-accessory" data-accessory-id="<?php echo $a['accessory_id']; ?>"> <i class="icon-trash"></i></button>
                                    </form>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
<script>
    function check_dep()
    {
        var select_acc = $('#select_acc').val();
        var result = select_acc.replace(/[^0-9;]/g, "-");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'accessory/check_accessory' ?>",
            data: {result: result}
        }).done(function(html) {
            alert(html);
        });       
    }


    jQuery(document).ready(function($) {
        var prepolulate_autocomplete;
<?php if (isset($prepopulate)): ?>
            prepolulate_autocomplete = [<?php echo $prepopulate ?>];
<?php else: ?>
            prepolulate_autocomplete = [];
<?php endif; ?>
        $(".get_accessory").tokenInput("<?php echo base_url() . 'accessory/get_accessory'; ?>", {
            theme: "facebook",
            searchingText: 'Searching..',
            minChars: 2,
            preventDuplicates: true,
            prePopulate: prepolulate_autocomplete
        });

        $('.delete-accessory').click(function() {
            var accessory_id = $(this).data('accessory-id');
            var self = $(this);
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '<?php echo base_url() . 'accessory/delete_accessory' ?>',
                data: {accessory_id: accessory_id}
            }).done(function(html) {
                if (html.sts == 1) {
                    self.closest('tr').remove();
                } else {
                    self.closest('td').html(html.msg)
                }
            });
        });
    });
</script>