<!-- main content data start here -->
<div class="page-content">
    <div class="page-header">
        <div class="table-header">
            MNC Latest Deals 
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <form action="<?php echo site_url("lastestdeal_upload"); ?>" id="validation-form" class="form-horizontal" method="POST">
                        <div class="span6">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs" id="myTab3">
                                    <li class="active">
                                        <a data-toggle="tab" href="#home3">
                                            <i class="pink icon-dashboard bigger-110"></i>
                                            Details
                                        </a>
                                    </li>                                    
                                </ul>
                                <?php
//                                print_r($single_res_detail);
                                foreach ($sing_res as $sing_res_data)
                                    
                                    ?>
                                <div class="tab-content">
                                    <div id="home3" class="tab-pane active">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                            <div class="col-sm-9" >
                                                <select name="brand_id" id="brand_id" onchange="getmodel();" style="width: 340px;">
                                                    <option  value="">Select Brand</option>
                                                    <?php
                                                    foreach ($brand as $brand_res) {
                                                        ?>
                                                        <option 
                                                        <?php
                                                        if ($single_res_detail[0]['brand_id'] == $brand_res['brand_id']) {
                                                            echo 'selected';
                                                        } else if ($_POST['brand_id'] == $brand_res['brand_id']) {
                                                            echo 'selected';
                                                        }
                                                        ?> value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                            <?php
                                                        }
                                                        ?>  
                                                </select>  
                                                <br> <span class="error"><?php echo form_error('brand_id'); ?></span>

                                            </div> 
                                        </div> 
                                        <?php
//                                                            print_r($single_res_detail);
                                        ?>
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                            <div class="col-sm-9" style="float:left;" id="model_res" >
                                                <select name="model_id" id="model_id" style="width: 340px;">
                                                    <option  value="">Select model</option>                                                                        
                                                    <?php
                                                    if (!empty($single_res_detail[0]['model_name'])) {
                                                        ?>
                                                        <option selected="selected" value="<?php echo $sing_res_data['model_id'] ?>"><?php echo $single_res_detail[0]['model_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>  
                                                <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                            </div>


                                        </div>
                                        <?php
//                                                            print_r($single_res_detail);
                                        ?>
                                        <div class="form-group" id="" >
                                            <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                            <div class="col-sm-9" style="float:left;" id="variant_results" >
                                                <select name="all_product_name" id="all_product_name">
                                                    <option  value="">Select Variant</option>
                                                    <?php
                                                    if (!empty($single_res_detail[0]['pro_name'])) {
                                                        ?>
                                                        <option selected="selected" value="<?php echo $sing_res_data['variant_id'] ?>"><?php echo $single_res_detail[0]['pro_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>  
                                                <input type="hidden" name="expert_rating_ids" id="expert_rating_ids" value="<?php
                                                    if ($get_res->expert_rating_id != '') {
                                                        echo $get_res->expert_rating_id;
                                                    } else if (isset($_POST['expert_rating_ids'])) {
                                                        echo $_POST['expert_rating_ids'];
                                                    }
                                                    ?>" class="col-xs-10 col-sm-5">                                                                
                                                <br> <span class="error"><?php echo form_error('product_name'); ?></span>
<!--                                                <input type="text" name="product_names" id="product_names" value="<?php
                                                       if (!isset($_POST['product_names'])) {
                                                           if ($sing_res_data['pro_name'] != '') {
                                                               echo $sing_res_data['pro_name'];
                                                           }
                                                       } else {
                                                           echo $_POST['product_names'];
                                                       }
                                                    ?>" class="col-xs-10 col-sm-5"/>    -->
                                            </div>

                                        </div>



                                        <!--                                        <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="product_name">Variant Name</label>
                                                                                    <div class="col-sm-9">
                                                                                        <div class="clearfix">
                                                                                            <input type="text" name="product_name" id="product_names" value="<?php
                                                if (!isset($_POST['product_name'])) {
                                                    if ($sing_res_data['pro_name'] != '') {
                                                        echo $sing_res_data['pro_name'];
                                                    }
                                                } else {
                                                    echo $_POST['product_name'];
                                                }
                                                    ?>" class="col-xs-10 col-sm-5"/>                                                                
                                                                                            <span class="error"><?php echo form_error('product_name'); ?></span>
                                                                                            <input type="hidden" name="deal_id" id="deal_id" 
                                                                                                   value="<?php
                                        if (!isset($_POST['deal_id'])) {
                                            if ($sing_res_data['deal_id'] != '') {
                                                echo $sing_res_data['deal_id'];
                                            }
                                        } else {
                                            echo $_POST['deal_id'];
                                        }
                                                    ?>" class="col-xs-10 col-sm-5"/> 
                                                                                        </div>
                                        
                                                                                    </div>
                                                                                </div>
                                                                                <div class="space-4"></div>   -->

                                        <!--                                        <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                                    <div class="col-sm-9">
                                                                                        <div class="clearfix">
                                                                                            <select name="brand_id" id="brand_id" class="col-xs-10 col-sm-5">
                                                                                                <option value="">Select Brand</option>    
                                        <?php
                                        foreach ($all_brand as $all_brand_res) {
                                            ?>
                                                                                                            <option <?php
                                        if ($_POST['brand_id'] == $all_brand_res['brand_id']) {
                                            echo 'selected';
                                        } else if ($sing_res_data['brand_id'] == $all_brand_res['brand_id']) {
                                            echo 'selected';
                                        }
                                            ?>
                                            <?php
                                            ?> value="<?php echo $all_brand_res['brand_id']; ?>"><?php echo $all_brand_res['brand_name']; ?></option>    
                                            <?php
                                        }
                                        ?>                                                        
                                                                                            </select>
                                                                                            <span class="error"><?php echo form_error('brand_id'); ?></span> 
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                        
                                                                                <div class="space-4"></div>   -->


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="city_id">City</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select name="city_id" id="city_id" class="col-xs-10 col-sm-5">
                                                        <option value="">Select City</option>    
                                                        <?php
                                                        foreach ($city as $city_res) {
                                                            ?>
                                                            <option <?php
                                                        if ($_POST['city_id'] == $city_res['city_id']) {
                                                            echo 'selected';
                                                        } else if ($sing_res_data['city_id'] == $city_res['city_id']) {
                                                            echo 'selected';
                                                        }
                                                            ?>
                                                            <?php
                                                            ?> value="<?php echo $city_res['city_id']; ?>"><?php echo $city_res['name']; ?></option>    
                                                                <?php
                                                            }
                                                            ?>                                                        
                                                    </select>
                                                    <span class="error"><?php echo form_error('city_id'); ?></span> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>                                        



                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="discount_type">Discount Type</label>
                                            <div class="col-xs-12 col-sm-9">

                                                <select name="discount_type" id="discount_type" class="col-xs-10 col-sm-5">
                                                    <option  value="">Select Discount Type</option>
                                                    <option <?php
                                                            if ($_POST['discount_type'] == 1) {
                                                                echo 'selected';
                                                            } else if ($sing_res_data['discount_type'] == 1) {
                                                                echo 'selected';
                                                            }
                                                            ?>
                                                        value="1">Percent</option>    
                                                    <option <?php
                                                    if ($_POST['discount_type'] == 2) {
                                                        echo 'selected';
                                                    } else if ($sing_res_data['discount_type'] == 2) {
                                                        echo 'selected';
                                                    }
                                                            ?> value="2">INR</option>    
                                                </select>  
                                                <span class="error"><?php echo form_error('discount_type'); ?></span>
                                            </div>

                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="amount_per">Amount/Percent</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" value="<?php
                                                    if (!isset($_POST['amount_per'])) {
                                                        if ($sing_res_data['amount_per'] != '') {
                                                            echo $sing_res_data['amount_per'];
                                                        }
                                                    } else {
                                                        echo $_POST['amount_per'];
                                                    }
                                                            ?>" name="amount_per" id="amount_per"  class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('amount_per'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="deals_heading">Deals Heading</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" value="<?php
                                                           if (!isset($_POST['deals_heading'])) {
                                                               if ($sing_res_data['deals_heading'] != '') {
                                                                   echo $sing_res_data['deals_heading'];
                                                               }
                                                           } else {
                                                               echo $_POST['deals_heading'];
                                                           }
                                                            ?>" name="deals_heading" id="deals_heading"  class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('deals_heading'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>





                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="start_date"> Start Date</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="start_date" 
                                                           value="<?php
                                                           if (!isset($_POST['start_date'])) {
                                                               if ($sing_res_data['start_date'] != '') {
                                                                   echo $sing_res_data['start_date'];
                                                               }
                                                           } else {
                                                               echo $_POST['start_date'];
                                                           }
                                                            ?>" id="start_date" data-date-format="yyyy-mm-dd" readonly="readonly" class="col-xs-10 col-sm-5 date-picker">                                                                
                                                    <span class="error"><?php echo form_error('start_date'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="end_date">End Date</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="end_date"
                                                           value="<?php
                                                           if (!isset($_POST['end_date'])) {
                                                               if ($sing_res_data['end_date'] != '') {
                                                                   echo $sing_res_data['end_date'];
                                                               }
                                                           } else {
                                                               echo $_POST['end_date'];
                                                           }
                                                            ?>" id="end_date" data-date-format="yyyy-mm-dd" readonly="readonly" class="col-xs-10 col-sm-5 date-picker">                                                                
                                                    <span class="error"><?php echo form_error('end_date'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>


                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>
                                            <div class="col-xs-12 col-sm-9">

                                                <select name="status" id="status" class="col-xs-10 col-sm-5">
                                                    <option  value="">Select Status</option>
                                                    <option <?php
                                                           if ($_POST['status'] == 1) {
                                                               echo 'selected';
                                                           } else if ($sing_res_data['status'] == 1) {
                                                               echo 'selected';
                                                           }
                                                            ?>  value="1">Enabled</option>    
                                                    <option <?php
                                                    if ($_POST['status'] == 2) {
                                                        echo 'selected';
                                                    } else if ($sing_res_data['status'] == 2) {
                                                        echo 'selected';
                                                    }
                                                            ?> value="2">Disabled</option>    
                                                </select>  
                                                <span class="error"><?php echo form_error('status'); ?></span>
                                            </div>

                                        </div>

                                        <div class="space-4"></div>


                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="term_condition">Term & Condition</label>
                                            <div class="col-xs-12 col-sm-9">
<!--                                                <textarea name="term_condition" id="term_condition" class="col-xs-10 col-sm-5"><?php
                                                    if (!isset($_POST['term_condition'])) {
                                                        if ($sing_res_data['term_condition'] != '') {
                                                            echo $sing_res_data['term_condition'];
                                                        }
                                                    } else {
                                                        echo $_POST['term_condition'];
                                                    }
                                                            ?></textarea>  -->
                                                <div class="wysiwyg-editor" id="editor1" name="details" onkeyup="passdata();"><?php
                                                if (!isset($_POST['term_condition'])) {
                                                    if ($sing_res_data['term_condition'] != '') {
                                                        echo $sing_res_data['term_condition'];
                                                    }
                                                } else {
                                                    echo $_POST['term_condition'];
                                                }
                                                            ?></div>
                                                <textarea style="display:none;" name="term_condition" id="term_condition"><?php
                                                    if (!isset($_POST['term_condition'])) {
                                                        if ($sing_res_data['term_condition'] != '') {
                                                            echo $sing_res_data['term_condition'];
                                                        }
                                                    } else {
                                                        echo $_POST['term_condition'];
                                                    }
                                                            ?>"</textarea>
                                                <script>
                                                    function passdata()
                                                    {
                                                        var editor1 = $('#editor1').html();
                                                        $('#term_condition').val(editor1);
                                                    }
                                                </script>

                                                <span class="error"><?php echo form_error('term_condition'); ?></span>
                                            </div>

                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="space-4"></div>
<?php
if (empty($sing_res)) {
    ?>
                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="submit" class="btn btn-info">                                                
                                        Submit
                                    </button>
                                    &nbsp; &nbsp; &nbsp;
                                    <button class="btn" type="reset">
                                        <i class="icon-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>
    <?php
} else {
    ?>
                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="update" class="btn btn-info">                                                
                                        Update
                                    </button>
                                </div>
                            </div> 
    <?php
}
?>  
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
<?php
if (!empty($all_res)) {
    ?>
                <div class="form-group">
                    <div class="table-header">
                        Results for "MNC Latest Deals"
                    </div>
                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Variant Name</th>
                                <th>City</th>
                                <th>Discount Type</th>
                                <th>Amount/Percent</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                                <th>Terms & Condition</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
    <?php
    $i = 1;
    foreach ($all_res as $all_res_data) {
        ?>                        
                                <tr>
                            <form action="<?php echo site_url("lastestdeal_upload"); ?>" class="form-horizontal" method="POST">
                                <td><?php echo $i++; ?></td>
                                <td>
        <?php echo $all_res_data['pro_name']; ?>
                                    <input type="hidden" name="deal_id" value="<?php echo $all_res_data['deal_id']; ?>" />
                                </td>
                                <td><?php echo $all_res_data['name']; ?></td>
                                <td><?php
                            if ($all_res_data['discount_type'] == 1) {
                                echo 'Percent';
                            } else {
                                echo 'INR';
                            }
        ?></td>
                                <td><?php echo $all_res_data['amount_per']; ?></td>
                                <td><?php echo $all_res_data['start_date']; ?></td>                            
                                <td><?php echo $all_res_data['end_date']; ?></td>                            

                                <td>
        <?php
        if ($all_res_data['status'] == 1) {
            echo 'Enable';
        } else {
            echo 'Disable';
        }
        ?>
                                </td>
                                <td><?php echo $all_res_data['term_condition']; ?></td>                            
                                <td class="align_tble">
                                    <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                        Edit
                                    </button>                                                        
                                </td>                    
                                <td>
                                    <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                        Delete
                                    </button>
                                </td> 
                            </form>
                            </tr>

        <?php
    }
    ?>

                        </tbody>
                    </table>
                </div>
    <?php
}
?>
        </div>     

    </div>
<?php
//    print_r($sing_res_data);
if (!empty($sing_res)) {
//        print_r($sing_res);
    $variant_id = $sing_res[0]['variant_id'];
    $pro_name = $sing_res[0]['pro_name'];
    $prepopulate = $data = '{id:"' . $variant_id . '",name:"' . $pro_name . '"}';
}
?>
    <script src="<?php echo base_url() . 'assets/js/modules/validate_latest_deal.js'; ?>"></script>
    <script>
        $(document).ready(function() {
            var prepolulate_autocomplete;
<?php if (isset($prepopulate)) { ?>
                prepolulate_autocomplete = [<?php echo $prepopulate ?>];
<?php } else { ?>
                prepolulate_autocomplete = [];
<?php } ?>

            $("#product_names").tokenInput("<?php echo base_url() . 'price/search_product'; ?>", {
                theme: "facebook",
                searchingText: 'Searching..',
                tokenLimit: 1,
                minChars: 3,
                prePopulate: prepolulate_autocomplete
            });
        });
    </script>

    <!-- main content data end here -->

</div><!-- /.main-content -->                

</div><!-- /.main-container-inner -->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

