<div class="page-content">
    <div class="page-header">
        <h1>
            Dealer
            <small>
                <i class="icon-double-angle-right"></i>
                Dealer Price Status
            </small>
        </h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="span6">
                <div id="recent-box" class="widget-box transparent">
                    <div class="widget-header">
                        <div class="widget-toolbar no-border">
                            <ul id="recent-tab" class="nav nav-tabs">
                                <li class="active">
                                    <a href="#task-tab" data-toggle="tab">Latest Price Status</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main padding-4">
                            <div class="tab-content padding-8 overflow-visible">
                                    <div class="tab-pane active" id="task-tab" >
                                     <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Dealer</th>
                                                <th>Upload Date</th>
                                                <th>No of records</th>                                                
                                                <th>Action</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
//                                            print_r($dealer_data);
                                            foreach($dealer_data as $dealer_data_res)
                                            {    
                                            ?>
                                            <tr>
                                                <td><?php echo $dealer_data_res['dealer_id'];?></td>                                                
                                                <td><?php echo $dealer_data_res['dealer_name'];?></td>                                                
                                                <td><?php echo $dealer_data_res['added_date'];?></td>                                                
                                                <td><?php echo $dealer_data_res['count'];?></td>                                                
                                                <td><a href="<?php echo base_url().'dealer_prices?supplier='.$dealer_data_res['dealer_id'];?>">View</a></td>
                                            </tr>
                                            <?php 
                                            }    
                                            ?>
                                        </tbody>
                                        <tfoot>
                                         <tr>
                                             <td align="right" colspan="5"><a href="<?php echo base_url().'dealer_prices?supplier='?>">View All</a></td>
                                            </tr>
                                               
                                        </tfoot>
                                    </table>

                                </div>

                            </div>
                        </div><!--/widget-main-->
                    </div><!--/widget-body-->
                </div><!--/widget-box-->
            </div>
            <?php
//            exit;
            ?>


            <div class="space-6"></div>

            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.page-content -->