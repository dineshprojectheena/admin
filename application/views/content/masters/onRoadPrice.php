<!-- main content data start here -->
<div class="page-content">
    <div class="page-header">
        <div class="table-header">
            On Road Price
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'price/onroad_result'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function(sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
        });
    </script>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <form id="validation-form" action="<?php echo site_url("onRoad_upload"); ?>" class="form-horizontal" method="POST">
                        <div class="span6">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs" id="myTab3">
                                    <li class="active">
                                        <a data-toggle="tab" href="#home3">
                                            <i class="pink icon-dashboard bigger-110"></i>
                                            Details
                                        </a>
                                    </li>
                                </ul>
                                <?php
                                foreach ($sing_res as $sing_res_data)
//                                echo '<pre>';
//                                print_r($single_res_detail);    
//                                echo '</pre>';
                                ?>
                                <div class="tab-content">
                                    <div id="home3" class="tab-pane active">


                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                            <div class="col-sm-9" >
                                                <select name="brand_id" id="brand_id" onchange="getmodel();" style="width: 340px;">
                                                    <option  value="">Select Brand</option>
                                                    <?php
                                                    foreach ($brand as $brand_res) {
                                                        ?>
                                                        <option value="<?php echo $brand_res['brand_id']; ?>"  <?php if($brand_res['brand_id']==$single_res_detail[0]['brand_id']) { echo 'selected';} ?>  ><?php echo $brand_res['brand_name']; ?></option>    
                                                        <?php
                                                        }
                                                        ?>  
                                                </select>  
                                                <br> <span class="error"><?php echo form_error('brand_id'); ?></span>
                                            </div> 
                                        </div> 
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                            <div class="col-sm-9" style="float:left;" id="model_res" >
                                                <select name="model_id" id="model_id" style="width: 340px;">
                                                    <option  value="">Select model</option>                                                                        
                                                    <?php
                                                    if (!empty($single_res_detail[0]['model_name'])) {
                                                        ?>
                                                        <option selected="selected" value="<?php echo $single_res_detail[0]['model_id'] ?>" ><?php echo $single_res_detail[0]['model_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>  
                                                <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                            </div>
                                        </div>
                                        <?php
//                                                            print_r($single_res_detail);
                                        ?>
                                        <div class="form-group" id="" >
                                            <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                            <div class="col-sm-9" style="float:left;" id="variant_results" >
                                                <select name="all_product_name" id="all_product_name">
                                                    <option  value="">Select Variant</option>
                                                    <?php
                                                    if (!empty($single_res_detail[0]['pro_name'])) {
                                                        ?>
                                                        <option selected="selected" value="<?php echo $single_res_detail[0]['variant_id'] ?>"><?php echo $single_res_detail[0]['pro_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>  
                                            </div>
                                        </div>


                                        <div class="space-4"></div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="city_id">City</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select name="city" id="city" class="col-xs-10 col-sm-5">
                                                        <option value="">Select City</option> 
                                                        <?php
                                                        foreach ($city as $city_data) {
                                                            ?>
                                                            <option <?php
                                                            if ($sing_res_data['city_id'] == $city_data['city_id']) {
                                                                echo 'selected';
                                                            }
                                                            ?> value="<?php echo $city_data['city_id'] ?>"><?php echo $city_data['name'] ?></option>    
                                                            </option> 
                                                            <?php
                                                        }
                                                        ?>                                                        
                                                    </select>
                                                    <input type="hidden" name="onroad_price_id" id="onroad_price_id" value="<?php
                                                    if ($sing_res_data['onroad_price_id'] != '') {
                                                        echo $sing_res_data['onroad_price_id'];
                                                    } else if (isset($_POST['onroad_price_id'])) {
                                                        echo $_POST['onroad_price_id'];
                                                    }
                                                    ?>" class="col-xs-10 col-sm-5"> 
                                                    <span class="error"><?php echo form_error('city'); ?></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="octroi">Local Body Tax (Octroi) </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="octroi" id="octroi" value="<?php
                                                    if ($sing_res_data['octroi'] != '') {
                                                        echo $sing_res_data['octroi'];
                                                    } else if (isset($_POST['octroi'])) {
                                                        echo $_POST['octroi'];
                                                    }
                                                    ?>" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('octroi'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="registeration_charges">RTO Tax & Registration Charges</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="registeration_charges" id="registeration_charges" value="<?php
                                                    if ($sing_res_data['registeration_charges'] != '') {
                                                        echo $sing_res_data['registeration_charges'];
                                                    } else if (isset($_POST['registeration_charges'])) {
                                                        echo $_POST['registeration_charges'];
                                                    }
                                                    ?>" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('registeration_charges'); ?></span>    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="handling_price">Handling Price</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="handling_price" id="handling_price" value="<?php
                                                    if ($sing_res_data['handling_price'] != '') {
                                                        echo $sing_res_data['handling_price'];
                                                    } else if (isset($_POST['handling_price'])) {
                                                        echo $_POST['handling_price'];
                                                    }
                                                    ?>" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('handling_price'); ?></span>    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="comp_insurance">Comprehensive Insurance</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="comp_insurance" id="comp_insurance" value="<?php
                                                    if ($sing_res_data['handling_price'] != '') {
                                                        echo $sing_res_data['handling_price'];
                                                    } else if (isset($_POST['handling_price'])) {
                                                        echo $_POST['handling_price'];
                                                    }
                                                    ?>" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('comp_insurance'); ?></span>    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>



                                        <div class="space-4"></div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="info_title">Other Charges</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="other_charges" id="other_charges" value="<?php
                                                    if ($sing_res_data['other_charges'] != '') {
                                                        echo $sing_res_data['other_charges'];
                                                    } else if (isset($_POST['other_charges'])) {
                                                        echo $_POST['other_charges'];
                                                    }
                                                    ?>" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('other_charges'); ?></span>    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                   

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="space-4"></div>
                        <?php
                        if (empty($sing_res)) {
                            ?>
                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="submit" class="btn btn-info">                                                
                                        Submit
                                    </button>
                                    &nbsp; &nbsp; &nbsp;
                                    <button class="btn" type="reset">
                                        <i class="icon-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="update" class="btn btn-info">                                                
                                        Update
                                    </button>
                                </div>
                            </div> 
                            <?php
                        }
                        ?>   
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product Name</th>
                    <th>City</th>
                    <th> Local Body Tax (Octroi)</th>
                    <th> RTO Tax & Registration Charges</th>
                    <th> Handling Price</th>
                    <th> Comprehensive Insurance</th>
                    <th> Other Charges</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>  
    </div>
    <?php
    if (!empty($sing_res)) {
        $variant_id = $sing_res[0]['product_id'];
        $pro_name = $sing_res[0]['pro_name'];
        $prepopulate = $data = '{id:"' . $variant_id . '",name:"' . $pro_name . '"}';
    }
    ?>
    <script src="<?php echo base_url().'assets/js/modules/validate_onroad_price.js';?>"></script>
    <!-- main content data end here -->

</div><!-- /.main-content -->                

</div><!-- /.main-container-inner -->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->
