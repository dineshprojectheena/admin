
<!-- main content data start here -->
<div class="page-content">
    <div class="page-header">
        <div class="table-header">
            Ex-ShowRoom Price
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <form action="<?php echo site_url("exShowroom_upload"); ?>" class="form-horizontal" method="POST">
                        <div class="span6">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs" id="myTab3">
                                    <li class="active">
                                        <a data-toggle="tab" href="#home3">
                                            <i class="pink icon-dashboard bigger-110"></i>
                                            Details
                                        </a>
                                    </li>                                    
                                </ul>
                                <?php
                                foreach ($sing_res as $sing_res_data)
                                print_r($sing_res_data);    
                                    
                                    
                                ?>
                                <div class="tab-content">
                                    <div id="home3" class="tab-pane active">
                                        <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="brand_id" id="brand_id" onchange="getmodel();" style="width: 340px;">
                                                                        <option  value="">Select Brand</option>
                                                                        <?php
                                                                        foreach ($brand as $brand_res) {
                                                                            ?>
                                                                            <option 
                                                                            <?php
                                                                            if ($results_up_copy[0]['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            } else if ($results_up[0]['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>   value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('brand_id'); ?></span>
                                                                </div> 
                                                            </div> 

                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                                                <div class="col-sm-9" style="float:left;" id="model_res" >
                                                                    <select name="model_id" id="model_id" style="width: 340px;">
                                                                        <option  value="">Select model</option>                                                                        
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                                                </div>


                                                            </div>

                                                            <div class="form-group" id="" >
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                                                <div class="col-sm-9" style="float:left;" id="variant_results" >
                                                                    <select name="all_product_name" id="all_product_name">
                                                                        <option  value="">Select Variant</option>
                                                                        <?php
                                                                        if(!empty($sing_res_data))
                                                                        {
                                                                        ?>
                                                                        <option selected="selected" value="<?php  echo $sing_res_data['product_id']?>"><?php  echo $sing_res_data['pro_name']?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('product_name'); ?></span>
                                                                </div>
                                                            </div> 
                                        
                                        
                                        <div class="form-group">
                                            <!--<label class="col-sm-3 control-label no-padding-right" for="product_name">Variant Name</label>-->
                                            <div class="col-sm-9">
                                                <div class="clearfix">
<!--                                                    <input type="text" name="product_name" id="product_names" 
                                                           value="<?php
                                                           if ($sing_res_data['pro_name'] != '') {
                                                               echo $sing_res_data['pro_name'];
                                                           } else if (isset($_POST['product_name'])) {
                                                               echo $_POST['product_name'];
                                                           }
                                                           ?>" class="col-xs-10 col-sm-5">                                                                -->
                                                    <input type="hidden" name="exshowroom_price_id" id="exshowroom_price_id" 
                                                           value="<?php
                                                           if ($sing_res_data['exshowroom_price_id'] != '') {
                                                               echo $sing_res_data['exshowroom_price_id'];
                                                           } else if (isset($_POST['exshowroom_price_id'])) {
                                                               echo $_POST['exshowroom_price_id'];
                                                           }
                                                           ?>" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('product_name'); ?></span>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="space-4"></div>   

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="city">City</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select name="city" id="city" class="col-xs-10 col-sm-5">
                                                        <option value="">Select City</option>    
                                                        <?php
                                                        foreach ($city as $city_res) {
                                                            ?>
                                                            <option <?php
                                                            if ($sing_res_data['city_id'] == $city_res['city_id']) {
                                                                echo 'selected';
                                                            }
                                                            ?> value="<?php echo $city_res['city_id']; ?>"><?php echo $city_res['name']; ?></option>    
                                                                <?php
                                                            }
                                                            ?>                                                        
                                                    </select>
                                                    <span class="error"><?php echo form_error('city'); ?></span> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="ex_price">Ex-Showroom Price</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" value="<?php
                                                    if (!isset($_POST['ex_price'])) {
                                                        if ($sing_res_data['exshowroom_price'] != '') {
                                                            echo $sing_res_data['exshowroom_price'];
                                                        }
                                                    } else if (isset($_POST['ex_price'])) {
                                                        echo $_POST['ex_price'];
                                                    }
                                                    ?>" name="ex_price" id="ex_price" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('ex_price'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="price_available">Price Applicable Since</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="price_available" readonly="readonly" style="width:335px;"  class="span10 date-picker" data-date-format="yyyy-mm-dd"
                                                           value="<?php
                                                           if (!isset($_POST['price_available'])) {
                                                               if ($sing_res_data['price_applicable_since'] != '') {
                                                                   echo $sing_res_data['price_applicable_since'];
                                                               }
                                                           } else if (isset($_POST['price_available'])) {
                                                               echo $_POST['price_available'];
                                                           }
                                                           ?>" id="price_available" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('price_available'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>


                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>
                                            <div class="col-xs-12 col-sm-9">
                                                <?php $status = $sing_res_data['status']; ?>
                                                <select name="status" id="status" class="col-xs-10 col-sm-5">
                                                    <option  value="">Select Status</option>
                                                    <option <?php
                                                    if (!isset($_POST['status'])) {
                                                        if ($status == 1) {
                                                            echo 'selected';
                                                        }
                                                    } else if (isset($_POST['status'])) {
                                                        if ($_POST['status'] == 1) {
                                                            echo 'selected';
                                                        }
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?> value="1">Enabled</option>    
                                                    <option <?php
                                                    if (!isset($_POST['status'])) {
                                                        if ($status == 2) {
                                                            echo 'selected';
                                                        }
                                                    } else if (isset($_POST['status'])) {
                                                        if ($_POST['status'] == 2) {
                                                            echo 'selected';
                                                        }
                                                    } else {
                                                        echo '';
                                                    }
                                                    ?> value="1">Disable</option>    
                                                </select>  
                                                <span class="error"><?php echo form_error('status'); ?></span>
                                            </div>

                                        </div>

                                        <div class="space-4"></div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="source">Source</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="source"
                                                           value="<?php
                                                           if (!isset($_POST['source'])) {
                                                               if ($sing_res_data['source'] != '') {
                                                                   echo $sing_res_data['source'];
                                                               }
                                                           } else if (isset($_POST['source'])) {
                                                               echo $_POST['source'];
                                                           }
                                                           ?>"
                                                           id="source" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('source'); ?></span>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="author">Author</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="author" value="<?php
                                                    if (!isset($_POST['author'])) {
                                                        if ($sing_res_data['author'] != '') {
                                                            echo $sing_res_data['author'];
                                                        }
                                                    } else if (isset($_POST['author'])) {
                                                        echo $_POST['author'];
                                                    }
                                                    ?>" id="author" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('author'); ?></span>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="space-4"></div>
                        <?php
                        if (empty($sing_res)) {
                            ?>
                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="submit" class="btn btn-info">                                                
                                        Submit
                                    </button>
                                    &nbsp; &nbsp; &nbsp;
                                    <button class="btn" type="reset">
                                        <i class="icon-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="update" class="btn btn-info">                                                
                                        Update
                                    </button>
                                </div>
                            </div> 
                            <?php
                        }
                        ?>  
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php
            if (!empty($all_res)) {
                ?>
                <div class="form-group">
                    <div class="table-header">
                        Results for "Ex-ShowRoom Price"
                    </div>
                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Variant Name</th>
                                <th>City</th>
                                <th>Ex-Showroom Price</th>
                                <th>Source </th>
                                <th>Author</th>
                                <th>Status</th>
                                <th>Added Date</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            $i = 1;
                            foreach ($all_res as $all_res_data) {
                                ?>
                                <tr>
                            <form action="<?php echo site_url("exShowroom_upload"); ?>" class="form-horizontal" method="POST">
                                <td><?php echo $i++; ?></td>
                                <td>
                                    <?php echo $all_res_data['pro_name']; ?>
                                    <input type="hidden" name="exshowroom_price_id" value="<?php echo $all_res_data['exshowroom_price_id']; ?>" />
                                </td>
                                <td><?php echo $all_res_data['name']; ?></td>
                                <td><?php echo $all_res_data['exshowroom_price']; ?></td>
                                <td><?php echo $all_res_data['source']; ?></td>
                                <td><?php echo $all_res_data['author']; ?></td>                            
                                <td>
                                    <?php
                                    if ($all_res_data['status'] == 1) {
                                        echo 'Enable';
                                    } else {
                                        echo 'Disable';
                                    }
                                    ?>
                                </td>
                                <td><?php echo substr($all_res_data['added_date'],0,10); ?></td>  
                                <td class="align_tble">
                                    <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                        Edit
                                    </button>                                                        
                                </td>                    
                                <td>
                                    <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                        Delete
                                    </button>
                                </td> 
                            </form>
                            </tr>

                            <?php
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <?php
            }
            ?>
        </div>     
    </div>
    <?php
    if(!empty($sing_res)){
//        print_r($sing_res);
        $variant_id = $sing_res[0]['product_id'];
        $pro_name = $sing_res[0]['pro_name'];
        $prepopulate = $data = '{id:"' . $variant_id . '",name:"' . $pro_name . '"}';
    }
    ?>

    <script>
        $(document).ready(function() {
            var prepolulate_autocomplete;
<?php if (isset($prepopulate)) { ?>
                prepolulate_autocomplete = [<?php echo $prepopulate ?>];
<?php } else { ?>
                prepolulate_autocomplete = [];
<?php } ?>

            $("#product_names").tokenInput("<?php echo base_url() . 'price/search_product'; ?>", {
                theme: "facebook",
                searchingText: 'Searching..',
                tokenLimit: 1,
                minChars: 3,
                prePopulate: prepolulate_autocomplete
            });
        });
     document.getElementById('brand_id').value = '';    
                </script>
                <style>
                    #model_id
                    {
                        width: 340px !important;    
                    }
                    #all_product_name
                    {
                        width: 340px !important;    
                    }
                </style>



    <!-- main content data end here -->

</div><!-- /.main-content -->                

</div><!-- /.main-container-inner -->

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

