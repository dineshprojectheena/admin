<!-- main content data start here-->
<div class="page-content">
    <div class="page-header">
        <div class="table-header">
            Variant Price 
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <script>
                                        $(document).ready(function(){
                                            $('.table').dataTable({
                                                "bProcessing": true,
                                                "bServerSide": true,
                                                "sAjaxSource": '<?php echo base_url().'price/mnc_result';?>',
                                                "aaSorting": [[4, "desc"]],
                                                "fnServerData": function(sSource, aoData, fnCallback) {
                                                    $.ajax({
                                                        "dataType": 'json',
                                                        "type": "POST",
                                                        "url": sSource,
                                                        "data": aoData,
                                                        "success": fnCallback
                                                    });
                                                }
                                            });
                                        });
                                    </script>
                    <form id="validation-form"  action="<?php echo site_url("mnc_upload"); ?>" class="form-horizontal" method="POST">
                        <div class="span6">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs" id="myTab3">
                                    <li class="active">
                                        <a data-toggle="tab" href="#home3">
                                            <i class="pink icon-dashboard bigger-110"></i>
                                            Details
                                        </a>
                                    </li>                                    
                                </ul>
                                <?php
//                                print_r($single_res_detail);
                                foreach ($sing_res as $sing_res_data)
//                                    echo '<pre>';
//                                    print_r($single_res_detail);
//                                    echo '</pre>';
                                    ?>
                                <div class="tab-content">
                                    <div id="home3" class="tab-pane active">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                            <div class="col-sm-9" >
                                                <select name="brand_id" id="brand_id" onchange="getmodel();" style="width: 340px;">
                                                    <option  value="">Select Brand</option>
                                                    <?php
                                                    foreach ($brand as $brand_res) {
                                                        ?>
                                                        <option value="<?php echo $brand_res['brand_id']; ?>"  <?php if($brand_res['brand_id']==$single_res_detail[0]['brand_id']) { echo 'selected';} ?>  ><?php echo $brand_res['brand_name']; ?></option>    
                                                        <?php
                                                        }
                                                        ?>  
                                                </select>  
                                                <br> <span class="error"><?php echo form_error('brand_id'); ?></span>
                                            </div> 
                                        </div> 
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                            <div class="col-sm-9" style="float:left;" id="model_res" >
                                                <select name="model_id" id="model_id" style="width: 340px;">
                                                    <option  value="">Select model</option>                                                                        
                                                    <?php
                                                    if (!empty($single_res_detail[0]['model_name'])) {
                                                        ?>
                                                        <option selected="selected" value="<?php echo $single_res_detail[0]['model_id'] ?>" ><?php echo $single_res_detail[0]['model_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>  
                                                <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                            <div class="col-sm-9" style="float:left;" id="variant_results" >
                                                <select name="all_product_name" id="all_product_name">
                                                    <option  value="">Select Variant</option>
                                                    <?php
                                                    if (!empty($single_res_detail[0]['pro_name'])) {
                                                        ?>
                                                        <option selected="selected" value="<?php echo $single_res_detail[0]['variant_id'] ?>"><?php echo $single_res_detail[0]['pro_name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>  
                                            </div>
                                        </div>

                                        <div class="space-4"></div>   

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="city">City</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select name="city" id="city" class="col-xs-10 col-sm-5">
                                                        <option value="">Select City</option>    
                                                        <?php
                                                        foreach ($city as $city_res) {
                                                            ?>
                                                            <option <?php
                                                        if ($sing_res_data['city_id'] == $city_res['city_id']) {
                                                            echo 'selected';
                                                        }
                                                            ?> value="<?php echo $city_res['city_id']; ?>"><?php echo $city_res['name']; ?></option>    
                                                                <?php
                                                            }
                                                            ?>                                                        
                                                    </select>

                                                    <input type="hidden" name="mnc_exshowroom_price_id" id="mnc_exshowroom_price_id" 
                                                           value="<?php
                                                            if ($sing_res_data['mnc_exshowroom_price_id'] != '') {
                                                                echo $sing_res_data['mnc_exshowroom_price_id'];
                                                            } else if (isset($_POST['exshowroom_price_id'])) {
                                                                echo $_POST['exshowroom_price_id'];
                                                            }
                                                            ?>" class="col-xs-10 col-sm-5">   
                                                    <input type="hidden" name="exshowroom_price_id" id="exshowroom_price_id" 
                                                           value="<?php
                                                           if ($sing_res_data['exshowroom_price_id'] != '') {
                                                               echo $sing_res_data['exshowroom_price_id'];
                                                           } else if (isset($_POST['exshowroom_price_id'])) {
                                                               echo $_POST['exshowroom_price_id'];
                                                           }
                                                            ?>" class="col-xs-10 col-sm-5">  

                                                    <span class="error"><?php echo form_error('city'); ?></span> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>
                                            <div class="col-xs-12 col-sm-9">

                                                <select name="status" id="status" class="col-xs-10 col-sm-5">
                                                    <option  value="">Select Status</option>
                                                    <option <?php
                                                           if ($sing_res_data['status'] == 1) {
                                                               echo 'selected';
                                                           }
                                                            ?>  value="1">Enabled</option>    
                                                    <option <?php
                                                    if ($sing_res_data['status'] == 2) {
                                                        echo 'selected';
                                                    }
                                                            ?> value="2">Disabled</option>    
                                                </select>  
                                                <span class="error"><?php echo form_error('status'); ?></span>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="authors"> Author</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="authors"
                                                           value="<?php
                                                    if ($sing_res_data['authors'] != '') {
                                                        echo $sing_res_data['authors'];
                                                    } else if (isset($_POST['authors'])) {
                                                        echo $_POST['authors'];
                                                    }
                                                            ?>" id="authors" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('authors'); ?></span>    
                                                </div>

                                            </div>
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="delivery_date">Delivery date</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="delivery_date" readonly="readonly" style="width:335px;" class="span10 date-picker" data-date-format="yyyy-mm-dd"
                                                           value="<?php
                                                           if (!isset($_POST['delivery_date'])) {
                                                               if ($sing_res_data['delivery_date'] != '') {
                                                                   echo $sing_res_data['delivery_date'];
                                                               }
                                                           } else if (isset($_POST['delivery_date'])) {
                                                               echo $_POST['delivery_date'];
                                                           }
                                                            ?>" id="delivery_date" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('delivery_date'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>


                                        <div class="space-4"></div>
                                        <h4 class="header blue bolder smaller">MNC Exshowroom Price</h4>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="mnc_exshowroom_price"> MNC Ex-Showroom Price</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" value="<?php
                                                           if ($sing_res_data['mnc_exshowroom_price'] != '') {
                                                               echo $sing_res_data['mnc_exshowroom_price'];
                                                           } else if (isset($_POST['mnc_exshowroom_price'])) {
                                                               echo $_POST['mnc_exshowroom_price'];
                                                           }
                                                            ?>" name="mnc_exshowroom_price" id="mnc_exshowroom_price" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('mnc_exshowroom_price'); ?></span>    
                                                </div>

                                            </div>
                                        </div>
                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="discount_percent"> Discount Type</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select name="discount_type" id="discount_type" class="col-xs-10 col-sm-5" onchange="change_type();">
                                                        <option value="">Select Discount Type</option>    
                                                        <option value="1">Amount</option>    
                                                        <option value="2">Percent</option>    
                                                    </select>                                                               
                                                    <script>
                                                        function change_type()
                                                        {
                                                            var discount_type = $('#discount_type').val();
                                                            if (discount_type == 1)
                                                            {
                                                                $('#dis_per').css("display", "none");
                                                                $('#dis_rs').css("display", "block");
                                                            }
                                                            else if (discount_type == 2)
                                                            {
                                                                $('#dis_per').css("display", "block");
                                                                $('#dis_rs').css("display", "none");
                                                            }
                                                            else
                                                            {
                                                                $('#dis_per').css("display", "none");
                                                                $('#dis_rs').css("display", "none");
                                                            }
                                                        }
                                                    </script>
                                                    <span class="error"><?php echo form_error('discount_percent'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group" style="display:none;" id="dis_per">
                                            <label class="col-sm-3 control-label no-padding-right" for="discount_percent"> Discount Percent</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="discount_percent"
                                                           value="<?php
                                                           if ($sing_res_data['discount_percent'] != '') {
                                                               echo $sing_res_data['discount_percent'];
                                                           } else if (isset($_POST['discount_percent'])) {
                                                               echo $_POST['discount_percent'];
                                                           }
                                                            ?>" id="discount_percent" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('discount_percent'); ?></span>    
                                                </div>
                                            </div>

                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group" style="display:none;" id="dis_rs">
                                            <label class="col-sm-3 control-label no-padding-right" for="discount_rs"> Discount Rupees</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="discount_rs"
                                                           value="<?php
                                                           if ($sing_res_data['discount_rs'] != '') {
                                                               echo $sing_res_data['discount_rs'];
                                                           } else if (isset($_POST['discount_rs'])) {
                                                               echo $_POST['discount_rs'];
                                                           }
                                                            ?>" id="discount_rs" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('discount_rs'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>






                                        <h4 class="header blue bolder smaller">Ex-ShowRoom Price </h4>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="ex_price">Ex-Showroom Price</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" value="<?php
                                                           if (!isset($_POST['ex_price'])) {
                                                               if ($sing_res_data['exshowroom_price'] != '') {
                                                                   echo $sing_res_data['exshowroom_price'];
                                                               }
                                                           } else if (isset($_POST['ex_price'])) {
                                                               echo $_POST['ex_price'];
                                                           }
                                                            ?>" name="ex_price" id="ex_price" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('ex_price'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="price_available">Price Applicable Since</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="price_available" readonly="readonly" style="width:335px;"  class="span10 date-picker" data-date-format="yyyy-mm-dd"
                                                           value="<?php
                                                           if (!isset($_POST['price_available'])) {
                                                               if ($sing_res_data['price_applicable_since'] != '') {
                                                                   echo $sing_res_data['price_applicable_since'];
                                                               }
                                                           } else if (isset($_POST['price_available'])) {
                                                               echo $_POST['price_available'];
                                                           }
                                                            ?>" id="price_available" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('price_available'); ?></span>    
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>



                                        <div class="space-4"></div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="source">Source</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" name="source"
                                                           value="<?php
                                        if (!isset($_POST['source'])) {
                                            if ($sing_res_data['source'] != '') {
                                                echo $sing_res_data['source'];
                                            }
                                        } else if (isset($_POST['source'])) {
                                            echo $_POST['source'];
                                        }
                                        ?>"
                                                           id="source" value="" class="col-xs-10 col-sm-5">                                                                
                                                    <span class="error"><?php echo form_error('source'); ?></span>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="space-4"></div>







                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="space-4"></div>
                        <?php
                        if (empty($sing_res)) {
                            ?>
                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="submit" class="btn btn-info">                                                
                                        Submit
                                    </button>
                                    &nbsp; &nbsp; &nbsp;
                                    <button class="btn" type="reset">
                                        <i class="icon-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" name="update" class="btn btn-info">                                                
                                        Update
                                    </button>
                                </div>
                            </div> 
                            <?php
                        }
                        ?>  
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Variant Name</th>
                    <th>City</th>
                    <th>MNC Ex-ShowRoom Price</th>
                    <th>Discount Percent</th>
                    <th>Discount Rupee</th>
                    <th>Status</th>
                    <th>Added Date</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>  
    </div>
    <script src="<?php echo base_url().'assets/js/modules/validate_product_price.js' ?>" ></script>
    
</div>
</div>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
</div>
