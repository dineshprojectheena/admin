<div class="page-content">
    <div class="page-header">
        <h1>
            Dashboard 
            <small>
                <i class="icon-double-angle-right"></i>
                overview &amp; stats
            </small>
        </h1>
    </div>
    <script>
        $(document).ready(function () {
            $('#car_record').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'stitcher/datatables_allUsers'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
            $('#accessory_record').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'stitcher/datatables_accessory'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
            $('#upcoming_record').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'stitcher/datatables_upcoming'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
        });
    </script>
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-block alert-success">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                </button>

                <i class="icon-ok green"></i>

                Welcome to
                <strong class="green">
                    MNC 
                </strong>
                Admin Panel
            </div>


            <div class="row-fluid">
                <h4 class="lighter smaller">
                    <i class="icon-rss orange"></i>
                    Latest Statics
                </h4>
                <hr>
                <div class="tabbable">
                    <?php
                    $max_days=date('t');
                    ?>
                    <ul class="nav nav-tabs" id="myTab">
                        <li <?php if($_GET['id']==1) {?>class="active"<?php }?>>
                            <a href="<?php echo base_url().'home/?id=1'?>">

                                <i class="green ace-icon fa fa-home bigger-120"></i>
                                1 Day
                                <?php if($_GET['id']==1) {?>
                                <span class="badge badge-danger">Latest</span>
                                <?php }?>
                            </a>
                        </li>

                        <li <?php if($_GET['id']==7) {?>class="active"<?php }?>>
                            <a href="<?php echo base_url().'home/?id=7'?>">
                                7 Days
                            <?php if($_GET['id']==7) {?>
                                <span class="badge badge-danger">Latest</span>
                                <?php }?>    
                            </a>
                        </li>
                        <li <?php if($_GET['id']==$max_days) {?>class="active"<?php }?>>
                            <a href="<?php echo base_url().'home/?id='.$max_days?>">
                                <?php echo $max_days;?> Days
                            <?php if($_GET['id']==$max_days) {?>
                                <span class="badge badge-danger">Latest</span>
                                <?php }?>    
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="1days" class="tab-pane fade active in">
                            <div class="span7 infobox-container">
                                <div class="infobox infobox-pink">
                                    <div class="infobox-icon">
                                        <i class="icon-shopping-cart"></i>
                                    </div>
                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $car_exchange['car_exchange']; ?></span>
                                        <div class="infobox-content">New Car Exchange</div>
                                    </div>
                                </div>

                                <div class="infobox infobox-green">
                                    <div class="infobox-icon">
                                        <i class="icon-comments"></i>
                                    </div>
                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $new_testdrive['new_testdrive']; ?></span>
                                        <div class="infobox-content">New Book Test Drive</div>
                                    </div>

                                </div>

                                <div class="infobox infobox-blue  ">
                                    <div class="infobox-icon">
                                        <i class="icon-twitter"></i>
                                    </div>

                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $car_lease['car_lease']; ?></span>
                                        <div class="infobox-content">New Car Lease</div>
                                    </div>                        
                                </div>

                                <div class="infobox infobox-blue">
                                    <div class="infobox-icon">
                                        <i class="icon-beaker"></i>
                                    </div>
                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $car_upcoming_car_enquiry['car_upcoming_enquiry']; ?></span>
                                        <div class="infobox-content">New Upcoming Enquiry</div>
                                    </div>                        
                                </div>

                                <div class="infobox infobox-pink">
                                    <div class="infobox-icon">
                                        <i class="icon-shopping-cart"></i>
                                    </div>
                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $contact_us['contact_us']; ?></span>
                                        <div class="infobox-content">New Contact Us</div>
                                    </div>

                                </div>

                            </div>
                            <hr>
                            <div class="span7 infobox-container">
                                <div class="infobox infobox-pink">
                                    <div class="infobox-icon">
                                        <i class="icon-shopping-cart"></i>
                                    </div>
                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $new_user['new_user']; ?></span>
                                        <div class="infobox-content">New User Registered</div>
                                    </div>                        
                                </div>

                                <div class="infobox infobox-green">
                                    <div class="infobox-icon">
                                        <i class="icon-comments"></i>
                                    </div>
                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $car_confiure['car_confiure']; ?></span>
                                        <div class="infobox-content">New Car Configured</div>
                                    </div>

                                </div>

                                <div class="infobox infobox-blue  ">
                                    <div class="infobox-icon">
                                        <i class="icon-twitter"></i>
                                    </div>

                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $new_order['total']; ?></span>
                                        <div class="infobox-content">New Car Orders</div>
                                    </div>                        
                                </div>

                                <div class="infobox infobox-blue">
                                    <div class="infobox-icon">
                                        <i class="icon-beaker"></i>
                                    </div>
                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $new_accessory['new_accessory']; ?></span>
                                        <div class="infobox-content">New Accessory Sale</div>
                                    </div>                        
                                </div>

                                <div class="infobox infobox-pink">
                                    <div class="infobox-icon">
                                        <i class="icon-shopping-cart"></i>
                                    </div>
                                    <div class="infobox-data">
                                        <span class="infobox-data-number"><?php echo $car_upcoming_sale['car_upcoming_sale']; ?></span>
                                        <div class="infobox-content">New Upcoming Sale </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div id="7days" class="tab-pane fade">
                            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
                        </div>

                        <div id="30days" class="tab-pane fade">
                            <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
                        </div>
                    </div>
                </div>


            </div>

        </div>

        <div class="vspace"></div>
    </div>
    <div class="row">
        <div class="space-6"></div>

        <div class="col-md-6 infobox-container">
            <div class="col-md-8">
                <div class="widget-box" style="display:none;">
                    <div class="widget-header widget-header-flat widget-header-small">
                        <h5 class="pull-left">
                            <i class="icon-signal"></i>
                            <a href="<?php echo base_url() . 'test-drive' ?>">Test Drive</a>
                        </h5>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="clearfix">
                                <div class="infobox infobox-green infobox-small infobox-dark"> 
                                    <div class="infobox-progress"> 
                                        <div class="easy-pie-chart percentage easyPieChart" data-percent="61" data-size="39" style="width: 39px; height: 39px; line-height: 39px;"> 
                                            <span class="percent infobox-content"><?php echo $test_drive['count'] ?></span>
                                        </div>
                                    </div> 
                                    <div class="infobox-data"> 
                                        <div class="infobox-content">Total</div>
                                        <div class="infobox-content">Test Drive</div> 
                                    </div> 
                                </div>
                                <div class="infobox infobox-blue infobox-small infobox-dark">
                                    <div class="infobox-chart">
                                        <div class="easy-pie-chart percentage easyPieChart" data-percent="61" data-size="39" style="width: 39px; height: 39px; line-height: 39px;"> 
                                            <span class="percent infobox-content"><?php echo $test_success['success_test'] ?></span>
                                        </div>
                                    </div>
                                    <div class="infobox-data">
                                        <div class="infobox-content">Completed</div>
                                        <div class="infobox-content">Test Drive</div>
                                    </div>
                                </div>
                                <div class="infobox infobox-grey infobox-small infobox-dark">
                                    <!--                                            <div class="infobox-icon"> 
                                                                                    <i class="icon-download-alt"></i>
                                                                                </div>-->
                                    <div class="infobox-chart">
                                        <div class="easy-pie-chart percentage easyPieChart" data-percent="61" data-size="39" style="width: 39px; height: 39px; line-height: 39px;"> 
                                            <span class="percent infobox-content"><?php echo $test_fail['pending_test'] ?></span>
                                        </div>
                                    </div>
                                    <div class="infobox-data"> 
                                        <div class="infobox-content">Pending</div>
                                        <div class="infobox-content">Testdrive</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="vspace-sm"></div>


    </div>
    <div class="span6">
        <div id="recent-box" class="widget-box transparent">
            <div class="widget-header">
                <h4 class="lighter smaller">
                    <i class="icon-rss orange"></i>
                    Latest Orders 
                </h4>

                <div class="widget-toolbar no-border">
                    <ul id="recent-tab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#task-tab" data-toggle="tab">Car Orders</a>
                        </li>
                        <li>
                            <a href="#task-tab2" data-toggle="tab">Accessory Orders</a>
                        </li>
                        <li>
                            <a href="#task-tab3" data-toggle="tab">Upcoming Car Orders</a>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main padding-4">
                    <div class="tab-content padding-8 overflow-visible">
                        <div class="tab-pane active" id="task-tab">
                            <table class="table table-striped table-bordered table-hover" id="car_record">
                                <thead>
                                    <tr>
                                        <th>Order Id</th>
                                        <th>Product</th>
                                        <th>Customer</th>
                                        <th>Email Id</th>
                                        <th>Affiliate Code</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
<!--                                                <th>Download</th>-->

                                    </tr>
                                </thead>
                            </table>

                        </div>

                        <div class="tab-pane" id="task-tab2">
                            <table class="table table-striped table-bordered table-hover" id="accessory_record">
                                <thead>
                                    <tr>
                                        <th>Accessory Order Id</th>
                                        <th>Invoice No</th>
                                        <th>Customer</th>
                                        <th>Email Id</th>
                                        <!--<th>No of Accessory</th>-->
                                        <th>Date Added</th>                                                
                                        <th>Action</th>                                                
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="tab-pane" id="task-tab3">
                            <table class="table table-striped table-bordered table-hover" id="upcoming_record">
                                <thead>
                                    <tr>
                                        <th>Upcoming Order Id</th>
                                        <th>Invoice No</th>
                                        <th>Customer</th>
                                        <th>Email Id</th>
                                        <!--<th>No of Accessory</th>-->
                                        <th>Date Added</th>                                                
                                        <th>Action</th>                                                
                                    </tr>
                                </thead>
                            </table>
                        </div>


                    </div>
                </div><!--/widget-main-->
            </div><!--/widget-body-->
        </div><!--/widget-box-->
    </div>
    <?php
//            exit;
    ?>


    <div class="space-6"></div>

    <!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->