<div class="page-content">

    <!-- /section:settings.box -->
    <div class="page-header">
        <h1>
            Dealer Document Details

        </h1>
    </div><!-- /.page-header -->

    <div class="row">
        <div class="col-xs-12">

            <div class="">
                <div id="user-profile-2" class="user-profile">
                    <div class="tabbable">
                        <ul class="nav nav-tabs padding-18">
                            <!--                            <li class="active">
                                                            <a data-toggle="tab" href="#home" aria-expanded="true">
                                                                <i class="green ace-icon fa fa-user bigger-120"></i>
                                                                Dealer Details
                                                            </a>
                                                        </li>-->

                            <li class="active">
                                <a data-toggle="tab" href="#pictures" aria-expanded="false">
                                    <i class="pink ace-icon fa fa-picture-o bigger-120"></i>
                                    Dealer Documents
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content no-border padding-24">
                            <div id="home" class="tab-pane">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 center">
                                        <span class="profile-picture">
                                            <img class="editable img-responsive" alt="Alex's Avatar" id="avatar2" src="../assets/avatars/profile-pic.jpg">
                                        </span>

                                        <div class="space space-4"></div>


                                    </div><!-- /.col -->

                                    <div class="col-xs-12 col-sm-9">
                                        <h4 class="blue">
                                            <span class="middle">Alex M. Doe</span>

                                            <span class="label label-purple arrowed-in-right">
                                                <i class="ace-icon fa fa-circle smaller-80 align-middle"></i>
                                                online
                                            </span>
                                        </h4>

                                        <div class="profile-user-info">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Username </div>

                                                <div class="profile-info-value">
                                                    <span>alexdoe</span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Location </div>

                                                <div class="profile-info-value">
                                                    <i class="fa fa-map-marker light-orange bigger-110"></i>
                                                    <span>Netherlands</span>
                                                    <span>Amsterdam</span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Age </div>

                                                <div class="profile-info-value">
                                                    <span>38</span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Joined </div>

                                                <div class="profile-info-value">
                                                    <span>2010/06/20</span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Last Online </div>

                                                <div class="profile-info-value">
                                                    <span>3 hours ago</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="hr hr-8 dotted"></div>

                                        <div class="profile-user-info">
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Website </div>

                                                <div class="profile-info-value">
                                                    <a href="#" target="_blank">www.alexdoe.com</a>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    <i class="middle ace-icon fa fa-facebook-square bigger-150 blue"></i>
                                                </div>

                                                <div class="profile-info-value">
                                                    <a href="#">Find me on Facebook</a>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name">
                                                    <i class="middle ace-icon fa fa-twitter-square bigger-150 light-blue"></i>
                                                </div>

                                                <div class="profile-info-value">
                                                    <a href="#">Follow me on Twitter</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.col -->
                                </div><!-- /.row -->


                            </div><!-- /#home -->


                            <div id="pictures" class="tab-pane active">
                                <ul class="ace-thumbnails">
                                    <?php
                                    $i = 1;
//                                    print_r($document);
                                    foreach ($document as $k => $v) {
                                        $new_status = '0';
                                        $link = dealer_url() . 'uploads/dealer/' . $v;
//                                        echo '<br>';
                                        if ($k == 'mou_pic') {
                                            $doc = 'MOU Document';
                                        }
                                        if ($k == 'agreement_pic') {
                                            $doc = 'Agreement Document';
                                        }
                                        if ($k == 'comission_pic') {
                                            $doc = 'Commission Document';
                                        }
                                        if ($k == 'process_flow_chart_pic') {
                                            $doc = 'Process Flow Document';
                                        }
                                        if ($k == 'dealer_location_map_pic') {
                                            $doc = 'Dealer Location Map Document';
                                        }
                                        $display = 'block';
                                        if($k == 'dealer_id') {
                                            $display = 'none';
                                            $dealer_main_id = $v;
                                        }
                                        if($v=='')
                                        {
                                        $display = 'none';    
                                        }    
                                        
//                                            echo '<pre>';
                                        foreach ($document_status as $document_status_data) {
                                            if ($document_status_data['file_type'] == $k) {
                                                $document_status_data['file_type'];
                                                $new_status = $document_status_data['status'];
                                            }
                                        }
                                        ?>
                                        <li style="display:<?php echo $display; ?>">
                                            <a class="documetns" href="<?php echo $link; ?>" title="<?php echo $doc; ?>" data-rel="colorbox">
                                                <img width="150" height="150" alt="<?php echo $doc; ?>" src="<?php echo $link; ?>" />
                                            </a>
                                            <div class="text center" aling="center">
                                                <div class="inner"><?php echo $doc; ?></div>
                                            </div>
                                            <div class="text center" aling="center">
                                                <select class="form-control" id="dealer_document" onchange="update_status(this.value, '<?php echo $dealer_main_id; ?>', '<?php echo $k; ?>');">
                                                    <option  <?php
                                                    if ($new_status == 0) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="0">Select Status</option>
                                                    <option <?php
                                                    if ($new_status == 1) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="1">Approve</option>
                                                    <option <?php
                                                    if ($new_status == 2) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="2">Reject</option>
                                                </select>
                                            </div>

                                        </li>



                                        <!--<br>-->
                                        <!--                                        <li>
                                                                                    <a href="<?php echo $link; ?>" title="Photo Title" data-rel="colorbox" class="cboxElement">
                                                                                        <img width="150" height="150" alt="150x150" src="<?php echo $link; ?>">
                                                                                    </a>
                                                                                    
                                                                                </li>-->
    <?php
}
?>


                                </ul>
                            </div><!-- /#pictures -->
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function update_status(id, dealer_id, file)
                {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>dealer_prices/approve_document",
                        data: {id: id, dealer_id: dealer_id, file: file}
                    }).done(function (html) {
                        alert(html);
//                document.cookie = 'ref_id=' + html + ';' + expires + ';path=/';
//                $("#divResult").html(html).show();
                    });
                }
            </script>


            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>