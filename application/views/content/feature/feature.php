<?php

/**
 * @author Nikhil Kataria <nikhil@projectheena.com>
 * @param type $feature_id
 */
function get_feature_type_name($feature_id){
    $feature_type_label = NULL;
    switch ($feature_id) {
        case 1:
            $feature_type_label = 'Exteriors';
            break;
        case 2:
            $feature_type_label = 'Interiors';
            break;
        case 3:
            $feature_type_label = 'Comfort';
            break;
        case 4:
            $feature_type_label = 'Safety Security';
            break;
        case 5:
            $feature_type_label = 'Audio Communication';
            break;
        case 5:
            $feature_type_label = 'Instrument Panel';
            break;
        default:
            break;
    }
    return $feature_type_label;
}
?>
<div class="page-content">
    <script>
        $(document).ready(function() {
            $('.table').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '<?php echo base_url() . 'feature/feature_result'; ?>',
                "aaSorting": [[4, "desc"]],
                "fnServerData": function(sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "type": "POST",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                }
            });
        });
    </script>
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                features
            </small>
        </h1>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php if (isset($error_msg)): ?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $error_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <?php if (isset($success_msg)): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $success_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <form class="form-horizontal" id="validation-form" method="post" enctype="multipart/form-data" role="form" action="<?php echo base_url() . 'feature/create_feature' ?>" role="form" >

                <div style="display: block;" class="form-group <?php echo (strlen( form_error('feature_type') ) > 0 )? 'has-error'  : '' ; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_type">Featured Type</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="car_type" class="select2" name="feature_type" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('feature_type', '1'); ?> value="1">Exteriors</option>
                                <option <?php echo set_select('feature_type', '2'); ?> value="2">Interiors</option>
                                <option <?php echo set_select('feature_type', '3'); ?> value="3">Comfort & Convenienc</option>
                                <option <?php echo set_select('feature_type', '4'); ?> value="4">Safety</option>
                                <option <?php echo set_select('feature_type', '5'); ?> value="5">Entertainment & Communication</option>
                                <option <?php echo set_select('feature_type', '6'); ?> value="6">Service</option>
                                <option <?php echo set_select('feature_type', '7'); ?> value="7">Other</option>
                            </select>
                        </div>
                        <?php echo form_error('feature_type'); ?>
                    </div>
                </div>    
                  
                
                <div style="display: block;" class="form-group <?php echo (strlen(form_error('featured_type_id')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_name">Feature Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="feature_name" name="feature_name" data-placeholder="Click to Choose..." style="width:230px;">
                                <option value="">&nbsp;</option>
                                <?php
                                foreach ($features_sub_type as $features_sub_type_data) {
                                    ?>
                                    <option <?php echo set_select('feature_name', $features_sub_type_data['featured_sub_type']); ?> value="<?php echo $features_sub_type_data['featured_sub_type']; ?>"><?php echo $features_sub_type_data['featured_sub_type']; ?></option>                                
                                    <?php
                                }
                                ?>                                
                            </select>
                        </div>
                        <?php echo form_error('feature_name'); ?>
                    </div>
                </div>               


<!--                <div class="form-group <?php echo (strlen(form_error('feature_name')) > 0 ) ? 'has-error' : ''; ?>"">
    <label class="col-sm-3 control-label no-padding-right" for="feature_name">Feature Name</label>
    <div class="col-sm-9">
        <div class="clearfix">
            <input type="text" name="feature_name" value="<?php echo set_value('feature_name'); ?>" class="col-xs-10 col-sm-5" />
        </div>
                <?php echo form_error('feature_name'); ?>
    </div>
</div>

<div class="space-4"></div>-->



                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('recomended')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="recomended">Recommended</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="car_type" style="width:230px;" name="recomended" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('recomended', '1', TRUE); ?> value="1">Yes</option>
                                <option <?php echo set_select('recomended', '0'); ?> value="0">No</option>
                            </select>
                        </div>
                        <?php echo form_error('recomended'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div style="display: none;" class="form-group <?php echo (strlen(form_error('cost')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="cost">Price</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="number" name="cost" value="<?php echo set_value('cost'); ?>" class="col-xs-10 col-sm-5" />
                        </div>
                        <?php echo form_error('cost'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('feature_desc')) > 0 ) ? 'has-error' : ''; ?>">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_desc">Description</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <textarea name="feature_desc" style="width:230px;height:100px;" class="col-xs-5 col-sm-5"><?php echo set_value('feature_desc'); ?> </textarea>
                        </div>
                        <?php echo form_error('feature_desc'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('feature_img')) > 0 ) ? 'has-error' : ''; ?>">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_img">Image</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="file" accept="image/*"   name="feature_img" class="col-xs-10 col-sm-3" >
                        </div>                        
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info">
                            <i class="icon-ok bigger-110"></i>
                            Submit
                        </button>

                        &nbsp; &nbsp; &nbsp;
                        <button class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            Reset
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Sr</th>
                    <th>Feature Category</th>
                    <th>Feature Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table> 


        <!--        <div class="col-xs-12">
        <?php if (isset($all_features)): ?>                                    
                            <div class="table-header">
                                Results for "Features"
                            </div>
            
            
                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr</th>
                                        <th>Feature Category</th>
                                        <th>Feature Name</th>
                                        <th>Description</th>
                                        
                                        <th></th>
                                        <th></th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
            <?php
            $i = 1;
            foreach ($all_features as $a) :
//                            print_r($a);
                ?>
                                            <tr>
                                                <td>
                <?php echo $i++; ?>    
                                                </td>
                                                <td>
                <?php
                if ($a['feature_type'] == 1) {
                    echo 'Exteriors';
                }

                if ($a['feature_type'] == 2) {
                    echo 'Interiors';
                }

                if ($a['feature_type'] == 3) {
                    echo 'Comfort';
                }

                if ($a['feature_type'] == 4) {
                    echo 'Safety';
                }

                if ($a['feature_type'] == 5) {
                    echo 'Entertainment & Communication';
                }

                if ($a['feature_type'] == 5) {
                    echo 'Service';
                }

                if ($a['feature_type'] == 7) {
                    echo 'Other';
                }

//                                echo $a['feature_id'];
                ?>                                                    
                                                </td>
                                                <td>
                <?php echo $a ["feature_name"]; ?>
                                                </td>
                                                <td>
                <?php echo $a["feature_desc"]; ?>
                                                </td>
                                                <td>
                                                    <form action="<?php echo base_url() . 'feature/update_feature'; ?>" method="POST">
                                                        <input type="hidden" name="feature_id"  value="<?php echo $a['feature_id']; ?>" ?>
                                                        <button type="submit" class="btn btn-xs btn-success"> <i class="icon-pencil"></i> </button>
                                                        <button type="button" class="btn btn-xs btn-danger delete-feature" data-feature-id="<?php echo $a['feature_id']; ?>"> <i class="icon-trash"></i></button>
                                                    </form>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
    <?php endforeach; ?>
                                </tbody>
            
                            </table>
            
            
            
            
            
            
                            <table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Feature Name</th>
                                        <th>Value</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
            
                                
                            </table>
<?php endif; ?>
                </div>-->
    </div>
</div>
 <script src="<?php echo base_url().'assets/js/modules/validate_feature.js';?>"></script>
<script>
    jQuery(document).ready(function($) {
        $('.delete-feature').click(function() {
            var feature_id = $(this).data('feature-id');
            var self = $(this);
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '<?php echo base_url() . 'feature/delete_feature' ?>',
                data: {feature_id: feature_id}
            }).done(function(html) {
                if (html.sts == 1) {
                    self.closest('tr').remove();
                } else {
                    self.closest('td').html(html.msg)
                }
            });
        });
    });
    function get_feature_name()
    {
        var id_data = $('#car_type').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/get_feature_name') ?>",
            data: {id_data: id_data}
        }).done(function(htmls) {
            $('#feature_name').html(htmls);
//            alert(htmls);
        });
    }


</script>
<!--<script type="text/javascript" src="<?php echo base_url() . 'assets/js/validation/feature.js' ?>"></script>-->
