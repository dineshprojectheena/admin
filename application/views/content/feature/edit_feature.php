<div class="page-content">
    <div class="page-header">
        <h1>
            Dashboard
            <small>
                <i class="icon-double-angle-right"></i>
                features
            </small>
        </h1>
    </div>
    <div class="row">
        <div class="col-xs-12">

            <?php if (isset($error_msg)): ?> 
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $error_msg ?>
                    <br>
                </div>
            <?php endif; ?>

            <?php if (isset($success_msg)): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?php echo $success_msg ?>
                    <br>
                </div>
            <?php endif; ?>
            <?php
//            print_r($feature_details);            
            ?>
            
           

            <form id="validation-form" class="form-horizontal" method="post" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'feature/edit_feature' ?>" id="create-feature" role="form" >

                <div style="display: block;" class="form-group <?php echo (strlen(form_error('feature_type')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_type">Feature Category</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="car_type" class="select2" name="feature_type" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option <?php echo set_select('feature_type', '1', ($feature_details['feature_type']=='1') ? TRUE : FALSE) ?>  value="1">Exteriors</option>
                                <option <?php echo set_select('feature_type', '2', ($feature_details['feature_type']=='2') ? TRUE : FALSE) ?> value="2">Interiors</option>
                                <option <?php echo set_select('feature_type', '3', ($feature_details['feature_type']=='3') ? TRUE : FALSE) ?> value="3">Comfort & Convenienc</option>
                                <option <?php echo set_select('feature_type', '4', ($feature_details['feature_type']=='4') ? TRUE : FALSE) ?> value="4">Safety</option>
                                <option <?php echo set_select('feature_type', '5', ($feature_details['feature_type']=='5') ? TRUE : FALSE) ?> value="5">Entertainment & Communication</option>
                                <option <?php echo set_select('feature_type', '6', ($feature_details['feature_type']=='6') ? TRUE : FALSE) ?> value="6">Service</option>
				<option <?php echo set_select('feature_type', '7', ($feature_details['feature_type']=='7') ? TRUE : FALSE) ?> value="7">Other</option>
				
                            </select>
                        </div>
                        <?php echo form_error('feature_type'); ?>
                    </div>
                </div>    

                <div style="display: block;" class="form-group <?php echo (strlen(form_error('featured_type_id')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_name">Feature Name</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="feature_name" class="select2" name="feature_name" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <?php
                                foreach ($features_sub_type as $features_sub_type_data) {
                                    ?>
                                    <option <?php echo set_select('feature_name', $features_sub_type_data['featured_sub_type'], ($feature_details['feature_name']==$features_sub_type_data['featured_sub_type']) ? TRUE : FALSE) ?> value="<?php echo $features_sub_type_data['featured_sub_type']; ?>"><?php echo $features_sub_type_data['featured_sub_type']; ?></option>                                
                                    <?php
                                }
                                ?>                                
                            </select>
                        </div>
                        <?php echo form_error('feature_name'); ?>


                    </div>
                </div>  
                
                <div class="form-group">
                    <!--<label class="col-sm-3 control-label no-padding-right" for="feature_name">Feature Name</label>-->
                    <div class="col-sm-9" <?php echo (strlen(form_error('feature_name')) > 0 ) ? 'has-error' : ''; ?>">
                        <div class="clearfix">
                            <input type="hidden" name="feature_id" value="<?php echo set_value('feature_id', $feature_details['feature_id']) ?>" />
                            <!--<input type="text" name="feature_name" value="<?php echo set_value('feature_name', $feature_details['feature_name']) ?>" class="col-xs-10 col-sm-5" />-->

                        </div>
                        <?php echo form_error('feature_name'); ?>
                    </div>
                </div>



                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('recomended')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="recomended">Recommended</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="car_type" class="select2" name="recomended" data-placeholder="Click to Choose...">
                                <option value="">&nbsp;</option>
                                <option value="1" <?php echo set_select('recomended', '1', ($feature_details['recomended'] == '1') ? TRUE : FALSE) ?>>Yes</option>
                                <option value="0" <?php echo set_select('recomended', '1', ($feature_details['recomended'] === '0') ? TRUE : FALSE) ?>>No</option>
                            </select>                            
                        </div>
                        <?php echo form_error('recomended'); ?>
                    </div>
                </div>



                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('feature_desc')) > 0 ) ? 'has-error' : ''; ?>"">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_desc">Description</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <textarea name="feature_desc" class="col-xs-10 col-sm-5"><?php echo set_value('feature_desc', $feature_details['feature_desc']) ?></textarea>
                        </div>
                        <?php echo form_error('feature_desc'); ?>
                    </div>
                </div>

                <div class="space-4"></div>

                <div class="form-group <?php echo (strlen(form_error('feature_img')) > 0 ) ? 'has-error' : ''; ?>">
                    <label class="col-sm-3 control-label no-padding-right" for="feature_img">Image</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="file" name="feature_img" class="col-xs-10 col-sm-5" >
                            <input type="hidden" name="feature_img2" value="<?php echo set_value('feature_img', $feature_details['feature_img']) ?>" class="col-xs-10 col-sm-5" >
                        </div>                        
                    </div>
                </div>

                <div class="space-4"></div>


                <div class="clearfix form-actions">
                    <div class="col-md-offset-3 col-md-9">
                        <button class="btn btn-info">
                            <i class="icon-ok bigger-110"></i>
                            Update
                        </button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
 <script src="<?php echo base_url().'assets/js/modules/validate_feature.js';?>"></script>
<script>
    prePopulate: [
        {id: 123, name: "Slurms MacKenzie"},
    ]
</script>
