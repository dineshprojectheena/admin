<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->

    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">
                
                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">
                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->


                    <!-- main content data start here -->
                    <?php $this->load->view('content/'.$content) ?>    
                    <!-- main content data end here -->

                </div><!-- /.main-content -->                

            </div><!-- /.main-container-inner -->

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!-- footer js file links start here -->
        <?php $this->load->view('templates/js') ?>    
        <!-- footer js file links end here -->

        <!-- footer data links start here -->
        <?php $this->load->view('templates/footer') ?>    
        <!-- footer data links end here -->  

</body>
</html>



