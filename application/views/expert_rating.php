
<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->


        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php
                    $this->load->view('templates/breadcrumbs');

//                            echo $_POST['model_id'];
                    ?>    
                    <!-- breadcrumbs data links end here -->

                    <script>
                        $(document).ready(function () {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'expert_rating/rating_result'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function (sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });
                        get_expert_model();
                        function get_expert_model()
                        {
                            var model_id = "<?php
                    if (!empty($single_res['model_id'])) {
                        echo $single_res['model_id'];
                    } else if (!empty($_POST['model_id'])) {
                        echo $_POST['model_id'];
                    }
                    ?>";
//                            alert(model_id);
                            var brand_id = $('#brand_id').val();
                            if (brand_id != '')
                            {

                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo site_url('stitcher/get_exp_brand_wise_model') ?>",
                                    data: {brand_id: brand_id, model_id: model_id}
                                }).done(function (html) {
                                    $("#model_res").html(html);
                                    get_exp_variant();
                                });
                            }
                        }
                        function get_exp_variant()
                        {

                            var variant_id = "<?php
                    if (!empty($single_res['variant_id'])) {
                        echo $single_res['variant_id'];
                    } else if (!empty($_POST['all_product_name'])) {
                        echo $_POST['all_product_name'];
                    }
                    ?>";
                            var brand_id = $('#brand_id').val();
                            var model_id = $('#model_id').val();

                            $.ajax({
                                type: "POST",
                                url: "<?php echo site_url('stitcher/get_exp_model_wise_variant') ?>",
                                data: {model_id: model_id, brand_id: brand_id, variant_id: variant_id}
                            }).done(function (html) {
                                $("#variant_results").html(html);
                            });
                        }
                    </script>


                    <!-- main content data start here -->
                    <?php ?>                       
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Expert rating
                            </div>

                        </div>

                        
                        <?php 
                        $status = $this->session->flashdata('status');                        
                        if (!empty($status)): ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                <?php echo $status ?>
                                <br>
                            </div>
                        <?php endif; ?>




                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form  id="validation-form" action="<?php echo site_url("rating_upload"); ?>" class="form-horizontal" enctype="multipart/form-data" accept-charset="utf-8" method="POST" >
                                            <div class="span6">
                                                <?php
                                                $results = validation_errors();
//                                                print_r($results);

                                                if (!empty($results)) {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>
                                                    </ul>

                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">

                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                <div class="col-sm-9" >
                                                                    <select name="brand_id" id="brand_id" onchange="get_expert_model();" style="width: 340px;">
                                                                        <option  value="">Select Brand</option>
                                                                        <?php
                                                                        foreach ($brand as $brand_res) {
                                                                            ?>
                                                                            <option 
                                                                            <?php
                                                                            if ($single_res['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            } else if ($_POST['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?> value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                    </select>  
                                                                    <span class="error"><?php echo form_error('brand_id'); ?></span>

                                                                </div> 
                                                            </div> 
                                                            <?php
//                                                            print_r($single_res_detail);
                                                            ?>
                                                            <div class="form-group">
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                                                <div class="col-sm-9" style="float:left;" id="model_res" >
                                                                    <select name="model_id" id="model_id" style="width: 340px;">
                                                                        <option  value="">Select model</option>                                                                        

                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                                                </div>


                                                            </div>
                                                            <?php
//                                                            print_r($single_res);
                                                            ?>
                                                            <div class="form-group" id="" >
                                                                <label  class="col-sm-3 control-label no-padding-right" for="model_id">Variant</label>
                                                                <div class="col-sm-9" style="float:left;" >
                                                                    <div  id="variant_results" >
                                                                        <select name="all_product_name" id="all_product_name"  style="width: 340px;">
                                                                            <option  value="">Select Variant</option>

                                                                        </select>  
                                                                    </div>  

                                                                    <br> <span class="error"><?php echo form_error('product_name'); ?></span>
                                                                </div>
                                                                <input type="hidden" name="expert_rating_ids" id="expert_rating_ids" value="<?php
                                                                if ($single_res['expert_rating_id'] != '') {
                                                                    echo $single_res['expert_rating_id'];
                                                                } else if (isset($_POST['expert_rating_ids'])) {
                                                                    echo $_POST['expert_rating_ids'];
                                                                }
                                                                ?>" class="col-xs-10 col-sm-5">                                                                
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="total">Rating</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">    

<!--                                                                        <div class="flexible-stars" style="float:left;"
                                                                             data-gold="sprite-gold-star"
                                                                             data-silver="sprite-silver-star"
                                                                             data-half="sprite-half-star"
                                                                             data-url="/flexible-stars/example/handler/"
                                                                             data-init="<?php
                                                                             if ($single_res['total'] != '') {
                                                                                 echo $single_res['total'];
                                                                             } else if (isset($_POST['total'])) {
                                                                                 echo $_POST['total'];
                                                                             }
                                                                             ?>"
                                                                             data-doRate="#total"
                                                                             >


                                                                        </div>-->
                                                                        <input type="text" value="<?php
                                                                        if ($single_res['total'] != '') {
                                                                            echo $single_res['total'];
                                                                        } else if (isset($_POST['total'])) {
                                                                            echo $_POST['total'];
                                                                        }
                                                                        ?>" id="total"  name="total" placeholder="0" style="width: 190px;">

                                                                        <span class="error" ><?php echo form_error('total'); ?></span> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="profile">Picture</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <?php
                                                                        if ($single_res['profile_id'] != '') {
                                                                            ?>
                                                                            <img src="<?php echo base_url() . "uploads/expert/" . $single_res['profile_id']; ?>" style="width:50px;" />
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <input type="file" accept="image/*"  name="expert_pic" id="expert_pic" value="<?php
                                                                        if ($single_res['profile_id'] != '') {
                                                                            echo $single_res['profile_id'];
                                                                        } else if (isset($_POST['pro_image2'])) {
                                                                            echo $_POST['pro_image2'];
                                                                        }
                                                                        ?>" style="width: 340px;">                                                                
                                                                        <input type="hidden"  value="<?php
                                                                        if ($single_res['profile_id'] != '') {
                                                                            echo $single_res['profile_id'];
                                                                        } else if (isset($_POST['pro_image2'])) {
                                                                            echo $_POST['pro_image2'];
                                                                        }
                                                                        ?>" id="pro_image2" name="pro_image2">                                                                        

                                                                        <span class="error"><?php echo form_error('expert_pic'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="source">Source</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="source" id="source" value="<?php
                                                                        if ($single_res['source'] != '') {
                                                                            echo $single_res['source'];
                                                                        } else if (isset($_POST['source'])) {
                                                                            echo $_POST['source'];
                                                                        }
                                                                        ?>" style="width: 340px;">                                                                
                                                                        <span class="error"><?php echo form_error('source'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="com_name">Journalist Name</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="com_name" id="com_name" value="<?php
                                                                        if ($single_res['journilist_name'] != '') {
                                                                            echo $single_res['journilist_name'];
                                                                        } else if (isset($_POST['com_name'])) {
                                                                            echo $_POST['com_name'];
                                                                        }
                                                                        ?>" style="width: 340px;">                                                                
                                                                        <span class="error"><?php echo form_error('com_name'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>




                                                            <!--                                                            <div class="form-group">
                                                                                                                            <label class="col-sm-3 control-label no-padding-right" for="journilist_name">Journalist Name</label>
                                                                                                                            <div class="col-sm-9">
                                                                                                                                <div class="clearfix">
                                                                                                                                    <input type="text" name="journilist_name" id="journilist_name" value="<?php
                                                            if ($single_res['journilist_name'] != '') {
                                                                echo $single_res['journilist_name'];
                                                            } else if (isset($_POST['journilist_name'])) {
                                                                echo $_POST['journilist_name'];
                                                            }
                                                            ?>" class="col-xs-10 col-sm-5">                                                                
                                                                                                                                    <span class="error"><?php echo form_error('journilist_name'); ?></span>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                            
                                                                                                                        <div class="space-4"></div>-->


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="heading">Heading</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="heading" id="heading" value="<?php
                                                                        if ($single_res['heading'] != '') {
                                                                            echo $single_res['heading'];
                                                                        } else if (isset($_POST['heading'])) {
                                                                            echo $_POST['heading'];
                                                                        }
                                                                        ?>" style="width:340px;">                                                                
                                                                        <span class="error"><?php echo form_error('heading'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <hr>    
                                                            </div>    
                                                            <div class="form-group" id="res">
                                                                <label class="col-sm-3 control-label no-padding-right" for="what_i_like">Details</label>
                                                                <!--<div class="col-sm-9"></div>-->
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <?php
                                                                        echo $msg;
//                                                                        $this->load_editor();
//                                                                        echo $this->load_editor_data();
//                                                                        echo $this->expert_rating->load_editor();
//                                                                        echo $this->expert_rating->load_editor();

                                                                        ?>
                                                                        <!--<iframe id="my_iframe" src="http://localhost/mynewcaradmin/CKEditorForm" style="width:98%;height:650px;border:0px;"></iframe>-->

<!--                                                                        <div id="editor">
                                                                            <?php
                                                                            if ($single_res['what_i_like'] != '') {
                                                                                echo $single_res['what_i_like'];
                                                                            } else if (isset($_POST['details'])) {
                                                                                echo $_POST['details'];
                                                                            }
                                                                            ?>
                                                                        </div>-->

                                                                        <input type="hidden" name="details" id="details" value="
                                                                        <?php
//                                                                        if ($single_res['what_i_like'] != '') {
//                                                                            echo $single_res['what_i_like'];
//                                                                        } else if (isset($_POST['details'])) {
//                                                                            echo $_POST['details'];
//                                                                        }
                                                                        ?>"/>

                                                                    </div>
                                                                    <div  class="detail_error">
                                                                        <span class="error"><?php echo form_error('details'); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                        </div>                                            
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($single_res)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" onclick="return check_validation('1'), pass_desc_txt2();" id="submit" value="1" name="submit" class="btn btn-info" onclick="check_like();">                                                
                                                            Submit
                                                        </button>

                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" onclick="return check_validation('2'), pass_desc_txt2();" id="submit" value="2" name="update" class="btn btn-info" onclick="check_like();">                                                
                                                        <!--<button type="submit" id="update" onclick="return check_validation('2'), pass_desc_txt2();" name="update" class="btn btn-info">-->                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Variant Name</th>
                                        <th>Source</th>
                                        <th>Variant Rating</th>
                                        <th>Journalist Name</th>                                                        
                                        <th>Heading</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                        <tr>
                                            <th colspan="8" >
                                                <button class="btn btn-xs btn-danger" style="float:right" onclick="delete_all_rating();" name="deleteall" type="submit">
                                                    Delete All
                                                </button>

                                            </th>

                                        </tr>
                                    </tfoot>
                            </table> 
                        </div>

                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->
            <script>
                initSample();
            </script>
            <script>
                function delete_all_rating()
                {

                    var val = [];
                    $(':checkbox:checked').each(function (i) {
                        val[i] = $(this).val();
//                        alert(val[i]);
                    });
                    window.location.href="<?php echo base_url('expert_rating/expert_rating_delete/')?>/?id="+val;
//                    alert(val);

                }
                function check_like()
                {
                    var details = $("#details").val();
                    if (details == '')
                    {
                        var editor3 = $("#editor3").val();
                        $("#details").val(editor3);
                    }
                    return false;
                }
            </script>
            <script src="star/jquery.flexible.stars.js"></script>

            <style type="text/css" media="screen">
                i {
                    display: inline-block
                }
                [class^="sprite-"] {
                    background: url(star/star.png) no-repeat top left;
                }
                .sprite-gold-star {
                    background-position: 0 0;
                    width: 29px;
                    height: 28px;
                }
                .sprite-half-star {
                    background-position: 0 -78px;
                    width: 29px;
                    height: 28px;
                }
                .sprite-silver-star {
                    background-position: 0 -156px;
                    width: 29px;
                    height: 28px;
                }
            </style>

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <script src="<?php echo base_url() . 'assets/js/modules/validate_expert_rating.js'; ?>"></script>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  
            <style>
                .help-inline
                {
                    display:block !important;    
                }
            </style>
            <script>
//                    CKFinder.setupCKEditor(editor, '/');
                get_expert_model();
            </script>
    </body>
</html>




