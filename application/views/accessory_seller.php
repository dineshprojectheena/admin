
<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                
                <script>
                                        $(document).ready(function() {
                                            $('.table').dataTable({
                                                "bProcessing": true,
                                                "bServerSide": true,
                                                "sAjaxSource": '<?php echo base_url().'accessory/accessory_result';?>',
                                                "aaSorting": [[4, "desc"]],
                                                "fnServerData": function(sSource, aoData, fnCallback) {
                                                    $.ajax({
                                                        "dataType": 'json',
                                                        "type": "POST",
                                                        "url": sSource,
                                                        "data": aoData,
                                                        "success": fnCallback
                                                    });
                                                }
                                            });
                                        });
                                    </script>

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                    <!-- main content data start here -->
                    <?php
                    foreach($ress2 as $get_res)
//                    print_r($ress2);    
                    ?>                       
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Accessory Seller
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form" action="<?php echo site_url("acc_sell_upload"); ?>" role="form" enctype="multipart/form-data" method="POST" class="form-horizontal" method="POST">
                                            <div class="span6">
                                                <?php
                                                $results = validation_errors();
                                                if (!empty($results)) {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>


                                                    </ul>


                                                    <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="acc_name">Name</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="acc_name" id="acc_name" value="<?php
                                                                        if ($get_res->name != '') {
                                                                        echo $get_res->name;
                                                                        } else if (isset($_POST['acc_name'])) {
                                                                            echo $_POST['acc_name'];
                                                                        }
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                
                                                                        <input type="hidden" name="accessory_seller_id" id="accessory_seller_id" value="<?php
                                                                        if ($get_res->accessory_seller_id != '') {
                                                                        echo $get_res->accessory_seller_id;
                                                                        } else if (isset($_POST['accessory_seller_id'])) {
                                                                            echo $_POST['accessory_seller_id'];
                                                                        }                                                                     
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                
                                                                        <span class="error"><?php echo form_error('acc_name'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="city">City</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">

                                                                        <select name="city" id="city" style="float:left; width: 335px;">
                                                                            <option value="">Select City</option>    
                                                                           <?php
                                                                            foreach ($city as $citys) {
                                                                                ?>
                                                                                <option 
                                                                                <?php
                                                                                if($get_res->city_id == $citys->city_id || $_POST['city']==$citys->city_id)
                                                                                {
                                                                                   echo $res = 'selected';
                                                                                }
                                                                                 else {
                                                                                   echo $res = '';
                                                                                }
                                                                                ?>
                                                                                    value="<?php echo $citys->city_id; ?>"><?php echo $citys->name; ?></option>    
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                        </select>

                                                                        <span class="error"><?php echo form_error('city'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>  
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="post_code">Post Code</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="post_code" id="post_code" value="<?php
                                                                        if ($get_res->postcode != '') {
                                                                        echo $get_res->postcode;
                                                                        } else if (isset($_POST['post_code'])) {
                                                                            echo $_POST['post_code'];
                                                                        } 
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                

                                                                        <span class="error"><?php echo form_error('post_code'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="street">Street</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="street" id="street" value="<?php
                                                                        if ($get_res->street != '') {
                                                                        echo $get_res->street;
                                                                        } else if (isset($_POST['street'])) {
                                                                            echo $_POST['street'];
                                                                        } 
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                

                                                                        <span class="error"><?php echo form_error('street'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="pics">Picture</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="file" accept="image/*" name="picture" id="picture" value="<?php
                                                                        if ($get_res->picture!=''){
                                                                        echo $get_res->picture;
                                                                        } else if (isset($_POST['picture'])){
                                                                            echo $_POST['picture'];
                                                                        }                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <input type="hidden" name="profile_pic" id="profile_pic" value="<?php
                                                                        if($get_res->picture!='') 
                                                                        {
                                                                        echo $get_res->picture;
                                                                        }else if(isset($_POST['profile_pic'])) {
                                                                        echo $_POST['profile_pic'];
                                                                        }                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <span class="error"><?php echo form_error('picture'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="contact_person_firstname">Contact Person Firstname</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="contact_person_firstname" id="contact_person_firstname" value="<?php
                                                                        if ($get_res->contact_person_firstname != '') {
                                                                        echo $get_res->contact_person_firstname;
                                                                        } else if (isset($_POST['contact_person_firstname'])) {
                                                                            echo $_POST['contact_person_firstname'];
                                                                        }                                                                      
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <span class="error"><?php echo form_error('contact_person_firstname'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="contact_person_lastname">Contact Person Lastname</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="contact_person_lastname" id="contact_person_lastname" value="<?php
                                                                       if ($get_res->contact_person_lastname != '') {
                                                                        echo $get_res->contact_person_lastname;
                                                                        } else if (isset($_POST['contact_person_lastname'])) {
                                                                            echo $_POST['contact_person_lastname'];
                                                                        }                                                                      
                                                                        
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <span class="error"><?php echo form_error('contact_person_lastname'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="contact_person_tel">Contact Person Telephone</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="contact_person_tel" id="contact_person_tel" value="<?php
                                                                        if ($get_res->contact_person_telephoneno != '') {
                                                                        echo $get_res->contact_person_telephoneno;
                                                                        } else if (isset($_POST['contact_person_tel'])) {
                                                                            echo $_POST['contact_person_tel'];
                                                                        }
                                                                        
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <span class="error"><?php echo form_error('contact_person_tel'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="contact_person_email">Contact Person Email</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="contact_person_email" id="contact_person_email" value="<?php
                                                                         if ($get_res->contact_person_emailid != '') {
                                                                        echo $get_res->contact_person_emailid;
                                                                        } else if (isset($_POST['contact_person_email'])) {
                                                                            echo $_POST['contact_person_email'];
                                                                        }
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <span class="error"><?php echo form_error('contact_person_email'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="fax">Fax</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="fax" id="fax" value="<?php
                                                                         if ($get_res->fax != '') {
                                                                        echo $get_res->fax;
                                                                        } else if (isset($_POST['fax'])) {
                                                                            echo $_POST['fax'];
                                                                        }
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <span class="error"><?php echo form_error('fax'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="accessory_brand">Accessory Brand</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="accessory_brand" id="others_brand" value="<?php
                                                                        if ($get_res->accessory_brand != '') {
                                                                        echo $get_res->accessory_brand;
                                                                        } else if (isset($_POST['accessory_brand'])) {
                                                                            echo $_POST['accessory_brand'];
                                                                        }
                                                                        
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <span class="error"><?php echo form_error('accessory_brand'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                        </div>                                            
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($get_res)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                    <th>Accessory Name</th>
                                                    <th>Email</th>
                                                    <th>Contact No</th>
                                                    <th>Brand</th>
                                                    <th>City</th>                                                    
                                                    <!--<th>Status</th>-->
                                                    <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                
                                

<!--                                <div class="form-group">
                                    <div class="form-group">
                                        <?php
                                        if (isset($ress)):
                                            ?>                                    
                                            <div class="table-header">
                                                Results for "Accessory Seller"
                                            </div>
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th>Accessory Brand</th>
                                                        <th>Contact Person Email</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php
                                                    $i = 1;
                                                    foreach ($ress as $r) :
                                                        ?>
                                                        <tr>                                                
                                                            <td>
                                                                <?php echo $i; ?>                                                        
                                                            </td>
                                                            <td>
                                                                <?php echo $r->name; ?>
                                                            </td>

                                                            td>
                                                                <?php echo $r->accessory_brand; ?>
                                                            </td
                                                            <td>
                                                                <?php echo $r->contact_person_emailid; ?>
                                                            </td>


                                                    <form action="<?php echo 'acc_sell_upload'; ?>" method="POST">
                                                        <input type="hidden" name="accessory_seller_id"  value="<?php echo $r->accessory_seller_id; ?>" ?>
                                                        <td class="align_tble">
                                                            <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                                Edit
                                                            </button>                                                        
                                                        </td>
                                                    </form>
                                                    <form action="<?php echo 'acc_sell_upload'; ?>" method="POST" >
                                                        <input type="hidden" name="accessory_seller_id"  value="<?php echo $r->accessory_seller_id; ?>" ?>
                                                        <td>
                                                            <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                delete
                                                            </button>

                                                        </td>                                                    
                                                    </form>
							<td></td>
                                                    </tr>
                                                    <?php $i++; ?>    
                                                <?php endforeach; ?>


                                                </tbody>
                                            </table>
                                            <?php
                                        else:
//echo '<p>No Task Exist</p>';
                                        endif;
                                        ?>
                                    </div>


                                </div>-->
                            </div>     

                        </div>

			


                        <!-- main content data end here -->

                    </div><!-- /.main-content -->           
                    <?php
//                    print_r($get_res);
                    ?>
                      <script>	    
                      var prePopulate_data2=[{id:"<?php echo $get_res->accessory_brand;?>",name:"<?php echo $get_res->brand_name; ?>"}];
//                    alert("<?php // echo $get_res->accessory_brand; ?><?php echo $get_res->brand_name; ?>");
//                    alert(prePopulate_data);
                      </script>	
                    
                    

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->
	    

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <script src="<?php echo base_url().'assets/js/modules/validate_acc_seller.js' ?>" ></script>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



