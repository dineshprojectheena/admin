<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">

                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->

                <div class="main-content">

                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->
                    
                    <script>
                        $(document).ready(function() {
                            $('.table').dataTable({
                                "bProcessing": true,
                                "bServerSide": true,
                                "sAjaxSource": '<?php echo base_url() . 'stitcher/news_result'; ?>',
                                "aaSorting": [[4, "desc"]],
                                "fnServerData": function(sSource, aoData, fnCallback) {
                                    $.ajax({
                                        "dataType": 'json',
                                        "type": "POST",
                                        "url": sSource,
                                        "data": aoData,
                                        "success": fnCallback
                                    });
                                }
                            });
                        });
                    </script>


                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                News
                            </div>
                            <?php //echo validation_errors(); ?>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <form action="<?php echo site_url("news_upload"); ?>" class="form-horizontal" id="validation-form" method="POST" enctype="multipart/form-data" role="form" novalidate="novalidate">
                                    <div class="span6">
                                        <div class="tabbable tabs-left">
                                            <ul class="nav nav-tabs" id="myTab3">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#home3">
                                                        <i class="pink icon-dashboard bigger-110"></i>
                                                        Details
                                                    </a>
                                                </li>

                                                <li style="display:none;">
                                                    <a data-toggle="tab" href="#profile3">
                                                        <i class="blue icon-user bigger-110"></i>
                                                        Seo
                                                    </a>
                                                </li>                                            
                                            </ul>

                                            <div class="tab-content">
                                                <div id="home3" class="tab-pane active">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="news_title">News Heading</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input type="text" name="news_title" id="news_title" class="col-xs-10 col-sm-5" value="<?php
                                                                if (isset($_POST['news_title'])) {
                                                                    echo $_POST['news_title'];
                                                                }
                                                                ?>"><span class="error"><?php echo form_error('news_title'); ?></span>
                                                            </div>


                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="details">Details</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
<!--                                                                <textarea name="details" id="details" class="col-xs-10 col-sm-5">
                                                                <?php
                                                                if (isset($_POST['details'])) {
                                                                    echo $_POST['details'];
                                                                }
                                                                ?>
                                                                    </textarea>-->
                                                                <div class="wysiwyg-editor" id="editor1" name="details" onkeyup="passdata();"></div>
                                                                <textarea style="display:none;" type="hidden" id="news_titles" name="news_titles"><?php
                                                                    if (isset($_POST['news_titles'])) {
                                                                        echo $_POST['news_titles'];
                                                                    } else {
                                                                        echo $r->details;
                                                                    }
                                                                    ?></textarea>

<!--<input type="hidden" id="news_titles" value="<?php if (isset($_POST['news_titles'])) {
                                                                        echo $_POST['news_titles'];
                                                                    } ?>" name="news_titles"/>-->
                                                                <script>
                                                                    function passdata()
                                                                    {
                                                                        var editor1 = $('#editor1').html();
                                                                        $('#news_titles').val(editor1);
                                                                    }
                                                                </script>    

                                                                <span class="error"><?php echo form_error('details'); ?></span>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="pub_date">Publish Date</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">

                                                                <input class="span10 date-picker" readonly="true" data-date-format="yyyy-mm-dd" value="<?php
                                                                if (isset($_POST['id-date-picker-1'])) {
                                                                    echo $_POST['id-date-picker-1'];
                                                                } else {
                                                                    echo date('Y-m-d');
                                                                }
                                                                ?>"  name="id-date-picker-1" id="id-date-picker-1"  type="text" />

                                                                <span class="add-on">
                                                                    <i class="icon-calendar"></i>
                                                                </span>
                                                            </div>

                                                        </div>
                                                    </div>                                                 

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="news_title">Author</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input type="text" name="Author" id="Author" class="col-xs-10 col-sm-5"
                                                                       value="<?php
                                                                       if (isset($_POST['Author'])) {
                                                                           echo $_POST['Author'];
                                                                       }
                                                                       ?>"
                                                                       >
                                                                <span class="error"><?php echo form_error('Author'); ?></span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="pic">Pic</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input type="file" name="news_pic" id="news_pic" class="col-xs-10 col-sm-5">

                                                                <span class="error"><?php echo form_error('pic'); ?></span>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>




                                                    <div class="form-group">
                                                        <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="status">Status</label>
                                                        <div class="col-xs-12 col-sm-9">
                                                            <select id="status" name="status" data-placeholder="Click to Choose..." style="width: 200px;">
                                                                <option  value="">Select Status</option>
                                                                <option <?php
                                                                if (isset($_POST['status'])) {
                                                                    if ($_POST['status'] == '2') {
                                                                        echo 'selected';
                                                                    }
                                                                }
                                                                ?> value="1">Enable</option>    
                                                                <option <?php
                                                                if (isset($_POST['status'])) {
                                                                    if ($_POST['status'] == '1') {
                                                                        echo 'selected';
                                                                    }
                                                                }
                                                                ?> value="2">Disable</option>    

                                                            </select>
                                                            <span class="error"><?php echo form_error('status'); ?></span>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div id="profile3" class="tab-pane" style="display:none;">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="title">Title</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <input type="text" name="title" id="title" class="col-xs-10 col-sm-5">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="editor1">Description</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <div class="wysiwyg-editor" id="editor1" onblur="pass_desc_txt();" name="editor1" style="height:100px;width:99%;"></div>
                                                                <input type="hidden" name="description" id="description" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="space-4"></div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label no-padding-right" for="editor2">Keyword</label>
                                                        <div class="col-sm-9">
                                                            <div class="clearfix">
                                                                <div class="wysiwyg-editor" id="editor2" onblur="pass_key_txt();" name="editor2" style="height:100px;width:99%;"></div>
                                                                <input type="hidden" name="keyword" id="keyword" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="space-4"></div>

                                                </div>                                            
                                            </div>
                                        </div>
                                    </div>







                                    <div class="space-4"></div>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" name="submit" class="btn btn-info">                                                
                                                Submit
                                            </button>

                                            &nbsp; &nbsp; &nbsp;
                                            <button class="btn" type="reset">
                                                <i class="icon-undo bigger-110"></i>
                                                Reset
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>News Heading</th>
                                            <th>Author</th>
                                            <th>Publish Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table> 


                                <!--                                <div class="form-group">
                                                                    <div class="table-header">
                                                                        Results for "News"
                                                                    </div>
                                                                    <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>#</th>
                                                                                <th>News Heading</th>
                                                                                <th>Author</th>
                                                                                <th>Publish Date</th>
                                                                                <th>Status</th>
                                                                                <th></th>
                                                                                <th></th>
                                                                            </tr>
                                                                        </thead>
                                
                                                                        <tbody>
                                <?php
                                $i = 1;
                                if (isset($results)):foreach ($results as $r) :
                                        ?>
                                                                                            <tr>	
                                        
                                                                                                <td class="align_tble">
                                        <?php echo $i; ?>
                                        
                                                                                                </td>
                                                                                                <td class="align_tble">
                                        <?php echo $r->news_title; ?>
                                                                                                </td>
                                                                                                <td class="align_tble">
                                        <?php echo $r->author; ?>
                                                                                                </td>
                                                                                                <td class="align_tble">
                                        <?php echo $r->publish_date; ?>
                                                                                                </td>
                                        
                                                                                                <td class="align_tble">
                                        <?php
                                        $status = $r->status;
                                        if ($status == 1) {
                                            $chk1 = 'selected';
                                        } else if ($status == 2) {
                                            $chk2 = 'selected';
                                        }
                                        ?>
                                                                                                    <select name="<?php echo 'status' . $i; ?>" id="<?php echo 'status' . $i; ?>" disabled class="removeborder">
                                                                                                        <option <?php
                                        if ($status == '1') {
                                            echo $chk1;
                                        }
                                        ?> value="1">Enabled</option>    
                                                                                                        <option <?php
                                        if ($status == '2') {
                                            echo $chk2;
                                        }
                                        ?> value="2">Disabled</option>    
                                                                                                    </select>                                                            
                                                                                                </td>
                                                                                        <form action="<?php echo 'stitcher/get_update_news'; ?>" method="POST">
                                                                                            <input type="hidden" name="upd_id"  value="<?php echo $r->latest_news_id; ?>" ?>     
                                                                                            <td class="align_tble">
                                                                                                <button type="submit" class="btn btn-xs btn-success" name="update">
                                                                                                    Edit
                                                                                                </button>                                                        
                                                                                            </td>
                                                                                        </form>
                                                                                        <form action="<?php echo 'stitcher/delete_news'; ?>" method="POST">                                                         
                                                                                            <td>
                                                                                                <input type="hidden" name="count"  value="<?php echo $r->latest_news_id; ?>" ?> 
                                                                                                <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                                                    delete
                                                                                                </button>
                                                                                            </td>   
                                                                                        </form>
                                                                                        </tr>
                                        <?php $i++; ?>    
                                    <?php endforeach; ?>
                                    <?php
                                else:
                                endif;
                                ?>
                                
                                                                        </tbody>
                                                                    </table>
                                
                                                                </div>-->
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->
            <!--[if !IE]>-->


            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <script src="<?php echo base_url() . 'assets/js/modules/validate_news.js'; ?>"></script>
            <!-- footer js file links end here -->

            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



