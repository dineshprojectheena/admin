
<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header') ?>    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->
        <div class="main-container" id="main-container">
            <div class="main-container-inner">
                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                <div class="main-content">
                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <!-- breadcrumbs data links end here -->                    
                    <script>
                                        $(document).ready(function() {
                                            $('.table').dataTable({
                                                "bProcessing": true,
                                                "bServerSide": true,
                                                "sAjaxSource": '<?php echo base_url().'module/model_result';?>',
                                                "aaSorting": [[4, "desc"]],
                                                "fnServerData": function(sSource, aoData, fnCallback) {
                                                    $.ajax({
                                                        "dataType": 'json',
                                                        "type": "POST",
                                                        "url": sSource,
                                                        "data": aoData,
                                                        "success": fnCallback
                                                    });
                                                }
                                            });
                                        });
                    </script>
                     <!-- main content data start here -->
                    <?php
                    foreach($ress2 as $get_res)
                    ?>                       
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Model
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form" action="<?php echo site_url("model_upload"); ?>" class="form-horizontal" method="POST">
                                            <div class="span6">
                                                <?php
                                                $results = validation_errors();
                                                if (!empty($results)) {
                                                    ?>
                                                    <div class="def_error">
                                                        <button type="button" class="close" data-dismiss="alert">
                                                            <i class="icon-remove"></i>
                                                        </button>

                                                        <strong>
                                                            <i class="icon-remove"></i>
                                                            Warning:
                                                        </strong>
                                                        Please check the form carefully for errors!                                
                                                        <br>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="tabbable tabs-left" >
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>


                                                    </ul>


                                                    <div class="tab-content" >
                                                        <div id="home3" class="tab-pane active">

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="brand">Brand</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <?php
//                                                                        echo $get_res['brand_id'];
//                                                                        print_r($get_res);
                                                                        ?>                                                                        
                                                                        <select name="brand" id="brand" style="width:340px;float:left;">
                                                                            <option  value="">Select Brand</option>
                                                                            <?php
                                                                            foreach ($brand as $brand_res) {
                                                                                ?>
                                                                                <option 
                                                                                <?php
                                                                                if ($get_res['brand_id'] == $brand_res['brand_id']) {
                                                                                    echo 'selected';
                                                                                }
                                                                                ?>   value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                    <?php
                                                                                }
                                                                                ?>  


                                                                                                                      
                                                                        </select>  
                                                                        <span class="error"><?php echo form_error('brand'); ?></span>
                                                                        <input type="hidden" name="model_id" id="model_id" value="<?php
                                                                        if ($get_res['model_id'] != '') {
                                                                            echo $get_res['model_id'];
                                                                        } else if (isset($_POST['model_id'])) {
                                                                            echo $_POST['model_id'];
                                                                        }
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                

                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <!--                                                            <div class="form-group">
                                                                                                                            <label class="col-sm-3 control-label no-padding-right" for="car_type">Car Type</label>
                                                                                                                            <div class="col-sm-9">
                                                                                                                                <div class="clearfix">
                                                                                                                                    <input type="text" name="car_type" id="car_type" value="<?php
                                                            if (!isset($_POST['car_type'])) {
                                                                if ($get_res['car_type'] != '') {
                                                                    echo $get_res['car_type'];
                                                                }
                                                            } else {
                                                                echo $_POST['car_type'];
                                                            }
                                                            ?>" class="col-xs-10 col-sm-5">                                                                
                                                                                                                                    
                                                                                                                                    <span class="error"><?php echo form_error('car_type'); ?></span>
                                                            
                                                                                                                                </div>
                                                            
                                                                                                                            </div>
                                                                                                                        </div>
                                                            
                                                                                                                        <div class="space-4"></div>-->


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="model_name">Model Name</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="model_name" id="model_name" value="<?php
                                                                        if (!isset($_POST['model_name'])) {
                                                                            if ($get_res['model_name'] != '') {
                                                                                echo $get_res['model_name'];
                                                                            }
                                                                        } else {
                                                                            echo $_POST['model_name'];
                                                                        }
                                                                        ?>" class="col-xs-10 col-sm-5">                                                                                                                                        
                                                                        <span class="error"><?php echo form_error('model_name'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
                                                                <div class="col-sm-9">
                                                                    <div class="clearfix">
                                                                        <?php
                                                                        $status = $get_res['status'];
                                                                        ?>                                             
                                                                        <select name="status" id="status" style="width:340px;float:left;">
                                                                            <option  value="">Select Status</option>
                                                                            <option <?php
                                                                            if (!isset($_POST['status'])) {
                                                                                if ($status == 1) {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                echo '';
                                                                            }
                                                                            ?> value="1">Enabled</option>    
                                                                            <option <?php
                                                                            if (!isset($_POST['status'])) {
                                                                                if ($status == 2) {
                                                                                    echo 'selected';
                                                                                }
                                                                            } else {
                                                                                echo '';
                                                                            }
                                                                            ?> value="2">Disabled</option>      
                                                                        </select>  
                                                                        <span class="error"><?php echo form_error('status'); ?></span>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>




                                                        </div>                                            
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($get_res)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="form-group">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Model Id</th>
                                                <th>Model Name</th>
                                                <!--<th>Car Type</th>-->
                                                <th>Brand</th>
                                                <th>Status</th>
                                                <th>Action</th>
<!--                                            <th>Download</th>-->
                                            </tr>
                                        </thead>
                                    </table>
                                    
                                    
                                    
                                    <!--                                    <div class="form-group">
                                    <?php
                                    if (isset($ress)):
                                        ?>                                    
                                                                                            <div class="table-header">
                                                                                                Results for "Model"
                                                                                            </div>
                                                                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>#</th>
                                                                                                        <th>Model Name</th>
                                                                                                        <th>Car Type</th>
                                                                                                        <th>Status</th>
                                                                                                        <th></th>
                                                                                                        <th></th>
                                                                                                    </tr>
                                                                                                </thead>
                                                
                                                                                                <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($ress as $r) {
                                            ?>
                                                                                                                    <tr>   
                                                                                                                <form action="<?php echo 'model_upload'; ?>" method="POST">
                                                                                                                    <td>
                                            <?php echo $i; ?>  
                                                                                                                        <input type="hidden" name="model_id"  value="<?php echo $r['model_id']; ?>" ?>
                                                                                                                    </td>
                                                                                                                    <td>
                                            <?php echo $r['model_name']; ?>
                                                                                                                    </td>
                                                                                                                        <td>
                                            <?php echo $r['car_type']; ?>
                                                                                                                    </td>
                                                                                                                    <td>
                                            <?php
                                            if ($r['status'] == 1) {
                                                echo 'Enable';
                                            } else {
                                                echo 'Disable';
                                            }
                                            ?>
                                                                                                                    </td>
                                                                                                                    <td class="align_tble">
                                                                                                                        <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                                                                                            Edit
                                                                                                                        </button>                                                        
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                                                                                            delete
                                                                                                                        </button>
                                                            
                                                                                                                    </td>                                                    
                                                                                                                    <td>                                                            
                                                                                                                    </td>                                                    
                                                                                                                </form>
                                                                                                                </tr>
                                            <?php $i++; ?>    
                                        <?php } ?>
                                                                                                </tbody>
                                                                                            </table>
                                        <?php
                                    else:
                                    endif;
                                    ?>
                                                                        </div>-->


                                </div>
                            </div>     

                        </div>




                        <!-- main content data end here -->

                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <script src="<?php echo base_url().'assets/js/modules/validate_model.js' ?>" ></script>
            <!-- footer js file links end here -->
            <!-- footer data links start here -->
            <?php $this->load->view('templates/footer') ?>    
            <!-- footer data links end here -->  

    </body>
</html>



