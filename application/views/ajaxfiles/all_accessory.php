<?php
$features = array('1' => 'Exteriors',
    '2' => 'Interiors',
    '3' => 'Safety & Technology',
    '4' => 'Audio & Communication',
    '5' => 'Services',
    '6' => 'Equipment Package',
    '7' => 'Services');

$subcategory = array(
    '1' => 'Headlights',
    '2' => 'Sunroofs',
    '3' => 'Tyres',
    '4' => 'Steering Wheels',
    '5' => 'Seats',
    '6' => 'Navigation System',
    '7' => 'Audio System',
    '8' => 'Speakers',
    '9' => 'Telephone',
    '10' => 'Others',
    '11' => 'Finance',
    '12' => 'AMC Contract',
    '13' => 'Insurance for car',
    '14' => 'Extended Warranty'
);
$count = count($subcategory);
?>
<style>
    .feature-mapper-div .tab-pane{
        overflow: auto;
        height: 300px;
    }

    .feature-mapper-div ul{
        margin: 0;
        padding: 0;
    }

    .feature-mapper-div ul li{
        list-style-type: none;
    }
</style>

<div class="span6">
    <div class="tabbable">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active">
                <a data-toggle="tab" href="#Exterior">
                    <i class="green icon-home bigger-110"></i>
                    Exterior
                </a>
            </li>

            <li>
                <a data-toggle="tab" href="#Interior">
                    Interior

                </a>
            </li>

            <li >
                <a data-toggle="tab" href="#Safety">
                    Safety & Technology

                </a>

            </li>
            <li >
                <a data-toggle="tab" href="#Audio">
                    Audio & Communication

                </a>

            </li>
<!--            <li>
                <a data-toggle="tab" href="#services">
                    Services														
                </a>												
            </li>-->
            <li>
                <a data-toggle="tab" href="#equipment">
                    Equipment Package														
                </a>												
            </li>
            <li>
                <a data-toggle="tab" href="#isservices">
                    Is Services														
                </a>												
            </li>
        </ul>

        <div class="tab-content">
            <div id="Exterior" class="tab-pane in active">
                <p>
                <ul>
                    <?php
                    foreach($exterior_accessory as $e1):
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="accessory_checkbox" type="checkbox" value="<?php echo $e1['accessory_id'] ?>" data-name="<?php echo $e1["accessory_name"] ?>" <?php echo in_array($e1['accessory_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php
                                        for ($i = 0; $i <= $count; $i++) {
                                            if ($i == $e1['accessory_sub_cat']) {
                                                print_r($subcategory[$i]);
                                            }
                                        }
//                                        echo $e["accessory_name"]; 
                                        ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e1["accessory_name"]; ?>
                                    </span>
                                </label>
                            </div>
                        </li>



                    <?php endforeach; ?>
                </ul>    
                </p>
            </div>

            <div id="Interior" class="tab-pane">
                <p>
                <ul>
                    <?php
                    foreach ($interior_accessory as $e2) :
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="accessory_checkbox" type="checkbox" value="<?php echo $e2['accessory_id'] ?>" data-name="<?php echo $e2["accessory_name"] ?>" <?php echo in_array($e2['accessory_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php
                                        for ($j = 0; $j <= $count; $j++) {
                                            if ($j == $e2['accessory_sub_cat']) {
                                                print_r($subcategory[$j]);
                                            }
                                        }
//                                        echo $e["accessory_name"]; 
                                        ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e2["accessory_name"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>



                    <?php endforeach; ?>
                </ul>    
                </p>
            </div>

            <div id="Safety" class="tab-pane">
                <p>
                <ul>
                    <?php
                    foreach ($safety_accessory as $e3) :
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="accessory_checkbox" type="checkbox" value="<?php echo $e3['accessory_id'] ?>" data-name="<?php echo $e3["accessory_name"] ?>" <?php echo in_array($e3['accessory_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php
                                        for ($k = 0; $k <= $count; $k++) {
                                            if ($k == $e3['accessory_sub_cat']) {
                                                print_r($subcategory[$k]);
                                            }
                                        }
//                                        echo $e["accessory_name"]; 
                                        ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e3["accessory_name"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>



                    <?php endforeach; ?>
                </ul>      
                </p>
            </div>
            <div id="Audio" class="tab-pane">
                <p>
                <ul>
                    <?php
                    foreach ($audio_accessory as $e4) :
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="accessory_checkbox" type="checkbox" value="<?php echo $e4['accessory_id'] ?>" data-name="<?php echo $e4["accessory_name"] ?>" <?php echo in_array($e4['accessory_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php
                                        for ($l = 0; $l <= $count; $l++) {
                                            if ($l == $e4['accessory_sub_cat']) {
                                                print_r($subcategory[$l]);
                                            }
                                        }
//                                        echo $e["accessory_name"]; 
                                        ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e4["accessory_name"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>



                    <?php endforeach; ?>
                </ul>    
                </p>
            </div>
            <div id="services" class="tab-pane">
                <p>
                <ul>
                    <?php
                    foreach ($services_accessory as $e5) :
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="accessory_checkbox" type="checkbox" value="<?php echo $e5['accessory_id'] ?>" data-name="<?php echo $e5["accessory_name"] ?>" <?php echo in_array($e5['accessory_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php
                                        for ($m = 0; $m <= $count; $m++) {
                                            if ($m == $e5['accessory_sub_cat']) {
                                                print_r($subcategory[$m]);
                                            }
                                        }
//                                        echo $e["accessory_name"]; 
                                        ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e5["accessory_name"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>



                    <?php endforeach; ?>
                </ul>     
                </p>
            </div>
            <div id="equipment" class="tab-pane">
                <p>
                <ul>
                    <?php
                    foreach ($equipment_accessory as $e6) :
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="accessory_checkbox" type="checkbox" value="<?php echo $e6['accessory_id'] ?>" data-name="<?php echo $e6["accessory_name"] ?>" <?php echo in_array($e6['accessory_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php
                                        for ($n = 0; $n <= $count; $n++) {
                                            if ($n == $e6['accessory_sub_cat']) {
                                                print_r($subcategory[$n]);
                                            }
                                        }
//                                        echo $e["accessory_name"]; 
                                        ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e6["accessory_name"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>



                    <?php endforeach; ?>
                </ul>      
                </p>
            </div>
            <div id="isservices" class="tab-pane">
                <p>
                <ul>
                    <?php
//                    print_r($ser_accessory);
                    foreach($ser_accessory as $e6):
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="accessory_checkbox" type="checkbox" value="<?php echo $e6['accessory_id'] ?>" data-name="<?php echo $e6["accessory_name"] ?>" <?php echo in_array($e6['accessory_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php
                                        for ($n = 0; $n <= $count; $n++) {
                                            if ($n == $e6['accessory_sub_cat']) {
                                                print_r($subcategory[$n]);
                                            }
                                        }
//                                        echo $e["accessory_name"]; 
                                        ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e6["accessory_name"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>



                    <?php endforeach; ?>
                </ul>      
                </p>
            </div>

        </div>
    </div>
</div><!--/span-->


<input type="hidden" id="count" name="count" value="1" />
<div class="row">
    <div class="col-md-12">
        <p>
            Accessory Selected :
            <span class="selected-feature"></span>
        </p>
    </div>
</div>
<script>

    $(document).ready(function() {
        $(".accessory_checkbox").click(function() {
            
            var count=parseInt($('#count').val());
            if(count<=1)
            {
            var feature_name = "";
            $("input.accessory_checkbox:checked").each(function() {
            feature_name+=$(this).data('name') + ',';
//            alert(feature_name);
            count+=1;
            });
            $('#count').val(count);
            
            $('span.selected-feature').html(feature_name);
            }
            else
            {
            alert('You Can select one accessory detail at a time....');    
            }    
            exit;
//            if (feature_name.length != 0) {
//                $('textarea.feature_names, .feature_names').val('');
//                $('textarea.feature_names, .feature_names').val(feature_name);
//                $('input.feature_id').val(feature_id);
//                $('span.selected-feature').html($("input.accessory_checkbox:checked").length + ' Features selected');
//                $('#myModal').modal('hide')
//            } else {
//                $('.form_error_msg').html('Please select atleast 1 Feature');
//            }
//            $('span.selected-feature').html(feature_name);
        });

        $(".select-feature").click(function() {
            var feature_name = ""
            var feature_id = ""
            $("input.accessory_checkbox:checked").each(function() {
                feature_name+=$(this).data('name') + ', ';
                feature_id+= $(this).val() + ',';
            });
            var res = feature_name.split(","); 
//        alert(res);
            var res2 = feature_id.split(","); 
            if (feature_name.length != 0) {
//            alert($('#price').val());
                $('textarea.feature_names, .feature_names').val('');
                $('textarea.feature_names, .feature_names').val(res[0]);
                $('input.feature_id').val(res2[0]);
                $('span.selected-feature').html($("input.accessory_checkbox:checked").length + ' Features selected');
                $('#myModal').modal('hide')
            } else {
                $('.form_error_msg').html('Please select atleast 1 Feature');
            }

        });
    });

</script>