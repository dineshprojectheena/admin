<style>
    .feature-mapper-div .tab-pane{
        overflow: auto;
        height: 300px;
    }

    .feature-mapper-div ul{
        margin: 0;
        padding: 0;
    }

    .feature-mapper-div ul li{
        list-style-type: none;
    }
</style>
<div class="tabbable">
    <ul class="nav nav-tabs" id="myTab">
        <li class="active">
            <a data-toggle="tab" href="#a1">
                <i class="green icon-home bigger-110"></i>
                Exterior
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#a2">
                Interior

            </a>
        </li>

        <li >
            <a data-toggle="tab" href="#a3">
                Comfort & Convenienc

            </a>

        </li>
        <li >
            <a data-toggle="tab" href="#a4">
                Safety

            </a>

        </li>
        <li>
            <a data-toggle="tab" href="#a5">
                Entertainment & Communication														
            </a>												
        </li>
<!--        <li>
            <a data-toggle="tab" href="#a6">
                Service														
            </a>												
        </li>-->
        <li>
            <a data-toggle="tab" href="#a7">
                Other														
            </a>												
        </li>
    </ul>

    <div class="tab-content">
        <div id="a1" class="tab-pane in active">
            <p>
            <ul style="list-style: none outside none;">
                <?php
                foreach ($features as $e) :
                    if($e['feature_type']==1){
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="feature-checkbox" type="checkbox" value="<?php echo $e['feature_id'] ?>" data-name="<?php echo $e["feature_name"] . '->' . $e["feature_desc"]; ?>" <?php echo in_array($e['feature_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php echo $e["feature_name"]; ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e["feature_desc"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
            </ul>  
            </p>
        </div>
        
        <div id="a2" class="tab-pane">
                <p>
                <ul style="list-style: none outside none;">
                <?php
                foreach ($features as $e) :
                    if($e['feature_type']==2){
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="feature-checkbox" type="checkbox" value="<?php echo $e['feature_id'] ?>" data-name="<?php echo $e["feature_name"] . '->' . $e["feature_desc"]; ?>" <?php echo in_array($e['feature_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php echo $e["feature_name"]; ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e["feature_desc"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
                </ul>   
                </p>
            </div>
        
            <div id="a3" class="tab-pane">
                <p>
                <ul style="list-style: none outside none;">
                <?php
                foreach ($features as $e) :
                    if($e['feature_type']==3){
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="feature-checkbox" type="checkbox" value="<?php echo $e['feature_id'] ?>" data-name="<?php echo $e["feature_name"] . '->' . $e["feature_desc"]; ?>" <?php echo in_array($e['feature_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php echo $e["feature_name"]; ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e["feature_desc"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
                </ul>   
                </p>
            </div>
            
            <div id="a4" class="tab-pane">
                <p>
                <ul style="list-style: none outside none;">
                <?php
                foreach ($features as $e) :
                    if($e['feature_type']==4){
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="feature-checkbox" type="checkbox" value="<?php echo $e['feature_id'] ?>" data-name="<?php echo $e["feature_name"] . '->' . $e["feature_desc"]; ?>" <?php echo in_array($e['feature_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php echo $e["feature_name"]; ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e["feature_desc"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
                </ul>   
                </p>
            </div>
        
        <div id="a5" class="tab-pane">
                <p>
                <ul style="list-style: none outside none;">
                <?php
                foreach ($features as $e) :
                    if($e['feature_type']==5){
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="feature-checkbox" type="checkbox" value="<?php echo $e['feature_id'] ?>" data-name="<?php echo $e["feature_name"] . '->' . $e["feature_desc"]; ?>" <?php echo in_array($e['feature_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php echo $e["feature_name"]; ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e["feature_desc"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
                </ul>   
                </p>
            </div>
        
            <div id="a6" class="tab-pane">
                <p>
                <ul style="list-style: none outside none;">
                <?php
                foreach ($features as $e) :
                    if($e['feature_type']==6){
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="feature-checkbox" type="checkbox" value="<?php echo $e['feature_id'] ?>" data-name="<?php echo $e["feature_name"] . '->' . $e["feature_desc"]; ?>" <?php echo in_array($e['feature_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php echo $e["feature_name"]; ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e["feature_desc"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
                </ul>   
                </p>
            </div>
        
        
            <div id="a7" class="tab-pane">
                <p>
                <ul style="list-style: none outside none;">
                <?php
                foreach ($features as $e) :
                    if($e['feature_type']==7){
                        ?>
                        <li>
                            <div class="checkbox">
                                <label>
                                    <input width="100px" class="feature-checkbox" type="checkbox" value="<?php echo $e['feature_id'] ?>" data-name="<?php echo $e["feature_name"] . '->' . $e["feature_desc"]; ?>" <?php echo in_array($e['feature_id'], $variant_features) ? 'checked' : '' ?> /> 
                                    <span style="width:300px;">
                                        <?php echo $e["feature_name"]; ?>
                                        ->
                                    </span>
                                    <span style="width:300px;">
                                        <?php echo $e["feature_desc"]; ?>

                                    </span>

                                </label>
                            </div>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
                </ul>   
                </p>
            </div>
        
        
        <div class="row">
            <div class="col-md-12">
                <p>
                    Features Selected :
                    <span class="selected-feature"></span>
                </p>
        <!--        <input type="text" value="" id="price" placeholder="Price"  />     -->
                <input class="feature-checkbox" type="hidden" value="0" id="counter"  />     
                <p class="form_error_msg">

                </p>
                <!--        <div class="selected-feature">
                            <span class="label label-default">Default</span>
                        </div>-->
            </div>
        </div>
    </div>
    <!--<div class="tab-content feature-mapper-div">
        <div class="tab-pane active" id="exterior">
            <ul style="list-style: none outside none;">
    <?php foreach ($features as $e) :
        ?>
                    <li>
                                <div class="checkbox">
                                    <label>
                                        <input width="100px" class="feature-checkbox" type="checkbox" value="<?php echo $e['feature_id'] ?>" data-name="<?php echo $e["feature_name"] . '->' . $e["feature_desc"]; ?>" <?php echo in_array($e['feature_id'], $variant_features) ? 'checked' : '' ?> /> 
                                        <span style="width:300px;">
        <?php echo $e["feature_name"]; ?>
                                        ->
                                        </span>
                                        <span style="width:300px;">
        <?php echo $e["feature_desc"]; ?>
                                        
                                        </span>
                                        
                                    </label>
                                </div>
                    </li>
    <?php endforeach; ?>
            </ul>
        </div>
        <div class="tab-pane" id="interior">
            <ul style="list-style: none outside none;">
    <?php foreach ($features as $i) : ?>
        <?php if ($i["feature_type"] == 2): ?>
                                <li>
                                    <div class="checkbox">
                                        <label>
                                            <input class="feature-checkbox" type="checkbox" value="<?php echo $i['feature_id'] ?>" data-name="<?php echo $i["feature_name"] ?>" /> <?php echo $i["feature_name"] ?>
                                        </label>
                                    </div>
                                </li>
        <?php endif; ?>
    <?php endforeach; ?>
            </ul>
    
        </div>
        <div class="tab-pane" id="safety">
            <ul style="list-style: none outside none;">
    <?php foreach ($features as $s) : ?>
        <?php if ($s["feature_type"] == 3): ?>
                                <li>
                                    <div class="checkbox">
                                        <label>
                                            <input class="feature-checkbox" type="checkbox" value="<?php echo $s['feature_id'] ?>" data-name="<?php echo $s["feature_name"] ?>" /> <?php echo $s["feature_name"] ?>
                                        </label>
                                    </div>
                                </li>
        <?php endif; ?>
    <?php endforeach; ?>
            </ul>
        </div>
        <div class="tab-pane" id="cnc">
            <ul style="list-style: none outside none;">
    <?php foreach ($features as $cnc) : ?>
        <?php if ($cnc["feature_type"] == 4): ?>
                                <li>
                                    <div class="checkbox">
                                        <label>
                                            <input class="feature-checkbox" type="checkbox" value="<?php echo $cnc['feature_id'] ?>" data-name="<?php echo $cnc["feature_name"] ?>" /> <?php echo $cnc["feature_name"] ?>
                                        </label>
                                    </div>
                                </li>
        <?php endif; ?>
    <?php endforeach; ?>
            </ul>
        </div>
        <div class="tab-pane" id="enc">
            <ul style="list-style: none outside none;">
    <?php foreach ($features as $enc) : ?>
        <?php if ($enc["feature_type"] == 5): ?>
                                <li>
                                    <div class="checkbox">
                                        <label>
                                            <input class="feature-checkbox" type="checkbox" value="<?php echo $enc['feature_id'] ?>" data-name="<?php echo $enc["feature_name"] ?>" /> <?php echo $enc["feature_name"] ?>
                                        </label>
                                    </div>
                                </li>
        <?php endif; ?>
    <?php endforeach; ?>
            </ul>
        </div>
        <div class="tab-pane" id="service">
            <ul style="list-style: none outside none;">
    <?php foreach ($features as $ser) : ?>
        <?php if ($ser["feature_type"] == 6): ?>
                                <li>
                                    <div class="checkbox">
                                        <label>
                                            <input class="feature-checkbox" type="checkbox" value="<?php echo $ser['feature_id'] ?>" data-name="<?php echo $ser["feature_name"] ?>" /> <?php echo $ser["feature_name"] ?>
                                        </label>
                                    </div>
                                </li>
        <?php endif; ?>
    <?php endforeach; ?>
            </ul>
        </div>
    </div>-->


</div>
<script>
    $(document).ready(function() {
        $(".feature-checkbox").click(function(){
            var counter=$('#counter').val();
            if(counter < 1)
            {
                $('#counter').val(parseInt(counter) + 1);
                var feature_name = "";
                $("input.feature-checkbox:checked").each(function() {
                    feature_name += $(this).data('name') + ',';
                });
                $('span.selected-feature').html(feature_name);
                $('.select-feature').click();
            }
            else
            {
                alert('One feature at a single time');
            }
        });

        $(".select-feature").click(function() {
            var feature_name = ""
            var feature_id = ""
            $("input.feature-checkbox:checked").each(function() {
                feature_name += $(this).data('name') + ', ';
                feature_id += $(this).val() + ',';
            });
            var res = feature_name.split(",");
//        alert(res);
            var res2 = feature_id.split(",");
//        alert(res2);
//        alert(feature_name);    
            if (feature_name.length != 0) {
//            alert($('#price').val());
                $('textarea.feature_names, .feature_names').val('');
                $('textarea.feature_names, .feature_names').val(res[0]);
                $('input.feature_id').val(res2[0]);
                $('span.selected-feature').html($("input.feature-checkbox:checked").length + ' Features selected');
                $('#myModal').modal('hide')
            } else {
                $('.form_error_msg').html('Please select atleast 1 Feature');
            }

        });

    });
</script>