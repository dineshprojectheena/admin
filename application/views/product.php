<!DOCTYPE html>
<html lang="en">
    <!-- Body data links start here -->
    <?php $this->load->view('templates/header');
//    print_r($results_up);
    ?>    
    	<style>
	#commentForm {
		width: 500px;
	}
	#commentForm label {
		width: 250px;
	}
	#commentForm label.error, #commentForm input.submit {
		margin-left: 253px;
	}
	#signupForm {
		width: 670px;
	}
	#signupForm label.error {
		margin-left: 10px;
		width: auto;
		display: inline;
	}
	#newsletter_topics label.error {
		display: none;
		margin-left: 103px;
	}
	</style>
    <style>
        .col-sm-3
        {
            width:20% !important;    
        }
        .tab-content
        {
            overflow:hidden !important;    
        }
    </style>  
    
    <!-- Body data links end here -->
    <body class="navbar-fixed breadcrumbs-fixed">
        <!-- Header data links start here -->
        <?php $this->load->view('templates/topbar') ?>    
        <!-- Header data links end here -->

        <div class="main-container" id="main-container">
            <div class="main-container-inner">
                <!-- Left sidebar data links start here -->
                <?php $this->load->view('templates/sidebar') ?>    
                <!-- Left sidebar data links end here -->
                <div class="main-content">
                    <!-- breadcrumbs data links start here -->
                    <?php $this->load->view('templates/breadcrumbs') ?>    
                    <script>
                                        $(document).ready(function(){
                                            $('.table').dataTable({
                                                "bProcessing": true,
                                                "bServerSide": true,
                                                "sAjaxSource": '<?php echo base_url().'product/product_result';?>',
                                                "aaSorting": [[4, "desc"]],
                                                "fnServerData": function(sSource, aoData, fnCallback) {
                                                    $.ajax({
                                                        "dataType": 'json',
                                                        "type": "POST",
                                                        "url": sSource,
                                                        "data": aoData,
                                                        "success": fnCallback
                                                    });
                                                }
                                            });
                                        });
                                    </script>
                    <!-- breadcrumbs data links end here -->
                    <!-- main content data start here -->
                    <div class="page-content">
                        <div class="page-header">
                            <div class="table-header">
                                Variant Details
                            </div>
                        </div>
			<?php if (isset($error_msg)): ?>
		        <div class="alert alert-danger">
		            <button data-dismiss="alert" class="close" type="button">
		                <i class="ace-icon fa fa-times"></i>
		            </button>
		            <?php echo $error_msg ?>
		            <br>
		        </div>
		    <?php endif; ?>

		    <?php if (isset($success_msg)): ?>
		        <div class="alert alert-success">
		            <button data-dismiss="alert" class="close" type="button">
		                <i class="ace-icon fa fa-times"></i>
		            </button>
		            <?php echo $success_msg ?>
		            <br>
		        </div>
		    <?php endif; ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form id="validation-form"  action="<?php echo site_url("product_upload"); ?>"  enctype="multipart/form-data" role="form" class="form-horizontal" method="POST">
                                            <div class="span6">                                                
                                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Body Color</h4>
                                                            </div>
<!--                                                            <div class="modal-body" >
                                                                <div class="row-fluid">
                                                                    <table class="table table-hover no-margin-bottom no-border-top">
                                                                        <tbody>
                                                                            <tr>
                                                                                <?php
                                                                                if(!empty($results_up[0]['body_color_id']))
                                                                                {    
                                                                                $colors = $results_up[0]['body_color_id'];
                                                                                }
                                                                                else if(!empty($results_up_copy[0]['body_color_id']))
                                                                                {    
                                                                                $colors = $results_up_copy[0]['body_color_id'];
                                                                                }
//                                                                                echo $colors;
                                                                                $colorsexp = explode(',', $colors);
                                                                                $i = 1;
                                                                                foreach($body_color as $body_color_data){
                                                                                    echo $colorsexp[$j];
                                                                                    ?>   
                                                                                    <td style="float:left;width:15%;">
                                                                                        <input type="checkbox" class="chkNumber" name="border_id" value="<?php echo $body_color_data['body_color_id']; ?>"
                                                                                        <?php
                                                                                        if (in_array($body_color_data['body_color_id'], $colorsexp)) {
                                                                                            echo 'checked';
                                                                                        }
                                                                                        ?>
                                                                                               />  
                                                                                        <img src="<?php echo base_url() . 'uploads/body_color/' . $body_color_data['body_icon']; ?>" width="40px"/> </td>
                                                                                    <?php
                                                                                    $i++;
                                                                                }
                                                                                ?>

                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>    
                                                            </div>-->
<!--                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary btnGetAll">Save changes</button>
                                                                <input type="hidden" name="total_color_id" id="total_color_id" value="<?php
                                                                if (!isset($_POST['total_color_id'])) {
                                                                    if ($colors != '') {
                                                                        echo $colors;
                                                                    }
                                                                } else {
                                                                    echo $_POST['total_color_id'];
                                                                }
                                                                ?>"/>  
                                                            </div>-->
<!--                                                            <script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
                                                            <script type="text/javascript">
                                                                $(function() {
                                                                    $('.btnGetAll').click(function() {
                                                                        if ($('.chkNumber:checked').length) {
                                                                            var chkId = '';
                                                                            $('.chkNumber:checked').each(function() {
                                                                                chkId += $(this).val() + ",";
                                                                            });
                                                                            chkId = chkId.slice(0, -1);
//                                                                            alert(chkId);
                                                                            $('#total_color_id').val(chkId);
                                                                            $('.btn-default').click();
                                                                        }
                                                                        else {
                                                                            alert('Nothing Selected');
                                                                        }
                                                                    });
                                                                    $('.btnGetAll2').click(function() {
                                                                        if ($('.chkNumber2:checked').length) {
                                                                            var chkId = '';
                                                                            $('.chkNumber2:checked').each(function() {
                                                                                chkId += $(this).val() + ",";
                                                                            });
                                                                            chkId = chkId.slice(0, -1);
//                                                                            alert(chkId);
                                                                            $('#wheel_id').val(chkId);
                                                                            $('.btn-default').click();
                                                                        }
                                                                        else {
                                                                            alert('Nothing Selected');
                                                                        }
                                                                    });

                                                                });
                                                            </script>-->
                                                        </div>
                                                    </div>
                                                </div>


<!--                                                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Body Wheel</h4>
                                                            </div>
                                                            <div class="modal-body" >
                                                                <div class="row-fluid">
                                                                    <table class="table table-hover no-margin-bottom no-border-top">
                                                                        <tbody>
                                                                            <tr>
                                                                                <?php
                                                                                
                                                                                $wheel = $results_up[0]['wheel_id'];
                                                                                $wheelexp = explode(',', $wheel);
                                                                                $j = 1;
                                                                                foreach ($body_wheel as $body_wheel_data) {//                                                                              
                                                                                    ?>   
                                                                                    <td style="float:left;width:15%;">
                                                                                        <input type="checkbox" class="chkNumber2" name="wheel_id" value="<?php echo $body_wheel_data['wheel_id']; ?>"
                                                                                        <?php
                                                                                        if (in_array($body_wheel_data['wheel_id'], $wheelexp)) {
                                                                                            echo 'checked';
                                                                                        }
                                                                                        ?>
                                                                                               />  
                                                                                        <img src="<?php echo base_url() . 'uploads/wheel/' . $body_wheel_data['wheel_design']; ?>" width="40px"/> </td>
                                                                                    <?php
                                                                                    $i++;
                                                                                }
                                                                                ?>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>    
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary btnGetAll2">Save changes</button>
                                                                <input type="hidden" name="wheel_id" id="wheel_id" value="<?php
                                                                if (!isset($_POST['wheel_id'])) {
                                                                    if ($wheel != '') {
                                                                        echo $wheel;
                                                                    }
                                                                } else {
                                                                    echo $_POST['wheel_id'];
                                                                }
                                                                ?>"/>  
                                                            </div>                                                            
                                                        </div>
                                                    </div>
                                                </div>-->

                                                <div class="tabbable tabs-left">
                                                    <ul class="nav nav-tabs" id="myTab3">
                                                        <li class="active">
                                                            <a data-toggle="tab" href="#home3">
                                                                <i class="pink icon-dashboard bigger-110"></i>
                                                                Details
                                                            </a>
                                                        </li>

                                                    </ul>
                                                     <div class="tab-content">
                                                        <div id="home3" class="tab-pane active">
                                                            <h4 class="header blue bolder smaller">Variant Details</h4>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="brand_id">Brand</label>
                                                                <div class="col-sm-3">
                                                                   <?php
//                                                                    print_r($results_up);
                                                                    ?>
                                                    
                                                                    <select name="brand_id" id="brand_id" style="width:180px;" onchange="getmodel();">
                                                                        <option  value="">Select Brand</option>
                                                                        <?php
                                                                        foreach ($brand as $brand_res){
                                                                            ?>
                                                                            <option 
                                                                            <?php                                                                            
                                                                            if($results_up_copy[0]['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            else if($_POST['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            else if($results_up[0]['brand_id'] == $brand_res['brand_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>   value="<?php echo $brand_res['brand_id']; ?>"><?php echo $brand_res['brand_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>  
                                                                    </select>  
                                                                     <br> <span class="error"><?php echo form_error('brand_id'); ?></span>
                                                                </div> 


                                                                <label class="col-sm-3 control-label no-padding-right" for="model_id">Model</label>
                                                                <div class="col-sm-3" id="model_res">
                                                                    <select name="model_id" id="model_id" style="width:180px;">
                                                                        <option  value="">Select model</option>
                                                                        <?php
                                                                        foreach ($model as $model_res) {
                                                                            ?>
                                                                            <option <?php
                                                                            if($results_up_copy[0]['model_id']==$model_res['model_id']){
                                                                                echo 'selected';
                                                                            }
                                                                            else if ($_POST['model_id']== $model_res['model_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            else if ($results_up[0]['model_id']== $model_res['model_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>  value="<?php echo $model_res['model_id']; ?>"><?php echo $model_res['model_name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                    </select>  
                                                                     <br> <span class="error"><?php echo form_error('model_id'); ?></span>
                                                                </div>


                                                            </div>

                                                            <div class="space-4"></div>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="pro_name">Variant Name</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="pro_name" id="pro_name" value="<?php
                                                                    if (!isset($_POST['pro_name'])) {
                                                                        if ($results_up[0]['pro_name'] != '') {
                                                                            echo $results_up[0]['pro_name'];
                                                                        }
                                                                        else if ($results_up_copy[0]['pro_name'] != '') {
                                                                            echo $results_up_copy[0]['pro_name'];
                                                                        }
                                                                    }
                                                                    else if (isset($_POST['pro_name'])) {
                                                                        echo $_POST['pro_name'];
                                                                    }
                                                                    ?>" >
                                                                    <input type="hidden" name="variant_id"  value="<?php echo $results_up[0]['variant_id']; ?>" ?>
                                                                     <br> <span class="error"><?php echo form_error('pro_name'); ?></span>
                                                                </div>
                                                                
                                                                <label class="col-sm-3 control-label no-padding-right" for="pro_type">Variant Type</label>
                                                                <div class="col-sm-3">
                                                                    <?php
//                                                                    echo $results_up[0]['pro_type'];
                                                                    $pro_type = array('Hatch Back', 'Sedan', 'Station Wagon', 'Van/ Mini Van', 'SUV/MUV', 'Coupe', 'Convertible');
                                                                    ?>
                                                                    <select name="pro_type" id="pro_type" style="width:180px;">
                                                                        <option  value="">Select Variant Type</option>
                                                                        <?php
                                                                        foreach ($pro_type as $pro_type_res) {
                                                                            ?>
                                                                            <option <?php
                                                                            if ($results_up[0]['pro_type'] == preg_replace('/[^a-zA-Z0-9]+/', '-', $pro_type_res)) {
                                                                                echo 'selected';
                                                                            }
                                                                            else if ($results_up_copy[0]['pro_type'] == preg_replace('/[^a-zA-Z0-9]+/', '-', $pro_type_res)) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>
                                                                                value="<?php echo preg_replace('/[^a-zA-Z0-9]+/', '-', $pro_type_res); ?>"><?php echo $pro_type_res; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                    </select>  
                                                                    <br> <span class="error"><?php echo form_error('pro_type'); ?></span>
                                                                </div>

                                                                
                                                            </div>

                                                            <div class="space-4"></div>

<!--                                                            <div class="form-group">
                                                                <div style="display:none;">
								 <label class="col-sm-3 control-label no-padding-right" for="city">City</label>
                                                                <div class="col-sm-3" id="city">
                                                                    <select name="city" id="city" style="width:180px;">
                                                                        <option  value="">Select City</option>
                                                                        <?php
                                                                        foreach ($city as $city_res) {
                                                                            ?>
                                                                            <option <?php
                                                                            if ($results_up[0]['city'] == $city_res['city_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            else if ($results_up_copy[0]['city'] == $city_res['city_id']) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>  value="<?php echo $city_res['city_id']; ?>"><?php echo $city_res['name']; ?></option>    
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                    </select>  
                                                                     <br> <span class="error"><?php echo form_error('city'); ?></span>
                                                                </div>
                                                                </div>
								<div style="display:none;">
                                                                <label class="col-sm-3 control-label no-padding-right" for="vairant">Variant</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="vairant" id="vairant" value="<?php
                                                                    if (!isset($_POST['vairant'])) {
                                                                        if($results_up_copy[0]['variant'] != '') {
                                                                            echo $results_up_copy[0]['variant'];
                                                                        }
                                                                        else if ($results_up[0]['variant'] != '') {
                                                                            echo $results_up[0]['variant'];
                                                                        }
                                                                    } else if (isset($_POST['vairant'])) {
                                                                        echo $_POST['vairant'];
                                                                    }
                                                                    ?>" >                                                                   
                                                                     <br> <span class="error"><?php echo form_error('vairant');
                                                                    ?></span>
                                                                </div>
								</div>
                                                                 
                                                            </div>-->

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                
                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_name">Engine Name</label>
                                                                <div class="col-sm-3">
                                                                    <div class="clearfix">
                                                                        <input type="text" name="engine_name" id="engine_name" value="<?php
                                                                        if (!isset($_POST['engine_name'])) {
                                                                            if ($results_up[0]['engine_name'] != '') {
                                                                                echo $results_up[0]['engine_name'];
                                                                            }
                                                                            else if ($results_up_copy[0]['engine_name'] != '') {
                                                                                echo $results_up_copy[0]['engine_name'];
                                                                            }
                                                                        } else if (isset($_POST['engine_name'])) {
                                                                            echo $_POST['engine_name'];
                                                                        }
                                                                        ?>" >                                                                
                                                                         <span class="sample_data">(CC123)</span> <br> <span class="error"><?php echo form_error('engine_name'); ?></span>
                                                                    </div>

                                                                </div>
                                                                <label class="col-sm-3 control-label no-padding-right" for="status">Status</label>
                                                                <div class="col-sm-3">
                                                                    <?php
                                                                    if(!empty($results_up_copy[0]['status']))
                                                                    {
                                                                    $status = $results_up_copy[0]['status'];    
                                                                    }    
                                                                    else {                                                                    
                                                                    $status = $results_up[0]['status'];
                                                                    }
                                                                    ?>
                                                                    <select name="status" id="status" style="width:180px;">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php
                                                                        if (!isset($_POST['status'])) {
                                                                            if ($status == 1) {
                                                                                echo 'selected';
                                                                            }
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> value="1">Enabled</option>    
                                                                        <option <?php
                                                                        if (!isset($_POST['status'])) {
                                                                            if ($status == 2) {
                                                                                echo 'selected';
                                                                            }
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> value="2">Disabled</option>    
                                                                    </select>  
                                                                     <br> <span class="error"><?php echo form_error('status'); ?></span>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                
                                                                <label class="col-sm-3 control-label no-padding-right" for="pro_images">Variant Image</label>
                                                                <div class="col-sm-3">
                                                                    <div class="clearfix">
                                                                        <input type="file" accept="image/*"  name="product_images" id="product_images">                                                                
                                                                        <input type="hidden" name="pro_images2" id="pro_images2"  value="<?php
                                                                        if (!isset($_POST['pro_images2'])) {
                                                                            if ($results_up[0]['pro_image'] != '') {
                                                                                echo $results_up[0]['pro_image'];
                                                                            }
                                                                            else if ($results_up_copy[0]['pro_image'] != '') {
                                                                                echo $results_up_copy[0]['pro_image'];
                                                                            }
                                                                        } else if (isset($_POST['pro_images2'])) {
                                                                            echo $_POST['pro_images2'];
                                                                        }
                                                                        ?>">                                                                
                                                                         <br> <span class="error"><?php echo form_error('product_images'); ?></span>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                            
                                                                
                                                                
                                                            </div>

                                                            <div id="smallModal" class="modal fade" tabindex="-1" role="dialog">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                            <h4 class="modal-title">Small Modal</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Add the <code>.modal-sm</code> class on <code>.modal-dialog</code> to create this small modal.</p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                            <button type="button" class="btn btn-primary">OK</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>



                                                            

                                                            <div class="space-4"></div>
                                                            <div style="display:none;">
                                                                <h4 class="header blue bolder smaller">Color</h4>

                                                                <div class="form-group">
                                                                    <label class="col-sm-3 control-label no-padding-right" for="ext_images">Variant Colors</label>
                                                                    <div class="col-sm-3">
                                                                        <button class="btn" type="button" class="md-trigger"  data-toggle="modal" data-target="#myModal">
                                                                            <i class="icon-pencil bigger-125"></i>
                                                                            Images
                                                                        </button>
                                                                         <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('tramission_type'); ?></span>

                                                                    </div>
                                                                    <div class="md-overlay"></div>
                                                                    <label class="col-sm-3 control-label no-padding-right" for="pro_wheel">Variant Wheel</label>
                                                                    <div class="col-sm-3">
                                                                        <button class="btn" type="button" class="md-trigger"  data-toggle="modal" data-target="#myModal2">
                                                                            <i class="icon-pencil bigger-125"></i>
                                                                            Images
                                                                        </button>
                                                                         <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('pro_wheel'); ?></span>

                                                                    </div>
                                                                    <div class="md-overlay"></div>
                                                                </div>                                                         
                                                            </div>     
                                                            
                                                            
                                                            
                                                             <h4 class="header blue bolder smaller">Engine</h4>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="fuel_type">Fuel Type</label>
                                                                <div class="col-sm-3">
                                                                    <select name="fuel_type" id="fuel_type" style="width:180px;">
                                                                        <option  value="">Select Fuel Type</option>
                                                                        <option <?php
                                                                        if ($results_up[0]['fuel_type'] == '1') {
                                                                            echo 'selected';
                                                                        }
                                                                        else if ($results_up_copy[0]['fuel_type'] == '1') {
                                                                            echo 'selected';
                                                                        }
                                                                        ?> value="1">Petrol</option>    
                                                                        <option <?php
                                                                        if ($results_up_copy[0]['fuel_type'] == '2') {
                                                                            echo 'selected';
                                                                        }
                                                                        else if ($results_up[0]['fuel_type'] == '2') {
                                                                            echo 'selected';
                                                                        }
                                                                        ?> value="2">Diesel</option>    
                                                                        <option <?php
                                                                        if($results_up[0]['fuel_type']=='3'){
                                                                            echo 'selected';
                                                                        }
                                                                        else if($results_up_copy[0]['fuel_type'] == '3') {
                                                                            echo 'selected';
                                                                        }
                                                                        ?> value="3">CNG</option>    
                                                                        <option <?php
                                                                        if ($results_up[0]['fuel_type'] == '4') {
                                                                            echo 'selected';
                                                                        }
                                                                        else if($results_up_copy[0]['fuel_type'] == '4'){
                                                                            echo 'selected';
                                                                        }
                                                                        ?> value="4">Electric</option>    
                                                                    </select>  
                                                                     <br> <span class="error"><?php echo form_error('fuel_type'); ?></span>
                                                                </div> 
                                                                
<!--                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_types">Engine Types</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_types" id="engine_types" value="<?php
                                                                    if (!isset($_POST['engine_types'])) {
                                                                        if ($results_up[1]['engine_types'] != '') {
                                                                            echo $results_up[1]['engine_types'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_types'] != '') {
                                                                            echo $results_up_copy[1]['engine_types'];
                                                                        }
                                                                    } else if (isset($_POST['engine_types'])) {
                                                                        echo $_POST['engine_types'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('engine_types'); ?></span>
                                                                </div>-->
                                                                <label class="col-sm-3 control-label no-padding-right" for="displacement">Displacement (cc)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="displacement" id="displacement" value="<?php
                                                                    if (!isset($_POST['displacement'])) {
                                                                        if ($results_up[1]['displacement'] != '') {
                                                                            echo $results_up[1]['displacement'];
                                                                        }
                                                                        else if ($results_up_copy[1]['displacement'] != '') {
                                                                            echo $results_up_copy[1]['displacement'];
                                                                        }
                                                                    } else if (isset($_POST['displacement'])) {
                                                                        echo $_POST['displacement'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(1598)</span> <br> <span class="error"><?php echo form_error('displacement'); ?></span>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="cylinders">No of Cylinders </label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="cylinders" id="cylinders" value="<?php
                                                                    if (!isset($_POST['cylinders'])) {
                                                                        if ($results_up[1]['cylinders'] != '') {
                                                                            echo $results_up[1]['cylinders'];
                                                                        }
                                                                        else if ($results_up_copy[1]['cylinders'] != '') {
                                                                            echo $results_up_copy[1]['cylinders'];
                                                                        }
                                                                    } else if (isset($_POST['cylinders'])) {
                                                                        echo $_POST['cylinders'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(4)</span> <br> <span class="error"><?php echo form_error('cylinders'); ?></span>

                                                                </div>
                                                                
                                                                
                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_emission">Emission Norms</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_emission" id="engine_emission" value="<?php
                                                                    if (!isset($_POST['engine_emission'])) {
                                                                        if ($results_up[1]['engine_emission'] != '') {
                                                                            echo $results_up[1]['engine_emission'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_emission'] != '') {
                                                                            echo $results_up_copy[1]['engine_emission'];
                                                                        }
                                                                    } else if (isset($_POST['engine_emission'])) {
                                                                        echo $_POST['engine_emission'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(BSIV)</span> <br> <span class="error"><?php echo form_error('engine_emission'); ?></span>

                                                                </div>

                                                            </div>

                                                            <div class="space-4"></div>                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_maximum_torque">Torque (Nm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_maximum_torque" id="engine_maximum_torque" value="<?php
                                                                    if (!isset($_POST['engine_maximum_torque'])) {
                                                                        if ($results_up[1]['engine_maximum_torque'] != '') {
                                                                            echo $results_up[1]['engine_maximum_torque'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_maximum_torque'] != '') {
                                                                            echo $results_up_copy[1]['engine_maximum_torque'];
                                                                        }
                                                                    } else if (isset($_POST['engine_maximum_torque'])) {
                                                                        echo $_POST['engine_maximum_torque'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(153@3800)</span> <br> <span class="error"><?php echo form_error('engine_maximum_torque'); ?></span>

                                                                </div>




                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_maximum_power">Power (bhp)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_maximum_power" id="engine_maximum_power" value="<?php
                                                                    if (!isset($_POST['engine_maximum_power'])) {
                                                                        if ($results_up[1]['engine_maximum_power'] != '') {
                                                                            echo $results_up[1]['engine_maximum_power'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_maximum_power'] != '') {
                                                                            echo $results_up_copy[1]['engine_maximum_power'];
                                                                        }
                                                                    } else if (isset($_POST['engine_maximum_power'])) {
                                                                        echo $_POST['engine_maximum_power'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(105)</span> <br> <span class="error"><?php echo form_error('engine_maximum_power'); ?></span>

                                                                </div>

                                                            </div>
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="pro_detail">Description</label>
                                                                <div class="col-sm-9">
                                                                    <textarea id="pro_detail" name="pro_detail" style="width:77%;"><?php
                                                                        if (!isset($_POST['pro_detail'])) {
                                                                            if ($results_up[0]['pro_detail'] != '') {
                                                                                echo $results_up[0]['pro_detail'];
                                                                            }
                                                                            else if ($results_up_copy[0]['pro_detail'] != '') {
                                                                                echo $results_up_copy[0]['pro_detail'];
                                                                            }
                                                                        } else if (isset($_POST['pro_detail'])) {
                                                                            echo $_POST['pro_detail'];
                                                                        }
                                                                        ?></textarea>
                                                                      <br> <span class="error"><?php echo form_error('pro_detail'); ?></span>

                                                                </div>                                                                
                                                            </div>

                                                            <div class="space-4"></div>   
                                                            
                                                            
                                                            
                                                            <h4 class="header blue bolder smaller">Transmission</h4>


                                                            <div class="form-group">
                                                            <label class="col-sm-3 control-label no-padding-right" for="tramission_type">Transmission Type</label>
                                                                <div class="col-sm-3">
                                                                    <?php
                                                                    $trans_type=array('Manual', 'Automatic', 'Semi-Automatic');
                                                                    ?>
                                                                    <select name="tramission_type" id="tramission_type" style="width:180px;">
                                                                        <option  value="">Select Transmission Type</option>
                                                                        <?php
                                                                        foreach ($trans_type as $trans_type_res) {
                                                                            ?>
                                                                            <option <?php
                                                                            if ($results_up[0]['tramission_type'] == preg_replace('/[^a-zA-Z0-9]+/', '-', $trans_type_res)) {
                                                                                echo 'selected';
                                                                            }
                                                                            else if ($results_up_copy[0]['tramission_type'] == preg_replace('/[^a-zA-Z0-9]+/', '-', $trans_type_res)) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>
                                                                                value="<?php echo $trans_type_res; ?>"><?php echo $trans_type_res; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                    </select>  
                                                                     <br> <span class="error"><?php echo form_error('tramission_type'); ?></span>
                                                                </div> 
                                                            <label class="col-sm-3 control-label no-padding-right" for="no_gear">No of gears(Gears)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="no_gear" id="no_gear" value="<?php
                                                                    if (!isset($_POST['no_gear'])) {
                                                                        if ($results_up[1]['no_gear'] != '') {
                                                                            echo $results_up[1]['no_gear'];
                                                                        }
                                                                        else if($results_up_copy[1]['no_gear'] != '') {
                                                                            echo $results_up_copy[1]['no_gear'];
                                                                        }
                                                                    } else if (isset($_POST['no_gear'])) {
                                                                        echo $_POST['no_gear'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(5/M)</span> <br> <span class="error"><?php echo form_error('no_gear'); ?></span>
                                                                </div>  
                                                            </div>
                                                            <div class="form-group">
                                                            <label class="col-sm-3 control-label no-padding-right" for="drivetrain">Drivetrain</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="drivetrain" id="drivetrain" value="<?php
                                                                    if (!isset($_POST['drivetrain'])) {
                                                                        if ($results_up[1]['drivetrain'] != '') {
                                                                            echo $results_up[1]['drivetrain'];
                                                                        }
                                                                        else if ($results_up_copy[1]['drivetrain'] != '') {
                                                                            echo $results_up_copy[1]['drivetrain'];
                                                                        }
                                                                    } else if (isset($_POST['drivetrain'])) {
                                                                        echo $_POST['drivetrain'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Front wheel drive)</span> <br> <span class="error"><?php echo form_error('drivetrain'); ?></span>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            <h4 class="header blue bolder smaller">Dimensions</h4>


                                                            <div class="form-group">
                                                            <label class="col-sm-3 control-label no-padding-right" for="dimensions_mm_length">Length (mm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="dimensions_mm_length" id="dimensions_mm_length" value="<?php
                                                                    if (!isset($_POST['dimensions_mm_length'])) {
                                                                        if ($results_up[1]['dimensions_mm_length'] != '') {
                                                                            echo $results_up[1]['dimensions_mm_length'];
                                                                        }
                                                                        if ($results_up_copy[1]['dimensions_mm_length'] != '') {
                                                                            echo $results_up_copy[1]['dimensions_mm_length'];
                                                                        }
                                                                    } else if (isset($_POST['dimensions_mm_length'])) {
                                                                        echo $_POST['dimensions_mm_length'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(4386)</span> <br> <span class="error"><?php echo form_error('dimensions_mm_length'); ?></span>
                                                                </div>
                                                                <label class="col-sm-3 control-label no-padding-right" for="dimensions_mm_width">Width (mm) </label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="dimensions_mm_width" id="dimensions_mm_width" value="<?php
                                                                    if (!isset($_POST['dimensions_mm_width'])) {
                                                                        if ($results_up[1]['dimensions_mm_width'] != '') {
                                                                            echo $results_up[1]['dimensions_mm_width'];
                                                                        }
                                                                        if ($results_up_copy[1]['dimensions_mm_width'] != '') {
                                                                            echo $results_up_copy[1]['dimensions_mm_width'];
                                                                        }
                                                                    } else if (isset($_POST['dimensions_mm_width'])) {
                                                                        echo $_POST['dimensions_mm_width'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(1699)</span> <br> <span class="error"><?php echo form_error('dimensions_mm_width'); ?></span>

                                                                </div>
                                                            
                                                            </div>
                                                            
                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="dimensions_mm_height">Height (mm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="dimensions_mm_height" id="dimensions_mm_height" value="<?php
                                                                    if (!isset($_POST['dimensions_mm_height'])) {
                                                                        if ($results_up[1]['dimensions_mm_height'] != '') {
                                                                            echo $results_up[1]['dimensions_mm_height'];
                                                                        }
                                                                        if ($results_up_copy[1]['dimensions_mm_height'] != '') {
                                                                            echo $results_up_copy[1]['dimensions_mm_height'];
                                                                        }
                                                                    } else if (isset($_POST['dimensions_mm_height'])) {
                                                                        echo $_POST['dimensions_mm_height'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(110)</span> <br> <span class="error"><?php echo form_error('dimensions_mm_height'); ?></span>

                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="dimensions_mm_weelbase">Wheelbase (mm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="dimensions_mm_weelbase" id="dimensions_mm_weelbase" value="<?php
                                                                    if (!isset($_POST['dimensions_mm_weelbase'])) {
                                                                        if ($results_up[1]['dimensions_mm_weelbase'] != '') {
                                                                            echo $results_up[1]['dimensions_mm_weelbase'];
                                                                        }
                                                                        if ($results_up_copy[1]['dimensions_mm_weelbase'] != '') {
                                                                            echo $results_up_copy[1]['dimensions_mm_weelbase'];
                                                                        }
                                                                    } else if (isset($_POST['dimensions_mm_weelbase'])) {
                                                                        echo $_POST['dimensions_mm_weelbase'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(120)</span> <br> <span class="error"><?php echo form_error('dimensions_mm_weelbase'); ?></span>

                                                                </div>
                                                            </div>

                                                            
                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="dimensions_mm_height">Rear seat legroom (cm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="rear_seat_legroom" id="rear_seat_legroom" value="<?php
                                                                    if (!isset($_POST['rear_seat_legroom'])) {
                                                                        if ($results_up[1]['rear_seat_legroom'] != '') {
                                                                            echo $results_up[1]['rear_seat_legroom'];
                                                                        }
                                                                        if ($results_up_copy[1]['rear_seat_legroom'] != '') {
                                                                            echo $results_up_copy[1]['rear_seat_legroom'];
                                                                        }
                                                                    } else if (isset($_POST['rear_seat_legroom'])) {
                                                                        echo $_POST['rear_seat_legroom'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(70/93)</span> <br> <span class="error"><?php echo form_error('rear_seat_legroom'); ?></span>

                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="headroom">Headroom (cm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="headroom" id="headroom" value="<?php
                                                                    if (!isset($_POST['headroom'])) {
                                                                        if ($results_up[1]['headroom'] != '') {
                                                                            echo $results_up[1]['headroom'];
                                                                        }
                                                                        if ($results_up_copy[1]['headroom'] != '') {
                                                                            echo $results_up_copy[1]['headroom'];
                                                                        }
                                                                    } else if (isset($_POST['headroom'])) {
                                                                        echo $_POST['headroom'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(91)</span> <br> <span class="error"><?php echo form_error('headroom'); ?></span>

                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="dimensions_mm_height">Seat Width (cm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="seat_width" id="seat_width" value="<?php
                                                                    if (!isset($_POST['seat_width'])) {
                                                                        if ($results_up[1]['seat_width'] != '') {
                                                                            echo $results_up[1]['seat_width'];
                                                                        }
                                                                        if ($results_up_copy[1]['seat_width'] != '') {
                                                                            echo $results_up_copy[1]['seat_width'];
                                                                        }
                                                                    } else if (isset($_POST['seat_width'])) {
                                                                        echo $_POST['seat_width'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(70/93)</span> <br> <span class="error"><?php echo form_error('seat_width'); ?></span>

                                                                </div>

                                                               <label class="col-sm-3 control-label no-padding-right" for="dimensions_mm_ground_clearance">Ground clearance (mm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="dimensions_mm_ground_clearance" id="dimensions_mm_ground_clearance" value="<?php
                                                                    if (!isset($_POST['dimensions_mm_ground_clearance'])) {
                                                                        if ($results_up[1]['dimensions_mm_ground_clearance'] != '') {
                                                                            echo $results_up[1]['dimensions_mm_ground_clearance'];
                                                                        }
                                                                        if ($results_up_copy[1]['dimensions_mm_ground_clearance'] != '') {
                                                                            echo $results_up_copy[1]['dimensions_mm_ground_clearance'];
                                                                        }
                                                                    } else if (isset($_POST['dimensions_mm_ground_clearance'])) {
                                                                        echo $_POST['dimensions_mm_ground_clearance'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(123)</span> <br> <span class="error"><?php echo form_error('dimensions_mm_ground_clearance'); ?></span>
                                                                </div>
                                                            </div>

                                                            


                                                            <div class="form-group">

                                                                

<!--                                                                <label class="col-sm-3 control-label no-padding-right" for="kerb_weight">Kerb Weight</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="kerb_weight" id="kerb_weight" value="<?php
                                                                    if (!isset($_POST['kerb_weight'])) {
                                                                        if ($results_up[1]['kerb_weight'] != '') {
                                                                            echo $results_up[1]['kerb_weight'];
                                                                        }
                                                                        if ($results_up_copy[1]['kerb_weight'] != '') {
                                                                            echo $results_up_copy[1]['kerb_weight'];
                                                                        }
                                                                    } else if (isset($_POST['kerb_weight'])) {
                                                                        echo $_POST['kerb_weight'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('kerb_weight'); ?></span>

                                                                </div>-->
                                                            </div>

                                                            <div class="space-4"></div>  
                                                            
                                                            
                                                             <h4 class="header blue bolder smaller">Tyres</h4>
                                                             <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="tyre_size">Tire Size</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="tyre_size" id="tyre_size" value="<?php
                                                                    if (!isset($_POST['tyre_size'])) {
                                                                        if ($results_up[1]['tyre_size'] != '') {
                                                                            echo $results_up[1]['tyre_size'];
                                                                        }
                                                                        if ($results_up_copy[1]['tyre_size'] != '') {
                                                                            echo $results_up_copy[1]['tyre_size'];
                                                                        }
                                                                    } else if (isset($_POST['tyre_size'])) {
                                                                        echo $_POST['tyre_size'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(185/60 R15)</span> <br> <span class="error"><?php echo form_error('tyre_size'); ?></span>
                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="wheel_size">Wheel Size</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="wheel_size" id="wheel_size" value="<?php
                                                                    if (!isset($_POST['wheel_size'])) {
                                                                        if ($results_up[1]['wheel_size'] != '') {
                                                                            echo $results_up[1]['wheel_size'];
                                                                        }
                                                                        if ($results_up_copy[1]['wheel_size'] != '') {
                                                                            echo $results_up_copy[1]['wheel_size'];
                                                                        }
                                                                    } else if (isset($_POST['wheel_size'])) {
                                                                        echo $_POST['wheel_size'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(15")</span> <br> <span class="error"><?php echo form_error('wheel_size'); ?></span>

                                                                </div>

                                                            </div>


                                                            <div class="space-4"></div>   
                                                            
                                                            
                                                             <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="spare_tyre">Spare Tyre</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="spare_type" id="spare_type" value="<?php
                                                                    if (!isset($_POST['spare_type'])) {
                                                                        if ($results_up[1]['spare_type'] != '') {
                                                                            echo $results_up[1]['spare_type'];
                                                                        }
                                                                        if ($results_up_copy[1]['spare_type'] != '') {
                                                                            echo $results_up_copy[1]['spare_type'];
                                                                        }
                                                                    } else if (isset($_POST['spare_type'])) {
                                                                        echo $_POST['spare_type'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Y)</span> <br> <span class="error"><?php echo form_error('spare_type'); ?></span>
                                                                </div>

                                                            </div>
                                                            
                                                            
                                                            
                                                             <h4 class="header blue bolder smaller">Steering</h4>
                                                             <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="streeing_power_type">Steering Type</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="streeing_power_type" id="streeing_power_type" value="<?php
                                                                    if (!isset($_POST['streeing_power_type'])) {
                                                                        if ($results_up[1]['streeing_power_type'] != '') {
                                                                            echo $results_up[1]['streeing_power_type'];
                                                                        }
                                                                        if ($results_up_copy[1]['streeing_power_type'] != '') {
                                                                            echo $results_up_copy[1]['streeing_power_type'];
                                                                        }
                                                                    } else if (isset($_POST['streeing_power_type'])) {
                                                                        echo $_POST['streeing_power_type'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(121)</span> <br> <span class="error"><?php echo form_error('streeing_power_type'); ?></span>
                                                                </div> 

                                                                <label class="col-sm-3 control-label no-padding-right" for="streeing_turn_radius">Turning Radius (m)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="streeing_turn_radius" id="streeing_turn_radius" value="<?php
                                                                    if (!isset($_POST['streeing_turn_radius'])) {
                                                                        if ($results_up[1]['streeing_turn_radius'] != '') {
                                                                            echo $results_up[1]['streeing_turn_radius'];
                                                                        }
                                                                        if ($results_up_copy[1]['streeing_turn_radius'] != '') {
                                                                            echo $results_up_copy[1]['streeing_turn_radius'];
                                                                        }
                                                                    } else if (isset($_POST['streeing_turn_radius'])) {
                                                                        echo $_POST['streeing_turn_radius'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(5.3)</span> <br> <span class="error"><?php echo form_error('streeing_turn_radius'); ?></span>
                                                                </div>


                                                            </div>
                                                             <h4 class="header blue bolder smaller">Capacity
                                                             </h4>
                                                             <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="body_seating_capacity">Seating capacity</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="body_seating_capacity" id="body_seating_capacity" value="<?php
                                                                    if (!isset($_POST['body_seating_capacity'])) {
                                                                        if ($results_up[1]['body_seating_capacity'] != '') {
                                                                            echo $results_up[1]['body_seating_capacity'];
                                                                        }
                                                                        else if($results_up_copy[1]['body_seating_capacity'] != '') {
                                                                            echo $results_up_copy[1]['body_seating_capacity'];
                                                                        }
                                                                    } else if (isset($_POST['body_seating_capacity'])) {
                                                                        echo $_POST['body_seating_capacity'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(5)</span> <br> <span class="error"><?php echo form_error('body_seating_capacity'); ?></span>

                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="body_seating_capacity">Trunk Volume (l)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="trunk_volume" id="trunk_volume" value="<?php
                                                                    if (!isset($_POST['trunk_volume'])) {
                                                                        if ($results_up[1]['trunk_volume'] != '') {
                                                                            echo $results_up[1]['trunk_volume'];
                                                                        }
                                                                        else if($results_up_copy[1]['trunk_volume'] != '') {
                                                                            echo $results_up_copy[1]['trunk_volume'];
                                                                        }
                                                                    } else if (isset($_POST['trunk_volume'])) {
                                                                        echo $_POST['trunk_volume'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(5)</span> <br> <span class="error"><?php echo form_error('trunk_volume'); ?></span>

                                                                </div>


                                                            </div>
                                                             
                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="fuel_tank_capacity1">Fuel Tank (l)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="fuel_tank_capacity1" id="fuel_tank" value="<?php
                                                                    if (!isset($_POST['fuel_tank_capacity1'])) {
                                                                        if ($results_up[1]['fuel_tank_capacity1'] != '') {
                                                                            echo $results_up[1]['fuel_tank_capacity1'];
                                                                        }
                                                                        else if($results_up_copy[1]['fuel_tank_capacity1'] != '') {
                                                                            echo $results_up_copy[1]['fuel_tank_capacity1'];
                                                                        }
                                                                    } else if (isset($_POST['fuel_tank_capacity1'])) {
                                                                        echo $_POST['fuel_tank_capacity1'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(5)</span> <br> <span class="error"><?php echo form_error('fuel_tank_capacity1'); ?></span>

                                                                </div>

                                                                


                                                            </div>
                                                             
                                                             
                                                             <h4 class="header blue bolder smaller">Performance</h4>
                                                             <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="body_seating_capacity">Top Speed</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="top_speed" id="top_speed" value="<?php
                                                                    if (!isset($_POST['top_speed'])) {
                                                                        if ($results_up[1]['top_speed'] != '') {
                                                                            echo $results_up[1]['top_speed'];
                                                                        }
                                                                        else if($results_up_copy[1]['top_speed'] != '') {
                                                                            echo $results_up_copy[1]['top_speed'];
                                                                        }
                                                                    } else if (isset($_POST['top_speed'])) {
                                                                        echo $_POST['top_speed'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(183)</span> <br> <span class="error"><?php echo form_error('top_speed'); ?></span>

                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="acceleration">Acceleration ( 0-100 kmph)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="acceleration" id="acceleration" value="<?php
                                                                    if (!isset($_POST['acceleration'])) {
                                                                        if ($results_up[1]['acceleration'] != '') {
                                                                            echo $results_up[1]['acceleration'];
                                                                        }
                                                                        else if($results_up_copy[1]['acceleration'] != '') {
                                                                            echo $results_up_copy[1]['acceleration'];
                                                                        }
                                                                    } else if (isset($_POST['acceleration'])) {
                                                                        echo $_POST['acceleration'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(5)</span> <br> <span class="error"><?php echo form_error('acceleration'); ?></span>

                                                                </div>


                                                            </div>
                                                             
                                                            
                                                             <h4 class="header blue bolder smaller">Brakes</h4>
                                                             <div class="form-group">
                                                             <label class="col-sm-3 control-label no-padding-right" for="brakes_front">Brakes Front</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="brakes_front" id="brakes_front" value="<?php
                                                                    if (!isset($_POST['brakes_front'])) {
                                                                        if ($results_up[1]['brakes_front'] != '') {
                                                                            echo $results_up[1]['brakes_front'];
                                                                        }
                                                                       else if ($results_up_copy[1]['brakes_front'] != '') {
                                                                            echo $results_up_copy[1]['brakes_front'];
                                                                        }
                                                                    } else if (isset($_POST['brakes_front'])) {
                                                                        echo $_POST['brakes_front'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Disc)</span> <br> <span class="error"><?php echo form_error('brakes_front'); ?></span>
                                                                </div>
                                                                
                                                              <label class="col-sm-3 control-label no-padding-right" for="brakes_rear">Brakes Rear</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="brakes_rear" id="brakes_rear" value="<?php
                                                                    if (!isset($_POST['brakes_rear'])) {
                                                                        if ($results_up[1]['brakes_rear'] != '') {
                                                                            echo $results_up[1]['brakes_rear'];
                                                                        }
                                                                        else if ($results_up_copy[1]['brakes_rear'] != '') {
                                                                            echo $results_up_copy[1]['brakes_rear'];
                                                                        }
                                                                    } else if (isset($_POST['brakes_rear'])) {
                                                                        echo $_POST['brakes_rear'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Drum)</span> <br> <span class="error"><?php echo form_error('brakes_rear'); ?></span>
                                                                </div>
                                                             
                                                                


                                                            </div>
                                                             
                                                            
                                                             <h4 class="header blue bolder smaller">Suspension</h4>
                                                             <div class="form-group">

                                                                    <label class="col-sm-3 control-label no-padding-right" for="suspension_front">Suspension front</label>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" name="suspension_front" id="suspension_front" value="<?php
                                                                        if (!isset($_POST['suspension_front'])) {
                                                                            if ($results_up[1]['suspension_front'] != '') {
                                                                                echo $results_up[1]['suspension_front'];
                                                                            }
                                                                            else if ($results_up_copy[1]['suspension_front'] != '') {
                                                                                echo $results_up_copy[1]['suspension_front'];
                                                                            }
                                                                        } else if (isset($_POST['suspension_front'])) {
                                                                            echo $_POST['suspension_front'];
                                                                        }
                                                                        ?>" >                                                                
                                                                         <span class="sample_data">(Mcpherson Strut with Lower Traingular links and Torsion stabiliser)</span> <br> <span class="error"><?php echo form_error('suspension_front'); ?></span>

                                                                    </div>

                                                                    <label class="col-sm-3 control-label no-padding-right" for="suspension_rear">Suspension rear</label>
                                                                    <div class="col-sm-3">
                                                                        <input type="text" name="suspension_rear" id="suspension_rear" value="<?php
                                                                        if (!isset($_POST['suspension_rear'])) {
                                                                            if ($results_up[1]['suspension_rear'] != '') {
                                                                                echo $results_up[1]['suspension_rear'];
                                                                            }
                                                                            else if ($results_up_copy[1]['suspension_rear'] != '') {
                                                                                echo $results_up_copy[1]['suspension_rear'];
                                                                            }
                                                                        } else if (isset($_POST['suspension_rear'])) {
                                                                            echo $_POST['suspension_rear'];
                                                                        }
                                                                        ?>" >                                                                
                                                                         <span class="sample_data">(Compound link crank axle)</span> <br> <span class="error"><?php echo form_error('suspension_rear'); ?></span>
                                                                    </div>
                                                                </div>
                                                             
                                                            
                                                             <h4 class="header blue bolder smaller">Weight</h4>
                                                             <div class="form-group">
                                                                    <label class="col-sm-3 control-label no-padding-right" for="kerb_weight">Kerb Weight</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="kerb_weight" id="kerb_weight" value="<?php
                                                                    if (!isset($_POST['kerb_weight'])) {
                                                                        if ($results_up[1]['kerb_weight'] != '') {
                                                                            echo $results_up[1]['kerb_weight'];
                                                                        }
                                                                        if ($results_up_copy[1]['kerb_weight'] != '') {
                                                                            echo $results_up_copy[1]['kerb_weight'];
                                                                        }
                                                                    } else if (isset($_POST['kerb_weight'])) {
                                                                        echo $_POST['kerb_weight'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(1145)</span> <br> <span class="error"><?php echo form_error('kerb_weight'); ?></span>

                                                                </div>
                                                                    
                                                                </div>
                                                             
                                                            
                                                             <h4 class="header blue bolder smaller">Warranty </h4>
                                                             <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="kerb_weight">Standard Warranty </label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="standard_warranty" id="kerb_weight" value="<?php
                                                                    if (!isset($_POST['standard_warranty'])) {
                                                                        if ($results_up[1]['standard_warranty'] != '') {
                                                                            echo $results_up[1]['standard_warranty'];
                                                                        }
                                                                        if ($results_up_copy[1]['standard_warranty'] != '') {
                                                                            echo $results_up_copy[1]['standard_warranty'];
                                                                        }
                                                                    } else if (isset($_POST['standard_warranty'])) {
                                                                        echo $_POST['standard_warranty'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(2 years/ Unlimited Kms)</span> <br> <span class="error"><?php echo form_error('standard_warranty'); ?></span>

                                                                </div>
                                                                    
                                                                </div>
                                                             
                                                             
                                                                                                                          
                                                            
                                                            
                                                                                                                    


<!--                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_no_of_valves">Engine no of valves</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_no_of_valves" id="engine_no_of_valves" value="<?php
                                                                    if (!isset($_POST['engine_no_of_valves'])) {
                                                                        if ($results_up[1]['engine_no_of_valves'] != '') {
                                                                            echo $results_up[1]['engine_no_of_valves'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_no_of_valves'] != '') {
                                                                            echo $results_up_copy[1]['engine_no_of_valves'];
                                                                        }
                                                                    } else if (isset($_POST['engine_no_of_valves'])) {
                                                                        echo $_POST['engine_no_of_valves'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('engine_no_of_valves'); ?></span>
                                                                </div>
                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_bore_stroke">Engine Broke stoke</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_bore_stroke" id="engine_bore_stroke" value="<?php
                                                                    if (!isset($_POST['engine_bore_stroke'])) {
                                                                        if ($results_up[1]['engine_bore_stroke'] != '') {
                                                                            echo $results_up[1]['engine_bore_stroke'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_bore_stroke'] != '') {
                                                                            echo $results_up_copy[1]['engine_bore_stroke'];
                                                                        }
                                                                    } else if (isset($_POST['engine_bore_stroke'])) {
                                                                        echo $_POST['engine_bore_stroke'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('engine_bore_stroke'); ?></span>

                                                                </div>
                                                            </div>-->

                                                            
                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="mileage">IDC Mileage (km/litre)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="mileage" id="mileage" value="<?php
                                                                    if (!isset($_POST['mileage'])) {
                                                                        if ($results_up[1]['mileage'] != '') {
                                                                            echo $results_up[1]['mileage'];
                                                                        }
                                                                        else if ($results_up_copy[1]['mileage'] != '') {
                                                                            echo $results_up_copy[1]['mileage'];
                                                                        }
                                                                    } else if (isset($_POST['mileage'])) {
                                                                        echo $_POST['mileage'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(15)</span> <br> <span class="error"><?php echo form_error('mileage'); ?></span>
                                                                </div>
<!--                                                                <label class="col-sm-3 control-label no-padding-right" for="alternate_fuel">Alternate Fuel</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="alternate_fuel" id="alternate_fuel" value="<?php
                                                                    if (!isset($_POST['alternate_fuel'])) {
                                                                        if($results_up[1]['alternate_fuel'] != '') {
                                                                            echo $results_up[1]['alternate_fuel'];
                                                                        }
                                                                        else if ($results_up_copy[1]['alternate_fuel'] != '') {
                                                                            echo $results_up_copy[1]['alternate_fuel'];
                                                                        }
                                                                    } else if (isset($_POST['alternate_fuel'])) {
                                                                        echo $_POST['alternate_fuel'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('alternate_fuel'); ?></span>

                                                                </div>-->
                                                            </div>

                                                            <div class="space-4"></div>
                                                            
                                                            
                                                            <h4 class="header blue bolder smaller">Seo Content</h4>
                                                                <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="seo_keyword">Seo Keyword</label>
                                                                <div class="col-sm-9">
                                                                    <textarea style="width:77%;" name="seo_keyword" id="seo_keyword"><?php
                                                                    if (!isset($_POST['seo_keyword'])) {
                                                                        if ($results_up[1]['seo_keyword'] != '') {
                                                                            echo $results_up[1]['seo_keyword'];
                                                                        }
                                                                        if ($results_up_copy[1]['seo_keyword'] != '') {
                                                                            echo $results_up_copy[1]['seo_keyword'];
                                                                        }
                                                                    } else if (isset($_POST['seo_keyword'])) {
                                                                        echo $_POST['seo_keyword'];
                                                                    }
                                                                    ?></textarea>
                                                                    <br> <span class="error"><?php echo form_error('seo_keyword'); ?></span>
                                                                </div>
                                                                </div>
                                                                <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="seo_tag">Seo Tag</label>
                                                                <div class="col-sm-9">
                                                                    <textarea style="width:77%;" name="seo_tag" id="seo_tag"><?php
                                                                    if (!isset($_POST['seo_tag'])) {
                                                                        if ($results_up[1]['seo_tag'] != '') {
                                                                            echo $results_up[1]['seo_tag'];
                                                                        }
                                                                        if ($results_up_copy[1]['seo_tag'] != '') {
                                                                            echo $results_up_copy[1]['seo_tag'];
                                                                        }
                                                                    } else if (isset($_POST['seo_tag'])) {
                                                                        echo $_POST['seo_tag'];
                                                                    }
                                                                    ?></textarea>
                                                                    <br> <span class="error"><?php echo form_error('seo_tag'); ?></span>
                                                                </div>
                                                                </div>
                                                                <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="seo_description">Seo Description</label>
                                                                <div class="col-sm-9">
                                                                    <textarea style="width:77%;" name="seo_description" id="seo_description"><?php
                                                                    if (!isset($_POST['seo_description'])) {
                                                                        if ($results_up[1]['seo_description'] != '') {
                                                                            echo $results_up[1]['seo_description'];
                                                                        }
                                                                        if ($results_up_copy[1]['seo_description'] != '') {
                                                                            echo $results_up_copy[1]['seo_description'];
                                                                        }
                                                                    } else if (isset($_POST['seo_description'])) {
                                                                        echo $_POST['seo_description'];
                                                                    }
                                                                    ?></textarea>
                                                                    <br> <span class="error"><?php echo form_error('seo_description'); ?></span>
                                                                </div>
                                                                </div>



<!--                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="bore_stroke">Bore(mm) x Stroke(mm)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="bore_stroke" id="bore_stroke" value="<?php
                                                                    if (!isset($_POST['bore_stroke'])) {
                                                                        if ($results_up[1]['bore_stroke'] != '') {
                                                                            echo $results_up[1]['bore_stroke'];
                                                                        }
                                                                        else if ($results_up_copy[1]['bore_stroke'] != '') {
                                                                            echo $results_up_copy[1]['bore_stroke'];
                                                                        }
                                                                    } else if (isset($_POST['bore_stroke'])) {
                                                                        echo $_POST['bore_stroke'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('bore_stroke'); ?></span>
                                                                </div>
                                                                <label class="col-sm-3 control-label no-padding-right" for="comparision_ratio">Compression Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="comparision_ratio" id="comparision_ratio" value="<?php
                                                                    if (!isset($_POST['comparision_ratio'])) {
                                                                        if ($results_up[1]['comparision_ratio'] != '') {
                                                                            echo $results_up[1]['comparision_ratio'];
                                                                        }
                                                                        else if ($results_up_copy[1]['comparision_ratio'] != '') {
                                                                            echo $results_up_copy[1]['comparision_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['comparision_ratio'])) {
                                                                        echo $_POST['comparision_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('comparision_ratio'); ?></span>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>                                                  -->


                                                            <!--div class="form-group">

                                                               <label class="col-sm-3 control-label no-padding-right" for="engine_compression_ratio">Engine Compression Ratio</label>
                                                                
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_compression_ratio" id="engine_compression_ratio" value="<?php
                                                            if (!isset($_POST['engine_compression_ratio'])) {
                                                                if ($results_up[1]['engine_compression_ratio'] != '') {
                                                                    echo $results_up[1]['engine_compression_ratio'];
                                                                }
                                                            } else if (isset($_POST['engine_compression_ratio'])) {
                                                                echo $_POST['engine_compression_ratio'];
                                                            }
                                                            ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('engine_compression_ratio'); ?></span>
                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_fuel_distribution">Engine Fuel Distribution</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_fuel_distribution" id="engine_fuel_distribution" value="<?php
                                                            if (!isset($_POST['engine_fuel_distribution'])) {
                                                                if ($results_up[1]['engine_fuel_distribution'] != '') {
                                                                    echo $results_up[1]['engine_fuel_distribution'];
                                                                }
                                                            } else if (isset($_POST['engine_fuel_distribution'])) {
                                                                echo $_POST['engine_fuel_distribution'];
                                                            }
                                                            ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('engine_fuel_distribution'); ?></span>

                                                                </div>

                                                            </div>

                                                            <div class="space-4"></div-->


<!--                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="valve_cylinder">Valve/Cylinder (Configuration)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="valve_cylinder" id="valve_cylinder" value="<?php
                                                                    if (!isset($_POST['valve_cylinder'])) {
                                                                        if ($results_up[1]['valve_cylinder'] != '') {
                                                                            echo $results_up[1]['valve_cylinder'];
                                                                        }
                                                                        else if ($results_up_copy[1]['valve_cylinder'] != '') {
                                                                            echo $results_up_copy[1]['valve_cylinder'];
                                                                        }
                                                                    } else if (isset($_POST['valve_cylinder'])) {
                                                                        echo $_POST['valve_cylinder'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('valve_cylinder'); ?></span>
                                                                </div>

                                                                

                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="fuel_system">Fuel System</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="fuel_system" id="fuel_system" value="<?php
                                                                    if (!isset($_POST['fuel_system'])) {
                                                                        if ($results_up[1]['fuel_system'] != '') {
                                                                            echo $results_up[1]['fuel_system'];
                                                                        }
                                                                        else if ($results_up_copy[1]['fuel_system'] != '') {
                                                                            echo $results_up_copy[1]['fuel_system'];
                                                                        }
                                                                    } else if (isset($_POST['fuel_system'])) {
                                                                        echo $_POST['fuel_system'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('fuel_system'); ?></span>
                                                                </div>

                                                                

                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                                                                            

                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="dual_cluth">Dual Clutch</label>
                                                                <div class="col-sm-3">
                                                                    <?php
                                                                    if(!empty($results_up_copy[1]['dual_cluth']))
                                                                    {    
                                                                    $dual_cluth = $results_up_copy[1]['dual_cluth'];
                                                                    }
                                                                    else {
                                                                    $dual_cluth = $results_up[1]['dual_cluth'];
                                                                    }
                                                                    ?>
                                                                    <select name="dual_cluth" id="dual_cluth" style="width:180px;">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php
                                                                        if (!isset($_POST['dual_cluth'])) {
                                                                            if ($dual_cluth == 1) {
                                                                                echo 'selected';
                                                                            }
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> value="1">Yes</option>    
                                                                        <option <?php
                                                                        if (!isset($_POST['dual_cluth'])) {
                                                                            if ($dual_cluth == 2) {
                                                                                echo 'selected';
                                                                            }
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> value="2">No</option>    
                                                                    </select>  
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('dual_cluth'); ?></span>

                                                                </div>
                                                                <label class="col-sm-3 control-label no-padding-right" for="sport_mode">Sport Mode</label>
                                                                <div class="col-sm-3">
                                                                    <?php
                                                                     if(!empty($results_up_copy[1]['sport_mode']))
                                                                    {    
                                                                    $sport_mode = $results_up_copy[1]['dual_cluth'];
                                                                    }
                                                                    else {
                                                                    $sport_mode = $results_up[1]['sport_mode'];
                                                                    }
                                                                    ?>
                                                                    <select name="sport_mode" id="sport_mode" style="width:180px;">
                                                                        <option  value="">Select Status</option>
                                                                        <option <?php
                                                                        if (!isset($_POST['sport_mode'])) {
                                                                            if ($sport_mode == 1) {
                                                                                echo 'selected';
                                                                            }
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> value="1">Yes</option>    
                                                                        <option <?php
                                                                        if (!isset($_POST['sport_mode'])) {
                                                                            if ($sport_mode == 2) {
                                                                                echo 'selected';
                                                                            }
                                                                        } else {
                                                                            echo '';
                                                                        }
                                                                        ?> value="2">No</option>    
                                                                    </select>  
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('sport_mode'); ?></span>

                                                                </div>
                                                            </div>-->

                                                            
                                                            

<!--                                                            <h4 class="header blue bolder smaller">Capacity</h4>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="body_no_of_doors">Body No's of door</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="body_no_of_doors" id="body_no_of_doors" value="<?php
                                                                    if (!isset($_POST['body_no_of_doors'])) {
                                                                        if ($results_up[1]['body_no_of_doors'] != '') {
                                                                            echo $results_up[1]['body_no_of_doors'];
                                                                        }
                                                                        else if ($results_up_copy[1]['body_no_of_doors'] != '') {
                                                                            echo $results_up_copy[1]['body_no_of_doors'];
                                                                        }
                                                                    } else if (isset($_POST['body_no_of_doors'])) {
                                                                        echo $_POST['body_no_of_doors'];
                                                                    }
                                                                    ?>" >  <span class="sample_data">(Sample Data)</span>                                                              
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('body_no_of_doors'); ?></span>

                                                                </div>
                                                                

                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_capacity">Engine capacity</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_capacity" id="engine_capacity" value="<?php
                                                                    if (!isset($_POST['engine_capacity'])) {
                                                                        if ($results_up[1]['engine_capacity'] != '') {
                                                                            echo $results_up[1]['engine_capacity'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_capacity'] != '') {
                                                                            echo $results_up_copy[1]['engine_capacity'];
                                                                        }
                                                                    } else if (isset($_POST['engine_capacity'])) {
                                                                        echo $_POST['engine_capacity'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('engine_capacity'); ?></span>

                                                                </div>
                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_no_of_cylinder">Engine no of cylinder</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_no_of_cylinder" id="engine_no_of_cylinder" value="<?php
                                                                    if (!isset($_POST['engine_no_of_cylinder'])) {
                                                                        if ($results_up[1]['engine_no_of_cylinder'] != '') {
                                                                            echo $results_up[1]['engine_no_of_cylinder'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_no_of_cylinder'] != '') {
                                                                            echo $results_up_copy[1]['engine_no_of_cylinder'];
                                                                        }
                                                                    } else if (isset($_POST['engine_no_of_cylinder'])) {
                                                                        echo $_POST['engine_no_of_cylinder'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('engine_no_of_cylinder'); ?></span>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="no_seating_rows">No of Seating Rows</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="no_seating_rows" id="no_seating_rows" value="<?php
                                                                    if (!isset($_POST['no_seating_rows'])) {
                                                                        if ($results_up[1]['no_seating_rows'] != '') {
                                                                            echo $results_up[1]['no_seating_rows'];
                                                                        }
                                                                        else if ($results_up_copy[1]['no_seating_rows'] != '') {
                                                                            echo $results_up_copy[1]['no_seating_rows'];
                                                                        }
                                                                    } else if (isset($_POST['no_seating_rows'])) {
                                                                        echo $_POST['no_seating_rows'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('no_seating_rows'); ?></span>

                                                                </div>
                                                                <label class="col-sm-3 control-label no-padding-right" for="bootspace">Bootspace(liters)</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="bootspace" id="bootspace" value="<?php
                                                                    if (!isset($_POST['bootspace'])) {
                                                                        if ($results_up[1]['bootspace'] != '') {
                                                                            echo $results_up[1]['bootspace'];
                                                                        }
                                                                        else if ($results_up_copy[1]['bootspace'] != '') {
                                                                            echo $results_up_copy[1]['bootspace'];
                                                                        }
                                                                    } else if (isset($_POST['bootspace'])) {
                                                                        echo $_POST['bootspace'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('bootspace'); ?></span>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="fuel_tank_capacity1">Fuel tank capacity</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="fuel_tank_capacity1" id="fuel_tank_capacity1" value="<?php
                                                                    if (!isset($_POST['fuel_tank_capacity1'])) {
                                                                        if ($results_up[1]['fuel_tank_capacity1'] != '') {
                                                                            echo $results_up[1]['fuel_tank_capacity1'];
                                                                        }
                                                                        else if ($results_up_copy[1]['fuel_tank_capacity1'] != '') {
                                                                            echo $results_up_copy[1]['fuel_tank_capacity1'];
                                                                        }
                                                                    } else if (isset($_POST['fuel_tank_capacity1'])) {
                                                                        echo $_POST['fuel_tank_capacity1'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('fuel_tank_capacity1'); ?></span>

                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="boot_capacity1">Boot Capacity</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="boot_capacity1" id="boot_capacity1" value="<?php
                                                                    if (!isset($_POST['boot_capacity1'])) {
                                                                        if ($results_up[1]['boot_capacity1'] != '') {
                                                                            echo $results_up[1]['boot_capacity1'];
                                                                        }
                                                                        else if ($results_up_copy[1]['boot_capacity1'] != '') {
                                                                            echo $results_up_copy[1]['boot_capacity1'];
                                                                        }
                                                                    } else if (isset($_POST['boot_capacity1'])) {
                                                                        echo $_POST['boot_capacity1'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('boot_capacity1'); ?></span>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>

                                                            

                                                            <h4 class="header blue bolder smaller">Suspensions, Brakes, Steering & Tyres</h4>



                                                            <div class="form-group">
                                                                

                                                                <div class="space-4"></div>




                                                                <label class="col-sm-3 control-label no-padding-right" for="engine_air_charging">Engine air charging</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="engine_air_charging" id="engine_air_charging" value="<?php
                                                                    if (!isset($_POST['engine_air_charging'])) {
                                                                        if ($results_up[1]['engine_air_charging'] != '') {
                                                                            echo $results_up[1]['engine_air_charging'];
                                                                        }
                                                                        else if ($results_up_copy[1]['engine_air_charging'] != '') {
                                                                            echo $results_up_copy[1]['engine_air_charging'];
                                                                        }
                                                                    } else if (isset($_POST['engine_air_charging'])) {
                                                                        echo $_POST['engine_air_charging'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('engine_air_charging'); ?></span>
                                                                </div>

                                                                

                                                            </div>

                                                            <div class="space-4"></div>

                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="gross_vehicle_weight">gross vehicle Weight</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="gross_vehicle_weight" id="gross_vehicle_weight" value="<?php
                                                                    if (!isset($_POST['gross_vehicle_weight'])) {
                                                                        if ($results_up[1]['gross_vehicle_weight'] != '') {
                                                                            echo $results_up[1]['gross_vehicle_weight'];
                                                                        }
                                                                        else if ($results_up_copy[1]['gross_vehicle_weight'] != '') {
                                                                            echo $results_up[1]['gross_vehicle_weight'];
                                                                        }
                                                                    } else if (isset($_POST['gross_vehicle_weight'])) {
                                                                        echo $_POST['gross_vehicle_weight'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('gross_vehicle_weight'); ?></span>

                                                                </div>

                                                                
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_drive">Transmission Drive</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_drive" id="transmission_drive" value="<?php
                                                                    if (!isset($_POST['transmission_drive'])) {
                                                                        if ($results_up[1]['transmission_drive'] != '') {
                                                                            echo $results_up[1]['transmission_drive'];
                                                                        }
                                                                        else if ($results_up_copy[1]['transmission_drive'] != '') {
                                                                            echo $results_up_copy[1]['transmission_drive'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_drive'])) {
                                                                        echo $_POST['transmission_drive'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_drive'); ?></span>
                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_clutch_type">Transmission Clutch type</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_clutch_type" id="transmission_clutch_type" value="<?php
                                                                    if (!isset($_POST['transmission_clutch_type'])) {
                                                                        if ($results_up[1]['transmission_clutch_type'] != '') {
                                                                            echo $results_up[1]['transmission_clutch_type'];
                                                                        }
                                                                        else if ($results_up_copy[1]['transmission_clutch_type'] != '') {
                                                                            echo $results_up_copy[1]['transmission_clutch_type'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_clutch_type'])) {
                                                                        echo $_POST['transmission_clutch_type'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_clutch_type'); ?></span>
                                                                </div>


                                                            </div>




                                                            <div class="space-4"></div>


                                                            <div class="form-group">


                                                               

                                                                <label class="col-sm-3 control-label no-padding-right" for="streeing_type">Streeing type</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="streeing_type" id="streeing_type" value="<?php
                                                                    if (!isset($_POST['streeing_type'])) {
                                                                        if ($results_up[1]['streeing_type'] != '') {
                                                                            echo $results_up[1]['streeing_type'];
                                                                        }
                                                                        else if ($results_up_copy[1]['streeing_type'] != '') {
                                                                            echo $results_up_copy[1]['streeing_type'];
                                                                        }
                                                                    } else if (isset($_POST['streeing_type'])) {
                                                                        echo $_POST['streeing_type'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('streeing_type'); ?></span>

                                                                </div>

                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">


                                                                <label class="col-sm-3 control-label no-padding-right" for="front_tyres">Front tyres</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="front_tyres" id="front_tyres" value="<?php
                                                                    if (!isset($_POST['front_tyres'])) {
                                                                        if ($results_up[1]['front_tyres'] != '') {
                                                                            echo $results_up[1]['front_tyres'];
                                                                        }
                                                                        else if ($results_up_copy[1]['front_tyres'] != '') {
                                                                            echo $results_up_copy[1]['front_tyres'];
                                                                        }
                                                                    } else if (isset($_POST['front_tyres'])) {
                                                                        echo $_POST['front_tyres'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('front_tyres'); ?></span>
                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="rear_tyre">Rear tyres</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="rear_tyre" id="streeing_type" value="<?php
                                                                    if (!isset($_POST['rear_tyre'])) {
                                                                        if ($results_up[1]['rear_tyre'] != '') {
                                                                            echo $results_up[1]['rear_tyre'];
                                                                        }
                                                                        else if ($results_up_copy[1]['rear_tyre'] != '') {
                                                                            echo $results_up_copy[1]['rear_tyre'];
                                                                        }
                                                                    } else if (isset($_POST['rear_tyre'])) {
                                                                        echo $_POST['rear_tyre'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('rear_tyre'); ?></span>

                                                                </div>

                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="tranmssion_first_gear_ratio">Transmission First Gear Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="tranmssion_first_gear_ratio" id="tranmssion_first_gear_ratio" value="<?php
                                                                    if (!isset($_POST['tranmssion_first_gear_ratio'])) {
                                                                        if ($results_up[1]['tranmssion_first_gear_ratio'] != '') {
                                                                            echo $results_up[1]['tranmssion_first_gear_ratio'];
                                                                        }
                                                                        if($results_up_copy[1]['tranmssion_first_gear_ratio'] != '') {
                                                                            echo $results_up_copy[1]['tranmssion_first_gear_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['tranmssion_first_gear_ratio'])) {
                                                                        echo $_POST['tranmssion_first_gear_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('tranmssion_first_gear_ratio'); ?></span>
                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_second_gear_ratio">Transmission Second Gear Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_second_gear_ratio" id="transmission_second_gear_ratio" value="<?php
                                                                    if (!isset($_POST['transmission_second_gear_ratio'])) {
                                                                        if ($results_up[1]['transmission_second_gear_ratio'] != '') {
                                                                            echo $results_up[1]['transmission_second_gear_ratio'];
                                                                        }
                                                                        else if ($results_up_copy[1]['transmission_second_gear_ratio'] != '') {
                                                                            echo $results_up_copy[1]['transmission_second_gear_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_second_gear_ratio'])) {
                                                                        echo $_POST['transmission_second_gear_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_second_gear_ratio'); ?></span>
                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>                                                            

                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_third_gear_ratio">Transmission Third Gear Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_third_gear_ratio" id="transmission_third_gear_ratio" value="<?php
                                                                    if (!isset($_POST['transmission_third_gear_ratio'])) {
                                                                        if ($results_up[1]['transmission_third_gear_ratio'] != '') {
                                                                            echo $results_up[1]['transmission_third_gear_ratio'];
                                                                        }
                                                                        else if ($results_up_copy[1]['transmission_third_gear_ratio'] != '') {
                                                                            echo $results_up_copy[1]['transmission_third_gear_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_third_gear_ratio'])) {
                                                                        echo $_POST['transmission_third_gear_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_third_gear_ratio'); ?></span>

                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_fourth_gear_ratio">Transmission Fourth Gear Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_fourth_gear_ratio" id="transmission_fourth_gear_ratio" value="<?php
                                                                    if (!isset($_POST['transmission_fourth_gear_ratio'])) {
                                                                        if ($results_up[1]['transmission_fourth_gear_ratio'] != '') {
                                                                            echo $results_up[1]['transmission_fourth_gear_ratio'];
                                                                        }
                                                                        else if ($results_up_copy[1]['transmission_fourth_gear_ratio'] != '') {
                                                                            echo $results_up_copy[1]['transmission_fourth_gear_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_fourth_gear_ratio'])) {
                                                                        echo $_POST['transmission_fourth_gear_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_fourth_gear_ratio'); ?></span>

                                                                </div>

                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_fifth_gear_ratio">Transmission Fifth Gear Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_fifth_gear_ratio" id="transmission_fifth_gear_ratio" value="<?php
                                                                    if (!isset($_POST['transmission_fifth_gear_ratio'])) {
                                                                        if ($results_up[1]['transmission_fifth_gear_ratio'] != '') {
                                                                            echo $results_up[1]['transmission_fifth_gear_ratio'];
                                                                        }
                                                                        else if ($results_up_copy[1]['transmission_fifth_gear_ratio'] != '') {
                                                                            echo $results_up_copy[1]['transmission_fifth_gear_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_fifth_gear_ratio'])) {
                                                                        echo $_POST['transmission_fifth_gear_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_fifth_gear_ratio'); ?></span>
                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_sixth_gear_ratio">Transmission Sixth Gear Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_sixth_gear_ratio" id="transmission_sixth_gear_ratio" value="<?php
                                                                    if (!isset($_POST['transmission_sixth_gear_ratio'])) {
                                                                        if ($results_up[1]['transmission_sixth_gear_ratio'] != '') {
                                                                            echo $results_up[1]['transmission_sixth_gear_ratio'];
                                                                        }
                                                                        if ($results_up_copy[1]['transmission_sixth_gear_ratio'] != '') {
                                                                            echo $results_up_copy[1]['transmission_sixth_gear_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_sixth_gear_ratio'])) {
                                                                        echo $_POST['transmission_sixth_gear_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_sixth_gear_ratio'); ?></span>

                                                                </div>
                                                            </div>

                                                            <div class="space-4"></div>


                                                            <div class="form-group">

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_reverse_gear_ratio">Transmission Reverse Gear Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_reverse_gear_ratio" id="transmission_reverse_gear_ratio" value="<?php
                                                                    if (!isset($_POST['transmission_reverse_gear_ratio'])) {
                                                                        if ($results_up[1]['transmission_reverse_gear_ratio'] != '') {
                                                                            echo $results_up[1]['transmission_reverse_gear_ratio'];
                                                                        }
                                                                        if ($results_up_copy[1]['transmission_reverse_gear_ratio'] != '') {
                                                                            echo $results_up_copy[1]['transmission_reverse_gear_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_reverse_gear_ratio'])) {
                                                                        echo $_POST['transmission_reverse_gear_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_reverse_gear_ratio'); ?></span>

                                                                </div>

                                                                <label class="col-sm-3 control-label no-padding-right" for="transmission_final_drive_ratio">Transmission Final Drive Ratio</label>
                                                                <div class="col-sm-3">
                                                                    <input type="text" name="transmission_final_drive_ratio" id="transmission_final_drive_ratio" value="<?php
                                                                    if (!isset($_POST['transmission_final_drive_ratio'])) {
                                                                        if ($results_up[1]['transmission_final_drive_ratio'] != '') {
                                                                            echo $results_up[1]['transmission_final_drive_ratio'];
                                                                        }
                                                                        if ($results_up_copy[1]['transmission_final_drive_ratio'] != '') {
                                                                            echo $results_up_copy[1]['transmission_final_drive_ratio'];
                                                                        }
                                                                    } else if (isset($_POST['transmission_final_drive_ratio'])) {
                                                                        echo $_POST['transmission_final_drive_ratio'];
                                                                    }
                                                                    ?>" >                                                                
                                                                     <span class="sample_data">(Sample Data)</span> <br> <span class="error"><?php echo form_error('transmission_final_drive_ratio'); ?></span>

                                                                </div>

                                                            </div>-->

                                                                                                                   




                                                            





                                                            






                                                        </div>                                                   



                                                    </div>
                                                </div>
                                            </div>

                                            <div class="space-4"></div>
                                            <?php
                                            if (empty($results_up)) {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="submit" class="btn btn-info">                                                
                                                            Submit
                                                        </button>

                                                        &nbsp; &nbsp; &nbsp;
                                                        <button class="btn" type="reset">
                                                            <i class="icon-undo bigger-110"></i>
                                                            Reset
                                                        </button>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="clearfix form-actions">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" name="update" class="btn btn-info">                                                
                                                            Update
                                                        </button>

                                                    </div>
                                                </div>


                                                <?php
                                            }
                                            ?>   


                                        </form>
                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                                    <th>#</th>
                                                    <th>Variant Name</th>
                                                    <th>Model Name</th>
                                                    <th>Brand Name</th>
                                                    <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                                
                                

<!--                                <div class="form-group">
                                    <?php
                                    if (isset($load_product)):
//                                        echo '<pre>';
//                                        print_r($load_product); 
//                                        echo '</pre>';
                                        ?>                                    
                                        <div class="table-header">
                                            Results for "Variant"
                                        </div>
                                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Variant Name</th>
                                                    <th>Model Name</th>
                                                    <th>Brand Name</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($load_product as $r) :
                                                    ?>
                                                    <tr>                                                
                                                        <td>
                                                            <?php echo $i; ?>                                                        
                                                        </td>
                                                        <td>
                                                            <?php echo $r['pro_name']; ?>
                                                        </td>

                                                        <td>
                                                            <?php echo $r['model_name']; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $r['brand_name']; ?>
                                                        </td>

                                                        <td>
                                                            <?php
                                                            $status = $r['status'];
                                                            if ($status == 1) {
                                                                echo 'Enable';
                                                            } else if ($status == 2) {
                                                                echo 'Disable';
                                                            }
                                                            ?>                                                            
                                                        </td>
                                                <form action="<?php echo 'product_upload'; ?>" method="POST">
                                                    <input type="hidden" name="variant_id"  value="<?php echo $r['variant_id']; ?>" ?>
                                                    <td class="align_tble">
                                                        <button type="submit" class="btn btn-xs btn-success" style="display:block;" name="get">
                                                            Edit
                                                        </button>                                                        
                                                    </td>
                                                </form>
                                                <form action="<?php echo 'product_upload'; ?>" method="POST" >
                                                    <input type="hidden" name="variant_id"  value="<?php echo $r['variant_id']; ?>" ?>
                                                    <td>
                                                        <button class="btn btn-xs btn-danger" type="submit" name="delete">
                                                            delete
                                                        </button>

                                                    </td>                                                    
                                                </form>
                                                <form action="<?php echo 'product_upload'; ?>" method="POST" >
                                                    <input type="hidden" name="variant_id"  value="<?php echo $r['variant_id']; ?>" ?>
                                                    <td>
                                                        <button class="btn btn-xs btn-success" type="submit" name="copy">
                                                            Copy
                                                        </button>
                                                    </td>                                                    
                                                </form>
                                                </tr>
                                                <?php $i++; ?>    
                                            <?php endforeach; ?>


                                            </tbody>
                                        </table>
                                        <?php
                                    else:
//echo '<p>No Task Exist</p>';
                                    endif;
                                    ?>
                                </div>-->
                            </div>     

                        </div>




                        <!-- main content data end here -->
                    </div><!-- /.main-content -->                

                </div><!-- /.main-container-inner -->

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- footer js file links start here -->
            <?php $this->load->view('templates/js') ?>    
            <script src="<?php echo base_url().'assets/js/modules/validate_product.js' ?>" ></script>
            <?php $this->load->view('templates/footer') ?>    
            
    </body>
</html>







