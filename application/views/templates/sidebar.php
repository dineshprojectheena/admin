<div class="sidebar sidebar" id="sidebar">
    <ul class="nav nav-list">
        <li class="active">
            <a href="">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>
        
        
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Master </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'brand' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Brand
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'model' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Model
                        </span>
                    </a>
                </li>
<!--                <li>
                    <a href="<?php echo base_url() . 'dealer' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Dealer
                        </span>
                    </a>
                </li>-->


<!--                <li>
                    <a href="<?php echo base_url() . 'accessory_seller' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Accessory Seller
                        </span>
                    </a>
                </li>-->


<!--                <li>
                    <a href="<?php echo base_url() . 'service_seller' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Service Seller
                        </span>
                    </a>
                </li>-->
                
                <li>
                    <a href="<?php echo base_url() . 'product' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Variant
                        </span>
                    </a>
                </li>
<!--                <li>
                    <a href="<?php echo base_url() . 'gallery' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Variant Gallery
                        </span>
                    </a>
                </li>-->
<!--                <li>
                    <a href="<?php echo base_url() . 'exShowroom' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Ex-Showroom Price
                        </span>
                    </a>
                </li>-->
                <li>
                    <a href="<?php echo base_url() . 'mnc' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Variant Price
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'product_video' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Variant Video
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'onRoad' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            On Road Price
                        </span>
                    </a>
                </li>
                
<!--                <li>
                    <a href="#" class="dropdown-toggle">
                        <i class="icon-list-alt"></i>
                        <span class="menu-text"> Feature </span>
                        <b class="arrow icon-angle-down"></b>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="<?php echo base_url() . 'feature' ?>">
                                <i class="icon-calendar"></i>
                                <span class="menu-text">
                                    Features
                                </span>
                            </a>

                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'feature/map_feature' ?>">
                                <i class="icon-calendar"></i>
                                <span class="menu-text">
                                    Map Features
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>-->

<!--                <li>
                    <a href="#" class="dropdown-toggle">
                        <i class="icon-list-alt"></i>
                        <span class="menu-text"> Accessories </span>
                        <b class="arrow icon-angle-down"></b>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="<?php echo base_url() . 'accessory' ?>">
                                <i class="icon-calendar"></i>
                                <span class="menu-text">
                                    Accessories
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'accessory/map_accessory'; ?>">
                                <i class="icon-calendar"></i>
                                <span class="menu-text">
                                    Map Accessories
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'accessory/accessory_depencies'; ?>">
                                <i class="icon-calendar"></i>
                                <span class="menu-text">
                                    Accessory Dependencies
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'accessory/accessory_group'; ?>">
                                <i class="icon-calendar"></i>
                                <span class="menu-text">
                                    Accessory Group
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>-->
        </ul>
        </li>
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> System </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'slider_data' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Slider
                        </span>
                    </a>
                </li>
<!--                <li>
                    <a href="<?php echo base_url() . 'faq' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Faq
                        </span>
                    </a>
                </li>-->
<!--                <li>
                    <a href="<?php echo base_url() . 'news' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            News
                        </span>
                    </a>
                </li>-->
<!--                <li>
                    <a href="<?php echo base_url() . 'information' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Information
                        </span>
                    </a>
                </li>-->

                <li>
                    <a href="<?php echo base_url() . 'rating' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Expert Rating
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle">
                        <i class="icon-list-alt"></i>
                        <span class="menu-text"> Home Structure </span>

                        <b class="arrow icon-angle-down"></b>
                    </a>

                    <ul class="submenu">
                        <li>
                            <a href="<?php echo base_url() . 'home_content' ?>">
                                <i class="icon-calendar"></i>

                                <span class="menu-text">
                                    Car Comparsion
                                </span>
                            </a>
                        </li>
<!--                        <li>
                            <a href="<?php echo base_url() . 'home_expert_reviews' ?>">
                                <i class="icon-calendar"></i>

                                <span class="menu-text">
                                    Expert Review
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'latest_arrivals' ?>">
                                <i class="icon-calendar"></i>

                                <span class="menu-text">
                                    Latest Arrival
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'top_selling_cars' ?>">
                                <i class="icon-calendar"></i>
                                <span class="menu-text">
                                    Group Deals
                                </span>
                            </a>
                        </li>-->
                    </ul>
                </li>

            </ul>
        </li>            
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Extra </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
<!--                <li>
                    <a href="<?php echo base_url() . 'lastestdeal' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Latest Deal
                        </span>
                    </a>
                </li>-->
<!--                <li>
                    <a href="<?php echo base_url() . 'groupdeal' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Group Deal
                        </span>
                    </a>
                </li>-->
                <li>
                    <a href="<?php echo base_url() . 'landing_page' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Landing Page
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'coupon' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Coupon
                        </span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Order </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'lastestorder' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Latest Orders
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'order/orderdownload' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Download Orders
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'order-to-dealer' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Order to dealer
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'top-affiliate' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Top Affiliate
                        </span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Accessory</span>

                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'accessory_master' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Accessory Master
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'bulkaccessory' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk Accessory
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'bulkaccessory_hotdeal' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk Accessory Hot Deal 
                        </span>
                    </a>
                </li>
            </ul>    
        </li>    
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Bulk Upload</span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'bulkproduct' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk Product
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() . 'bulkrating' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk Rating
                        </span>
                    </a>
                </li>                
                <li>
                    <a href="<?php echo base_url() . 'bulkprice' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk Price
                        </span>
                    </a>
                </li>                
                <li>
                    <a href="<?php echo base_url() . 'bulkhotdeal' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk Hot Deals
                        </span>
                    </a>
                </li>                
                <li >
                    <a href="<?php echo base_url() . 'bulkcoupon'?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk Coupon Offer
                        </span>
                    </a>
                </li>                
                <li >
                    <a href="<?php echo base_url() . 'bulkseo'?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk SEO
                        </span>
                    </a>
                </li>                
                <li >
                    <a href="<?php echo base_url() . 'mosf'?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk MOSF
                        </span>
                    </a>
                </li>                
<!--                <li>
                    <a href="<?php echo base_url() . 'bulkgroupdeal' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Bulk Group Deals
                        </span>
                    </a>
                </li>                -->
            </ul>            
        </li> 
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Affiliate</span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'affiliate' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Affiliate User/Code
                        </span>
                    </a>
                </li>
                
            </ul>            
        </li>   
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Dealer </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'dealer-join-us' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Join Us Enquiry
                        </span>
                    </a>
                </li>
<!--                <li>
                    <a href="<?php echo base_url() . 'dealer' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Create Dealer
                        </span>
                    </a>
                </li>-->
                <li>
                    <a href="<?php echo base_url() . 'dealer_list_prices' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Dealer Price
                        </span>
                    </a>
                </li>
<!--                <li>
                    <a href="<?php echo base_url() . 'dealer_hot_deals' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Dealer Hot Deal
                        </span>
                    </a>
                </li>-->
            </ul>    
        </li>
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Upcoming Cars</span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'upcoming_variant' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Models
                        </span>
                    </a>
                </li>
                
                <li>
                    <a href="<?php echo base_url() . 'upcoming_variant_order' ?>">
                        <i class="icon-calendar"></i>
                        <span class="menu-text">
                            Orders
                        </span>
                    </a>
                </li>
                
            </ul>    
        </li>
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list-alt"></i>
                <span class="menu-text"> Report </span>

                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url() . 'brand' ?>">
                        <i class="icon-calendar"></i>

                        <span class="menu-text">
                            Car Booking 
                        </span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>            

</ul>

<div class="sidebar-collapse" id="sidebar-collapse">
    <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
</div>
</div>
