        <meta charset="utf-8" />
        <title>MNC Admin Panel</title>
        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- basic styles -->

        <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/select2.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/chosen.css">

        <!--[if IE 7]>
          <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->

        <!-- page specific plugin styles -->

        <!-- fonts -->

        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ace-fonts.css" />

        <!-- ace styles -->
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ace.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ace-skins.min.css" />
        
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorpicker.css" />
        
        
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/app.css" />

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/ace-extra.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/jquery.validate.min.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>/assets/js/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/respond.min.js"></script>
        <![endif]-->
        <style>
        .align_tble
        {
        text-align:center;
        padding:5px;            
        }
         .align_tble_hd
        {
        text-align:center;
        padding:10px;            
        }   
        .removeborder
	{
       background: transparent !important;border: 0px solid !important;        
	}
        .modal-lg{
            width: 800px !important;
         }
         .help-inline
         {
          color: #B94A48;    
         }
        </style>
        <script>
            var prePopulate_data;
            var prePopulate_data2;
        </script>
        <meta name="description" content="Online daily deals shopping website. Finding online best deal of the day is not tough task now. If you are looking for daily deal online shopping website then you can stop here as dealtz.com is popular for daily deals online ecommerce store." />
        <meta name="keywords" content="Online Shopping, India, Books, Clothing, Electronics, Store, Dealtz" />
        