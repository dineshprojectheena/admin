<!--<![endif]-->		
<link rel="stylesheet" href="<?= base_url(); ?>assets/thirdparty/datatable/DT_bootstrap.min.css"/>
<script src="<?= base_url(); ?>assets/thirdparty/datatable/datatable.min.js"></script>
<script src="<?= base_url(); ?>assets/thirdparty/datatable/DT_bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.hotkeys.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.min.js"></script>
<script>
    function getmodel()
    {

        var brand_id = $('#brand_id').val();
//                    alert(brand_id);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/get_brand_wise_model') ?>",
            data: {brand_id: brand_id}
        }).done(function (html) {
//                        alert(html);
            $("#model_res").html(html);
        });

    }
    function get_variant()
    {
        var brand_id = $('#brand_id').val();
        var model_id = $('#model_id').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/get_model_wise_variant') ?>",
            data: {model_id: model_id, brand_id: brand_id}
        }).done(function (html) {
//                        alert(html);
            $("#variant_results").html(html);
        });

    }

</script>

<script type="text/javascript">
    function pass_desc_txt()
    {
        var editor1 = $('#editor1').html();
        $('#description').val(editor1);
    }
    function pass_desc_txt2()
    {
        var editor3 = $('#editor3').html();
        $('#details').val(editor3);
    }
    function pass_key_txt()
    {
        var editor2 = $('#editor2').html();
        $('#keyword').val(editor2);
    }
    $(function () {

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        //but we want to change a few buttons colors for the third style
        $('#editor1').ace_wysiwyg({
            toolbar:
                    [
                        'font',
                        null,
                        'fontSize',
                        null,
                        {name: 'bold', className: 'btn-info'},
                        {name: 'italic', className: 'btn-info'},
                        {name: 'strikethrough', className: 'btn-info'},
                        {name: 'underline', className: 'btn-info'},
                        null,
                        {name: 'insertunorderedlist', className: 'btn-success'},
                        {name: 'insertorderedlist', className: 'btn-success'},
                        {name: 'outdent', className: 'btn-purple'},
                        {name: 'indent', className: 'btn-purple'},
                        null,
                        {name: 'justifyleft', className: 'btn-primary'},
                        {name: 'justifycenter', className: 'btn-primary'},
                        {name: 'justifyright', className: 'btn-primary'},
                        {name: 'justifyfull', className: 'btn-inverse'},
                        null,
                        {name: 'createLink', className: 'btn-pink'},
                        {name: 'unlink', className: 'btn-pink'},
                        null,
                        {name: 'insertImage', className: 'btn-success'},
                        null,
                        'foreColor',
                        null,
                        {name: 'undo', className: 'btn-grey'},
                        {name: 'redo', className: 'btn-grey'}
                    ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');






        $('[data-toggle="buttons-radio"]').on('click', function (e) {
            var target = $(e.target);
            var which = parseInt($.trim(target.text()));
            var toolbar = $('#editor1').prev().get(0);
            if (which == 1 || which == 2 || which == 3) {
                toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
                if (which == 1)
                    $(toolbar).addClass('wysiwyg-style1');
                else if (which == 2)
                    $(toolbar).addClass('wysiwyg-style2');
            }
        });

        $('#editor2').ace_wysiwyg({
            toolbar:
                    [
                        'font',
                        null,
                        'fontSize',
                        null,
                        {name: 'bold', className: 'btn-info'},
                        {name: 'italic', className: 'btn-info'},
                        {name: 'strikethrough', className: 'btn-info'},
                        {name: 'underline', className: 'btn-info'},
                        null,
                        {name: 'insertunorderedlist', className: 'btn-success'},
                        {name: 'insertorderedlist', className: 'btn-success'},
                        {name: 'outdent', className: 'btn-purple'},
                        {name: 'indent', className: 'btn-purple'},
                        null,
                        {name: 'justifyleft', className: 'btn-primary'},
                        {name: 'justifycenter', className: 'btn-primary'},
                        {name: 'justifyright', className: 'btn-primary'},
                        {name: 'justifyfull', className: 'btn-inverse'},
                        null,
                        {name: 'createLink', className: 'btn-pink'},
                        {name: 'unlink', className: 'btn-pink'},
                        null,
                        {name: 'insertImage', className: 'btn-success'},
                        null,
                        'foreColor',
                        null,
                        {name: 'undo', className: 'btn-grey'},
                        {name: 'redo', className: 'btn-grey'}
                    ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');






        $('[data-toggle="buttons-radio"]').on('click', function (e) {
            var target = $(e.target);
            var which = parseInt($.trim(target.text()));
            var toolbar = $('#editor2').prev().get(0);
            if (which == 1 || which == 2 || which == 3) {
                toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
                if (which == 1)
                    $(toolbar).addClass('wysiwyg-style1');
                else if (which == 2)
                    $(toolbar).addClass('wysiwyg-style2');
            }
        });


        $('#editor3').ace_wysiwyg({
            toolbar:
                    [
                        'font',
                        null,
                        'fontSize',
                        null,
                        {name: 'bold', className: 'btn-info'},
                        {name: 'italic', className: 'btn-info'},
                        {name: 'strikethrough', className: 'btn-info'},
                        {name: 'underline', className: 'btn-info'},
                        null,
                        {name: 'insertunorderedlist', className: 'btn-success'},
                        {name: 'insertorderedlist', className: 'btn-success'},
                        {name: 'outdent', className: 'btn-purple'},
                        {name: 'indent', className: 'btn-purple'},
                        null,
                        {name: 'justifyleft', className: 'btn-primary'},
                        {name: 'justifycenter', className: 'btn-primary'},
                        {name: 'justifyright', className: 'btn-primary'},
                        {name: 'justifyfull', className: 'btn-inverse'},
                        null,
                        {name: 'createLink', className: 'btn-pink'},
                        {name: 'unlink', className: 'btn-pink'},
                        null,
                        {name: 'insertImage', className: 'btn-success'},
                        null,
                        'foreColor',
                        null,
                        {name: 'undo', className: 'btn-grey'},
                        {name: 'redo', className: 'btn-grey'}
                    ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');






        $('[data-toggle="buttons-radio"]').on('click', function (e) {
            var target = $(e.target);
            var which = parseInt($.trim(target.text()));
            var toolbar = $('#editor3').prev().get(0);
            if (which == 1 || which == 2 || which == 3) {
                toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
                if (which == 1)
                    $(toolbar).addClass('wysiwyg-style1');
                else if (which == 2)
                    $(toolbar).addClass('wysiwyg-style2');
            }
        });

        if (typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase())) {

            var lastResizableImg = null;
            function destroyResizable() {
                if (lastResizableImg == null)
                    return;
                lastResizableImg.resizable("destroy");
                lastResizableImg.removeData('resizable');
                lastResizableImg = null;
            }

            var enableImageResize = function () {
                $('.wysiwyg-editor')
                        .on('mousedown', function (e) {
                            var target = $(e.target);
                            if (e.target instanceof HTMLImageElement) {
                                if (!target.data('resizable')) {
                                    target.resizable({
                                        aspectRatio: e.target.width / e.target.height,
                                    });
                                    target.data('resizable', true);

                                    if (lastResizableImg != null) {//disable previous resizable image
                                        lastResizableImg.resizable("destroy");
                                        lastResizableImg.removeData('resizable');
                                    }
                                    lastResizableImg = target;
                                }
                            }
                        })
                        .on('click', function (e) {
                            if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                                destroyResizable();
                            }
                        })
                        .on('keydown', function () {
                            destroyResizable();
                        });
            }

            enableImageResize();


        }


    });

    function getcity()
    {
        var state_data = $(".state_data").val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/getcity') ?>",
            data: {state_data: state_data}
        }).done(function (html) {
            //alert(html);	
            $("#city").html(html);
        });
    }


</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/facebook_auto_suggest_src/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/facebook_auto_suggest_src/token-input-facebook.css" type="text/css" />
<script type="text/javascript">
    window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
</script>
<script type="text/javascript">
    if ("ontouchend" in document)
        document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/typeahead-bs2.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easy-pie-chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.category.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fuelux/fuelux.spinner.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/date-time/daterangepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/date-time/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.knob.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-tag.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.js"></script>

<script src="<?php echo base_url(); ?>/assets/js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.bootstrap.js"></script>

<!--ace scripts--> 

<script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.js"></script>
<script src="<?php echo base_url(); ?>assets/js/page-js/forms.js"></script>
<script>
//            var brand_id_change="<?php echo $single_res_detail[0]['brand_id']; ?>";    
//            if(brand_id_change!='')
//            {    
//            $('#brand_id').val(brand_id_change);
//            getmodel();
//            }
//            var model_id_change="<?php echo $single_res_detail[0]['model_id']; ?>";    
//            alert(model_id_change);
//            exit;


//            function sleep(milliseconds) {
//            var start = new Date().getTime();
//            for (var i = 0; i < 1e7; i++) {
//              if ((new Date().getTime() - start) > milliseconds){
//                break;
//              }
//            }

//          }
</script>

 <!--<script src="http://www.flotcharts.org/flot/jquery.flot.categories.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/js/page-js/dashboard.js"></script>-->
