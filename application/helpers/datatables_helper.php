<?php

function check_status($status = '') {
    return ($status == 1) ? 'Enabled' : 'Disabled';
}

function check_upcoming_status($status = '',$upcoming) {
//    return $upcoming;
    return ($status == 3) ? 'Enabled' : 'Disabled';
}

function check_activation($status = '') {
    return (strtolower($status) == 'y') ? 'Active' : 'Deactive';
}

function action($status, $cond, $join_id) {
    if ($cond == 1) {
        if ($status == 1) {
            $status = 2;
            $btn = 'Disapprove <br> Dealer';
            $btn_color = 'danger';
            $page = 'dis_approve';
        } else {
            $status = 1;
            $btn = 'Approve <br> Dealer';
            $btn_color = 'success';
            $page = 'approve';
        }
        $html_data = <<<END
    <a href="dealer_prices/$page?join_us=$join_id&status=$status" name="update" class="btn btn-minier btn-$btn_color">$btn</a>
END;
    }
    return $html_data;
}

function dealer_document_verification($dealer_join_us_id, $cond) {
    if ($cond == 1) {
        $status = 1;
        $btn = 'Verify<br>Document';
        $btn_color = 'info';
        $page = 'dealer_verify_documents';
        $html_data = <<<END
    <a href="dealer_prices/$page?join_us=$dealer_join_us_id" name="verify" class="btn btn-minier btn-$btn_color">$btn</a>
END;
    }
    return $html_data;
}
function dealer_credential($dealer_join_us_id, $cond) {
    if ($cond == 1) {
        $status = 1;
        $btn = 'Send<br>Credientials';
        $btn_color = 'info';
        $page = 'dealer_send_credientials';
        $html_data = <<<END
    <a href="dealer_prices/$page?join_us=$dealer_join_us_id" name="verify" class="btn btn-minier btn-$btn_color">$btn</a>
END;
    }
    return $html_data;
}
