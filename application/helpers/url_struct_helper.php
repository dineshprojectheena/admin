<?php
function url_struct($data){    
    return base_url().preg_replace('/[^A-Za-z0-9z \/\\-]/', '', $data);   
}
function img_url($data){    
    return 'http://images.mynewcar.in/'.$data;   
}

function dealer_url() {
    return 'http://mncbeta.com/mynewcardealer/';
}

function sendMailViaSparkPost($postFields){
//    print_r($postFields);
//    die();
//    echo SPARKPOST_KEY;
    $flag = FALSE;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, SPARKPOST_URL);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: " . SPARKPOST_KEY,'Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    $data = curl_exec($ch);
    curl_close($ch);
    $dataArray = json_decode($data);
//    echo '<pre>'; print_r($dataArray); echo '</pre>';
//    exit;
    if (isset($dataArray->results)) {
        if ($dataArray->results->total_rejected_recipients > 0) {
            log_message('Error', 'Total rejected mail count - ' . $dataArray->results->total_rejected_recipients . ' and total accepted mail count - ' . $dataArray->results->total_accepted_recipients);
        }
        $flag = TRUE;
    }
    return $flag;
}

