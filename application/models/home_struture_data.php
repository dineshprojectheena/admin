<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_struture_data extends CI_Model {
    ### product search section start here

    function search_product($search_product, $search_product2) {
        $this->db->select('variant_id,pro_name_comp');
        if ($search_product != '') {
            $this->db->where('status', '1');
            $this->db->like('pro_name_comp', $search_product);
            $get_data = $this->db->get('variant');
        }
        if ($search_product2 != '') {
            $this->db->where('status', '1');
            $this->db->like('pro_name_comp', $search_product2);
            $get_data = $this->db->get('variant');
        }
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    ### product search section end here
    ### car comparison section start here

    function upload_car_comparison($data) {
        $update=$this->db->query("update featured set status='2' where featured_type='car_comparison'");
        $insert=$this->db->insert('featured', $data);
        return $insert;
    }

    function load_featured($id, $type) {
        if($id!=''){
            $this->db->where('featured_id', $id);
        }
        $this->db->where('featured_type', $type);
        $this->db->select('prd_id_1,prd_id_2,featured_id,status');
        
        return $this->db->query("select *,(select pro_name_comp from variant where variant_id=featured.prd_id_1) as pro_name_comp,(select pro_name_comp from variant where variant_id=featured.prd_id_2) as pro_name_comp2 from featured where featured_type='$type'")->result_array();
        
//        return $res = $this->db->get('featured')->result_array();
    }

    function load_featured_pro($type,$featured_id) {
        if($type != '') {
            return $query = $this->db->query("SELECT f.`featured_id`,f.`status`, f.`prd_id_1`, f.`status`,v.`pro_name` FROM `featured` f,`variant` v WHERE f.`prd_id_1`=v.`variant_id` and f.featured_type='$type'")->result_array();
        }        
        else if($featured_id!= '') {
            return $query = $this->db->query("SELECT f.`featured_id`,f.`status`, f.`prd_id_1`, f.`status`,v.`pro_name` FROM `featured` f,`variant` v WHERE f.`prd_id_1`=v.`variant_id` and f.featured_id='$featured_id'")->result_array();
        }
        
    }

    function del_car_comparison($id) {
        $this->db->where('featured_id', $id);
        return $res = $this->db->delete('featured');
    }

    function get_product_detail($pro1) {
        $this->db->where('variant_id', $pro1);
        $this->db->select('pro_name,variant_id');
        $this->db->limit(1);
        return $res = $this->db->get('variant')->result_array();
    }

    function update_car_comparison($data, $featured_id) {
        $update=$this->db->query("update featured set status='2' where featured_type='car_comparison'");
        $this->db->where('featured_id', $featured_id);
        $get_data = $this->db->update('featured', $data);
    }

    ### car comparison section end here
    ### Home page expert review prduct section start here

    function upload_expert_review($data,$featured_type) {        
        $insert=$this->db->insert('featured', $data);
        $last_id=$this->db->insert_id();
        if($last_id!='')
        {
        $get_data=$this->db->query("UPDATE `featured` SET `status` = '2' WHERE `featured_id`!='$last_id' and `featured_type`='$featured_type'");
        }
        return;
    }

    function del_expert_reviews($id) {
        $this->db->where('featured_id', $id);
        return $res = $this->db->delete('featured');
    }

    function update_expert_reviews($data, $featured_id,$featured_type,$status){
        $this->db->where('featured_id',$featured_id);
        $get_data=$this->db->update('featured', $data);
        if($status==1)
        {
        $get_data=$this->db->query("UPDATE `featured` SET `status` = '2' WHERE `featured_id`!='$featured_id' and `featured_type`='$featured_type'");    
        }          
    }

    ### Home page expert review prduct section end here
    ### Home page latest arrivals prduct section start here

    function upload_latest_arrivals($data,$featured_type) {
        $insert=$this->db->insert('featured', $data);
        $last_id=$this->db->insert_id();
        if($last_id!='')
        {
        $get_data=$this->db->query("UPDATE `featured` SET `status` = '2' WHERE `featured_id`!='$last_id' and `featured_type`='$featured_type'");
        }   
        
    }

    function del_latest_arrivals($id) {
        $this->db->where('featured_id', $id);
        return $res = $this->db->delete('featured');
    }

    function update_latest_arrivals($data,$featured_id,$type,$status) {        
        $prd_id_1=$data['prd_id_1'];
        $this->db->where('featured_id', $featured_id);
        $get_data = $this->db->update('featured', $data);                    
        if($status==1)
        {        
        $get_data=$this->db->query("UPDATE `featured` SET `status` = '2' WHERE `prd_id_1` != '$prd_id_1' AND `featured_type`='$type'");    
        }                 
    }

    ### Home page page latest arrivals section end here
    ### Home page my new car works prduct section start here

    function upload_top_selling_cars($data) {
//        $update=$this->db->query("update featured set status='2' where featured_type='top_selling_cars'");
        $insert = $this->db->insert('featured', $data);
        return $insert;
    }

    function del_top_selling_cars($id) {
        
        $this->db->where('featured_id', $id);
        return $res = $this->db->delete('featured');
    }

    function update_top_selling_cars($data, $featured_id) {
//        $update=$this->db->query("update featured set status='2' where featured_type='top_selling_cars'");
        $this->db->where('featured_id', $featured_id);
        $get_data = $this->db->update('featured', $data);
    }

    ### Home page my new car works product section end here
}
