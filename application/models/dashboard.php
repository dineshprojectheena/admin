<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Model {
    #### Header city section get here #### 

    function dashboard_test_drive($id) {
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        if ($id == 1) {
            return $count = $this->db->query("select count(*) as count from test_drive_enquiry where added_date > DATE_SUB(NOW(), INTERVAL $days DAY)")->row_array();
        }
        if ($id == 2) {
            return $count = $this->db->query("select count(*) as success_test from test_drive_enquiry where status!='0' and added_date > DATE_SUB(NOW(), INTERVAL $days DAY)")->row_array();
        }
        if ($id == 3) {
            return $count = $this->db->query("select count(*) as pending_test from test_drive_enquiry where status='0' and added_date > DATE_SUB(NOW(), INTERVAL $days DAY)")->row_array();
        }
    }

    function new_test_drive_enquiry() {
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        return $count = $this->db->query("SELECT count(*) as new_testdrive FROM `test_drive_enquiry` WHERE added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
    }
    function new_orders() {
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        return $count = $this->db->query("SELECT count(*) as total FROM `order` WHERE added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    
    function total_orders() {
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        return $count = $this->db->query("SELECT count(*) as total_order FROM `order`")->row_array();
//        print_r($count);
    }
    
    function new_users(){
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        return $count = $this->db->query("SELECT count(*) as new_user FROM `user` WHERE date_added > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    
    function new_accessory(){
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        
        return $count = $this->db->query("SELECT count(*) as new_accessory FROM `accessory_configuration` WHERE added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    
    function car_confiured(){
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        
        return $count = $this->db->query("SELECT count(*) as car_confiure FROM `car_configuration` WHERE `status`!='2' and added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    
    function car_exchange(){
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        return $count = $this->db->query("SELECT count(*) as car_exchange FROM `exchange_a_car` WHERE added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    
    function car_lease(){
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        return $count = $this->db->query("SELECT count(*) as car_lease FROM `leasing` WHERE added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    
    function car_upcoming_car(){
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        return $count = $this->db->query("SELECT count(*) as car_upcoming_enquiry FROM `upcoming_enquiry` WHERE added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    
    function contact_us(){
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }    
        return $count = $this->db->query("SELECT count(*) as contact_us FROM `contact` WHERE added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    
    function car_upcoming_sale(){
        if(!empty($_GET['id']))
        {
        $days=$_GET['id'];    
        }    
        else
        {
        $days=1;    
        }
        return $count = $this->db->query("SELECT count(*) as car_upcoming_sale FROM `upcoming_configuration` WHERE status='2' and added_date > DATE_SUB(NOW(), INTERVAL $days DAY);")->row_array();
//        print_r($count);
    }
    

}
