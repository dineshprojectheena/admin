<?php

/**
 * @author Nikhil Kataria <nikhil@projectheena.com>
 * @desc contains all feature related queries
 * 
 */
class Feature_model extends CI_Model {

    function get_all_features() {
        $this->db->select('*');
        return $this->db->get('features')->result_array();
    } 
            
            
    
            
    function create_feature($data) {
        $feature_type=$data['feature_type'];
        $feature_name=$data['feature_name'];
        $feature_desc=$data['feature_desc'];
        $update=$this->db->query("SELECT * FROM `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and feature_desc='$feature_desc'")->num_rows();                      
        if($update==0)
        {    
        $this->db->insert('features', $data);
        return $this->db->insert_id();
        }
    }

    

    function delete_feature($feature_id) {
        return $this->db->delete('features', array('feature_id' => $feature_id));
    }

    function delete_feature_mapping($feature_id) {
        return $this->db->delete('feature_variant_mapping', array('feature_id' => $feature_id));
    }

    function delete_preffered_feature_mapping($feature_id, $variant_id) {
        return $this->db->delete('feature_variant_mapping', array('feature_id' => $feature_id, 'variant_id' => $variant_id));
    }

    function get_feature_details($feature_id) {
        $this->db->select('*');
        $this->db->where('feature_id', $feature_id);
        return $this->db->get('features')->row_array();
    }

    function get_feature_variant($variant_id) {
        $this->db->select('variant_id as id, pro_name as name');
        $this->db->where('variant_id', $variant_id);
        return $this->db->get('variant')->row_array();
    }

    function update_feature($data, $feature_id) {
        return $this->db->update('features', $data, "feature_id = $feature_id");
    }
    
    function get_all_variants(){
        $this->db->select('*');
        return $this->db->get('variant')->result_array();
    }
    
    function delete_previous_variant_feature_mapping($variant_id, $data) {
        return $this->db->update_batch('feature_variant_mapping', $data, 'variant_id');
    }
    
    function create_feature_variant_mapping($data) {
        $variant_id=$data[0]['variant_id'];        
        $feature_id=$data[0]['feature_id'];        
        $status=$data[0]['status'];
        $price=$data[0]['price'];
        $update=$this->db->query("SELECT * FROM `feature_variant_mapping` where `feature_id`='$feature_id' and `variant_id`='$variant_id'")->num_rows();                      
        if($update==0)
        {
        return $this->db->insert_batch('feature_variant_mapping', $data);
        }
        else 
        {
        $update=$this->db->query("update `feature_variant_mapping` set `feature_id`='$feature_id',`variant_id`='$variant_id',status='$status',price='$price' where `feature_id`='$feature_id' and `variant_id`='$variant_id'");                          
        }    
    }
    
    function get_variant_features($variant_id){
        $this->db->select('feature_id');
        $this->db->where('variant_id', $variant_id);
        $this->db->where('status', 1);
        return $this->db->get('feature_variant_mapping')->result_array();
    }
    
    function get_all_features_sub_type()
    {
    return $this->db->get('feature_types')->result_array();    
    }
    
    function get_all_map_features()
    {
    return $sel=$this->db->query("select vm.*,v.pro_name,feature_name,feature_desc from feature_variant_mapping vm,variant v,features f where v.variant_id=vm.variant_id and f.feature_id=vm.feature_id order by vm.id DESC")->result_array();                          
    }
    
    function del_all_map_features($feature_id)
    {
    return $sel=$this->db->query("delete from feature_variant_mapping where id='$feature_id'")->result_array();                          
    }
    

}