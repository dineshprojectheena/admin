<?php

/**
 * @author Nikhil Kataria <nikhil@projectheena.com>
 * @desc contains all feature related queries
 * 
 */
class Accessory_model extends CI_Model {

    function get_all_accessory() {
        $this->db->select('*');
        return $this->db->get('accessory')->result_array();
    }

    function create_accessory($data){
        $accessory_name=$data['accessory_name'];
        $accessory_cat=$data['accessory_cat'];
        $accessory_sub_cat=$data['accessory_sub_cat'];        
        $res=$this->db->query("select * from accessory where accessory_name='$accessory_name'"
                ."and accessory_cat='$accessory_cat' and accessory_sub_cat='$accessory_sub_cat'")->num_rows();
//        exit;
        if($res==0)
        {    
        $this->db->insert('accessory', $data);
        return $this->db->insert_id();
        }
    }

    function delete_accessory($accessory_id) {
        return $this->db->delete('accessory', array('accessory_id' => $accessory_id));
    }

    function delete_accessory_dependencies($accessory_id) {
        return $this->db->delete('accessory_dependencies_mapping', array('accessory_id' => $accessory_id));
    }

    function get_accessory_details($accessory_id) {
        $this->db->select('*');
        $this->db->where('accessory_id', $accessory_id);
        return $this->db->get('accessory')->row_array();
    }

    function update_accessory($data, $accessory_id) {
        return $this->db->update('accessory', $data, "accessory_id = $accessory_id");
    }

    function get_accessory($query) {
        $this->db->select('accessory_name as name,accessory_id as id');
        $this->db->like('accessory_name', $query, 'both');
        $this->db->limit(5);
        $query = $this->db->get('accessory');
        return $query->result_array();
    }

    function insert_accessory_dependency($data) {
        return $this->db->insert_batch('accessory_dependencies_mapping', $data);
    }

    function get_accessory_dependencies($accessory_id, $dependency_type) {
        $this->db->select('accessory.accessory_name as name,accessory_dependencies_mapping.dependent_accessory_id as id');
        $this->db->where('accessory_dependencies_mapping.accessory_id ', $accessory_id);
        $this->db->where('dependency_type', $dependency_type);
        $this->db->join('accessory', 'accessory.accessory_id = accessory_dependencies_mapping.dependent_accessory_id');
        return $this->db->get('accessory_dependencies_mapping')->result_array();
    }

    function delete_previous_accessory_dependency($accessory_id) {
        $this->db->where('accessory_id', $accessory_id);
        $this->db->delete('accessory_dependencies_mapping');
    }
    
    function get_all_accessory_features($id)
    {    
    return $this->db->query("select * from accessory where accessory_cat='$id' and status='1'")->result_array();    
    }
    
    function create_accessory_variant_mapping($insert_array)
    { 
    print_r($insert_array);
//    exit;
        
     $accessory_id=$insert_array[0]['accessory_id'];
     $variant_id=$insert_array[0]['variant_id'];
//        echo "select * from accessory_variant_mapping where accessory_id='$accessory_id' and variant_id='$variant_id'";
        $res=$this->db->query("select * from accessory_variant_mapping where accessory_id='$accessory_id'"
                ."and variant_id='$variant_id'")->num_rows();    
//        exit;
        if($res==0)
        {    
//        exit;
        return $this->db->insert_batch('accessory_variant_mapping',$insert_array);    
//    return $this->db->query("select * from accessory where accessory_cat='$id' and status='1'")->result_array();    
        }  
    }
    
    function get_pos_neg_res($select_acc,$i)
    {
    if($i==0)
    {    
    return $this->db->query("select dependent_accessory_id,dependent_accessory_name from accessory_dependencies_mapping where dependent_accessory_id IN ($select_acc) and dependency_type='$i'")->result_array(); 
    }
    else if($i==1)
    {    
    return $this->db->query("select dependent_accessory_id,dependent_accessory_name from accessory_dependencies_mapping where dependent_accessory_id IN ($select_acc) and dependency_type='$i'")->result_array();    
    }
    }
    
    function get_accessory_name($dependent_accessory_id)
    {
    return $this->db->query("select accessory_name from accessory where accessory_id='$dependent_accessory_id'")->result_array();        
    }
    function create_variant_dependency($data)
    {
    $accessory_id=$data['accessory_id'];    
    $dependent_accessory_id=$data['dependent_accessory_id'];    
    $dependency_type=$data['dependency_type'];    
    $res=$this->db->query("select * from accessory_dependencies_mapping where accessory_id='$accessory_id' and dependent_accessory_id='$dependent_accessory_id' and dependency_type='$dependency_type'")->result_array();            
    if(count($res)==0)
    { 
    $this->db->insert('accessory_dependencies_mapping', $data);
    return $this->db->insert_id();    
    }
    }
    function get_variant_dependency($id)
    {
    if($id==1)
    {
    return $res=$this->db->query("select ad.*,a.accessory_name from accessory_dependencies_mapping ad,accessory a where a.accessory_id=ad.accessory_id")->result_array();                
    }   
    }
    function delete_acc_dependencies($accessory_id)
    {
    return $res=$this->db->query("DELETE FROM `accessory_dependencies_mapping` WHERE `id`='$accessory_id'");                    
    }
    function get_all_map_accessory()
    {
    return $sel=$this->db->query("select vm.*,v.pro_name,a.accessory_name,a.accessory_information from accessory_variant_mapping vm,variant v,accessory a "
            . "where v.variant_id=vm.variant_id and a.accessory_id=vm.accessory_id order by vm.id DESC")->result_array();                          ;
    }
    
    
}