<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reviews_mdl extends CI_Model {
    /* user review goes here */

    function user_review($data) {
        $get_data = $this->db->insert('user_rating', $data);
        return;
    }

    /* user review goes here */

    /* user review goes here */

    function get_review($pro_id, $user_sort, $id, $pag) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select('user_id,user_rating_id,date,heading,what_i_like,what_i_dont_like,total');
        if ($user_sort == 'Rating') {
            $this->db->order_by("total", "desc");
        }
        if ($user_sort == 'Date') {
            $this->db->order_by("date", "desc");
        }
        $this->db->order_by("user_rating_id","desc");
        if ($id == 1) {
            return $get_data = $this->db->get('user_rating')->result_array();            
        } else {
            $get_data = $this->db->get('user_rating');
            return $tot = $get_data->num_rows();
        }
    }

    function get_exp_review($pro_id, $user_sort, $id) {

        $this->db->where('pro_id', $pro_id);
        $this->db->select('expert_rating_id,date,heading,what_i_like,what_i_dont_like,total,profile_id');
        if ($user_sort == 'Rating') {
            $this->db->order_by("total", "desc");
        }
        if ($user_sort == 'Date') {
            $this->db->order_by("date", "desc");            
        }
        $this->db->order_by("expert_rating_id","desc");
        if ($id == 1) {
              return $get_data = $this->db->get('expert_rating')->result_array();
        } else {
            $get_data = $this->db->get('expert_rating');
            return $tot = $get_data->num_rows();
        }
    }
    
    function get_user_review_img($user_id)
    {
    return $this->db->query("SELECT fb_id FROM `user` where user_id='$user_id'")->result_array();           
    }
    
    function get_tot_review($pro_id, $type) {
        if ($type == "expert") {
            $this->db->where('pro_id', $pro_id);
            $this->db->select_avg('total');
            $get_data = $this->db->get('expert_rating');
        }

        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $get;
    }

    function tot_user_reviews($pro_id, $id) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select_avg('total');
        if ($id == 1) {
            $get_data = $this->db->get('expert_rating');
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        if ($id == 2) {
            $get_data = $this->db->get('user_rating');
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    /* user review goes here */
}
