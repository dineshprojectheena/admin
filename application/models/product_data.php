<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_data extends CI_Model {

    function get_model() {
//        $this->db->select('model_name,model_id');
//        return $get_data = $this->db->get('model')->result_array();
        return $get_data = $this->db->query("select model_name,model_id from model where status='1'")->result_array();
    }

    function get_brand() {
//        $this->db->select('brand_name,brand_id');
        return $get_data = $this->db->query("select brand_name,brand_id from brand where status='1'")->result_array();
    }
    
    function upload_video($data2)
    {
     $get_data = $this->db->insert('variant_video', $data2);   
     return $pro_id = $this->db->insert_id();
    }
    
    function update_video($data2,$variant_video_id)
    {
     $this->db->where('variant_video_id', $variant_video_id);
        $get_data = $this->db->update('variant_video', $data2);
        return;
    }
    
    
    function get_product($variant_id) {
        $this->db->where('variant_id', $variant_id);
        $get_data1 = $this->db->get('variant')->result_array();

        $this->db->where('variant_id', $variant_id);
        $get_data2 = $this->db->get('specification')->result_array();

        return $specifiaction_data = array_merge($get_data1, $get_data2);
    }
    function get_all_variant() {
        return $specifiaction_data = $this->db->query("select variant_id,pro_name_comp from variant order by pro_name_comp ASC ")->result_array();
    }

    function get_body_color() {
        $this->db->select('body_color_id,body_icon,color');
        $this->db->where('status', '1');
        return $get_data1 = $this->db->get('body_color')->result_array();
    }

    function get_body_wheel() {
        $this->db->select('wheel_design,wheel_id,wheel_description');
        $this->db->where('status', '1');
        return $get_data1 = $this->db->get('wheel')->result_array();
    }

    function upload_product($data, $data2) {
        $get_data = $this->db->insert('variant', $data);
        $pro_id = $this->db->insert_id();
        $pro_array = array('variant_id' => $pro_id);
        $specifiaction_data = array_merge($pro_array, $data2);
        $get_data = $this->db->insert('specification', $specifiaction_data);
    }

    function update_product($data, $data2, $variant_id) {
        $this->db->where('variant_id', $variant_id);
        $get_data = $this->db->update('variant', $data);
//        $pro_id = $this->db->insert_id();
        $pro_array = array('variant_id' => $variant_id);
        $specifiaction_data = array_merge($pro_array, $data2);
        $this->db->where('variant_id', $variant_id);
        $get_data = $this->db->update('specification', $specifiaction_data);
    }

    function load_product() {
//        echo 'SELECT v.`variant_id`,v.`pro_name`,v.`model_id`,m.`model_name`,b.`brand_id`,b.`brand_name`,v.`status` FROM `variant` v,`model`m,`brand` b where m.`model_id`=v.`model_id` and v.`brand_id`=b.`brand_id`';
        return $this->db->query('SELECT v.`variant_id`,v.`pro_name`,v.`model_id`,v.`status`,(select b.brand_name from brand b where v.brand_id=b.brand_id) as brand_name,(select m.model_name from model m where m.model_id=v.model_id) as model_name FROM `variant` v')->result_array();
//        return $this->db->query('SELECT v.`variant_id`,v.`pro_name`,v.`model_id`,m.`model_name`,b.`brand_id`,b.`brand_name`,v.`status` FROM `variant` v,`model`m,`brand` b where m.`model_id`=v.`model_id` and v.`brand_id`=b.`brand_id`')->result_array();
    }

    function delete_varaint($variant_id) {
        
        
        
       $select=$this->db->query("select status,upcoming from variant where variant_id='$variant_id'")->row_array(); 
       print_r($select);
//       exit;
       if($select['upcoming']=='y')
       {
       
       $this->db->query("update variant set status='3' where variant_id='$variant_id'");     
       }
       else
       {
       if($select['status']==2)
       {
       $status=1;    
       }    
       else
       {
       $status=2;    
       }    
           
       $this->db->query("update variant set status='$status' where variant_id='$variant_id'");     
       }
       return;
       }

    function upload_gallery($data) {
        return $get_data = $this->db->insert('product_gallery', $data);
    }

    function update_gallery($data, $product_gallery_id) {
        $this->db->where('product_gallery_id', $product_gallery_id);
        return $get_data=$this->db->update('product_gallery',$data);
    }

    function load_gallery($id) {
        if ($id == '') {
            return $this->db->query('SELECT g.product_gallery_id,g.image,g.video,g.page,v.variant_id,v.pro_name FROM `product_gallery` g,`variant` v where g.`prduct_id`=v.`variant_id`')->result_array();
        } else if ($id != '') {
            return $this->db->query("SELECT g.product_gallery_id,g.image,g.video,g.page,v.variant_id,v.pro_name FROM `product_gallery` g,`variant` v where g.`prduct_id`=v.`variant_id` and g.product_gallery_id='$id'")->result_array();
        }
    }
    function get_color() {
            return $this->db->query("SELECT DISTINCT(feature_desc) as color  FROM `features` WHERE `feature_name` = 'Color'")->result_array();
    }

    function get_coupon() {
            return $this->db->query("select * from coupon")->result_array();
    }

    function delete_gallery($product_gallery_id) {
        $this->db->where('product_gallery_id', $product_gallery_id);
        $get_data = $this->db->get('product_gallery')->result_array();
        unlink('uploads/gallery/' . $get_data[0]['image']);
        $this->db->where('product_gallery_id', $product_gallery_id);
        $del = $this->db->delete('product_gallery');
//        exit;
    }

}
