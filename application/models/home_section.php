<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_section extends CI_Model {
    #### Header city section get here #### 

    function all_citys_detalis() {
        return $query = $this->db->query("SELECT name,city_id FROM `city`")->result_array();
    }

    #### Home page featured product goes here #### 

    function featured($featured_type) {
        return $query = $this->db->query("SELECT f.`featured_id`, f.`prd_id_1`,f.`prd_id_2` FROM `featured` f WHERE f.`status`='1' and f.`featured_type`='$featured_type'")->result_array();
    }

    function top_selling_featured($featured_type) {
        return $query = $this->db->query("SELECT f.`featured_id`, f.`prd_id_1`, f.`status`,v.`pro_name`,v.`pro_image` FROM `featured` f,`variant` v WHERE f.`status`='1' and f.`featured_type`='$featured_type' and f.`prd_id_1`=v.`variant_id`")->result_array();
    }

    function product_detail($pro_id, $comp_cond) {
        if ($comp_cond == 1) {
            return $query = $this->db->query("SELECT v.pro_name,v.city,v.fuel_type,v.tramission_type,v.body_color_id,v.pro_image,v.variant_id,v.pro_detail,m.model_name,(select avg(total) from user_rating where pro_id=$pro_id) as user_rating,(select avg(total) from expert_rating where pro_id=$pro_id) as exp_rating,(select mnc_exshowroom_price from mnc_exshowroom_price where product_id=$pro_id) as mnc_price,(select exshowroom_price from exshowroom_price where product_id=$pro_id) as ex_price FROM `variant` v,`model` m WHERE m.model_id=v.model_id and v.variant_id=$pro_id")->result_array();
        } else {
            $this->db->where('variant_id', $pro_id);
            $this->db->select('pro_name,pro_image,variant_id');
            return $get_data = $this->db->get('variant')->result_array();
        }
    }

    function product_features($pro_id) {
        return $query = $this->db->query("SELECT m.`id`, m.`feature_id`, m.`variant_id`, m.`status`,f.feature_name,f.feature_desc,f.feature_type FROM `feature_variant_mapping` m,`features` f WHERE m.feature_id=f.feature_id and  m.variant_id=$pro_id")->result_array();
    }

    function get_features_name($pro_ids, $id) {
        if ($id == 1) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 2) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 3) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 4) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 5) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 6) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        }
    }

    function product_variant_detail($id) {
        return $query = $this->db->query("SELECT * FROM `specification` WHERE variant_id='$id'")->result_array();
    }

    function product_color($color_id, $comp_cond) {
        if ($comp_cond == 2) {
            return $query = $this->db->query("SELECT `body_color_id`, `body_icon`, `color` FROM `body_color` WHERE `status`='1' and `body_color_id` IN ($color_id)")->result_array();
        }
    }

    function reting_product_detail($pro_id) {
        $this->db->where('variant_id', $pro_id);
        $this->db->select('pro_name,pro_image,variant_id,pro_detail');
        return $get_data = $this->db->get('variant')->result_array();
    }

    function expert_review_count($pro_id, $id) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select_avg('total');
        if ($id == 1) {
            return $get_data = $this->db->get('expert_rating')->result_array();
        }
        if ($id == 2) {
            return $get_data = $this->db->get('user_rating')->result_array();
        }
    }
    
    function rating_details($id)
    {
    $get_data=$this->db->query("SELECT pro_name,variant_id,model_id,brand_id FROM `variant` where variant_id='$id'")->result_array();
    $model_id=$get_data[0]['model_id'];
    $brand_id=$get_data[0]['brand_id'];
    return $all_res=$this->db->query("SELECT v.pro_name,v.variant_id,v.model_id,v.brand_id,b.brand_name,m.model_name FROM `variant` v,brand b,model m where v.model_id='$model_id' and v.brand_id='$brand_id' and b.brand_id=v.brand_id and m.model_id=v.model_id")->result_array();
//   
    }
    

    function ex_showroom_price($pro_id) {
        
        return $get_data = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$pro_id'")->result_array();
    }

    function mnc_ex_showroom_price($pro_id) {
        return $get_data = $this->db->query("SELECT `exshowroom_price` FROM `exshowroom_price` where product_id='$pro_id'")->result_array();
    }

    function get_news() {
//        $this->db->where('product_id',$pro_id);
        $this->db->select('news_title,latest_news_id');
        $this->db->order_by("added_date", "desc");
        return $get_data = $this->db->get('news', '4')->result_array();
    }

    function get_allnews_details() {
        return $get_data = $this->db->get('news')->result_array();
    }

    function get_news_details($id) {
        if ($id != '') {
            $this->db->where('latest_news_id', $id);
        }
        $this->db->select('*');
        return $get_data = $this->db->get('news')->result_array();
    }

    ####get all brands related process here...####

    function get_brands($id) {
        if ($id == "") {
            return $query = $this->db->query("SELECT `brand_id`,`brand_name`,`brand_image` FROM `brand` WHERE `status`='1'")->result_array();
        }
    }

    ####get home page carousel process here...####

    function home_page_carousel($brand_id){
        $m_q='';
        $main_city = $this->input->cookie('main_city', TRUE);
        $m_q.="SELECT `variant_id`,`pro_name`, `pro_image`,`brand_id` FROM `variant` WHERE 1=1 ";
        if($brand_id!=""){
            $m_q.=" and `brand_id`='$brand_id'";
        }
        
        if($main_city != "") {
            $m_q.=" and `city`='$main_city'";
        }
        $m_q;
        return $query = $this->db->query($m_q)->result_array();
    }

    function home_page_carousel_exshow($product_id, $product_id2) {
        if ($product_id != '') {
            return $query = $this->db->query("SELECT `mnc_exshowroom_price`,total_savings,total_benefits FROM `mnc_exshowroom_price` WHERE `product_id`='$product_id'")->result_array();
        }
        if ($product_id2 != '') {
            return $query = $this->db->query("SELECT `exshowroom_price`,total_savings,total_benefits FROM `exshowroom_price` WHERE `product_id`='$product_id2'")->result_array();
        }
    }

    function select_your_car_pro($brand_name) {
        return $query = $this->db->query("SELECT `brand_id`, `brand_name`, `brand_image`, `details` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
    }

    function select_your_car_model($brand_name) {
        $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
        $brand_id = $query[0]['brand_id'];
        return $model_result = $this->db->query("SELECT `model_name`,`model_id` FROM `model` WHERE `brand_id`='$brand_id'")->result_array();
    }

    function select_your_car_variant($brand_name) {
        $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
        $brand_id = $query[0]['brand_id'];
        $m_q = '';
        $main_city = $this->input->cookie('main_city', TRUE);
        $m_q.="SELECT `variant_id`,`pro_name` FROM `variant` WHERE 1=1 ";
        if ($brand_id != "") {
            $m_q.=" and `brand_id`='$brand_id'";
        }
        if ($main_city != "") {
            $m_q.=" and `city`='$main_city'";
        }
        return $variant_result = $this->db->query($m_q)->result_array();
    }

    function select_your_car_pro_details($brand_name, $cond, $main_city) {
        $m_q = '';
        if ($main_city == '') {
            $main_city = $this->input->cookie('main_city', TRUE);
        }
        $m_q.="SELECT v.variant_id,v.pro_name,v.pro_image,m.mnc_exshowroom_price,e.exshowroom_price FROM `variant` v Left JOIN mnc_exshowroom_price m on m.product_id=v.variant_id Left JOIN exshowroom_price e on e.product_id=v.variant_id  WHERE 1=1 ";
        if ($main_city != "") {
            $m_q.=" and v.`city`='$main_city' ";
        }

        if ($cond == 1) {
            $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
            $brand_id = $query[0]['brand_id'];
            $m_q.=" and v.`brand_id`='$brand_id'";
            return $variant_result = $this->db->query($m_q)->result_array();
        } else if ($cond == 2) {
            $variant_id = $brand_name;
            $m_q.=" and v.`variant_id`='$variant_id'";
            return $variant_result = $this->db->query($m_q)->result_array();
        } else if ($cond == 3) {
            $model_id = $brand_name;
            $m_q.=" and v.`model_id`='$model_id'";
            return $variant_result = $this->db->query($m_q)->result_array();
//            return $variant_result = $this->db->query("SELECT v.variant_id,v.pro_name,v.pro_image,m.mnc_exshowroom_price,e.exshowroom_price FROM `variant` v Left JOIN mnc_exshowroom_price m on m.product_id=v.variant_id Left JOIN exshowroom_price e on e.product_id=v.variant_id  WHERE v.`model_id`='$model_id'")->result_array();
        }
//        echo $m_q;
    }

    function select_offers_product($brand) {
        $brand_result = $this->db->query("SELECT brand_id,brand_name FROM `brand` where `brand_name`='$brand'")->result_array();
        $brand_id = $brand_result[0]['brand_id'];
        $offer_result = $this->db->query("SELECT * FROM `deal` where `brand_id`='$brand_id' and status='1'")->result_array();
        foreach ($offer_result as $offer_result_data) {
            $variant_id = $offer_result_data['variant_id'];
            $variant_name = $this->db->query("SELECT pro_name FROM `variant` where `variant_id`='$variant_id'")->result_array();
            $final[] = array_merge($variant_name[0], $offer_result_data);
        }
        return $final;
    }

    function pro_offer($pro_id, $user_id) {
        return $res = $this->db->query("SELECT * FROM `deal` d,join_group j where d.`variant_id`='$pro_id' and j.user_id='$user_id' and j.status='1'")->result_array();
    }

    ####get home page carousel process here...####
    ####get car type selection page process here...####

    function car_type_selection_filters($id, $type,$pro_price, $brand, $fuel, $min, $min2,$city) {
//        echo $city;
        if ($city== '') {
            $city= $this->input->cookie('main_city', TRUE);
        }
        
        if ($id==1){
            return $query=$this->db->query("SELECT `brand_id`,`brand_name` FROM `brand`")->result_array();
        } else if($id==2){
            $main_query = "";
            $main_query.= "SELECT v.`variant_id`,v.pro_name,v.`pro_image`,m.`model_name`,b.`brand_name`,b.`brand_image` FROM `variant` v left join `model` m on m.`model_id`=v.`model_id` left join `brand` b on b.`brand_id`=v.`brand_id`,`mnc_exshowroom_price` e";
            $main_query.=" where v.variant_id=e.product_id and ";
            if ($type != '') {
                $main_query.=" v.`pro_type`='$type' and ";
            }
             if ($pro_price != '') {
                $exp_price=explode("_",$pro_price);  
                print_r($exp_price);
                $main_query.=" e.`mnc_exshowroom_price` between '$exp_price[0]' and '$exp_price[1]' and ";
            }
            if ($brand != '') {
                $main_query.=" v.`brand_id`='$brand' and ";
            }
            if ($fuel != '') {
                $main_query.=" v.`fuel_type`='$fuel' and ";
            }
            if ($min != '' && $min2 != '') {
                $main_query.=" v.`seating_capacity` between '$min' and '$min2' and ";
            }
            if ($city != "") {
                $main_query.=" v.`city`='$city' and ";
            }

            $main_query.=" 1";
            
            $query = $this->db->query($main_query)->result_array();
            $array = array();
            foreach ($query as $res) {
                $variant_id = $res["variant_id"];
                $ex_show_price = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id'")->result_array();
                if (empty($ex_show_price[0])) {
                    $ex_show_price[0] = array('exshowroom_price' => '');
                }
                $mnc_ex_show_price = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id'")->result_array();
                if (empty($mnc_ex_show_price[0])) {
                    $mnc_ex_show_price[0] = array('mnc_exshowroom_price' => '');
                }
                $user_rating = $this->db->query("SELECT AVG(`total`) as user_total FROM `user_rating` where pro_id='$variant_id'")->result_array();
                if (empty($user_rating[0])) {
                    $user_rating[0] = array('user_total' => '');
                }
                $expert_rating = $this->db->query("SELECT AVG(`total`) as expert_total FROM `expert_rating` where pro_id='$variant_id'")->result_array();
                if (empty($expert_rating[0])) {
                    $expert_rating[0] = array('expert_total' => '');
                }
                $array2 = array('query' => $main_query);
                $combine = array_merge($res, $ex_show_price[0], $mnc_ex_show_price[0], $user_rating[0], $expert_rating[0], $array2);
                $array[] = $combine;
            }
            return $array;
        }
    }

    ####get car type selection page process here...####
    ####get car introduction page details here...####

    function car_intro_detail($id) {
        return $car_intro_detail = $this->db->query("SELECT v.pro_name,v.pro_name_comp,v.wheel_id,v.body_color_id,v.variant_id,v.pro_detail,v.pro_image,(select brand_name from brand where brand_id=v.brand_id) as brand_name,(select model_name from model where model_id=v.model_id) as model_name FROM `variant` v where v.variant_id='$id'  and v.status='1'")->result_array();
    }
    
    function car_configuration($user_id,$id,$cond)
    {
    if($cond=='1')
    {    
    return $user=$this->db->query("SELECT *  FROM `car_configuration` where user_id='$user_id' and product_id='$id' and status!='2'")->result_array();   
    }
    else 
    {    
    return $user=$this->db->query("SELECT *  FROM `car_configuration` where user_id='$user_id' and product_id='$id' ")->result_array();   
    }
    }
    
    function car_emi($id,$user_id)
{
return $user=$this->db->query("SELECT *  FROM `emi_calculation` where user_id='$user_id' and pro_id='$id'")->result_array();   

}
    

    function car_intro_gallery_detail($id, $type) {
        return $car_intro_detail = $this->db->query("SELECT image,product_gallery_id,video FROM `product_gallery` where prduct_id='$id' and page='$type'")->result_array();
    }

    function car_intro_user_reviews($id) {
        return $car_intro_detail = $this->db->query("SELECT r.`total`,r.`what_i_like`,r.`what_i_dont_like`,u.`firstname` FROM `user_rating` r,`user` u where u.`user_id`=r.`user_id` and r.`pro_id`='$id' order by `user_rating_id` DESC limit 1")->result_array();
    }

    function car_intro_expert_reviews($id) {
        return $car_intro_detail = $this->db->query("SELECT `expert_rating_id`,`journilist_name`,`what_i_like`,`what_i_dont_like`,`total` FROM `expert_rating` WHERE `pro_id`='$id' ORDER BY `expert_rating_id` DESC limit 1")->result_array();
    }

    function car_intro_exshowroom_price($id) {
        return $car_intro_detail = $this->db->query("SELECT `exshowroom_price` FROM `exshowroom_price` WHERE `product_id`='$id'")->result_array();
    }

    function car_variant_details($id) {
        $car_intro_detail = $this->db->query("SELECT model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id`='$id'")->result_array();
        foreach ($car_intro_detail as $car_intro_detail_data)
            $model_id = $car_intro_detail_data['model_id'];
        $brand_id = $car_intro_detail_data['brand_id'];
        $pro_name = $car_intro_detail_data['pro_name'];
        $car_variant_detail = $this->db->query("SELECT v.*,s.mileage,s.engine_maximum_power FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and v.`pro_name`='$pro_name' and s.variant_id=v.variant_id")->result_array();

        foreach ($car_variant_detail as $car_variant_detail_data) {
            $variant_id = $car_variant_detail_data['variant_id'];
            $car_variant_detail = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id'")->result_array();
            $car_variant_detail2 = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id'")->result_array();
            $merge_data[] = array_merge($car_variant_detail, $car_variant_detail_data, $car_variant_detail2);
        }
        return $merge_data;
    }

    function filter_car_variant_details($id, $trimline, $engine, $transmission) {
        if ($trimline != '') {
//        $trimline='1,2,3,';    
            $selected_data = explode(',', $trimline);
            $res = '';
            for ($i = 0; $i <= count($selected_data) - 2; $i++) {
                $res = $res . "'" . $selected_data[$i] . "',";
            }
            $select_acc = rtrim($res, ",");
            $car_intro_detail = $this->db->query("SELECT model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id` IN ($select_acc)")->result_array();
        } else {
            $car_intro_detail = $this->db->query("SELECT model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id`='$id'")->result_array();
        }

        foreach ($car_intro_detail as $car_intro_detail_data) {
            $model_id = $car_intro_detail_data['model_id'];
            $brand_id = $car_intro_detail_data['brand_id'];
            $pro_name = $car_intro_detail_data['pro_name'];
            $car_variant_detail = '';
            $car_variant_detail.="SELECT v.*,s.mileage,s.engine_maximum_power FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and v.`pro_name`='$pro_name' and s.variant_id=v.variant_id";
            if ($engine != '') {
                $car_variant_detail.=" and v.fuel_type='$engine'";
            }
            if ($transmission != '') {
                $car_variant_detail.=" and v.tramission_type='$transmission'";
            }
            $merge_data[] = '';
            $car_variant_details = $this->db->query($car_variant_detail)->result_array();
            foreach ($car_variant_details as $car_variant_detail_data) {
                $variant_id = $car_variant_detail_data['variant_id'];
                $ex_price = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id'")->result_array();
                $mncex_price = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id'")->result_array();
                $merge_data[] = array_merge($car_variant_detail_data, $ex_price, $mncex_price);
            }
        }
        return $merge_data;
    }

    ####get car introduction page details here...####
    #####get booking details here...####

    function booking_detail($pro_id, $user_id, $cond) {
        error_reporting(0);
        if ($cond == 1) {
	    $city= $this->input->cookie('main_city', TRUE);
            $variant_type_detail = $this->db->query("SELECT variant_type FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $variant_type = explode(',', $variant_type_detail[0]['variant_type']);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                if ($variant_type_data != '') {
                    $ids = "'" . $variant_type_data . "'," . $ids;
                }
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
            $car_intro_detail = $this->db->query("SELECT variant_id,model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id` IN ($ids)")->result_array();
	    foreach ($car_intro_detail as $car_intro_detail_data) {
                $model_id = $car_intro_detail_data['model_id'];
                $brand_id = $car_intro_detail_data['brand_id'];
                $pro_name = $car_intro_detail_data['pro_name'];
                $variant_id = $car_intro_detail_data['variant_id'];
                $car_variant_detail = '';
                $car_variant_detail.="SELECT v.*,s.mileage,s.engine_maximum_power FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and v.`pro_name`='$pro_name' and s.variant_id=v.variant_id and v.variant_id='$variant_id'";
                $merge_data[] = '';
                $car_variant_details = $this->db->query($car_variant_detail)->result_array();
                foreach ($car_variant_details as $car_variant_detail_data) {
                    $variant_id = $car_variant_detail_data['variant_id'];
                    $ex_price = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id' and city_id='$city'")->row_array();
                    $mncex_price = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id' and city_id='$city'")->row_array();
                    $merge_data[] = array_merge($car_variant_detail_data, $ex_price, $mncex_price);
                }
                return $merge_data;
            }
        } else if ($cond == 2) {
            $variant_type_detail = $this->db->query("SELECT color,wheel FROM `car_configuration` WHERE `product_id`='$pro_id'  and user_id='$user_id'")->result_array();
            $color = $variant_type_detail[0]['color'];
            $wheel = $variant_type_detail[0]['wheel'];
            return $car_intro_detail = $this->db->query("SELECT f.*,fv.* FROM `features` f,`feature_variant_mapping` fv where f.feature_id IN ('$color','$wheel') and fv.feature_id=f.feature_id and fv.variant_id='$pro_id'")->result_array();
        } else if ($cond == 3) {
            $variant_type_detail = $this->db->query("SELECT leather,cloth,dashboard FROM `car_configuration` WHERE `product_id`='$pro_id'  and user_id='$user_id'")->result_array();
            $leather = $variant_type_detail[0]['leather'];
            $cloth = $variant_type_detail[0]['cloth'];
            $dashboard = $variant_type_detail[0]['dashboard'];
            return $car_intro_details = $this->db->query("SELECT f.*,fv.* FROM `features` f,`feature_variant_mapping` fv where f.feature_id IN ('$leather','$cloth','$dashboard') and fv.feature_id=f.feature_id and fv.variant_id='$pro_id'")->result_array();
        } else if ($cond == 4) {
            $variant_type_detail = $this->db->query("SELECT accessory FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $variant_type = explode(',', $variant_type_detail[0]['accessory']);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                $ids = "'" . trim($variant_type_data) . "'," . $ids;
            }
            $ids = substr($ids, 0, -1);
	    	
            return $car_intro_details = $this->db->query("SELECT a.*,av.* FROM `accessory` a,`accessory_variant_mapping` av where av.accessory_id IN ($ids) and av.accessory_id=a.accessory_id and av.variant_id='$pro_id'")->result_array();
        }
    }

    #####get booking details here...####
    ##### On road price calucaltion ####

    function onroad_price_calculation($pro_id, $user_id, $cond) {
        if ($cond == 1) {
            $car_intro_details = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and user_id='$user_id'")->result_array();
            if (!empty($car_intro_details)) {
                $color = $car_intro_details[0]['color'];
                $wheel = $car_intro_details[0]['wheel'];
                $variant_type = $car_intro_details[0]['variant_type'];
                $leather = $car_intro_details[0]['leather'];
                $cloth = $car_intro_details[0]['cloth'];
                $dashboard = $car_intro_details[0]['dashboard'];
                $car_color_price = $this->db->query("SELECT price FROM `feature_variant_mapping` where feature_id IN ('$color','$wheel','$leather','$cloth','$dashboard')")->result_array();
                $price = '';
                foreach ($car_color_price as $car_color_price_data) {
                    $car_color_price_data['price'];
                    $price = $price + $car_color_price_data['price'];
                }
                return $price;
            }
        } else if ($cond == 2) {

            $car_intro_details = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and user_id='$user_id'")->result_array();
            if (!empty($car_intro_details)) {

                $accessory = $car_intro_details[0]['accessory'];
                $variant_type = explode(',', $accessory);
                $ids = '';
                foreach ($variant_type as $variant_type_data) {
                    $ids = "'" . $variant_type_data . "'," . $ids;
                }
                $ids = substr($ids, 0, -1);

                $car_color_price = $this->db->query("SELECT price FROM `feature_variant_mapping` where feature_id IN ($ids)")->result_array();
                $price = '';
                foreach ($car_color_price as $car_color_price_data) {
                    $car_color_price_data['price'];
                    $price = $price + $car_color_price_data['price'];
                }
                return $price;
            }
        }
    }

    ##### On road price calucaltion ####

    #
   function additional_info($id, $user_id) {
        $car_intro_detail = $this->db->query("SELECT * FROM `car_configuration` WHERE `product_id`='$id' and user_id='$user_id' and status!='2'")->result_array();
        if (!empty($car_intro_detail)) {
            $ids = "'" . $car_intro_detail[0]['color'] . "'," . "'" . $car_intro_detail[0]['wheel'] . "'," . "'" . $car_intro_detail[0]['leather'] . "'," . "'" . $car_intro_detail[0]['cloth'] . "'," . "'" . $car_intro_detail[0]['dashboard'] . "'";
            return $caluclation = $this->db->query("SELECT sum(price) as price FROM `feature_variant_mapping` WHERE `feature_id` IN ($ids) and variant_id='$id'")->result_array();
        }
    }

    function accessory_details_res($id, $user_id) {
        $car_intro_detail = $this->db->query("SELECT * FROM `car_configuration` WHERE `product_id`='$id' and user_id='$user_id' and status!='2'")->result_array();
        if (!empty($car_intro_detail)) {
            $ids = $car_intro_detail[0]['accessory'];
            $selected_data = explode(',', $ids);
            $res = '';
            for ($i = 0; $i <= count($selected_data) - 2; $i++) {
                $res = $res . "'" . $selected_data[$i] . "',";
            }
            $select_acc = rtrim($res, ",");
            if ($select_acc == '') {
                $select_acc = "''";
            }
            return $caluclation = $this->db->query("SELECT sum(price) as price FROM `accessory_variant_mapping` WHERE `accessory_id` IN ($select_acc) and variant_id='$id'")->result_array();
        }
    }
    
    function onraod_calculation()
    {
    $main_city=$this->input->cookie('main_city',TRUE);    
    return $caluclation = $this->db->query("SELECT octroi,road_tax,registeration_charges,handling_price,other_charges,home_delievery_charge FROM `onroad_price` WHERE `city_id`='$main_city'")->result_array();
//    print_r($caluclation);
//    echo $user_id;    
    }
    
    

    #
    ####Change Car start here...####

    function comparison_model_data($brand_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `model` WHERE `brand_id`='$brand_data'")->result_array();
    }

    function comparison_variant_data($brand_data, $brand_model) {
        return $car_intro_detail = $this->db->query("SELECT variant_id,pro_name FROM `variant` WHERE `model_id`='$brand_model' and `brand_id`='$brand_data'")->result_array();
    }

    ####Change Car start here...####
    #####Change exterior_selection here...####

    function car_color($color_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `body_color` WHERE `body_color_id` IN($color_data)")->result_array();
    }

    function car_wheel($wheel_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `wheel` WHERE `wheel_id` IN($wheel_data)")->result_array();
//    echo $wheel_data;    
    }

    function car_specifiaction($id, $type) {

        if (!empty($id)) {
            return $car_intro_detail = $this->db->query("SELECT m.`price`,f.`feature_id`, f.`feature_type`, f.`feature_name`, f.`feature_desc`, f.`feature_img`, f.`recomended`, f.`cost` FROM `feature_variant_mapping` m,`features` f WHERE m.`variant_id`='$id' and m.`feature_id`=f.`feature_id` and  `f`.`feature_name` like '%$type%'")->result_array();
        }
    }

    #####Change exterior_selection here...####
    ######Change configuration accessory here...####

    function car_exterior($id, $pro_id) {
        return $car_intro_detail = $this->db->query("SELECT m.*,a.* FROM `accessory_variant_mapping` m,`accessory` a where a.accessory_id=m.accessory_id and m.variant_id='$pro_id' and a.accessory_cat='$id'")->result_array();
    }

    function car_exterior_selected($user_id, $pro_id) {
        return $car_intro_detail = $this->db->query("SELECT accessory FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and status!='2'")->result_array();
    }

    ######Change configuration accessory here...####
    ###### Selected Car Configuration Details ######

    function config_selected_detail($pro_id, $user_id) {
        return $data = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and status!='2'")->result_array();
    }

    ###### Selected Car Configuration Details ######
    #### Get brand & model wise variants ####

    function get_model_wise_variants($model, $brand_id) {
//        return '0';
        return $data = $this->db->query("SELECT variant,variant_id FROM `variant` where model_id='$model' and brand_id='$brand_id'")->result_array();
    }

    #### Get brand & model wise variants ####
    #### Get variant id wise video ####

    function get_product_wise_video($variant_video) {
        return $data = $this->db->query("SELECT video FROM  `product_gallery` where prduct_id='$variant_video'")->result_array();
    }

    #### Get variant id wise video ####
    #### Order Status Start####

    function order_confirm($pro_id, $user_id) {
//        echo '<pre>';
        $pro_id;
        $user_id;
        $data = $this->db->query("SELECT car_configuration_id,product_id FROM `car_configuration` where user_id='$user_id' and product_id='$pro_id' and status!='2'")->result_array();
        if (!empty($data)) {
            $car_configuration_id = $data[0]['car_configuration_id'];
            $product_id = $data[0]['product_id'];
            $update_config = $this->db->query("UPDATE `car_configuration` SET `status`='2' WHERE car_configuration_id='$car_configuration_id'");
            $check = $this->db->query("select `order_id` from `order` where product_id='$pro_id' and user_id='$user_id' and configuration_id='$car_configuration_id'");
            $count_order = $check->num_rows();
            $invoice_no = 'INV-MNC-' . $car_configuration_id;
            if ($count_order == 0) {
                $insert_order = $this->db->query("INSERT INTO `order`(`product_id`,`user_id`,`invoice_no`,`configuration_id`) VALUES ('$pro_id','$user_id','$invoice_no','$car_configuration_id')");
            }
        }
        $order_details = $this->db->query("select `order_id`,`user_id`,`configuration_id`,`invoice_no`,`product_id` from `order` where user_id='$user_id' order by order_id DESC limit 1")->result_array();
        $order_details_rec = array('invoice_no' => $order_details[0]['invoice_no']);
        $order_id = $order_details[0]['order_id'];
        $user_id = $order_details[0]['user_id'];
        $configuration_id = $order_details[0]['configuration_id'];
        $invoice_no = $order_details[0]['invoice_no'];
        $product_id = $order_details[0]['product_id'];
        $user_details = $this->db->query("select firstname from `user` where user_id='$user_id'")->result_array();
        $pro_details = $this->db->query("select variant_id,pro_name,pro_image,pro_detail,variant from `variant` where variant_id='$pro_id'")->result_array();
        return $final_res = array_merge($user_details[0], $pro_details[0], $order_details_rec);
//        echo '</pre>';
    }

    function dependencies_res($select_acc) {
        return $details = $this->db->query("select accessory_id,dependent_accessory_id,dependency_type,dependent_accessory_name from `accessory_dependencies_mapping` where accessory_id IN ($select_acc)")->result_array();
    }

    #### Order Status End####
    
    
    #### Order Status End####
    function max_price($city)
    {
    $que="";    
    $que.="SELECT MAX(m.mnc_exshowroom_price) as max_price FROM `mnc_exshowroom_price` m,`variant` v where v.`variant_id`=m.`product_id`";    
    if($city!='')
    {
    $que.=" and v.city='$city'";        
    }    
    return $details=$this->db->query($que)->result_array();     
    }
    #### Order Status End####
    
    function check_username($regis_username,$a)
    {
     if($a==1)
     {    
     return $details=$this->db->query("select user_id from user where username='$regis_username'")->result_array();  
     }
     if($a==2)
     {    
     return $details=$this->db->query("select user_id from user where email='$regis_username'")->result_array();  
     }
    }
    function custome_user($regis_username,$regis_email,$regis_password,$id)
    {
        
    $mds_pass=md5($regis_password);
    $res=$this->db->query("select * from temp_user where email_id='$regis_email' and username='$regis_username' and status!='1'")->result_array();  
    if(empty($res))
    {    
    $uniue_id=md5(uniqid($regis_username,true));
    $details=$this->db->query("INSERT INTO `temp_user`(`temp_id`, `username`, `email_id`, `password`, `random_key`,`status`) "
            . "VALUES ('','$regis_username','$regis_email','$mds_pass','$uniue_id','0')");      
    $id=$this->db->insert_id();
    return $res=$this->db->query("select * from temp_user where temp_id='$id' and email_id='$regis_email' and username='$regis_username' and status!='1'")->result_array();  
    
    }
    else{
    
    return $res;        
    }
    }
    function activate_user($id)
    {
    $res=$this->db->query("select * from temp_user where status='0' and random_key='$id'")->result_array();  
    if(!empty($res))
    {
    $temp_id=$res[0]['temp_id'];    
    $username=$res[0]['username'];    
    $email_id=$res[0]['email_id'];    
    $password=$res[0]['password'];    
    $random_key=$res[0]['random_key'];    
    $res2=$this->db->query("select * from user where username='$username' and email='$email_id' and password='$password'");      
    if(!empty($res2))
    {    
    $res1=$this->db->query("INSERT INTO `user`(`username`,`firstname`,`password`,`email`) VALUES ('$username','$username','$password','$email_id')");      
     $upd=$this->db->query("UPDATE `temp_user` SET `status`='1' WHERE temp_id='$temp_id'")->result_array();      
     
    }
    }
    return;
    }    
    function get_change_user($id)
    {
    return $upd=$this->db->query("SELECT * FROM `temp_forget` where random_key='$id' and status='0'")->result_array();          
    }    
    function bank_per()
    {
    return $upd=$this->db->query("SELECT * FROM `bank_per`")->result_array();              
    }
    
    function select_config_breadcrub($pro_id,$user_id)
    {
    return $upd=$this->db->query("SELECT * FROM `car_configuration` where user_id='$user_id' and product_id='$pro_id'")->result_array();              
    }
    
    function product_comp_color($pro_id)
    {
    $upd=$this->db->query("SELECT distinct(feature_id) FROM `feature_variant_mapping` where variant_id='$pro_id'")->result_array();                  
    $id='';
//    $upd=array_unique($upd);
    foreach($upd as $upd_res)
    {
    $feature_id=$upd_res['feature_id'];    
    $id=$id."'".$feature_id."',";
    }
    $ids=substr($id,0,-1);
    if($ids=='')
    {
    $ids="''";    
    }        
    return $all_res=$this->db->query("SELECT feature_name,feature_desc,feature_img FROM `features` where feature_id IN ($ids) and feature_name='Color'")->result_array();                  
    
//    echo '<br>';        
    }
    
   function product_comp_feature($id)
   {
//   echo '<pre>';       
   
   $upd=$this->db->query("SELECT distinct(accessory_id) FROM `accessory_variant_mapping` where variant_id='$id'")->result_array();                  
   $id='';
   foreach($upd as $upd_res)
    {
    $feature_id=$upd_res['accessory_id'];    
    $id=$id."'".$feature_id."',";
    }
   $ids=substr($id,0,-1);
   return $all_res=$this->db->query("SELECT accessory_id,accessory_cat,accessory_sub_cat,accessory_name FROM `accessory` where accessory_id IN ($ids) order by accessory_sub_cat,accessory_cat ASC")->result_array();                  
//   echo '</pre>';
   }  
    
    
//   function add_emi($emi,$pro_id,$user_id)
   function add_emi($emi,$pro_id,$user_id,$down_payment,$loan1,$bank,$rate1,$months1)
   {   
   $del=$this->db->query("delete from  `emi_calculation` where `pro_id`='$pro_id' and `user_id`='$user_id'");                      
   return $upd=$this->db->query("INSERT INTO `emi_calculation`(`emi_id`, `emi_amount`, `pro_id`, `user_id`, `down_payment`, `load_amaount`, `bank`, `int_rate`, `tenure`, `added_date`) "
           . "VALUES ('','$emi','$pro_id','$user_id','$down_payment','$loan1','$bank','$rate1','$months1','')");                     
   }
   function select_emi($pro_id,$user_id)
   {
   return $del=$this->db->query("SELECT * FROM `emi_calculation` where `pro_id`='$pro_id' and `user_id`='$user_id'")->result_array();                      
   }
   
   function order_record($id)
   {
   return $del=$this->db->query("SELECT * FROM `order` where `order_id`='$id'")->result_array();                      
   }
   
    
   function customer_details($user_id)
   {
   return $del=$this->db->query("SELECT * FROM `user` where `user_id`='$user_id'")->result_array();                      
   }
   function registeration_details($user_id) {
        return $user = $this->db->query("SELECT * FROM `shipping_detail` where user_id='$user_id'")->result_array();
   }
   
   function accessory_configuration_details($id) {
        $id;
        return $res = $this->db->query("select a.accessory_configuration_stock,a.accessory_configuration_id,a.quantity,a.accessory_id,c.* from accessory_configuration_stock a,car_accessory c where a.accessory_configuration_id='$id' and c.car_accessory_id=a.accessory_id")->result_array();
    }
    
    function acc_customer_details($user_id, $config_id) {
        return $user = $this->db->query("SELECT *  FROM `accessory_shipping_detail` where user_id='$user_id' and config_id='$config_id'")->result_array();
    }
   
   

   function getcitydata($state_data)
   {
   echo $state_data;	
   //return $del=$this->db->query("SELECT * FROM `city` where `state_id`='$state_data'")->result_array();
   }
   
   function order_wise_detail($order_id) {
        return $details = $this->db->query("select product_id,city_id,user_id,configuration_id,dis_price,on_road_price,(select name from city c where c.city_id=order.city_id) as city_name from `order` where order_id='$order_id'")->result_array();
    }
   
   function get_feature_results($id)
   {
   return $id;    
   }
   
   function landing_data($data)
   {
       print_r($data);
                exit;
//   return $get_data = $this->db->insert('landing_page',$data);    
   }
   
   
   
   ################ Upcoming Section ##################
   
   function upcoming_configuration_details($id) {
        $id;
        return $res = $this->db->query("select *,(select pro_name_comp from variant where variant_id=upcoming_configuration.variant_id) as product_name,(select pro_image from variant where variant_id=upcoming_configuration.variant_id) as pro_image,(select name from city where city_id=upcoming_configuration.city_id) as city_name  from upcoming_configuration where upcoming_configuration_id='$id'")->row_array();
    }
   
   
    
    
}

