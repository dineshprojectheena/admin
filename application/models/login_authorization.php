<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @param type $email
 * @return type Array
 * @author Dinesh Kumawat
 * @modified_on 11/07/2014
 * @called_from login authorization
 */
class Login_authorization extends CI_Model {
    function facebook_social_auth($data) {
        $activity_data['user_last_logged_in'] = date('Y-m-d H:i:s');
        $activity_data['user_last_ip'] = $this->input->ip_address();
        $activity_data['type'] = 'fb';
        $fb_id=$data['fb_id'];
        $fb_link=$data['fb_link'];
        $email=$data['email'];
        $query =$this->db->query("SELECT user_id,username,firstname,lastname,email,fb_id FROM `user` where email='$email' or fb_id='$fb_id'"); 
         if($query->num_rows() > 0) {
            $res = $query->row_array();
            $track_detail=array(
                'user_id' => $res['user_id'],
                'ip' => $activity_data['user_last_ip'],
                'login_time' => $activity_data['user_last_logged_in'],
                'login_via' => $activity_data['type'],
            );
            $result=$query->result_array();
            $user_id=$result[0]['user_id'];
            $upd =$this->db->query("update `user` set fb_id='$fb_id',fb_link='$fb_link' where user_id='$user_id'");             
            $this->db->insert('user_activity', $track_detail);
            return $res;
        } else {
            $this->db->insert('user', $data);
            $this->db->select('user_id,username,firstname,email,fb_id');
            $this->db->where('fb_id', $data['fb_id']);
            return $res = $this->db->get('user')->row_array();
        }
    }
    
    function linkdin_social_auth($data) {
        $activity_data['user_last_logged_in'] = date('Y-m-d H:i:s');
        $activity_data['user_last_ip'] = $this->input->ip_address();
        $activity_data['type']='ld';
        $linkdin=$data['linkdin'];
        $linkedin_link=$data['linkedin_link'];
        $email_id=$data['email'];
        $query =$this->db->query("SELECT user_id,username,firstname,lastname,email,linkdin FROM `user` where email='$email_id' or linkdin='$linkdin'"); 
        if($query->num_rows()>0){
            $res = $query->row_array();
            $track_detail = array(
                'user_id' => $res['user_id'],
                'ip' => $activity_data['user_last_ip'],
                'login_time' => $activity_data['user_last_logged_in'],
                'login_via' => $activity_data['type']
            );
            $result=$query->result_array();
            $user_id=$result[0]['user_id'];
            $upd =$this->db->query("update `user` set linkdin='$linkdin',linkedin_link='$linkedin_link' where user_id='$user_id'"); 
            $this->db->insert('user_activity', $track_detail);
            return $res;
//          return $ress=$this->db->query("SELECT user_id,username,firstname,lastname,email,linkdin FROM `user` where email='$email_id' or linkdin='$linkdin'"); 
        }else{
            $this->db->insert('user',$data);      
            $insert_id = $this->db->insert_id();
                
            return $query=$this->db->query("SELECT user_id,username,firstname,email,linkdin FROM `user` where email='$email_id' or linkdin='$linkdin'")->row_array(); 
        }
    }

    function join_brand_group($brand_id, $user_id, $status) {
        $count_res=$this->db->query("SELECT * FROM `join_group` where user_id='$user_id' and status='$status' and brand_id='$brand_id'")->num_rows(); 
        if($count_res==0)
        {
        $res=$this->db->query("INSERT INTO `join_group` (`join_id` ,`user_id` ,`status` ,`brand_id`) VALUES ('', '$user_id', '$status', '$brand_id')");
        return $count_res;
        }
        else 
        {
        return $count_res;    
        }    
    }
    function join_group_status($user_id,$brand_id)
    {
    return $count_res=$this->db->query("SELECT * FROM `join_group` where user_id='$user_id' and status='1' and brand_id='$brand_id'")->result_array();     
    
    }
    function brand_wise_user($brand_id)
    {
    return $res=$this->db->query("SELECT u.fb_id,u.user_id FROM `join_group` j,`user` u where j.brand_id='$brand_id' and u.user_id=j.user_id")->result_array();          
    }
    
    function custom_login($signin_email,$signin_password)
    {
    $activity_data['user_last_logged_in'] = date('Y-m-d H:i:s');
    $activity_data['user_last_ip'] = $this->input->ip_address();
    $activity_data['type']='custom';    
    $signin_password=  md5($signin_password);    
    $query =$this->db->query("SELECT user_id,username,firstname,lastname,email FROM `user` where password='$signin_password' and (email='$signin_email' or username='$signin_email')"); 
        if($query->num_rows()>0){
            $res = $query->row_array();
            $track_detail = array(
                'user_id' => $res['user_id'],
                'ip' => $activity_data['user_last_ip'],
                'login_time' => $activity_data['user_last_logged_in'],
                'login_via' => $activity_data['type']
            );
            $this->db->insert('user_activity', $track_detail);
            return $res;
        }    
    }
    function check_email($signin_email)
    {
    return $query =$this->db->query("SELECT user_id FROM `user` where email='$signin_email'")->result_array();     
    }
    function update_unq_forget($forget_email,$uniue_id,$user_id)
    {
    $query =$this->db->query("SELECT user_id FROM `temp_forget` where email_id='$forget_email' and user_id='$user_id' and status='0'")->result_array();         
   
    if(empty($query))
    {
    return $query=$this->db->query("INSERT INTO `temp_forget`(`temp_id`, `user_id`, `email_id`, `random_key`, `added_date`, `status`) "
            . "VALUES ('','$user_id','$forget_email','$uniue_id','','0')");         
    }
    else{
    $querys=$this->db->query("update `temp_forget` set status='1' where user_id='$user_id'");             
    return $query=$this->db->query("INSERT INTO `temp_forget`(`temp_id`, `user_id`, `email_id`, `random_key`, `added_date`, `status`) "
            . "VALUES ('','$user_id','$forget_email','$uniue_id','','0')");         
    }    
    } 
    function update_password($user_id,$new_password,$re_new_password)
    {
    $new_password=md5($new_password);    
    $querys=$this->db->query("update `temp_forget` set status='1' where user_id='$user_id'");             
    return $querys=$this->db->query("update `user` set password='$new_password' where user_id='$user_id'");                 
    }
    
    
    
    
}