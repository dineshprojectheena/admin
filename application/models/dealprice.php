<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dealprice extends CI_Model {

    function deal_upload($data){
        
        $variant_id=$data['variant_id'];
        $city_id=$data['city_id'];
        
        $res1=$this->db->query("select * from deal where variant_id='$variant_id' and city_id='$city_id'");
        echo $cnt=$res1->num_rows();
        
        
        if($cnt==0)
        {        
        $res=$this->db->query("select brand_id from variant where variant_id='$variant_id'")->result_array();
        $brand_id=$res[0];
        $final=array_merge($data,$brand_id);
        $insert=$this->db->insert('deal', $final);
        return $insert;
        }
    }
    
   
    
    function all_brands()
    {
        return $this->db->query('select brand_id,brand_name from brand')->result_array();             
    }

    function get_deal($id) {
        if ($id == '') {
            return $result = $this->db->query('select d.*,v.pro_name,c.name from deal d,city c,variant v where c.city_id=d.city_id and v.variant_id=d.variant_id')->result_array();
        } else if ($id != '') {
            return $result = $this->db->query("select d.*,v.pro_name,c.name from deal d,city c,variant v where c.city_id=d.city_id and v.variant_id=d.variant_id and deal_id='$id'")->result_array();
        }
    }
    
     

    function deal_del($deal_id) {
        $this->db->where('deal_id', $deal_id);
        $get_data = $this->db->delete('deal');
    }
    

    function deal_update($data, $id) {
         $variant_id=$data['variant_id'];
        $res=$this->db->query("select brand_id from variant where variant_id='$variant_id'")->result_array();
        $brand_id=$res[0];
        $final=  array_merge($data,$brand_id);
        $this->db->where('deal_id',$id);
        $get_data = $this->db->update('deal',$data);
    }
    
    
    
    function brandeal_upload($data){
     $insert = $this->db->insert('brand_deal', $data);
        return $insert;
    }
    
    function get_branddeal($id) {
        if ($id == '') {
            return $result = $this->db->query('select d.*,c.name,b.brand_name from brand_deal d,city c,brand b where c.city_id=d.city_id and d.brand_id=b.brand_id')->result_array();
        } else if ($id != '') {
            return $result = $this->db->query("select d.*,v.pro_name,c.name from brand_deal d,city c,variant v where c.city_id=d.city_id and v.variant_id=d.variant_id and deal_id='$id'")->result_array();
        }
    }
    
    function brandeal_del($deal_id){
        $this->db->where('deal_id', $deal_id);
        return $get_data = $this->db->delete('brand_deal');
    }
    
    

}
