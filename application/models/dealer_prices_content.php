<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dealer_prices_content extends CI_Model{
    function dealer_prices_data()
    {
    return $get_data=$this->db->query("SELECT * FROM `dealer`")->result_array();         
    }  
    function dealer_uploads_count()
    {
    $get_data=$this->db->query("SELECT dealer_id,dealer_name,email FROM `dealer` where status='1'")->result_array();         
//    print_r($get_data);
    $final=array();
    foreach($get_data as $get_data_res)
    {
    $dealer_id=$get_data_res['dealer_id'];    
    $get_count=$this->db->query("SELECT count(dealer_inventory_id) as count,added_date from dealer_inventory where dealer_id='$dealer_id' order by added_date DESC")->row_array();         
    $res=  array_merge($get_data_res, $get_count);
    $final[]=$res;
    }
    return ($final);
    
    }  
    
    function dealer_hot_deals_count()
    {
    $get_data=$this->db->query("SELECT dealer_id,dealer_name,email FROM `dealer` where status='1'")->result_array();         
    $final=array();
    foreach($get_data as $get_data_res)
    {
    $dealer_id=$get_data_res['dealer_id'];    
    $get_count=$this->db->query("SELECT count(hot_deal_id) as count,added_date from dealer_hot_deals where dealer_id='$dealer_id' order by added_date DESC")->row_array();         
    $res=  array_merge($get_data_res, $get_count);
    $final[]=$res;
    }
    return ($final);
    }
    
    
}
