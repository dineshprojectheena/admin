<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Model {
    function select_user_details($id)
    {    
        return $this->db->query("SELECT user_id,username,firstname,lastname,email,gender,dob,location,fb_id,telephone,address FROM `user` where username='$id'")->result_array();        
    }
    
    function update_details($data)
    {
        $this->db->where('user_id',$_POST['id']);
        $insert=$this->db->update('user',$data);
        return $insert;   
    }
    
    function check_username($username)
    {
    $data=$this->db->query("SELECT user_id FROM `user` where username='$username'");
    return $data->num_rows();
    }
    
    function select_car_confiuration($user_id)
    {
    return $data=$this->db->query("SELECT c.*,v.pro_name,v.pro_type,v.pro_image,v.pro_detail FROM `car_configuration` c,`variant` v where c.user_id='$user_id' and v.variant_id=c.product_id and c.status!='2'")->result_array();
    
//    foreach($data as $data_res)
//    {
//    print_r($data_res);    
//    }    
    }
    function select_brand_user($user_id)
    {
    return $data=$this->db->query("SELECT g.brand_id,b.brand_name,b.brand_image FROM `join_group` g,`brand` b where g.user_id='$user_id' and b.brand_id=g.brand_id")->result_array();
    }
    
    function select_book_car($user_id)
    {
    return $data=$this->db->query("SELECT o.order_id,o.product_id,o.invoice_no,v.pro_name,v.pro_detail,v.pro_image FROM `order` o,`variant` v where o.user_id='$user_id' and o.product_id=v.variant_id")->result_array();
    }
}
