<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends CI_Model {

    function dashborad_reports() {
//        echo $data='SELECT o.*,u.firstname,v.pro_name,v.variant FROM `order` o,user u,variant v where o.user_id=u.user_id and variant_id=product_id ORDER BY `order_id` DESC';
        $data=$this->db->query('SELECT o.*,u.firstname,u.email,v.pro_name_comp,v.variant,(select code from affiliate where affiliate_id=o.ref_id) as code FROM `order` o,user u,variant v where o.user_id=u.user_id and variant_id=product_id ORDER BY `order_id` DESC ')->result_array();
//        print_r($data);
        return $data;
    }

    function order_reports($id, $i,$added_by='customer') {
        if($i == 1) {
            return $this->db->query("SELECT o.*,u.firstname,u.email,u.lastname,u.telephone,u.address,v.pro_name,v.variant FROM `order` o,user u,variant v where o.user_id=u.user_id and variant_id=product_id and order_id='$id'")->result_array();
        }
        else if($i == 2) {
            return $this->db->query("SELECT v.pro_name,v.variant,pro_image FROM `order` o,variant v where o.product_id=v.variant_id and o.order_id='$id'")->result_array();
        }
        else if($i==3){            
            return $this->db->query("SELECT o.*,s.* FROM `order_confirmation` o,`order_status` s where o.order_id='$id' and s.order_status_id=o.order_status_id and o.added_by='$added_by'")->result_array();            
        }      
    }
    function order_status_update($status,$order_id,$notify,$comments,$dealer_id,$added_by)
    {
    if($added_by=='dealer')
    {
    $date=date('Y-m-d h:i:s');    
    return $this->db->query("INSERT INTO `order_confirmation` VALUES ('','$order_id','$status','$comments','$notify','$date','$added_by','$dealer_id')");        
    }
    else 
    {    
    $date=date('Y-m-d h:i:s');    
    return $this->db->query("INSERT INTO `order_confirmation` VALUES ('','$order_id','$status','$comments','$notify','$date','$added_by','')");    
    }
    }
    function order_email($id)
    {
    $email=$this->db->query("select user_id from `order` where order_id='$id'")->result_array();
    $user_id=$email[0]['user_id'];
    return $email_id=$this->db->query("select email from `user` where user_id='$user_id'")->result_array();        
    }
    
    function user_details($order_id)
    {
    
    return $order_status=$this->db->query("select o.user_id,o.invoice_no,(select u.firstname from user u where u.user_id=o.user_id) as firstname,(select u.lastname from user u where u.user_id=o.user_id) as lastname from `order` o where o.order_id='$order_id'")->result_array();
    
    }    
    function order_status()
    {
    return $order_status=$this->db->query("select * from `order_status`")->result_array();
    }
    
    function get_dealer($query)
    {
    $this->db->select('dealer_name as name,dealer_id as id');
        $this->db->like('dealer_name', $query, 'both');
        $this->db->limit(5);
        $query = $this->db->get('dealer');
        return $query->result_array();
    }
    
    function dealer_email($order_id)
    {
    return $email_id=$this->db->query("select email from `dealer` where dealer_id='$order_id'")->result_array();        
    }
    
    
    

}
