<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Masters extends CI_Model {
    /* Localization process start here */

    function get_city() {
        $get_data = $this->db->get('city');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $get;
    }
    
     function document_status($dealer_id){
        return $this->db->query("select * from `dealer_document_status` where dealer_id='$dealer_id'")->result_array();
    }

    function get_state() {
        $get_data = $this->db->get('state');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data2[] = $row;
            }
            return $data2;
        }
        return $get2;
    }

    function get_country() {
        $get_data = $this->db->get('country');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data3[] = $row;
            }
            return $data3;
        }
        return $get3;
    }

    /* Localization process start here */





    /* Banner process start here */

    function banner_upload_data() {
        $banner_name = $_POST['banner_name'];
        $status = $_POST['status'];
        $filename = $_FILES['accessory_pic']['name'];
        $title = $_POST['title'];
        $link = $_POST['link'];
        $banner_desc = $_POST["banner_desc"];
        $new_banner_data = array(
            'name' => $banner_name,
            'status' => $status,
            'image' => $filename,
            'title' => $title,
            'description' => $link,
            'link' => $banner_desc
        );
        $insert = $this->db->insert('banner', $new_banner_data);
        return $insert;
    }

    function get_banner($banner_id) {
        $this->db->where('banner_id', $banner_id);
        $get_data = $this->db->get('banner');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function uploaded_banner_data() {
        $get_data = $this->db->get('banner');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function del_banner($del_id) {
        $this->db->where('banner_id', $del_id);
        $this->db->delete('banner');
        return;
    }

    function update_data() {
        $banner_name = $_POST['banner_name'];
        $banner_id = $_POST['banner_id'];
        $status = $_POST['status'];
        $filename = $_FILES['accessory_pic']['name'];
        $title = $_POST['title'];
        $link = $_POST['link'];
        $banner_desc = $_POST["banner_desc"];
        $new_banner_data = array(
            'name' => $banner_name,
            'status' => $status,
            'image' => $filename,
            'title' => $title,
            'description' => $link,
            'link' => $banner_desc
        );
        $this->db->where('banner_id', $banner_id);
        $this->db->update('banner', $new_banner_data);
        return;
    }

    /* Banner process end here */


    /* Faq process start here */

    function faq_insert_data() {
        $added_date = date('y-m-d H:i:s');
        $data = array(
            'main_heading' => $_POST['main_heading'],
            'descrtiption' => $_POST['banner_desc'],
            'type' => $_POST['type'],
            'added_date' => $added_date,
            'status' => $_POST['status']
        );
        $insert = $this->db->insert('faq', $data);
        return $insert;
    }

    function get_faq($faq_id) {
        $this->db->where('faq_id', $faq_id);
        $get_data = $this->db->get('faq');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function faq_data_res() {
        $get_data = $this->db->get('faq');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function del_faq($del_id) {
        $this->db->where('faq_id', $del_id);
        $this->db->delete('faq');
        return;
    }

    function all_car_type() {
        return $this->db->query('select * from `pro_type`')->result_array();
    }

    function update_faq() {
        $added_date = date('y-m-d H:i:s');
        $faq_id = $_POST['faq_id'];
        $data = array(
            'main_heading' => $_POST['main_heading'],
            'descrtiption' => $_POST['banner_desc'],
            'type' => $_POST['type'],
            'updated_date' => $added_date,
            'status' => $_POST['status']
        );
        $this->db->where('faq_id', $faq_id);
        $this->db->update('faq', $data);
        return;
    }

    /* Faq process end here */





    /* News process start here */

    function news_insert_data($data) {
        $added_date = date('y-m-d H:i:s');

        $insert = $this->db->insert('news', $data);
        return $insert;
    }

    function news_data_res() {
        $get_data = $this->db->get('news');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function del_news($del_id) {
        $this->db->where('latest_news_id', $del_id);
        $this->db->delete('news');
        return;
    }

    function sear_news_data_res() {
        $upd_id = $_POST['upd_id'];
        $this->db->where('latest_news_id', $upd_id);
        $get_data = $this->db->get('news');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function news_update_data($data) {
        $added_date = date('y-m-d H:i:s');


        $this->db->where('latest_news_id', $_POST['new_id']);
        $insert = $this->db->update('news', $data);
        return $insert;
    }

    /* News process end here */



    /* Brand Group process start here */

    function brand_group_data_res() {
        $get_data = $this->db->get('brand_group');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function brand_group_insert() {
        $added_date = date('y-m-d H:i:s');
        $data = array(
            'group_name' => $_POST['group_name'],
            'group_size' => $_POST['group_size'],
            'status' => $_POST['status'],
            'added_date' => $added_date
        );
        $insert = $this->db->insert('brand_group', $data);
        return $insert;
    }

    function del_brand_group($brand_group_id) {
        $this->db->where('brand_group_id', $brand_group_id);
        $this->db->delete('brand_group');
        return;
    }

    function get_brand_group($brandgrp_id) {
        $this->db->where('brand_group_id', $brandgrp_id);
        $get_data = $this->db->get('brand_group');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function brand_group_update() {
        $brandgrp_id = $_POST['group_id'];
        $added_date = date('y-m-d H:i:s');
        $data = array(
            'group_name' => $_POST['group_name'],
            'group_size' => $_POST['group_size'],
            'status' => $_POST['status'],
            'updated_date' => $added_date
        );
        $this->db->where('brand_group_id', $brandgrp_id);
        $insert = $this->db->update('brand_group', $data);
        return $insert;
    }

    function get_brand_group_res() {
        $get_data = $this->db->get('brand_group');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    /* Brand Group process End here */





    /* Brand process start here */

    function brand_insert($data, $brand_name) {
        $get = $this->db->query("SELECT brand_id FROM `brand` where brand_name='$brand_name'");
        if ($get->num_rows() == 0) {
            $insert = $this->db->insert('brand', $data);
            return $insert;
        }
        return;
    }

    function brand_res() {
        $this->db->select('*');
        $this->db->from('brand');
//        $this->db->join('brand_group', 'brand_group.brand_group_id = brand.brand_group_id');
        $get_data = $this->db->get();
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function del_brand($brand_group_id) {
        $this->db->where('brand_id', $brand_group_id);
        $this->db->delete('brand');
        return;
    }

    function get_brand($brandgrp_id) {
        $this->db->where('brand_id', $brandgrp_id);
        $get_data = $this->db->get('brand');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function brand_update($data, $ids, $brand_name) {
        $get = $this->db->query("SELECT brand_id FROM `brand` where `brand_id`='$ids'");
        if ($get->num_rows() == 1) {
            $this->db->where('brand_id', $ids);
            return $update = $this->db->update('brand', $data);
        }
        return;
    }

    /* Brand process End here */




    /* Information Pages process End here */

    function info_upload() {
        $added_date = date('y-m-d H:i:s');
        $data = array(
            'title' => $_POST['info_title'],
            'description' => addslashes($_POST['details']),
            'sort_order' => $_POST['sort_order'],
            'status' => $_POST['status'],
            'cust_title' => $_POST['title'],
            'meta_description' => addslashes($_POST['description']),
            'meta_keyword' => addslashes($_POST['keyword']),
            'added_date' => $added_date
        );
        $insert = $this->db->insert('information', $data);
        return $insert;
    }

    function info_data() {
        $get_data = $this->db->get('information');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function del_info() {
        $information_id = $this->input->post('information_id');
        $this->db->where('information_id', $information_id);
        $this->db->delete('information');
        return;
    }

    function get_info() {
        $information_id = $this->input->post('information_id');
        $this->db->where('information_id', $information_id);
        $get_data = $this->db->get('information');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function info_update() {
        $added_date = date('y-m-d H:i:s');
        $information_id = $_POST['information_id'];
        $data = array(
            'title' => $_POST['info_title'],
            'description' => addslashes($_POST['details']),
            'sort_order' => $_POST['sort_order'],
            'status' => $_POST['status'],
            'cust_title' => $_POST['title'],
            'meta_description' => addslashes($_POST['description']),
            'meta_keyword' => addslashes($_POST['keyword']),
            'updated_date' => $added_date
        );
        $this->db->where('information_id', $information_id);
        $insert = $this->db->update('information', $data);
        return $insert;
    }

    /* Information Pages process End here */



    /* Dealer Master process End here */
    
    
    function dealer_join_us($join_us)
    {
        return $select_user = $this->db->query("select * from `dealer_join_us` where `dealer_join_us_id`='$join_us'")->row_array();
    }

    function dealer_profile_upload($image_data){
        $dealer_id = $image_data['dealer_id'];
        $select_user = $this->db->query("select dealer_id from `dealer_files` where `dealer_id`='$dealer_id'")->num_rows();
        if ($select_user == 0) {
            $insert = $this->db->insert('dealer_files', $image_data);
            return $insert_id = $this->db->insert_id();
        }

//        else
//        {    
//        $insert = $this->db->insert('dealer_files', $image_data);    
//        }
    }

    function dealer_insert($data, $data2, $brands) {

        $username = $data['username'];
        $added_date = $data['added_date'];
        $email = $data['email'];
        $status = $data['status'];
        $password = $data['user_password'];
        $contact_no = $data['contact_no'];
        $user_password = md5(md5($data['user_password']));
        $select_user=$this->db->query("select dealer_username from dealer_login where dealer_username='$username'")->num_rows();
        if($select_user == 0){
            $insert = $this->db->insert('dealer',$data);
            $insert_id = $this->db->insert_id();            
            $others_brand = explode(",", $data['others_brand']);
            foreach ($others_brand as $others_brand_res) {
                $dealer_brand_insert = $this->db->query("INSERT INTO `dealer_brand`(`dealer_brand_id`, `dealer_id`, `brand_id`, `added_date`) "
                        . "VALUES ('','$insert_id','$others_brand_res','$added_date')");
            }
            if($username != '') {
                $dealer_login_insert = $this->db->query("INSERT INTO `dealer_login`(`dealer_login_id`, `dealer_id`,"
                        . " `dealer_username`, `dealer_email`, `dealer_password`, `dealer_shown_password`,"
                        . " `first_name`, `last_name`, `dob`, `phoneno`, `status`, `added_date`)"
                        . " VALUES ('','$insert_id','$username','$email','$user_password','$password','','','','$contact_no','$status','$added_date')");
            }            
        }
        return $insert_id;
    }

    function dealer_update($data, $image_data) {
        $status = $data['status'];
        $username = $data['username'];
        $dealer_id = $data['dealer_login_id'];
        $added_date = $data['added_date'];
        $email = $data['email'];
        $password = $data['user_password'];
        $contact_no = $data['contact_no'];
        $user_password = md5(md5($data['user_password']));


        $select_user = $this->db->query("select dealer_id from dealer where dealer_id='$dealer_id'")->num_rows();
        if ($select_user == 1) {
            $this->db->where('dealer_id', $dealer_id);
            $dealer_update=$this->db->update('dealer', $data);
            $others_brand = explode(",", $data['others_brand']);
            $del_brand = $this->db->query("delete from dealer_brand where dealer_id='$dealer_id'");
            foreach ($others_brand as $others_brand_res) {
                $dealer_brand_insert = $this->db->query("INSERT INTO `dealer_brand`(`dealer_brand_id`, `dealer_id`, `brand_id`, `added_date`) "
                        . "VALUES ('','$dealer_id','$others_brand_res','$added_date')");
            }
            if ($username != '') {
                $dealer_login_insert = $this->db->query("UPDATE `dealer_login` SET "
                        . "`dealer_username`='$username',`dealer_email`='$email',`dealer_password`='$user_password',`dealer_shown_password`='$password',"
                        . "`first_name`='',`last_name`='',`dob`='',"
                        . "`phoneno`='$contact_no',`status`='$status',`added_date`='$added_date' WHERE dealer_id='$dealer_id'");
            }
            if (!empty($image_data)) {
                $this->db->where('dealer_id', $dealer_id);
                $dealer_update=$this->db->update('dealer_files', $image_data);
            }
            return;
        }
    }

    function get_dealer() {
        return $get_data = $this->db->query("SELECT d.dealer_name,d.status,d.contact_no,d.dealer_id,(select c.name from city c where c.city_id=d.city) as city,(select b.brand_name from brand b where b.brand_id=d.primary_brand) as brand_name FROM `dealer` d")->result_array();
//        echo '<pre>';
//        print_r($get_data);
//        echo '</pre>';
//        exit;
        $get_data = $this->db->get('dealer');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data4[] = $row;
            }
//            print_r($data4);
            return $data4;
        }
    }

    function get_edit_dealer($dealer_id, $id) {
        if ($id == 1) {
            $get_data = $this->db->query("SELECT d.* FROM `dealer` d WHERE `dealer_id`='$dealer_id'")->row_array();
//        if($get_data->num_rows()>0) {
//        foreach ($get_data->result() as $row) {
//        $data4[] = $row;
//        }
            return $get_data;
//        }         
        }
        if ($id == 2) {
//        echo "SELECT * FROM `dealer_login` WHERE `dealer_id`='$dealer_id'";    
            return $get_data = $this->db->query("SELECT * FROM `dealer_login` WHERE `dealer_id`='$dealer_id'")->row_array();
        }
        if ($id == 3) {
//        echo "SELECT * FROM `dealer_login` WHERE `dealer_id`='$dealer_id'";    
            return $get_data = $this->db->query("SELECT * FROM `dealer_files` WHERE `dealer_id`='$dealer_id'")->row_array();
        }
    }

    function delete_dealer() {
        $dealer_id = $this->input->post('dealer_id');
        $this->db->where('dealer_id', $dealer_id);
        $this->db->delete('dealer');


        $this->db->where('dealer_id', $dealer_id);
        $this->db->delete('dealer_login');


        $this->db->where('dealer_id', $dealer_id);
        $this->db->delete('dealer_brand');

        $this->db->where('dealer_id', $dealer_id);
        $this->db->delete('dealer_files');



        return;
    }

    function get_brand_wise_model($brand_id) {
        return $get_data = $this->db->query("SELECT * FROM `model` where `brand_id`='$brand_id'")->result_array();
    }

    /* Information Pages process End here */

    function ext_brand($dealer_id) {
        return $get_data = $this->db->query("SELECT b.brand_id,b.brand_name FROM `dealer_brand` d,`brand` b where b.brand_id=d.brand_id and dealer_id='$dealer_id'")->result_array();
    }

    function get_model_wise_variant($model_id, $brand_id) {
        return $get_data = $this->db->query("SELECT variant_id,pro_name,pro_name_comp FROM `variant` where brand_id='$brand_id' and model_id='$model_id' and status='1'")->result_array();
    }

}
