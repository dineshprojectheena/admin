<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Modules extends CI_Model {
    
    function get_city() {        
        $get_data = $this->db->get('city');
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $get;
    }
    
    
    ########################## accessory seller start  ##########################
    function acc_sell_upload($data) {
        
        $insert = $this->db->insert('accessory_seller', $data);
        return $insert;
    }
    
    
    function get_accessory_seller($id) 
    {
        if($id!='')
        {
        $get_data=$this->db->query("select a.*,(select brand_name from brand where brand_id=a.accessory_brand) as brand_name from accessory_seller a where a.accessory_seller_id='$id'");
//        $get_data =$this->db->where('accessory_seller_id', $id); 
//        $get_data = $this->db->get('accessory_seller');    
        }
        else 
        {
        $get_data = $this->db->get('accessory_seller');
        }
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
	    //print_r($row->accessory_brand);
	    //exit;		
                $data[] = $row;
            }
            return $data;
        }
        return $get;
    }
    
    function acc_sell_del($id) {        
         $this->db->where('accessory_seller_id', $id);
         $get_data = $this->db->delete('accessory_seller');        
    }
    
    
    function acc_sell_update($data,$id) {        
         $this->db->where('accessory_seller_id', $id);
         $get_data = $this->db->update('accessory_seller',$data);   
    
         
    }
    
    
    
    
   ########################## accessory seller start  ##########################
     function ser_sell_upload($data) {
        
        $insert = $this->db->insert('service_seller', $data);
        return $insert;
    }
    function get_service_seller($id) 
    {
        if($id!='')
        {
        $this->db->where('service_seller_id', $id); 
        $get_data = $this->db->get('service_seller');    
        }
        else 
        {
        $get_data = $this->db->get('service_seller');
        }
        
        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $get;
    }
    
    function ser_sell_del($id) {        
         $this->db->where('service_seller_id', $id);
         $get_data = $this->db->delete('service_seller');        
    }
    
    function ser_sell_update($data,$id) {        
         $this->db->where('service_seller_id', $id);
         $get_data = $this->db->update('service_seller',$data);        
    
         
    }
   ########################## accessory seller end  ##########################
    
    
     ########################## model start  ##########################
     function get_brand()
    {
        $this->db->select('brand_id, brand_name');
        $this->db->where('status','1');
        return $get_data = $this->db->get('brand')->result_array(); 
    }
    
     function get_model($id)
    {
        if($id=='')
        {
        return $get_data= $this->db->get('model')->result_array();                 
        }
        else {
        $this->db->where('model_id', $id);    
        return $get_data= $this->db->get('model')->result_array();
        }
    }
    
     function model_upload($data)
    {
         $brand_id=$data['brand_id'];
         $model_name=htmlspecialchars($data['model_name'],ENT_QUOTES);
         
         $res=$this->db->query("select * from model where model_name='$model_name' and brand_id='$brand_id'")->num_rows();
         if($res==0)
         {
         $get_data = $this->db->insert('model',$data);             
         }    

        return $get_data ;
    }
    
    function model_del($id){        
         $upd=$this->db->query("UPDATE `model` SET `status` = '2' WHERE `model_id` = BINARY '$id'");
         $get=$this->db->query("select * from model where `model_id` = BINARY '$id'")->row_array();
         
         $brand_id=$get['brand_id'];
         $model_id=$get['model_id'];
         $variant=$this->db->query("SELECT variant_id FROM `variant` where `model_id`='$model_id' and brand_id='$brand_id'")->result_array();
//         echo '<pre>';
         foreach($variant as $variant_data)
         {
         $variant_id=$variant_data['variant_id'];        
         $upd_variant=$this->db->query("UPDATE `variant` SET `status` = '2' WHERE `variant_id` =  '$variant_id'");             
//         echo "UPDATE `variant` SET `status` = '2' WHERE `variant` =  '$variant_id'";             
         }  
         return;
//         echo '</pre>';
//         $get_data=$this->db->delete('model');        
    }
    
    function model_updates($data,$id) {        
         $this->db->where('model_id', $id);
         $get_data = $this->db->update('model',$data);
         return $get_data ;
    }
    
    
    ########################## accessory seller end  ##########################
    
    
     ########################## model start  ##########################
    
     function color_upload($data)
    {
        $get_data=$this->db->insert('body_color',$data);         
        return $get_data ;
    }
    
     function get_color($id)
    {
        if($id=='')
        {
        return $get_data= $this->db->get('body_color')->result_array();                 
        }
        else {
        $this->db->where('body_color_id', $id);    
        return $get_data= $this->db->get('body_color')->result_array();
        }        
//        if($get_data->num_rows() > 0) {
//            foreach($get_data->result() as $row) {
//                $data[] = $row;
//            }
//            return $data;
//        }
//        return $get;
    }
    
    function body_color_del($id) {        
         $this->db->where('body_color_id', $id);
         $get_data = $this->db->delete('body_color');        
    }
    
    function color_update($data,$id) {        
         $this->db->where('body_color_id', $id);
         $get_data = $this->db->update('body_color',$data);
         return $get_data ;
    }   
    
    ########################## accessory seller end  ##########################
    
     ########################## wheel start  ##########################
    
     function wheel_upload($data)
    {
        $get_data=$this->db->insert('wheel',$data);         
        return $get_data ;
        
    }
    
    
     function get_wheel($id)
    {
        if($id=='')
        {
        return $get_data=$this->db->get('wheel')->result_array();                 
        }
        else {
        $this->db->where('wheel_id', $id);    
        return $get_data= $this->db->get('wheel')->result_array();
        }        
//        if($get_data->num_rows() > 0) {
//            foreach($get_data->result() as $row) {
//                $data[] = $row;
//            }
//            return $data;
//        }
//        return $data;
    }
    
    function wheel_del($id) {        
         $this->db->where('wheel_id', $id);
         $get_data = $this->db->delete('wheel');        
    }
    
    function wheel_update($data,$id) {        
         $this->db->where('wheel_id', $id);
         $get_data = $this->db->update('wheel',$data);
         return $get_data ;
    }   
    
    ########################## accessory seller end  ##########################
    
    
    
}
