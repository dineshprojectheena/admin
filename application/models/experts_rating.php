<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Experts_rating extends CI_Model {    
    
    function rating_upload($rec_data) { 
        
        $insert = $this->db->insert('expert_rating', $rec_data);
        return $insert;
    }
    function show_rating($id){ 
        return $get_data = $this->db->query("select e.*,v.brand_id,v.variant_id,v.model_id,v.pro_name from expert_rating e,variant v where e.expert_rating_id='$id' and e.pro_id=v.variant_id")->row_array();
    }    
    
    function show_rating_data($pro_id)
    {
    return $this->db->query("SELECT v.`variant_id` , v.`pro_name` , b.`brand_id` , b.`brand_name` , m.`model_name` , m.`model_id` FROM `variant` v, `brand` b, `model` m WHERE v.variant_id = '$pro_id' and v.`brand_id`=b.`brand_id` and v.`model_id`=m.`model_id`")->result_array();       
    }
    
    
    function rating_del($id)
    { 
     $this->db->where('expert_rating_id',$id);
        $this->db->delete('expert_rating');
        return;           
     
    }
    function rating_update($rec_data,$id)
    { 
     $this->db->where('expert_rating_id',$id);
     $this->db->update('expert_rating',$rec_data);
     return;           
     
    }
}    