<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Common extends CI_Model {
function get_product_detail($query)
{
$this->db->select('pro_name_comp as name,variant_id as id');
$this->db->like('pro_name_comp', $query, 'both');
$this->db->limit(5);
$query = $this->db->get('variant');
return $query->result_array();
}

function get_brand_detail($q)
{
$this->db->select('brand_name as name,brand_id as id');
$this->db->like('brand_name', $q, 'both');
$this->db->limit(5);
$query = $this->db->get('brand');
return $query->result_array();
}

function get_city() {
return $get = $this->db->get('city')->result_array();
}

function get_accessory_category() {
return $get = $this->db->query('select * from accessory_category')->result_array();
}

function get_accessory_sub_category() {
return $get = $this->db->query('select * from accessory_sub_category')->result_array();
}

function get_accessory_type() {
return $get = $this->db->query('select * from accessory_type')->result_array();
}

function get_feature_variant($variant_id){
$this->db->select('variant_id as id, pro_name as name');
$this->db->where('variant_id', $variant_id);
return $this->db->get('variant')->row_array();
}
function admin_authentication($username, $password, $cond,$id)
{
if($cond==1)
{
$username;
$passwords = MD5(MD5($password));
return $this->db->query("select login_id,username from admin_login where username='$username' and password='$passwords' and status='1'")->row_array();
}
else
{
$activity_data['user_last_logged_in'] = date('Y-m-d H:i:s');
$activity_data['user_last_ip'] = $this->input->ip_address();
$activity_data['type'] = 'admin';
$track_detail = array(
'user_id' => $id,
 'ip' => $activity_data['user_last_ip'],
 'login_time' => $activity_data['user_last_logged_in'],
 'login_via' => $activity_data['type']
);
$this->db->insert('user_activity', $track_detail);
return;
}
}


}
