<?php

class Mosf extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

//
    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('login_authorization');
        $this->load->model('deal_data_section');
        $this->load->model('mosf_data_section');
        $this->output->nocache();
    }

    function mynewcar_online_shopping_festival() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');
        ####Get City Id ####
        $date['city'] = $this->common_city();
        ####Get All City's data ####
        $data['all_Citys'] = $this->common_section->all_citys_detalis();
        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }
        $data['userdata'] = $this->session->userdata('logged_in_user');
//        $data['hot_deals_data'] = $this->mosf_data_section->mosf_deal_data2();

        $data['all_brands'] = $this->common_section->get_brands($id = '');
        $data['all_car_type'] = $this->mosf_data_section->get_car_type();
        $data['content'] = 'mosf';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '2');

        echo $this->load->view('index', $data);
    }

    function mosf_data() {
        $count = $_POST['count'];
        $hot_deals_data = $this->mosf_data_section->mosf_deal_data2($count);
//        echo '<pre>';
//        print_r($hot_deals_data);
//        echo '</pre>';

        foreach ($hot_deals_data as $c => $key) {
            $sort_mnc_exshowroom_price[] = $key['mnc_exshowroom_price'];
        }
        if ($_GET['sort'] == 'DESC') {
            array_multisort($sort_mnc_exshowroom_price, SORT_DESC, $hot_deals_data);
        } else {
            array_multisort($sort_mnc_exshowroom_price, SORT_ASC, $hot_deals_data);
        }
        $ii = 1;
        $data['userdata'] = $this->session->userdata("logged_in_user");
        $user_id = $data['userdata']['user_id'];
        if ($user_id == '') {
            $user_id = '0';
        }
        if (!empty($hot_deals_data)) {
            $cnt = 0;
            foreach ($hot_deals_data as $hot_deals_data_res) {
                if ($this->input->cookie('main_city', TRUE) == $hot_deals_data_res['city_id']) {
                    $total_saving = $this->home_feature->get_total_saving($hot_deals_data_res['variant_id'], $user_id);
                    ?>
                    <?php
                    if (indianFormatNumber($hot_deals_data_res['exshowroom_price']) > 0) {
                        if ($ii <= $count) {
                            ?>
                            <div class="col-md-4 col-xs-12 col-cust-both" style="background-color:white;">
                                <!--<div class="container-fluid">-->
                                <!--<div class="row">-->
                                <div class="ribbin">	
                                    <div class="corner">
                                        <h2><?php echo $hot_deals_data_res['ribbon_text']; ?></h2>
                                        <div class="group-item">
                                            <div class="col-md-12 col-cust-both" align="center">
                                                <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $hot_deals_data_res['pro_name_comp']) . '-' . $hot_deals_data_res['variant_id']) ?>" >        
                                                    <img alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $hot_deals_data_res['pro_image'])); ?>" src="<?php echo img_url('uploads/product/' . $hot_deals_data_res['pro_image']); ?>" class="img-responsive">
                                                </a>                                                                        
                                            </div>    
                                            <div class="group-content " style="text-align:left;">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-12" >
                                                            <h4 class="hot_dealreview_title"> <?php echo $hot_deals_data_res['pro_name_comp'] ?></h4>
                                                        </div> 
                                                        <div class="col-md-12">
                                                            <div class="group-price">
                                                                <div class="mnc-price">
                                                                    <span class="price-span">
                                                                        Ex-Showroom Price
                                                                    </span>
                                                                    <?php
                                                                    if (indianFormatNumber($hot_deals_data_res['exshowroom_price']) > 0) {
                                                                        ?>
                                                                        <span class="text-maroon">
                                                                            <i class="fa fa-rupee"></i>
                                                                            <?php
                                                                            if (indianFormatNumber($hot_deals_data_res['mnc_exshowroom_price']) == 0) {
                                                                                echo '0';
                                                                            } else {
                                                                                echo indianFormatNumber($hot_deals_data_res['mnc_exshowroom_price']);
                                                                            }
                                                                            ?>   
                                                                        </span>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <span class="text-maroon">

                                                                            <?php
                                                                            echo Not_applicable;
                                                                            ?>    
                                                                        </span>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>     
                                                            </div>
                                                        </div> 
                                                        
                                                        <div class="col-md-12">
                                                            <span class="price-span" style="font-weight:bold;">Total Benefits Upto</span>
                                                            <span class="price-span text-maroon"><i class="fa fa-rupee"></i>
                                                                <?php
                                                                if (indianFormatNumber($hot_deals_data_res['total_mosf_benefits']) == 0) {
                                                                    echo '0';
                                                                } else {
                                                                    echo indianFormatNumber($hot_deals_data_res['total_mosf_benefits']);
                                                                }
                                                                ?> 
                                                            </span>
                                                        </div>  
                                                        <div class="col-md-12">
                                                            <span class="price-span">Valid Until </span>
                                                            <span class="price-span text-maroon">
                                                                <?php
                                                                if (!empty($hot_deals_data_res['valid_until'])) {
                                                                    echo '- ' . $hot_deals_data_res['valid_until'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?> 
                                                            </span>
                                                        </div>  
                                                        <div class="col-md-12">
                                                            <?php
                                                            if (!empty($hot_deals_data_res['deal'])) {
                                                                ?>
                                                                <span class="price-span">Offer:-  </span>
                                                                <span class="price-span text-maroon">
                                                                <?php
                                                                echo $hot_deals_data_res['deal'];
                                                                ?>
                                                                </span>    
                                                                <?php
                                                                }
                                                                ?> 
                                                            
                                                        </div>  
                                                        <div class="col-md-12">
<!--                                                            <div class="pull-left">                                                                    
                                                                <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carinsurance_hotdeals.png' ?>" data-toggle="tooltip" class="main_tooltip"  data-placement="right" title="Car Insurance Offers"/>
                                                                <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carfinance_hotdeals.png' ?>"  data-toggle="tooltip" class="main_tooltip"  data-placement="right" title="Car Finance Offers"/>			
                                                                <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carexchange.png' ?>" data-toggle="tooltip" class="main_tooltip"  data-placement="right" title="Car Exchange  Offers"/>
                                                                <?php
                                                                if (!empty($hot_deals_data_res['accessory_package'])) {
                                                                    ?>
                                                                    <img src="<?php echo base_url() . 'assets/img/product/otherbenefits.png' ?>" data-toggle="tooltip" class="main_tooltip"  data-placement="top" title="<?php echo $hot_deals_data_res['accessory_package']; ?>"/>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>-->
                                                            <div align="right">
                                                                <a href="<?php echo site_url('car/' . preg_replace('/\s+/', '-', $hot_deals_data_res['pro_name_comp']) . '-' . $hot_deals_data_res['variant_id']) ?>" class="btn btn-xs carsuel_book" >Book Now</a>
                                                            </div>
                                                        </div>

                                                    </div>      
                                                </div>
                                            </div>
                                        </div>
                                    </div>      
                                </div>      
                                <!--</div>--> 
                                <!--</div>-->
                                <!--</div>-->
                            </div>

                            <?php
                            $ii++;
                        }
                    }

                    $cnt++;
                }
            }
        } else {
            ?>
            <div align="center" class="no_carousel">
                <img src="<?php echo base_url(); ?>/assets/img/ComingSoon.png">
            </div>
            <?php
        }
    }

}
