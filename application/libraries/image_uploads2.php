<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This Library is created by Jigar  which will handle all the
 * Image Uploading/Deleting Activities on ProjectHeena
 */
class Image_uploads {

    function __construct() {
        $CI = & get_instance();
        $CI->load->library('upload');
        $CI->load->library('image_lib');
    }

    /* This function performs the uploading of Images(Single/Multiple) */

    function uploadTaskImages($folder_name) {
        $CI = & get_instance();

        /*
         *  Check whether the upload path already exist ?
         * If not, then create a new folder else
         * upload file in exisiting folder
         */
        $pathToUpload = './uploads/projects/' . $folder_name;
        if (!file_exists($pathToUpload)) {
            mkdir($pathToUpload, 0777, TRUE);
            chmod($pathToUpload, 0777);
            mkdir($pathToUpload . '/images', 0777, TRUE);
            chmod($pathToUpload . '/images', 0777);
            mkdir($pathToUpload . '/thb', 0777, TRUE);
            chmod($pathToUpload . '/thb', 0777);
        }

        /*
         * Count how many files have user selected to upload
         */
        $total_files_to_upload = count($_FILES['filename']['name']);
        $length = strlen($_FILES['filename']['name'][0]);

        /*
         * $length != 0 => if user has selected some files; (if condition)
         * $length == 0 => if user has selected no files; (else condition)
         */

        if ($length != 0) {
            $image_name = time();
            for ($i = 0; $i < $total_files_to_upload; $i++) {
                $_FILES['userfile']['name'] = $_FILES['filename']['name'][$i];
                $_FILES['userfile']['type'] = $_FILES['filename']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $_FILES['filename']['error'][$i];
                $_FILES['userfile']['size'] = $_FILES['filename']['size'][$i];
                $image_name = $image_name + 1;
                $config['file_name'] = $image_name;
                $config['overwrite'] = FALSE;
                $config['upload_path'] = $pathToUpload . '/images/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = '1024';
                $config['max_width'] = '0';
                $config['max_height'] = '0';
                $CI->upload->initialize($config);
                if ($CI->upload->do_upload()) {
                    $data = $CI->upload->data();
                    chmod($data['full_path'], 0777);
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $data['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['thumb_marker'] = '';
                    $config['new_image'] = $pathToUpload . '/thb/' . $image_name . $data['file_ext'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150;
                    $config['height'] = 150;
                    $CI->image_lib->initialize($config);
                    $CI->image_lib->resize();
                } else {
                    $error = $CI->upload->display_errors();
                    $CI->session->set_flashdata('error_msg', $error);
                    redirect('create-task');
                }
            }
        } else {
            /*
             * Check if User had already Uploaded Any files earlier
             */
            $files = $this->getTaskImages($folder_name);
            if (count($files) >= 1) {

            } else {
                $this->default_task_image($pathToUpload);
            }
        }
    }

    /*
     * Since user has not selected any files to upload,
     * we upload our own default image
     */

    function default_task_image($dest_url) {
        $image_name = 'default';
        $img_src_url = realpath(APPPATH . "../uploads/default_images/tasks/images/default.jpg");
        if (!copy($img_src_url, $dest_url . '/images/' . $image_name . '.jpg')) {
            echo "failed to copy...\n";
        } else {
            chmod($dest_url . '/images/' . $image_name . '.jpg', 0777);
        }
        $thb_src_url = realpath(APPPATH . "../uploads/default_images/tasks/thb/default.jpg");
        if (!copy($thb_src_url, $dest_url . '/thb/' . $image_name . '.jpg')) {
            echo "failed to copy...\n";
        } else {
            chmod($dest_url . '/thb/' . $image_name . '.jpg', 0777);
        }
    }

    /*
     * This function deletes the Task Image from the folder.
     * It does NOT delete the folder.
     */

    function deleteTaskImage($task_id, $img_name) {
        $thumb_path = './uploads/projects/' . $task_id . '/thb/' . $img_name;
        $img_path = './uploads/projects/' . $task_id . '/images/' . $img_name;
        unlink($img_path);
        unlink($thumb_path);
    }

    /*
     * If user selects new images to upload, this funciton will be called
     * to delete the previous images which are stored in the folder
     */

    function deletePreviousImage($task_id) {
        $path = './uploads/projects/' . $task_id . '/thb/';
        $thb_files = scandir($path);
        foreach ($thb_files as $file) {
            if ($file != '.' && $file != '..') {
                $thumb_path = './uploads/projects/' . $task_id . '/thb/' . $file;
                unlink($thumb_path);
            }
        }

        $path = './uploads/projects/' . $task_id . '/images/';
        $img_files = scandir($path);
        foreach ($img_files as $file) {
            if ($file != '.' && $file != '..') {
                $img_path = './uploads/projects/' . $task_id . '/images/' . $file;
                unlink($img_path);
            }
        }
    }

    /*
     * This function is used to retrieve the names of Task Images
     * which belongs to $TaskId
     */

    function getTaskImages($task_id) {
        $path = './uploads/projects/' . $task_id . '/thb/';
        $files = scandir($path);
        $newFiles = array();
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                array_push($newFiles, $file);
            }
        }
        return $newFiles;
    }

    /*
     * This function will upload the Profile Image of the user when user first
     * signs up in ProjectHeena
     */

    function uploadProfileImage($user_id, $img_url) {
        // First create folder with name as user_id
        $folderName = $user_id;
        $pathToUpload = './uploads/users/' . $folderName;
        if (!file_exists($pathToUpload)) {
            mkdir($pathToUpload, 0111, TRUE);
            chmod($pathToUpload, 0777);
        } else {
            echo 'Folder Already Exist which shouldnt be..Contact Admin';
        }

        // Then copy the image from the URL and save it in your folder
        $folderPath = realpath(APPPATH . "../uploads/users/$user_id/");
        if ($img_url != NULL) {
            if (!copy($img_url, "$folderPath/me.jpg")) {
                echo "failed to copy...\n";
                log_message('Error', 'Image Error - Failed to copy profile pic for '.$user_id);
            }
        } else {
            $img_url = realpath(APPPATH . "../uploads/default_images/users/me.jpg");
            if (!copy($img_url, "$folderPath/me.jpg")) {
                echo "failed to copy...\n";
                log_message('Error', 'Image Error - Failed to copy Default Profile pic for '.$user_id);
            }
        }

        chmod("$folderPath/me.jpg", 0777);
        return;
    }

    /* ===================== NGO Images ================== */


    /*
     * This function will upload the Profile Image of the ngo when it registors
     */

    function uploadNgoImages($folder_name, $image_name) {

        /*
         *  Check whether the upload path already exist ?
         * If not, then create a new folder else
         * upload file in exisiting folder
         */
        $pathToUpload = './uploads/ngo/' . $folder_name;
        if (!file_exists($pathToUpload)) {
            mkdir($pathToUpload, 0777, TRUE);
            chmod($pathToUpload, 0777);

            mkdir($pathToUpload . '/overviewImages', 0777, TRUE);
            chmod($pathToUpload . '/overviewImages', 0777);

            mkdir($pathToUpload . '/overviewImages/thb', 0777, TRUE);
            chmod($pathToUpload . '/overviewImages/thb', 0777);

            mkdir($pathToUpload . '/overviewImages/images', 0777, TRUE);
            chmod($pathToUpload . '/overviewImages/images', 0777);

            mkdir($pathToUpload . '/profileImage', 0777, TRUE);
            chmod($pathToUpload . '/profileImage', 0777);

            mkdir($pathToUpload . '/profileImage/images', 0777, TRUE);
            chmod($pathToUpload . '/profileImage/images', 0777);

            mkdir($pathToUpload . '/profileImage/thb', 0777, TRUE);
            chmod($pathToUpload . '/profileImage/thb', 0777);
        }
        $length = strlen($_FILES['filename']['name'][0]);
        $length1 = strlen($_FILES["upload"]['name'][0]);

        /*
         * $length != 0 => if user has selected some files; (if condition)
         * $length == 0 => if user has selected no files; (else condition)
         */

        //if both images are given
        if ($length != 0 && $length1 != 0) {
            $image_name1 = time();
            $uploadPath = $pathToUpload . '/overviewImages/images/';
            $uploadPath1 = $pathToUpload . '/overviewImages/thb/';
            $filename = 'filename';
            $this->uploadNgoImage($filename, $image_name1, $uploadPath, $uploadPath1);


//            $image_name1 = time();
            $uploadPath2 = $pathToUpload . '/profileImage/images/';
            $uploadPath3 = $pathToUpload . '/profileImage/thb/';
            $filename = 'upload';
            $this->uploadNgoImage($filename, $image_name, $uploadPath2, $uploadPath3);
        }

        /* below code will work when javascript validation will fail */

        //if no image is given
        elseif ($length == 0 && $length1 == 0) {
            $this->uploadDefaultNgoProfileImage($pathToUpload, $image_name);
            $this->uploadDefaultNgoImage($pathToUpload);
        }
        //if ngo profile image is given
        elseif ($length == 0) {
            $filename = 'upload';
            $image_name1 = time();
            $uploadPath = $pathToUpload . '/profileImage/images';
            $uploadPath1 = $pathToUpload . '/profileImage/thb';
            $this->uploadDefaultNgoImage($pathToUpload);

            $this->uploadNgoImage($filename, $image_name, $uploadPath, $uploadPath1);
        } elseif ($length1 == 0) {
            $filename = 'filename';
            $image_name1 = time();
            $uploadPath = $pathToUpload . '/overviewImages/images';
            $uploadPath1 = $pathToUpload . '/overviewImages/thb';
            $this->uploadDefaultNgoProfileImage($pathToUpload, $image_name);
            $this->uploadNgoImage($filename, $image_name, $uploadPath, $uploadPath1);
        }
    }

    function uploadNgoImage($filename, $image_name, $pathToUpload, $thumbnailpath) {

        $CI = & get_instance();
        $_FILES['userfile']['name'] = $_FILES[$filename]['name'][0];
        $_FILES['userfile']['type'] = $_FILES[$filename]['type'][0];
        $_FILES['userfile']['tmp_name'] = $_FILES[$filename]['tmp_name'][0];
        $_FILES['userfile']['error'] = $_FILES[$filename]['error'][0];
        $_FILES['userfile']['size'] = $_FILES[$filename]['size'][0];

        $config['file_name'] = $image_name;
        $config['overwrite'] = FALSE;
        $config['upload_path'] = $pathToUpload;
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '1024';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $CI->upload->initialize($config);


        if ($CI->upload->do_upload()) {
            $data = $CI->upload->data();
            chmod($data['full_path'], 0777);
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['full_path'];
            $config['create_thumb'] = TRUE;
            $config['thumb_marker'] = '';
            $config['new_image'] = $thumbnailpath . $image_name . $data['file_ext'];
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 150;
            $config['height'] = 150;
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
        } else {
            $errors = $CI->upload->display_errors();
        }
        return;
    }

    function uploadDefaultNgoProfileImage($dest_url, $image_name) {
        $img_src_url = realpath(APPPATH . "../uploads/default_images/tasks/thb/default.png");
        if (!(copy($img_src_url, $dest_url . '/profileImage/images/' . $image_name . '.jpg') && copy($img_src_url, $dest_url . '/profileImage/thb/' . $image_name . '.jpg'))) {
            echo "failed to copy...\n";
        } else {
            chmod($dest_url . '/profileImage/thb/' . $image_name . '.jpg', 0777);
            chmod($dest_url . '/profileImage/images/' . $image_name . '.jpg', 0777);
        }
        return;
    }

    function uploadDefaultNgoImage($dest_url) {
        $image_name = time();
        $img_src_url = realpath(APPPATH . "../uploads/default_images/tasks/images/default.jpg");
        if (!(copy($img_src_url, $dest_url . '/overviewImages/thb/' . $image_name . '.jpg') && copy($img_src_url, $dest_url . '/overviewImages/images/' . $image_name . '.jpg'))) {
            echo "failed to copy...\n";
        } else {
            chmod($dest_url . '/overviewImages/thb/' . $image_name . '.jpg', 0777);
            chmod($dest_url . '/overviewImages/images/' . $image_name . '.jpg', 0777);
        }
        return;
    }

    /*
     * This function deletes the Ngo Image from the folder.
     * It does NOT delete the folder.
     */

    function deleteNgoImage() {
        $CI = & get_instance();
        $ngo_id = $CI->input->post('ngo_id');
        $img_name = $CI->input->post('imgname');
        $data_type = $CI->input->post('data_type');
        if ($data_type == 0) {
            $foldername = 'profileImage';
        } else if ($data_type == 1) {
            $foldername = 'overviewImages';
        }
        $thumb_path = './uploads/ngo/' . $ngo_id . '/' . $foldername . '/thb/' . $img_name;
        $img_path = './uploads/ngo/' . $ngo_id . '/' . $foldername . '/images/' . $img_name;
        unlink($img_path);
        unlink($thumb_path);
    }

    function getNgoProfileImage($path) {
        $files = scandir($path);
        $newFiles = array();
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                array_push($newFiles, $file);
            }
        }
        return $newFiles;
    }

    /*
     * If user selects new images to upload, this funciton will be called
     * to delete the previous images which are stored in the folder
     */

    function deletePreviousNgoImage($path) {
        $thb_files = scandir($path . '/thb');
        foreach ($thb_files as $file) {

            if ($file != '.' && $file != '..') {
                $thumb_path = $path . '/thb/' . $file;
                unlink($thumb_path);
            }
        }
        $img_files = scandir($path . '/images');
        foreach ($img_files as $file) {
            if ($file != '.' && $file != '..') {
                $img_path = $path . '/images/' . $file;
                unlink($img_path);
            }
        }
        return;
    }

}