<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MNC_form_validation extends CI_form_validation {
   function handle_upload()
  {
    if (isset($_FILES['image']) && !empty($_FILES['image']['name']))
      {
      if ($this->upload->do_upload('image'))
      {
        // set a $_POST value for 'image' that we can use later
        $upload_data    = $this->upload->data();
        $_POST['image'] = $upload_data['file_name'];
        return true;
      }
      else
      {
        // possibly do some clean up ... then throw an error
        $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
        return false;
      }
    }
    else
    {
      // throw an error because nothing was uploaded
      $this->form_validation->set_message('handle_upload', "You must upload an image!");
      return false;
    }
  }   
    
}