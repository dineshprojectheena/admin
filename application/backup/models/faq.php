<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Faq extends CI_Model {

    function get_faq($id)
    {    
        $this->db->where('status','1');
        $this->db->where('type',$id);
        $this->db->select('main_heading,descrtiption,status,type');
        $query = $this->db->get('faq');
        return $query->result_array();
    }
}
