<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @param type $email
 * @return type Array
 * @author Dinesh Kumawat
 * @modified_on 11/07/2014
 * @called_from login authorization
 */
class Login_authorization extends CI_Model {

    function gmail_social_auth($data, $unq_id) {
        $activity_data['user_last_logged_in'] = date('Y-m-d h:i:s');
        $activity_data['user_last_ip'] = $this->input->ip_address();
        $activity_data['type'] = 'gm';
        $gm_id = $data['gm_id'];
        $gm_link = $data['link'];
        $email = $data['email'];
//        echo $query="SELECT user_id,username,firstname,lastname,email,gm_id FROM `user` where email='$email' or gm_id='$gm_id'"; 
//        exit;
        $query = $this->db->query("SELECT user_id,username,firstname,lastname,email,gm_id FROM `user` where email='$email' or gm_id='$gm_id'");
        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            $track_detail = array(
                'user_id' => $res['user_id'],
                'ip' => $activity_data['user_last_ip'],
                'login_time' => $activity_data['user_last_logged_in'],
                'login_via' => $activity_data['type'],
            );
            $result = $query->result_array();
            $user_id = $result[0]['user_id'];
            $upd = $this->db->query("update `user` set gm_id='$gm_id',gm_link='$gm_link' where user_id='$user_id'");
            $this->db->insert('user_activity', $track_detail);
            return $res;
        } else {
            $ids = array('fb_id' => $unq_id);
            $datas = array_merge($data, $ids);
            $this->db->insert('user', $datas);
            $email_id = $data['email'];
            ################ Email Send When User Register
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
//            $email=$datas['email'];
            $html_data = "";
            $html_data.=" 
                Welcome To Mynewcar.in<br>
                Hi, $email
                <br>    
                Welcome to www.Mynewcar.in you have successfully registered.
                <br>                
                Thank You,
                <br>The Mynewcar.in Team.";

            $html_data;
            if ($mandrill_ready) {
                $email = array(
//            'html' => $this->load->view('emailers/download_config', TRUE), //Consider using a view file
                    'html' => $html_data, //Consider using a view file
                    'text' => 'Account created successully',
                    'subject' => 'Welcome to Mynewcar.in',
                    'from_email' => 'account@mynewcar.in',
                    'from_name' => 'MNC Account',
                    'to' => array(array('email' => $email_id))
                );
                $result = $this->mandrill->messages_send($email);
            }
            ################ Email Send When User Register 
//            echo $query="SELECT user_id,username,firstname,email,gm_id FROM `user` where email='$email' or gm_id='$gm_id'"; 
            return $query = $this->db->query("SELECT user_id,username,firstname,email,gm_id FROM `user` where email='$email' or gm_id='$gm_id'")->row_array();
        }
    }

    function facebook_social_auth($data) {
        $activity_data['user_last_logged_in'] = date('Y-m-d h:i:s');
        $activity_data['user_last_ip'] = $this->input->ip_address();
        $activity_data['type'] = 'fb';
        $fb_id = $data['fb_id'];
        $fb_link = $data['fb_link'];
        $email = $data['email'];
        $query = $this->db->query("SELECT user_id,username,firstname,lastname,email,fb_id FROM `user` where email='$email' or fb_id='$fb_id'");
        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            $track_detail = array(
                'user_id' => $res['user_id'],
                'ip' => $activity_data['user_last_ip'],
                'login_time' => $activity_data['user_last_logged_in'],
                'login_via' => $activity_data['type'],
            );
            $result = $query->result_array();
            $user_id = $result[0]['user_id'];
            $upd = $this->db->query("update `user` set fb_id='$fb_id',fb_link='$fb_link' where user_id='$user_id'");
            $this->db->insert('user_activity', $track_detail);
            return $res;
        } else {
            $this->db->insert('user', $data);
            $email_id = $data['email'];
            ################ Email Send When User Register
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
//            $email=$datas['email'];
            $html_data = "";
            $html_data.=" 
                Welcome To Mynewcar.in<br>
                Hi, $email
                <br>    
                Welcome to www.Mynewcar.in you have successfully registered.
                <br>                
                Thank You,
                <br>The Mynewcar.in Team.";

            $html_data;
            if ($mandrill_ready) {
                $email = array(
//            'html' => $this->load->view('emailers/download_config', TRUE), //Consider using a view file
                    'html' => $html_data, //Consider using a view file
                    'text' => 'Account created successully',
                    'subject' => 'Welcome to Mynewcar.in',
                    'from_email' => 'account@mnc.com',
                    'from_name' => 'MNC Account',
                    'to' => array(array('email' => $email_id))
                );
                $result = $this->mandrill->messages_send($email);
            }
            ################ Email Send When User Register 


            $this->db->select('user_id,username,firstname,email,fb_id');
            $this->db->where('fb_id', $data['fb_id']);
            return $res = $this->db->get('user')->row_array();
        }
    }

    function linkdin_social_auth($data, $unq_id) {
        $activity_data['user_last_logged_in'] = date('Y-m-d h:i:s');
        $activity_data['user_last_ip'] = $this->input->ip_address();
        $activity_data['type'] = 'ld';
        $linkdin = $data['linkdin'];
        $linkedin_link = $data['linkedin_link'];
        $email_id = $data['email'];
        $query = $this->db->query("SELECT user_id,username,firstname,lastname,email,linkdin FROM `user` where email='$email_id' or linkdin='$linkdin'");
        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            $track_detail = array(
                'user_id' => $res['user_id'],
                'ip' => $activity_data['user_last_ip'],
                'login_time' => $activity_data['user_last_logged_in'],
                'login_via' => $activity_data['type']
            );
            $result = $query->result_array();
            $user_id = $result[0]['user_id'];
            $upd = $this->db->query("update `user` set linkdin='$linkdin',linkedin_link='$linkedin_link' where user_id='$user_id'");
            $this->db->insert('user_activity', $track_detail);
            return $res;
//          return $ress=$this->db->query("SELECT user_id,username,firstname,lastname,email,linkdin FROM `user` where email='$email_id' or linkdin='$linkdin'"); 
        } else {
            $ids = array('fb_id' => $unq_id);
            $datas = array_merge($data, $ids);
            $this->db->insert('user', $datas);
            ################ Email Send When User Register
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
//            $email=$datas['email'];
            $html_data = "";
            $html_data.=" 
                Welcome To Mynewcar.in<br>
                Hi, $email
                <br>    
                Welcome to www.Mynewcar.in you have successfully registered.
                <br>                
                Thank You,
                <br>The Mynewcar.in Team.";

            $html_data;
            if ($mandrill_ready) {
                $email = array(
//            'html' => $this->load->view('emailers/download_config', TRUE), //Consider using a view file
                    'html' => $html_data, //Consider using a view file
                    'text' => 'Account created successully',
                    'subject' => 'Welcome to Mynewcar.in',
                    'from_email' => 'account@mnc.com',
                    'from_name' => 'MNC Account',
                    'to' => array(array('email' => $email_id))
                );
                $result = $this->mandrill->messages_send($email);
            }
            ################ Email Send When User Register 
            return $query = $this->db->query("SELECT user_id,username,firstname,email,linkdin FROM `user` where email='$email_id' or linkdin='$linkdin'")->row_array();
        }
    }

    function join_brand_group($brand_id, $user_id, $status) {
        $count_res = $this->db->query("SELECT * FROM `join_group` where user_id='$user_id' and status='$status' and brand_id='$brand_id'")->num_rows();
        if ($count_res == 0) {
            $res = $this->db->query("INSERT INTO `join_group` (`join_id` ,`user_id` ,`status` ,`brand_id`) VALUES ('', '$user_id', '$status', '$brand_id')");
            return $count_res;
        } else {
            return $count_res;
        }
    }

    function join_group_status($user_id, $brand_id) {
        return $count_res = $this->db->query("SELECT * FROM `join_group` where user_id='$user_id' and status='1' and brand_id='$brand_id'")->result_array();
    }

    function brand_wise_user($brand_id) {
        return $res = $this->db->query("SELECT u.fb_id,u.user_id FROM `join_group` j,`user` u where j.brand_id='$brand_id' and u.user_id=j.user_id")->result_array();
    }

    function custom_login($signin_email, $signin_password) {
        $activity_data['user_last_logged_in'] = date('Y-m-d h:i:s');
        $activity_data['user_last_ip'] = $this->input->ip_address();
        $activity_data['type'] = 'custom';
        $signin_password = md5($signin_password);
        $query = $this->db->query("SELECT user_id,username,firstname,lastname,email FROM `user` where password='$signin_password' and (email='$signin_email' or username='$signin_email')");


        if ($query->num_rows() > 0) {
            $res = $query->row_array();

            
            $track_detail = array(
                'user_id' => $res['user_id'],
                'ip' => $activity_data['user_last_ip'],
                'login_time' => $activity_data['user_last_logged_in'],
                'login_via' => $activity_data['type']
            );
            $this->db->insert('user_activity', $track_detail);
            return $res;
        }
    }

    function check_email($signin_email) {
        return $query = $this->db->query("SELECT user_id FROM `user` where email='$signin_email'")->result_array();
    }

    function update_unq_forget($forget_email, $uniue_id, $user_id) {
        $query = $this->db->query("SELECT user_id FROM `temp_forget` where email_id='$forget_email' and user_id='$user_id' and status='0'")->result_array();

        if (empty($query)) {
            return $query = $this->db->query("INSERT INTO `temp_forget`(`temp_id`, `user_id`, `email_id`, `random_key`, `added_date`, `status`) "
                    . "VALUES ('','$user_id','$forget_email','$uniue_id',NOW(),'0')");
        } else {
            $querys = $this->db->query("update `temp_forget` set status='1' where user_id='$user_id'");
            return $query = $this->db->query("INSERT INTO `temp_forget`(`temp_id`, `user_id`, `email_id`, `random_key`, `added_date`, `status`) "
                    . "VALUES ('','$user_id','$forget_email','$uniue_id',NOW(),'0')");
        }
    }

    function update_password($user_id, $new_password, $re_new_password) {
        $new_password = md5($new_password);
        $querys = $this->db->query("update `temp_forget` set status='1' where user_id='$user_id'");
        return $querys = $this->db->query("update `user` set password='$new_password',last_profile_updated=NOW() where user_id='$user_id'");
    }
    
    #### Change Password
    
    function get_change_user($id) {
        return $upd = $this->db->query("SELECT * FROM `temp_forget` where random_key='$id' and status='0'")->result_array();
    }
    
    #### Check username
    
    function check_username($regis_username, $a) {
        if ($a == 1) {
            return $details = $this->db->query("select user_id from user where username='$regis_username'")->result_array();
        }
        if ($a == 2) {
            return $details = $this->db->query("select user_id from user where email='$regis_username'")->result_array();
        }
        if ($a == 4) {
            return $details = $this->db->query("select user_id from user where email='$regis_username'")->result_array();
        }
    }
    
    
    function custome_user($regis_username, $regis_email, $regis_password, $id) {

        $mds_pass = md5($regis_password);
        $res = $this->db->query("select * from temp_user where email_id='$regis_email' and username='$regis_username' and status!='1'")->result_array();
        if (empty($res)) {
            $uniue_id = md5(uniqid($regis_username, true));
            $details = $this->db->query("INSERT INTO `temp_user`(`temp_id`, `username`, `email_id`, `password`, `random_key`,`status`) "
                    . "VALUES ('','$regis_username','$regis_email','$mds_pass','$uniue_id','0')");
            $id = $this->db->insert_id();
            return $res = $this->db->query("select * from temp_user where temp_id='$id' and email_id='$regis_email' and username='$regis_username' and status!='1'")->result_array();
        } else {

            return $res;
        }
    }
    
    
    function activate_user($id) {
//        echo $res = "select * from temp_user where status='0' and random_key='$id'";
        $res = $this->db->query("select * from temp_user where status='0' and random_key='$id'")->result_array();
        $res_data = array();
        if (!empty($res)) {
            $temp_id = $res[0]['temp_id'];
            $username = $res[0]['username'];
            $email_id = $res[0]['email_id'];

            $password = $res[0]['password'];
            $res_data[] = $username;
            $res_data[] = $email_id;
            $res_data[] = $password;
            $random_key = $res[0]['random_key'];
            $res2 = $this->db->query("select * from user where username='$username' and email='$email_id' and password='$password'");
            if (!empty($res2)) {
                $res1 = $this->db->query("INSERT INTO `user`(`username`,`firstname`,`password`,`email`,`last_profile_updated`,`date_added`,`last_logged_In`) VALUES ('$username','$username','$password','$email_id',now(),now(),now())");
                $upd = $this->db->query("UPDATE `temp_user` SET `status`='1' WHERE temp_id='$temp_id'");
            }
        }
        return $res_data;
    }
    
    
    
    function upload_profile($img_name) {
        $exp = explode('.', $img_name);
        $profile = $exp[0];
        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        return $data = $this->db->query("UPDATE `user` SET `fb_id`='$profile' WHERE `user_id`='$user_id'");
    }
    
    function upload_cover($img_name) {
        $exp = explode('.', $img_name);
        $cover = $exp[0];

        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        return $data = $this->db->query("UPDATE `user` SET `cover`='$cover' WHERE `user_id`='$user_id'");
    }
    
    
    

}
