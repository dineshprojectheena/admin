<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_section extends CI_Model {
    
    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }
    function product_color($color_id, $comp_cond) {
        if ($comp_cond == 2) {
            return $query = $this->db->query("SELECT `body_color_id`, `body_icon`, `color` FROM `body_color` WHERE `status` IN ('1','3') and `body_color_id` IN ($color_id)")->result_array();
        }
    }
    
    function product_configure($id,$user_id,$main_city)
    {
    return $spec = $this->db->query("SELECT *  FROM `car_configuration` where product_id='$id' and user_id='$user_id' and user_id>'0' and status!='2' order by car_configuration_id DESC limit 1")->row_array();    
    }
    
    
    function pro_specification($pro_id) {
        return $spec = $this->db->query("SELECT *  FROM `specification` where variant_id='$pro_id'")->result_array();
    }
    
    function expert_review_count($pro_id, $id) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select_avg('total');
        if ($id == 1) {
            return $get_data = $this->db->get('expert_rating')->result_array();
        }
        if ($id == 2) {
            return $get_data = $this->db->get('user_rating')->result_array();
        }
    }
    function reting_product_detail($pro_id) {
        return $query = $this->db->query("SELECT booking_amount,pro_name_comp,pro_name,pro_image,variant_id,pro_detail,upcoming,launching_date FROM `variant` WHERE variant_id='$pro_id'  and `status` IN ('1','3')")->result_array();
    }
    
    function product_variant_detail($id) {
        return $query = $this->db->query("SELECT * FROM `specification` WHERE variant_id='$id'")->result_array();
    }
    
    function get_features_name($pro_ids, $id) {
        if ($id == 1) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 2) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 3) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 4) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 5) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 6) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        }
    }
    
    
    function product_features($pro_id, $user_id) {



        return $sel = $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$pro_id'")->result_array();
    }
    
    function rating_details($id) {
        $get_data = $this->db->query("SELECT pro_name,variant_id,model_id,brand_id,pro_name_comp FROM `variant` where variant_id='$id'  and status='1'")->result_array();
        $model_id = $get_data[0]['model_id'];
        $brand_id = $get_data[0]['brand_id'];
        return $all_res = $this->db->query("SELECT v.pro_name,v.pro_name_comp,v.variant_id,(select brand_name from brand where brand_id='$brand_id') as brand_name,(select model_name from model where model_id='$model_id') as model_name FROM `variant` v where v.brand_id='$brand_id' and v.model_id='$model_id'  and v.status='1'")->result_array();
    }
    
    function car_gallery_detail($pro_id) {
        $data = $this->db->query("select v.variant_id,(select b.brand_name from brand b where v.brand_id=b.brand_id) as brand_name,(select m.model_name from model m where m.model_id=v.model_id) as model_name from variant v where v.variant_id='$pro_id' and v.`status` IN ('1','3')")->row_array();
        $brand_name = $data['brand_name'];
        $model_name = $data['model_name'];
        $directory = APPPATH . "../admin/uploads/portfolio/" . ucfirst(strtolower($data['brand_name'])) . '/' . ucfirst(strtolower($data['model_name'])) . '/';
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        $data = array();
        foreach ($scanned_directory as $scanned_directory_data) {
            $arr = array("Image" => $scanned_directory_data, "Brand" => ucfirst(strtolower($brand_name)), "Model" => ucfirst(strtolower($model_name)), "Brand2" => $brand_name);
            $data[] = $arr;
        }
//        print_r($data);
        return ($data);
    }
    
    function car_intro_gallery_detail($id, $type) {
//        echo $id;
        if ($type == 'All') {
            return $car_intro_detail = $this->db->query("SELECT * FROM `product_gallery` where prduct_id='$id'")->result_array();
        } else {
            return $car_intro_detail = $this->db->query("SELECT image,product_gallery_id,video FROM `product_gallery` where prduct_id='$id' and page='$type'")->result_array();
        }
    }
    
    function select_offers_product($brand, $cond) {
        if ($cond == 1) {
            $main_city = $this->common_city();
            $brand_result = $this->db->query("SELECT brand_id,brand_name FROM `brand` where `brand_name`='$brand'")->result_array();
            $brand_id = $brand_result[0]['brand_id'];
            $offer_result = $this->db->query("SELECT * FROM `deal` where `brand_id`='$brand_id' and status='1' and city_id='$main_city'")->result_array();
            $final = array();
//        print_r($offer_result);
            foreach ($offer_result as $offer_result_data) {
                $variant_id = $offer_result_data['variant_id'];
                $variant_name = $this->db->query("SELECT pro_name FROM `variant` where `variant_id`='$variant_id'  and `status` IN ('1','3')")->result_array();
                $final[] = array_merge($variant_name[0], $offer_result_data);
            }
            return $final;
        } else {
            $main_city = $this->common_city();
            $brand_result = $this->db->query("SELECT brand_id,brand_name FROM `brand` where `brand_name`='$brand'")->result_array();
            $brand_id = $brand_result[0]['brand_id'];
            $offer_result = $this->db->query("SELECT * FROM `deal` where `brand_id`='$brand_id' and status='1' and city_id='$main_city'")->result_array();
            $final = array();
//        print_r($offer_result);
            foreach ($offer_result as $offer_result_data) {
                $variant_id = $offer_result_data['variant_id'];
                $variant_name = $this->db->query("SELECT pro_name FROM `variant` where `variant_id`='$variant_id'  and `status` IN ('1','3')")->result_array();
                $final[] = array_merge($variant_name[0], $offer_result_data);
            }
            return $final;
        }
    }
    
    function onraod_detail($pro_id) {
        $main_city = $this->common_city();

        return $query = $this->db->query("select mnc_on_road_price from onroad_price where variant_id='$pro_id' and city_id='$main_city'")->result_array();
    }
    
    function car_specifiaction($id, $type) {

        if (!empty($id)) {
            return $car_intro_detail = $this->db->query("SELECT m.`price`,f.`feature_id`, f.`feature_type`, f.`feature_name`, f.`feature_desc`, f.`feature_img`, f.`recomended`, f.`cost` FROM `feature_variant_mapping` m,`features` f WHERE m.`variant_id`='$id' and m.`feature_id`=f.`feature_id` and  `f`.`feature_name` like '%$type%'")->result_array();
        }
    }
    
    function accessory_services($id, $user_id, $cond) {
        if ($cond == 1) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='11' limit 1")->result_array();
        } else if ($cond == 2) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='12' limit 1")->result_array();
        } else if ($cond == 3) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='13' limit 1")->result_array();
        } else if ($cond == 4) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='14' limit 1")->result_array();
        } else if ($cond == 5) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='15' limit 1")->result_array();
        } else if ($cond == 6) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='16' limit 1")->result_array();
        }
    }
    
    function get_emi_record($emi_id, $prod_id) {
//    echo "SELECT * FROM `emi_calculation` where emi_id='$emi_id' and pro_id='$prod_id'";    
        return $del = $this->db->query("SELECT * FROM `emi_calculation` where emi_id='$emi_id' and pro_id='$prod_id'")->result_array();
    }
    
    function get_total_saving($id, $user_id) {
        $main_city = $this->common_city();
        return $sel = $this->db->query("select m.product_id,m.city_id,m.cash_discount,m.insurance_discount,m.exchange_bonus,m.loyality_bonus,m.corporate_discount,m.accessory_discount,m.special_reward,m.accessory_package from mnc_exshowroom_price m where m.product_id='$id' and m.city_id='$main_city'")->row_array();
    }
    
    function get_onroad_price($id, $user_id) {
        $main_city = $this->common_city();
        return $sel = $this->db->query("select m.extended_warranty,m.product_id,m.zero_depreciation,m.city_id,e.exshowroom_price,o.comp_insurance,o.road_tax,o.handling_price,o.lbt,o.mnc_on_road_price,o.onroad_price from mnc_exshowroom_price m,exshowroom_price e,onroad_price o where e.product_id=m.product_id and e.city_id=m.city_id and m.product_id=o.variant_id and m.city_id=o.city_id and m.city_id='$main_city' and m.product_id='$id'")->result_array();
//    return $sel = $this->db->query("select m.product_id,m.city_id,e.exshowroom_price,(select comp_insurance from onroad_price where onroad_price.variant_id=m.product_id and onroad_price.city_id='$main_city') as comp_insurance from mnc_exshowroom_price m,exshowroom_price e where m.product_id='$id' and m.city_id='$main_city' and e.product_id=m.product_id")->result_array();    
    }
    
     function coupon_discount($c_coupon_id) {
        return $check = $this->db->query("select * from `coupon` where coupon_id='$c_coupon_id'")->row_array();
    }
    
    function price_specification($pro_id) {
        $main_city = $this->common_city();
        return $sel = $this->db->query("select * from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city'")->row_array();
    }
    
    function get_meta($cond, $id) {
        if ($cond == 1) {
            return $query = $this->db->query("select seo_keyword,seo_title,seo_description from variant where variant_id='$id'")->result_array();
        }
        if ($cond == 2) {
            return $query = $this->db->query("select seo_keyword,seo_title,seo_description from brand where brand_id='$id'")->result_array();
        }
    }
    
    function coupon_configuration($coupon_code, $pro_id, $city, $color) {
        $start_date = date('Y-m-d');
        $main_city = $this->common_city();

        $brand_res = "SELECT brand_id,model_id FROM `variant` where variant_id='$pro_id'";
        $brand_res = $this->db->query("SELECT brand_id,model_id FROM `variant` where variant_id='$pro_id'")->row_array();
        $brand_id = $brand_res['brand_id'];
        $model_id = $brand_res['model_id'];
        $coupon_checl=$this->db->query("SELECT * FROM `coupon` where ((coupn_code='$coupon_code'))")->num_rows();
        if($coupon_checl==0)
        {
        return '0';    
        }
        $coupon_checl2=$this->db->query("SELECT * FROM `coupon` where ((coupn_code='$coupon_code') and coupon_used>=no_of_coupon)")->num_rows();
        if($coupon_checl2==1)
        {
        return '1';    
        }
        
        $car_coupon='';
        $car_coupon.="SELECT * FROM `coupon` where (coupn_code='$coupon_code' and coupon_used<=no_of_coupon) and ";
        if($start_date!='') {
            $car_coupon.=" (start_date<='$start_date' and end_date>='$start_date')";
        }
//        echo $car_coupon;
        $get=$this->db->query($car_coupon)->result_array();
        
        $counts=0;
        foreach ($get as $get_data) {
            $type = $get_data['type'];
            $exp_type = array_filter(explode('-', $type));
            $tot=count($exp_type);
            if(in_array("1", $exp_type)){            
            if($city==$get_data['city_id'])
            {    
            $counts=$counts+1;
            }               
            }
            
            if(in_array("2", $exp_type)){            
            if($pro_id==$get_data['variant_id'])
            {    
            $counts=$counts+1;
            }               
            }
            
            if(in_array("3", $exp_type)){            
            if($brand_id==$get_data['brand'])
            {    
            $counts=$counts+1;
            }               
            }
            
            if(in_array("4", $exp_type)){            
            if($color==$get_data['color'])
            {    
            $counts=$counts+1;
            }               
            }
            
            
            if(in_array("5", $exp_type)){            
            if($model_id==$get_data['model'])
            {    
            $counts=$counts+1;
            }               
            }
            
            
            if($tot==$counts)
            {
//            echo $tot;    
//            echo $counts;    
            $amount_percent = $get_data['amount_percent'];
            $coupon_id = $get_data['coupon_id'];
            $coupon_used = $get_data['coupon_used'];
            $coupon_material = $get_data['coupon_material'];
            if($coupon_material!='')
            {
            $coupon_material='1';    
            }    
            else 
            {
            $coupon_material='0';    
            }    
            
            
            
            
            
            
            $coupon_used = $coupon_used + 1;
            $cook_coupon_id = $pro_id . '_-_' . $coupon_id . '_-_' . $coupon_code.'_-_'.$main_city;            
            
            
//            $upd = $this->db->query("update `coupon` set coupon_used='$coupon_used' where coupon_id='$coupon_id'");
            setcookie('coupon_id', $cook_coupon_id, time() + (86400 * 30), "/");
            return '3';
            }
            else
            {
            return '4';    
            }    
        }
        return '5';
    }

//    function coupon_discount($c_coupon_id) {
//        return $check = $this->db->query("select * from `coupon` where coupon_id='$c_coupon_id'")->row_array();
//    }
    
     
    
}
