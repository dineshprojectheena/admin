<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_home_section extends CI_Model {
# Api Data process start here #

    function all_citys_detalis() {
        return $query = $this->db->query("SELECT name,city_id FROM `city`")->result_array();
    }

    function get_brands($id){
//        if ($id == "") {
            return $query = $this->db->query("SELECT `brand_id`,`brand_name`,`brand_image`,CONCAT((select path_url from path where path_name='brand'),'',`brand_image`) FROM `brand` WHERE `status`='1' order by brand_name")->result_array();
//        }
    }
    
    function get_car_type(){
            return $query = $this->db->query("SELECT pro_type_id,pro_type_name,image,CONCAT((select path_url from path where path_name='type'),'',`image`) as path from pro_type")->result_array();
    }

    function get_slider($city) {
        if ($city == '') {
            $city = '1';
        }
        $check = $this->db->query("select s.* from `slider` s,`city` c where s.city=c.name and c.city_id='$city'")->result_array();
        return $check;
    }

//    function get_car_type() {        
//        $check = $this->db->query("select distinct(pro_type) from variant")->result_array();
//        return $check;
//    }

    function get_rev_model($brand_data) {
        return $query = $this->db->query("SELECT model_id,model_name FROM `model` where brand_id='$brand_data'")->result_array();
    }

    function get_rev_variant($brand_data, $model_data) {
        return $query = $this->db->query("SELECT variant_id,pro_name FROM `variant` where brand_id='$brand_data' and model_id='$model_data' and status='1'")->result_array();
    }

    function max_price($city) {
        $que = "";
        $que.="SELECT MAX(m.mnc_exshowroom_price) as max_price,MIN(m.mnc_exshowroom_price) as min_price FROM `mnc_exshowroom_price` m";
        if ($city != '') {
            $que.=" where m.city_id='$city'";
        }
        return $details = $this->db->query($que)->row_array();
    }

    function home_page_carousel($brand_id, $main_city) {
        $m_q = "SELECT h.percent,h.variant_id,v.pro_name_comp,v.pro_image,"
                . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city' limit 1) as exshowroom_price,"
                . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as mnc_exshowroom_price,"
                . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as total_benefits,"
                . "(select m.total_benefits_perc from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as total_benefits_perc,"
                . "(select m.finance_benefit from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as finance_benefit,"
                . "(select m.exchange_benefit from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as exchange_benefit,"
                . "(select m.accessory_package from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as accessory_package,"
                . "(select m.accessory_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as accessory_benefits"
                . " FROM `hot_deals` h,variant v  WHERE 1=1 and v.variant_id=h.variant_id and v.brand_id='$brand_id' and h.city_id='$main_city' and v.status='1'";
        return $m_q_d = $this->db->query($m_q)->result_array();
    }
    
    function hot_deal_res($brand,$main_city,$pro_type)
    {   
        if ($brand != '') {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where brand_id='$brand'")->row_array();
            $brand_id = "'" . $brand['brand_id'] . "'";
        } else {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where status='1'")->result_array();
            $res = '';
            foreach ($brand as $brand_res) {
                $res = $res . "'" . $brand_res['brand_id'] . "',";
            }
            $brand_id = rtrim($res, ",");
        }
        $res = array();
        $main_data = '';
        $main_data.="SELECT h.*,v.pro_name_comp,v.pro_image,"
                . "(select brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                . "(select m.total_savings from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_savings,"
                . "(select m.total_benefits_perc from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits_perc,"
                . "(select m.finance_benefit from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as finance_benefit,"
                . "(select m.accessory_package from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as accessory_package,"
                . "(select m.extended_warranty from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as extended_warranty,"
                . "(select m.accessory_discount from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as accessory_discount,"
                . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                . " FROM hot_deals h,variant v where v.variant_id=h.variant_id and v.brand_id IN ($brand_id)  and v.status='1'";
        if ($pro_type != '') {
            $main_data.=" and v.pro_type='$pro_type'";
        }

        $main_data.=" order by brand_id ASC";
        $data = $this->db->query($main_data)->result_array();
//        print_r($main_data);
//        echo '<pre>';
        $main_data=array();
        foreach($data as $data_res)
        {
        if($data_res['city_id']==$main_city)
        {
        $main_data[]=$data_res;    
        }       
        } 
      
        return $main_data;
       
    }
    
    function pro_specification_res($product_id)
    {
        
    return $spec = $this->db->query("SELECT s.*,(select `fuel_type` from `fuel_type` where fuel_type.fuel_type_id=v.fuel_type) as fuel_type,v.tramission_type  FROM `specification` s,variant v where s.variant_id='$product_id' and v.variant_id=s.variant_id")->row_array();
    }
    
    function pro_feature_res($product_id)
    {
        
         return $sel = $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$product_id'")->result_array();
    }
    

    function product_detail($pro_id, $main_city) {
        $que = "SELECT v.variant_id,v.pro_name,v.seating_capacity,v.pro_name_comp,v.tramission_type,"
                . "(select path_url from path where path_name='product') as path_url,"
                . "v.pro_image,v.pro_detail,v.pro_type,"
                
                . "(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                . "(select m.model_name from model m where m.model_id=v.model_id) as model_name, "
                . "(select f.fuel_type from fuel_type f where f.fuel_type_id=v.fuel_type) as fuel_type,"
                . "(select avg(total) from user_rating where pro_id='$pro_id') as user_rating,"
                . "(select avg(total) from expert_rating where pro_id='$pro_id') as exp_rating,"
                . "(select mnc_exshowroom_price from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as mnc_price,"
                . "(select exshowroom_price from exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as ex_price,"
                . "(select total_benefits from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits,"
                . "(select total_benefits_perc from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits_perc,"
                . "(select total_savings from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_savings"
                . " FROM `variant` v,`model` m "
                . " WHERE m.model_id=v.model_id and v.variant_id='$pro_id' and v.variant_id!='' and v.status='1'";
        $query = $this->db->query($que)->result_array();
        
        
        $color= $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,(select path_url from path where path_name='color') as path_url,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$pro_id' and feature_type='7'")->result_array();
        
       
        
        $que="select u.user_id,u.user_rating_id,u.date,heading,u.what_i_like,u.what_i_dont_like,u.total,(select a.fb_id from user a where a.user_id=u.user_id) as fb_link from user_rating u where pro_id='$pro_id' ";
        $user_review= $this->db->query($que)->result_array();
        
        
        $que="select * from expert_rating where pro_id='$pro_id' ";
        $exp_review= $this->db->query($que)->result_array();
        
        
        $data = $this->db->query("select v.variant_id,(select path_url from path where path_name='portfolio') as path_url,(select b.brand_name from brand b where v.brand_id=b.brand_id) as brand_name,(select m.model_name from model m where m.model_id=v.model_id) as model_name from variant v where v.variant_id='$pro_id' and v.status='1'")->row_array();
        $path_url = $data['path_url'];        
        
        $brand_name = $data['brand_name'];
        $model_name = $data['model_name'];
        $directory = APPPATH . "../admin/uploads/portfolio/" . ucfirst(strtolower($data['brand_name'])) . '/' . ucfirst(strtolower($data['model_name'])) . '/';
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        $gallery = array();
        foreach ($scanned_directory as $scanned_directory_data) {
            $arr = array("Image_path" => $path_url,"Image" => $scanned_directory_data, "Brand" => ucfirst(strtolower($brand_name)), "Model" => ucfirst(strtolower($model_name)));
            $gallery[] = $arr;
        }   
        
        
        
        return $data=array('product'=>$query,'color'=>$color,'user_review'=>$user_review,'exp_review'=>$exp_review,'gallery'=>$gallery);
        
        
    } 
    
    function get_coming_soon()
    {
        
        $que.="select * from variant where upcoming='y' and status='3'";

        if (!empty($exp_estimated_price[0]) && !empty($exp_estimated_price[1])){
            $que.=" and extimated_price >= '$exp_estimated_price[0]' and extimated_price_max<= '$exp_estimated_price[1]'";
        }

        if ($launching_date != '') {
            $que.=" and launching_date='$launching_date'";
        }
        if ($brand_id != '') {
            $que.=" and brand_id='$brand_id'";
        }
        $check = $this->db->query($que)->result_array();
        return $check;         
    }
    
    

    function pro_specification($pro_id) {
        return $spec = $this->db->query("SELECT *  FROM `specification` where variant_id='$pro_id'")->result_array();
    }

    function product_features($pro_id) {
        return $sel = $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$pro_id'")->result_array();
    }

    function get_review($pro_id) {
        $que = "";
        $que.="select u.user_id,u.user_rating_id,u.date,heading,u.what_i_like,u.what_i_dont_like,u.total,(select a.fb_id from user a where a.user_id=u.user_id) as fb_link from user_rating u where pro_id='$pro_id' ";
        if ($user_sort == 'Rating') {
            $que.="order by total,user_rating_id DESC";
        } else if ($user_sort == 'Date') {
            $que.="order by date,user_rating_id DESC";
        } else {
            $que.="order by user_rating_id DESC";
        }
        return $get_data = $this->db->query($que)->result_array();
    }

    function get_exp_review($pro_id) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select('expert_rating_id,date,heading,what_i_like,what_i_dont_like,total,profile_id');
        $this->db->order_by("expert_rating_id", "desc");
        return $get_data = $this->db->get('expert_rating')->result_array();
    }

    function car_gallery_detail($pro_id) {
        $data = $this->db->query("select v.variant_id,(select b.brand_name from brand b where v.brand_id=b.brand_id) as brand_name,(select m.model_name from model m where m.model_id=v.model_id) as model_name from variant v where v.variant_id='$pro_id' and v.status='1'")->row_array();
        $brand_name = $data['brand_name'];
        $model_name = $data['model_name'];
        $directory = APPPATH . "../admin/uploads/portfolio/" . ucfirst(strtolower($data['brand_name'])) . '/' . ucfirst(strtolower($data['model_name'])) . '/';
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        $data = array();
        foreach ($scanned_directory as $scanned_directory_data) {
            $arr = array("Image" => $scanned_directory_data, "Brand" => ucfirst(strtolower($brand_name)), "Model" => ucfirst(strtolower($model_name)));
            $data[] = $arr;
        }
        return ($data);
    }

    function car_specifiaction($id, $type) {

        if (!empty($id)) {
            return $car_intro_detail = $this->db->query("SELECT m.`price`,f.`feature_id`, f.`feature_type`, f.`feature_name`, f.`feature_desc`, f.`feature_img`, f.`recomended`, f.`cost` FROM `feature_variant_mapping` m,`features` f WHERE m.`variant_id`='$id' and m.`feature_id`=f.`feature_id` and  `f`.`feature_name` like '%$type%'")->result_array();
        }
    }

    function accessory_services($id, $cond) {
        if ($cond == 1) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='11' limit 1")->result_array();
        } else if ($cond == 2) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='12' limit 1")->result_array();
        } else if ($cond == 3) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='13' limit 1")->result_array();
        } else if ($cond == 4) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='14' limit 1")->result_array();
        } else if ($cond == 5) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='15' limit 1")->result_array();
        } else if ($cond == 6) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='16' limit 1")->result_array();
        }
    }

    function get_total_saving($id, $main_city) {
        return $sel = $this->db->query("select m.total_savings,m.product_id,m.city_id,m.cash_discount,m.insurance_discount,m.exchange_bonus,m.loyality_bonus,m.corporate_discount,m.accessory_discount,m.special_reward,m.accessory_package from mnc_exshowroom_price m where m.product_id='$id' and m.city_id='$main_city'")->row_array();
    }

    function get_onroad_price($id, $main_city) {
        return $sel = $this->db->query("select m.product_id,m.zero_depreciation,m.city_id,e.exshowroom_price,o.comp_insurance,o.road_tax,o.handling_price,o.lbt,o.mnc_on_road_price,o.onroad_price from mnc_exshowroom_price m,exshowroom_price e,onroad_price o where e.product_id=m.product_id and e.city_id=m.city_id and m.product_id=o.variant_id and m.city_id=o.city_id and m.city_id='$main_city' and m.product_id='$id'")->result_array();
    }

    function rating_details($id) {
        $get_data = $this->db->query("SELECT pro_name,variant_id,model_id,brand_id,pro_name_comp FROM `variant` where variant_id='$id'  and status='1'")->result_array();
        $model_id = $get_data[0]['model_id'];
        $brand_id = $get_data[0]['brand_id'];
        return $all_res = $this->db->query("SELECT v.pro_name,v.pro_name_comp,v.variant_id,(select brand_name from brand where brand_id='$brand_id') as brand_name,(select model_name from model where model_id='$model_id') as model_name FROM `variant` v where v.brand_id='$brand_id' and v.model_id='$model_id'  and v.status='1'")->result_array();
    }

    function all_fuel() {
        return $get_data = $this->db->query('select fuel_type_id,fuel_type from fuel_type')->result_array();
    }

    function getPaginatedCars($city, $type, $trans, $pric_min, $pric_max, $brand, $model_id, $fuel, $min, $min2, $q_type, $offset, $per_page) {
        $main_query = "";
        $main_query.= "SELECT v.pro_name_comp,v.pro_name,v.`variant_id`,v.`pro_image`,mex.mnc_exshowroom_price,mex.mnc_exshowroom_price,mex.total_benefits_perc,"
                . "(select `exshowroom_price` from `exshowroom_price` ex where v.`variant_id`=ex.`product_id` and ex.`city_id`='$city' limit 1) as exshowroom_price,"
                . "(select `brand_name` from `brand` where brand.brand_id=v.brand_id limit 1) as brand_name,"
                . "(select `brand_image` from `brand` where brand.brand_id=v.brand_id limit 1) as brand_image,"
                . "(select `model_name` from `model` where `model`.`model_id`=v.`model_id` limit 1) as model_name,"
                . "(select AVG(`total`) from `expert_rating` er where er.`pro_id`=v.`variant_id` limit 1) as exp_total,"
                . "(select AVG(`total`) from `user_rating` ur where ur.`pro_id`=v.`variant_id` limit 1) as usr_total ";

        $main_query.="FROM `variant` v, mnc_exshowroom_price mex where mex.product_id = v.variant_id and mex.city_id='$city'  and v.status='1' and ";

        if ($type != '' && $type != '-') {
            $main_query.=" v.`pro_type`='$type' and ";
        }
        if ($trans != '' && $trans != '-') {
            $main_query.=" v.`tramission_type`= '$trans' and ";
        } else {
            $main_query.="  ";
        }
        if ($pric_min != '' && $pric_max != '' && $pric_min != '-' && $pric_max != '-') {
            $main_query.=" mex.`mnc_exshowroom_price` between '$pric_min' and '$pric_max' and ";
        }
        if ($brand != '' && $brand != '-') {
            $main_query.=" v.`brand_id` =$brand and ";
        }
        if ($model_id != '' && $model_id != '-') {
            $main_query.=" v.`model_id` =$model_id and ";
        }
        if ($fuel != '' && $fuel != '-') {
//            $fuel = substr($fuel, 0, 3);
            $main_query.=" v.`fuel_type` =$fuel and ";
        }
        if ($min != '' && $min2 != '' && $min != '-' && $min2 != '-') {
            $main_query.=" v.`seating_capacity` between '$min' and '$min2' and ";
        }
        if ($q_type === 0) {
            $main_query.=" 1";
        } else {
            $main_query.=" 1 ORDER BY `variant_id` DESC limit $per_page OFFSET $offset";
        }
//        echo $main_query;
//        exit;

        return $this->db->query($main_query)->result_array();
    }

    function custom_login($signin_email, $signin_password) {
        $activity_data['user_last_logged_in'] = date('Y-m-d h:i:s');
        $activity_data['user_last_ip'] = $this->input->ip_address();
        $activity_data['type'] = 'custom';
        $signin_password = md5($signin_password);
        $query = $this->db->query("SELECT user_id,username,firstname,lastname,email FROM `user` where password='$signin_password' and (email='$signin_email' or username='$signin_email')");
        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            $track_detail = array(
                'user_id' => $res['user_id'],
                'ip' => $activity_data['user_last_ip'],
                'login_time' => $activity_data['user_last_logged_in'],
                'login_via' => $activity_data['type']
            );
            $this->db->insert('user_activity', $track_detail);
            return $res;
        }
    }

    function check_email($signin_email) {
        return $query = $this->db->query("SELECT user_id FROM `user` where email='$signin_email'")->result_array();
    }

    function update_unq_forget($forget_email, $uniue_id, $user_id) {
        $query = $this->db->query("SELECT user_id FROM `temp_forget` where email_id='$forget_email' and user_id='$user_id' and status='0'")->result_array();

        if (empty($query)) {
            return $query = $this->db->query("INSERT INTO `temp_forget`(`temp_id`, `user_id`, `email_id`, `random_key`, `added_date`, `status`) "
                    . "VALUES ('','$user_id','$forget_email','$uniue_id','','0')");
        } else {
            $querys = $this->db->query("update `temp_forget` set status='1' where user_id='$user_id'");
            return $query = $this->db->query("INSERT INTO `temp_forget`(`temp_id`, `user_id`, `email_id`, `random_key`, `added_date`, `status`) "
                    . "VALUES ('','$user_id','$forget_email','$uniue_id','','0')");
        }
    }

    function custome_user($regis_username, $regis_email, $regis_password, $id) {

        $mds_pass = md5($regis_password);
        $res = $this->db->query("select * from temp_user where email_id='$regis_email' and username='$regis_username' and status!='1'")->result_array();
        if (empty($res)) {
            $uniue_id = md5(uniqid($regis_username, true));
            $details = $this->db->query("INSERT INTO `temp_user`(`temp_id`, `username`, `email_id`, `password`, `random_key`,`status`) "
                    . "VALUES ('','$regis_username','$regis_email','$mds_pass','$uniue_id','0')");
            $id = $this->db->insert_id();
            return $res = $this->db->query("select * from temp_user where temp_id='$id' and email_id='$regis_email' and username='$regis_username' and status!='1'")->result_array();
        } else {

            return $res;
        }
    }

//    function product_detail($pro_id,$main_city){            
//                $que="SELECT v.pro_name,v.seating_capacity,v.pro_name_comp,v.city,v.fuel_type,v.tramission_type,v.body_color_id,v.pro_image,v.variant_id,v.pro_detail,m.model_name,v.pro_type,"
//                        . "(select avg(total) from user_rating where pro_id='$pro_id') as user_rating,"
//                        . "(select avg(total) from expert_rating where pro_id='$pro_id') as exp_rating,"
//                        . "(select mnc_exshowroom_price from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as mnc_price,"
//                        . "(select total_benefits from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits,"
//                        . "(select total_benefits_perc from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits_perc,"
//                        . "(select total_savings from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_savings,"
//                        . "(select exshowroom_price from exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as ex_price,"
//                        . "(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
//                        . "(select m.model_name from model m where m.model_id=v.model_id) as model_name "
//                        . "FROM `variant` v,`model` m "
//                        . "WHERE m.model_id=v.model_id and v.variant_id='$pro_id' and v.variant_id!=''  and v.status='1'";
//                return $query = $this->db->query($que)->result_array();            
//        
//    }
#### Home page featured product goes here #### 

    function featured($featured_type) {
        return $query = $this->db->query("SELECT f.`featured_id`, f.`prd_id_1`,f.`prd_id_2` FROM `featured` f WHERE f.`status`='1' and f.`featured_type`='$featured_type'")->result_array();
    }

    function top_selling_featured($featured_type) {
        $main_city = $this->input->cookie('main_city', TRUE);
//        echo '<pre>';
        $query = $this->db->query("SELECT f.`featured_id`, f.`prd_id_1`, f.`status`,v.`pro_name`,v.`pro_name_comp`,v.`pro_image`,(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,(select b.brand_id from brand b where b.brand_id=v.brand_id) as brand_id,(select m.model_name from model m where m.model_id=v.model_id) as model_name FROM `featured` f,`variant` v WHERE f.`status`='1' and f.`featured_type`='$featured_type' and f.`prd_id_1`=v.`variant_id` and v.status='1'")->result_array();

        $final = array();
        foreach ($query as $query_res) {
            $prd_id_1 = $query_res['prd_id_1'];
            $brand_id = $query_res['brand_id'];
            $deals_data = $this->db->query("SELECT d.*,b.start_date,d.end_date,v.pro_name_comp,v.pro_image FROM deal d,brand_deal b,variant v where d.brand_id='$brand_id' and d.brand_id=b.brand_id and b.city_id='$main_city' and v.variant_id=d.variant_id and v.variant_id='$prd_id_1'  and v.status='1' and Date(d.end_date) >= '" . date('Y-m-d') . "'")->row_array();
            $deal = array('deal' => $deals_data);
            $comb = array_merge($query_res, $deal);
            $final[] = $comb;
        }
//        echo '</pre>';
        return($final);

//        return $query;
    }

    function home_group_deal() {
        $main_city = $this->input->cookie('main_city', TRUE);
        $date = date('Y-m-d');
        return $query = $this->db->query("select f.prd_id_1,v.pro_name_comp,v.pro_name,v.pro_image,m.mnc_exshowroom_price,e.exshowroom_price from featured f,variant v,exshowroom_price e,mnc_exshowroom_price m where f.featured_type='top_selling_cars' and v.variant_id=e.product_id and v.variant_id=m.product_id and f.prd_id_1=v.variant_id and v.status='1'")->result_array();
    }

    function get_meta($cond, $id) {
        if ($cond == 1) {
            return $query = $this->db->query("select seo_keyword,seo_title,seo_description from variant where variant_id='$id'")->result_array();
        }
        if ($cond == 2) {
            return $query = $this->db->query("select seo_keyword,seo_title,seo_description from brand where brand_id='$id'")->result_array();
        }
    }

    function onraod_detail($pro_id) {
        $main_city = $this->input->cookie('main_city', TRUE);
        return $query = $this->db->query("select onroad_price from onroad_price where variant_id='$pro_id' and city_id='$main_city'")->result_array();
    }

//    function product_detail($pro_id, $comp_cond) {
//        if ($comp_cond == 1) {
//            $main_city = $this->input->cookie('main_city', TRUE);
//            if ($main_city == '') {
//                $main_city = "0";
//            }
//            if ($pro_id != '') {
//                $que = "SELECT v.pro_name,v.seating_capacity,v.pro_name_comp,v.city,v.fuel_type,v.tramission_type,v.body_color_id,v.pro_image,v.variant_id,v.pro_detail,m.model_name,v.pro_type,"
//                        . "(select avg(total) from user_rating where pro_id='$pro_id') as user_rating,"
//                        . "(select avg(total) from expert_rating where pro_id='$pro_id') as exp_rating,"
//                        . "(select mnc_exshowroom_price from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as mnc_price,"
//                        . "(select total_benefits from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits,"
//                        . "(select total_benefits_perc from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits_perc,"
//                        . "(select total_savings from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_savings,"
//                        . "(select exshowroom_price from exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as ex_price,"
//                        . "(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
//                        . "(select m.model_name from model m where m.model_id=v.model_id) as model_name "
//                        . "FROM `variant` v,`model` m "
//                        . "WHERE m.model_id=v.model_id and v.variant_id='$pro_id' and v.variant_id!=''  and v.status='1'";
//                return $query = $this->db->query($que)->result_array();
//            }
//        } else {
////            $this->db->where('variant_id', $pro_id);
////            $this->db->select('pro_name,pro_image,variant_id');
//            return $query = $this->db->query("select v.pro_name,v.pro_name_comp,v.pro_image,v.variant_id,(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,(select m.model_name from model m where m.model_id=v.model_id) as model_name from variant v where v.variant_id='$pro_id'  and v.status='1'")->result_array();
//        }
//    }














    function get_features_name($pro_ids, $id) {
        if ($id == 1) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 2) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 3) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 4) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 5) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 6) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        }
    }

    function product_variant_detail($id) {
        return $query = $this->db->query("SELECT * FROM `specification` WHERE variant_id='$id'")->result_array();
    }

    function product_color($color_id, $comp_cond) {
        if ($comp_cond == 2) {
            return $query = $this->db->query("SELECT `body_color_id`, `body_icon`, `color` FROM `body_color` WHERE `status`='1' and `body_color_id` IN ($color_id)")->result_array();
        }
    }

    function reting_product_detail($pro_id) {
//         and status='1'
        return $query = $this->db->query("SELECT pro_name_comp,pro_name,pro_image,variant_id,pro_detail FROM `variant` WHERE variant_id='$pro_id'  and status='1'")->result_array();
//        $this->db->where('variant_id', $pro_id);
//        $this->db->select('');
//        return $get_data = $this->db->get('variant')->result_array();
    }

    function expert_review_count($pro_id, $id) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select_avg('total');
        if ($id == 1) {
            return $get_data = $this->db->get('expert_rating')->result_array();
        }
        if ($id == 2) {
            return $get_data = $this->db->get('user_rating')->result_array();
        }
    }

    function ex_showroom_price($pro_id) {
        $main_city = $this->input->cookie('main_city', TRUE);
        $data.='';
        $data.="SELECT m.product_id,m.mnc_exshowroom_price,e.exshowroom_price FROM `mnc_exshowroom_price` m,`exshowroom_price` e "
                . "where m.product_id='$pro_id' and m.product_id=e.product_id";
        if ($main_city != '') {
            $data.=" and m.city_id='$main_city' and e.city_id='$main_city'";
        }

        return $query2 = $this->db->query($data)->result_array();
    }

    function mnc_ex_showroom_price($pro_id) {
        $data = '';
        $main_city = $this->input->cookie('main_city', TRUE);
        $data.="SELECT exshowroom_price FROM `exshowroom_price` where product_id='$pro_id'";
        if ($main_city != '') {
            $data.=" and city_id='$main_city'";
        }
        return $get_data = $this->db->query($data)->result_array();
    }

    function get_news() {
//        $this->db->where('product_id',$pro_id);
        $this->db->select('news_title,latest_news_id');
        $this->db->order_by("added_date", "desc");
        return $get_data = $this->db->get('news', '4')->result_array();
    }

    function get_allnews_details() {
        return $get_data = $this->db->get('news')->result_array();
    }

    function get_news_details($id) {
        if ($id != '') {
            $this->db->where('latest_news_id', $id);
        }
        $this->db->select('*');
        return $get_data = $this->db->get('news')->result_array();
    }

####get all brands related process here...####

//    function get_car_type(){
//        return $query = $this->db->query("SELECT * FROM `pro_type` ")->result_array();
//    }

####get home page carousel process here...####

    function home_page_carousel_exshow($product_id) {
        $main_city = $this->input->cookie('main_city', TRUE);
        $data.='';
        $data.="SELECT m.*,e.exshowroom_price FROM `mnc_exshowroom_price` m,`exshowroom_price` e "
                . "where m.product_id='$product_id' and m.product_id=e.product_id";
        if ($main_city != '') {
            $data.=" and m.city_id='$main_city' and e.city_id='$main_city'";
        }

        return $query2 = $this->db->query($data)->result_array();
    }

    function select_your_car_pro($brand_name) {
        return $query = $this->db->query("SELECT `brand_id`, `brand_name`, `brand_image`, `details` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
    }

    function select_your_car_model($brand_name) {
        $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
        $brand_id = $query[0]['brand_id'];
        return $model_result = $this->db->query("SELECT `model_name`,`model_id` FROM `model` WHERE `brand_id`='$brand_id'")->result_array();
    }

    function select_your_car_variant($brand_name) {
        $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
        $brand_id = $query[0]['brand_id'];
        $m_q = '';
        $main_city = $this->input->cookie('main_city', TRUE);
        $m_q.="SELECT `variant_id`,`pro_name` FROM `variant` WHERE  status='1' ";
        if ($brand_id != "") {
            $m_q.=" and `brand_id`='$brand_id'";
        }
        if ($main_city != "") {
            $m_q.=" and `city`='$main_city'";
        }
        return $variant_result = $this->db->query($m_q)->result_array();
    }

    function select_your_car_pro_details($brand_name, $cond, $main_city) {
        $m_q = '';
        if ($main_city == '') {
            $main_city = $this->input->cookie('main_city', TRUE);
        }
        $m_q.="SELECT distinct(v.variant_id),v.pro_name,v.pro_name_comp,v.pro_image,m.mnc_exshowroom_price,m.total_benefits_perc,e.exshowroom_price,m.total_benefits,m.total_savings,"
                . "(select AVG(`total`) from user_rating u where v.variant_id=u.pro_id) as user_total,"
                . "(select AVG(`total`) from expert_rating er where v.variant_id=er.pro_id) as expert_total "
                . "FROM `variant` v Left JOIN mnc_exshowroom_price m on m.product_id=v.variant_id Left JOIN exshowroom_price e on e.product_id=v.variant_id  WHERE 1=1 and v.status='1'";
        if ($main_city != "") {
            $m_q.=" and e.`city_id`='$main_city' and m.`city_id`='$main_city'  ";
        }
        if ($cond == 1) {
            $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
            $brand_id = $query[0]['brand_id'];
            $m_q.=" and v.`brand_id`='$brand_id'";
//            echo '<pre>';
//            print_r($m_q);
//            echo '</pre>';
            return $variant_result = $this->db->query($m_q)->result_array();
        } else if ($cond == 2) {
            $variant_id = $brand_name;
            $m_q.=" and v.`variant_id`='$variant_id'";
            return $variant_result = $this->db->query($m_q)->result_array();
        } else if ($cond == 3) {
            $model_id = $brand_name;
            $m_q.=" and v.`model_id`='$model_id'";
            return $variant_result = $this->db->query($m_q)->result_array();
//            return $variant_result = $this->db->query("SELECT v.variant_id,v.pro_name,v.pro_image,m.mnc_exshowroom_price,e.exshowroom_price FROM `variant` v Left JOIN mnc_exshowroom_price m on m.product_id=v.variant_id Left JOIN exshowroom_price e on e.product_id=v.variant_id  WHERE v.`model_id`='$model_id'")->result_array();
        }
//        echo $m_q;
    }

    function select_offers_product($brand, $cond) {
        if ($cond == 1) {
            $main_city = $this->input->cookie('main_city', TRUE);
            $brand_result = $this->db->query("SELECT brand_id,brand_name FROM `brand` where `brand_name`='$brand'")->result_array();
            $brand_id = $brand_result[0]['brand_id'];
            $offer_result = $this->db->query("SELECT * FROM `deal` where `brand_id`='$brand_id' and status='1' and city_id='$main_city'")->result_array();
            $final = array();
//        print_r($offer_result);
            foreach ($offer_result as $offer_result_data) {
                $variant_id = $offer_result_data['variant_id'];
                $variant_name = $this->db->query("SELECT pro_name FROM `variant` where `variant_id`='$variant_id'  and status='1'")->result_array();
                $final[] = array_merge($variant_name[0], $offer_result_data);
            }
            return $final;
        } else {
            $main_city = $this->input->cookie('main_city', TRUE);
            $brand_result = $this->db->query("SELECT brand_id,brand_name FROM `brand` where `brand_name`='$brand'")->result_array();
            $brand_id = $brand_result[0]['brand_id'];
            $offer_result = $this->db->query("SELECT * FROM `deal` where `brand_id`='$brand_id' and status='1' and city_id='$main_city'")->result_array();
            $final = array();
//        print_r($offer_result);
            foreach ($offer_result as $offer_result_data) {
                $variant_id = $offer_result_data['variant_id'];
                $variant_name = $this->db->query("SELECT pro_name FROM `variant` where `variant_id`='$variant_id'  and status='1'")->result_array();
                $final[] = array_merge($variant_name[0], $offer_result_data);
            }
            return $final;
        }
    }

    function pro_offer($pro_id, $user_id) {
        $main_city = $this->input->cookie('main_city', TRUE);
        return $res = $this->db->query("SELECT * FROM `deal` d where d.`variant_id`='$pro_id' and city_id='$main_city'")->result_array();
    }

    function pro_brand_offer($pro_id, $user_id) {
        $main_city = $this->input->cookie('main_city', TRUE);
        return $res = $this->db->query("SELECT * FROM `brand_deal` where city_id='$main_city'")->result_array();
    }

####get home page carousel process here...####
####get car type selection page process here...####

    function car_type_selection_brand() {
        return $query = $this->db->query("SELECT `brand_id`,`brand_name` FROM `brand` where status='1' ORDER BY brand_name")->result_array();
    }

    function car_type_selection_filters($id, $type, $pro_price, $brand, $fuel, $min, $min2, $city, $pric1, $pric2, $trans) {
        if ($city == '') {
            $city = $this->input->cookie('main_city', TRUE);
        }

        $main_query = "";
        $main_query.= "SELECT v.pro_name,v.brand_id,v.model_id,v.`variant_id`,v.`pro_image`,e.mnc_exshowroom_price,ex.exshowroom_price FROM `variant` v,`mnc_exshowroom_price` e,exshowroom_price ex where v.variant_id=e.product_id and v.variant_id=ex.product_id and  v.status='1'";
        if ($type != '') {
            $main_query.=" v.`pro_type`='$type' and ";
        } else {
            $main_query.=" v.`pro_type`!='$type' and ";
        }
        if ($trans != '') {
            $main_query.=" v.`tramission_type` IN ($trans) and ";
        } else {
            $main_query.="  ";
        }
        if ($pric1 != '' && $pric2 != '') {
            $main_query.=" e.`mnc_exshowroom_price` between '$pric1' and '$pric2' and ";
        }
        if ($brand != '') {
            $main_query.=" v.`brand_id` IN ($brand) and ";
        }
        if ($fuel != '') {
            $main_query.=" v.`fuel_type` IN ($fuel) and ";
        }
        if ($min != '' && $min2 != '') {
            $main_query.=" v.`seating_capacity` between '$min' and '$min2' and ";
        }
        if ($city != "") {
            $main_query.="e.`city_id`='$city' and ex.`city_id`='$city' and ";
        }
        $main_query.=" 1";
//          
        $query = $this->db->query($main_query)->result_array();
        $array = array();
        foreach ($query as $res) {
            $brand_id = $res["brand_id"];
            $variant_id = $res["variant_id"];
            $brand_res = $this->db->query("SELECT brand_image FROM `brand` where brand_id='$brand_id'")->result_array();
            if (empty($brand_res)) {
                $brand_res[0] = array('brand_image' => '');
            } else {
                $brand_res[0];
            }

            $user_rating = $this->db->query("SELECT AVG(`total`) as user_total FROM `user_rating` where pro_id='$variant_id'")->result_array();
            if (empty($user_rating)) {
                $user_rating[0] = array('user_total' => '');
            } else {
                $user_rating[0];
            }

            $expert_rating = $this->db->query("SELECT AVG(`total`) as expert_total FROM `expert_rating` where pro_id='$variant_id'")->result_array();
//                print_r($expert_rating);
            if (empty($expert_rating)) {
                $expert_rating[0] = array('expert_total' => '');
            } else {
                $expert_rating[0];
            }

            $model_id = $res["model_id"];
            $model_res = $this->db->query("SELECT model_name FROM `model` where model_id='$model_id'")->result_array();
            if (empty($model_res)) {
                $model_res[0] = array('model_name' => '');
            } else {
                $model_res[0];
            }

            $combine = array_merge($res, $brand_res[0], $model_res[0], $user_rating[0], $expert_rating[0]);
            $array[] = $combine;
        }
    }

####get car type selection page process here...####
####get car introduction page details here...####

    function car_intro_gallery_detail($id, $type) {
//        echo $id;
        if ($type == 'All') {
            return $car_intro_detail = $this->db->query("SELECT * FROM `product_gallery` where prduct_id='$id'")->result_array();
        } else {
            return $car_intro_detail = $this->db->query("SELECT image,product_gallery_id,video FROM `product_gallery` where prduct_id='$id' and page='$type'")->result_array();
        }
    }

    function car_intro_user_reviews($id) {
        return $car_intro_detail = $this->db->query("SELECT r.`total`,r.`what_i_like`,r.`what_i_dont_like`,u.`firstname` FROM `user_rating` r,`user` u where u.`user_id`=r.`user_id` and r.`pro_id`='$id' order by `user_rating_id` DESC limit 1")->result_array();
    }

    function car_intro_expert_reviews($id) {
        return $car_intro_detail = $this->db->query("SELECT `expert_rating_id`,`journilist_name`,`what_i_like`,`what_i_dont_like`,`total` FROM `expert_rating` WHERE `pro_id`='$id' ORDER BY `expert_rating_id` DESC limit 1")->result_array();
    }

    function car_intro_exshowroom_price($id) {
        $city = $this->input->cookie('main_city', TRUE);
        return $car_intro_detail = $this->db->query("SELECT m.*,e.`exshowroom_price`,m.`delivery_date` FROM `mnc_exshowroom_price` m,`exshowroom_price` e WHERE m.`product_id`='$id' and m.city_id='$city' and e.city_id='$city' and m.product_id=e.product_id")->result_array();
//        $car_intro_detail2=$this->db->query("SELECT `exshowroom_price` FROM `exshowroom_price` WHERE `product_id`='$id' and city_id='$city'")->result_array();
//        return $final=  array_merge($car_intro_detail,$car_intro_detail2);        
    }

    function car_variant_details($id) {
//        echo $id;
        $car_intro_detail = $this->db->query("SELECT model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id`='$id'  and status='1'")->result_array();
        foreach ($car_intro_detail as $car_intro_detail_data)
            $model_id = $car_intro_detail_data['model_id'];
        $brand_id = $car_intro_detail_data['brand_id'];
//        $pro_name = $car_intro_detail_data['pro_name'];
        $city = $this->input->cookie('main_city', TRUE);
        $car_variant_detail = $this->db->query("SELECT v.*,s.mileage,s.engine_maximum_power FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and  s.variant_id=v.variant_id  and v.status='1'")->result_array();
//        print_r($car_variant_detail);
//        exit;
        $merge_data = array();
        foreach ($car_variant_detail as $car_variant_detail_data) {
            $variant_id = $car_variant_detail_data['variant_id'];
            $car_variant_detail = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id' and city_id='$city'")->result_array();
            $car_variant_detail2 = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id' and city_id='$city'")->result_array();
            $merge_data[] = array_merge($car_variant_detail, $car_variant_detail_data, $car_variant_detail2);
        }
//print_r($merge_data);
        return $merge_data;
    }

    function filter_car_variant_details($id, $trimline, $engine, $transmission) {

//        if ($trimline != '') {
//            $select_acc = rtrim($trimline, ",");
//            $car_intro_detail = "SELECT model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id` ='$select_acc'";
//            $car_intro_detail = $this->db->query($car_intro_detail)->result_array();
//        } else {
        $car_intro_detail = $this->db->query("SELECT model_id,brand_id,pro_name,pro_name_comp,pro_type,variant FROM `variant` WHERE `variant_id`='$id' and status='1'")->result_array();
//        }
//            print_r($car_intro_detail);
        foreach ($car_intro_detail as $car_intro_detail_data) {
            $model_id = $car_intro_detail_data['model_id'];
            $brand_id = $car_intro_detail_data['brand_id'];
            $pro_name = $car_intro_detail_data['pro_name'];
            $city = $this->input->cookie('main_city', TRUE);
            $car_variant_detail = '';
            $car_variant_detail.="SELECT v.*,s.mileage,s.engine_maximum_power FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and s.variant_id=v.variant_id";
            if ($engine != '') {
                $car_variant_detail.=" and v.fuel_type='$engine'";
            }
            if ($transmission != '') {
                $transmission;
                $car_variant_detail.=" and v.tramission_type like '%$transmission%'";
            }
            if ($select_acc != '') {
                $car_variant_detail.=" and v.variant_id='$select_acc'";
            }
//          echo $car_variant_detail;
//          exit;
            $merge_data[] = '';
            $car_variant_details = $this->db->query($car_variant_detail)->result_array();
            foreach ($car_variant_details as $car_variant_detail_data) {
                $variant_id = $car_variant_detail_data['variant_id'];
                $ex_price = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id' and city_id='$city'")->result_array();
                $mncex_price = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id' and city_id='$city'")->result_array();
                $merge_data[] = array_merge($car_variant_detail_data, $ex_price, $mncex_price);
            }
        }
        return $merge_data;
    }

####get car introduction page details here...####
#####get booking details here...####

    function booking_detail($pro_id, $user_id, $cond) {
        if ($cond == 1) {
            $city = $this->input->cookie('main_city', TRUE);
            $variant_type_detail = $this->db->query("SELECT variant_type FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $variant_type = explode(',', $variant_type_detail[0]['variant_type']);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                if ($variant_type_data != '') {
                    $ids = "'" . $variant_type_data . "'," . $ids;
                }
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
            $car_intro_detail = $this->db->query("SELECT variant_id,model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id` IN ($ids)")->result_array();
            foreach ($car_intro_detail as $car_intro_detail_data) {
                $model_id = $car_intro_detail_data['model_id'];
                $brand_id = $car_intro_detail_data['brand_id'];
                $pro_name = $car_intro_detail_data['pro_name'];
                $variant_id = $car_intro_detail_data['variant_id'];
                $car_variant_detail = '';
                $car_variant_detail.="SELECT v.*,s.mileage,s.engine_maximum_power,(select f.fuel_type from fuel_type f where f.fuel_type_id=v.fuel_type) as fuel_type FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and v.`pro_name`='$pro_name' and s.variant_id=v.variant_id and v.variant_id='$variant_id'  and v.status='1'";
                $merge_data[] = '';
                $car_variant_details = $this->db->query($car_variant_detail)->result_array();
                foreach ($car_variant_details as $car_variant_detail_data) {
                    $variant_id = $car_variant_detail_data['variant_id'];
                    $ex_price = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id' and city_id='$city'")->row_array();
                    $mncex_price = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id' and city_id='$city'")->row_array();
                    $merge_data[] = array_merge($car_variant_detail_data, $ex_price, $mncex_price);
                }
                return $merge_data;
            }
        } else if ($cond == 2) {
            $variant_type_detail = $this->db->query("SELECT color,wheel FROM `car_configuration` WHERE `product_id`='$pro_id'  and user_id='$user_id'")->result_array();
            $color = $variant_type_detail[0]['color'];
            $wheel = $variant_type_detail[0]['wheel'];
            return $car_intro_detail = $this->db->query("SELECT f.*,fv.* FROM `features` f,`feature_variant_mapping` fv where f.feature_id IN ('$color','$wheel') and fv.feature_id=f.feature_id and fv.variant_id='$pro_id'")->result_array();
        } else if ($cond == 3) {
            $variant_type_detail = $this->db->query("SELECT leather,cloth,dashboard FROM `car_configuration` WHERE `product_id`='$pro_id'  and user_id='$user_id'")->result_array();
            $leather = $variant_type_detail[0]['leather'];
            $cloth = $variant_type_detail[0]['cloth'];
            $dashboard = $variant_type_detail[0]['dashboard'];
            return $car_intro_details = $this->db->query("SELECT f.*,fv.* FROM `features` f,`feature_variant_mapping` fv where f.feature_id IN ('$leather','$cloth','$dashboard') and fv.feature_id=f.feature_id and fv.variant_id='$pro_id'")->result_array();
        } else if ($cond == 4) {
            $variant_type_detail = $this->db->query("SELECT accessory FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $variant_type = explode(',', $variant_type_detail[0]['accessory']);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                $ids = "'" . trim($variant_type_data) . "'," . $ids;
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
            return $car_intro_details = $this->db->query("SELECT a.*,av.* FROM `accessory` a,`accessory_variant_mapping` av where av.accessory_id IN ($ids) and av.accessory_id=a.accessory_id and av.variant_id='$pro_id'")->result_array();
        } else if ($cond == 5) {
            $variant_type_detail = $this->db->query("SELECT finance_id,amc_id,insurance_id,insurance_id,extended_id,used_id FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $ids = $variant_type_detail[0]['finance_id'] . ',' . $variant_type_detail[0]['amc_id'] . ',' . $variant_type_detail[0]['insurance_id'] . ',' . $variant_type_detail[0]['extended_id'] . ',' . $variant_type_detail[0]['used_id'];
            $variant_type = explode(',', $ids);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                if ($variant_type_data != '') {
                    $ids = "'" . trim($variant_type_data) . "'," . $ids;
                }
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
//
            return $car_intro_details = $this->db->query("SELECT a.*,av.* FROM `accessory` a,`accessory_variant_mapping` av where av.accessory_id IN ($ids) and av.accessory_id=a.accessory_id and av.variant_id='$pro_id'")->result_array();
        } else if ($cond == 6) {
            $car_intro_details = $this->db->query("SELECT `color`,`wheel`,`leather`,`cloth`,`dashboard` "
                            . "FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id' and `status`=''")->row_array();
            return ($car_intro_details['color'] . ',' . $car_intro_details['wheel'] . ',' . $car_intro_details['leather'] . ',' . $car_intro_details['cloth'] . ',' . $car_intro_details['dashboard']);
        }
    }

#####get booking details here...####
##### On road price calucaltion ####

    function onroad_price_calculation($pro_id, $user_id, $cond) {
        if ($cond == 1) {
            $car_intro_details = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and user_id='$user_id'")->result_array();
            if (!empty($car_intro_details)) {
                $color = $car_intro_details[0]['color'];
                $wheel = $car_intro_details[0]['wheel'];
                $variant_type = $car_intro_details[0]['variant_type'];
                $leather = $car_intro_details[0]['leather'];
                $cloth = $car_intro_details[0]['cloth'];
                $dashboard = $car_intro_details[0]['dashboard'];
                $car_color_price = $this->db->query("SELECT price FROM `feature_variant_mapping` where feature_id IN ('$color','$wheel','$leather','$cloth','$dashboard')")->result_array();
                $price = '';
                foreach ($car_color_price as $car_color_price_data) {
                    $car_color_price_data['price'];
                    $price = $price + $car_color_price_data['price'];
                }
                return $price;
            }
        } else if ($cond == 2) {

            $car_intro_details = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and user_id='$user_id'")->result_array();
            if (!empty($car_intro_details)) {

                $accessory = $car_intro_details[0]['accessory'];
                $variant_type = explode(',', $accessory);
                $ids = '';
                foreach ($variant_type as $variant_type_data) {
                    $ids = "'" . $variant_type_data . "'," . $ids;
                }
                $ids = substr($ids, 0, -1);

                $car_color_price = $this->db->query("SELECT price FROM `feature_variant_mapping` where feature_id IN ($ids)")->result_array();
                $price = '';
                foreach ($car_color_price as $car_color_price_data) {
                    $car_color_price_data['price'];
                    $price = $price + $car_color_price_data['price'];
                }
                return $price;
            }
        }
    }

##### On road price calucaltion ####
#

    function additional_info($id, $user_id) {
        $car_intro_detail = $this->db->query("SELECT * FROM `car_configuration` WHERE `product_id`='$id' and user_id='$user_id' and status!='2'")->result_array();
        if (!empty($car_intro_detail)) {
            $ids = "'" . $car_intro_detail[0]['color'] . "'," . "'" . $car_intro_detail[0]['wheel'] . "'," . "'" . $car_intro_detail[0]['leather'] . "'," . "'" . $car_intro_detail[0]['cloth'] . "'," . "'" . $car_intro_detail[0]['dashboard'] . "'";
            return $caluclation = $this->db->query("SELECT sum(price) as price FROM `feature_variant_mapping` WHERE `feature_id` IN ($ids) and variant_id='$id'")->result_array();
        }
    }

    function accessory_details_res($id, $user_id) {
        $car_intro_detail = $this->db->query("SELECT * FROM `car_configuration` WHERE `product_id`='$id' and user_id='$user_id' and status!='2'")->result_array();
        if (!empty($car_intro_detail)) {
            $ids = $car_intro_detail[0]['accessory'];
            $selected_data = explode(',', $ids);
            $res = '';
            for ($i = 0; $i <= count($selected_data) - 2; $i++) {
                $res = $res . "'" . $selected_data[$i] . "',";
            }
            $select_acc = rtrim($res, ",");
            if ($select_acc == '') {
                $select_acc = "''";
            }
            return $caluclation = $this->db->query("SELECT sum(price) as price FROM `accessory_variant_mapping` WHERE `accessory_id` IN ($select_acc) and variant_id='$id'")->result_array();
        }
    }

    function onraod_calculation($id, $user_id) {
//    echo $id;    
        $main_city = $this->input->cookie('main_city', TRUE);
        return $caluclation = $this->db->query("SELECT * FROM `onroad_price` WHERE `city_id`='$main_city' and variant_id='$id'")->result_array();
    }

#
####Change Car start here...####

    function comparison_model_data($brand_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `model` WHERE `brand_id`='$brand_data'")->result_array();
    }

    function comparison_variant_data($brand_data, $brand_model) {
        return $car_intro_detail = $this->db->query("SELECT variant_id,pro_name,fuel_type,pro_name_comp FROM `variant` WHERE `model_id`='$brand_model' and `brand_id`='$brand_data'  and status='1'")->result_array();
    }

    function get_fuel() {
        return $fuel_ytpe = $this->db->query("select * from fuel_type")->result_array();
    }

####Change Car start here...####
#####Change exterior_selection here...####

    function car_color($color_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `body_color` WHERE `body_color_id` IN($color_data)")->result_array();
    }

    function car_wheel($wheel_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `wheel` WHERE `wheel_id` IN($wheel_data)")->result_array();
//    echo $wheel_data;    
    }

#####Change exterior_selection here...####
######Change configuration accessory here...####

    function car_exterior($id, $pro_id) {
        return $car_intro_detail = $this->db->query("SELECT m.*,a.* FROM `accessory_variant_mapping` m,`accessory` a where a.accessory_id=m.accessory_id and m.variant_id='$pro_id' and a.accessory_cat='$id'")->result_array();
    }

    function car_exterior_selected($user_id, $pro_id) {
        return $car_intro_detail = $this->db->query("SELECT accessory,accessory_total FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and status!='2'")->result_array();
    }

######Change configuration accessory here...####
###### Selected Car Configuration Details ######

    function config_selected_detail($pro_id, $user_id) {
        return $data = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and status!='2'")->result_array();
    }

###### Selected Car Configuration Details ######
#### Get brand & model wise variants ####

    function get_model_wise_variants($model, $brand_id) {
//        return '0';
        return $data = $this->db->query("SELECT variant,variant_id,pro_name FROM `variant` where model_id='$model' and brand_id='$brand_id'  and status='1'")->result_array();
    }

#### Get brand & model wise variants ####
#### Get variant id wise video ####

    function get_product_wise_video($variant_video) {
        return $data = $this->db->query("SELECT video FROM  `product_gallery` where prduct_id='$variant_video' and page='Video'")->result_array();
    }

#### Get variant id wise video ####
#### Order Status Start####

    function order_confirm($pro_id, $user_id, $tot_price, $tot_days, $fewamaount) {
//      $discount_price = $this->input->cookie('discount_price',TRUE);
        $on_road_price = $this->input->cookie('on_road_price', TRUE);
        $group_discount = $this->input->cookie('group_discount', TRUE);

        $main_city = $this->input->cookie('main_city', TRUE);
        $data2 = "SELECT c.user_id,c.car_configuration_id,c.product_id,c.invoice_no,c.color,(select f.feature_desc from features f  where  f.feature_id = c.color) as feature_desc FROM `car_configuration` c where c.user_id='$user_id' and c.product_id='$pro_id' and c.status!='2'";
        $data = $this->db->query($data2)->result_array();
        if (!empty($data)) {
            $car_configuration_id = $data[0]['car_configuration_id'];
            $invoice_no = $data[0]['invoice_no'];
            $product_id = $data[0]['product_id'];
            $check = $this->db->query("select `order_id` from `order` where product_id='$pro_id' and user_id='$user_id' and city_id='$main_city' and configuration_id='$car_configuration_id'");
            $res = $check->result_array();
            $count_order = $check->num_rows();
            if ($count_order == 0) {
                $insert_order = $this->db->query("INSERT INTO `order`(`paid_amount`,`product_id`,`city_id`,`user_id`,`invoice_no`,`configuration_id`,"
                        . "`dis_price`,`tot_price`,`days`,`on_road_price`)"
                        . " VALUES ('$fewamaount','$pro_id','$main_city','$user_id','$invoice_no','$car_configuration_id','$group_discount','$tot_price','$tot_days','$on_road_price')");
                $order_id = $this->db->insert_id();
                $update_config = $this->db->query("UPDATE `car_configuration` SET `status`='2' WHERE car_configuration_id='$car_configuration_id'");
                $update_emi = $this->db->query("UPDATE `emi_calculation` SET `order_id`='$order_id' WHERE pro_id='$pro_id' and user_id='$user_id'");
            }
        }
        $order_details = $this->db->query("select o.`order_id`,o.dis_price,o.`user_id`,o.`configuration_id`,o.`invoice_no`,o.`product_id`,o.on_road_price,v.pro_name_comp,o.tot_price,v.pro_image,(select c.name from city c  where c.city_id=o.city_id) as city_name from `order` o,variant v where o.user_id='$user_id' and o.city_id='$main_city' and o.product_id=v.variant_id order by o.order_id DESC limit 1")->row_array();
        $user_details = $this->db->query("select firstname,email from `user` where user_id='$user_id'")->row_array();

        $final_res = array_merge($order_details, $user_details);
        return ($final_res);
    }

    function order_confirm_mail($order_id, $id) {
        if ($id == 1) {
            return $details = $this->db->query("select *  from `order` WHERE order_id='$order_id'")->num_rows();
        } else {
            return $details = $this->db->query("UPDATE `order` SET `e_status`='1' WHERE order_id='$order_id'");
        }
    }

    function order_wise_detail($order_id) {
        return $details = $this->db->query("select product_id,city_id,user_id,configuration_id,dis_price,on_road_price,(select name from city c where c.city_id=order.city_id) as city_name from `order` where order_id='$order_id'")->result_array();
    }

    function car_intro_detail($id) {
        return $car_intro_detail = $this->db->query("SELECT v.pro_name,v.pro_name_comp,v.wheel_id,v.body_color_id,v.variant_id,v.pro_detail,v.pro_image,(select brand_name from brand where brand_id=v.brand_id) as brand_name,(select model_name from model where model_id=v.model_id) as model_name FROM `variant` v where v.variant_id='$id'  and v.status='1'")->result_array();
    }

    function dependencies_res($select_acc) {
        return $details = $this->db->query("select accessory_id,dependent_accessory_id,dependency_type,dependent_accessory_name from `accessory_dependencies_mapping` where accessory_id IN ($select_acc)")->result_array();
    }

#### Order Status End####
#### Order Status End####
#### Order Status End####

    function check_username($regis_username, $a) {
        if ($a == 1) {
            return $details = $this->db->query("select user_id from user where username='$regis_username'")->result_array();
        }
        if ($a == 2) {
            return $details = $this->db->query("select user_id from user where email='$regis_username'")->result_array();
        }
    }

    function activate_user($id) {
//        echo $res = "select * from temp_user where status='0' and random_key='$id'";
        $res = $this->db->query("select * from temp_user where status='0' and random_key='$id'")->result_array();
        $res_data = array();
        if (!empty($res)) {
            $temp_id = $res[0]['temp_id'];
            $username = $res[0]['username'];
            $email_id = $res[0]['email_id'];

            $password = $res[0]['password'];
            $res_data[] = $username;
            $res_data[] = $email_id;
            $res_data[] = $password;
            $random_key = $res[0]['random_key'];
            $res2 = $this->db->query("select * from user where username='$username' and email='$email_id' and password='$password'");
            if (!empty($res2)) {
                $res1 = $this->db->query("INSERT INTO `user`(`username`,`firstname`,`password`,`email`) VALUES ('$username','$username','$password','$email_id')");
                $upd = $this->db->query("UPDATE `temp_user` SET `status`='1' WHERE temp_id='$temp_id'");
            }
        }
        return $res_data;
    }

    function get_change_user($id) {
        return $upd = $this->db->query("SELECT * FROM `temp_forget` where random_key='$id' and status='0'")->result_array();
    }

    function bank_per() {
        return $upd = $this->db->query("SELECT * FROM `bank_per`")->result_array();
    }

    function select_config_breadcrub($pro_id, $user_id, $data_cookies) {
        return $upd = $this->db->query("SELECT * FROM `car_configuration` where user_id='$user_id' and product_id='$pro_id' and user_cookie='$data_cookies' and status!='2'")->result_array();
    }

    function product_comp_color($pro_id) {
        $upd = $this->db->query("SELECT distinct(feature_id) FROM `feature_variant_mapping` where variant_id='$pro_id'")->result_array();
        $id = '';
//    $upd=array_unique($upd);
        foreach ($upd as $upd_res) {
            $feature_id = $upd_res['feature_id'];
            $id = $id . "'" . $feature_id . "',";
        }
        $ids = substr($id, 0, -1);
        if ($ids == '') {
            $ids = "''";
        }
        return $all_res = $this->db->query("SELECT feature_name,feature_desc,feature_img FROM `features` where feature_id IN ($ids) and feature_name='Color'")->result_array();

//    echo '<br>';        
    }

    function product_comp_feature($id) {
//   echo '<pre>';       

        $upd = $this->db->query("SELECT distinct(accessory_id) FROM `accessory_variant_mapping` where variant_id='$id'")->result_array();
        $id = '';
        foreach ($upd as $upd_res) {
            $feature_id = $upd_res['accessory_id'];
            $id = $id . "'" . $feature_id . "',";
        }
        $ids = substr($id, 0, -1);
        return $all_res = $this->db->query("SELECT accessory_id,accessory_cat,accessory_sub_cat,accessory_name FROM `accessory` where accessory_id IN ($ids) order by accessory_sub_cat,accessory_cat ASC")->result_array();
//   echo '</pre>';
    }

    function add_emi($emi, $pro_id, $user_id, $down_payment, $loan1, $bank, $rate1, $months1) {
        $del = $this->db->query("delete from  `emi_calculation` where `pro_id`='$pro_id' and `user_id`='$user_id'");
        $upd = $this->db->query("INSERT INTO `emi_calculation`(`emi_id`, `emi_amount`, `pro_id`, `user_id`, `down_payment`, `load_amaount`, `bank`, `int_rate`, `tenure`, `added_date`) "
                . "VALUES ('','$emi','$pro_id','$user_id','$down_payment','$loan1','$bank','$rate1','$months1','')");

        return $order_id = $this->db->insert_id();
        ;
    }

    function select_emi($pro_id, $user_id) {
        return $del = $this->db->query("SELECT * FROM `emi_calculation` where `pro_id`='$pro_id' and `user_id`='$user_id' and order_id=''")->result_array();
    }

    function get_emi_record($emi_id, $prod_id) {
        return $del = $this->db->query("SELECT * FROM `emi_calculation` where emi_id='$emi_id' and pro_id='$prod_id'")->result_array();
    }

    function load_saved_price($brand_data) {
        $brand_data;
        $del = $this->db->query("SELECT variant_id FROM `variant` where `brand_id`='$brand_data'  and status='1'")->result_array();
        $data = '';
        foreach ($del as $del_data) {
            $variant_id = $del_data['variant_id'];
            $data = $data . "'" . $variant_id . "',";
        }
        $data = rtrim($data, ",");
        if (empty($data)) {
            $data = "''";
        }
        $sel = $this->db->query("SELECT d.*,e.mnc_exshowroom_price FROM `deal` d,`mnc_exshowroom_price` e where d.`variant_id` IN ($data) and d.variant_id=e.product_id")->result_array();

        $disocunts = '';
        foreach ($sel as $sel_data) {
            $pos = strpos($sel_data['amount_per'], '%');
            if ($pos === false) {
                $discount = $sel_data['amount_per'];
                $disocunts = $disocunts + $discount;
//    echo 'not';
            } else {
                $data = rtrim($sel_data['amount_per'], "%");
                $discount = $sel_data['mnc_exshowroom_price'] * ($data / 100);
                $disocunts = $disocunts + $discount;
//    echo 'yes';
            }
        }
        echo $disocunts;
    }

    function select_offers_brand($brand_id) {
        $main_city = $this->input->cookie('main_city', TRUE);
        $sel = '';
        $sel.="SELECT * FROM brand_deal where 1=1";
        if ($brand_id != '') {
            $sel.=" and brand_id='$brand_id'";
        }
        if ($main_city != '') {
            $sel.=" and city_id='$main_city'";
        }
        $sel.=" order by deal_id limit 1";
//        echo $sel;
        return $sel = $this->db->query($sel)->result_array();
    }

    function search_car_res($id) {
        return $sel = $this->db->query("select v.variant_id,v.pro_name,v.pro_image,pro_name_comp from variant v where v.pro_name_comp like '%$id%' and v.status='1' order by v.variant_id LIMIT 5")->result_array();
    }

    function search_user_res($id) {
        return $sel = $this->db->query("select user_id,firstname from user where firstname like '%$id%' order by user_id LIMIT 5")->result_array();
    }

    function customer_address($user_id) {
        return $sel = $this->db->query("select * from user where user_id ='$user_id'")->result_array();
    }

    function select_buyers_bought($brand_id) {
//$main_city=$this->input->cookie('main_city', TRUE); 
//echo $total_sell="SELECT COUNT(*) as total  FROM `order` where city_id='$main_city'"; 
        $total_sell = $this->db->query("SELECT COUNT(*) as total  FROM `order`")->result_array();

        $totalsell = $total_sell[0]['total'];
        $brand_wise_sell = $this->db->query("SELECT COUNT(*) as B_total FROM `order` o,`variant` v where v.variant_id=o.product_id and v.brand_id='$brand_id'  and v.status='1'")->result_array();
        $totalbrandwise = $brand_wise_sell[0]['B_total'];
        return $totalbrandwise . '/' . $totalsell;
    }

    function select_overall_deals($brand_id) {
        $main_city = $this->input->cookie('main_city', TRUE);
        $total_sell = $this->db->query("SELECT variant_id,discount_type,amount_per,start_date,end_date FROM `deal` where city_id='$main_city' and brand_id='$brand_id'")->result_array();
        $q = "";
        $res = array();
        foreach ($total_sell as $total_sell_data) {
            if ($total_sell_data['discount_type'] == '1') {
                $product_id = $total_sell_data['variant_id'];
                $q.="SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$product_id' ";
                if ($main_city != '') {
                    $q.="and city_id='$main_city'";
                }
//echo $q;
                $total_price = $this->db->query($q)->result_array();
                $total_sell_data = array_merge($total_sell_data, $total_price[0]);
            } else {
                $total_price[0] = array('mnc_exshowroom_price' => '');
                $total_sell_data = array_merge($total_sell_data, $total_price[0]);
            }
            $res[] = $total_sell_data;
            unset($q);
        }
//print_r($res);
//echo '</pre>';
        return ($res);
    }

    function customer_details($user_id) {
        return $user = $this->db->query("SELECT username,firstname,pincode,lastname,email,state,address,telephone,city  FROM `user` where user_id='$user_id'")->result_array();
    }

    function registeration_details($user_id) {
        return $user = $this->db->query("SELECT * FROM `shipping_detail` where user_id='$user_id'")->result_array();
    }

    function car_configuration($user_id, $id, $cond) {
        if ($cond == '1') {
            return $user = $this->db->query("SELECT *  FROM `car_configuration` where user_id='$user_id' and product_id='$id' and status!='2'")->result_array();
        } else {
            return $user = $this->db->query("SELECT *  FROM `car_configuration` where user_id='$user_id' and product_id='$id' ")->result_array();
        }
    }

    function car_emi($id, $user_id) {
        return $user = $this->db->query("SELECT *  FROM `emi_calculation` where user_id='$user_id' and pro_id='$id'")->result_array();
    }

    /*     * *** product specification **** */



    /*     * *** forum content **** */

    function forum_brand_id($brand) {
        return $spec = $this->db->query("SELECT brand_id,brand_name  FROM `brand` where brand_name='$brand'")->result_array();
    }

    function forum_brand_count($brand_id, $cond) {
        $spec = $this->db->query("SELECT user_id  FROM `join_group` where brand_id='$brand_id'");
        if ($cond == '1') {
            return $spec->num_rows();
        } else {
            $spec = $spec->result_array();
//print_r($spec);
            $usersdata = '';
            foreach ($spec as $spec_data) {
                $usersdata = "'" . $spec_data['user_id'] . "'," . $usersdata;
//echo $user_id=$spec_data['user_id'];    
//$users=$this->db->query("SELECT firstname,fb_id  FROM `user` where user_id='$user_id'")->result_array();        
//if(!empty($users))
//{  
////$firsname=$users[0]['firstname'];    
////$fb_id=$users[0]['fb_id'];    
////$users
//$usersdata[]=$users;
//}  
            }
//echo rtim($usersdata;
            $trans = substr($usersdata, 0, -1);
            $users = "SELECT firstname,fb_id  FROM `user` where user_id IN ($trans)";
            $users = $this->db->query("SELECT firstname,fb_id  FROM `user` where user_id IN ($trans)")->result_array();
//print_r($users);
            return ($users);
        }
    }

    function check_brand_join($brand_id, $ses_id) {
//echo $brand_id;    
//echo $ses_id;   
        return $count_res = $this->db->query("SELECT * FROM `join_group` where user_id='$ses_id' and brand_id='$brand_id'")->num_rows();
    }

    function group_deal_brands() {
        $count_res = $this->db->query("SELECT distinct(`brand_id`) as brand_id FROM `brand_deal`")->result_array();
        $array_data = array();
        foreach ($count_res as $count_res_data) {
            $brand_id = $count_res_data['brand_id'];
            $data = $this->db->query("SELECT brand_name FROM `brand` where brand_id='$brand_id'")->result_array();
            $array_data[] = $data;
//print_r($data);
        }
        return ($array_data);
    }

    function upload_profile($img_name) {
        $exp = explode('.', $img_name);
        $profile = $exp[0];

//print_r($exp);
//exit;
        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];

//echo $data="UPDATE `user` SET `fb_id`='$profile' WHERE `user_id`='$user_id'";
//exit;
        return $data = $this->db->query("UPDATE `user` SET `fb_id`='$profile' WHERE `user_id`='$user_id'");
    }

    function upload_cover($img_name) {
        $exp = explode('.', $img_name);
        $cover = $exp[0];

        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        return $data = $this->db->query("UPDATE `user` SET `cover`='$cover' WHERE `user_id`='$user_id'");
    }

    function order_user_detail($user_id) {
        return $data = $this->db->query("select * from `user` WHERE `user_id`='$user_id'")->result_array();
    }

    function order_billing_details($user_id, $id) {
        if ($id == 1) {
            return $data = $this->db->query("SELECT `firstname`, `lastname`, `email`, `telephone`, `address`, `city`, `state`, `pincode`, `country` FROM `user` WHERE `user_id`='$user_id'")->result_array();
        }
        if ($id == 2) {
            return $data = $this->db->query("SELECT * FROM `shipping_detail` WHERE `user_id`='$user_id'")->result_array();
        }
    }

    function shiping_detail($user_id) {
        return $data = $this->db->query("SELECT * FROM `shipping_detail` WHERE `user_id`='$user_id'")->result_array();
    }

    function deal_data() {
        $main_city = $this->input->cookie('main_city', TRUE);
        $data = $this->db->query("SELECT brand_name,brand_id FROM brand where status='1'")->result_array();
        $final = array();
        foreach ($data as $data_res) {
            $date = date('Y-m-d');
            $brand_id = $data_res['brand_id'];
            $deals_data = $this->db->query("SELECT d.*,b.start_date,d.end_date,v.pro_name_comp,v.pro_image,"
                            . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                            . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                            . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                            . " FROM deal d,brand_deal b,variant v where d.brand_id='$brand_id' and d.brand_id=b.brand_id and b.city_id='$main_city' and v.variant_id=d.variant_id and Date(d.end_date) >= '" . date('Y-m-d') . "'  and v.status='1'")->result_array();
            $deal = array('deal' => $deals_data);
            $merge = array_merge($data_res, $deal);
            $final[] = $merge;
        }

        return ($final);
    }

    function hot_deal_data() {
        $sort = $_GET['sort'];
        $type = $_GET['type'];
        $brand = $_GET['brand'];
        if ($brand != '') {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where brand_name='$brand'")->row_array();
            $brand_id = "'" . $brand['brand_id'] . "'";
        } else {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where status='1'")->result_array();
            $res = '';
            foreach ($brand as $brand_res) {
                $res = $res . "'" . $brand_res['brand_id'] . "',";
            }
            $brand_id = rtrim($res, ",");
        }
        $main_city = $this->input->cookie('main_city', TRUE);
        $res = array();
        $main_data = '';
        $main_data.="SELECT h.*,v.pro_name_comp,v.pro_image,"
                . "(select brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                . " FROM hot_deals h,variant v where v.variant_id=h.variant_id and v.brand_id IN ($brand_id)  and v.status='1'";
        if ($type != '') {
            $main_data.=" and v.pro_type='$type'";
        }

        $main_data.=" order by brand_id ASC";
        $main_data;
        $data = $this->db->query($main_data)->result_array();
        return $data;
    }

    /*
      function hot_deal_data()
      {
      $main_city=$this->input->cookie('main_city', TRUE);
      $brand=$this->db->query("SELECT brand_name,brand_id from brand where status='1'")->result_array();
      //    echo '<pre>';
      $res=array();
      foreach($brand as $brand_data)
      {
      $brand_id=$brand_data['brand_id'];
      $brand_data=$brand_data['brand_name'];
      $brand_name=array('brand_name'=>$brand_data);
      $data=$this->db->query("SELECT h.*,v.pro_name_comp,v.pro_image,(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price FROM hot_deals h,variant v where v.variant_id=h.variant_id and v.brand_id='$brand_id'")->result_array();
      $hot_deal_data=array('hot_deal'=>$data);
      $combine_data=array_merge($brand_name,$hot_deal_data);
      $res[]=$combine_data;
      }
      return $res;
      //    print_r($res);
      //    echo '</pre>';
      }
     */

    function hot_deal_data2() {
        $sort = $_GET['sort'];
        $type = $_GET['type'];
        $brand = $_GET['brand'];
        if ($brand != '') {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where brand_name='$brand'")->row_array();
            $brand_id = "'" . $brand['brand_id'] . "'";
        } else {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where status='1'")->result_array();
            $res = '';
            foreach ($brand as $brand_res) {
                $res = $res . "'" . $brand_res['brand_id'] . "',";
            }
            $brand_id = rtrim($res, ",");
        }
        $main_city = $this->input->cookie('main_city', TRUE);
        $res = array();
        $main_data = '';
        $main_data.="SELECT h.*,v.pro_name_comp,v.pro_image,"
                . "(select brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                . "(select m.total_savings from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_savings,"
                . "(select m.total_benefits_perc from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits_perc,"
                . "(select m.finance_benefit from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as finance_benefit,"
                . "(select m.accessory_package from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as accessory_package,"
                . "(select m.extended_warranty from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as extended_warranty,"
                . "(select m.accessory_discount from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as accessory_discount,"
                . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                . " FROM hot_deals h,variant v where v.variant_id=h.variant_id and v.brand_id IN ($brand_id)  and v.status='1'";
        if ($type != '') {
            $main_data.=" and v.pro_type='$type'";
        }

        $main_data.=" order by brand_id ASC";
        $main_data;
        $data = $this->db->query($main_data)->result_array();
        return $data;
    }

    /*
      function hot_deal_data2()
      {
      $sort=$_GET['sort'];
      $brand=$_GET['brand'];
      if($brand!='')
      {
      $brand=$this->db->query("SELECT brand_name,brand_id from brand where brand_name='$brand'")->result_array();
      }
      else {
      $brand=$this->db->query("SELECT brand_name,brand_id from brand where status='1'")->result_array();
      }
      //    print_r($brand);
      $type=$_GET['type'];
      $main_city=$this->input->cookie('main_city', TRUE);

      $res=array();

      //    print_r($brand);
      //    exit;
      echo '<pre>';
      foreach($brand as $brand_data)
      {
      $brand_id=$brand_data['brand_id'];
      $brand_data=$brand_data['brand_name'];
      $brand_name=array('brand_name'=>$brand_data);
      //    print_r($brand_data);
      $data.="SELECT h.*,v.pro_name_comp,v.pro_image,"
      . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
      . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
      . " FROM hot_deals h,variant v where v.variant_id=h.variant_id and v.brand_id='$brand_id'";
      if($type!='')
      {
      $data.=" and v.pro_type='$type'";
      }
      //    echo $data;
      $data_res=$this->db->query($data)->result_array();

      if(!empty($data_res))
      {
      $hot_deal_data=array('hot_deal'=>$data_res);
      $combine_data=array_merge($brand_name,$hot_deal_data);
      $res[]=$combine_data;
      }
      /*

      //    $data;
      $data=$this->db->query($data)->result_array();


      //    $data=$this->db->query("SELECT h.*,v.pro_name_comp,v.pro_image,"
      //            . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
      //            . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
      //            . " FROM hot_deals h,variant v where v.variant_id=h.variant_id and v.brand_id='$brand_id'")->result_array();


      }
      //    print_r($data_res);
      //    echo '</pre>';
      //    echo '</pre>';
      //    print_r($res);
      //    echo '</pre>';
      //    exit;
      //    return $res;

      }
     */

    function getbrandwisemodel($brand) {
        return $this->db->query("select * from model where brand_id='$brand'")->result_array();
    }

    function booking_product_features($pro_id) {
        return $sel = $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$pro_id'  and v.status='1'")->result_array();
    }

    function order_invoice_update($car_configuration_id) {
        $invoice = 'INV-MNC-' . $car_configuration_id;
        $this->db->query("UPDATE `car_configuration` SET `invoice_no` = '$invoice' WHERE `car_configuration_id`='$car_configuration_id'");
        $this->db->query("UPDATE `order` SET `invoice_no` = '$invoice' WHERE `configuration_id`='$car_configuration_id'");
        return $invoice;
    }

    function hot_deal_data4() {
        $city = $_GET['city'];
        $car = $_GET['car'];
        $main_city = $city;
        if ($main_city == '') {
            $main_city = $this->input->cookie('main_city', TRUE);
        }

        $res = array();
        $main_data = '';
        $main_data.="SELECT v.variant_id,v.pro_name_comp,v.pro_image,v.pro_detail,"
                . "(select name from city c where c.city_id='$city') as city_name,"
                . "(select brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                . " FROM variant v where 1  and v.status='1' ";
        if ($car != '') {
            $main_data.=" and v.variant_id='$car'";
        }

        $main_data.=" order by brand_id ASC";
//    echo $main_data;
        $data = $this->db->query($main_data)->result_array();
        return $data;
    }

    function home_seo_data($content, $id) {
        if ($id == 1) {
            return $get = $this->db->query("SELECT * FROM  `seo_tag` where page='$content' limit 0,1")->row_array();
        } else if ($id == 2) {
            return $get = $this->db->query("SELECT * FROM  `seo_tag` where page='$content' limit 1,1")->row_array();
        } else if ($id == 3) {
            return $get = $this->db->query("SELECT * FROM  `seo_tag` where page='$content' limit 1,1")->row_array();
        } else if ($id == 4) {
//    echo $content;    
//    echo $get="SELECT * FROM  `seo_tag` where page='$content' limit 1,1";     
            return $get = $this->db->query("SELECT * FROM  `seo_tag` where page='$content'")->row_array();
        }
    }

    function get_test_drive_data($test_brand_data, $test_brand_model, $test_car_variant) {
        return $get = $this->db->query("SELECT pro_name_comp,(select brand_name from brand where variant.brand_id=brand.brand_id and brand_id='$test_brand_data') as brand_name,(select model_name from model where variant.model_id=model.model_id and model_id='$test_brand_model') as model_name FROM `variant` where variant_id='$test_car_variant'")->row_array();
    }

    function get_product() {
        return $get = $this->db->query("SELECT pro_name_comp,variant_id FROM `variant` where status='1'")->result_array();
    }

}
