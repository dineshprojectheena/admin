<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brand_section extends CI_Model {
    
    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }
    
     function select_your_car_pro($brand_name) {
        return $query = $this->db->query("SELECT `brand_id`, `brand_name`, `brand_image`, `details` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
    }
    
    function select_your_car_model($brand_name) {
        $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
        $brand_id = $query[0]['brand_id'];
        return $model_result = $this->db->query("SELECT `model_name`,`model_id` FROM `model` WHERE `brand_id`='$brand_id' and status='1' ORDER BY `model_name` ASC")->result_array();
    }
    
    function select_buyers_bought($brand_id) {
//$main_city=$this->common_city();     
//echo $total_sell="SELECT COUNT(*) as total  FROM `order` where city_id='$main_city'"; 
        $total_sell = $this->db->query("SELECT COUNT(*) as total  FROM `order`")->result_array();

        $totalsell = $total_sell[0]['total'];
        $brand_wise_sell = $this->db->query("SELECT COUNT(*) as B_total FROM `order` o,`variant` v where v.variant_id=o.product_id and v.brand_id='$brand_id'  and v.status='1'")->result_array();
        $totalbrandwise = $brand_wise_sell[0]['B_total'];
        return $totalbrandwise . '/' . $totalsell;
    }
    
    function select_overall_deals($brand_id) {
        $main_city = $this->common_city();
        $total_sell = $this->db->query("SELECT variant_id,discount_type,amount_per,start_date,end_date FROM `deal` where city_id='$main_city' and brand_id='$brand_id'")->result_array();
        $q = "";
        $res = array();
        foreach ($total_sell as $total_sell_data) {
            if ($total_sell_data['discount_type'] == '1') {
                $product_id = $total_sell_data['variant_id'];
                $q.="SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$product_id' ";
                if ($main_city != '') {
                    $q.="and city_id='$main_city'";
                }
//echo $q;
                $total_price = $this->db->query($q)->result_array();
                $total_sell_data = array_merge($total_sell_data, $total_price[0]);
            } else {
                $total_price[0] = array('mnc_exshowroom_price' => '');
                $total_sell_data = array_merge($total_sell_data, $total_price[0]);
            }
            $res[] = $total_sell_data;
            unset($q);
        }
//print_r($res);
//echo '</pre>';
        return ($res);
    }
    
     function select_your_car_variant($brand_name) {
        $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
        $brand_id = $query[0]['brand_id'];
        $m_q = '';
        $main_city = $this->common_city();
        $m_q.="SELECT `variant_id`,`pro_name` FROM `variant` WHERE  status='1' ";
        if ($brand_id != "") {
            $m_q.=" and `brand_id`='$brand_id'";
        }
        if ($main_city != "") {
            $m_q.=" and `city`='$main_city'";
        }
        return $variant_result = $this->db->query($m_q)->result_array();
    }
    
    function select_your_car_pro_details($brand_name, $cond, $main_city, $brand_id2) {
        if (!empty($_GET['model'])) {
            $_GET['model'];
            $model = str_replace('_', ' ', $_GET['model']);
        }
//        echo $model;
        if ($main_city == '') {
            $main_city = $this->common_city();
        }
        $query = $this->db->query("SELECT `brand_id` FROM `brand` WHERE `brand_name`='$brand_name'")->result_array();
        $brand_id = $query[0]['brand_id'];
        if (!empty($model)) {
            $model = "SELECT `model_name`,`model_id` FROM `model` WHERE `model_name`='$model' and status='1' ORDER BY `model_name` ASC";
        } else {
            $model = "SELECT `model_name`,`model_id` FROM `model` WHERE `brand_id`='$brand_id' and status='1' ORDER BY `model_name` ASC";
        }
        $model_res = $this->db->query($model)->result_array();
        $i = 1;
//        $all_res = array();

        foreach ($model_res as $model_res_data) {
            $model_id = $model_res_data['model_id'];
            $model_name = $model_res_data['model_name'];
            $m_q.="SELECT distinct(v.variant_id),v.pro_name,v.pro_name_comp,v.pro_image,m.mnc_exshowroom_price,m.total_benefits_perc,e.exshowroom_price,m.total_benefits,m.total_savings,"
                    . "(select AVG(`total`) from user_rating u where v.variant_id=u.pro_id) as user_total,"
//                . "(select model_name from model md where md.model_id=v.model_id) as model_name,"
                    . "(select AVG(`total`) from expert_rating er where v.variant_id=er.pro_id) as expert_total "
                    . "FROM `variant` v Left JOIN mnc_exshowroom_price m on m.product_id=v.variant_id Left JOIN exshowroom_price e on e.product_id=v.variant_id  WHERE 1=1 and v.status='1' and v.model_id='$model_id'";
            if ($main_city != "") {
                $m_q.=" and e.`city_id`='$main_city' and m.`city_id`='$main_city'  ";
            }
            if ($cond == 1) {
                if (empty($brand_id2)) {
                    $m_q.=" and v.`brand_id`='$brand_id'";
                } else {
                    $m_q.=" and v.`brand_id`='$brand_id2'";
                }
            }
            if ($cond == 1) {
                if (empty($brand_id2)) {
                    $m_q.=" and v.`brand_id`='$brand_id'";
                } else {
                    $m_q.=" and v.`brand_id`='$brand_id2'";
                }
            } else if ($cond == 2) {
                $variant_id = $brand_name;
                $m_q.=" and v.`variant_id`='$variant_id'";
            }
            $variant_result = $this->db->query($m_q)->result_array();

            $combine_res[] = array_merge($variant_result, $model_res_data);
//            $all_res[] = $combine_res;
            unset($m_q);
            $i++;
        }
        return $combine_res;
//        echo '<pre>';
//        print_r($combine_res);
//        echo '</pre>';
    }
    
    
    
    function select_offers_product($brand, $cond) {
        if ($cond == 1) {
            $main_city = $this->common_city();
            $brand_result = $this->db->query("SELECT brand_id,brand_name FROM `brand` where `brand_name`='$brand'")->result_array();
            $brand_id = $brand_result[0]['brand_id'];
            $offer_result = $this->db->query("SELECT * FROM `deal` where `brand_id`='$brand_id' and status='1' and city_id='$main_city'")->result_array();
            $final = array();
//        print_r($offer_result);
            foreach ($offer_result as $offer_result_data) {
                $variant_id = $offer_result_data['variant_id'];
                $variant_name = $this->db->query("SELECT pro_name FROM `variant` where `variant_id`='$variant_id'  and status='1'")->result_array();
                $final[] = array_merge($variant_name[0], $offer_result_data);
            }
            return $final;
        } else {
            $main_city = $this->common_city();
            $brand_result = $this->db->query("SELECT brand_id,brand_name FROM `brand` where `brand_name`='$brand'")->result_array();
            $brand_id = $brand_result[0]['brand_id'];
            $offer_result = $this->db->query("SELECT * FROM `deal` where `brand_id`='$brand_id' and status='1' and city_id='$main_city'")->result_array();
            $final = array();
//        print_r($offer_result);
            foreach ($offer_result as $offer_result_data) {
                $variant_id = $offer_result_data['variant_id'];
                $variant_name = $this->db->query("SELECT pro_name FROM `variant` where `variant_id`='$variant_id'  and status='1'")->result_array();
                $final[] = array_merge($variant_name[0], $offer_result_data);
            }
            return $final;
        }
    }
    
    function select_offers_brand($brand_id) {
        $main_city = $this->common_city();
        $sel = '';
        $sel.="SELECT * FROM brand_deal where 1=1";
        if ($brand_id != '') {
            $sel.=" and brand_id='$brand_id'";
        }
        if ($main_city != '') {
            $sel.=" and city_id='$main_city'";
        }
        $sel.=" order by deal_id limit 1";
//        echo $sel;
        return $sel = $this->db->query($sel)->result_array();
    }
    
    function get_meta($cond, $id) {
        if ($cond == 1) {
            return $query = $this->db->query("select seo_keyword,seo_title,seo_description from variant where variant_id='$id'")->result_array();
        }
        if ($cond == 2) {
            return $query = $this->db->query("select seo_keyword,seo_title,seo_description from brand where brand_id='$id'")->result_array();
        }
    }
    
    
    
}
