<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comparision_section extends CI_Model {
    
     public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    function product_detail($pro_id, $comp_cond) {
        if ($comp_cond == 1) {
            $main_city = $this->common_city();
            if ($main_city == '') {
                $main_city = "0";
            }
            if ($pro_id != '') {
                $que = "SELECT v.pro_name,v.seating_capacity,v.pro_name_comp,v.city,v.fuel_type,v.tramission_type,v.body_color_id,v.pro_image,v.variant_id,v.pro_detail,m.model_name,v.pro_type,"
                        . "(select avg(total) from user_rating where pro_id='$pro_id') as user_rating,"
                        . "(select avg(total) from expert_rating where pro_id='$pro_id') as exp_rating,"
                        . "(select mnc_exshowroom_price from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as mnc_price,"
                        . "(select total_benefits from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits,"
                        . "(select total_benefits_perc from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits_perc,"
                        . "(select total_savings from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_savings,"
                        . "(select exshowroom_price from exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as ex_price,"
                        . "(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                        . "(select m.model_name from model m where m.model_id=v.model_id) as model_name "
                        . "FROM `variant` v,`model` m "
                        . "WHERE m.model_id=v.model_id and v.variant_id='$pro_id' and v.variant_id!=''  and v.status='1'";
                return $query = $this->db->query($que)->result_array();
            }
        } else {
            return $query = $this->db->query("select v.pro_name,v.pro_name_comp,v.pro_image,v.variant_id,(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,(select m.model_name from model m where m.model_id=v.model_id) as model_name from variant v where v.variant_id='$pro_id'  and v.status='1'")->result_array();
        }
    }

    function product_features($pro_id, $user_id) {
        return $sel = $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$pro_id'")->result_array();
    }
    
    
    function product_comp_color($pro_id) {
        $upd = $this->db->query("SELECT distinct(feature_id) FROM `feature_variant_mapping` where variant_id='$pro_id'")->result_array();
        $id = '';
        foreach ($upd as $upd_res) {
            $feature_id = $upd_res['feature_id'];
            $id = $id . "'" . $feature_id . "',";
        }
        $ids = substr($id, 0, -1);
        if ($ids == '') {
            $ids = "''";
        }
        return $all_res = $this->db->query("SELECT feature_name,feature_desc,feature_img FROM `features` where feature_id IN ($ids) and feature_name='Color'")->result_array();
    }
    
    
    function product_variant_detail($id) {
        return $query = $this->db->query("SELECT * FROM `specification` WHERE variant_id='$id'")->result_array();
    }
    
    function get_features_name($pro_ids, $id) {
        if ($id == 1) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 2) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 3) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 4) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 5) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 6) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        }
    }
    

}
