<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_section extends CI_Model {
    
    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }
    
    
    function car_type_selection_brand() {
        return $query = $this->db->query("SELECT `brand_id`,`brand_name` FROM `brand` where status='1' ORDER BY brand_name")->result_array();
    }
    
    
    function getPaginatedCars($city, $type, $trans, $pric_min, $pric_max, $brand, $model_id, $fuel, $min, $min2, $q_type, $offset, $per_page) {
        $main_query = "";
//            $main_query.= "SELECT DISTINCT(v.`variant_id`),v.pro_name_comp,v.pro_name,v.`pro_image`,e.mnc_exshowroom_price,(select `exshowroom_price` from `exshowroom_price` ex where v.`variant_id`=ex.`product_id` and ex.`city_id`='$city' limit 1) as exshowroom_price,(select `brand_name` from `brand` where brand.brand_id=v.brand_id limit 1) as brand_name,(select `brand_image` from `brand` where brand.brand_id=v.brand_id limit 1) as brand_image,(select `model_name` from `model` where `model`.`model_id`=v.`model_id` limit 1) as model_name,(select AVG(`total`) from `expert_rating` er where er.`pro_id`=v.`variant_id` limit 1) as exp_total,(select AVG(`total`) from `user_rating` ur where ur.`pro_id`=v.`variant_id` limit 1) as usr_total FROM `variant` v ,mnc_exshowroom_price e where v.variant_id=e.product_id and ";
        $main_query.= "SELECT v.pro_name_comp,v.pro_name,v.`variant_id`,v.`pro_image`,mex.mnc_exshowroom_price,mex.mnc_exshowroom_price,mex.total_benefits_perc,"
//            . "(select `mnc_exshowroom_price` from `mnc_exshowroom_price` me where v.`variant_id`=me.`product_id` and  me.`city_id`='$city'  limit 1) as mnc_exshowroom_price,"
                . "(select `exshowroom_price` from `exshowroom_price` ex where v.`variant_id`=ex.`product_id` and ex.`city_id`='$city' limit 1) as exshowroom_price,"
                . "(select `brand_name` from `brand` where brand.brand_id=v.brand_id limit 1) as brand_name,"
                . "(select `brand_image` from `brand` where brand.brand_id=v.brand_id limit 1) as brand_image,"
                . "(select `model_name` from `model` where `model`.`model_id`=v.`model_id` limit 1) as model_name,"
                . "(select AVG(`total`) from `expert_rating` er where er.`pro_id`=v.`variant_id` limit 1) as exp_total,"
                . "(select AVG(`total`) from `user_rating` ur where ur.`pro_id`=v.`variant_id` limit 1) as usr_total ";

        $main_query.="FROM `variant` v, mnc_exshowroom_price mex where mex.product_id = v.variant_id and mex.city_id='$city'  and v.status='1' and ";

        if ($type != '') {
            $main_query.=" v.`pro_type`='$type' and ";
        }
        if ($trans != '') {
            $main_query.=" v.`tramission_type`= '$trans' and ";
        } else {
            $main_query.="  ";
        }
        if ($pric_min != '' && $pric_max != '') {
            $main_query.=" mex.`mnc_exshowroom_price` between '$pric_min' and '$pric_max' and mex.`mnc_exshowroom_price`>'0' and ";
        }
        if ($brand != '') {
            $main_query.=" v.`brand_id` =$brand and ";
        }
        if ($model_id != '') {
            $main_query.=" v.`model_id` =$model_id and ";
        }
        if ($fuel != '') {
//            $fuel = substr($fuel, 0, 3);
            $main_query.=" v.`fuel_type` =$fuel and ";
        }
        if ($min != '' && $min2 != '') {
            $main_query.=" v.`seating_capacity` between '$min' and '$min2' and ";
        }


        if ($q_type === 0) {
            $main_query.=" 1";
        } else {
            $main_query.=" 1 ORDER BY `variant_id` DESC limit $per_page OFFSET $offset";
        }
//        echo $main_query;
//        exit;

        return $this->db->query($main_query)->result_array();
    }
    
    function getbrandwisemodel($brand) {
        return $this->db->query("select * from model where brand_id='$brand' and status='1'")->result_array();
    }
    
    
    
}
