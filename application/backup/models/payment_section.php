<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_section extends CI_Model {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    function product_configuration($pro_id, $color, $user_id, $emi_amount, $car_finance, $car_insurance, $extended_waranty, $car_exchange, $home_delivery, $corporate_dis, $loyality_bonus, $emi_id, $final_total_saving_price, $final_mnc_on_road_price) {
        $check = $this->db->query("SELECT * FROM `car_configuration` where  product_id='$pro_id' and status!='2' and user_id='$user_id' order by car_configuration_id DESC limit 1")->result_array();
        
        $last_id = $this->db->query("SELECT car_configuration_id FROM `car_configuration` ORDER BY `car_configuration_id` DESC LIMIT 1")->row_array();
        
        $inv_last_id = $last_id['car_configuration_id'];
        $inv_id = 'INV-MNC-'.($inv_last_id + 1);
        
        $count = count($check);
        if ($count == 0) {
            $res = "INSERT INTO `car_configuration`(`invoice_no`,`color`,`variant_type`,`product_id`,`user_id`,`introduction`,`user_cookie`,"
                    . "`emi_amount`,`finance_id`,`added_date`,`amc_id`,`insurance_id`,`extended_id`,`used_id`,`home_delivery`,`corporate_discount`,`loyality_bonus`,`emi_id`,`total_saving`,`on_road_price`)"
                    . " VALUES ('$inv_id','$color','$pro_id','$pro_id','$user_id','1','',"
                    . "'$emi_amount','$car_finance',NOW(),'','$car_insurance','$extended_waranty','$car_exchange','$home_delivery','$corporate_dis','$loyality_bonus','$emi_id','$final_total_saving_price','$final_mnc_on_road_price')";
            
            $res = $this->db->query("INSERT INTO `car_configuration`(`invoice_no`,`color`,`variant_type`,`product_id`,`user_id`,`introduction`,`user_cookie`,"
                    . "`emi_amount`,`finance_id`,`added_date`,`amc_id`,`insurance_id`,`extended_id`,`used_id`,`home_delivery`,`corporate_discount`,`loyality_bonus`,`emi_id`,`total_saving`,`on_road_price`)"
                    . " VALUES ('$inv_id','$color','$pro_id','$pro_id','$user_id','1','',"
                    . "'$emi_amount','$car_finance',NOW(),'','$car_insurance','$extended_waranty','$car_exchange','$home_delivery','$corporate_dis','$loyality_bonus','$emi_id','$final_total_saving_price','$final_mnc_on_road_price')");
            $inv_last_id = $this->db->insert_id();
        } else {
            $res = $this->db->query("UPDATE `car_configuration` SET user_cookie='',`user_id`='$user_id',color='$color',`variant_type`='$pro_id',`product_id`='$pro_id',"
                    . "introduction='1',emi_amount='$emi_amount',finance_id='$car_finance',insurance_id='$car_insurance',extended_id='$extended_waranty'"
                    . ",used_id='$car_exchange',added_date=NOW(),home_delivery='$home_delivery',corporate_discount='$corporate_dis',loyality_bonus='$loyality_bonus',emi_id='$emi_id',total_saving='$final_total_saving_price',on_road_price='$final_mnc_on_road_price' "
                    . " where `product_id`='$pro_id' and car_configuration_id='$inv_last_id' and status!='2'");
            $inv_last_id;
        }
        if (!empty($inv_last_id)) {
            return $last_id = $this->db->query("SELECT * FROM `car_configuration` where car_configuration_id='$inv_last_id' ORDER BY `car_configuration_id` DESC LIMIT 1")->row_array();
        }
    }
    
    function booking_amount($pro_id)
    {
      return $booking_amount=$this->db->query("SELECT variant_id,booking_amount from variant where variant_id='$pro_id'")->row_array();  
    }   

    function car_intro_detail($id) {
        return $car_intro_detail = $this->db->query("SELECT v.pro_name,v.pro_name_comp,v.wheel_id,v.body_color_id,v.variant_id,v.pro_detail,v.pro_image,(select brand_name from brand where brand_id=v.brand_id) as brand_name,(select model_name from model where model_id=v.model_id) as model_name FROM `variant` v where v.variant_id='$id'  and v.status='1'")->result_array();
    }

    function car_intro_exshowroom_price($id) {
        $city = $this->common_city();
        return $car_intro_detail = $this->db->query("SELECT m.*,e.`exshowroom_price`,m.`delivery_date` FROM `mnc_exshowroom_price` m,`exshowroom_price` e WHERE m.`product_id`='$id' and m.city_id='$city' and e.city_id='$city' and m.product_id=e.product_id")->result_array();
    }

    function order_billing_details($user_id, $id) {
        if ($id == 1) {
            return $data = $this->db->query("SELECT `firstname`, `lastname`, `email`, `telephone`, `address`, `city`, `state`, `pincode`, `country` FROM `user` WHERE `user_id`='$user_id'")->result_array();
        }
        if ($id == 2) {
            return $data = $this->db->query("SELECT * FROM `shipping_detail` WHERE `user_id`='$user_id'")->result_array();
        }
    }

    function user_address_update($name, $l_name, $telephone, $city, $state, $postal_code, $country, $address, $user_id) {
        $query = $this->db->query("UPDATE `user` set firstname='$name',lastname='$l_name',`telephone`='$telephone',`city`='$city',`state`='$state',`pincode`='$postal_code',`last_profile_updated`=NOW(),"
                . "`country`='$country',`address`='$address' WHERE user_id='$user_id'");
        return $check = $this->db->query("select user_id from `shipping_detail` WHERE user_id='$user_id'")->num_rows();
    }

    function user_shipping_address($ship_email, $ship_name, $ship_l_name, $user_id, $ship_phone, $ship_city, $ship_state, $ship_postal_code, $ship_country, $ship_address, $date, $check,$city) {
        if ($check == 0) {
            $insert = $this->db->query("INSERT INTO `shipping_detail`(`shipping_id`,`email`,`name`,`lastname`,`user_id`,`telephone`, `city`, `state`, `pincode`, `country`, `address`, `date_added`) "
                    . "VALUES ('','$ship_email','$ship_name',`city`='$city','$ship_l_name','$user_id','$ship_phone','$ship_city','$ship_state','$ship_postal_code','$ship_country','$ship_address','$date')");
            return 'insert';
        } else {
            $update = $this->db->query("UPDATE `shipping_detail` set `name`='$ship_name',`lastname`='$ship_l_name',`telephone`='$ship_phone',`state`='$ship_state',`pincode`='$ship_postal_code',"
                    . "`country`='$ship_country',`city`='$city',`address`='$ship_address',`email`='$ship_email' WHERE user_id='$user_id'");
            return 'update';
        }
    }

    function order_configuration($id) {
//        if(!empty($id)){
        return $last_id = $this->db->query("SELECT * FROM `car_configuration` where car_configuration_id='$id' ORDER BY `car_configuration_id` DESC LIMIT 1")->row_array();
//        }
    }

    function order_confirm($conf_id, $pro_id, $user_id) {
        $res_conf = $this->order_configuration($conf_id);
        
        $booking_amt=$this->booking_amount($pro_id);
        
        if ($pro_id == $booking_amt['variant_id']) {
            if ($booking_amt['booking_amount'] > 0) {
                $booking_price = $booking_amt['booking_amount'];
            } else {
                $booking_price = trans_amt;
            }
        }
        
//        
//        print_r($booking_amt);
//        exit;
//        $fewamaount = trans_amt;
        $exp_cook_coupon_id = explode('_', $cook_coupon_id);
        $main_city = $this->common_city();
        $ref_id = $this->input->cookie('ref_id', TRUE);
        $data2 = "SELECT c.* FROM `car_configuration` c where c.user_id='$user_id' and c.product_id='$pro_id' and c.status!='2' order by c.car_configuration_id DESC limit 1";
        $data = $this->db->query($data2)->result_array();
        $car_configuration_id = $data[0]['car_configuration_id'];
        $invoice_no = $data[0]['invoice_no'];
        $product_id = $data[0]['product_id'];
        $on_road_price = $data[0]['on_road_price'];
        $date_time = date('Y-m-d h:i:s');
        if(!empty($car_configuration_id)){
        $check = $this->db->query("select `order_id` from `order` where product_id='$pro_id' and user_id='$user_id' and configuration_id='$car_configuration_id'");
        $res = $check->result_array();
        $count_order = $check->num_rows();
        if($count_order == 0){
            $insert_order = $this->db->query("INSERT INTO `order`(`paid_amount`,`product_id`,`city_id`,`user_id`,`invoice_no`,`configuration_id`,"
                    . "`dis_price`,`tot_price`,`days`,`on_road_price`,`ref_id`,`added_date`)"
                    . " VALUES ('$booking_price','$pro_id','$main_city','$user_id','$invoice_no','$car_configuration_id',"
                    . "'','','','$on_road_price','$ref_id','$date_time')");
            $order_id = $this->db->insert_id();
            $update_config = $this->db->query("UPDATE `car_configuration` SET `status`='2' WHERE car_configuration_id='$car_configuration_id'");
            $update_emi = $this->db->query("UPDATE `emi_calculation` SET `order_id`='$order_id' WHERE pro_id='$pro_id' and user_id='$user_id'");

            if ($pro_id == $exp_cook_coupon_id[0] && !empty($exp_cook_coupon_id[1])) {
                $coupon_id = $exp_cook_coupon_id[1];
                $select_coupon = $this->db->query("select * from `coupon` where coupon_id='$coupon_id'")->row_array();
                $coupon_id = $select_coupon['coupon_id'];
                $start_date = $select_coupon['start_date'];
                $end_date = $select_coupon['end_date'];
                $coupon_type = $select_coupon['coupon_type'];
                $coupon_material = $select_coupon['coupon_material'];
                $amount_percent = $select_coupon['amount_percent'];
                $coupn_code = $select_coupon['coupn_code'];
                $insert_coupon = $this->db->query("INSERT INTO `order_coupon` SET `order_id` = '$order_id',`coupon_id` = '$coupon_id',`start_date` = '$start_date',`end_date` = '$end_date',`coupon_type` = '$coupon_type',`coupon_material` = '$coupon_material',`amount_percent` = '$amount_percent',`coupn_code` = '$coupn_code'");
            }
        }
        }
//        }

//        echo "select o.`order_id`,o.dis_price,o.`user_id`,o.`configuration_id`,o.`invoice_no`,o.`product_id`,o.on_road_price,v.pro_name_comp,o.tot_price,v.pro_image,(select c.name from city c  where c.city_id=o.city_id) as city_name from `order` o,variant v where o.user_id='$user_id' and o.city_id='$main_city' and o.product_id='$pro_id' and o.product_id=v.variant_id order by o.order_id DESC limit 1";
        $order_details = $this->db->query("select o.paid_amount,o.`order_id`,o.dis_price,o.`user_id`,o.`configuration_id`,o.`invoice_no`,o.`product_id`,o.on_road_price,v.pro_name_comp,o.tot_price,v.pro_image,(select c.name from city c  where c.city_id=o.city_id) as city_name from `order` o,variant v where o.user_id='$user_id' and o.city_id='$main_city' and o.product_id='$pro_id' and o.invoice_no!='' and o.product_id=v.variant_id order by o.order_id DESC limit 1")->row_array();
        $user_details = $this->db->query("select firstname,email from `user` where user_id='$user_id'")->row_array();
        $final_res = array_merge($order_details, $user_details);

        return ($final_res);
    }

    function customer_details($user_id) {
        return $user = $this->db->query("SELECT username,firstname,pincode,lastname,email,state,address,telephone,city  FROM `user` where user_id='$user_id'")->result_array();
    }

    function registeration_details($user_id) {
        return $user = $this->db->query("SELECT * FROM `shipping_detail` where user_id='$user_id'")->result_array();
    }

    function car_configuration($user_id, $id, $cond, $invoice_no = '') {
        if ($cond == '1') {
            return $user = $this->db->query("SELECT *  FROM `car_configuration` where user_id='$user_id' and product_id='$id' and status!='2'")->result_array();
        } else {
            return $user = $this->db->query("SELECT *  FROM `car_configuration` where user_id='$user_id' and product_id='$id' and invoice_no='$invoice_no'")->result_array();
        }
    }

    function coupon_used($order_id) {
        return $details = $this->db->query("select *  from `order_coupon` WHERE order_id='$order_id'")->row_array();
    }

    function car_emi($id, $user_id) {
        return $user = $this->db->query("SELECT *  FROM `emi_calculation` where user_id='$user_id' and pro_id='$id'")->result_array();
    }

    function booking_product_features($pro_id) {
        return $sel = $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$pro_id'  and v.status='1'")->result_array();
    }

    function booking_detail($pro_id, $user_id, $cond) {
        if ($cond == 1) {
            $city = $this->common_city();
            $variant_type_detail = $this->db->query("SELECT variant_type FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $variant_type = explode(',', $variant_type_detail[0]['variant_type']);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                if ($variant_type_data != '') {
                    $ids = "'" . $variant_type_data . "'," . $ids;
                }
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
            $car_intro_detail = $this->db->query("SELECT variant_id,model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id` IN ($ids)")->result_array();
            foreach ($car_intro_detail as $car_intro_detail_data) {
                $model_id = $car_intro_detail_data['model_id'];
                $brand_id = $car_intro_detail_data['brand_id'];
                $pro_name = $car_intro_detail_data['pro_name'];
                $variant_id = $car_intro_detail_data['variant_id'];
                $car_variant_detail = '';
                $car_variant_detail.="SELECT v.*,s.mileage,s.engine_maximum_power,(select f.fuel_type from fuel_type f where f.fuel_type_id=v.fuel_type) as fuel_type FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and v.`pro_name`='$pro_name' and s.variant_id=v.variant_id and v.variant_id='$variant_id'  and v.status='1'";
                $merge_data[] = '';
                $car_variant_details = $this->db->query($car_variant_detail)->result_array();
                foreach ($car_variant_details as $car_variant_detail_data) {
                    $variant_id = $car_variant_detail_data['variant_id'];
                    $ex_price = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id' and city_id='$city'")->row_array();
                    $mncex_price = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id' and city_id='$city'")->row_array();
                    $merge_data[] = array_merge($car_variant_detail_data, $ex_price, $mncex_price);
                }
                return $merge_data;
            }
        } else if ($cond == 2) {
            $variant_type_detail = $this->db->query("SELECT color,wheel FROM `car_configuration` WHERE `product_id`='$pro_id'  and user_id='$user_id'")->result_array();
            $color = $variant_type_detail[0]['color'];
            $wheel = $variant_type_detail[0]['wheel'];
            return $car_intro_detail = $this->db->query("SELECT f.*,fv.* FROM `features` f,`feature_variant_mapping` fv where f.feature_id IN ('$color','$wheel') and fv.feature_id=f.feature_id and fv.variant_id='$pro_id'")->result_array();
        } else if ($cond == 3) {
            $variant_type_detail = $this->db->query("SELECT leather,cloth,dashboard FROM `car_configuration` WHERE `product_id`='$pro_id'  and user_id='$user_id'")->result_array();
            $leather = $variant_type_detail[0]['leather'];
            $cloth = $variant_type_detail[0]['cloth'];
            $dashboard = $variant_type_detail[0]['dashboard'];
            return $car_intro_details = $this->db->query("SELECT f.*,fv.* FROM `features` f,`feature_variant_mapping` fv where f.feature_id IN ('$leather','$cloth','$dashboard') and fv.feature_id=f.feature_id and fv.variant_id='$pro_id'")->result_array();
        } else if ($cond == 4) {
            $variant_type_detail = $this->db->query("SELECT accessory FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $variant_type = explode(',', $variant_type_detail[0]['accessory']);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                $ids = "'" . trim($variant_type_data) . "'," . $ids;
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
            return $car_intro_details = $this->db->query("SELECT a.*,av.* FROM `accessory` a,`accessory_variant_mapping` av where av.accessory_id IN ($ids) and av.accessory_id=a.accessory_id and av.variant_id='$pro_id'")->result_array();
        } else if ($cond == 5) {
            $variant_type_detail = $this->db->query("SELECT finance_id,amc_id,insurance_id,insurance_id,extended_id,used_id FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $ids = $variant_type_detail[0]['finance_id'] . ',' . $variant_type_detail[0]['amc_id'] . ',' . $variant_type_detail[0]['insurance_id'] . ',' . $variant_type_detail[0]['extended_id'] . ',' . $variant_type_detail[0]['used_id'];
            $variant_type = explode(',', $ids);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                if ($variant_type_data != '') {
                    $ids = "'" . trim($variant_type_data) . "'," . $ids;
                }
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
//
            return $car_intro_details = $this->db->query("SELECT a.*,av.* FROM `accessory` a,`accessory_variant_mapping` av where av.accessory_id IN ($ids) and av.accessory_id=a.accessory_id and av.variant_id='$pro_id'")->result_array();
        } else if ($cond == 6) {
            $car_intro_details = $this->db->query("SELECT `color`,`wheel`,`leather`,`cloth`,`dashboard` "
                            . "FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id' and `status`=''")->row_array();
            return ($car_intro_details['color'] . ',' . $car_intro_details['wheel'] . ',' . $car_intro_details['leather'] . ',' . $car_intro_details['cloth'] . ',' . $car_intro_details['dashboard']);
        }
    }

    function pro_specification($pro_id) {
        return $spec = $this->db->query("SELECT *  FROM `specification` where variant_id='$pro_id'")->result_array();
    }

    function get_onroad_price($id, $user_id) {
        $main_city = $this->common_city();
        return $sel = $this->db->query("select m.extended_warranty,m.product_id,m.zero_depreciation,m.city_id,e.exshowroom_price,o.comp_insurance,o.road_tax,o.handling_price,o.lbt,o.mnc_on_road_price,o.onroad_price from mnc_exshowroom_price m,exshowroom_price e,onroad_price o where e.product_id=m.product_id and e.city_id=m.city_id and m.product_id=o.variant_id and m.city_id=o.city_id and m.city_id='$main_city' and m.product_id='$id'")->result_array();
//    return $sel = $this->db->query("select m.product_id,m.city_id,e.exshowroom_price,(select comp_insurance from onroad_price where onroad_price.variant_id=m.product_id and onroad_price.city_id='$main_city') as comp_insurance from mnc_exshowroom_price m,exshowroom_price e where m.product_id='$id' and m.city_id='$main_city' and e.product_id=m.product_id")->result_array();    
    }

    function order_user_detail($user_id) {
        return $data = $this->db->query("select * from `user` WHERE `user_id`='$user_id'")->result_array();
    }

    function order_wise_detail($order_id) {
        return $details = $this->db->query("select *,(select name from city c where c.city_id=order.city_id) as city_name from `order` where order_id='$order_id'")->result_array();
    }

}
