<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_section extends CI_Model {
    #### Header city section get here #### 

    public function common_city() {
        if ($_GET['city'] != '') {
//        get_city_id($city);    
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    function affilite($id) {
        return $query = $this->db->query("SELECT affiliate_id FROM `affiliate` where code='$id' and status='1'")->row_array();
    }

    function get_city_id($city) {
        return $query = $this->db->query("SELECT name,city_id FROM `city` where name='$city'")->row_array();
    }

    

    function get_rev_model($brand_data) {
        return $query = $this->db->query("SELECT model_id,model_name FROM `model` where brand_id='$brand_data' and status='1' ORDER BY `model_name` ASC")->result_array();
    }

    function get_rev_variant($brand_data, $model_data) {
//    echo $query ="SELECT variant_id,pro_name FROM `variant` where brand_id='$brand_data' and model_id='$model_data'";
        return $query = $this->db->query("SELECT variant_id,pro_name FROM `variant` where brand_id='$brand_data' and model_id='$model_data' and status='1'")->result_array();
    }

   
    function top_selling_featured($featured_type) {
        $main_city = $this->common_city();

        $query = $this->db->query("SELECT f.`featured_id`, f.`prd_id_1`, f.`status`,v.`pro_name`,v.`pro_name_comp`,v.`pro_image`,(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,(select b.brand_id from brand b where b.brand_id=v.brand_id) as brand_id,(select m.model_name from model m where m.model_id=v.model_id) as model_name FROM `featured` f,`variant` v WHERE f.`status`='1' and f.`featured_type`='$featured_type' and f.`prd_id_1`=v.`variant_id` and v.status='1'")->result_array();

        $final = array();
        foreach ($query as $query_res) {
            $prd_id_1 = $query_res['prd_id_1'];
            $brand_id = $query_res['brand_id'];
            $deals_data = $this->db->query("SELECT d.*,b.start_date,d.end_date,v.pro_name_comp,v.pro_image FROM deal d,brand_deal b,variant v where d.brand_id='$brand_id' and d.brand_id=b.brand_id and b.city_id='$main_city' and v.variant_id=d.variant_id and v.variant_id='$prd_id_1'  and v.status='1' and Date(d.end_date) >= '" . date('Y-m-d') . "'")->row_array();
            $deal = array('deal' => $deals_data);
            $comb = array_merge($query_res, $deal);
            $final[] = $comb;
        }
//        echo '</pre>';
        return($final);

//        return $query;
    }

    function home_group_deal() {
        $main_city = $this->common_city();
        $date = date('Y-m-d');
        return $query = $this->db->query("select f.prd_id_1,v.pro_name_comp,v.pro_name,v.pro_image,m.mnc_exshowroom_price,e.exshowroom_price from featured f,variant v,exshowroom_price e,mnc_exshowroom_price m where f.featured_type='top_selling_cars' and v.variant_id=e.product_id and v.variant_id=m.product_id and f.prd_id_1=v.variant_id and v.status='1'")->result_array();
    }

    function get_meta($cond, $id) {
        if ($cond == 1) {
            return $query = $this->db->query("select seo_keyword,seo_title,seo_description from variant where variant_id='$id'")->result_array();
        }
        if ($cond == 2) {
            return $query = $this->db->query("select seo_keyword,seo_title,seo_description from brand where brand_id='$id'")->result_array();
        }
    }

    function onraod_detail($pro_id) {
        $main_city = $this->common_city();

        return $query = $this->db->query("select mnc_on_road_price from onroad_price where variant_id='$pro_id' and city_id='$main_city'")->result_array();
    }

    

    

    function get_onroad_price($id, $user_id) {
        $main_city = $this->common_city();
        return $sel = $this->db->query("select m.extended_warranty,m.product_id,m.zero_depreciation,m.city_id,e.exshowroom_price,o.comp_insurance,o.road_tax,o.handling_price,o.lbt,o.mnc_on_road_price,o.onroad_price from mnc_exshowroom_price m,exshowroom_price e,onroad_price o where e.product_id=m.product_id and e.city_id=m.city_id and m.product_id=o.variant_id and m.city_id=o.city_id and m.city_id='$main_city' and m.product_id='$id'")->result_array();
//    return $sel = $this->db->query("select m.product_id,m.city_id,e.exshowroom_price,(select comp_insurance from onroad_price where onroad_price.variant_id=m.product_id and onroad_price.city_id='$main_city') as comp_insurance from mnc_exshowroom_price m,exshowroom_price e where m.product_id='$id' and m.city_id='$main_city' and e.product_id=m.product_id")->result_array();    
    }

    function price_specification($pro_id) {
        $main_city = $this->common_city();
        return $sel = $this->db->query("select * from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city'")->row_array();
    }

    function product_features($pro_id, $user_id) {



        return $sel = $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$pro_id'")->result_array();
    }

    function get_features_name($pro_ids, $id) {
        if ($id == 1) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 2) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 3) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 4) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 5) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        } else if ($id == 6) {
            return $query = $this->db->query("SELECT distinct(`feature_name`) FROM `features`f,`feature_variant_mapping` m  where m.feature_id=f.feature_id and m.`variant_id` IN ($pro_ids) and f.feature_type='$id'")->result_array();
        }
    }

    function product_variant_detail($id) {
        return $query = $this->db->query("SELECT * FROM `specification` WHERE variant_id='$id'")->result_array();
    }

    

    function reting_product_detail($pro_id) {
        return $query = $this->db->query("SELECT pro_name_comp,pro_name,pro_image,variant_id,pro_detail,upcoming,launching_date FROM `variant` WHERE variant_id='$pro_id'  and status='1'")->result_array();
    }

    

    

    function rating_details($id) {
        $get_data = $this->db->query("SELECT pro_name,variant_id,model_id,brand_id,pro_name_comp FROM `variant` where variant_id='$id'  and status='1'")->result_array();
        $model_id = $get_data[0]['model_id'];
        $brand_id = $get_data[0]['brand_id'];
        return $all_res = $this->db->query("SELECT v.pro_name,v.pro_name_comp,v.variant_id,(select brand_name from brand where brand_id='$brand_id') as brand_name,(select model_name from model where model_id='$model_id') as model_name FROM `variant` v where v.brand_id='$brand_id' and v.model_id='$model_id'  and v.status='1'")->result_array();
    }

    function ex_showroom_price($pro_id) {
        $main_city = $this->common_city();
        $data.='';
        $data.="SELECT m.product_id,m.mnc_exshowroom_price,e.exshowroom_price FROM `mnc_exshowroom_price` m,`exshowroom_price` e "
                . "where m.product_id='$pro_id' and m.product_id=e.product_id";
        if ($main_city != '') {
            $data.=" and m.city_id='$main_city' and e.city_id='$main_city'";
        }

        return $query2 = $this->db->query($data)->result_array();
    }

    function mnc_ex_showroom_price($pro_id) {
        $data = '';
        $main_city = $this->common_city();
        $data.="SELECT exshowroom_price FROM `exshowroom_price` where product_id='$pro_id'";
        if ($main_city != '') {
            $data.=" and city_id='$main_city'";
        }
        return $get_data = $this->db->query($data)->result_array();
    }

    function get_news() {
//        $this->db->where('product_id',$pro_id);
        $this->db->select('news_title,latest_news_id');
        $this->db->order_by("added_date", "desc");
        return $get_data = $this->db->get('news', '4')->result_array();
    }

    function get_allnews_details() {
        return $get_data = $this->db->get('news')->result_array();
    }

    function get_news_details($id) {
        if ($id != '') {
            $this->db->where('latest_news_id', $id);
        }
        $this->db->select('*');
        return $get_data = $this->db->get('news')->result_array();
    }

    ####get all brands related process here...####

    

    

    ####get home page carousel process here...####

    

    

   

    

   

    


    function pro_offer($pro_id, $user_id) {
        $main_city = $this->common_city();
        return $res = $this->db->query("SELECT * FROM `deal` d where d.`variant_id`='$pro_id' and city_id='$main_city'")->result_array();
    }

    function pro_brand_offer($pro_id, $user_id) {
        $main_city = $this->common_city();
        return $res = $this->db->query("SELECT * FROM `brand_deal` where city_id='$main_city'")->result_array();
    }

    ####get home page carousel process here...####
    ####get car type selection page process here...####

    

    function car_type_selection_filters($id, $type, $pro_price, $brand, $fuel, $min, $min2, $city, $pric1, $pric2, $trans) {
        if ($city == '') {
            $city = $this->common_city();
        }

        $main_query = "";
        $main_query.= "SELECT v.pro_name,v.brand_id,v.model_id,v.`variant_id`,v.`pro_image`,e.mnc_exshowroom_price,ex.exshowroom_price FROM `variant` v,`mnc_exshowroom_price` e,exshowroom_price ex where v.variant_id=e.product_id and v.variant_id=ex.product_id and  v.status='1'";
        if ($type != '') {
            $main_query.=" v.`pro_type`='$type' and ";
        } else {
            $main_query.=" v.`pro_type`!='$type' and ";
        }
        if ($trans != '') {
            $main_query.=" v.`tramission_type` IN ($trans) and ";
        } else {
            $main_query.="  ";
        }
        if ($pric1 != '' && $pric2 != '') {
            $main_query.=" e.`mnc_exshowroom_price` between '$pric1' and '$pric2' and ";
        }
        if ($brand != '') {
            $main_query.=" v.`brand_id` IN ($brand) and ";
        }
        if ($fuel != '') {
            $main_query.=" v.`fuel_type` IN ($fuel) and ";
        }
        if ($min != '' && $min2 != '') {
            $main_query.=" v.`seating_capacity` between '$min' and '$min2' and ";
        }
        if ($city != "") {
            $main_query.="e.`city_id`='$city' and ex.`city_id`='$city' and ";
        }
        $main_query.=" 1";
//          
        $query = $this->db->query($main_query)->result_array();
        $array = array();
        foreach ($query as $res) {
            $brand_id = $res["brand_id"];
            $variant_id = $res["variant_id"];
            $brand_res = $this->db->query("SELECT brand_image FROM `brand` where brand_id='$brand_id'")->result_array();
            if (empty($brand_res)) {
                $brand_res[0] = array('brand_image' => '');
            } else {
                $brand_res[0];
            }

            $user_rating = $this->db->query("SELECT AVG(`total`) as user_total FROM `user_rating` where pro_id='$variant_id'")->result_array();
            if (empty($user_rating)) {
                $user_rating[0] = array('user_total' => '');
            } else {
                $user_rating[0];
            }

            $expert_rating = $this->db->query("SELECT AVG(`total`) as expert_total FROM `expert_rating` where pro_id='$variant_id'")->result_array();
//                print_r($expert_rating);
            if (empty($expert_rating)) {
                $expert_rating[0] = array('expert_total' => '');
            } else {
                $expert_rating[0];
            }

            $model_id = $res["model_id"];
            $model_res = $this->db->query("SELECT model_name FROM `model` where model_id='$model_id' and status='1' ORDER BY `model_name` ASC")->result_array();
            if (empty($model_res)) {
                $model_res[0] = array('model_name' => '');
            } else {
                $model_res[0];
            }

            $combine = array_merge($res, $brand_res[0], $model_res[0], $user_rating[0], $expert_rating[0]);
            $array[] = $combine;
        }
    }

    ####get car type selection page process here...####
    ####get car introduction page details here...####

    function car_intro_gallery_detail($id, $type) {
//        echo $id;
        if ($type == 'All') {
            return $car_intro_detail = $this->db->query("SELECT * FROM `product_gallery` where prduct_id='$id'")->result_array();
        } else {
            return $car_intro_detail = $this->db->query("SELECT image,product_gallery_id,video FROM `product_gallery` where prduct_id='$id' and page='$type'")->result_array();
        }
    }

    function car_intro_user_reviews($id) {
        return $car_intro_detail = $this->db->query("SELECT r.`total`,r.`what_i_like`,r.`what_i_dont_like`,u.`firstname` FROM `user_rating` r,`user` u where u.`user_id`=r.`user_id` and r.`pro_id`='$id' order by `user_rating_id` DESC limit 1")->result_array();
    }

    function car_intro_expert_reviews($id) {
        return $car_intro_detail = $this->db->query("SELECT `expert_rating_id`,`journilist_name`,`what_i_like`,`what_i_dont_like`,`total` FROM `expert_rating` WHERE `pro_id`='$id' ORDER BY `expert_rating_id` DESC limit 1")->result_array();
    }

    function car_intro_exshowroom_price($id) {
        $city = $this->common_city();
        return $car_intro_detail = $this->db->query("SELECT m.*,e.`exshowroom_price`,m.`delivery_date` FROM `mnc_exshowroom_price` m,`exshowroom_price` e WHERE m.`product_id`='$id' and m.city_id='$city' and e.city_id='$city' and m.product_id=e.product_id")->result_array();
//        $car_intro_detail2=$this->db->query("SELECT `exshowroom_price` FROM `exshowroom_price` WHERE `product_id`='$id' and city_id='$city'")->result_array();
//        return $final=  array_merge($car_intro_detail,$car_intro_detail2);        
    }

    function car_variant_details($id) {
//        echo $id;
        $car_intro_detail = $this->db->query("SELECT model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id`='$id'  and status='1'")->result_array();
        foreach ($car_intro_detail as $car_intro_detail_data)
            $model_id = $car_intro_detail_data['model_id'];
        $brand_id = $car_intro_detail_data['brand_id'];
//        $pro_name = $car_intro_detail_data['pro_name'];
        $city = $this->common_city();
        $car_variant_detail = $this->db->query("SELECT v.*,s.mileage,s.engine_maximum_power FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and  s.variant_id=v.variant_id  and v.status='1'")->result_array();
//        print_r($car_variant_detail);
//        exit;
        $merge_data = array();
        foreach ($car_variant_detail as $car_variant_detail_data) {
            $variant_id = $car_variant_detail_data['variant_id'];
            $car_variant_detail = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id' and city_id='$city'")->result_array();
            $car_variant_detail2 = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id' and city_id='$city'")->result_array();
            $merge_data[] = array_merge($car_variant_detail, $car_variant_detail_data, $car_variant_detail2);
        }
        //print_r($merge_data);
        return $merge_data;
    }

    function filter_car_variant_details($id, $trimline, $engine, $transmission) {

//        if ($trimline != '') {
//            $select_acc = rtrim($trimline, ",");
//            $car_intro_detail = "SELECT model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id` ='$select_acc'";
//            $car_intro_detail = $this->db->query($car_intro_detail)->result_array();
//        } else {
        $car_intro_detail = $this->db->query("SELECT model_id,brand_id,pro_name,pro_name_comp,pro_type,variant FROM `variant` WHERE `variant_id`='$id' and status='1'")->result_array();
//        }
//            print_r($car_intro_detail);
        foreach ($car_intro_detail as $car_intro_detail_data) {
            $model_id = $car_intro_detail_data['model_id'];
            $brand_id = $car_intro_detail_data['brand_id'];
            $pro_name = $car_intro_detail_data['pro_name'];
            $city = $this->common_city();
            $car_variant_detail = '';
            $car_variant_detail.="SELECT v.*,s.mileage,s.engine_maximum_power FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and s.variant_id=v.variant_id";
            if ($engine != '') {
                $car_variant_detail.=" and v.fuel_type='$engine'";
            }
            if ($transmission != '') {
                $transmission;
                $car_variant_detail.=" and v.tramission_type like '%$transmission%'";
            }
            if ($select_acc != '') {
                $car_variant_detail.=" and v.variant_id='$select_acc'";
            }
//          echo $car_variant_detail;
//          exit;
            $merge_data[] = '';
            $car_variant_details = $this->db->query($car_variant_detail)->result_array();
            foreach ($car_variant_details as $car_variant_detail_data) {
                $variant_id = $car_variant_detail_data['variant_id'];
                $ex_price = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id' and city_id='$city'")->result_array();
                $mncex_price = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id' and city_id='$city'")->result_array();
                $merge_data[] = array_merge($car_variant_detail_data, $ex_price, $mncex_price);
            }
        }
        return $merge_data;
    }

    ####get car introduction page details here...####
    #####get booking details here...####

    function booking_detail($pro_id, $user_id, $cond) {
        if ($cond == 1) {
            $city = $this->common_city();
            $variant_type_detail = $this->db->query("SELECT variant_type FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $variant_type = explode(',', $variant_type_detail[0]['variant_type']);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                if ($variant_type_data != '') {
                    $ids = "'" . $variant_type_data . "'," . $ids;
                }
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
            $car_intro_detail = $this->db->query("SELECT variant_id,model_id,brand_id,pro_name,pro_type,variant FROM `variant` WHERE `variant_id` IN ($ids)")->result_array();
            foreach ($car_intro_detail as $car_intro_detail_data) {
                $model_id = $car_intro_detail_data['model_id'];
                $brand_id = $car_intro_detail_data['brand_id'];
                $pro_name = $car_intro_detail_data['pro_name'];
                $variant_id = $car_intro_detail_data['variant_id'];
                $car_variant_detail = '';
                $car_variant_detail.="SELECT v.*,s.mileage,s.engine_maximum_power,(select f.fuel_type from fuel_type f where f.fuel_type_id=v.fuel_type) as fuel_type FROM `variant` v,specification s WHERE v.`model_id`='$model_id' and v.`brand_id`='$brand_id' and v.`pro_name`='$pro_name' and s.variant_id=v.variant_id and v.variant_id='$variant_id'  and v.status='1'";
                $merge_data[] = '';
                $car_variant_details = $this->db->query($car_variant_detail)->result_array();
                foreach ($car_variant_details as $car_variant_detail_data) {
                    $variant_id = $car_variant_detail_data['variant_id'];
                    $ex_price = $this->db->query("SELECT exshowroom_price FROM `exshowroom_price` where product_id='$variant_id' and city_id='$city'")->row_array();
                    $mncex_price = $this->db->query("SELECT mnc_exshowroom_price FROM `mnc_exshowroom_price` where product_id='$variant_id' and city_id='$city'")->row_array();
                    $merge_data[] = array_merge($car_variant_detail_data, $ex_price, $mncex_price);
                }
                return $merge_data;
            }
        } else if ($cond == 2) {
            $variant_type_detail = $this->db->query("SELECT color,wheel FROM `car_configuration` WHERE `product_id`='$pro_id'  and user_id='$user_id'")->result_array();
            $color = $variant_type_detail[0]['color'];
            $wheel = $variant_type_detail[0]['wheel'];
            return $car_intro_detail = $this->db->query("SELECT f.*,fv.* FROM `features` f,`feature_variant_mapping` fv where f.feature_id IN ('$color','$wheel') and fv.feature_id=f.feature_id and fv.variant_id='$pro_id'")->result_array();
        } else if ($cond == 3) {
            $variant_type_detail = $this->db->query("SELECT leather,cloth,dashboard FROM `car_configuration` WHERE `product_id`='$pro_id'  and user_id='$user_id'")->result_array();
            $leather = $variant_type_detail[0]['leather'];
            $cloth = $variant_type_detail[0]['cloth'];
            $dashboard = $variant_type_detail[0]['dashboard'];
            return $car_intro_details = $this->db->query("SELECT f.*,fv.* FROM `features` f,`feature_variant_mapping` fv where f.feature_id IN ('$leather','$cloth','$dashboard') and fv.feature_id=f.feature_id and fv.variant_id='$pro_id'")->result_array();
        } else if ($cond == 4) {
            $variant_type_detail = $this->db->query("SELECT accessory FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $variant_type = explode(',', $variant_type_detail[0]['accessory']);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                $ids = "'" . trim($variant_type_data) . "'," . $ids;
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
            return $car_intro_details = $this->db->query("SELECT a.*,av.* FROM `accessory` a,`accessory_variant_mapping` av where av.accessory_id IN ($ids) and av.accessory_id=a.accessory_id and av.variant_id='$pro_id'")->result_array();
        } else if ($cond == 5) {
            $variant_type_detail = $this->db->query("SELECT finance_id,amc_id,insurance_id,insurance_id,extended_id,used_id FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id'")->result_array();
            $ids = $variant_type_detail[0]['finance_id'] . ',' . $variant_type_detail[0]['amc_id'] . ',' . $variant_type_detail[0]['insurance_id'] . ',' . $variant_type_detail[0]['extended_id'] . ',' . $variant_type_detail[0]['used_id'];
            $variant_type = explode(',', $ids);
            $ids = '';
            foreach ($variant_type as $variant_type_data) {
                if ($variant_type_data != '') {
                    $ids = "'" . trim($variant_type_data) . "'," . $ids;
                }
            }
            $ids = substr($ids, 0, -1);
            if ($ids == '') {
                $ids = "''";
            }
//
            return $car_intro_details = $this->db->query("SELECT a.*,av.* FROM `accessory` a,`accessory_variant_mapping` av where av.accessory_id IN ($ids) and av.accessory_id=a.accessory_id and av.variant_id='$pro_id'")->result_array();
        } else if ($cond == 6) {
            $car_intro_details = $this->db->query("SELECT `color`,`wheel`,`leather`,`cloth`,`dashboard` "
                            . "FROM `car_configuration` WHERE `product_id`='$pro_id' and user_id='$user_id' and `status`=''")->row_array();
            return ($car_intro_details['color'] . ',' . $car_intro_details['wheel'] . ',' . $car_intro_details['leather'] . ',' . $car_intro_details['cloth'] . ',' . $car_intro_details['dashboard']);
        }
    }

    #####get booking details here...####
    ##### On road price calucaltion ####

    function onroad_price_calculation($pro_id, $user_id, $cond) {
        if ($cond == 1) {
            $car_intro_details = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and user_id='$user_id'")->result_array();
            if (!empty($car_intro_details)) {
                $color = $car_intro_details[0]['color'];
                $wheel = $car_intro_details[0]['wheel'];
                $variant_type = $car_intro_details[0]['variant_type'];
                $leather = $car_intro_details[0]['leather'];
                $cloth = $car_intro_details[0]['cloth'];
                $dashboard = $car_intro_details[0]['dashboard'];
                $car_color_price = $this->db->query("SELECT price FROM `feature_variant_mapping` where feature_id IN ('$color','$wheel','$leather','$cloth','$dashboard')")->result_array();
                $price = '';
                foreach ($car_color_price as $car_color_price_data) {
                    $car_color_price_data['price'];
                    $price = $price + $car_color_price_data['price'];
                }
                return $price;
            }
        } else if ($cond == 2) {

            $car_intro_details = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and user_id='$user_id'")->result_array();
            if (!empty($car_intro_details)) {

                $accessory = $car_intro_details[0]['accessory'];
                $variant_type = explode(',', $accessory);
                $ids = '';
                foreach ($variant_type as $variant_type_data) {
                    $ids = "'" . $variant_type_data . "'," . $ids;
                }
                $ids = substr($ids, 0, -1);

                $car_color_price = $this->db->query("SELECT price FROM `feature_variant_mapping` where feature_id IN ($ids)")->result_array();
                $price = '';
                foreach ($car_color_price as $car_color_price_data) {
                    $car_color_price_data['price'];
                    $price = $price + $car_color_price_data['price'];
                }
                return $price;
            }
        }
    }

    ##### On road price calucaltion ####

    #
   function additional_info($id, $user_id) {
        $car_intro_detail = $this->db->query("SELECT * FROM `car_configuration` WHERE `product_id`='$id' and user_id='$user_id' and status!='2'")->result_array();
        if (!empty($car_intro_detail)) {
            $ids = "'" . $car_intro_detail[0]['color'] . "'," . "'" . $car_intro_detail[0]['wheel'] . "'," . "'" . $car_intro_detail[0]['leather'] . "'," . "'" . $car_intro_detail[0]['cloth'] . "'," . "'" . $car_intro_detail[0]['dashboard'] . "'";
            return $caluclation = $this->db->query("SELECT sum(price) as price FROM `feature_variant_mapping` WHERE `feature_id` IN ($ids) and variant_id='$id'")->result_array();
        }
    }

    function accessory_details_res($id, $user_id) {
        $car_intro_detail = $this->db->query("SELECT * FROM `car_configuration` WHERE `product_id`='$id' and user_id='$user_id' and status!='2'")->result_array();
        if (!empty($car_intro_detail)) {
            $ids = $car_intro_detail[0]['accessory'];
            $selected_data = explode(',', $ids);
            $res = '';
            for ($i = 0; $i <= count($selected_data) - 2; $i++) {
                $res = $res . "'" . $selected_data[$i] . "',";
            }
            $select_acc = rtrim($res, ",");
            if ($select_acc == '') {
                $select_acc = "''";
            }
            return $caluclation = $this->db->query("SELECT sum(price) as price FROM `accessory_variant_mapping` WHERE `accessory_id` IN ($select_acc) and variant_id='$id'")->result_array();
        }
    }

    function onraod_calculation($id, $user_id) {
//    echo $id;    
        $main_city = $this->common_city();
        return $caluclation = $this->db->query("SELECT * FROM `onroad_price` WHERE `city_id`='$main_city' and variant_id='$id'")->result_array();
    }

    #
    ####Change Car start here...####

    

    

    ####Change Car start here...####
    #####Change exterior_selection here...####

    function car_color($color_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `body_color` WHERE `body_color_id` IN($color_data)")->result_array();
    }

    function car_wheel($wheel_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `wheel` WHERE `wheel_id` IN($wheel_data)")->result_array();
//    echo $wheel_data;    
    }

    function car_specifiaction($id, $type) {

        if (!empty($id)) {
            return $car_intro_detail = $this->db->query("SELECT m.`price`,f.`feature_id`, f.`feature_type`, f.`feature_name`, f.`feature_desc`, f.`feature_img`, f.`recomended`, f.`cost` FROM `feature_variant_mapping` m,`features` f WHERE m.`variant_id`='$id' and m.`feature_id`=f.`feature_id` and  `f`.`feature_name` like '%$type%'")->result_array();
        }
    }

    #####Change exterior_selection here...####
    ######Change configuration accessory here...####

    function car_exterior($id, $pro_id) {
        return $car_intro_detail = $this->db->query("SELECT m.*,a.* FROM `accessory_variant_mapping` m,`accessory` a where a.accessory_id=m.accessory_id and m.variant_id='$pro_id' and a.accessory_cat='$id'")->result_array();
    }

    function car_exterior_selected($user_id, $pro_id) {
        return $car_intro_detail = $this->db->query("SELECT accessory,accessory_total FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and status!='2'")->result_array();
    }

    ######Change configuration accessory here...####
    ###### Selected Car Configuration Details ######

    function config_selected_detail($pro_id, $user_id) {
        return $data = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and user_id='$user_id' and status!='2'")->result_array();
    }

    ###### Selected Car Configuration Details ######
    #### Get brand & model wise variants ####

    function get_model_wise_variants($model, $brand_id) {
//        return '0';
        return $data = $this->db->query("SELECT variant,variant_id,pro_name FROM `variant` where model_id='$model' and brand_id='$brand_id'  and status='1'")->result_array();
    }

    #### Get brand & model wise variants ####
    #### Get variant id wise video ####

    function get_product_wise_video($variant_video) {
        return $data = $this->db->query("SELECT video FROM  `product_gallery` where prduct_id='$variant_video' and page='Video'")->result_array();
    }

    #### Get variant id wise video ####
    #### Order Status Start####

    function order_confirm($pro_id, $user_id, $tot_price, $tot_days, $fewamaount) {
//      $discount_price = $this->input->cookie('discount_price',TRUE);
        $on_road_price = $this->input->cookie('on_road_price', TRUE);
        $group_discount = $this->input->cookie('group_discount', TRUE);
        $cook_coupon_id = $this->input->cookie('coupon_id', TRUE);
        $exp_cook_coupon_id = explode('_', $cook_coupon_id);        
            
        $ref_id = $this->input->cookie('ref_id', TRUE);
        $main_city = $this->common_city();
        $data2 = "SELECT c.user_id,c.car_configuration_id,c.product_id,c.invoice_no,c.color,(select f.feature_desc from features f  where  f.feature_id = c.color) as feature_desc FROM `car_configuration` c where c.user_id='$user_id' and c.product_id='$pro_id' and c.status!='2' order by c.car_configuration_id DESC limit 1";
        $data = $this->db->query($data2)->result_array();
        if (!empty($data)) {
            $car_configuration_id = $data[0]['car_configuration_id'];
            $invoice_no = $data[0]['invoice_no'];
            $product_id = $data[0]['product_id'];
            $check = $this->db->query("select `order_id` from `order` where product_id='$pro_id' and user_id='$user_id' and city_id='$main_city' and configuration_id='$car_configuration_id'");
            $res = $check->result_array();
            $count_order = $check->num_rows();
            if ($count_order == 0) {
                $insert_order = $this->db->query("INSERT INTO `order`(`paid_amount`,`product_id`,`city_id`,`user_id`,`invoice_no`,`configuration_id`,"
                        . "`dis_price`,`tot_price`,`days`,`on_road_price`,`ref_id`)"
                        . " VALUES ('$fewamaount','$pro_id','$main_city','$user_id','$invoice_no','$car_configuration_id',"
                        . "'$group_discount','$tot_price','$tot_days','$on_road_price','$ref_id')");
                $order_id = $this->db->insert_id();
                if($pro_id==$exp_cook_coupon_id[0] && !empty($exp_cook_coupon_id[1]))
                {
                $coupon_id=$exp_cook_coupon_id[1];    
                $select_coupon = $this->db->query("select * from `coupon` where coupon_id='$coupon_id'")->row_array();
                $coupon_id=$select_coupon['coupon_id'];
                $start_date=$select_coupon['start_date'];
                $end_date=$select_coupon['end_date'];
                $coupon_type=$select_coupon['coupon_type'];
                $coupon_material=$select_coupon['coupon_material'];
                $amount_percent=$select_coupon['amount_percent'];
                $coupn_code=$select_coupon['coupn_code'];
                $insert_coupon = $this->db->query("INSERT INTO `order_coupon` SET `order_id` = '$order_id',`coupon_id` = '$coupon_id',`start_date` = '$start_date',`end_date` = '$end_date',`coupon_type` = '$coupon_type',`coupon_material` = '$coupon_material',`amount_percent` = '$amount_percent',`coupn_code` = '$coupn_code'");
                }
//                exit;
                $update_config = $this->db->query("UPDATE `car_configuration` SET `status`='2' WHERE car_configuration_id='$car_configuration_id'");
                $update_emi = $this->db->query("UPDATE `emi_calculation` SET `order_id`='$order_id' WHERE pro_id='$pro_id' and user_id='$user_id'");
            }
        }
        $order_details = $this->db->query("select o.`order_id`,o.dis_price,o.`user_id`,o.`configuration_id`,o.`invoice_no`,o.`product_id`,o.on_road_price,v.pro_name_comp,o.tot_price,v.pro_image,(select c.name from city c  where c.city_id=o.city_id) as city_name from `order` o,variant v where o.user_id='$user_id' and o.city_id='$main_city' and o.product_id=v.variant_id order by o.order_id DESC limit 1")->row_array();
        $user_details = $this->db->query("select firstname,email from `user` where user_id='$user_id'")->row_array();

        $final_res = array_merge($order_details, $user_details);
        return ($final_res);
    }
    
    function coupon_used($order_id)
    {
     return $details = $this->db->query("select *  from `order_coupon` WHERE order_id='$order_id'")->row_array();   
    }
    

    function order_confirm_mail($order_id, $id) {
        if ($id == 1) {
            return $details = $this->db->query("select *  from `order` WHERE order_id='$order_id'")->num_rows();
        } else {
            return $details = $this->db->query("UPDATE `order` SET `e_status`='1' WHERE order_id='$order_id'");
        }
    }

    function order_wise_detail($order_id) {
        return $details = $this->db->query("select order_id,invoice_no,product_id,city_id,user_id,configuration_id,dis_price,on_road_price,(select name from city c where c.city_id=order.city_id) as city_name from `order` where order_id='$order_id'")->result_array();
    }

    function car_intro_detail($id) {
        return $car_intro_detail = $this->db->query("SELECT v.pro_name,v.pro_name_comp,v.wheel_id,v.body_color_id,v.variant_id,v.pro_detail,v.pro_image,(select brand_name from brand where brand_id=v.brand_id) as brand_name,(select model_name from model where model_id=v.model_id) as model_name FROM `variant` v where v.variant_id='$id'  and v.status='1'")->result_array();
    }

    function dependencies_res($select_acc) {
        return $details = $this->db->query("select accessory_id,dependent_accessory_id,dependency_type,dependent_accessory_name from `accessory_dependencies_mapping` where accessory_id IN ($select_acc)")->result_array();
    }

    #### Order Status End####
    #### Order Status End####

    function max_price($city) {
        $que = "";
        $que.="SELECT MAX(m.mnc_exshowroom_price) as max_price,MIN(m.mnc_exshowroom_price) as min_price FROM `mnc_exshowroom_price` m";
        if ($city != '') {
            $que.=" where m.city_id='$city'";
        }
        return $details = $this->db->query($que)->row_array();
    }

    #### Order Status End####

    

    

    

    

    

    function select_config_breadcrub($pro_id, $user_id, $data_cookies) {
        return $upd = $this->db->query("SELECT * FROM `car_configuration` where user_id='$user_id' and product_id='$pro_id' and user_cookie='$data_cookies' and status!='2'")->result_array();
    }

    function product_comp_color($pro_id) {
        $upd = $this->db->query("SELECT distinct(feature_id) FROM `feature_variant_mapping` where variant_id='$pro_id'")->result_array();
        $id = '';
//    $upd=array_unique($upd);
        foreach ($upd as $upd_res) {
            $feature_id = $upd_res['feature_id'];
            $id = $id . "'" . $feature_id . "',";
        }
        $ids = substr($id, 0, -1);
        if ($ids == '') {
            $ids = "''";
        }
        return $all_res = $this->db->query("SELECT feature_name,feature_desc,feature_img FROM `features` where feature_id IN ($ids) and feature_name='Color'")->result_array();

//    echo '<br>';        
    }

    function product_comp_feature($id) {
//   echo '<pre>';       

        $upd = $this->db->query("SELECT distinct(accessory_id) FROM `accessory_variant_mapping` where variant_id='$id'")->result_array();
        $id = '';
        foreach ($upd as $upd_res) {
            $feature_id = $upd_res['accessory_id'];
            $id = $id . "'" . $feature_id . "',";
        }
        $ids = substr($id, 0, -1);
        return $all_res = $this->db->query("SELECT accessory_id,accessory_cat,accessory_sub_cat,accessory_name FROM `accessory` where accessory_id IN ($ids) order by accessory_sub_cat,accessory_cat ASC")->result_array();
//   echo '</pre>';
    }


    function select_emi($pro_id, $user_id) {
        return $del = $this->db->query("SELECT * FROM `emi_calculation` where `pro_id`='$pro_id' and `user_id`='$user_id' and order_id=''")->result_array();
    }

    function get_emi_record($emi_id, $prod_id) {
//    echo "SELECT * FROM `emi_calculation` where emi_id='$emi_id' and pro_id='$prod_id'";    
        return $del = $this->db->query("SELECT * FROM `emi_calculation` where emi_id='$emi_id' and pro_id='$prod_id'")->result_array();
    }

    function load_saved_price($brand_data) {
        $brand_data;
        $del = $this->db->query("SELECT variant_id FROM `variant` where `brand_id`='$brand_data'  and status='1'")->result_array();
        $data = '';
        foreach ($del as $del_data) {
            $variant_id = $del_data['variant_id'];
            $data = $data . "'" . $variant_id . "',";
        }
        $data = rtrim($data, ",");
        if (empty($data)) {
            $data = "''";
        }
        $sel = $this->db->query("SELECT d.*,e.mnc_exshowroom_price FROM `deal` d,`mnc_exshowroom_price` e where d.`variant_id` IN ($data) and d.variant_id=e.product_id")->result_array();

        $disocunts = '';
        foreach ($sel as $sel_data) {
            $pos = strpos($sel_data['amount_per'], '%');
            if ($pos === false) {
                $discount = $sel_data['amount_per'];
                $disocunts = $disocunts + $discount;
//    echo 'not';
            } else {
                $data = rtrim($sel_data['amount_per'], "%");
                $discount = $sel_data['mnc_exshowroom_price'] * ($data / 100);
                $disocunts = $disocunts + $discount;
//    echo 'yes';
            }
        }
        echo $disocunts;
    }

    

    function accessory_services($id, $user_id, $cond) {
        if ($cond == 1) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='11' limit 1")->result_array();
        } else if ($cond == 2) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='12' limit 1")->result_array();
        } else if ($cond == 3) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='13' limit 1")->result_array();
        } else if ($cond == 4) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='14' limit 1")->result_array();
        } else if ($cond == 5) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='15' limit 1")->result_array();
        } else if ($cond == 6) {
            return $sel = $this->db->query("SELECT a.accessory_name,a.accessory_information,a.accessory_sub_cat,a.accessory_id,m.price,m.id "
                            . "FROM `accessory` a,accessory_variant_mapping m where a.`accessory_cat`='7' and a.accessory_id=m.accessory_id"
                            . " and m.variant_id='$id' and a.accessory_sub_cat='16' limit 1")->result_array();
        }
    }

    function search_car_res($id) {
        return $sel = $this->db->query("select v.variant_id,v.pro_name,v.pro_image,pro_name_comp from variant v where v.pro_name_comp like '%$id%' and v.status='1' order by v.variant_id LIMIT 5")->result_array();
    }

    function search_user_res($id) {
        return $sel = $this->db->query("select user_id,firstname from user where firstname like '%$id%' order by user_id LIMIT 5")->result_array();
    }

    function customer_address($user_id) {
        return $sel = $this->db->query("select * from user where user_id ='$user_id'")->result_array();
    }

    

    

    function customer_details($user_id) {
        return $user = $this->db->query("SELECT username,firstname,pincode,lastname,email,state,address,telephone,city  FROM `user` where user_id='$user_id'")->result_array();
    }

    function registeration_details($user_id) {
        return $user = $this->db->query("SELECT * FROM `shipping_detail` where user_id='$user_id'")->result_array();
    }

    function car_configuration($user_id, $id, $cond, $invoice_no = '') {
        if ($cond == '1') {
            return $user = $this->db->query("SELECT *  FROM `car_configuration` where user_id='$user_id' and product_id='$id' and status!='2'")->result_array();
        } else {
            return $user = $this->db->query("SELECT *  FROM `car_configuration` where user_id='$user_id' and product_id='$id' and invoice_no='$invoice_no'")->result_array();
        }
    }

    function car_emi($id, $user_id) {
        return $user = $this->db->query("SELECT *  FROM `emi_calculation` where user_id='$user_id' and pro_id='$id'")->result_array();
    }

    /*     * *** product specification **** */

    

    /*     * *** forum content **** */

    function forum_brand_id($brand) {
        return $spec = $this->db->query("SELECT brand_id,brand_name  FROM `brand` where brand_name='$brand'")->result_array();
    }

    function forum_brand_count($brand_id, $cond) {
        $spec = $this->db->query("SELECT user_id  FROM `join_group` where brand_id='$brand_id'");
        if ($cond == '1') {
            return $spec->num_rows();
        } else {
            $spec = $spec->result_array();
//print_r($spec);
            $usersdata = '';
            foreach ($spec as $spec_data) {
                $usersdata = "'" . $spec_data['user_id'] . "'," . $usersdata;
//echo $user_id=$spec_data['user_id'];    
//$users=$this->db->query("SELECT firstname,fb_id  FROM `user` where user_id='$user_id'")->result_array();        
//if(!empty($users))
//{  
////$firsname=$users[0]['firstname'];    
////$fb_id=$users[0]['fb_id'];    
////$users
//$usersdata[]=$users;
//}  
            }
//echo rtim($usersdata;
            $trans = substr($usersdata, 0, -1);
            $users = "SELECT firstname,fb_id  FROM `user` where user_id IN ($trans)";
            $users = $this->db->query("SELECT firstname,fb_id  FROM `user` where user_id IN ($trans)")->result_array();
//print_r($users);
            return ($users);
        }
    }

    function check_brand_join($brand_id, $ses_id) {
//echo $brand_id;    
//echo $ses_id;   
        return $count_res = $this->db->query("SELECT * FROM `join_group` where user_id='$ses_id' and brand_id='$brand_id'")->num_rows();
    }

    function group_deal_brands() {
        $count_res = $this->db->query("SELECT distinct(`brand_id`) as brand_id FROM `brand_deal`")->result_array();
        $array_data = array();
        foreach ($count_res as $count_res_data) {
            $brand_id = $count_res_data['brand_id'];
            $data = $this->db->query("SELECT brand_name FROM `brand` where brand_id='$brand_id'")->result_array();
            $array_data[] = $data;
//print_r($data);
        }
        return ($array_data);
    }

    function upload_profile($img_name) {
        $exp = explode('.', $img_name);
        $profile = $exp[0];
        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        return $data = $this->db->query("UPDATE `user` SET `fb_id`='$profile' WHERE `user_id`='$user_id'");
    }

    function upload_cover($img_name) {
        $exp = explode('.', $img_name);
        $cover = $exp[0];

        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        return $data = $this->db->query("UPDATE `user` SET `cover`='$cover' WHERE `user_id`='$user_id'");
    }

    function order_user_detail($user_id) {
        return $data = $this->db->query("select * from `user` WHERE `user_id`='$user_id'")->result_array();
    }

    function order_billing_details($user_id, $id) {
        if ($id == 1) {
            return $data = $this->db->query("SELECT `firstname`, `lastname`, `email`, `telephone`, `address`, `city`, `state`, `pincode`, `country` FROM `user` WHERE `user_id`='$user_id'")->result_array();
        }
        if ($id == 2) {
            return $data = $this->db->query("SELECT * FROM `shipping_detail` WHERE `user_id`='$user_id'")->result_array();
        }
    }

    function shiping_detail($user_id) {
        return $data = $this->db->query("SELECT * FROM `shipping_detail` WHERE `user_id`='$user_id'")->result_array();
    }

    

    function hot_deal_data() {
        $sort = $_GET['sort'];
        $type = $_GET['type'];
        $brand = $_GET['brand'];
        if ($brand != '') {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where brand_name='$brand'")->row_array();
            $brand_id = "'" . $brand['brand_id'] . "'";
        } else {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where status='1'")->result_array();
            $res = '';
            foreach ($brand as $brand_res) {
                $res = $res . "'" . $brand_res['brand_id'] . "',";
            }
            $brand_id = rtrim($res, ",");
        }
        $main_city = $this->input->cookie('main_city', TRUE);
        $res = array();
        $main_data = '';
        $main_data.="SELECT h.*,v.pro_name_comp,v.pro_image,"
                . "(select brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                . " FROM hot_deals h,variant v where v.variant_id=h.variant_id and v.brand_id IN ($brand_id)  and v.status='1'";
        if ($type != '') {
            $main_data.=" and v.pro_type='$type'";
        }

        $main_data.=" order by brand_id ASC";
        $main_data;
        $data = $this->db->query($main_data)->result_array();
        return $data;
    }

    

    

    

    function car_gallery_detail($pro_id) {
        $data = $this->db->query("select v.variant_id,(select b.brand_name from brand b where v.brand_id=b.brand_id) as brand_name,(select m.model_name from model m where m.model_id=v.model_id) as model_name from variant v where v.variant_id='$pro_id' and v.status='1'")->row_array();
        $brand_name = $data['brand_name'];
        $model_name = $data['model_name'];
        $directory = APPPATH . "../admin/uploads/portfolio/" . ucfirst(strtolower($data['brand_name'])) . '/' . ucfirst(strtolower($data['model_name'])) . '/';
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        $data = array();
        foreach ($scanned_directory as $scanned_directory_data) {
            $arr = array("Image" => $scanned_directory_data, "Brand" => ucfirst(strtolower($brand_name)), "Model" => ucfirst(strtolower($model_name)), "Brand2" => $brand_name);
            $data[] = $arr;
        }
//        print_r($data);
        return ($data);
    }

    function booking_product_features($pro_id) {
        return $sel = $this->db->query("select vm.*,v.pro_name,feature_name,feature_desc,feature_img,feature_type "
                        . "from feature_variant_mapping vm,variant v,features f where "
                        . "v.variant_id=vm.variant_id and f.feature_id=vm.feature_id and vm.variant_id='$pro_id'  and v.status='1'")->result_array();
    }

    function order_invoice_update($car_configuration_id) {
        $invoice = 'INV-MNC-' . $car_configuration_id;
        $this->db->query("UPDATE `car_configuration` SET `invoice_no` = '$invoice' WHERE `car_configuration_id`='$car_configuration_id'");
        $this->db->query("UPDATE `order` SET `invoice_no` = '$invoice' WHERE `configuration_id`='$car_configuration_id'");
        return $invoice;
    }

    function hot_deal_data4() {
        $city = $_GET['city'];
        $car = $_GET['car'];
        $main_city = $city;
        if ($main_city == '') {
            $main_city = $this->input->cookie('main_city', TRUE);
        }

        $res = array();
        $main_data = '';
        $main_data.="SELECT v.variant_id,v.pro_name_comp,v.pro_image,v.pro_detail,"
                . "(select name from city c where c.city_id='$city') as city_name,"
                . "(select brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                . " FROM variant v where 1  and v.status='1' ";
        if ($car != '') {
            $main_data.=" and v.variant_id='$car'";
        }

        $main_data.=" order by brand_id ASC";
//    echo $main_data;
        $data = $this->db->query($main_data)->result_array();
        return $data;
    }

    

    function get_test_drive_data($test_brand_data, $test_brand_model, $test_car_variant) {
        return $get = $this->db->query("SELECT pro_name_comp,(select brand_name from brand where variant.brand_id=brand.brand_id and brand_id='$test_brand_data') as brand_name,(select model_name from model where variant.model_id=model.model_id and model_id='$test_brand_model') as model_name FROM `variant` where brand_id='$test_brand_data' and model_id='$test_brand_model'")->row_array();
    }

    function get_product() {
        return $get = $this->db->query("SELECT pro_name_comp,variant_id FROM `variant` where status='1'")->result_array();
    }

    function coupon($pro_id, $main_city, $fuel_type, $color) {
        $start_date = date('Y-m-d');
        $car_coupon = '';
        $car_coupon.="SELECT * FROM `coupon` where coupon_used<=no_of_coupon";
        if ($start_date != '' && $start_date != '') {
            $car_coupon.=" and start_date<='$start_date' and end_date>='$start_date'";
        }
        return $all_result = $this->db->query($car_coupon)->result_array();


//            if($pro_id != ''){
//                $car_coupon.=" and variant_id='$pro_id'";
//            }
//            echo $car_coupon;
//            $variant_result=$this->db->query($car_coupon)->result_array(); 
//            if($main_city != '') {
//                $car_coupon.=" and city_id='$main_city'";
//            }
//           if($fuel_type !='') {
//                $car_coupon.=" and fuel='$fuel_type'";
//            }
//    
//            if($color !='') {
//                $car_coupon.=" and color='$color'";
//            }
//    echo $car_coupon;
//    echo '<pre>';
//    print_r($all_result);
//    print_r($variant_result);
//    echo '</pre>';
//    return '';    
    }

    function coupon_configuration($coupon_code, $pro_id, $city, $color) {
        $start_date = date('Y-m-d');

        $brand_res = "SELECT brand_id FROM `variant` where variant_id='$pro_id'";
        $brand_res = $this->db->query("SELECT brand_id FROM `variant` where variant_id='$pro_id'")->row_array();
        $brand_id = $brand_res['brand_id'];
        $coupon_checl=$this->db->query("SELECT * FROM `coupon` where ((coupn_code='$coupon_code'))")->num_rows();
        if($coupon_checl==0)
        {
        return '0';    
        }
        
        $coupon_checl2=$this->db->query("SELECT * FROM `coupon` where ((coupn_code='$coupon_code') and coupon_used>=no_of_coupon)")->num_rows();
        if($coupon_checl2==1)
        {
        return '1';    
        }
        
        $car_coupon='';
        $car_coupon.="SELECT * FROM `coupon` where (coupn_code='$coupon_code' and coupon_used<=no_of_coupon) and ";
        if($start_date!='') {
            $car_coupon.=" (start_date<='$start_date' and end_date>='$start_date')";
        }
//        echo $car_coupon;
        $get = $this->db->query($car_coupon)->result_array();
//        print_r($get);
//        exit;
        $counts=0;
        foreach ($get as $get_data) {
            $type = $get_data['type'];
            $exp_type = explode('-', $type);$tot=count($exp_type);
            if(in_array("1", $exp_type)){            
            if($city==$get_data['city_id'])
            {    
            $counts=$counts+1;
            }               
            }
            
            if(in_array("2", $exp_type)){            
            if($pro_id==$get_data['variant_id'])
            {    
            $counts=$counts+1;
            }               
            }
            
            if(in_array("3", $exp_type)){            
            if($brand_id==$get_data['brand'])
            {    
            $counts=$counts+1;
            }               
            }
            
            if(in_array("4", $exp_type)){            
            if($color==$get_data['color'])
            {    
            $counts=$counts+1;
            }               
            }
            if($tot==$counts)
            {
            $amount_percent = $get_data['amount_percent'];
            $coupon_id = $get_data['coupon_id'];
            $coupon_used = $get_data['coupon_used'];
            $coupon_material = $get_data['coupon_material'];
            if($coupon_material!='')
            {
            $coupon_material='1';    
            }    
            else 
            {
            $coupon_material='0';    
            }    
            
            $coupon_used = $coupon_used + 1;
            $cook_coupon_id = $pro_id . '_' . $coupon_id . '_' . $coupon_code;
//            $upd = $this->db->query("update `coupon` set coupon_used='$coupon_used' where coupon_id='$coupon_id'");
            setcookie('coupon_id', $cook_coupon_id, time() + (86400 * 30), "/");
            return '3';
            }
            else
            {
            return '4';    
            }    
        }
        return '5';
    }

    function coupon_discount($c_coupon_id) {
        return $check = $this->db->query("select * from `coupon` where coupon_id='$c_coupon_id'")->row_array();
    }

    function get_landing_data($final) {
        return $check = $this->db->query("select *,(select pro_image from variant where variant.variant_id=landing_page.product) as pro_image from `landing_page` where name like '%$final%'")->row_array();
//    echo $final;    
    }

    

    function get_coming_soon() {
        $estimated_price = str_replace('_', ' ', $_GET['estimated_price']);
        $launching_date = str_replace('_', ' ', $_GET['launching_date']);
        $brand_id = $_GET['brand_id'];
        $que.="select * from variant where upcoming='y' and status='3'";

        if ($estimated_price != '') {
            $que.=" and extimated_price='$estimated_price'";
        }

        if ($launching_date != '') {
            $que.=" and launching_date='$launching_date'";
        }
        if ($brand_id != '') {
            $que.=" and brand_id='$brand_id'";
        }
//    echo $que;
        $check = $this->db->query($que)->result_array();
        return $check;
    }

    function get_estimated_price() {
        $check = $this->db->query("select DISTINCT(extimated_price) as extimated_price from variant where extimated_price!=''")->result_array();
        return $check;
    }

    function get_estimated_lauch_date() {
        $check = $this->db->query("select DISTINCT(launching_date) as launching_date from variant where launching_date!=''")->result_array();
        return $check;
    }

}
