<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_feature extends CI_Model {
    
    public function common_city() {
        if ($_GET['city'] != '') {
//        get_city_id($city);    
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    function max_price($city) {
        $que = "";
        $que.="SELECT MAX(m.mnc_exshowroom_price) as max_price,MIN(m.mnc_exshowroom_price) as min_price FROM `mnc_exshowroom_price` m";
        if ($city != '') {
            $que.=" where m.city_id='$city'";
        }
        return $details = $this->db->query($que)->row_array();
    }

    function slider($city) {
        $city = $this->common_city();
        if($city=='')
        {
        $city='1';    
        }    
//        echo "select s.* from `slider` s,`city` c where s.city=c.name and c.city_id='$city'"; 
        $check = $this->db->query("select s.* from `slider` s,`city` c where s.city=c.name and c.city_id='$city' order by s.sort ASC")->result_array();
        return $check;
    }
    
    
     #### Home page featured product goes here #### 

    function featured($featured_type) {
        return $query = $this->db->query("SELECT f.`featured_id`, f.`prd_id_1`,f.`prd_id_2` FROM `featured` f WHERE f.`status`='1' and f.`featured_type`='$featured_type'")->result_array();
    }
    
    function featured_compare($featured_type) {
        $query = $this->db->query("SELECT f.`featured_id`, f.`prd_id_1`,f.`prd_id_2` FROM `featured` f WHERE f.`status`='1' and f.`featured_type`='$featured_type'")->result_array();
        foreach($query as $query_data)
        {
        $pro1=$query_data['prd_id_1'];    
        $pro2=$query_data['prd_id_2'];
        return $query_res = $this->db->query("select variant_id,pro_name_comp from variant where variant_id IN ('$pro1','$pro2')")->result_array();        
        }    
//        return $query = $this->db->query("SELECT v.pro_name_comp FROM `variant` v WHERE f.`status`='1' and f.`featured_type`='$featured_type'")->result_array();
    }
    
    
    ### Product Details Here ####
    function product_detail($pro_id, $comp_cond) {
        if($comp_cond == 1){
            $main_city = $this->common_city();
            if ($main_city == '') {
                $main_city = "0";
            }
            if ($pro_id != '') {
                $que = "SELECT v.pro_name,v.seating_capacity,v.pro_name_comp,v.city,v.fuel_type,v.tramission_type,v.body_color_id,v.pro_image,v.variant_id,v.pro_detail,m.model_name,v.pro_type,"
                        . "(select avg(total) from user_rating where pro_id='$pro_id') as user_rating,"
                        . "(select avg(total) from expert_rating where pro_id='$pro_id') as exp_rating,"
                        . "(select mnc_exshowroom_price from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as mnc_price,"
                        . "(select total_benefits from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits,"
                        . "(select total_benefits_perc from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_benefits_perc,"
                        . "(select total_savings from mnc_exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as total_savings,"
                        . "(select exshowroom_price from exshowroom_price where product_id='$pro_id' and city_id='$main_city' limit 1) as ex_price,"
                        . "(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                        . "(select m.model_name from model m where m.model_id=v.model_id) as model_name "
                        . "FROM `variant` v,`model` m "
                        . "WHERE m.model_id=v.model_id and v.variant_id='$pro_id' and v.variant_id!=''  and v.status='1'";
                return $query = $this->db->query($que)->result_array();
            }
        } else {
            return $query = $this->db->query("select v.pro_name,v.pro_name_comp,v.pro_image,v.variant_id,(select b.brand_name from brand b where b.brand_id=v.brand_id) as brand_name,(select m.model_name from model m where m.model_id=v.model_id) as model_name from variant v where v.variant_id='$pro_id'  and v.status='1'")->result_array();
        }
    }
    
    function expert_review_count($pro_id, $id) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select_avg('total');
        if ($id == 1) {
            return $get_data = $this->db->get('expert_rating')->result_array();
        }
        if ($id == 2) {
            return $get_data = $this->db->get('user_rating')->result_array();
        }
    }
    
    
    function home_seo_data($content, $id) {
        if ($id == 1) {
            return $get = $this->db->query("SELECT * FROM  `seo_tag` where page='$content' limit 0,1")->row_array();
        } else if ($id == 2) {
            return $get = $this->db->query("SELECT * FROM  `seo_tag` where page='$content' limit 1,1")->row_array();
        } else if ($id == 3) {
            return $get = $this->db->query("SELECT * FROM  `seo_tag` where page='$content' limit 1,1")->row_array();
        } else if ($id == 4) {
            return $get = $this->db->query("SELECT * FROM  `seo_tag` where page='$content'")->row_array();
        }
    }
    
    #### Hot Deal section here
    
    function home_page_carousel($brand_id, $id) {

        if ($id == 1) {
            $m_q = "SELECT distinct(`model_id`) as model_id FROM `variant` WHERE  status='1' ";
            if ($brand_id != "") {
                $m_q.=" and `brand_id`='$brand_id'";
            }
            $m_q_d = $this->db->query($m_q)->result_array();
            $res = '';
            $final_data = array();
            foreach ($m_q_d as $m_q_data) {
                $m_qs = '';
                $model_id = $m_q_data['model_id'];
                $m_qs.="SELECT v.`brand_id`,v.`model_id`,v.`variant_id`,v.`pro_name`,v.`pro_name_comp`,v.`pro_image`,(select b.model_name from model b where b.model_id=v.model_id) as model_name FROM `variant` v WHERE 1=1 and v.model_id ='$model_id' and v.status='1' limit 1";
                $data1 = $this->db->query($m_qs)->result_array();
                $final_data[] = $data1[0];
            }
            return($final_data);
        } else if ($id == 2) {
            $main_city = $this->common_city();
            $m_q = "SELECT h.valid_until,h.percent,h.variant_id,v.pro_name_comp,v.pro_image,"
                    . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city' limit 1) as exshowroom_price,"
                    . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as mnc_exshowroom_price,"
                    . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as total_benefits,"
                    . "(select m.total_benefits_perc from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as total_benefits_perc,"
                    . "(select m.finance_benefit from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as finance_benefit,"
                    . "(select m.exchange_benefit from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as exchange_benefit,"
                    . "(select m.accessory_package from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as accessory_package,"
                    . "(select m.accessory_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city'  limit 1) as accessory_benefits"
                    . " FROM `hot_deals` h,variant v  WHERE 1=1 and v.variant_id=h.variant_id and v.brand_id='$brand_id' and h.city_id='$main_city' and v.status='1'";
            return $m_q_d = $this->db->query($m_q)->result_array();
        }
    }
    
    function home_page_carousel_exshow($product_id) {
        $main_city = $this->common_city();
        $data.='';
        $data.="SELECT m.*,e.exshowroom_price FROM `mnc_exshowroom_price` m,`exshowroom_price` e "
                . "where m.product_id='$product_id' and m.product_id=e.product_id";
        if ($main_city != '') {
            $data.=" and m.city_id='$main_city' and e.city_id='$main_city'";
        }

        return $query2 = $this->db->query($data)->result_array();
    }
    
    function get_total_saving($id, $user_id) {
        $main_city = $this->common_city();
        return $sel = $this->db->query("select m.product_id,m.city_id,m.cash_discount,m.insurance_discount,m.exchange_bonus,m.loyality_bonus,m.corporate_discount,m.accessory_discount,m.special_reward,m.accessory_package from mnc_exshowroom_price m where m.product_id='$id' and m.city_id='$main_city'")->row_array();
    }
    
    


}
