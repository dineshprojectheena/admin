<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_section extends CI_Model {

    function get_brands($id) {
        return $query = $this->db->query("SELECT `brand_id`,`brand_name`,`brand_image` FROM `brand` WHERE `status`='1' order by brand_name")->result_array();
    }

    function get_lease_brands($id) {
        return $query = $this->db->query("SELECT `brand_id`,`brand_name`,`brand_image` FROM `brand` WHERE `status` IN ('1','3') order by brand_name")->result_array();
    }

    

    function all_citys_detalis() {
        return $query = $this->db->query("SELECT name,city_id FROM `city` Order By name ASC")->result_array();
    }

    function all_fuel() {
        return $get_data = $this->db->query('select fuel_type_id,fuel_type from fuel_type')->result_array();
    }

    function comparison_model_data($brand_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `model` WHERE `brand_id`='$brand_data' and status='1' ORDER BY `model_name` ASC")->result_array();
    }
    
    function comparison_lease_model_data($brand_data) {
        return $car_intro_detail = $this->db->query("SELECT * FROM `model` WHERE `brand_id`='$brand_data' and status IN ('1','3') ORDER BY `model_name` ASC")->result_array();
    }

    function get_single_city($city)
    {
        return $detail=$this->db->query("SELECT * FROM `city` WHERE `city_id`='$city'")->row_array();
    }

    function get_brand_model($carbrands,$brand_model)
    {
        return $detail=$this->db->query("SELECT model_id,model_name,(select brand_name from brand where brand_id='$carbrands') as brand_name FROM `model` WHERE `model_id`='$brand_model'")->row_array();
    }

    function comparison_variant_data($brand_data, $brand_model) {
        return $car_intro_detail = $this->db->query("SELECT variant_id,pro_name,fuel_type,pro_name_comp FROM `variant` WHERE `model_id`='$brand_model' and `brand_id`='$brand_data'  and status='1'")->result_array();
    }

    function get_test_drive_data($test_brand_data, $test_brand_model, $test_car_variant) {
        return $get = $this->db->query("SELECT pro_name_comp,(select brand_name from brand where variant.brand_id=brand.brand_id and brand_id='$test_brand_data') as brand_name,(select model_name from model where variant.model_id=model.model_id and model_id='$test_brand_model') as model_name FROM `variant` where brand_id='$test_brand_data' and model_id='$test_brand_model'")->row_array();
    }
    
    
    function affiliate_data($peramter) {
        return $get = $this->db->query("SELECT affiliate_id FROM `affiliate` where code='$peramter'")->row_array();
    }
    
    
    

    function test_drive_data_upload($brand_name, $model_name, $test_date, $test_fuel_type, $test_full_name, $test_email, $test_location, $test_phone) {
        $date = date('Y-m-d h:i:s');
        $upload= $this->db->query("INSERT INTO `test_drive_enquiry` SET
`test_drive_enquiry_id` = '',
`brand_name` = '$brand_name',
`model_name` = '$model_name',
`date` = '$test_date',
`fuel` = '$test_fuel_type',
`full_name` = '$test_full_name',
`email` = '$test_email',
`location` = '$test_location',
`phone` = '$test_phone',
`added_date` = '$date'");
        return $enq_id = $this->db->insert_id();
        
    }

    function get_city() {
        $this->db->select('city_id,name');
        $query = $this->db->get('city');
        return $query->result_array();
    }

    function get_state() {
        $this->db->select('state_id,name');
        $query = $this->db->get('state');
        return $query->result_array();
    }

    function search_car_res($id){
        return $sel = $this->db->query("select v.variant_id,v.pro_image,pro_name_comp from variant v where v.pro_name_comp like '%$id%' and v.status='1' order by v.variant_id LIMIT 5")->result_array();
    }
    
    
    function search_car_data($id,$city){
        
        return $sel = $this->db->query("select v.variant_id,v.pro_image,v.pro_name_comp,"
                . "(select ex.exshowroom_price from exshowroom_price ex where ex.product_id=v.variant_id and ex.city_id='$city' limit 1) as exshowroom_price,"
                . "(select mnc.total_savings from mnc_exshowroom_price mnc where mnc.product_id=v.variant_id and mnc.city_id='$city' limit 1) as total_savings, "
                . "(select hd.valid_until from hot_deals hd where hd.variant_id=v.variant_id and hd.city_id='$city' limit 1) as valid_until "
                . "from variant v where v.pro_name_comp like '%$id%' and v.status='1' order by v.variant_id")->result_array();
    }
    
    

    function bank_per() {
        return $upd = $this->db->query("SELECT * FROM `bank_per`")->result_array();
    }

    function add_emi($emi, $pro_id, $user_id, $down_payment, $loan1, $bank, $rate1, $months1) {
        $del = $this->db->query("delete from  `emi_calculation` where `pro_id`='$pro_id' and `user_id`='$user_id' and order_id=''");
        $upd = $this->db->query("INSERT INTO `emi_calculation`(`emi_id`, `emi_amount`, `pro_id`, `user_id`, `down_payment`, `load_amaount`, `bank`, `int_rate`, `tenure`, `added_date`) "
                . "VALUES ('','$emi','$pro_id','$user_id','$down_payment','$loan1','$bank','$rate1','$months1','')");

        return $order_id = $this->db->insert_id();
    }

}
