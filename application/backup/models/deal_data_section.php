<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class deal_data_section extends CI_Model {
    
    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }
    
    function deal_data() {
        $main_city = $this->common_city();
        $data = $this->db->query("SELECT brand_name,brand_id FROM brand where status='1'")->result_array();
        $final = array();
        foreach ($data as $data_res) {
            $date = date('Y-m-d');
            $brand_id = $data_res['brand_id'];
            $deals_data = $this->db->query("SELECT d.*,b.start_date,d.end_date,v.pro_name_comp,v.pro_image,"
                            . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                            . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                            . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                            . " FROM deal d,brand_deal b,variant v where d.brand_id='$brand_id' and d.brand_id=b.brand_id and b.city_id='$main_city' and v.variant_id=d.variant_id and Date(d.end_date) >= '" . date('Y-m-d') . "'  and v.status='1'")->result_array();
            $deal = array('deal' => $deals_data);
            $merge = array_merge($data_res, $deal);
            $final[] = $merge;
        }

        return ($final);
    }
    
    function hot_deal_data2() {
        $sort = $_GET['sort'];
        $type = $_GET['type'];
        $brand = $_GET['brand'];
        if ($brand != '') {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where brand_name='$brand'")->row_array();
            $brand_id = "'" . $brand['brand_id'] . "'";
        } else {
            $brand = $this->db->query("SELECT brand_name,brand_id from brand where status='1'")->result_array();
            $res = '';
            foreach ($brand as $brand_res) {
                $res = $res . "'" . $brand_res['brand_id'] . "',";
            }
            $brand_id = rtrim($res, ",");
        }
        $main_city = $this->input->cookie('main_city', TRUE);
        $res = array();
        $main_data = '';
        $main_data.="SELECT h.*,v.pro_name_comp,v.pro_image,"
                . "(select brand_name from brand b where b.brand_id=v.brand_id) as brand_name,"
                . "(select m.mnc_exshowroom_price from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as mnc_exshowroom_price,"
                . "(select m.total_benefits from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits,"
                . "(select m.total_savings from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_savings,"
                . "(select m.total_benefits_perc from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as total_benefits_perc,"
                . "(select m.finance_benefit from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as finance_benefit,"
                . "(select m.accessory_package from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as accessory_package,"
                . "(select m.extended_warranty from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as extended_warranty,"
                . "(select m.accessory_discount from mnc_exshowroom_price m where m.product_id=v.variant_id and m.city_id='$main_city') as accessory_discount,"
                . "(select e.exshowroom_price from exshowroom_price e where e.product_id=v.variant_id and e.city_id='$main_city') as exshowroom_price"
                . " FROM hot_deals h,variant v where v.variant_id=h.variant_id and v.brand_id IN ($brand_id)  and v.status='1'";
        if ($type != '') {
            $main_data.=" and v.pro_type='$type'";
        }

        $main_data.=" order by brand_id ASC";
        $main_data;
        $data = $this->db->query($main_data)->result_array();
        return $data;
    }
    
    function get_car_type() {
        return $query = $this->db->query("SELECT distinct(pro_type) as pro_type FROM `variant` where status='1'")->result_array();
    }
    
}
