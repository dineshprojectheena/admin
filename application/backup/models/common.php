<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common extends CI_Model {

    function get_city()
    {    
        $this->db->select('city_id,name');
        $query = $this->db->get('city');
        return $query->result_array();
    }
    
    function get_state()
    {    
        $this->db->select('state_id,name');
        $query = $this->db->get('state');
        return $query->result_array();
    }
    
}
