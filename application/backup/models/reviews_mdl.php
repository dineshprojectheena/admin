<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reviews_mdl extends CI_Model {
    /* user review goes here */

    function user_review($data) {
        $get_data = $this->db->insert('user_rating', $data);
        return;
    }

    /* user review goes here */

    /* user review goes here */

    function get_review($pro_id, $user_sort, $id, $pag) {
//        $this->db->where('pro_id', $pro_id);
        $que="";
         $que.="select u.user_id,u.user_rating_id,u.date,heading,u.what_i_like,u.what_i_dont_like,u.total,(select a.fb_id from user a where a.user_id=u.user_id) as fb_link from user_rating u where pro_id='$pro_id' ";
//        $this->db->select('user_id,user_rating_id,date,heading,what_i_like,what_i_dont_like,total');
        
        if ($user_sort == 'Rating') {
            $que.="order by total,user_rating_id DESC";
//            $this->db->order_by("total", "desc");
        }
        else if ($user_sort == 'Date') {
            $que.="order by date,user_rating_id DESC";
//            $this->db->order_by("date", "desc");
        }
        else{
            $que.="order by user_rating_id DESC";
//            $this->db->order_by("date", "desc");
        }
        
//        echo $que;
//        $this->db->order_by("user_rating_id","desc");
        if ($id == 1) {
            return $get_data = $this->db->query($que)->result_array();            
        } else {
            return $get_data = $this->db->query($que)->num_rows();
//            return $tot = $get_data->num_rows();
        }
    }

    function get_exp_review($pro_id, $user_sort, $id) {

        $this->db->where('pro_id', $pro_id);
        $this->db->select('expert_rating_id,date,heading,what_i_like,what_i_dont_like,total,profile_id');
        if ($user_sort == 'Rating') {
            $this->db->order_by("total", "desc");
        }
        if ($user_sort == 'Date') {
            $this->db->order_by("date", "desc");            
        }
        $this->db->order_by("expert_rating_id","desc");
        if ($id == 1) {
              return $get_data = $this->db->get('expert_rating')->result_array();
        } else {
            $get_data = $this->db->get('expert_rating');
            return $tot = $get_data->num_rows();
        }
    }
    
    function get_user_review_img($user_id)
    {
    return $this->db->query("SELECT fb_id FROM `user` where user_id='$user_id'")->result_array();           
    }
    
    function get_tot_review($pro_id, $type) {
        if ($type == "expert") {
            $this->db->where('pro_id', $pro_id);
            $this->db->select_avg('total');
            $get_data = $this->db->get('expert_rating');
        }

        if ($get_data->num_rows() > 0) {
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $get;
    }

    function tot_user_reviews($pro_id, $id) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select_avg('total');
        if ($id == 1) {
            $get_data = $this->db->get('expert_rating');
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        if ($id == 2) {
            $get_data = $this->db->get('user_rating');
            foreach ($get_data->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    
    function reting_product_detail($pro_id) {
        return $query = $this->db->query("SELECT pro_name_comp,pro_name,pro_image,variant_id,pro_detail,upcoming,launching_date FROM `variant` WHERE variant_id='$pro_id'  and status='1'")->result_array();
    }
    
    function rating_details($id) {
        $get_data = $this->db->query("SELECT pro_name,variant_id,model_id,brand_id,pro_name_comp FROM `variant` where variant_id='$id'  and status='1'")->result_array();
        $model_id = $get_data[0]['model_id'];
        $brand_id = $get_data[0]['brand_id'];
        return $all_res = $this->db->query("SELECT v.pro_name,v.pro_name_comp,v.variant_id,(select brand_name from brand where brand_id='$brand_id') as brand_name,(select model_name from model where model_id='$model_id') as model_name FROM `variant` v where v.brand_id='$brand_id' and v.model_id='$model_id'  and v.status='1'")->result_array();
    }
    
    function expert_review_count($pro_id, $id) {
        $this->db->where('pro_id', $pro_id);
        $this->db->select_avg('total');
        if ($id == 1) {
            return $get_data = $this->db->get('expert_rating')->result_array();
        }
        if ($id == 2) {
            return $get_data = $this->db->get('user_rating')->result_array();
        }
    }
    
    
    

    /* user review goes here */
}
