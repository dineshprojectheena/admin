<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Coming_soon_section extends CI_Model {
    
     public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    function get_coming_soon() {
        $estimated_price = str_replace('_', ' ', $_GET['estimated_price']);
        $exp_estimated_price=explode('-',$estimated_price);
//        print_r($exp_estimated_price);
        $launching_date = str_replace('_', ' ', $_GET['launching_date']);
        $brand_id = $_GET['brand_id'];
        $que.="select * from variant where upcoming='y' and status='3'";

        if (!empty($exp_estimated_price[0]) && !empty($exp_estimated_price[1])){
            $que.=" and extimated_price >= '$exp_estimated_price[0]' and extimated_price_max<= '$exp_estimated_price[1]'";
        }

        if ($launching_date != '') {
            $que.=" and launching_date='$launching_date'";
        }
        if ($brand_id != '') {
            $que.=" and brand_id='$brand_id'";
        }
        
//    echo $que;
        $check = $this->db->query($que)->result_array();
        return $check;
    }
    
    function get_estimated_price($id) {
        
        if($id==1)
        {    
        $check = $this->db->query("select DISTINCT(extimated_price) as extimated_price from variant where extimated_price!=''")->result_array();
        }
        else
        {    
        $check = $this->db->query("select DISTINCT(extimated_price_max) as extimated_price_max from variant where extimated_price_max!=''")->result_array();
        }
        
        return $check;
    }

    function checl_estimated_price($id,$estimated_price) {
        
        if($id==1)
        {    
        $check = $this->db->query("select DISTINCT(extimated_price_max) as extimated_price_max from variant where extimated_price_max!='' and extimated_price_max < '$estimated_price'")->result_array();
        }
        if($id==2)
        {    
        $check = $this->db->query("select DISTINCT(extimated_price_max) as extimated_price_max from variant where extimated_price_max!='' and extimated_price_max >= '$estimated_price'")->result_array();
        }
//        else
//        {    
//        $check = $this->db->query("select DISTINCT(extimated_price_max) as extimated_price_max from variant where extimated_price_max!=''")->result_array();
//        }
        
        return $check;
    }

    function get_estimated_lauch_date() {
        $check = $this->db->query("select DISTINCT(launching_date) as launching_date from variant where launching_date!=''")->result_array();
        return $check;
    }
    

}
