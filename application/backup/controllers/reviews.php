<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reviews extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('login_authorization');
        $this->load->model('deal_data_section');
        $this->output->nocache();
        $this->load->library('form_validation');
        $this->load->model('Reviews_mdl');
    }

    function user_review($id = '') {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }


        $data['session'] = $this->session->userdata('logged_in_user');


        $data['main_id'] = $id;
        $exp_id = explode('-', $id);
        $tot = count($exp_id);
        $data['id'] = $exp_id[$tot - 1];
//        $data['id']=substr($id,-1);
//        exit;
        if (empty($data['id'])) {
            $data['id'] = $this->input->post('pro_id');
        }
        $data['content'] = 'userReviewPage';
        $data['exp_review_count'] = $this->Reviews_mdl->expert_review_count($data['id'], $id = '1');
        $data['user_review_count'] = $this->Reviews_mdl->expert_review_count($data['id'], $id = '2');
        $data['pro_details'] = $this->Reviews_mdl->reting_product_detail($data['id']);
        $data['pro_ext'] = $this->Reviews_mdl->rating_details($data['id']);
        $this->load->view('index', $data);
    }

    function review() {
        if (isset($_POST['review_submit'])) {
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('comment', 'heading', 'required');
            $this->form_validation->set_rules('like', 'I like', 'trim|required|xss_clean');
            $this->form_validation->set_rules('dislike', "I Don't like", 'trim|required|xss_clean');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->user_review();
            } else {
                $rating = $this->input->post('rating');
                $main_id = $this->input->post('main_id');
                $tot = count($rating);
                $gt = ($rating[0] + $rating[1] + $rating[2] + $rating[3] + $rating[4] + $rating[5] + $rating[6] + $rating[7] + $rating[8]) / $tot;
                $rec_data = array(
                    'pro_id' => $this->input->post('pro_id'),
                    'pro_type' => $this->input->post('pro_type'),
                    'user_id' => $this->input->post('user_id'),
                    'date' => date('Y-m-d'),
                    'heading' => $this->input->post('comment'),
                    'what_i_like' => $this->input->post('like'),
                    'what_i_dont_like' => $this->input->post('dislike'),
                    'comfort' => $rating['0'],
                    'space' => $rating['1'],
                    'safety' => $rating['2'],
                    'design' => $rating['3'],
                    'engine' => $rating['4'],
                    'fuel_economy' => $rating['5'],
                    'ride_handling' => $rating['6'],
                    'maintainence' => $rating['7'],
                    'value_for_money' => $rating['8'],
                    'total' => $gt,
                    'added_date' => date('Y-m-d h:i:s')
                );
                $query = $this->Reviews_mdl->user_review($rec_data);
                $this->session->set_flashdata('user_rating_sucess', "user_rating_sucess");
                redirect(base_url() . 'car/' . $main_id);
            }
        } else {
            redirect('user-review');
        }
    }

    function load_reviews() {
        $data['session'] = $this->session->userdata("logged_in_user");
//        $img = base_url() . '/uploads/user/' . $data['session']['fb_id'] . '.jpg';
        $pro_id = $this->input->post('pro_id');
        $user_sort = $this->input->post('user_sort');
        $res = $this->Reviews_mdl->get_review($pro_id, $user_sort, $id = '1');

        $tot = $this->Reviews_mdl->get_review($pro_id, $user_sort, $id = '2');
        echo $tot_rev = $tot . '-Total';
        if ($tot > 0){
            $i = 1;
            ?>
            <div itemprop="review" itemscope itemtype="http://schema.org/Review">
                <?php
                foreach ($res as $data) {
                    $posted_date = $data['date'];
                    $img=base_url().'/uploads/user/'.$data['fb_link'];
                    ?>
                    <tr>
                        <td>
                            <div class="row user-comment-box2" >
                                <?php
//                         print_r($data);                         
                                ?>
                                <div  class="col-md-2 col-sm-2 text-center">
                                    <img src="<?php echo $img; ?>" style="max-width: 85px;" class="img-circle img-thumbnail img-center">
                                    <div class="rating-div">
                                        <span class="text-golden">
                                            <?php
                                            $star = $data['total'];
                                            $exp = explode('.', $star);
                                            $point = $exp[1];
                                            if ($point > 00) {
                                                $half = '-half-full';
                                            } else if ($point == 00) {
                                                $half = '-o';
                                            } else {
                                                $half = '';
                                            }

                                            if (empty($star)) {
                                                ?>
                                                <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                <?php
                                            }

                                            if ($exp[0] == 1) {
                                                ?> 
                                                <i class="fa fa-star"></i><?php ?>
                                                <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                <?php
                                            } else if ($exp[0] == 2) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                            } else if ($exp[0] == 3) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                            } else if ($exp[0] == 4) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                            }
                                            ?>
                                        </span>
                                        <p class="date-p" itemprop="datePublished" content="<?php echo $posted_date;?>">Posted On:<br> 
                                            <?php echo $posted_date;?>
                                        </p>
                                    </div>                               
                                </div> 
                                <div class="col-md-10 col-sm-10">
                                    <h4 class="user-comment-heading left"> <?php echo $data['heading']; ?>
                                        <?php
                                        if ($i == 1) {
                                            $class = '';
                                            ?>
                                            <span>[ - Less ]</span>
                                            <?php
                                        } else {
                                            $class = 'hide';
                                            ?>    
                                            <span>[ + More ]</span>  
                                            <?php
                                        }
                                        ?>
                                    </h4>
                                    <br>
                                    <br>
                                    <div itemprop="description">
                                        <div class="clearfix">
                                            <div style="margin-right:10px;" class="pull-left">
                                                <i title="" data-placement="right" data-toggle="tooltip" class="fa fa-thumbs-up fa-2x tiphere" data-original-title="What Users Like"></i>
                                            </div>
                                            <div style="margin-right:10px;" >
                                                <p style="text-align:justify;">
                                                    <?php echo $data['what_i_like']; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div style="margin-right:10px;" class="pull-left">
                                                <i title="" data-placement="right" data-toggle="tooltip" class="fa fa-thumbs-down fa-2x tiphere" data-original-title="What Users Disliked"></i>
                                            </div>
                                            <div style="margin-right:10px;" >
                                                <p style="text-align:justify;">
                                                    <?php echo $data['what_i_dont_like']; ?>
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>  
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
            } else {
                $userdata = $this->session->userdata('logged_in_user');
                if(empty($userdata))
                {    
                ?>
                <div align="center">
                    <a id="a1" href="#" onclick="set_redirection('1');" data-toggle="modal" style="color:black;" data-target="#loginModal" >No User reviews available. Click here to submit your review.</a>
                </div>
                <?php
                }
            }
            ?>
        </div>        
        <script>
            $('.user-comment-box2').click(function () {
                //                    alert('sdsdsd');
                $(this).find('.user-comment').toggleClass('hide');
                if ($(this).find('.user-comment').hasClass('hide')) {
                    $(this).find('.user-comment-heading span').html('[ + More ]');
                } else {
                    $(this).find('.user-comment-heading span').html('[ - Less ]');
                }

            });
        </script>           
        <?php
    }

    function load_exp_reviews() {
        $pro_id = $this->input->post('pro_id');
        $user_sort = $this->input->post('user_sort');
        $pag = $this->input->post('pag');
        $res = $this->Reviews_mdl->get_exp_review($pro_id, $user_sort, $id = '1');



        $tot = $this->Reviews_mdl->get_exp_review($pro_id, $user_sort, $id = '2');
        echo $tot_rev = $tot . '-Total';
        if ($tot > 0) {
            $i = '1';
            ?>
            <?php
            foreach ($res as $data) {
                $posted_date = $data['date'];
                ?>
                <tr>
                    <td  style="text-align: left !important;">
                        <div class="row user-comment-box3">
                           <div style="" class="col-md-2 col-sm-2 text-center">
                                <img src="<?php echo img_url('uploads/expert/'.$data['profile_id']);?>" style="max-width: 85px;" class="img-circle img-thumbnail img-center">
                                <div class="rating-div">
                                    <span class="text-maroon">
                                        <?php
                                        echo $star = $data['total'];
                                        $exp = explode('.', $star);
                                        $point = $exp[1];
                                        if ($point > 00) {
                                            $half = '-half-full';
                                        } else if ($point == 00) {
                                            $half = '-o';
                                        } else {
                                            $half = '';
                                        }
                                        if (empty($star)) {
                                            ?>
                                            <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        }

                                        if ($exp[0] == 1) {
                                            ?>
                                            <i class="fa fa-star"></i><?php
                                            ?>
                                            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        } else if ($exp[0] == 2) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 3) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 4) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                        }
                                        ?>
                                    </span>
                                    <p class="date-p">Posted On:<br> <?php
                                        echo $posted_date;
                                        ?></p>
                                </div>
                            </div>


                            <div class="user-details col-md-10 col-sm-10">
                                <h4 class="user-comment-heading"> 
                                    <?php echo $data['heading']; ?>
                                    <?php
                                    if ($i == 1) {
                                        $class = '';
                                        ?>
                                        <span>[ - Less ]</span>
                                        <?php
                                    } else {
                                        $class = 'hide';
                                        ?>    
                                        <span>[ + More ]</span>  
                                        <?php
                                    }
                                    ?>
                                </h4>
                                <div  class="<?php echo $class; ?>">
                                    <div class="clearfix">
                                        <div style="width: 4%;" class="pull-left">
                                            <i title="" data-placement="right" data-toggle="tooltip" class="fa fa-thumbs-up fa-2x tiphere" data-original-title="What Users Like"></i>
                                        </div>
                                        <div style="width: 96%;" class="pull-right">
                                            <p>
                                                <?php echo $data['what_i_like']; ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div style="width: 4%;" class="pull-left">
                                            <i title="" data-placement="right" data-toggle="tooltip" class="fa fa-thumbs-down fa-2x tiphere" data-original-title="What Users Disliked"></i>
                                        </div>
                                        <div style="width: 96%;" class="pull-right">
                                            <p>
                                                <?php echo $data['what_i_dont_like']; ?>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                <!--                                <p class="review-count">
                                    <strong><?php echo $tot; ?></strong> Users found this review Helpful
                                </p>-->
                            </div>


                        </div>
                    </td>
                </tr>

                <?php
                $i++;
            }
            ?>
            <script>
                $('.user-comment-box3').click(function () {
                    //                    alert('sdsdsd');
                    $(this).find('.user-comment').toggleClass('hide');
                    if ($(this).find('.user-comment').hasClass('hide')) {
                        $(this).find('.user-comment-heading span').html('[ + More ]');
                    } else {
                        $(this).find('.user-comment-heading span').html('[ - Less ]');
                    }

                });
            </script>   
            <?php
        } else {
            echo '<div align="center"><a href="#" style="text-decoration:none;">There are no expert reviews yet.</a></div>';
        }
    }

    function tot_exp_reviews() {
        $pro_id = $this->input->post('pro_id');
        $type = $this->input->post('type');
        $res = $this->Reviews_mdl->get_tot_review($pro_id, $type);
        echo count($res);
    }

    function tot_user_reviews() {
        $pro_id = $this->input->post('pro_id');
        $user_res = $this->Reviews_mdl->tot_user_reviews($pro_id, $id = '1');
        $exp_res = $this->Reviews_mdl->tot_user_reviews($pro_id, $id = '2');
        #### user wise product rating result
        foreach ($user_res as $data)
            $star = $data->total;
        ?>
        <?php
        $exp = explode('.', $star);
        $point = $exp[1];
        if ($point > 00) {
            $half = '-half-full';
        } else if ($point == 00) {
            $half = '-o';
        } else {
            $half = '';
        }

        if ($exp[0] == 1) {
            ?>
            <i class="fa fa-star"></i><?php
            ?>
            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
            <?php
        } else if ($exp[0] == 2) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
        } else if ($exp[0] == 3) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
        } else if ($exp[0] == 4) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
        }

        #### expert wise product rating result

        echo 'exp_result';

        foreach ($exp_res as $data2)
            $star2 = $data2->total;
        ?>
        <?php
        $exp = explode('.', $star2);
        $point = $exp[1];
        if ($point > 00) {
            $half = '-half-full';
        } else if ($point == 00) {
            $half = '-o';
        } else {
            $half = '';
        }

        if ($exp[0] == 1) {
            ?>
            <i class="fa fa-star"></i><?php
            ?>
            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
            <?php
        } else if ($exp[0] == 2) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
        } else if ($exp[0] == 3) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
        } else if ($exp[0] == 4) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
        }
    }

}
