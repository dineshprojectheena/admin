<?php

require(APPPATH . '/libraries/REST_Controller.php');

class Api extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('api_home_section');
    }

    function city_get() {
        $data['all_main_Citys'] = $this->api_home_section->all_citys_detalis();
        $data = $data['all_main_Citys'];
        $this->response($data);
    }

    function brand_get() {
        $data['all_brands'] = $this->api_home_section->get_brands($id = '');
        $data = $data['all_brands'];
        $this->response($data);
    }

//    function brands_get() {
//        $data['all_brands'] = $this->api_home_section->get_brands($id = '');
//        $data = $data['all_brands'];
//        $this->response($data);
//    }





    function car_type_get() {
        $data['all_car_type'] = $this->api_home_section->get_car_type();
        $data = $data['all_car_type'];
        $this->response($data);
    }

    function slider_get() {
        $city_id = $this->get('city');
        $data['all_brands'] = $this->api_home_section->get_slider($city_id);
        $data = $data['all_brands'];
        $this->response($data);
    }

    function brand_model_get() {
        $brand_id = $this->get('id');
        $data['model_data'] = $this->api_home_section->get_rev_model($brand_id);
//        $data =$this->get('id');
        $data = $data['model_data'];
        $this->response($data);
    }

    function brand_model_variant_get() {
        $brand_id = $this->get('brand_id');
        $model_id = $this->get('model_id');
        $data['var_data'] = $this->api_home_section->get_rev_variant($brand_id, $model_id);
//        $data =$this->get('id');
        $data = $data['var_data'];
        $this->response($data);
    }

    function variant_get() {

        $data = $data['var_data'];
        $this->response($data);
    }

    function max_min_price_get() {
        $city_id = $this->get('city');
        $data['max_price_res'] = $this->api_home_section->max_price($city_id);

        $data = $data['max_price_res'];
        $this->response($data);
    }

    function brand_hot_deal_get() {
        $brand_id = $this->get('brand_id');
        $city_id = $this->get('city_id');
        $data['data'] = $this->api_home_section->home_page_carousel($brand_id, $city_id);
        $data = $data['data'];
        $this->response($data);
    }

    function hot_deals_get() {
        $brand_id = $this->get('brand_id');
        $city_id = $this->get('city_id');
        $sort=$this->get('sort');
        $pro_type=$this->get('pro_type');
        $hot_deals_data = $this->api_home_section->hot_deal_res($brand_id, $city_id,$pro_type);

        foreach ($hot_deals_data as $c => $key) {
            $sort_mnc_exshowroom_price[] = $key['mnc_exshowroom_price'];
        }
        if($sort=='DESC'){
        array_multisort($sort_mnc_exshowroom_price, SORT_DESC, $hot_deals_data);
        }else{
        array_multisort($sort_mnc_exshowroom_price, SORT_ASC, $hot_deals_data);
        }
 
        $data = $hot_deals_data;
        $this->response($data);
    }
    
    
    function pro_specification_get(){
        $product_id = $this->get('pro_id');
        $data['pro_specification']=$this->api_home_section->pro_specification_res($product_id);
        echo $this->load->view('api/specification', $data, TRUE);        
    }
    
    
    
    function pro_feature_get(){
        $product_id = $this->get('pro_id');
        $data['pro_feture']=$this->api_home_section->pro_feature_res($product_id);
        echo $this->load->view('api/feature', $data, TRUE);        
    }
    
    
    
    

    function product_details_get(){
        $product_id = $this->get('product_id');
        $city_id = $this->get('city_id');
        $data['data'] = $this->api_home_section->product_detail($product_id, $city_id);
        $data = $data['data'];
        $this->response($data);
    }
    
    function upcoming_cars_get(){
        $estimated_price = str_replace('_', ' ', $_GET['estimated_price']);
        
        $exp_estimated_price=explode('-',$estimated_price);
        $launching_date = str_replace('_', ' ', $_GET['launching_date']);
        $brand_id = $_GET['brand_id'];
        
        
        $city_id = $this->get('city_id');
        $data['all_coming_soon'] = $this->api_home_section->get_coming_soon();
        $data=$data['all_coming_soon'];
        $this->response($data);
    }

    function product_specification_get() {
        $product_id = $this->get('product_id');

        $data['pro_specification'] = $this->api_home_section->pro_specification($product_id);
        $data = $data['pro_specification'];
        $this->response($data);
    }

    function product_feature_get() {
        $product_id = $this->get('product_id');
        $data['pro_feture'] = $this->api_home_section->product_features($product_id);
        $data = $data['pro_feture'];
        $this->response($data);
    }

    function user_review_get() {
        $product_id = $this->get('product_id');
        $data['user_review'] = $this->api_home_section->get_review($product_id);
        $data = $data['user_review'];
        $this->response($data);
    }

    function expert_review_get() {
        $product_id = $this->get('product_id');
        $data['expert_review'] = $this->api_home_section->get_exp_review($product_id);
        $data = $data['expert_review'];
        $this->response($data);
    }

    function gallery_get() {
        $product_id = $this->get('product_id');
        $data['gallery'] = $this->api_home_section->car_gallery_detail($product_id);
        $data = $data['gallery'];
        $this->response($data);
    }

    function color_get() {
        $product_id = $this->get('product_id');
        $data['car_color'] = $this->api_home_section->car_specifiaction($product_id, $type = 'color');
        $data = $data['car_color'];
        $this->response($data);
    }

    function accessory_get() {
        $product_id = $this->get('product_id');
        $data[] = $this->api_home_section->accessory_services($product_id, $id = '1');
        $data[] = $this->api_home_section->accessory_services($product_id, $id = '2');
        $data[] = $this->api_home_section->accessory_services($product_id, $id = '3');
        $data[] = $this->api_home_section->accessory_services($product_id, $id = '4');
        $data[] = $this->api_home_section->accessory_services($product_id, $id = '5');
        $data[] = $this->api_home_section->accessory_services($product_id, $id = '6');
        $this->response($data);
    }

    function total_saving_get() {
        $product_id = $this->get('product_id');
        $city = $this->get('city');
        $data['total_saving'] = $this->api_home_section->get_total_saving($product_id, $city);
        $data = $data['total_saving'];
        $this->response($data);
    }

    function onroad_price_get() {
        $product_id = $this->get('product_id');
        $city = $this->get('city');
        $data['onroad_price_breakup'] = $this->api_home_section->get_onroad_price($product_id, $city);
        $data = $data['onroad_price_breakup'];
        $this->response($data);
    }

    function same_variants_get() {
        $product_id = $this->get('product_id');
        $data['same_variants'] = $this->api_home_section->rating_details($product_id);
        $data = $data['same_variants'];
        $this->response($data);
    }

    function comparison_detail_get() {
        $product_id = $this->get('product_id');
        $city_id = $this->get('city');
        $exp_car = explode('_', $product_id);
        $product_id = array();
        $product_details1 = array();
        $pro_ids = '';
        for ($i = 0; $i < count($exp_car); $i++) {
            $exp_id = explode('-', $exp_car[$i]);
            $tot = count($exp_id);
            $pro = $exp_id[$tot - 1];
            $pro_id = "'" . $pro . "'";
            $pro_ids = $pro_id . ',' . $pro_ids;
            $product_id[] = array('pro_id' => $pro);
            $product_details[] = $this->api_home_section->product_detail($pro, $city_id);
            $product_details2[] = $this->api_home_section->product_features($pro);
            $product_details3[] = $this->api_home_section->car_specifiaction($pro, $type = 'color');
        }
        $pro_ids = substr($pro_ids, 0, -1);

        $data['final'] = array('pro_details' => $product_details, 'features' => $product_details2, 'color' => $product_details3);
        $data = $data['final'];
        $this->response($data);
    }

    #### Home page brand with product slider start####

    function home_hotdeal_product_get() {
        $brand_id = $this->get('brand_id');
        $city_id = $this->get('city_id');
        $data['data'] = $this->api_home_section->home_page_carousel($brand_id, $city_id);
        $data = $data['data'];
        $this->response($data);
    }

    #### Home page brand with product slider start####

    function home_car_comparision_get() {
        $featured_type = 'car_comparison';
        $res3 = $this->api_home_section->featured($featured_type);
        if (!empty($res3)) {
            $total = array();
            foreach ($res3 as $car_com_data)
                $data1 = $this->api_home_section->product_detail($car_com_data['prd_id_1'], '');
            if (!empty($data1)) {
                $exp_review_count1 = $this->api_home_section->expert_review_count($data1['0']['variant_id'], $id = '1');
                $user_review_count1 = $this->api_home_section->expert_review_count($data1['0']['variant_id'], $id = '2');
                $data['car_comp_1'] = array_merge($data1, $exp_review_count1, $user_review_count1);
            }

            $data2 = $this->api_home_section->product_detail($car_com_data['prd_id_2'], '');
            if (!empty($data2)) {
                $exp_review_count2 = $this->api_home_section->expert_review_count($data2['0']['variant_id'], $id = '1');
                $user_review_count2 = $this->api_home_section->expert_review_count($data2['0']['variant_id'], $id = '2');
                $data['car_comp_2'] = array_merge($data2, $exp_review_count2, $user_review_count2);
            }
        }
        $data['car_comp_3'] = array_merge($data['car_comp_1'], $data['car_comp_2']);
        $data = $data['car_comp_3'];
        $this->response($data);
    }

    function home_news_get() {
        $this->load->library('rss');
        $feed = $this->rss;
        $feed->set_feed_url('http://mynewcar.in/blog/feed');
        $success = $feed->init();
        $feed->handle_content_type();
        $all_feed = array();
        foreach ($feed->get_items(0, 2) as $item) {
            $res1 = array('link' => $item->get_permalink(), 'title' => $item->get_title(), 'desc' => $item->get_description());
            $all_feed[] = $res1;
        }
        $data['feed_res_data'] = $all_feed;

//        $data['data']=$this->api_home_section->home_page_carousel($brand_id,$city_id);;
        $data = $data['feed_res_data'];
        $this->response($data);
    }

    function car_comparision_get() {
        $featured_type = 'car_comparison';
        $res3 = $this->api_home_section->featured($featured_type);
        if (!empty($res3)) {
            $total = array();
            foreach ($res3 as $car_com_data)
                $data1 = $this->api_home_section->product_detail($car_com_data['prd_id_1'], '');
            if (!empty($data1)) {
                $exp_review_count1 = $this->api_home_section->expert_review_count($data1['0']['variant_id'], $id = '1');
                $user_review_count1 = $this->api_home_section->expert_review_count($data1['0']['variant_id'], $id = '2');
                $data['car_comp_1'] = array_merge($data1, $exp_review_count1, $user_review_count1);
            }

            $data2 = $this->api_home_section->product_detail($car_com_data['prd_id_2'], '');
            if (!empty($data2)) {
                $exp_review_count2 = $this->api_home_section->expert_review_count($data2['0']['variant_id'], $id = '1');
                $user_review_count2 = $this->api_home_section->expert_review_count($data2['0']['variant_id'], $id = '2');
                $data['car_comp_2'] = array_merge($data2, $exp_review_count2, $user_review_count2);
            }
        }
        $data['car_comp_3'] = array_merge($data['car_comp_1'], $data['car_comp_2']);
        $data = $data['car_comp_3'];
        $this->response($data);
    }

    function feedback_enquiry_get() {
        $full_name = $this->get('id');
        $data = $full_name;
        $this->response($data);
    }

    function fuel_get() {
        $fuel = $this->api_home_section->all_fuel();
        $data = $fuel;
        $this->response($data);
    }

    function comparsion_overview_get() {
        $product_id = $this->get('product_id');
        $city_id = $this->get('city');
        $exp_car = explode('_', $product_id);
        $product_id = array();
        $product_details1 = array();
        $pro_ids = '';
        for ($i = 0; $i < count($exp_car); $i++) {
            $exp_id = explode('-', $exp_car[$i]);
            $tot = count($exp_id);
            $pro = $exp_id[$tot - 1];
            $pro_id = "'" . $pro . "'";
            $pro_ids = $pro_id . ',' . $pro_ids;
            $product_id[] = array('pro_id' => $pro);
            $product_details[] = $this->api_home_section->product_detail($pro, $city_id);
        }
        $data = $product_details;
        $this->response($data);
    }

    function comparsion_feature_get() {
        $product_id = $this->get('product_id');
        $city_id = $this->get('city');
        $exp_car = explode('_', $product_id);
        $product_id = array();
        $product_details1 = array();
        $pro_ids = '';
        for ($i = 0; $i < count($exp_car); $i++) {
            $exp_id = explode('-', $exp_car[$i]);
            $tot = count($exp_id);
            $pro = $exp_id[$tot - 1];
            $pro_id = "'" . $pro . "'";
            $pro_ids = $pro_id . ',' . $pro_ids;
            $product_id[] = array('pro_id' => $pro);
            $product_details2[] = $this->api_home_section->product_features($pro);
        }
        $data = $product_details2;
        $this->response($data);
    }

    function comparsion_color_get() {
        $product_id = $this->get('product_id');
        $city_id = $this->get('city');
        $exp_car = explode('_', $product_id);
        $product_id = array();
        $product_color = array();
        $pro_ids = '';
        for ($i = 0; $i < count($exp_car); $i++) {
            $exp_id = explode('-', $exp_car[$i]);
            $tot = count($exp_id);
            $pro = $exp_id[$tot - 1];
            $pro_id = "'" . $pro . "'";
            $pro_ids = $pro_id . ',' . $pro_ids;
            $product_id[] = array('pro_id' => $pro);

            $product_color[] = $this->api_home_section->product_comp_color($pro);
        }
        $data = $product_color;
        $this->response($data);
    }

    function comparsion_specification_get() {
        $product_id = $this->get('product_id');
        $city_id = $this->get('city');
        $exp_car = explode('_', $product_id);
        $product_id = array();
        $product_details1 = array();
        $pro_ids = '';
        for ($i = 0; $i < count($exp_car); $i++) {
            $exp_id = explode('-', $exp_car[$i]);
            $tot = count($exp_id);
            $pro = $exp_id[$tot - 1];
            $pro_id = "'" . $pro . "'";
            $pro_ids = $pro_id . ',' . $pro_ids;
            $product_id[] = array('pro_id' => $pro);
            $product_details[] = $this->api_home_section->product_detail($pro, $city_id);
            $product_details2[] = $this->api_home_section->product_features($pro);
        }
        $pro_ids;
        $pro_ids = substr($pro_ids, 0, -1);
        $data['product_details2'] = $product_details2;
        $product_details2 = array();
        foreach ($product_details as $product_details_data) {
            $data['variant_details'] = $this->api_home_section->product_variant_detail($product_details_data[0]['variant_id']);
            $product_details2[] = array_merge($product_details_data, $data['variant_details']);
        }
        for ($j = 1; $j <= 6; $j++) {
            $dist_features_names[] = $this->api_home_section->get_features_name($pro_ids, $j);
        }
        $data['product_details'] = $product_details2;
        $data = $data['product_details'];
        $this->response($data);
    }

    function car_search_get() {
        $per_page = 10;
        $offset = $this->get('offset');
        $city = $this->get('city');
        $brand = $this->get('brand');
        $model_id = $this->get('model');
        $fuel = $this->get('fuel');
        $type = $this->get('type');
        $min = $this->get('seat_min');
        $min2 = $this->get('seat_max');
        $pric_min = $this->get('min_price');
        $pric_max = $this->get('max_price');
        $trans = $this->get('transmission');
        $data['total_rows'] = count($this->api_home_section->getPaginatedCars($city, $type, $trans, $pric_min, $pric_max, $brand, $model_id, $fuel, $min, $min2, 0, 0, $per_page)); // 0 - Get Count
        $data['pro_res'] = $this->api_home_section->getPaginatedCars($city, $type, $trans, $pric_min, $pric_max, $brand, $model_id, $fuel, $min, $min2, 1, $offset, $per_page);
        $total_result = array($data['total_rows'], $data['pro_res']);
        $data = $total_result;
        $this->response($data);
    }

    function search_car_get() {
        $q = $this->get('search');
        $res = $this->api_home_section->search_car_res($q);
        $data = $res;
        $this->response($data);
    }

    function product_expert_review_get() {
        $product_id = $this->get('product_id');
        $res = $this->api_home_section->get_exp_review($product_id);
        $data = $res;
        $this->response($data);
    }

    function product_user_review_get() {
        $product_id = $this->get('product_id');
        $res = $this->api_home_section->get_review($product_id);
        $data = $res;
        $this->response($data);
    }

    function product_extra_product_get() {
        $product_id = $this->get('product_id');
        $data['pro_ext'] = $this->api_home_section->rating_details($product_id);
        $data = $data['pro_ext'];
        $this->response($data);
    }

    function product_color_get() {
        $product_id = $this->get('product_id');

        $data['car_color'] = $this->api_home_section->car_specifiaction($product_id, $type = 'color');
        $data = $data['car_color'];
        $this->response($data);
    }

    function request_a_callback_post() {
        $test_full_name = $this->post('enq_full_name');
        $test_email = $this->post('enq_email');
        $test_phone = $this->post('enq_phone');
        $test_enq_interested_car = $this->post('enq_interested_car');
        $mandrill_api_key = Mandrill_key;
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        $html_data = <<<END
            Following Feedback/Callback Enquiry Filled By User:-<br> 
            Full Name: $test_full_name<br> 
            Email: $test_email<br> 
            Phone: $test_phone<br> 
            Interested Car: $test_enq_interested_car<br> 
END;
        if ($mandrill_ready) {
            $email = array(
                'html' => $html_data, //Consider using a view file
                'text' => 'Following Feedback/Callback Enquiry Filled By User',
                'subject' => 'Feedback/Callback Enquiry Details',
                'from_email' => 'account@mnc.com',
                'from_name' => 'MNC Feedback/Callback Enquiry Detail',
                'to' => array(
//                    array('email' => 'contact@mynewcar.in'),
                    array('email' => 'dinesh@projectheena.com'),
                    array('email' => 'webdeveloperdinesh@gmail.com')
//                    array('email' => 'sbhuia@mynewcar.in'),
//                    array('email' => 'contact.mynewcar@gmail.com'),
//                    array('email' => 'mvasudev@mynewcar.in')
                )
            );
            $result = $this->mandrill->messages_send($email);
        }
        $this->response($result, 200);
    }

    function conatct_upload_post() {
        $data = $this->post();
        $mandrill_api_key = Mandrill_key;
        $this->load->library('Mandrill');
//            echo MNC_SUPPORT_EMAIL;
//            exit;
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        if ($mandrill_ready) {
            $email = array(
                'html' => $this->load->view('emailers/contact_email', $data, TRUE), //Consider using a view file
                'text' => 'You have a contact Email',
                'subject' => 'Email Via MNC Contact Us Form',
                'from_email' => $data["email"],
                'from_name' => $data["first_name"] . ' ' . $data["last_name"],
                'to' => array(
//                    array('email' => 'contact@mynewcar.in'),
                    array('email' => 'dinesh@projectheena.com'),
                    array('email' => 'webdeveloperdinesh@gmail.com')
//                    array('email' => 'sbhuia@mynewcar.in'),
//                    array('email' => 'contact.mynewcar@gmail.com'),
//                    array('email' => 'mvasudev@mynewcar.in')
                )
            );
            $result = $this->mandrill->messages_send($email);
        }
        $this->response($result, 200);
    }

    function login_post() {
        $signin_email = $this->post('signin_email');
        $url = $this->post('url');
        $signin_password = $this->post('signin_password');
        $result = $this->api_home_section->custom_login($signin_email, $signin_password);
        $this->response($result, 200);
    }

    function forget_post() {
        $forget_email = $this->post('forget_email');
        $res = $this->api_home_section->check_email($forget_email);
        $user_id = $res[0]['user_id'];
        $uniue_id = md5(uniqid($forget_email, true));
        $res = $this->api_home_section->update_unq_forget($forget_email, $uniue_id, $user_id);
        $mandrill_api_key = 'S8PpS9ZmNMup1ZYXTEvm5w';
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        $html_data = "";
        $html_data.="Hello,<br><b>Please click the below given link to change the account</b><br>";
        $html_data.=base_url() . "stitcher/change_password/" . $uniue_id;
        $html_data.="<br>";
        $html_data.="Thanks & Best Regards,<br>";
        $html_data.="MNC Team";
        $html_data;
        if ($mandrill_ready) {
            $email = array(
//            'html' => $this->load->view('emailers/download_config', TRUE), //Consider using a view file
                'html' => $html_data, //Consider using a view file
                'text' => 'You have a change password link',
                'subject' => 'please change your mnc account',
                'from_email' => 'account@mnc.com',
                'from_name' => 'MNC Account',
                'to' => array(array('email' => $forget_email))
            );
            $result = $this->mandrill->messages_send($email);
        }
        $this->response($result, 200);
    }

    function book_your_car_post() {
        $pro_id = $this->post('pro_id');
        $rating_variant = $this->post('rating_variant');
        $final_total_saving_price = $string = preg_replace('/[^A-Za-z0-9\-]/', '', ($this->post('final_total_saving_price')));
        $final_mnc_on_road_price = $string = preg_replace('/[^A-Za-z0-9\-]/', '', ($this->post('final_mnc_on_road_price')));
        $color = $this->post('color');

        if (isset($_POST['car_insurance'])) {
            $car_insurance = '1';
        } else {
            $car_insurance = '';
        }
        $car_insurance;

        if (isset($_POST['car_exchange_data'])) {
            $car_exchange = '1';
        } else {
            $car_exchange = '';
        }
        $car_exchange;

        if (isset($_POST['car_finance_data'])) {
            $car_finance = '1';
        } else {
            $car_finance = '';
        }
        $car_finance;


        if (isset($_POST['home_delivery'])) {
            $home_delivery = '1';
        } else {
            $home_delivery = '';
        }
        $home_delivery;



        if (isset($_POST['corporate_discount'])) {
            $corporate_dis = '1';
        } else {
            $corporate_dis = '';
        }
        $corporate_dis;

        if (isset($_POST['loyality_bonus'])) {
            $loyality_bonus = '1';
        } else {
            $loyality_bonus = '';
        }
        $loyality_bonus;

        if (isset($_POST['extended_waranty'])) {
            $extended_waranty = '1';
        } else {
            $extended_waranty = '';
        }
        $extended_waranty;



        $emi_id = $this->post('emi_id');
        $emi_amount = $this->post('car_finance_price');

        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        if ($user_id == '') {
            $user_id = '0';
        }
        $check = $this->db->query("SELECT * FROM `car_configuration` where product_id='$pro_id' and status!='2' and user_id='$user_id'")->result_array();
        $count = count($check);
        if ($count == 0) {
            $res = $this->db->query("INSERT INTO `car_configuration`(`color`,`variant_type`,`product_id`,`user_id`,`introduction`,"
                    . "`emi_amount`,`finance_id`,`amc_id`,`insurance_id`,`extended_id`,`used_id`,`home_delivery`,`corporate_discount`,`loyality_bonus`,`emi_id`,`total_saving`,`on_road_price`)"
                    . " VALUES ('$color','$pro_id','$pro_id','$user_id','1',"
                    . "'$emi_amount','$car_finance','','$car_insurance','$extended_waranty','$car_exchange','$home_delivery','$corporate_dis','$loyality_bonus','$emi_id','$final_total_saving_price','$final_mnc_on_road_price')");
            $result = array('configuration_id' => mysql_insert_id());
        } else {
            $res = $this->db->query("UPDATE `car_configuration` SET `user_id`='$user_id',color='$color',"
                    . "emi_amount='$emi_amount',finance_id='$car_finance',insurance_id='$car_insurance',extended_id='$extended_waranty'"
                    . ",home_delivery='$home_delivery',corporate_discount='$corporate_dis',loyality_bonus='$loyality_bonus',emi_id='$emi_id',total_saving='$final_total_saving_price',on_road_price='$final_mnc_on_road_price' "
                    . " where `product_id`='$pro_id' and user_id='$user_id' and status!='2'");

            $result = $check[0]['car_configuration_id'];
            $result = array('configuration_id' => $result);
        }
        $this->response($result, 200);
    }

    function coming_enquiry_post() {
        $test_full_name = $this->post('enq_full_name');
        $test_email = $this->post('enq_email');
        $test_phone = $this->post('enq_phone');
        $enq_comment = $this->post('enq_comment');
        $pro_link = $this->post('pro_link');
        $product_name = $this->post('product_name');

        $mandrill_api_key = Mandrill_key;
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        $html_data = <<<END
            Following Feedback/Callback Enquiry Filled By User:-<br> 
            Full Name: $test_full_name<br> 
            Email: $test_email<br> 
            Phone: $test_phone<br> 
            Comment: $enq_comment<br> 
            Product Name: $product_name<br> 
            Product Link: $pro_link<br> 
END;
        if ($mandrill_ready) {
            $email = array(
                'html' => $html_data, //Consider using a view file
                'text' => 'Following Coming Soon Product Enquiry Filled By User',
                'subject' => 'Coming Soon Enquiry Details',
                'from_email' => 'account@mnc.com',
                'from_name' => 'MNC Coming Soon Enquiry Detail',
                'to' => array(
//                    array('email' => 'contact@mynewcar.in'),
                    array('email' => 'dinesh@projectheena.com')
//                        array('email' =>'webdeveloperdinesh@gmail.com')
//                    array('email' => 'sbhuia@mynewcar.in'),
//                    array('email' => 'contact.mynewcar@gmail.com'),
//                    array('email' => 'mvasudev@mynewcar.in')
                )
            );
            $result = $this->mandrill->messages_send($email);
        }
        $this->response($result, 200);
    }

    function register_user_post() {
        $regis_username = $this->post('regis_username');
        $regis_email = $this->post('regis_email');
        $regis_password = $this->post('regis_password');

        if ($this->input->get('req_uri')) {
            $request_uri = $this->get('req_uri');
            $this->session->set_flashdata('req_uri', $request_uri);
        }

        $insert_user = $this->api_home_section->custome_user($regis_username, $regis_email, $regis_password, $id = '1');
//        print_r($insert_user);
        $email_to = $insert_user[0]['email_id'];
        if (!empty($insert_user)) {
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            $linksdata = base_url() . "stitcher/activate_link/" . $insert_user[0]['random_key'];
            $logo = base_url() . "uploads/logo.png";
            $html_data = <<<END
            Dear Mr/Ms. $regis_username,
            <br><br>        
            Welcome to MYNEWCAR.in.<br> 
            Please confirm your email and become a member of India’s only one stop ONLINE solution for Car Buyers and Car owners
            <a href="$linksdata">Click here</a>.                                   
            <br><br>         
            Link Not working!? Please paste the URL below into your browser
            <br>            
            $linksdata            
            <br><br>                 
            <div>Regards<br>         
            Team MYNEWCAR.in<br>         
            <img src="$logo" style='width:20%'/><br>
            <br>                 
            Phone: 022 45 02 03 04 | Email: contact@mynewcar.in<br>         
            www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br>         
            ---------------------------------------------------------------------------------------------------------------------------------<br>         
            DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>         
            Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>         
            Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>         
END;
            if ($mandrill_ready) {
                $email = array(
                    'html' => $html_data, //Consider using a view file
                    'text' => 'You have a activation link',
                    'subject' => 'please activate your mnc account',
                    'from_email' => 'account@mnc.com',
                    'from_name' => 'MNC Account',
                    'to' => array(array('email' => $email_to))
                );
                $result = $this->mandrill->messages_send($email);
            }
            $this->response($result, 200);
        }
    }

}

?>