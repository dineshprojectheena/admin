<?php

class Common extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
//            exit;
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

//
    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
//        $this->load->model('forum_section');
        $this->load->model('login_authorization');
//        $this->load->model('profile');
//        $this->load->model('faq');
        $this->output->nocache();
    }

    function index() {

        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();





        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }


        #### News Section here ####
        $this->load->library('rss');
        $feed = $this->rss;
        $feed->set_feed_url('http://blog.mynewcar.in/feed/');
        $success = $feed->init();
        $feed->handle_content_type();
        $data['feed_res_data'] = array();
        foreach ($feed->get_items(0, 2) as $item) {
            $res1 = array('link' => $item->get_permalink(), 'title' => $item->get_title(), 'desc' => $item->get_description());
            $data['feed_res_data'][] = $res1;
        }


        ####Get Max & Min Price data ####

        $data['max_price_res'] = $this->home_feature->max_price($date['city']);
        $date['max_price'] = $data['max_price_res']['max_price'];
        $date['min_price'] = $data['max_price_res']['min_price'];
        $date['main_price'] = $date['min_price'] . '-' . $date['max_price'];




        ####Get Slider data ####

        $data['main_slider'] = $this->home_feature->slider();
//        print_r($data['main_slider']);
        #### Home page Car comparison shows here ####
        $featured_type = 'car_comparison';
        $res3 = $this->home_feature->featured($featured_type);
        if (!empty($res3)) {
            $total = array();
            foreach ($res3 as $car_com_data)
                $data1 = $this->home_feature->product_detail($car_com_data['prd_id_1'], '');
            if (!empty($data1)) {
                $exp_review_count1 = $this->home_feature->expert_review_count($data1['0']['variant_id'], $id = '1');
                $user_review_count1 = $this->home_feature->expert_review_count($data1['0']['variant_id'], $id = '2');
                $data['car_comp_1'] = array_merge($data1, $exp_review_count1, $user_review_count1);
            }

            $data2 = $this->home_feature->product_detail($car_com_data['prd_id_2'], '');
            if (!empty($data2)) {
                $exp_review_count2 = $this->home_feature->expert_review_count($data2['0']['variant_id'], $id = '1');
                $user_review_count2 = $this->home_feature->expert_review_count($data2['0']['variant_id'], $id = '2');
                $data['car_comp_2'] = array_merge($data2, $exp_review_count2, $user_review_count2);
            }
        }

        $data['content'] = 'home';


        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');

        $data['userdata'] = $this->session->userdata('logged_in_user');


        $this->load->view('index', $data);
    }

    #### Home page hot deal section start here

    function home_brand_product() {
        $brand_id = $this->input->post('brand_data');

        $data['data'] = $this->home_feature->home_page_carousel($brand_id, $id = 2);
//        print_r($data['data']);
//        exit;
        $this->load->view('ajaxPages/homeCarousel', $data);
    }

    #### Brand Wise Model

    function comparison_model_data() {
        $brand_data = $this->input->post('brand_data');
        if($brand_data!=''){
        $car_model = $this->common_section->comparison_model_data($brand_data);
        ?>
        <option value="">Select Model</option>                    
        <?php
        foreach($car_model as $car_model_data) {
            if($car_model_data['model_name']!=''){
            ?>
            <option value="<?php echo $car_model_data['model_id']; ?>"><?php echo $car_model_data['model_name']; ?></option>
            <?php
            }
        }        
        }
        else {
        ?>
        <option value="">Select Model</option>                    
        <?php    
        }
    }

    function comparison_lease_model_data() {
        $brand_data = $this->input->post('brand_data');
        if($brand_data!='')
        {    
        $car_model = $this->common_section->comparison_lease_model_data($brand_data);
        ?>
        <option value="">Select Model</option>                    
        <?php        
        if(!empty($car_model))
        {    
        foreach($car_model as $car_model_data) {
            ?>
            <option value="<?php echo $car_model_data['model_id']; ?>"><?php echo $car_model_data['model_name']; ?></option>
            <?php
        }        
        }        
        }        
    }

    

    function fuel_type() {
        $fuel_data = $this->common_section->all_fuel();
//    print_r($fuel_data);
//    exit;
        ?>
        <option value="">Select Fuel Type</option>                    
        <?php
        foreach ($fuel_data as $fuel_data_data) {
            ?>
            <option value="<?php echo $fuel_data_data['fuel_type_id']; ?>"><?php echo $fuel_data_data['fuel_type']; ?></option>
            <?php
        }
        ?>
        <?php
    }

    #### Change Password over here

    function change_password($id) {
        $get_user = $this->login_authorization->get_change_user($id);
        $data['user_id'] = $get_user[0]['user_id'];
        $data['content'] = 'change_password';
        $this->load->view('index', $data);
    }

    function comparison_variant_data() {
        $brand_data = $this->input->post('brand_data');
        $brand_model = $this->input->post('brand_model');
        $variant_data = $this->common_section->comparison_variant_data($brand_data, $brand_model);
        $fuel = $this->common_section->all_fuel();
        ?>
        <option value="">Select Variant</option>
        <?php
        foreach ($variant_data as $variant_data_res) {
            foreach ($fuel as $fuel_data) {
                if ($variant_data_res['fuel_type'] == $fuel_data['fuel_type_id']) {
                    $fuel_name = $fuel_data['fuel_type'];
                }
            }
            ?>
            <option value="<?php echo $variant_data_res['variant_id']; ?>"><?php echo $variant_data_res['pro_name_comp']; ?></option>
            <?php
        }
        ?>
        <?php
    }

    function change_car_res($a) {
        $exp = explode('-dineshk-', $a);
        $url = $exp[0];
        $new = $exp[1];
        $position = $exp[2];
        $url = $exp[0];
        $new_data = array($position => $new);
        $exp2 = explode('-VS-', $url);
        $count = count($exp2);
        if ($position == 3 && $count != 3) {
            $final = array_merge($exp2, $new_data);
            $final = array_unique($final);
            $finals = implode("-VS-", $final);
        } else {
            $i = 1;
            $final = array();
            foreach ($exp2 as $exp2_data) {
                if ($position == $i) {
                    $exp2_data = $new;
                } else {
                    $exp2_data;
                }
                $final[] = $exp2_data;
                $finals = implode("-VS-", $final);
                echo '<br>';
                $i++;
            }
        }

        redirect(base_url() . 'compare-new-cars-online/' . urlencode($finals));
        exit;



        $res = '';
        $i = 1;
        foreach ($exp2 as $exp2_res) {
            $exp2_res;
            if ($i == $position) {
                $res = $new . '_' . $res;
            } else {
                $res = $exp2_res . '_' . $res;
            }
            $i++;
//        echo '<br>';
        }
        if ($position == 3) {
            $res = $res . '' . $new;
        }
        $exp_data = explode('_', $res);
        $exp_data = array_filter($exp_data);
        $result = array_unique($exp_data);
        $final = implode("_", $result);
//        exit;
        redirect(base_url() . 'comparision/' . $final);
    }

    #### Newsletter Subscribe

    function subscribe() {
        $email = $this->input->post('email');
        $this->load->library('mailchimp_library');
        $result = $this->mailchimp_library->call('lists/subscribe', array(
            'id' => MAILCHIMP_LIST_ID,
            'email' => array('email' => $email),
            'double_optin' => false,
            'update_existing' => true,
            'replace_interests' => false,
            'send_welcome' => false,
        ));
        $msg = 'Oops!!! There was some issue. Please try again later.';
        if ($result) {
            $msg = 'Congratulations! Now the latest deals, car buying tips and auto news will be sent straight to your inbox!';
        }
        $this->session->set_flashdata('newsletter_msg', $msg);
        redirect(site_url());
    }

    #### Request a call back 

    function feedback_enquiry() {
        $test_full_name = $this->input->post('enq_full_name');
        $test_email = $this->input->post('enq_email');
        $test_phone = $this->input->post('enq_phone');
        $test_enq_interested_car = $this->input->post('enq_interested_car');
        if($this->input->post('request_main_city')=='Others')
        {
        $request_main_city = $this->input->post('request_main_city2');    
        }
        else
        {    
        $request_main_city = $this->input->post('request_main_city');
        }
        $mandrill_api_key = Mandrill_key;
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        $html_data = <<<END
            Following Feedback/Callback Enquiry Filled By User:-<br> 
            Full Name: $test_full_name<br> 
            Email: $test_email<br> 
            Phone: $test_phone<br> 
            City: $request_main_city<br> 
            Interested Car: $test_enq_interested_car<br> 
END;
        
        $message='Thank you for your request. We will call you back in the next 48 hours';
        $this->sms_send->sms_pass($test_phone,$message);
        
        if ($mandrill_ready) {
            $email = array(
                'html' => $html_data, //Consider using a view file
                'text' => 'Following Feedback/Callback Enquiry Filled By User',
                'subject' => 'Feedback/Callback Enquiry Details',
                'from_email' => 'account@mnc.com',
                'from_name' => 'Call Back Enquiry',
                'to' => array(
                    array('email' => 'contact@mynewcar.in'),
                    array('email' => 'sbhuia@mynewcar.in'),
                    array('email' => 'contact.mynewcar@gmail.com'),
                    array('email' => 'dinesh@projectheena.com'),
                    array('email' => 'mvasudev@mynewcar.in')
//                      array('email' => 'webdeveloperdinesh@gmail.com')
                )
            );
            $result = $this->mandrill->messages_send($email);
        }
        $this->session->set_flashdata('feedback_success', 'feedback_success');
        redirect('');
    }

    #### Book a test drive

    function test_drive_enquiry() {
        $test_brand_data = $this->input->post('test_brand_data');
        $test_brand_model = $this->input->post('test_brand_model');
        $test_car_variant = $this->input->post('test_car_variant');
        $test_full_name = $this->input->post('test_full_name');
        $test_fuel_type = $this->input->post('test_fuel_type');
        $test_date = $this->input->post('test_date');
        $test_email = $this->input->post('test_email');
        $test_phone = $this->input->post('test_phone');
        $test_location = $this->input->post('test_location');
        $test_drive = $this->common_section->get_test_drive_data($test_brand_data, $test_brand_model, $test_car_variant);
        $pro_name_comp = $test_drive['pro_name_comp'];
        $brand_name = $test_drive['brand_name'];
        $model_name = $test_drive['model_name'];
        $upload_test_drive = $this->common_section->test_drive_data_upload($brand_name, $model_name, $test_date, $test_fuel_type, $test_full_name, $test_email, $test_location, $test_phone);
        
        
        $message='We have received your request. We will check the exact availability and confirm with you within the next 48 hours.';
        $this->sms_send->sms_pass($test_phone,$message);
        
        $mandrill_api_key = Mandrill_key;
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        $html_data = <<<END
            Following Test Drive Enquiry Filled By User:-<br> 
            <!--Product Name: $pro_name_comp<br> -->
            Test Drive Unique Id: $upload_test_drive<br> 
            Brand Name: $brand_name<br> 
            Model Name: $model_name<br> 
            Date: $test_date<br> 
            Fuel: $test_fuel_type<br> 
            Full Name: $test_full_name<br> 
            Email: $test_email<br> 
            Location: $test_location<br> 
            Phone: $test_phone<br> 
END;


        if ($mandrill_ready) {
            $email = array(
                'html' => $html_data, //Consider using a view file
                'text' => 'Following Test Drive Enquiry',
                'subject' => 'New Test Drive Enquiry',
                'from_email' => 'account@mnc.com',
                'from_name' => 'Test Drive Enquiry',
                'to' => array(
                    array('email' => 'contact@mynewcar.in'),
                    array('email' => 'sbhuia@mynewcar.in'),
                    array('email' => 'contact.mynewcar@gmail.com'),
//                    array('email' => 'dinesh@projectheena.com'),
//                    array('email' => 'webdeveloperdineshv@gmail.com')
                    array('email' => 'mvasudev@mynewcar.in')
                )
            );
            $result = $this->mandrill->messages_send($email);
        }

        $this->session->set_flashdata('test_enquiry', 'test_enquiry');
        redirect('');
    }

    #### Contact Us page  & statics pages here

    function contact_us() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();

        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();

        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }

        if ($this->session->flashdata('alert_msg')) {
            $data['alert_msg'] = $this->session->flashdata('alert_msg');
        }
        $data['content'] = 'contactUs';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');
        $data['city_res'] = $this->common_section->get_city();
        $data['state_res'] = $this->common_section->get_state();
        $this->load->view('index', $data);
    }

    function conatct_upload(){
        if (filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL) !== FALSE) {
            $data = $this->input->post();
            $number= $this->input->post('number');
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            if ($mandrill_ready) {
                $email = array(
                    'html' => $this->load->view('emailers/contact_email', $data, TRUE), //Consider using a view file
                    'text' => 'You have a contact Email',
                    'subject' => 'Email Via MNC Contact Us Form',
                    'from_email' => $data["email"],
                    'from_name' => 'Contact Us Enquiry',
                    'to' => array(
                        array('email' => 'contact@mynewcar.in'),
                        array('email' => 'sbhuia@mynewcar.in'),
                        array('email' => 'contact.mynewcar@gmail.com'),
//                    array('email' => 'dinesh@projectheena.com'),
                        array('email' => 'mvasudev@mynewcar.in')
//                      array('email' => 'webdeveloperdinesh@gmail.com')
                    )
//                        'to'=>array(array('email' => 'madhukarmanpuria@gmail.com')),
//                        'bcc'=>array(array('email' => 'webdeveloperdinesh@gmail.com'))
                );
                $result = $this->mandrill->messages_send($email);
            }
            $msg = 'Email sent successfully. You will hear from us soon';
            $this->session->set_flashdata('alert_msg', $msg);
            $message='Thank you for your request. We will call you back in the next 48 hours.';
            $this->sms_send->sms_pass($number,$message);
//            exit;
            redirect('car-enquiry');
        } else {
            $this->session->set_flashdata('alert_msg', TRANSACTION_ERROR_MSG);
            redirect('car-enquiry');
        }
    }

    function affiliate_id() {
        $peramter= $_POST['peramter'];
        $data['affiliate_id'] = $this->common_section->affiliate_data($peramter);
        print_r($data['affiliate_id']['affiliate_id']);
        
    }

    function about_us() {
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();        
        
        $data['content'] = 'aboutus';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');
        $this->load->view('index', $data);
    }

    function faq() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }
        $data['content'] = 'faq';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');
        $this->load->view('index', $data);
    }

    function privacy() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }

        $data['content'] = 'privacy';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');
        echo $this->load->view('index', $data);
    }

    function terms() {

        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }

        $data['content'] = 'terms';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');
        echo $this->load->view('index', $data);
    }

    function user_terms() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }


        $data['content'] = 'user_terms';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');
        echo $this->load->view('index', $data);
    }

    #### Common Search section of MNC

    function search_car() {
        if ($_POST) {
            $q = $_POST['inputSearch'];
            $res = $this->common_section->search_car_res($q);
            if (count($res) >= 0) {
                foreach ($res as $res_data) {
                    $brand_name = $res_data['brand_name'];
                    $model_name = $res_data['model_name'];
                    $pro_name = $res_data['pro_name'];
                    $pro_image = $res_data['pro_image'];
                    $pro_name_comp = $res_data['pro_name_comp'];
                    ?>
                    <a style="text-decoration:none;" href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_name_comp) . '-' . $res_data['variant_id']) ?>" >
                        <div class="display_box" align="left">                    
                            <img src="<?php echo img_url('uploads/product/'. $pro_image); ?>" style="width:50px;float:left; margin-right:6px;" />
                            <span class="name"><?php echo $pro_name_comp; ?></span>&nbsp;<br/>

                        </div>
                    </a>    
                    <?php
                }
            } else {
                ?>
                <a style="text-decoration:none;" href="<?php echo site_url('car/' . preg_replace('/\s+/', '-', $res_data['pro_name']) . '-' . $res_data['variant_id']) ?>" >
                    <div class="display_box" align="center">                    
                        No Result Found
                    </div>
                </a>    
                <?php
            }
        }
    }

    function search() {
        $date['city'] = $this->common_city();
        $data['name'] = $this->input->get('name');
        $data['search_data']= $this->common_section->search_car_data($data['name'],$date['city']);
//        echo '<pre>';
//        print_r($data['search_data']);
//        echo '</pre>';
//        exit;
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }


        $data['userdata'] = $this->session->userdata('logged_in_user');

        $data['content'] = 'search';
        echo $this->load->view('index', $data);
    }

    function emi_pro() {
        $emi = $this->input->post('emi');
        $pro_id = $this->input->post('pro_id');
        $down_payment = $this->input->post('down_payment');
        $loan1 = $this->input->post('loan1');
        $bank = $this->input->post('bank');
        $rate1 = $this->input->post('rate1');
        $months1 = $this->input->post('months1');
        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        if ($user_id == '') {
            $user_id = '0';
        }

        $get_user = $this->common_section->add_emi($emi, $pro_id, $user_id, $down_payment, $loan1, $bank, $rate1, $months1);
        setcookie('emi_id', $get_user, time() + (86400 * 30), "/");
    }

    #### Common 404 page

    function my404() {
        $data['content'] = '404';
        $this->load->view('index', $data);
    }

}
