<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Authorization extends CI_Controller {
    
    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    
    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('login_authorization');
        $this->load->model('profile');
        parse_str($_SERVER['QUERY_STRING'], $_REQUEST);
        $this->load->library('image_uploads');
        $this->output->nocache();
    }
    
    

    function googleAuth() {
        require_once APPPATH . 'config/google.php';
        require_once APPPATH . 'third_party/google/google_auth.php';
        $client = new Google_Client();
        $client->setApplicationName("Google UserInfo PHP Starter Application");
        $oauth2 = new Google_Oauth2Service($client);
        
         if ($this->input->get('req_uri')) {
            $request_uri = $this->input->get('req_uri');
            $this->session->set_flashdata('req_uri', $request_uri);
        }
 
        $authUrl = $client->createAuthUrl();
//        }
        $this->session->set_flashdata('user_login_sucess', "login_sucess");
        //exit($authUrl);
        redirect($authUrl);
    }

    function google_login() {
        
        require_once APPPATH . 'config/google.php';
        require_once APPPATH.'third_party/google/google_auth.php';
        $client = new Google_Client();
        $oauth2 = new Google_Oauth2Service($client);
        if(isset($_GET['code'])){ // IF GOOGLE
//            echo '1';
            $client->authenticate($_GET['code']);
//            echo '2';
            $_SESSION['token'] = $client->getAccessToken();
//            echo '3';
            $_SESSION['oauth_type'] = "google";
//            echo '4';
            $user_profile = $oauth2->userinfo->get();
            
            if(is_null($user_profile['given_name']) !== true)
            {
                $firstname=$user_profile['given_name'];
            }
            else {
                $firstname='';
            }
            
            if(is_null($user_profile['name']) !== true)
            {
                $username=$user_profile['name'];
            }
            else {
                $username='';
            }
            
            if(is_null($user_profile['gender']) !== true)
            {
                $gender=$user_profile['gender'];
            }
            else {
                $gender='';
            }
            
            if(is_null($user_profile['id']) !== true)
            {
                $id=$user_profile['id'];
            }
            else {
                $id='';
            }
            
            if(is_null($user_profile['email']) !== true)
            {
                $email=$user_profile['email'];
            }    
            else {
                $email='';
            }
                  
            if(is_null($user_profile['link']) !== true)
            {
                $link=$user_profile['link'];
            }    
            else {
                $link='';
            }            

            

            $res_data = array('firstname' => $firstname,
                'username' => $username . date("ymdHis"),
                'gender' => $gender,
                'gm_id' => $id,
                'gm_link' => $link,
                'email' => $email,
                'date_added' => date('Y-m-d h:i:s'),
                'last_logged_In' => date('Y-m-d h:i:s'));
            $unq_id = uniqid();
            $img_url = isset($user_profile['picture']) ? $user_profile['picture'] : NULL;
            $this->image_uploads->uploadProfileImage($unq_id, $img_url);
            $res = $this->login_authorization->gmail_social_auth($res_data,$unq_id);
            $cond = array('logged_in' => 'TRUE');
            $res_status = array_merge($res, $cond);
            $this->session->set_userdata("logged_in_user", $res_status);
            $session = $this->session->userdata("logged_in_user");
                 $this->session->set_flashdata('user_login_sucess', "login_sucess");
                if($this->session->flashdata('req_uri')) {
                    redirect($this->session->flashdata('req_uri'));
                } else {
                    redirect('');
                }
        } else {
            $this->session->set_flashdata('facebook_error', "Facebook");
            redirect($this->session->flashdata('req_uri'));
        }

    }

    function onlyFbLogin() {
//        echo $res==$data.'-|-'.$data2;
//        $session_id=$this->session->userdata($res);
        if ($this->input->get('req_uri')) {
            $request_uri = $this->input->get('req_uri');
            $this->session->set_flashdata('req_uri', $request_uri);
        }
        $this->load->library('facebook');
        /* Callback url of Facebook is set in constants.php */
        $data['fb_url'] = $this->facebook->getLoginUrl(array('scope' => 'email, user_birthday, user_work_history'));
        $this->session->set_flashdata('user_login_sucess', "login_sucess");
        redirect($data['fb_url']);
    }

    /* actions to do after user has logged in from fb */

    public function fblogin() {
        $this->load->library('facebook');
        $this->session->unset_userdata('oauth_request_token_secret');
        $this->session->unset_userdata('oauth_request_token');
        if ($this->input->get('error_reason') == 'error_reason' || $this->input->get('error') == 'access_denied') {
            $this->session->set_flashdata('login_error', FB_LOGIN_ERROR_MSG);
            redirect(base_url());
        }
        $userId = $this->facebook->getUser();

        if ($userId != 0) {
            $user = $this->facebook->api('/me');

            $img_url = 'https://graph.facebook.com/' . $user['id'] . '/picture?width=180&height=180';
            $dob = date("Y-m-d", strtotime($user['birthday']));
            if (!isset($user['email'])) {
                $this->session->set_flashdata('facebook_error', "Facebook");
                ?>
                <script>
                    window.location.href = "<?php echo $this->session->flashdata('req_uri'); ?>";
                </script>
                <?php
            } else {
                if (is_array($user['location'])) {
                    $address = $user['location']['name'];
                } else {
                    $address = '';
                }

                if (!isset($user['location'])) {
                    $user['location'] = '-';
                }
                $res_data = array(
                    'dob' => $dob,
                    'firstname' => $user['first_name'],
                    'username' => $user['first_name'] . '' . $user['last_name'] . '' . date("ymdHis"),
                    'gender' => $user['gender'],
                    'fb_id' => $user['id'],
                    'fb_link' => $user['link'],
                    'email' => $user['email'],
                    'date_added' => date('Y-m-d h:i:s'),
                    'last_logged_In' => date('Y-m-d h:i:s'),
                    'address' => $address
                );
                $this->image_uploads->uploadProfileImage($user['id'], $img_url);
                $res = $this->login_authorization->facebook_social_auth($res_data);
                // exit;
                $cond = array('logged_in' => 'TRUE');
                $res_status = array_merge($res, $cond);

                $this->session->set_userdata("logged_in_user", $res_status);
                $session = $this->session->userdata("logged_in_user");
                $this->session->set_flashdata('user_login_sucess', "login_sucess");
                if($this->session->flashdata('req_uri')) {
                    redirect($this->session->flashdata('req_uri'));
                } else {
                    redirect('dashboard');
                }
            }
        } else {
            $this->session->set_flashdata('facebook_error', "Facebook");
            ?>
            <script>
                window.location.href = "<?php echo $this->session->flashdata('req_uri'); ?>";
            </script>
            <?php
        }
    }

    function dashboard() {
        $data['content'] = 'dashboard';
        $this->load->view('index', $data);
    }

    function logout() {
        $this->session->set_userdata('logged_in', FALSE);
        $this->session->unset_userdata('logged_in_user');
        $this->session->sess_destroy();
        redirect(base_url());
    }

    function join_group() {
        $brand_id = $this->input->post('brand_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status');
        $status_return = $this->login_authorization->join_brand_group($brand_id, $user_id, $status);
        print_r($status_return);
    }
    
//    function linkdin_url() {
//        $this->config->load("linkedin", TRUE);
//            $config = $this->config->item('linkedin');
//            $this->load->library('linkedin', $config);
//            $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
//            
//            $token = $this->linkedin->retrieveTokenRequest();
//            $this->session->set_userdata('oauth_request_token_secret', $token['linkedin']['oauth_token_secret']);
//            $this->session->set_userdata('oauth_request_token', $token['linkedin']['oauth_token']);
//            $data['oauth_request_token_secret'] = $token['linkedin']['oauth_token_secret'];
//            $data['oauth_request_token'] = $token['linkedin']['oauth_token_secret'];
//            
//            echo '<pre>';
//            var_dump($data);
//            echo '</pre>';
//    }

    function linkdin_url(){
        if ($this->input->get('req_uri')) {
            $request_uri = $this->input->get('req_uri');
            $this->session->set_flashdata('req_uri', $request_uri);
        }
        
        $this->input->get('req_uri');
        $this->config->load("linkedin", TRUE);
        $config = $this->config->item('linkedin');
        $this->load->library('linkedin', $config);
        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        //$this->session->set_flashdata('user_login_sucess', "login_sucess");
        /* Callback url of Facebook is set in constants.php */
        $token = $this->linkedin->retrieveTokenRequest();
        $this->session->set_userdata('oauth_request_token_secret', $token['linkedin']['oauth_token_secret']);
        $this->session->set_userdata('oauth_request_token', $token['linkedin']['oauth_token']);
        $data = array(
            'oauth_request_token_secret' => $token['linkedin']['oauth_token_secret'],
            'oauth_request_token' => $token['linkedin']['oauth_token']
        );
        
        if ($this->session->flashdata('login_error')) {
            $data['login_error_msg'] = $this->session->flashdata('login_error');
        }
        $data['linkedin_url'] = "https://api.linkedin.com/uas/oauth/authorize?oauth_token=" . $token['linkedin']['oauth_token'];
        redirect($data['linkedin_url']);
    }

    function linkedLogin() {
        $this->config->load("linkedin", TRUE);
        $config = $this->config->item('linkedin');
        $this->load->library('linkedin', $config);
        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        $this->load->library('ph_utils');
        $oauth_token = $this->session->userdata('oauth_request_token');
        $oauth_token_secret = $this->session->userdata('oauth_request_token_secret');
        $oauth_verifier = $this->input->get('oauth_verifier');
        $response = $this->linkedin->retrieveTokenAccess($oauth_token, $oauth_token_secret, $oauth_verifier);
        $profile = $this->linkedin->profile('~:(id,site-standard-profile-request,first-name,last-name,picture-url,headline,location,date-of-birth,email-address,summary,primary-twitter-account)');
        $response = $this->linkedin->setTokenAccess($response['linkedin']);
        $this->session->set_flashdata('user_login_sucess', "login_sucess");
        $user = json_decode($profile['linkedin']);
        $lindin_id = $user->id;
        $firstName = $user->firstName;
        $lastName = $user->lastName;
        $emailAddress = $user->emailAddress;
        $lindin_link = $user->siteStandardProfileRequest->url;
        $img_url = isset($user->pictureUrl) ? $user->pictureUrl : NULL;
        $last_logged_In=date('Y-m-d h:i:s');
        $user_data=array(
            'linkdin' => $lindin_id,
            'firstname' => $firstName,
            'username' => $firstName . '' . $lastName . '' . date("ymdHis"),
            'lastname' => $lastName,
            'email' => $emailAddress,
            'last_logged_In' => date('Y-m-d h:i:s'),
            'date_added' => date('Y-m-d h:i:s')            
        );        
        $unq_id = uniqid();
        $img_url = isset($user->pictureUrl) ? $user->pictureUrl : NULL;
        $this->image_uploads->uploadProfileImage($unq_id, $img_url);
        if (!empty($emailAddress)) {
            $res = $this->login_authorization->linkdin_social_auth($user_data, $unq_id);

            $cond = array('logged_in' => 'TRUE');
            $res_status = array_merge($res, $cond);
            $this->session->set_userdata("logged_in_user", $res_status);
            $session = $this->session->userdata("logged_in_user");
            if ($this->session->flashdata('req_uri')) {
                redirect($this->session->flashdata('req_uri'));
            } else {
                redirect('');
            }
        } else {
            ?>
            <script>
                alert('Sorry Some issue occured with linkdin login,please try again with some other social..');
//                window.location.href = "";
            </script>
            <?php
        redirect(base_url());    
        }
    }

    public function linkedinLoginCancelled() {
        $this->session->set_flashdata('login_error', LINKEDIN_LOGIN_ERROR_MSG);
        $this->session->unset_userdata('oauth_request_token_secret');
        $this->session->unset_userdata('oauth_request_token');
        redirect('login');
    }

    function login() {
        $signin_email = $this->input->post('signin_email');
        $url = $this->input->post('url');
        $signin_password = $this->input->post('signin_password');
        $res = $this->login_authorization->custom_login($signin_email, $signin_password);
        
        if (count($res) > 0) {
            $cond = array('logged_in' => 'TRUE');
            $res_status = array_merge($res, $cond);
            $this->session->set_userdata("logged_in_user", $res_status);
            $session = $this->session->userdata("logged_in_user");
            $this->session->set_flashdata('user_login_sucess', "login_sucess");
            redirect($url);
        } else {
            $this->session->set_flashdata('login_failed', "login_failed");
            redirect($url);
        }
    }

    function forget() {
        $forget_email = $this->input->post('forget_email');
        $res = $this->login_authorization->check_email($forget_email);
        
        $user_id = $res[0]['user_id'];
        if (empty($res)) {
        }else{
            $uniue_id = md5(uniqid($forget_email, true));
            $res = $this->login_authorization->update_unq_forget($forget_email, $uniue_id, $user_id);
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            $html_data = "";
            $html_data.="Hello,<br><b>Please click the below given link to change the account</b><br>";
            $html_data.=base_url() . "common/change_password/" . $uniue_id;
            $html_data.="<br>";
            $html_data.="Thanks & Best Regards,<br>";
            $html_data.="MNC Team";
            $html_data;
            if ($mandrill_ready) {
                $email = array(
//            'html' => $this->load->view('emailers/download_config', TRUE), //Consider using a view file
                    'html' => $html_data, //Consider using a view file
                    'text' => 'You have a change password link',
                    'subject' => 'please change your mnc account',
                    'from_email' => 'account@mnc.com',
                    'from_name' => 'MNC Account',
                    'to' => array(array('email' => $forget_email))
                );
                $result = $this->mandrill->messages_send($email);
            }
                        
            $this->session->set_flashdata('account_registered', "password_reset_successfully");
            if ($this->session->flashdata('req_uri')) {
                redirect($this->session->flashdata('req_uri'));
            } else {
                redirect('');
            }
            
        }
    }
    
    
    # Change Password
    function update_password(){
        $user_id = $this->input->post('user_id');
        $new_password = $this->input->post('new_password');
        $re_new_password = $this->input->post('re_new_password');
        $res = $this->login_authorization->update_password($user_id, $new_password, $re_new_password);
        $this->session->set_flashdata('account_registered', "password_changed_successfully");
        if ($this->session->flashdata('req_uri')) {
            redirect($this->session->flashdata('req_uri'));
        } else {
            redirect('');
        }
    }
    
    #check username
    function checkuser() {
        $regis_username = $this->input->post('regis_username');
        $regis_email = $this->input->post('regis_email');
        $regis_password = $this->input->post('regis_password');
        $signin_email = $this->input->post('signin_email');
        $a = $this->input->post('a');
        if ($a == 1) {
            $check_username = $this->login_authorization->check_username($regis_username, $a);
            if (!empty($check_username)) {
                echo '<div style="color:red;">Username not available</div>';
            }
        }
        if ($a == 2) {
            $check_email = $this->login_authorization->check_username($regis_email, $a);
//     print_r($check_email);
            if (!empty($check_email)) {
                echo '<div style="color:red;">Email already register please try new..</div>';
            }
        }
        if ($a == 4) {
            $check_email = $this->login_authorization->check_username($signin_email, $a);
            echo count($check_email);
//     print_r($check_email);
//            if (!empty($check_email)) {
//                echo '<div style="color:red;">Email already register please try new..</div>';
//            }
        }
    }
    
    
    #Register User
    function register_user() {
        $regis_username = $this->input->post('regis_username');
        $regis_email = $this->input->post('regis_email');
        $regis_password = $this->input->post('regis_password');

        if ($this->input->get('req_uri')) {
            $request_uri = $this->input->get('req_uri');
            $this->session->set_flashdata('req_uri', $request_uri);
        }
        $insert_user = $this->login_authorization->custome_user($regis_username, $regis_email, $regis_password, $id = '1');
        
        $email_to = $insert_user[0]['email_id'];
        if(!empty($insert_user)) {
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            $linksdata = base_url() . "authorization/activate_link/" . $insert_user[0]['random_key'];
            $logo = base_url() . "uploads/logo.png";
            $html_data = <<<END
            Dear Mr/Ms. $regis_username,
            <br><br>        
            Welcome to MYNEWCAR.in.<br> 
            Please confirm your email and become a member of India’s only one stop ONLINE solution for Car Buyers and Car owners
            <a href="$linksdata">Click here</a>.                                   
            <br><br>         
            Link Not working!? Please paste the URL below into your browser
            <br>            
            $linksdata            
            <br><br>                 
            <div>Regards<br>         
            Team MYNEWCAR.in<br>         
            <img src="$logo" style='width:20%'/><br>
            <br>                 
            Phone: 022 45 02 03 04 | Email: contact@mynewcar.in<br>         
            www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br>         
            ---------------------------------------------------------------------------------------------------------------------------------<br>         
            DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>         
            Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>         
            Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>         
END;
            if ($mandrill_ready) {
                $email = array(
                    'html' => $html_data, //Consider using a view file
                    'text' => 'You have a activation link',
                    'subject' => 'please activate your mnc account',
                    'from_email' => 'account@mnc.com',
                    'from_name' => 'MNC Account',
                    'to' => array(array('email' => $email_to))
                );
                $result = $this->mandrill->messages_send($email);
            }
            
        
            $this->session->set_flashdata('account_registered', "custom_account");
            if ($this->session->flashdata('req_uri')) {
                redirect($this->session->flashdata('req_uri'));
            } else {
                redirect('');
            }


            exit;
        }
        ?>
        <script>
            alert('Register Successfully...Please check your activation confirm link in the mail box...');
        </script>       
        <?php
        redirect('');
    }
    
    function activate_link($id) {
        $insert_user = $this->login_authorization->activate_user($id);
        
        if (!empty($insert_user[0])) {
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            $logo = base_url() . 'uploads/logo.png';
            $html_data = <<<END
            Dear Mr/Ms. $insert_user[0], 
            <br><br>        
            Congratulations! You are now a member of the MYNEWCAR.in club.
            <br>        
            Your login email ID: $insert_user[1]
            <br>        
            As a registered user you now enjoy privileged access to customize and to book your car. You may also write reviews and rate cars and related services on our site.
            <br><br>        
            Regards<br>
            Team MYNEWCAR.in<br>             
            <img src="$logo"/><br>        
            
            <br>        
            Phone: 022 45 02 03 04 | Email: contact@mynewcar.in<br>
            www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br>
            ---------------------------------------------------------------------------------------------------------------------------------<br>
            DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>
            Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>
            Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>
                          
END;
            $email_to = $insert_user[1];
            if ($mandrill_ready) {
                $email = array(
                    'html' => $html_data, //Consider using a view file
                    'text' => 'Your account activated successfully',
                    'subject' => 'Your account activated successfully',
                    'from_email' => 'account@mnc.com',
                    'from_name' => 'MNC Account',
                    'to' => array(array('email' => $email_to))
                );
                $result = $this->mandrill->messages_send($email);
            }
            $this->session->set_flashdata('account_registered', "account_activated_successfully");
            if ($this->session->flashdata('req_uri')) {
                redirect($this->session->flashdata('req_uri'));
            } else {
                redirect('');
            }
        }
        ?>
        <script>
            alert('Account Activated successfully');
        </script>       
        <?php
//        redirect('');
    }
    
    
    #### Profile Pic changed
    
    function user() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');
        
        ####Get City Id ####

        $date['city'] = $this->common_city();
        

        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }

        
        $data['userdata'] = $this->session->userdata('logged_in_user');
        
        
        $data['content'] = 'userpage';
        $userdata = $this->session->userdata('logged_in_user');
        $username = $userdata['username'];
        $user_id = $userdata['user_id'];
        if (empty($user_id)) {
            $user_id = '0';
        }
        $data['user_details'] = $this->profile->select_user_details($username);
//        print_r($data['user_details']);
        $data['user_car_confiuration'] = $this->profile->select_car_confiuration($user_id);
        $data['user_book_confiuration'] = $this->profile->select_book_car($user_id);
//        echo '<pre>';
        $data['user_rating'] = $this->profile->select_user_rating($user_id);
//        echo '</pre>';
        $data['user_groups'] = $this->profile->select_brand_user($user_id);
//        echo '</pre>';
        $this->load->view('index', $data);
    }
    
    
    
    function upload_profile() {
        $config['upload_path'] = 'uploads/user/';
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->set_allowed_types('*');
        $data['upload_data'] = '';
        $img_name = $this->upload->do_uploads('profile');

        $get_image = $this->login_authorization->upload_profile($img_name);
//        exit;
        redirect(base_url() . 'user');
    }
    
    
    function upload_cover() {
        $config['upload_path'] = 'uploads/cover/';
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->set_allowed_types('*');
        $data['upload_data'] = '';
        $img_name = $this->upload->do_uploads('cover_profile');
        $get_image = $this->login_authorization->upload_cover($img_name);
        redirect(base_url() . 'user');
    }
    
    function edit_profile() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');
        
        ####Get City Id ####

        $date['city'] = $this->common_city();
        

        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }

        
        $data['userdata'] = $this->session->userdata('logged_in_user');
        
        
        $data['content'] = 'edit_profile';
        if ($this->session->flashdata('alert_msgs')) {
            $data['alert_msgs'] = $this->session->flashdata('alert_msgs');
        }
        $userdata = $this->session->userdata('logged_in_user');
        $username = $userdata['username'];
        $data['city_res'] = $this->common_section->get_city();
        $data['state_res'] = $this->common_section->get_state();
        $data['user_details'] = $this->profile->select_user_details($username);
        $this->load->view('index', $data);
    }

    function edit_profile_upload() {
        $username = $this->input->post('username');
        $f_name = $this->input->post('f_name');
        $l_name = $this->input->post('l_name');
        $email = $this->input->post('email');
        $number = $this->input->post('number');
        $email = $this->input->post('email');
        $enquiry = $this->input->post('enquiry');
//        exit;
        $data = array(
            "username" => $username,
            "firstname" => $f_name,
            "lastname" => $l_name,
            "email" => $email,
            "telephone" => $number,
            "last_profile_updated" => date('Y-m-d h:i:s'),
            "address" => $enquiry
        );
        $data['user_details'] = $this->profile->update_details($data);
        $msg = 'Profile updated successfully.';
        $this->session->set_flashdata('alert_msgs', $msg);
        redirect('user');
    }
    
    function delete_configuration($id) {
//    echo $id;
        $data['user_rating'] = $this->profile->delete_configuration($id);
        redirect('user');
    }
    
    
    
    
    
    
    
    
    

}
?>
