<?php

class Product extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }
    
    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('login_authorization');
        $this->load->model('product_section');
        $this->output->nocache();
    }
    
    
    function car_rating($id, $id2) {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');
        $data['bank_per'] = $this->common_section->bank_per();
        ####Get City Id ####
        $date['city'] = $this->common_city();
        ####Get All City's data ####
        $data['all_Citys'] = $this->common_section->all_citys_detalis();
        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }        
        $data['userdata'] = $this->session->userdata('logged_in_user');           
        $user_id = $data['userdata']['user_id'];
        if ($user_id == '') {
            $user_id = '0';
        }
        
        
        
        
        $main_city = $this->common_city();
        $data['main_city'] = $main_city;
        $main_color = explode('_', $this->input->cookie('main_color', TRUE));
        $data['main_color'] = $main_color;
        $exp = explode('-', $id);
        $data['id'] = $exp[count($exp) - 1];
        $data['main_id'] = $id;
        $data['exp_select'] = $id2;
        $data['content'] = 'carRatingPage';
        
        $product_details[] = $this->home_feature->product_detail($data['id'], $comp_cond = '1');

        $product_details2 = array();
        foreach ($product_details as $product_details_data) {
            $product_color = array();
            if (!empty($product_details_data[0]['body_color_id'])) {
                $product_details_data[0]['body_color_id'];
                $color_id_by_pro = $product_details_data[0]['body_color_id'];
                $product_color = $this->product_section->product_color($color_id_by_pro, $comp_cond = '2');
                $product_details2[] = array_merge($product_details_data, $product_color);
            } else {
                $product_details2[] = $product_details_data;
            }
        }
        
        $data['pro_specification'] = $this->product_section->pro_specification($data['id']);
//        print_r($data['pro_specification']);
        $data['product_details'] = $product_details2;
        $data['exp_review_count'] = $this->product_section->expert_review_count($data['id'], $id = '1');
        
        $data['user_review_count'] = $this->product_section->expert_review_count($data['id'], $id = '2');
        
        $data['pro_details'] = $this->product_section->reting_product_detail($data['id']);
//
//        print_r($data['user_review_count']);
//        exit;

        $data['pro_main_details'] = $data['pro_details'];
        $data['variant_details'] = $this->product_section->product_variant_detail($data['id']);

        $data['features_names'] = $this->product_section->get_features_name($data['id'], $id = '');
        $data['pro_feture'] = $this->product_section->product_features($data['id']);
        $data['pro_ext'] = $this->product_section->rating_details($data['id']);
        $data['fuel'] = $this->common_section->all_fuel();
        $type = 'All';
        $data['car_gallery'] = $this->product_section->car_gallery_detail($data['id']);
        $data['car_intro_gallery_detail'] = $this->product_section->car_intro_gallery_detail($data['id'], $type);
        $pro_name = $data['pro_ext'][0]['brand_name'];
        $data['selected_offers'] = $this->product_section->select_offers_product($pro_name, $cond = '2');
        

//        $data['final_result'] = $this->pro_price_calculation($data['id']);

        $data['onroad_detail'] = $this->product_section->onraod_detail($data['id']);
        $data['car_color'] = $this->product_section->car_specifiaction($data['id'], $type = 'color');
        $data['finance'] = $this->product_section->accessory_services($data['id'], $user_id, $id = '1');
        $data['amc_contract'] = $this->product_section->accessory_services($data['id'], $user_id, $id = '2');
        $data['ins_car'] = $this->product_section->accessory_services($data['id'], $user_id, $id = '3');
        $data['extended_waranty'] = $this->product_section->accessory_services($data['id'], $user_id, $id = '4');
        $data['used_cars'] = $this->product_section->accessory_services($data['id'], $user_id, $id = '5');
        $data['home_delivery'] = $this->product_section->accessory_services($data['id'], $user_id, $id = '6');
        $emi_id = $this->input->cookie('emi_id', TRUE);
        $data['get_emi'] = $this->product_section->get_emi_record($emi_id, $data['id']);
        $data['total_saving'] = $this->product_section->get_total_saving($data['id'], $user_id);
        $data['onroad_price_breakup'] = $this->product_section->get_onroad_price($data['id'], $user_id);
        $fuel_type = $data['product_details'][0][0]['fuel_type'];
        $cook_coupon_id = $this->input->cookie('coupon_id', TRUE);
        if (!empty($cook_coupon_id)){
            $exp_cook_coupon_id = explode('_-_', $cook_coupon_id);
            
            if (count($exp_cook_coupon_id) == 4) {
                $c_coupon_id = $exp_cook_coupon_id[1];
                
                if($date['city']==$exp_cook_coupon_id[3])
                {
                
                $data['coupon_data'] = $this->product_section->coupon_discount($c_coupon_id);
//                print_r($data['coupon_data']);    
                }
            }
        }
        $data['pro_price_details'] = $this->product_section->price_specification($data['id']);
        $data['pro_conf'] = $this->product_section->product_configure($data['id'],$user_id,$main_city);
//        print_r($data['pro_conf']);
        $data['seo'] = $this->product_section->get_meta($cond = '1', $data['id']);
        $this->load->view('index', $data);
    }
    
    
    function coupon_code_apply() {
        $coupon_code = $this->input->post('coupon_code');
        $pro_id = $this->input->post('pro_id');
        $color = $this->input->post('color');
        $city = $this->common_city();
        $data['main_city'] = $city;
        echo $coupon_check = $this->product_section->coupon_configuration($coupon_code, $pro_id, $city, $color);

    }
    
}
