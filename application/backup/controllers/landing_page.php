<?php

class Landing_page extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

//
    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('login_authorization');
        $this->load->model('landing_page_section');
        $this->output->nocache();
    }
    
    
    function landing_page_pro($name) {
       
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####
//        $all_Citys = $this->home_section->all_citys_detalis();

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }
        
        
        $exp = explode('-', $name);
        $final = '';
        for ($i = 0; $i <= count($exp) - 1; $i++) {
            $exp[$i];
            $final.=$exp[$i] . ' ';
        }
        $data['final'] = rtrim($final);
        $data['landing_data'] = $this->landing_page_section->get_landing_data($data['final']);
//        print_r($data['landing_data']);
        $data['content'] = 'landing_page';
        echo $this->load->view('index', $data);

    }
    
    function landing_data() {
        $name = $_POST['text1'];
        $lname = $_POST['text01'];
        $numbar = $_POST['text2'];
        $email = $_POST['text3'];
        $book = $_POST['text5'];
        $url = $_POST['url'];
        $landing_city = $_POST['landing_city'];
        $url = preg_replace('/\s+/', '_', $url);

        $data['data'] = array('name' => $name, 'lname' => $lname, 'book' => $book, 'email_id' => $email, 'numbar' => $numbar, 'landing_city' => $landing_city);

        $mandrill_api_key = Mandrill_key;
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        if ($mandrill_ready) {
            $email = array(
                'html' => $this->load->view('emailers/landing_data_enquiry', $data, TRUE), //Consider using a view file
                'text' => 'Enquiry for Landing Deal',
                'subject' => 'Enquiry for Landing Deal',
                'from_email' => $email,
                'from_name' => $name,
                'to' => array(
                    array('email' => 'contact@mynewcar.in'),
                    array('email' => 'sbhuia@mynewcar.in'),
                    array('email' => 'contact.mynewcar@gmail.com'),
                    array('email' => 'mvasudev@mynewcar.in')),
//                    array('email' => 'dinesh@projectheena.com'))
                'to' => array(array('email' => 'contact@mynewcar.in'))
            );
            $result = $this->mandrill->messages_send($email);
        }
        $this->session->set_flashdata('landing_enquiry', 'landing_enquiry');
        redirect(base_url() . 'landing-page/' . $url);
    }
    
    
    
    
    
    
}
