<?php 
class Thankyou extends CI_Controller 
{
     public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
//            exit;
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    } 

    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
//        $this->load->model('forum_section');
        $this->load->model('login_authorization');
//        $this->load->model('profile');
//        $this->load->model('faq');
        $this->output->nocache();
    }

    function thank_you() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();





        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }


        $data['content'] = 'thankyou';


        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');

        $data['userdata'] = $this->session->userdata('logged_in_user');


        $this->load->view('index', $data);
    }
} 
