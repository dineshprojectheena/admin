<?php

class Coming_soon extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

//
    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('home_section');
        $this->load->model('login_authorization');
        $this->load->model('coming_soon_section');
        $this->output->nocache();
    }

    function coming_soon_pro() {

        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }


//        $data['userdata'] = $this->session->userdata('logged_in_user');
//        
        $data['all_coming_soon'] = $this->coming_soon_section->get_coming_soon();
        
        $data['get_estimated_price'] = $this->coming_soon_section->get_estimated_price($id = '1');
        $final_get_estimated_price = array();
        foreach ($data['get_estimated_price'] as $get_estimated_price_data) {
            $exp_get_estimated_price_data = explode(' ', $get_estimated_price_data['extimated_price']);
            $final_get_estimated_price[] = $exp_get_estimated_price_data;
        }
        array_multisort($final_get_estimated_price, SORT_ASC, $final_get_estimated_price);


        $data['get_estimated_price'] = $final_get_estimated_price;





        $data['get_estimated_max_price'] = $this->coming_soon_section->get_estimated_price($id = '2');
        $final_get_estimated_max_price = array();
        foreach ($data['get_estimated_max_price'] as $get_estimated_max_price_data) {
            $exp_get_estimated_max_price_data = explode(' ', $get_estimated_max_price_data['extimated_price_max']);
            $final_get_estimated_max_price[] = $exp_get_estimated_max_price_data;
        }
        array_multisort($final_get_estimated_max_price, SORT_ASC, $final_get_estimated_max_price);


        $data['get_estimated_max_price'] = $final_get_estimated_max_price;

//        print_r($data['get_estimated_max_price']);


        $data['get_estimated_lauch_date'] = $this->coming_soon_section->get_estimated_lauch_date();

        $data['content'] = 'coming_soon';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');
//        $data['seo_tags'] = $this->home_section->home_seo_data($data['content'], $id = '2');
        echo $this->load->view('index', $data);
    }

    function check_max_price() {
        $estimated_price = $this->input->post('estimated_price');
        $estimated_max_price = $this->input->post('estimated_max_price');
        $get_estimated_price = $this->input->post('get_estimated_price');
        $data['inactive'] = $this->coming_soon_section->checl_estimated_price($id = '1', $estimated_price);
        $final_get_estimated_price = array();
        foreach ($data['inactive'] as $get_estimated_price_data) {
            $exp_get_estimated_price_data = $get_estimated_price_data['extimated_price_max'];
            $final_get_estimated_price[] = $exp_get_estimated_price_data;
        }
        array_multisort($final_get_estimated_price, SORT_ASC, $final_get_estimated_price);
        $data['inactive'] = $final_get_estimated_price;

        
        
        
        
        $data['active'] = $this->coming_soon_section->checl_estimated_price($id = '2', $estimated_price);
//        print_r($data['active']);
        $final_get_estimated_max_price = array();
        foreach ($data['active'] as $get_estimated_max_price_data) {
            $exp_get_estimated_max_price_data = $get_estimated_max_price_data['extimated_price_max'];
            $final_get_estimated_max_price[] = $exp_get_estimated_max_price_data;
        }
        
        array_multisort($final_get_estimated_max_price, SORT_ASC, $final_get_estimated_max_price);
//        print_r($final_get_estimated_max_price);

        $data['active'] = $final_get_estimated_max_price;
        
        
        
        $exp_prices = explode('-', $get_estimated_price);
//        print_r($exp_prices);
        ?>
        <select id="estimated_max_price" onchange="price_checking2();" name="estimated_max_price" class="form-control">
            <option value="100">Max Value</option>
            <?php
            foreach($data['inactive'] as $inactive_data)
            {    
            ?>
             <option disabled="disabled" <?php if($exp_prices[1]==$inactive_data){ echo 'selected';}?> value="<?php echo preg_replace('/\s+/', '_', ($inactive_data)); ?>"><?php echo $inactive_data.' Lakhs'?></option>                                     
            <?php
            }
            ?>
            <?php
            foreach($data['active'] as $active_data)
            {    
            ?>
             <option value="<?php echo $active_data; ?>" <?php if($exp_prices[1]==$active_data){ echo 'selected';}?>><?php echo $active_data.' Lakhs'?></option>                                     
            <?php
            }
            ?>                    
        </select>


        <?php

    }

    function coming_enquiry() {
        $test_full_name = $this->input->post('enq_full_name');
        $test_email = $this->input->post('enq_email');
        $test_phone = $this->input->post('enq_phone');
        $enq_comment = $this->input->post('enq_comment');
        $enq_com_product = $this->input->post('enq_com_product');
        $yes_upcoming_car = $this->input->post('yes_upcoming_car');
        if ($yes_upcoming_car != '') {
            $yes_upcoming_car = 'Upcoming Car: Customer will alert you with information about Upcoming Cars regularly!';
        }
        if (!empty($request)) {
            $request = 'yes';
        } else {
            $request = 'no';
        }

        $mandrill_api_key = Mandrill_key;
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        $html_data = <<<END
            Following Feedback/Callback Enquiry Filled By User:-<br> 
            Full Name: $test_full_name<br> 
            Email: $test_email<br> 
            Phone: $test_phone<br> 
            Product: $enq_com_product<br> 
            $yes_upcoming_car    
END;
        
        $message='We will alert you with information about Upcoming Cars regularly.';
        $this->sms_send->sms_pass($test_phone,$message);
        if ($mandrill_ready) {
            $email = array(
                'html' => $html_data, //Consider using a view file
                'text' => 'Following Coming Soon Product Enquiry Filled By User',
                'subject' => 'Coming Soon Enquiry Details',
                'from_email' => 'account@mnc.com',
                'from_name' => 'Upcoming Cars Enquiry',
                'to' => array(
                    array('email' => 'contact@mynewcar.in'),
                    array('email' => 'sbhuia@mynewcar.in'),
                    array('email' => 'contact.mynewcar@gmail.com'),
//                    array('email' => 'dinesh@projectheena.com'),
//                    array('email' => 'webdeveloperdinesh@gmail.com')
                    array('email' => 'mvasudev@mynewcar.in')
                )
            );
            $result = $this->mandrill->messages_send($email);
        }
        $this->session->set_flashdata('coming_success', 'coming_success');
        ?>
        <script>
            history.go(-1);
        </script>   
        <?php

    }

}
