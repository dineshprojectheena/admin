<?php

class Compare extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('login_authorization');
        $this->load->model('comparision_section');
        $this->output->nocache();
    }

    function comparision($id) {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');
        ####Get City Id ####
        $date['city'] = $this->common_city();

        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }


        $data['userdata'] = $this->session->userdata('logged_in_user');


        $exp_car = explode('-VS-', $id);

        $product_id = array();
        $product_details1 = array();
        $pro_ids = '';
        for ($i = 0; $i < count($exp_car); $i++) {
            $exp_id = explode('-', $exp_car[$i]);
            $tot = count($exp_id);
            $pro = $exp_id[$tot - 1];
            $pro_id = "'" . $pro . "'";
            $pro_ids = $pro_id . ',' . $pro_ids;
            $product_id[] = array('pro_id' => $pro);

            $product_details[] = $this->comparision_section->product_detail($pro, $comp_cond = '1');

            $product_details2[] = $this->comparision_section->product_features($pro);
        }

        $pro_ids;
        $pro_ids = substr($pro_ids, 0, -1);
        $data['product_details2'] = $product_details2;
        $product_details2 = array();
        foreach ($product_details as $product_details_data) {
            $data['variant_color'] = $this->comparision_section->product_comp_color($product_details_data[0]['variant_id']);
            $color = array('Color' => $data['variant_color']);

            $data['variant_details'] = $this->comparision_section->product_variant_detail($product_details_data[0]['variant_id']);
            $product_details2[] = array_merge($product_details_data, $data['variant_details'], $color);
        }

        $data['get_brands'] = $this->common_section->get_brands($brand_id = '');
        for ($j = 1; $j <= 6; $j++) {
            $dist_features_names[] = $this->comparision_section->get_features_name($pro_ids, $j);
        }
        $data['features_names'] = $dist_features_names;
        $data['product_details'] = $product_details2;

        $data['content'] = 'comparision';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['content'], $id = '1');
        $this->load->view('index', $data);
    }

}
