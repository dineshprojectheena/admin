<?php

class Scripts extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_section');
        $this->load->model('home_feature');
        $this->load->model('scripts_section');
        $this->load->model('login_authorization');
        $this->load->model('comparision_section');
        $this->output->nocache();
    }

    function sitemap_data() {
        
        $data['all_brands'] = $this->common_section->get_brands($id = '');
        # All brnad page urls
        $file = "sitemap.xml";  // output file
        $extension = ".html";  // scan files with extension
        $freq = "daily";   // scan frequency
        $priority = "0.2";   // site priority
        $pf = fopen($file, "w");
        if (!$pf) {
            echo "cannot create $file\n";
            return;
        } 
        $base_url = base_url();
        fwrite($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<urlset
      xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
      xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
      xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">
<!-- created by dinesh kumawat -->
");
        fwrite($pf, "
  <url>
  <loc>$base_url</loc>
  <changefreq>$freq</changefreq>
  <priority>1.0</priority>
   </url>    
");

        $url = array(base_url() . 'about-us', base_url() . 'car-enquiry',
            base_url() . 'privacy-policy', base_url() . 'terms-of-offer-for-sale'
            , base_url() . 'user-terms', base_url() . 'new-car-deals-discounts/'
            , base_url() . 'faq');

        foreach ($data['all_brands'] as $all_brands) {
            $url[] = base_url() . 'brand/' . $all_brands['brand_name'];
        }

        $data['all_product'] = $this->scripts_section->get_product();
        foreach ($data['all_product'] as $all_product) {
            $url[] = site_url('car/' . preg_replace('/\s+/', '-', $all_product['pro_name_comp']) . '-' . $all_product['variant_id']);
        }
        
        // 
        

        foreach ($url as $url_data) {
            fwrite($pf, "
  <url>
  <loc>$url_data/</loc>
  <changefreq>$freq</changefreq>
  <priority>$priority</priority>
   </url>    
");
        }
  }

}
