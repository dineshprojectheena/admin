<?php 
class Lease extends CI_Controller 
{
     public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
//            exit;
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    } 

    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('lease_section');
        $this->load->model('login_authorization');
        $this->output->nocache();
    }

    function leasing() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');
        $data['get_lease_brands'] = $this->common_section->get_lease_brands($brand_id = '');
        $date['city'] = $this->common_city();
        $data['all_Citys'] = $this->common_section->all_citys_detalis();
        $data['content'] = 'leasing';
        $this->load->view('index', $data);
    }
    function lease_upload() {
     $name=$_POST['name'];    
     $company_name=$_POST['company_name'];    
     $email=$_POST['email'];    
     $number=$_POST['number'];    
     echo $city=$_POST['city'];
        if($city=='Other Cities')
        {
        $city_name = $this->input->post('city2');    
        }
        else
        {    
        echo $city_name = $this->input->post('request_main_city');
        }
//     exit;
     
     $data['all_Citys'] = $this->common_section->get_single_city($city);
     
     if(!empty($data['all_Citys']))
     {    
     $city_name=$data['all_Citys']['name'];
     }
         
     $carbrands=$_POST['carbrands'];    
     $brand_model=$_POST['brand_model'];  
     $data['brand_model'] = $this->common_section->get_brand_model($carbrands,$brand_model);
     $model_name=$data['brand_model']['model_name'];
     $brand_name=$data['brand_model']['brand_name'];
     $i_am=$_POST['i_am'];    
     
     $data['lease_car'] = $this->lease_section->lease_a_car($name,$company_name,$Designation,$number,$city,$carbrands,$brand_model,$i_am);
     
     $mandrill_api_key = Mandrill_key;
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try {
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        $html_data = <<<END
            Following Leasing Details Filled By User:-<br> 
            Full Name: $name<br> 
            Email: $email<br> 
            Company Name: $company_name<br> 
            Number: $number<br>             
            City: $city_name<br>
            Brand: $brand_name<br>
            Model: $model_name<br>
            I am: $i_am<br>                
END;
        
        
        if ($mandrill_ready){
            $email=array(
                'html' => $html_data, //Consider using a view file
                'text' => 'Following Leasing Enquiry Filled By User',
                'subject' => 'Leasing Enquiry Details',
                'from_email' => 'account@mnc.com',
                'from_name' => 'Leasing Enquiry',
                'to' => array(
                    array('email' => 'contact@mynewcar.in'),
                    array('email' => 'sbhuia@mynewcar.in'),
                    array('email' => 'contact.mynewcar@gmail.com'),
                    array('email' => 'mvasudev@mynewcar.in'),
                    array('email' => 'dinesh@projectheena.com')
                )
            );
            $result = $this->mandrill->messages_send($email);
        }
        $this->session->set_flashdata('leasing_success', 'leasing_success');
        redirect('');
    }
    
    
} 
