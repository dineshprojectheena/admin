<?php

class Payment extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('login_authorization');
        $this->load->model('payment_section');
        $this->output->nocache();
    } 

    function car_payment() {
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');
        ####Get City Id ####
        $date['city'] = $this->common_city();
        ####Get All City's data ####
        $data['all_Citys'] = $this->common_section->all_citys_detalis();
        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }
        $data['userdata'] = $this->session->userdata('logged_in_user');
        $user_id = $data['userdata']['user_id'];
        if ($user_id == '') {
            $user_id = '0';
        }



        $pro_id = $this->input->post('pro_id');
        if ($pro_id == '') {
            redirect();
        }      
        
        
        
        $rating_variant = $this->input->post('rating_variant');
        $final_total_saving_price = $string = preg_replace('/[^A-Za-z0-9\-]/', '', ($this->input->post('final_total_saving_price')));
//        echo '<br>';
        $final_mnc_on_road_price = $string = preg_replace('/[^A-Za-z0-9\-]/', '', ($this->input->post('final_mnc_on_road_price')));
//        echo '<br>';
        $color = $this->input->post('color');
        
        
//        exit;
        if (isset($_POST['car_insurance'])) {
            $car_insurance = '1';
        } else {
            $car_insurance = '';
        }
        $car_insurance;

        if (isset($_POST['car_exchange_data'])) {
            $car_exchange = '1';
        } else {
            $car_exchange = '';
        }
        $car_exchange;

        if (isset($_POST['car_finance_data'])) {
            $car_finance = '1';
        } else {
            $car_finance = '';
        }
        $car_finance;


        if (isset($_POST['home_delivery'])) {
            $home_delivery = '1';
        } else {
            $home_delivery = '';
        }
        $home_delivery;



        if (isset($_POST['corporate_discount'])) {
            $corporate_dis = '1';
        } else {
            $corporate_dis = '';
        }
        $corporate_dis;

        if (isset($_POST['loyality_bonus'])) {
            $loyality_bonus = '1';
        } else {
            $loyality_bonus = '';
        }
        $loyality_bonus;

        if (isset($_POST['extended_waranty'])) {
            $extended_waranty = '1';
        } else {
            $extended_waranty = '';
        }
        $extended_waranty;       
        
        

        $emi_id = $this->input->post('emi_id');
        $emi_amount = $this->input->post('car_finance_price');


        $data['id'] = $pro_id;
        $data['main_id'] = $id;

        $data['product_configuration'] = $this->payment_section->product_configuration($pro_id, $color, $user_id, $emi_amount, $car_finance, $car_insurance, $extended_waranty, $car_exchange, $home_delivery, $corporate_dis, $loyality_bonus, $emi_id, $final_total_saving_price, $final_mnc_on_road_price);
        $data['booking_amount'] = $this->payment_section->booking_amount($pro_id);
//        print_r($data['booking_amount']);

        $data['car_into_details'] = $this->payment_section->car_intro_detail($data['id']);
        $data['user_details'] = $this->payment_section->order_billing_details($user_id, $id = '1');
        $data['shipping_details'] = $this->payment_section->order_billing_details($user_id, $id = '2');
//        print_r($data['product_configuration']);
        if (!empty($data['product_configuration']['car_configuration_id'])) {
            setcookie('configurator_id', $data['product_configuration']['car_configuration_id'] . '--' . $pro_id, time() + (86400 * 30), "/");
        }
        $data['content'] = 'paymentpage';
        $this->load->view('index', $data);
    }

    function secure() {
        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        if (empty($user_id)) {
            redirect('');
        }
        
        $fewamaount = $_POST['fewamaount'];
        $pro_id = $_POST['pro_id'];
        $booking_amount= $this->payment_section->booking_amount($pro_id);
//        print_r($data['booking_amount']);

        if ($pro_id == $booking_amount['variant_id']) {
            if ($booking_amount['booking_amount'] > 0) {
                $booking_price = $booking_amount['booking_amount'];
            } else {
                $booking_price = trans_amt;
            }
        }


        $hash = "f2793869fb81dc3f041d415781dec693" . "|" . $_POST['account_id'] . "|" . $booking_price . "|" . $_POST['reference_no'] . "|" . $_POST['return_url'] . "|" . $_POST['mode'];
        $secure_hash = md5($hash);
        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        if ($user_id == '') {
            $user_id = '0';
        }
       



        $tot_price = $_POST['tot_price'];
        $tot_days = $_POST['tot_days'];
        $on_road_price = $_POST['on_road_price'];
        $group_discount = $_POST['group_discount'];
        setcookie('group_discount', $group_discount, time() + (86400 * 30), "/");
        $this->session->set_flashdata('order_pro_id', $pro_id);
        $name = $_POST['name'];
        $l_name = $_POST['l_name'];
        $telephone = $_POST['phone'];
        $city = $_POST['city'];
        $state = $_POST['state'];
        $postal_code = $_POST['postal_code'];
        $country = $_POST['country'];
        $address = $_POST['address'];
        $payment = $_POST['payment'];


        $ship_name = $_POST['ship_name'];
        $ship_l_name = $_POST['ship_l_name'];
        $ship_address = $_POST['ship_address'];
        $ship_city = $_POST['ship_city'];
        $ship_state = $_POST['ship_state'];
        $ship_postal_code = $_POST['ship_postal_code'];
        $ship_country = $_POST['ship_country'];
        $ship_phone = $_POST['ship_phone'];
        $ship_email = $_POST['ship_email'];
        $email = $_POST['email'];
        $date = date('Y-m-d');
        $payment = 'ebs';
        $check = $this->payment_section->user_address_update($name, $l_name, $telephone, $city, $state, $postal_code, $country, $address, $user_id);
        
        $shipping_address = $this->payment_section->user_shipping_address($ship_email, $ship_name, $ship_l_name, $user_id, $ship_phone, $ship_city, $ship_state, $ship_postal_code, $ship_country, $ship_address, $date, $check,$city);
        if($payment == 'ebs'){
        ?> 
<!--<form  method="post" action="<?php echo base_url() . 'booking-success'; ?>" name="frmTransaction" id="frmTransaction" onSubmit="return validate()" style="display:none;">-->
            <form  method="post" action="https://secure.ebs.in/pg/ma/sale/pay" name="frmTransaction" id="frmTransaction" onSubmit="return validate()"  style="display:none;">
                <input name="account_id" type="text" value="<?php echo $_POST['account_id'] ?>">
                <input name="return_url" type="text" size="60" value="<?php echo $_POST['return_url'] ?>" />
                <input name="mode" type="text" size="60" value="<?php echo $_POST['mode'] ?>" />
                <input name="reference_no" type="text" value="<?php echo $_POST['reference_no'] ?>" />
                <input name="amount" type="text" value="<?php echo $booking_price; ?>"/>
                <input name="description" type="text" value="<?php echo $_POST['description'] ?>" /> 
                <input name="name" type="text" maxlength="255" value="<?php echo $_POST['name'] ?>" />
                <input name="address" type="text" maxlength="255" value="<?php echo $_POST['address'] ?>" />
                <input name="city" type="text" maxlength="255" value="<?php echo $_POST['city'] ?>" />
                <input name="state" type="text" maxlength="255" value="<?php echo $_POST['state'] ?>" />
                <input name="postal_code" type="text" maxlength="255" value="<?php echo $_POST['postal_code'] ?>" />
                <input name="country" type="text" maxlength="255" value="<?php echo $_POST['country'] ?>" />
                <input name="phone" type="text" maxlength="255" value="<?php echo $_POST['phone'] ?>" />
                <input name="email" type="text" size="60" value="<?php echo $_POST['email'] ?>" />
                <input name="ship_name" type="text" maxlength="255" value="<?php echo $_POST['ship_name'] ?>" />
                <input name="ship_address" type="text" maxlength="255" value="<?php echo $_POST['ship_address'] ?>" />
                <input name="ship_city" type="text" maxlength="255" value="<?php echo $_POST['ship_city'] ?>" />
                <input name="ship_state" type="text" maxlength="255" value="<?php echo $_POST['ship_state'] ?>" />
                <input name="ship_email" type="text" maxlength="255" value="<?php echo $_POST['$ship_email'] ?>" />
                <input name="ship_postal_code" type="text" maxlength="255" value="<?php echo $_POST['ship_postal_code'] ?>" />
                <input name="ship_country" type="text" maxlength="255" value="<?php echo $_POST['ship_country'] ?>" />
                <input name="ship_phone" type="text" maxlength="255" value="<?php echo $_POST['ship_phone'] ?>" />
                <input name="secure_hash" type="text" size="60" value="<?php echo $secure_hash; ?>" />
                <input name="submitted" value="Submit" style="display:none;" id="submit" type="submit" />

            </form>
            <script>
                            document.getElementById('submit').click();
            </script>
            <?php
        } else {
            $this->load->library('encdec_paytm');
            $configurator_id = $this->input->cookie('configurator_id', TRUE);
            $configurator_id_exp = explode('--', $configurator_id);
            $data['product_configuration'] = $this->payment_section->order_configuration($configurator_id_exp[0]);
            $checkSum = "";
            $paramList = array();
            $ORDER_ID = $data['product_configuration']['invoice_no'];
            $CUST_ID = $data['product_configuration']['user_id'];
            $INDUSTRY_TYPE_ID = 'Retail102';
            $CHANNEL_ID = 'WEB';
            
            $TXN_AMOUNT = $booking_price;
// Create an array having all required parameters for creating checksum.
            $paramList["MID"] = PAYTM_MERCHANT_MID;
            $paramList["ORDER_ID"] = $ORDER_ID;
            $paramList["CUST_ID"] = $CUST_ID;
            $paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
            $paramList["CHANNEL_ID"] = $CHANNEL_ID;
            $paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
            $paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
            $checkSum = $this->encdec_paytm->getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY, $sort = 1);
            ?>
            <form method="post" action="<?php echo PAYTM_TXN_URL ?>" name="f1">
            <?php
            foreach ($paramList as $name => $value) {
                echo '<input type="text" name="' . $name . '" value="' . $value . '">';
            }
            ?>
                <input type="text" name="CHECKSUMHASH" value="<?= $checkSum ?>">
                <script type="text/javascript">
                    document.f1.submit();
                </script>
            </form>
            <?php
        }
    }
    
    
    
    

    function send_sms($postData) {
//            print_r($postData);
        $url = "http://sms.rhibhuz.com/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
                //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);
        if (curl_errno($ch)) {
            return array('error' => curl_error($ch));
        }

        curl_close($ch);
        return $output;
    }
    

    function booking_success() {
        header("Pragma: no-cache");
        header("Cache-Control: no-cache");
        header("Expires: 0");
        $this->load->library('encdec_paytm');
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');
        ####Get City Id ####
        $date['city'] = $this->common_city();
        ####Get All City's data ####
        $data['all_Citys'] = $this->common_section->all_citys_detalis();
        #### Affiliate code here ####
        $data['userdata'] = $this->session->userdata('logged_in_user');
        $user_id = $data['userdata']['user_id'];
        if ($user_id == '') {
            $user_id = '0';
        }
        
        
        
        
        $data['customer_details'] = $this->payment_section->customer_details($user_id);
        
        if ($_POST['STATUS'] == 'TXN_FAILURE' && $_POST['STATUS'] != '') {
            $data['fail'] = "fail";
            $contact_no = $data['customer_details'][0]['telephone'];
            $msg = 'Sorry! Transaction failed on MyNewCar.in ,Requesting you to please book the car again or call 022-45020304 !';
            $auth_key = '89443AT3NmfHZ55b85230';
            $senderId = 'MNCIND';
            $route = 4;
            $postData = array(
                'authkey' => $auth_key,
                'mobiles' => $contact_no,
                'message' => urlencode($msg),
                'sender' => $senderId,
                'route' => $route
            );
            $data['content'] = 'bookingSuccess';
            $this->load->view('index', $data);
        } else {
            $data['fail'] = "pass";
            $configurator_id = $this->input->cookie('configurator_id', TRUE);  
            $configurator_id_exp = explode('--', $configurator_id);
            $data['product_configuration'] = $this->payment_section->order_configuration($configurator_id_exp[0]);
                       
            $data['order_result'] = $this->payment_section->order_confirm($configurator_id_exp[0], $configurator_id_exp[1], $user_id);
//            print_r($data['order_result']);
            
            $data['paid_amount']=$data['order_result']['paid_amount'];
            $order_id=$data['order_result']['order_id'];
            $car_configuration_id = $data['order_result']['car_configuration_id'];
            $order_email = $data['order_result']['email'];
            $firstname = $data['order_result']['firstname'];
            $invoice_no = $data['order_result']['invoice_no'];
            $city_name = $data['order_result']['city_name'];
            $pro_name_comp = $data['order_result']['pro_name_comp'];
            $on_road_price = $data['order_result']['on_road_price'];
            $order_id = $data['order_result']['order_id'];
            $product_name = $data['order_result']['product_name'];
            $price = $data['order_result']['tot_price]'];
            $dis_price = $data['order_result']['dis_price]'];

            $id = $data['order_result']['product_id'];
            $data['id'] = $data['order_result']['product_id'];
            $data['main_id'] = $data['order_result']['product_id'];

            $data['car_into_details'] = $this->payment_section->car_intro_detail($id);


            $data['customer_details'] = $this->payment_section->customer_details($user_id);

            $data['registeration_details'] = $this->payment_section->registeration_details($user_id);

            $data['car_configuration'] = $this->payment_section->car_configuration($user_id, $id, $cond = '2', $invoice_no);

            $data['coupon_used'] = $this->payment_section->coupon_used($order_id);

            $contact_no = $data['customer_details'][0]['telephone'];

//            $msg = 'Congratulations on your booking with MYNEWCAR.IN! Your booking number is ' . $invoice_no . '.Reach us at 022-45020304 or contact@mynewcar.in for any queries.';
            $msg = 'Congratulations on your booking with MYNEWCAR.IN!
                    Your booking number is '.$invoice_no.' Reach us at 022-45020304 or contact@mynewcar.in for any queries.
                    '.$invoice_no.' is our specific booking number which needs to be customised.';
            $auth_key = '89443AT3NmfHZ55b85230';
            $senderId = 'MNCIND';
            $route = 4;
            $postData = array(
                'authkey' => $auth_key,
                'mobiles' => $contact_no,
                'message' => urlencode($msg),
                'sender' => $senderId,
                'route' => $route
            );
//            print_r($postData);
//            
//            
//            
//            exit;

            

            if (!empty($invoice_no)) {
                $logo = base_url() . 'uploads/logo.png';

                $html_data = <<<END
                        <div align="left" style="width:100%;">
                        <div style="float:left;">   
                        Dear Mr/Ms. $firstname,<br><br>

                        Greetings of the Day!!<br><br>

                        Thank you for booking your new $pro_name_comp in $city_name on MYNEWCAR.in. We hereby acknowledge your Booking Order No. $invoice_no.<br><br>

                        Please find attached the booking confirmation for your reference.<br><br>

                        Our Customer Care will get in touch with you within the next 48 hours to confirm the delivery details along with offers on other related services and accessories requested by you.<br><br>    
                            
                        Regards,<br>
Team MYNEWCAR.in<br>
<img src="$logo" style="width:30%;"/><br>

Phone: 022 45 02 03 04 | Email: contact@mynewcar.in<br>
www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br><br>
---------------------------------------------------------------------------------------------------------------------------------
<br><br>
DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>
Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>
Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>        
                                
                                
                        </div>        
                        </div> 
                        <br><br>
    

END;
               
                
                $data['emi'] = $this->payment_section->car_emi($id, $user_id);
                $data['booking_varaint'] = $this->payment_section->booking_detail($data['id'], $user_id, $cond = '1');
                $data['booking_exterior'] = $this->payment_section->booking_detail($data['id'], $user_id, $cond = '2');
                $data['booking_interior'] = $this->payment_section->booking_detail($data['id'], $user_id, $cond = '3');
                $data['booking_accessory'] = $this->payment_section->booking_detail($data['id'], $user_id, $cond = '4');
                $data['pro_feture'] = $this->payment_section->booking_product_features($data['id']);
                $data['pro_specification'] = $this->payment_section->pro_specification($data['id']);
                $data['onroad_price_breakup'] = $this->payment_section->get_onroad_price($data['id'], $user_id = '0');
                 $html = $this->load->view('test', $data, TRUE);
//                echo $html;
//                echo APPPATH . '/third_party/Mpdf/mpdf.php';
                require_once APPPATH . '/third_party/Mpdf/mpdf.php';
                $mpdf = new mPDF('c', 'A4', '', '', 0, 0, 0, 0, 0, 0);
               
                $mpdf->WriteHTML($html);
                $pdf_name = 'MyNewCar-Invoice' . date('Y-m-d-H-i-s') . '-' . $user_id . '-' . $data['id'] . '.pdf';
               $pdf_name = 'MyNewCar-Invoice' . date('Y-m-d-H-i-s') . '-' . $user_id . '-' . $data['id'] . '.pdf';
                $pdfFilePath = FCPATH . "uploads/car_con_download/$pdf_name";
                $link = APPPATH . "../uploads/car_con_download/$pdf_name";
//                echo $link = APPPATH . "../admin/uploads/car_con_download/$pdf_name"; 
                $uploadPath = $pdf_name;
                $mpdf->Output($pdfFilePath, 'F');

                $datas = $this->input->post();
                
                $fileContent = @file_get_contents($link);
                $encodedContent = @base64_encode($fileContent);


                ## Mail validation to send one time email to the customer
                $mail_status = $this->input->cookie('mail_status', TRUE);

//                    if ($id . '-' . $user_id . '-' . $order_id . '-sent' != $mail_status) {
                $mandrill_api_key = Mandrill_key;
                $this->load->library('Mandrill');
                $mandrill_ready = NULL;
                try {
                    $this->mandrill->init($mandrill_api_key);
                    $mandrill_ready = TRUE;
                } catch (Mandrill_Exception $e) {
                    $mandrill_ready = FALSE;
                }

                if ($mandrill_ready) {
                    $email = array(
                        'html' => $html_data, //Consider using a view file
                        'text' => 'You have a configuration mail',
                        'subject' => 'MYNEWCAR.IN Booking Confirmation',
                        'from_email' => 'contact@mynewcar.in',
                        'from_name' => 'MYNEWCAR.IN',
                        'headers' => array('Reply-To' => 'contact@mynewcar.in'),
                        "attachments" => array(
                            array(
                                'content' => $encodedContent,
                                'type' => "application/pdf",
                                'name' => ' Booking Confirmation ' . $invoice_no . '.pdf',
                            )
                        ),
                        'to' => array(
                            array('email' => $order_email),
                            array('email' => 'contact@mynewcar.in'),
                            array('email' => 'sbhuia@mynewcar.in'),
                            array('email' => 'contact.mynewcar@gmail.com'),
                            array('email' => 'mvasudev@mynewcar.in')
                        )
                    );
                    $result = $this->mandrill->messages_send($email);
                    $ii = $ii + 1;
                }
            }
            $this->send_sms($postData);
            ## Mail validation start here
            setcookie('mail_status', $id . '-' . $user_id . '-' . $order_id . '-sent', time() + (86400 * 30 * 30), "/");


            ## Unset Cookies Data
            setcookie('emi_id', '', time() + (86400 * 30 * 30), "/");

            $data['order_user_detail'] = $this->payment_section->order_user_detail($user_id);
            $data['content'] = 'bookingSuccess';
            $this->load->view('index', $data);
//            } 
        }
    }

    function sendSms($mobno, $msg = '') {
        $auth_key = '89443AT3NmfHZ55b85230';
        $senderId = 'MNCIND';
        $message = urlencode($msg);
        $route = 4;
        $postData = array(
            'authkey' => $auth_key,
            'mobiles' => $mobno,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url = "http://sms.rhibhuz.com/sendhttp.php";

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
                //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);
        if (curl_errno($ch)) {
            return array('error' => curl_error($ch));
        }

        curl_close($ch);

        return array('success' => $output);
    }

    function download_sucess_pdf($order_id = '') {
        $order_wise_detail = $this->payment_section->order_wise_detail($order_id);
        $id = $order_wise_detail[0]['product_id'];
        $data['id'] = $order_wise_detail[0]['product_id'];
        $data['main_id'] = $order_wise_detail[0]['product_id'];
        $data['city_name'] = $order_wise_detail[0]['city_name'];
        $order_id = $order_wise_detail[0]['order_id'];
        $user_id = $order_wise_detail[0]['user_id'];
        $on_road_price = $order_wise_detail[0]['on_road_price'];
        $dis_price = $order_wise_detail[0]['dis_price'];
        $invoice_no = $order_wise_detail[0]['invoice_no'];
        $paid_amount= $order_wise_detail[0]['paid_amount'];
        $data['order_result'] = array('paid_amount'=>$paid_amount,'city_name' => $data['city_name'], 'on_road_price' => $on_road_price, 'dis_price' => $dis_price);
        $data['car_into_details'] = $this->payment_section->car_intro_detail($data['id']);
        $data['customer_details'] = $this->payment_section->customer_details($user_id);
        $data['registeration_details'] = $this->payment_section->registeration_details($user_id);
        $data['car_configuration'] = $this->payment_section->car_configuration($user_id, $id, $cond = '2', $invoice_no);

        $data['emi'] = $this->payment_section->car_emi($id, $user_id);
        $data['booking_varaint'] = $this->payment_section->booking_detail($data['id'], $user_id, $cond = '1');
        $data['booking_exterior'] = $this->payment_section->booking_detail($data['id'], $user_id, $cond = '2');
        $data['booking_interior'] = $this->payment_section->booking_detail($data['id'], $user_id, $cond = '3');
        $data['booking_accessory'] = $this->payment_section->booking_detail($data['id'], $user_id, $cond = '4');

        $data['pro_feture'] = $this->payment_section->booking_product_features($data['id']);
        $data['pro_specification'] = $this->payment_section->pro_specification($data['id']);
        $data['onroad_price_breakup'] = $this->payment_section->get_onroad_price($data['id'], $user_id = '0');
        $data['coupon_used'] = $this->payment_section->coupon_used($order_id);
        
        print_r($order_wise_detail);
        
        require_once APPPATH . '/third_party/Mpdf/mpdf.php';
        $mpdf = new mPDF('c', 'A4', '', '', 0, 0, 10, 10, 0, 0);
        $html = $this->load->view('test', $data, TRUE);
        
        $mpdf->WriteHTML($html);
        $pdf_name = 'MyNewCar-Invoice' . date('Y-m-d') . '-' . $user_id . '-' . $data['id'] . '.pdf';
        $path = base_url() . 'config_download/';
        $uploadPath = $pdf_name;
        $mpdf->Output($uploadPath, 'D');
    }

    #### Paytm
}
