<?php

class Brand extends CI_Controller {

    public function common_city() {
        if ($_GET['city'] != '') {
            setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
            $city = $_GET['city'];
        } else {
            $city = $this->input->cookie('main_city', TRUE);
            if ($city == '') {
                $val = 1;
                setcookie('main_city', $val, time() + (86400 * 30), "/");
            }
            $city = $this->input->cookie('main_city', TRUE);
        }
        return $city;
    }

//
    public function __construct() {
        parent::__construct();
        $this->load->model('affiliate');
        $this->load->model('common_section');
        $this->load->model('home_feature');
        $this->load->model('login_authorization');
        $this->load->model('brand_section');
        $this->output->nocache();
    }
    
    
    function select_your_car($pro_name = "") {
        
        
        ####Get Common Data home page ####
        $data['all_brands'] = $this->common_section->get_brands($brand_id = '');

        ####Get City Id ####

        $date['city'] = $this->common_city();


        ####Get All City's data ####

        $data['all_Citys'] = $this->common_section->all_citys_detalis();


        #### Affiliate code here ####
        if (!empty($_GET['ref'])) {
            $ref = $_GET['ref'];
            $affilite = $this->affiliate->affilite($ref);
            $affiliate_id = $affilite['affiliate_id'];
            setcookie('ref_id', $affiliate_id, time() + (86400 * 30 * 30), "/");
            redirect(base_url());
        }

        
        $data['userdata'] = $this->session->userdata('logged_in_user');
        
        
        
        $data['brand_name'] = $pro_name;
        
        
            
        $data['car_selection'] = $this->brand_section->select_your_car_pro($pro_name);
        
        $brand_id = $data['car_selection'][0]['brand_id'];
        
        $data['car_model'] = $this->brand_section->select_your_car_model($pro_name);
        
        
        $data['buyers_bought'] = $this->brand_section->select_buyers_bought($brand_id);
        
        
        $data['overall_deals'] = $this->brand_section->select_overall_deals($brand_id);
        
        
        $data['car_variant'] = $this->brand_section->select_your_car_variant($pro_name);
        $main_city = $this->common_city();
        $data['main_city'] = $main_city;
        $data['car_variant_data'] = $this->brand_section->select_your_car_pro_details($pro_name, $cond = '1', $main_city);

        $data['selected_offers'] = $this->brand_section->select_offers_product($pro_name, $cond = '1');
        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        if ($user_id == '') {
            $user_id = '0';
        }
        $brand_id = $data['car_selection'][0]['brand_id'];
        $data['join_status'] = $this->login_authorization->join_group_status($user_id, $brand_id);
        $data['brand_user'] = $this->login_authorization->brand_wise_user($brand_id);
        $data['brand_offer'] = $this->brand_section->select_offers_brand($brand_id);
        $data['seo'] = $this->brand_section->get_meta($cond = '2', $brand_id);
//        print_r($data['brand_offer']);
//        print_r($data['brand_offer']);
//        exit;
        $data['content'] = 'selectyourcar';
        $data['seo_tags'] = $this->home_feature->home_seo_data($data['brand_name'], $id = '4');
        $this->load->view('index', $data);
    }
    
    
    
}
