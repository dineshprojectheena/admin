<!DOCTYPE html>
<html lang="en">
    <?php $this->load->view('templates/header') ?>
    <body class="full_data">
        <?php $this->load->view('templates/topbar'); ?>
        
        <div class="main-content" >
            <?php $this->load->view('content/' . $content); ?>
        </div>
        
        <?php $this->load->view('templates/footer'); ?>
        
        <?php $this->load->view('templates/js') ?>
    </body>
</html>