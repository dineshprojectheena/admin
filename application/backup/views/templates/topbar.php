<?php
$city;
?>
<a  href="#" data-toggle="modal" data-target="#feedback_panel" class="hidden-sm hidden-xs">
    <div id="flip"><div class="btn-maroon" style="padding:6px 6px 6px 6px;"> 
            <span class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x" style="color:maroon"></i>
                <i class="fa fa-phone fa-stack-1x fa-inverse"></i></span> Request a call back</div>
    </div>
</a>

<header>
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-cust-both">
                    <div class="col-md-2 col-cust-left" align="center">
                        <a href="<?php echo site_url(); ?>"><img src="<?= base_url('uploads/logo2.png') ?>" title="MyNewCar" alt="MyNewCar"></a>                    
                    </div>
                    <div class="col-md-1 col-cust-left">

                    </div>
                    <div class="col-md-9 ad-container hidden-sm hidden-xs col-cust-right">
                        <?php
                        $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        if ($url == base_url()) {
                            ?>
                            <img src="<?= base_url('assets/img/common/header-steps.png') ?>" class="img-responsive" />
                            <?php
                        } else {
                            ?>
                            <img src="<?= base_url('assets/img/common/header-steps.png') ?>" class="img-responsive"  />                            
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container menu-container navbar navbar-default">
        <div class="row set_color_menu" itemscope itemtype="http://schema.org/WebSite">
            <meta itemprop="url" content="https://www.mynewcar.in/"/>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <ul class="nav navbar-nav collapse navbar-collapse" id="main-nav">
                <?php
                $idss = 'all';
                $url_exp = explode('/', $_SERVER['REQUEST_URI']);
                $main_url = $url_exp[1];
                $exp_url = explode('?', $main_url);
                $main_url = $exp_url[0];
                if ($main_url == '') {
                    $a = 'class="active"';
                } else if ($main_url == 'car-search-by-type' || $main_url == 'comparision' || $main_url == 'car') {
                    $f = 'active';
                } else if ($main_url == 'user') {
                    $d = 'active';
                } else if ($main_url == 'lease-your-car') {
                    $l = 'class="active"';
                } else if ($main_url == 'brand') {
                    $c = 'active';
                } else if ($main_url == 'new-car-deals-discounts') {
                    $g = 'class="active"';
                } else if ($main_url == 'upcoming-new-cars-india') {
                    $upc = 'class="active"';
                } else if ($main_url == 'faq') {
                    $h = 'class="active"';
                } else {
                    $a = 'class="active"';
                }
//                    echo $l;
                ?>
                <li <?php echo $a; ?> ><a href="<?php echo site_url() ?>">Home</a></li>
                <li class="dropdown <?php echo $f; ?>"  >
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Car Search & Buy<b class="caret"></b></a>
                    <ul class="dropdown-menu loginOptions">
                        <li><a href="#" onclick="$('#min_price').val(''), $('#max_price').val(''), $('#search_submit').click();">Car Type</a></li>
                        <li><a href="#" onclick="$('#search_submit').click();">Car Price</a></li>
                        <li><a href="#" onclick="window.location.href = document.getElementById('compare_data').value">Compare Cars</a></li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#testdrive">
                                Book a Test Drive
                            </a>
                        </li>                            
                    </ul>
                </li>
                <li <?php echo $l; ?> ><a href="<?php echo base_url() . 'lease-your-car'; ?>">Car Lease</a></li>
                <li class="dropdown <?php echo $c; ?>" >
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Brands<b class="caret"></b></a>
                    <ul class="dropdown-menu loginOptions">
                        <div class="brand_menu_dv" class="col-md-12">
                            <div class='brand_left' >
                                <div class="row">
                                    <?php
                                    if (!empty($all_brands)) {
                                        for ($i = 1, $j = 0; $i <= count($all_brands); $i++) {
//                                                if (!empty($all_brands[$j]['brand_image'])) {
                                            ?>
                                            <div class="col-md-2 col-xs-4 hidden-sm hidden-xs" >                                                            
                                                <div class="topbar_brand_div" align="center">
                                                    <a href="<?php echo base_url() . 'brand/' . $all_brands[$j]['brand_name']; ?>">
                                                        <img class="img-center " 
                                                             src="<?php echo img_url('uploads/brand/' . $all_brands[$j]['brand_image']); ?>" />
                                                    </a>

                                                    <a href="<?php echo base_url() . 'brand/' . $all_brands[$j]['brand_name']; ?>"  class="topbar_title">
                                                        <?php echo $all_brands[$j]['brand_name']; ?>
                                                    </a>                                                                    
                                                </div>                                                            
                                            </div>  
                                            <div class="col-md-2 visible-sm visible-xs">
                                                <ul class="dropdown-menu loginOptions">    
                                                    <li><a href="<?php echo base_url() . 'brand/' . $all_brands[$j]['brand_name']; ?>"><?php echo $all_brands[$j]['brand_name']; ?> </a></li>   
                                                </ul>    
                                            </div>  
                                            <?php
//                                                }
                                            ?>
                                            <?php
                                            $j++;
                                        }
                                    }
                                    ?>
                                </div>                          
                            </div>    
                        </div>
                    </ul>
                </li>
                <li <?php echo $g; ?> ><a href="<?php echo base_url() . 'new-car-deals-discounts/'; ?>">Hot Deals</a></li>
                <li <?php echo $upc; ?> ><a href="<?php echo base_url() . 'upcoming-new-cars-india/'; ?>">Upcoming Cars</a></li>
                <?php if (!empty($this->session->userdata('logged_in_user'))): ?>
                    <li class="dropdown <?php echo $d; ?>"  >
                        <a href="<?= site_url('user') ?>" class="dropdown-toggle">Profile<b class="caret"></b></a>
                        <ul class="dropdown-menu loginOptions">
                            <li ><a href="<?= site_url('authorization/logout'); ?>">Sign Out</a></li>                            
                        </ul>
                    </li>
                <?php else: ?>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#loginModal" data-toggle="dropdown" >Sign in
                        </a>
                    </li>
                <?php endif; ?> 
            </ul>
            <?php
            if ($this->input->cookie('main_city', TRUE) != '' || $_GET['city'] == '') {
                $city = $this->input->cookie('main_city', TRUE);
            } else if ($this->input->cookie('main_city', TRUE) == '' || $_GET['city'] != '') {
                $city = $_GET['city'];
            } else {
                $city = 1;
            }
            ?>

<!--            <form class=" navbar-form navbar-right topbar-search" role="search" itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">
                <meta itemprop="target" content=" https://www.mynewcar.in/search?q={query}"/>
                <div class="col-md-12 col-sx-12 col-sm-12">
                    <div class="col-md-10 col-sx-10 col-sm-12 select_box"> 
                        <select class="selectpicker pull-left"  id="main_citys"  onchange="setcitycookie('1');" >
                            <option value="">Select City</option>
                            <?php
                            foreach ($all_Citys as $all_Citys_res) {
                                ?>
                                <option <?php
                                if ($all_Citys_res['city_id'] == $city) {
                                    echo $sel = 'selected';
                                }
                                unset($sel);
                                ?> 
                                    value="<?php echo $all_Citys_res['city_id']; ?>">
                                        <?php echo $all_Citys_res['name']; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                        </div>
                    <div class="col-md-2 col-sx-12 col-sm-12"> 
                        <ul style="list-style-type: none;" class="pull-right">
                            <li class="dropdown">
                                <button type="button" class="dropdown-toggle btn btn-maroon  pull-right search_btn"><i class="fa fa-search"></i></button>    
                                <ul class="dropdown-menu" style="margin-top:30px;padding:10px 20px;position:absolute;">
                                    <li>
                                        <input type="text" itemprop="query-input" class="search form-control col-md-12 col-sx-12 col-sm-12" placeholder="Search" onkeydown="search_product_data();"  id="inputSearch" /><br /> 
                                        <div id="divResult">
                                        </div>    

                                    </li>                                
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>

            <div class="col-md-5 col-sx-12 col-sm-12 hidden-lg hidden-md hidden-sm hidden-xs">   
                <input type="text" class="search form-control col-md-12 col-sx-12 col-sm-12" placeholder="Search" onkeydown="search_product_data();"  id="inputSearch" /><br /> 
                <div id="divResult">
                </div>
            </div>-->


<form class=" navbar-form navbar-right topbar-search" role="search">
                <div class="col-md-12 col-sx-12 col-sm-12">
                    <div class="col-md-5 col-sx-12 col-sm-12 select_box"> 
                        <?php
//                        echo '<pre>';
//                        echo $city; 
//                        echo '</pre>';
                        ?>
                        
                        
                        <select class="selectpicker"  id="main_citys"  onchange="setcitycookie('1');">
                            <option value="">Select City</option>
                            <?php
                            foreach ($all_Citys as $all_Citys_res) {
                                ?>
                                <option <?php
                                if($all_Citys_res['city_id']==$city)
                                {
                                echo $sel='selected';    
                                }
                                unset($sel);
                                ?> 
                                value="<?php echo $all_Citys_res['city_id']; ?>">
                                <?php echo $all_Citys_res['name']; ?>
                                </option>
                                <?php
                                }  
                                ?>
                        </select>
                    </div>                    
                    <div class="col-md-5 col-sx-12 col-sm-12">   
                        <!--<div class="contentArea">-->
                        <input type="text" class="search form-control col-md-12 col-sx-12 col-sm-12" placeholder="Search" onkeydown="search_product_data();"  id="inputSearch" /><br /> 
                        <div id="divResult">
                        </div>
                        <!--</div>-->                            
                    </div>
                    <div class="col-md-2">    
                        <button type="button" class="btn btn-maroon hidden-sm hidden-xs pull-right search_btn"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>

            <div class="col-md-5 col-sx-12 col-sm-12 hidden-lg hidden-md hidden-sm hidden-xs">   
                <input type="text" class="search form-control col-md-12 col-sx-12 col-sm-12" placeholder="Search" onkeydown="search_product_data();"  id="inputSearch" /><br /> 
                <div id="divResult">
                </div>
            </div>

        </div>
    </div>
    <form action="<?php echo site_url('car-search-by-type'); ?>" method="POST" style="display:none;">
        <input type="text" value="<?php echo $max_price_res['min_price']; ?>" name="min_price" id="min_price" class="p1"/>
        <input type="text" value="<?php echo $max_price_res['max_price']; ?>" name="max_price" id="max_price" class="p2"/>
        <input type="text" value="" name="brand_name" id="brand_name" class="p3"/>
        <input type="text" name="model_name" id="model_name" class="p4"/>
        <input type="text" name="car_type" id="car_type" class="p5"/>
        <button class="btn btn-grey col-md-12" type="submit" id="search_submit" name="search_submit">Search</button>
    </form> 
</header>


<div class="modal fade" id="loginModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="col-md-5">
                        <h4>Login Into Your Account</h4>
                        <?php
                        $req_uri = base_url() . '' . $_SERVER['REQUEST_URI'];
                        $peramter_val = $actual_link = "$_SERVER[REQUEST_URI]";
                        $exp = explode('/', $peramter_val);
                        $cnt = count($exp);
                        $permater = $exp[$cnt - 1];
                        $page = $exp[$cnt - 2];
                        ?>
                        <hr class="examplefour"/>
                        <div class="col-md-12">
                            <form class="form-horizontal" autocomplete="off" action="<?php echo base_url() . 'authorization/login' ?>" role="form" id="login_page" method="POST">

                                <div class="form-group">
                                    <?php $req_uri = 'http://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI']; ?>
                                    Email or Username<br>
                                    <input type="text" class="form-control" id="signin_email" name="signin_email" onblur="checkuser('4');" placeholder="info@mynewcar.in" value="">
                                    <input type="hidden" class="form-control" id="url" name="url" placeholder="username" value="<?php echo $req_uri; ?>">
                                    <span class="error"><p id="signin_user"> </p></span>   

                                </div>
                                <div class="form-group">
                                    Password<br>
                                    <input type="password"  class="form-control" id="signin_password" name="signin_password" placeholder="Password" value="">
                                    <span class="error"><p id="signin_passs"> </p></span>   

                                </div>
                                <div class="form-group">
                                    <button type="button" onclick="return showforget('1');" class="btn btn-maroon col-md-12">Sign In</button>
                                    <button type="submit" style="display:none;" id="signin" class="btn btn-maroon col-md-12">Sign In</button>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 col-cust-both">
                                        <div class="checkbox pull-left">
                                            <label>
                                                <input type="checkbox">Remember me
                                            </label>
                                        </div>
                                        <div class="checkbox pull-right">
                                            <label>
                                                <a onclick="return showforget('2');">Forgot Password</a>
                                            </label>
                                        </div>

                                    </div>
                                </div>

                            </form>
                        </div> 

                        <form class="form-horizontal" autocomplete="off" action="<?php echo base_url() . 'authorization/forget' ?>" role="form" id="forget_page" style="display:none;" method="POST">
                            <h4>Forget Password</h4>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="email" class="form-control" id="forget_email" name="forget_email" placeholder="Email">
                                    <span class="error"><p id="for_email"> </p></span>  
                                </div>
                            </div>                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" onclick="return showforget('3');" class="btn btn-default">Forgot Password</button>
                                    <button type="submit" style="display:none;" id="for_pass" class="btn btn-maroon">Forget Password</button>                                    
                                </div>
                            </div>
                        </form>
                    </div>   
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <h4><a href="#" onclick="return custom('1');">Sign up for a MYNEWCAR.IN account!</a></h4>
                        <hr class="examplefour"/>
                        <form role="form" autocomplete="off" action="<?php echo base_url('authorization/register_user'); ?>" id="custom_login" method="POST" style="display:none;">
                            <div class="form-group">
                                Username</br>
                                <input type="text" class="form-control" id="regis_username" value="" name="regis_username" onblur="checkuser('1');"  placeholder="Enter username">
                                <span class="error"><p id="inv_name"> </p></span>                                
                            </div>
                            <div class="form-group">
                                Email address</br>
                                <input type="email" class="form-control" id="regis_email" value="" name="regis_email" onblur="checkuser('2');" placeholder="Enter email">
                                <span class="error"><p id="inv_email"> </p></span>
                            </div>
                            <div class="form-group">
                                Password</br>
                                <input type="password" class="form-control" id="regis_password" value="" name="regis_password" placeholder="Password">
                                <span class="error"><p id="inv_pass"> </p></span>
                            </div>
                            <button style="display:none;" type="submit" class="btn btn-maroon" id="reg__sub">Submit</button>
                            <button type="button" class="btn btn-maroon" onclick="checkuser('3');">Submit</button>
                            <button type="button" class="btn btn-grey" onclick="custom('2');">Back</button>
                        </form>

                        <div align="left" id="social_media_login">
                            <div class="col-md-12">
                                <a class="pull-left" href="<?php echo site_url('authorization/onlyFbLogin?req_uri=' . $req_uri); ?>" >
                                    <img src="<?php echo base_url() . '/assets/img/fb.png' ?>" alt="sing in with facebook" class="img-responsive"/>
                                </a>
                            </div>
                            <div class="col-md-12">
                                <a class="pull-left"href="<?php echo site_url('authorization/linkdin_url?req_uri=' . $req_uri); ?>" >
                                    <img src="<?php echo base_url() . '/assets/img/lkd.png' ?>" alt="sing in with linkedin" class="img-responsive"/>
                                </a>
                            </div>
                            <div class="col-md-12">
                                <a class="pull-left" href="<?php echo site_url('authorization/googleAuth?req_uri=' . $req_uri); ?>"  alt="sing in with google">
                                    <img src="<?php echo base_url() . '/assets/img/gm.png' ?>"  class="img-responsive" />
                                </a>
                            </div>    

                        </div>    
                    </div>
                </div>   
            </div>  

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="feedback_panel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-phone fa-stack-1x fa-inverse"></i></span>		
                    Request a call back
                </h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form action="<?php echo base_url(); ?>common/feedback_enquiry" method="POST">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-3 req_pad">
                                        <p>Full Name</p>
                                    </div>
                                    <div class="col-md-6 select-brand-container" id="car_varinant_loading">
                                        <input type="text" id="enq_full_name" name="enq_full_name" class="form-control" placeholder="" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 req_pad">
                                        <p>Email</p>
                                    </div>
                                    <div class="col-md-6 select-brand-container" id="car_varinant_loading">
                                        <input type="text" id="enq_email" name="enq_email" class="form-control" onblur="validateEmail(this);"  placeholder="" required>
                                        <div class="email_error"></div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3 req_pad">
                                        <p>Phone</p>
                                    </div>
                                    <div class="col-md-6 select-brand-container" id="car_varinant_loading">
                                        <input type="text" id="enq_phone" name="enq_phone" onblur="validateno(this);" class="form-control"  placeholder=""  required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 req_pad">
                                        <p>City</p>
                                    </div>
                                    <div class="col-md-6 select-brand-container" id="car_varinant_loading">
                                        <select class="form-control"  id="request_main_city" name="request_main_city" onchange="check_others_city();">
                                            <option value="">Select City</option>
                                            <?php
                                            foreach ($all_Citys as $all_Citys_res) {
                                                ?>
                                                <option value="<?php echo $all_Citys_res['name']; ?>"><?php echo $all_Citys_res['name']; ?></option>
                                                <?php
                                            }
                                            ?>
                                            <option value="Others" >Others...</option>
                                        </select>
                                        <input type="text" id="request_main_city2" name="request_main_city2" style="display:none;margin-top:5px;margin-bottom:5px;" class="form-control"  placeholder="other city...">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 req_pad">
                                        <p>Interested Car</p>
                                    </div>
                                    <div class="col-md-6 select-brand-container" id="car_varinant_loading">
                                        <input type="text" id="enq_interested_car" name="enq_interested_car" class="form-control"  placeholder=""  required/>

                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-maroon btn-sm">Submit</button>
                            </div>
                        </form>  
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Social Login Modal -->

<div class="modal fade" id="cityModalload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select City</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8 column-centered">
                            <div class="form-group">
                                <select class="selectpicker" data-live-search="true" data-size="5" id="main_city2" onchange="setcitycookie('2');">
                                    <optgroup label="Top Cities">
                                        <option value="">Select City</option>
                                        <?php
                                        foreach ($all_Citys as $all_Citys_res) {
                                            ?>
                                            <option <?php
                                            if ($main_city == $all_Citys_res['city_id']) {
                                                echo 'Selected';
                                            }
                                            ?> value="<?php echo $all_Citys_res['city_id']; ?>"><?php echo $all_Citys_res['name']; ?></option>
                                                <?php
                                            }
                                            ?>
                                    </optgroup>

                                </select>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



