<a style="display:none;" data-target="#login_errors" data-toggle="modal" id="login_errors_button" href="#">Error_login</a>
<div class="modal fade" id="login_errors">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="mess_heading">Login Errors!</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row" id="error_message">
                        Facebook Errors  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expert_review_selection">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="mess_heading">Expert Review Search</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4" id="exp_brand_res">

                                </div>
                                <div class="col-md-4" id="exp_model_res">

                                </div>
                                <div class="col-md-4" id="exp_var_res">

                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Enquiry Popup start here -->
<div aria-hidden="true" aria-labelledby="price-modal-label" role="dialog" tabindex="-1" id="coming_enqury" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<!--                <h4 class="modal-title left">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-phone fa-stack-1x fa-inverse"></i></span>		
                    Coming Soon enquire here
                </h4>-->
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo base_url(); ?>coming_soon/coming_enquiry" method="POST">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-4 select-brand-container" id="car_varinant_loading">
                                            <input type="text"  id="enq_full_name" name="enq_full_name" class="form-control" placeholder="Full Name" required="">
                                        </div>

                                        <div class="col-md-4 select-brand-container" id="car_varinant_loading">
                                            <input type="text" id="enq_email" name="enq_email" class="form-control" onblur="validateEmail(this);" placeholder="Email" required="">
                                            <div class="email_error"></div>
                                        </div>

                                        <div class="col-md-4 select-brand-container" id="car_varinant_loading">
                                            <input type="text" id="enq_phone" name="enq_phone" onblur="validateno(this);" class="form-control" placeholder="Phone Number" required="">
                                            <input type="hidden" id="enq_com_product" name="enq_com_product"  class="form-control">
                                        </div>
                                        
                                    </div>
<!--                                    <div class="row">
                                        <div class="col-md-12 select-brand-container" id="car_varinant_loading">
                                            <br>
                                            <input type="checkbox" id="yes_upcoming_car" name="yes_upcoming_car" value="1">
                                            <span class="alert_me_text"><label for="yes_upcoming_car">I want to hear about this product?</label></span>
                                        </div>
                                    </div>-->
                                    
                                </div>

                                <div class="modal-footer">
                                    <!--<button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>-->
                                    <button type="submit" class="btn btn-xs search-by-price-button carsuel_book" >Submit</button>
                                </div>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Enquiry Popup End here -->