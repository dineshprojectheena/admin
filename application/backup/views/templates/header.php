<!DOCTYPE html>
<?php
if (empty($this->input->cookie('main_city', TRUE))) {
    ?>
    <script>
    //alert('<?php echo $this->input->cookie('main_city', TRUE); ?>');    
        location.reload();
    </script>
    <?php
    exit;
}
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php
if (!empty($seo_tags)) {
    ?>
        <meta name="description" content="<?php echo $seo_tags['meta_description']; ?>" />
        <meta name="keyword" content="<?php echo $seo_tags['keywords']; ?>">
        <title><?php echo $seo_tags['title']; ?></title>
    <?php
} else {
    ?>
        <meta name="description" content="<?php echo $seo[0]['seo_description'] ?>" />
        <meta name="keywords" content="<?php echo $seo[0]['seo_keyword'] ?>" />
        <title><?php echo $seo[0]['seo_title'] ?></title>
    <?php
}
?>
    <meta name="google-site-verification" content="u69O89b-xVG7hsem3vm97ydnsdtYq5rHs7ktZyuKTYs" />

    <meta name="msvalidate.01" content="D631B093C34CC6274DC7E17BEA507A08" />
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/home/32X32.png" />    
    <link href='fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url("assets/css/font-awesome.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/font-awesome.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/thirdparty/rs-plugin/css/settings.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/thirdparty/flexSlider/flexslider.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/thirdparty/dataTable/css/jquery.dataTables.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/thirdparty/dataTable/css/jquery.dataTables_themeroller.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/thirdparty/prettyphoto/css/prettyPhoto.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url("assets/thirdparty/nouislider/nouislider.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/thirdparty/btselect/bootstrap-select.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/thirdparty/jQuery.toolbar/jquery.toolbars.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/presentation.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquery-ui.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/flexslider.css'); ?>" rel="stylesheet">    <!-- Custom Favicon -->
    <link href="<?php echo base_url('assets/css/extra.css'); ?>" rel="stylesheet">    <!-- Custom Favicon -->
    <script src="<?php echo base_url() . "assets/js/extra.js" ?>"></script>    
    <script src="<?php echo base_url() . "assets/js/jquery.js" ?>"></script>       
<?php $this->load->view('external_js/header_popup_js'); ?>
</head>
    <?php $this->load->view('templates/popup'); ?>