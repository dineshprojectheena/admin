<script src="<?php echo base_url("assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/rs-plugin/js/jquery.themepunch.revolution.min.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/vticker/vticker.min.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/flexSlider/jquery.flexslider.js") ?>"></script>

<script src="<?php echo base_url("assets/thirdparty/btstarrating/js/star-rating.min.js") ?>"></script>
<link href="<?php echo base_url("assets/thirdparty/btstarrating/css/star-rating.min.css") ?>" rel="stylesheet">

<script src="<?php echo base_url("assets/thirdparty/fancybox/jquery.fancybox.pack.js") ?>"></script>
<link href="<?php echo base_url("assets/thirdparty/fancybox/jquery.fancybox.css") ?>" rel="stylesheet">

<script src="<?php echo base_url("assets/thirdparty/fixedheader/jquery.fixedheadertable.min.js") ?>"></script>
<link href="<?php echo base_url("assets/thirdparty/fixedheader/css/defaultTheme.css") ?>" rel="stylesheet">
<script src="<?php echo base_url("assets/thirdparty/dataTable/media/js/jquery.dataTables.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/prettyphoto/js/jquery.prettyPhoto.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/jQuery.validate/jquery.validate.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/jquery.floatThead/jquery.floatThead.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/jQuery.toolbar/jquery.toolbar.min.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/masonry/masonry.pkgd.min.js") ?>"></script>
<script src="<?php echo base_url("assets/thirdparty/btselect/bootstrap-select.min.js") ?>"></script>
<?php
$main_city = $this->input->cookie('main_city', TRUE);
if (empty($max_price_res)) {
    $max_price_res = $this->home_feature->max_price($main_city);
    $max_price_main = $max_price_res[0]['max_price'];
    $min_price_main = $max_price_res[0]['min_price'];
}
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/counter/jquery.countdown.js"></script>
<script src="<?php echo base_url("assets/thirdparty/nouislider/jquery.nouislider.js") ?>"></script>
<script src="<?php echo base_url("assets/js/app.js") ?>"></script>
<?php $this->load->view('external_js/js_common'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>slider/js/jssor.slider.mini.js"></script>
<!-- MYNEWCAR Facebook Custom Audience Remarketing Pixels -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '102615470090878');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=102615470090878&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- MYNEWCAR Facebook Custom Audience Remarketing Pixels (LP) -->



