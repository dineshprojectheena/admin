<br>
<footer id="ftr">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-cust-both">
                <div class="col-md-3 col-sm-6 footer-item col-cust-both">
                    <h3>About Us</h3>
                    <p>MYNEWCAR.IN is a one stop shop for buying new cars and related services, easily and securely with best deals!</p>
                    <div class="social">
                        <a href="<?php echo FB_link; ?>"  target="_blank" class="fb tiphere" data-toggle="tooltip" data-placement="top" title="Facebook">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="<?php echo TW_link; ?>"  target="_blank" class="tw tiphere" data-toggle="tooltip" data-placement="top" title="Twitter">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="<?php echo GM_link; ?>"  target="_blank" class="gp tiphere" data-toggle="tooltip" data-placement="top" title="Google Plus">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="<?php echo LD_link; ?>"  target="_blank" class="ln tiphere" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 footer-item col-cust-both">
                    <div class="company-box">
                        <h3></h3>
                        <ul class="services">
                            <li>
                                <a style="" href="<?= site_url('about-us') ?>">About Us</a>
                            </li>
                            <li>
                                <a  href="<?php echo base_url() . 'car-enquiry'; ?>">Contact Us</a>
                            </li>
                            <li>
                                <a href="<?php echo My_car_link . 'blog/'; ?>">Media</a>
                            </li>
                            <li ><a  href="<?php echo site_url('faq') ?>">FAQs</a></li> 

                        </ul>
                    </div>

                </div>
                <div class="col-md-3 col-sm-6 footer-item col-cust-both">
                    <div class="company-box">
                        <h3></h3>
                        <ul class="information">
                            <li>
                                <a href="<?php echo base_url() . 'privacy-policy'; ?>">Privacy Policy</a>
                            </li>


                            <li>
                                <a href="<?php echo base_url() . 'terms-of-offer-for-sale'; ?>">Terms of Offer for Sale</a>
                            </li>

                            <li>
                                <a href="<?php echo base_url() . 'user-terms'; ?>">User Terms</a>
                            </li>

                            <li>
                                <a href="<?php echo base_url() . 'contact-us'; ?>">Dealers</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-3 footer-item col-cust-both">
                    <div class="company-box">
                        <h3>Newsletter</h3>
                        <p>
                            Sign Up to receive deals in your Inbox!
                        </p>
                        <div>
                            <form method="post" action="<?php echo site_url('common/subscribe') ?>" id="newsletter_form">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" placeholder="Enter email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success" onclick="validateNewsletterEmail();">Subscribe</button>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="footer-wrapper" align="center">
        <div class="footer clearfix"  >

            <div id="powered" ><p>Powered by Coense Solutions</p>
            </div>

            <div id="paymenticons">
                <img align="absmiddle" class="img-responsive" src="<?php echo base_url() ?>assets/img/footer_payment_type.png">
            </div>
        </div>
    </div>    
</footer>
<div id="fb-root"></div>
<div class="modal fade" id="testdrive" tabindex="-1" role="dialog" aria-labelledby="change-car-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="change-car-modal-label"><i class="fa fa-dashboard"></i> Book a Test Drive</h4>
            </div>
            <form action="<?php echo base_url('common/test_drive_enquiry'); ?>" method="POST">
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-6 req_pad2">
                                <p>Select Car Brand</p>
                            </div>
                            <input type="hidden" id="selected_car" name="selected_car" />
                            <div class="col-md-6 text-right select-brand-container">
                                <select class="form-control brand-selector" id="test_brand_data" name="test_brand_data" style="color:#00000 !important;" onchange="testdrive_setmodel();">
                                    <option value="" >Select Brand</option>
                                    <?php
                                    if (!empty($all_brands)) {
                                        foreach ($all_brands as $all_brands_res) {
                                            ?>    
                                            <option value="<?php echo $all_brands_res['brand_id']; ?>"  class='brand-img'> 
                                                <?php echo $all_brands_res['brand_name']; ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>                                
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 req_pad2">
                                <p>Select Car Model</p>
                            </div>
                            <div class="col-md-6 select-brand-container" id="car_model">
                                <select id="test_brand_model" name="test_brand_model" class="form-control">    
                                    <option value="">Select Model</option>   
                                </select>    
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-md-6 req_pad2">
                                <p>Select Car Variant</p>
                            </div>
                            <div class="col-md-6 select-brand-container">
                                <select class="form-control" id="test_car_variant" name="test_car_variant">
                                    <option value="">Select Variant</option>

                                </select>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-6 req_pad2">
                                <p>Fuel Type</p>
                            </div>
                            <div class="col-md-6 select-brand-container">
                                <select class="form-control" id="test_fuel_type" name="test_fuel_type">
                                    <option value="">Select Fuel Type</option>    
                                    <?php
                                    foreach ($fuel_data as $fuel_data_res) {
                                        ?>
                                        <option value="<?php echo $fuel_data_res['fuel_type'] ?>"><?php echo $fuel_data_res['fuel_type'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 req_pad2">
                                <p>Test Drive Date</p>
                            </div>
                            <div class="col-md-6 select-brand-container">
                                <input type="text" id="test_date"  name="test_date" class="form-control" onchange="validatedate();"  placeholder=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 req_pad2">
                                <p>Full Name</p>
                            </div>
                            <div class="col-md-6 select-brand-container">
                                <input type="text" id="test_full_name" name="test_full_name" class="form-control" placeholder=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 req_pad2">
                                <p>Email</p>
                            </div>
                            <div class="col-md-6 select-brand-container">
                                <input type="text" id="test_email" name="test_email"  onblur="validateEmail(this);" class="form-control"  placeholder=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 req_pad2">
                                <p>Phone</p>
                            </div>
                            <div class="col-md-6 select-brand-container">
                                <input type="text" id="test_phone" name="test_phone" onblur="validateno(this);" class="form-control"  placeholder=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 req_pad2">
                                <p>Location</p>
                            </div>
                            <div class="col-md-6 select-brand-container">
                                <input type="text" id="test_location" name="test_location" class="form-control"  placeholder=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="modal-footer">                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" align="right">
                                <button type="submit" class="btn btn-xs carsuel_book" onclick="return check_before_change2();">Submit</button>
                            </div>
                        </div>

                    </div>
                </div>


            </form>             
        </div>
    </div>
</div>




<div class="modal fade" id="emi_caulculator">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> Calculate Your EMI</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label" >Ex-Showroom Price</label>
                            </div>
                            <div class="col-md-3 seat-capacity-selection-box align-left" >    
                                <input type="text" readonly="" id="mnc_ex_price"  name="mnc_ex_price" value="<?php print_r(indianFormatNumber($final_result['exshowroom_price'][0]['mnc_exshowroom_price'])); //echo $final_result['exshowroom_price'][0]['exshowroom_price'];                   ?>" class="form-control emi_ex_price">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">Finance Required</label>
                            </div>
                            <div class="col-md-3 slider-div seat-capacity-selection-box"  onchange="select_fianance()">    
                                <div class="seat-capacity-selection"  data-min-value="0" data-max-value="100" data-initial-min="0" data-initial-max="100" data-step="1"></div>
                                <div class="pull-left">Min: <span class="min-seater">0</span>%</div>
                                <div class="pull-right" id="max_finn">Max: <span class="max-seater">100</span>%</div>                        
                            </div>                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label" for="ex-shpw-price">Down Payment</label>
                            </div>
                            <div class="col-md-3 seat-capacity-selection-box align-left"  >    
                                <input type="text" readonly="false" class="form-control" id="down_payment" name="down_payment">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label" for="ex-shpw-price">Loan Amount</label>
                            </div>
                            <div class="col-md-3 seat-capacity-selection-box align-left"  >    
                                <input id=loan1 onkeypress="emi();" class="form-control" readonly="true">
                            </div>
                        </div>
                    </div>

                    <?php
                    //$bank_per = $this->home_section->bank_per();
                    ?>
                    <div class="row req_pad2">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label" for="Bank">Bank</label>
                            </div>
                            <div class="col-md-3 seat-capacity-selection-box align-left"  >    
                                <select name="bank" id="bank" class="selectpicker" style="display: none;" onchange="setbankper();">
                                    <option value="">Select bank</option>                                
                                    <?php
                                    foreach ($bank_per as $bank_per_data) {
                                        ?>
                                        <option value="<?php echo $bank_per_data['bank_per']; ?>"><?php echo $bank_per_data['bank_name']; ?></option>
                                        <?php
                                    }
                                    ?>                                
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label" for="Bank">Interest Rate</label>
                            </div>
                            <div class="col-md-3 seat-capacity-selection-box align-left"  >    
                                <input id=rate1 onchange="checkemi();"  class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row req_pad2">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="control-label">Tenure (Months)</label>
                            </div>
                            <div class="col-md-3 seat-capacity-selection-box align-left"  >    
                                <select name="months1" id="months1" class="selectpicker" style="display: none;" onchange="emi();">                                
                                    <option value="">Select Bank Tenure</option>      
                                    <option>12</option>      
                                    <option>24</option>      
                                    <option>36</option>      
                                    <option>48</option>      
                                    <option>60</option>      
                                    <option>72</option>      
                                    <option>84</option>                                      
                                </select> 
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">EMI Amount</label>
                            </div>
                            <div class="col-md-3 seat-capacity-selection-box align-left"  >    
                                <input id=pay1 class="form-control">
                                <input type="hidden" id=tintt1>
                                <input type="hidden" id=gt1>
                            </div>
                        </div>
                    </div>
                    <div class="row req_pad2">
                        <div class="col-md-12">
                            <div class="col-md-9"></div>
                            <div class="col-md-3 seat-capacity-selection-box align-left"  >    
                                <button class="btn btn-maroon" type="button" onclick="setemi();" style="float:right;">Save EMI</button>
                            </div>
                        </div>
                    </div>
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div><!-- /.modal -->
</div><!-- /.modal -->

<?php $this->load->view('external_js/common'); ?>





