<?php
$current_segment = $this->uri->segment(1);
$data['session'] = $this->session->userdata("logged_in_user");
$user_id=$data['session']['user_id'];
?>
<div class="container" style="margin-bottom:10px;" id="config_header_sec">
    <div class="row">
        <div class="col-md-12 step-div">
            <ul class="hidden-sm hidden-xs">
		<!--               
		 <?php
                if (!empty($config_breadcrub[0]['introduction'])) {
                    $status = '<i class="fa fa-check-circle"></i>';
                    $class = 'class="comleted-step"';
                } else if ($content == 'carIntroduction') {
                    $status = '<i class="fa fa-thumb-tack"></i>';
                }
                ?>
                <li <?php echo $class; ?> >
                    <a href="<?= site_url('car-introduction/' . $main_id) ?>" style="width:300px;">
                        <?php if (empty($status)) { ?>
                            <span class="glyphicon glyphicon-hand-right"></span>
                            <?php
                        } else {
                            echo $status;
                        }
                        ?>
                        Overview  
                    </a>
                </li>
		-->
                <?php
                if(!empty($config_breadcrub[0]['variant_type'])) {
                    $status1 = '<i class="fa fa-check-circle"></i>';
                    $class1 = 'class="comleted-step"';
                } else if ($content == 'configureCarVariant') {
                    $status1 = '<i class="fa fa-thumb-tack"></i>';
                }
                ?>
                <li <?php echo $class1; ?> > 
                    <a href="<?= site_url('configure-variant/' . $main_id) ?>">
                        <?php if (empty($status1)) { ?>
                            <span class="glyphicon glyphicon-hand-right"></span>
                            <?php
                        } else {
                            echo $status1;
                        }
                        ?>
                        Variant  
                    </a>
                </li>


                <?php
                if (!empty($config_breadcrub[0]['color']) || !empty($config_breadcrub[0]['wheel'])) {
                    $status2 = '<i class="fa fa-check-circle"></i>';
                    $class2 = 'class="comleted-step"';
                } else if ($content == 'carExteriorSelection') {
                    $status2 = '<i class="fa fa-thumb-tack"></i>';
                }
                ?>
                <li <?php echo $class2; ?> > 
                    <a href="<?= site_url('exterior-selection/' . $main_id) ?>">
                        <?php if (empty($status2)) { ?>
                            <span class="glyphicon glyphicon-hand-right"></span>
                            <?php
                        } else {
                            echo $status2;
                        }
                        ?>                   
                        Exterior  
                    </a> 
                </li>
				<!--	
                <?php
                if (!empty($config_breadcrub[0]['leather']) || !empty($config_breadcrub[0]['cloth']) || !empty($config_breadcrub[0]['dashboard'])) {
                    $status3 = '<i class="fa fa-check-circle"></i>';
                    $class3 = 'class="comleted-step"';
                } else if ($content == 'carInteriorSelection') {
                    $status3 = '<i class="fa fa-thumb-tack"></i>';
                }
                ?>
                <li <?php echo $class3; ?> > 
                    <a href="<?= site_url('interior-selection/' . $main_id) ?>">
                        <?php if (empty($status3)) { ?>
                            <span class="glyphicon glyphicon-hand-right"></span>
                            <?php
                        } else {
                            echo $status3;
                        }
                        ?>
                        Interior  
                    </a>
                </li>
                <?php
                if (!empty($config_breadcrub[0]['accessory'])) {
                    $status4 = '<i class="fa fa-check-circle"></i>';
                    $class4 = 'class="comleted-step"';
                } else if ($content == 'configureCarAccessory') {
                    $status4 = '<i class="fa fa-thumb-tack"></i>';
                }
                ?>
                <li <?php echo $class4; ?> > 
                    <a href="<?= site_url('configure-accessory/' . $main_id) ?>">
                        <?php if (empty($status4)) { ?>
                            <span class="glyphicon glyphicon-hand-right"></span>
                            <?php
                        } else {
                            echo $status4;
                        }
                        ?>
                        Accessories  
                    </a>
                </li>
				-->
                <?php
                if (!empty($config_breadcrub[0]['amc_id']) || !empty($config_breadcrub[0]['insurance_id']) || !empty($config_breadcrub[0]['extended_id']) || !empty($config_breadcrub[0]['finance_id'])) {
                    $status5 = '<i class="fa fa-check-circle"></i>';
                    $class5 = 'class="comleted-step"';
                } else if ($content == 'accessory_services') {
                    $status5 = '<i class="fa fa-thumb-tack"></i>';
                }
                ?>
                <li <?php echo $class5; ?> > 
                    <a href="<?= site_url('accessory-services/' . $main_id) ?>">
                        <?php if (empty($status5)) { ?>
                            <span class="glyphicon glyphicon-hand-right"></span>
                            <?php
                        } else {
                            echo $status5;
                        }
                        ?>
                        Services   
                    </a>
                </li>

                <li >
                    <a href="<?= site_url('car-booking/' . $main_id) ?>" style="margin-left:-20px;" >
                        Book My New Car                        
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <style>
        .slider-sidebar table td{
            border: none !important;
            font-size: 14px;
        }
    </style>
    <div class="row">
        <div class="col-md-9 col-sm-6 col-xs-12" id="left_sidebar">
            <?php
            $car_into_details[0]['pro_image'];
            $image_data = explode('/', $car_into_details[0]['pro_image']);
            $cnt = count($image_data);
            $img_name = $image_data[$cnt - 1];
            $model_name = $image_data[$cnt - 2];
            $brand_name = $image_data[$cnt - 3];
            if($content == 'carInteriorSelection'){
                $image = base_url() . 'admin/uploads/product/' . $brand_name . '/' . $model_name . '/int.jpg';
            } else {
                $image = base_url() . 'admin/uploads/product/' . $brand_name . '/' . $model_name . '/ext.jpg';
            }
            ?>
            <img class="img-responsive img-thumbnail img-center" id="conf_main_image" src="<?php echo $image; ?>" style="max-height:285px;width:100%;" />
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="sidebar slider-sidebar" >
                <h5 class="sidebar-title" style="font-size: 14px;height: 69px;">
                    <span><?php echo $car_into_details[0]['brand_name'] . ' ' . $car_into_details[0]['model_name'] . ' ' . $car_into_details[0]['pro_name']; ?></span>
                </h5>
                <div class="price-details-sidebar sidebar-content" style="display:none;">
                    <table class="table" cellpadding="0" cellspacing="0">
                        <tr>
                            <?php
                            if (!empty($final_result['exshowroom_price'][0]['exshowroom_price'])) {
                                ?>
                                <td width="65%" style="padding: 2px;">
                                    Ex-Showroom Price
                                </td>
                                <td width="35%" style="padding: 2px;" >
                                    <span class="strike-through"><i class="fa fa-rupee"></i> 
                                        <?php
                                        $exp_show_price1 = $final_result['exshowroom_price'][0]['exshowroom_price'];
                                        echo indianFormatNumber($exp_show_price1);
                                        ?></span>  
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <tr>
                            <?php
                            if (!empty($final_result['exshowroom_price'][0]['mnc_exshowroom_price'])) {
                                ?>
                                <td width="65%" style="padding: 2px;" >

                                </td>
                                <td width="35%" style="padding: 2px;" >
                                    <i class="fa fa-rupee"></i> 
                                    <span id="tot_price" class="text-maroon mnc-price">
                                        <?php
                                        $exp_show_price = $final_result['exshowroom_price'][0]['mnc_exshowroom_price'];
                                        $main_price = $exp_show_price;
                                        echo indianFormatNumber($main_price);
                                        ?>  
                                    </span>    
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                        if (!empty($pro_offer)) {
                            ?>
                            <tr style="display:none;">
                                <td style="padding: 2px;">
                                    Group Discount
                                </td>
                                <td>
                                    <i class="fa fa-rupee"></i><span id="dis_price"><?php
//                                        echo $d1=date('Y-m-22');
                                        $date1 = date_create(date('Y-m-d'));
                                        $date2 = date_create($pro_offer[0]['end_date']);
                                        $diff = date_diff($date1, $date2);
                                        $day = $diff->format("%R%a");
                                        $day = str_replace('-', '', $day);
                                        $days = str_replace('+', '', $day);

                                        if ($pro_offer[0]['discount_type'] == 2) {
//                                        echo $pro_offer[0]['amount_per'];    
                                            $dis_price = $pro_offer[0]['amount_per'] . '';
                                            echo indianFormatNumber($dis_price);
                                        } else if ($pro_offer[0]['discount_type'] == 1) {
                                            $string = preg_replace('/[^0-9_.]/', '', $pro_offer[0]['amount_per']);
                                            $discount_per = (($string / 100));
                                            $dis_price = $main_price * $discount_per;
                                            echo indianFormatNumber($dis_price);
                                        }
                                        ?></span>

                                </td>
                            </tr>



                            <tr style="display:none;">
                                <td style="padding: 2px;" colspan="2">

                                    (Wait for <span id="days_data"><?php
                                        echo $days;
                                        $discount_price = $this->input->cookie('discount_price', TRUE);
                                        ?></span> Days)
                                    <input type="checkbox" <?php
                                    if ($discount_price != '') {
                                        echo 'checked';
                                    }
                                    ?> style="margin-top:-8px;" value="1" name="main_discount" id="main_discount" onclick="setdiscount();"/>    </label>
                                    <input type="hidden" style="float:right;margin-right:55px;" value="<?php
                                    if ($discount_price != '') {
                                        echo $days;
                                    }
                                    ?>" name="days" id="days" />    </label>

                                </td>

                            </tr>  
                            <?php
                        }
                        ?>

                        <tr style="display:none;">
                            <?php
                            if ($final_result['accessory_details'][0]['price'] != '') {
                                ?>
                                <td width="55%" style="padding: 2px;">
                                    Accessories 
                                </td>
                                <td width="46%" style="padding: 2px;">                                
                                    <i class="fa fa-rupee"></i> <?php
                                    $accessory = '0';
//                                    $accessory = $final_result['accessory_details'][0]['price'];
                                    echo indianFormatNumber($accessory);
                                    ?>
                                    <!--<span class="text-maroon info-span" data-toggle="modal" data-target="#price-modal"><i class="fa fa-info-circle"></i></span>-->                            
                                </td>
                                <?php
                            }
                            ?>
                        </tr> 
                        <tr style="display:none;">
                            <?php
                            if ($final_result['additional_config'][0]['price'] != '') {
                                ?>
                                <td width="55%" style="padding: 2px;">
                                    Additional Configuration 
                                </td>
                                <td width="46%" style="padding: 2px;">                                
                                    <i class="fa fa-rupee"></i> <?php
                                    $additional = $final_result['additional_config'][0]['price'];
                                    echo indianFormatNumber($additional);
                                    ?>  
                                    <!--<span class="text-maroon info-span" data-toggle="modal" data-target="#price-modal"><i class="fa fa-info-circle"></i></span>-->                            
                                </td>
                                <?php
                            }
                            ?>

                        </tr>
                    </table>


                    <div class="modal fade" id="price-modal" tabindex="-1" role="dialog" aria-labelledby="price-modal-label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h5 id="price-modal-label" style="color:white;"><i class="fa fa-money"></i> On-Road Price Breakup</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="car-review-table table table-bordered table-striped table-condensed" style="color:grey;border:1px solid #7B7776;margin-bottom:-10px; margin-top: -10px;">
<!--                                                <thead>
                                                    <tr >                                                            
                                                        <th align="left" style="background-color:#7B7776 !important;color: white;">Tax Type</th>
                                                        <th align="left" style="background-color:#7B7776 !important;color: white;">Amount</th>
                                                    </tr>
                                                </thead>-->
                                                <tbody>
                                                    <tr>                                                           
                                                        <td> Ex-Showroom Price</td>
                                                        <td>
                                                            <span class="strike-through"><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo indianFormatNumber($exp_show_price1);
//                                                            echo '<br>';
                                                                ?>
                                                            </span>    
                                                            &nbsp;
                                                            <span id="tot_price" class="text-maroon mnc-price">
                                                                <?php
                                                                echo indianFormatNumber($main_price);
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    if (!empty($final_result['onroad_calculation'])) {
                                                        if (strpos($final_result['onroad_calculation'][0]['octroi'], '%') !== false) {
                                                            $per = rtrim($final_result['onroad_calculation'][0]['octroi'], "%");
                                                            $on_cal = $exp_show_price * ($per / 100);
                                                            $octrol = $on_cal;
                                                            $per_oct = $final_result['onroad_calculation'][0]['octroi'];
                                                            $gtotal = $gtotal + $octrol;
                                                        } else {
                                                            $octrol = $final_result['onroad_calculation'][0]['octroi'];
                                                            $gtotal = $gtotal + $octrol;
                                                        }
                                                        ?>
                                                        <tr>                                                           
                                                            <td> Local Body Tax (Octroi)<?php
                                                                if (!empty($per_oct)) {
                                                                    echo '(' . $per_oct . ')';
                                                                }
                                                                ?></td>
                                                            <td>
                                                                <i class="fa fa-rupee"></i>
                                                                <?php
                                                                echo indianFormatNumber($octrol);
                                                                ?>                                                             
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        if (strpos($final_result['onroad_calculation'][0]['registeration_charges'], '%') !== false) {
                                                            $per2 = rtrim($final_result['onroad_calculation'][0]['registeration_charges'], "%");
                                                            $on_cal3 = $exp_show_price * ($per2 / 100);
                                                            $registeration_charges = $on_cal3;
                                                            $per_registeration_charges = $final_result['onroad_calculation'][0]['registeration_charges'];
                                                            $gtotal = $gtotal + $registeration_charges;
                                                        } else {
                                                            $registeration_charges = $final_result['onroad_calculation'][0]['registeration_charges'];
                                                            $gtotal = $gtotal + $registeration_charges;
                                                        }
                                                        ?>
                                                        <tr>                                                           
                                                            <td>RTO Tax & Registration Charges<?php
                                                                if (!empty($per_registeration_charges)) {
                                                                    echo '(' . $per_registeration_charges . ')';
                                                                }
                                                                ?></td>
                                                            <td><i class="fa fa-rupee"></i>
                                                                <?php
                                                                echo indianFormatNumber($registeration_charges);
//                                                                $registeration_charges; 
                                                                ?>                                                             
                                                            </td>
                                                        </tr>                                                        
                                                        <?php
                                                        if (strpos($final_result['onroad_calculation'][0]['handling_price'], '%') !== false) {
                                                            $per3 = rtrim($final_result['onroad_calculation'][0]['handling_price'], "%");
                                                            $on_cal4 = $exp_show_price * ($per3 / 100);
                                                            $handling_price = $on_cal4;
                                                            $per_handling_price = $final_result['onroad_calculation'][0]['handling_price'];
                                                            $gtotal = $gtotal + $handling_price;
                                                        } else {
                                                            $handling_price = $final_result['onroad_calculation'][0]['handling_price'];
                                                            $gtotal = $gtotal + $handling_price;
                                                        }
                                                        ?>
                                                        <tr>                                                           
                                                            <td>Handling Charges

                                                                <?php
                                                                if (!empty($per_handling_price)) {
                                                                    echo '(' . $per_handling_price . ')';
                                                                }
                                                                ?></td>
                                                            <td><i class="fa fa-rupee"></i>
                                                                <?php
                                                                echo indianFormatNumber($handling_price);
                                                                $handling_price;
                                                                ?>                                                             
                                                            </td>
                                                        </tr>   

                                                        <?php
                                                        if (strpos($final_result['onroad_calculation'][0]['comp_insurance'], '%') !== false) {
                                                            $per6 = rtrim($final_result['onroad_calculation'][0]['comp_insurance'], "%");
                                                            $on_cal6 = $exp_show_price * ($per6 / 100);
                                                            $comp_insurance = $on_cal6;
                                                            $per_comp_insurance = $final_result['onroad_calculation'][0]['comp_insurance'];
                                                            $gtotal = $gtotal + $comp_insurance;
                                                        } else {
                                                            $comp_insurance = $final_result['onroad_calculation'][0]['comp_insurance'];
                                                            $gtotal = $gtotal + $comp_insurance;
                                                        }
                                                        ?>
                                                        <tr>                                                           
                                                            <td> Zero Depreciation Insurance<?php
                                                                if (!empty($per_comp_insurance)) {
                                                                    echo '(' . $per_comp_insurance . ')';
                                                                }
                                                                ?></td>
                                                            <td><i class="fa fa-rupee"></i>
                                                                <?php
                                                                echo indianFormatNumber($comp_insurance);
                                                                $comp_insurance;
                                                                ?>                                                             
                                                            </td>
                                                        </tr>                                                        

                                                        <?php
                                                        if (strpos($final_result['onroad_calculation'][0]['other_charges'], '%') !== false) {
                                                            $per4 = rtrim($final_result['onroad_calculation'][0]['other_charges'], "%");
                                                            $on_cal5 = $exp_show_price * ($per4 / 100);
                                                            $other_charges = $on_cal5;
                                                            $per_other_charges = $final_result['onroad_calculation'][0]['other_charges'];
                                                            $gtotal = $gtotal + $other_charges;
                                                        }else{
                                                            $other_charges=$final_result['onroad_calculation'][0]['other_charges'];
                                                            $gtotal = $gtotal + $other_charges;
                                                        }
                                                        if($other_charges>0)
                                                        {
                                                        $gtotal=$gtotal-$comp_insurance;    
                                                        }    
                                                        ?>
                                                        <tr>                                                           
                                                            <td>Special One Time deal on Insurance Premium<?php
                                                                if (!empty($per_other_charges)) {
                                                                    echo '(' . $per_other_charges . ')';
                                                                }
                                                             ?>
                                                            </td>
                                                            <td><i class="fa fa-rupee"></i>
                                                                <?php
                                                                echo indianFormatNumber($other_charges);
                                                                $other_charges;
                                                                ?>                                                             
                                                            </td>
                                                        </tr>                                                        
                                                        <?php
                                                        if (strpos($final_result['onroad_calculation'][0]['home_delievery_charge'], '%') !== false) {
                                                            $per4 = rtrim($final_result['onroad_calculation'][0]['home_delievery_charge'], "%");
                                                            $on_cal5 = $exp_show_price * ($per4 / 100);
                                                            $home_delievery_charge = $on_cal5;
                                                            $per_home_delievery_charge = $final_result['onroad_calculation'][0]['home_delievery_charge'];
                                                            $gtotal = $gtotal + $home_delievery_charge;
                                                        } else {
                                                            $home_delievery_charge = $final_result['onroad_calculation'][0]['home_delievery_charge'];
                                                            $gtotal = $gtotal + $home_delievery_charge;
                                                        }
                                                    }
                                                    ?>
                                                    <tr>                                                           
                                                        <td> Delivery Period</td>
                                                        <td>
                                                            <?php
                                                            if ($final_result['exshowroom_price']['0']['delivery_date'] == '') {
                                                                echo '2 - 5 weeks';
                                                            } else {
                                                                echo $final_result['exshowroom_price']['0']['delivery_date'];
                                                            }
                                                            ?>

                                                        </td>
                                                    </tr>


                                                </tbody>     
                                            </table>   
                                            <h5 style="padding:5px 0px;display:none;"><a onclick="$('#extra_benefits').toggle();" href="#">Extra Benefits</a><h5> 
                                                    <table class="car-review-table table table-bordered table-striped table-condensed" id="extra_benefits" style="color:grey;border:1px solid #7B7776;margin-bottom:-10px; margin-top:0px;display:none;">
<!--                                                <thead>
                                                    <tr >                                                            
                                                        <th align="left" style="background-color:#7B7776 !important;color: white;">Tax Type</th>
                                                        <th align="left" style="background-color:#7B7776 !important;color: white;">Amount</th>
                                                    </tr>
                                                </thead>-->
                                                <tbody>
                                                    <tr>                                                           
                                                        <td> Cash Discount</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['cash_discount'];
                                                                
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Finance Benefit</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                               <?php
                                                                echo $final_result['exshowroom_price'][0]['finance_benefit'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['finance_benefit'];
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Exchange Benefit</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['exchange_benefit'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['exchange_benefit'];
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Loyality Bonus</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['loyality_bonus'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['loyality_bonus'];
                                                                ?> 
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Corporate Discount</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['corporate_discount'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['corporate_discount'];
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Waivier of Handling charges</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['waivier_handling_charges'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['waivier_handling_charges'];
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Accessories Package</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['accessory_package'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['accessory_package'];
                                                                ?> 
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Accessories Discount</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['accessory_discount'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['accessory_discount'];
                                                                ?> 
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Insurance Benefit</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['insurance_benefits'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['insurance_benefits'];
                                                                ?> 
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Extended Warranty</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['extended_warranty'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['extended_warranty'];
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Liquidation Support by OEM</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['liquidation_support'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['liquidation_support'];
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>
                                                    <tr>                                                           
                                                        <td> Special Reward</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $final_result['exshowroom_price'][0]['additional_liquidation_support'];
                                                                $total=$total+$final_result['exshowroom_price'][0]['additional_liquidation_support'];
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>
<!--                                                    <tr>                                                           
                                                        <td> Total</td>
                                                        <td>
                                                            <span><i class="fa fa-rupee"></i> 
                                                                <?php
                                                                echo $total;
                                                                ?>  
                                                            </span> 
                                                        </td>
                                                    </tr>-->
                                                    </tbody>    
                                            </table> 
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <table class="table" cellpadding="0" cellspacing="0" style="margin-top:-15px;">
                        <tr>
                            <?php
                            if (!empty($final_result['onroad_calculation'][0])) {
                                ?>
                                <td width="65%" style="padding: 2px;">
                                    On-Road Price &nbsp;<span class="text-maroon info-span" data-toggle="modal" data-target="#price-modal"><i class="fa fa-info-circle"></i></span>
                                </td>
                                <td width="35%" style="padding: 2px;" >
                                    <i class="fa fa-rupee"></i> 
                                    <span id="grand_price">
                                        <?php
                                        if ($discount_price != '') {
                                            $final_tot = $gtotal - $discount_price . '';
                                            $final_tot = $main_price + $final_tot;
                                            echo indianFormatNumber($final_tot-$total);
                                        } else {
                                            $final_tot = $gtotal;
                                            $final_tot = $main_price + $final_tot;
                                            echo indianFormatNumber($final_tot-$total);
                                        }
                                        
                                        $final_amount_data = $final_tot;
                                        ?>
                                    </span>  
                                    <input type="hidden" style="margin-top:-8px;" value="<?php echo $final_tot; ?>" name="grand_total" id="grand_total"/>


                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <tr>
                            <?php
                            if (!empty($final_result['exshowroom_price'][0]['exshowroom_price'])) {
                                ?>
                                <td width="65%" style="padding: 2px;">
                                    <a data-target="#emi_caulculator" data-toggle="modal" href="#">EMI Calculator</a>
                                </td>
                                <td width="35%" style="padding: 2px;" >
                                    <i class="fa fa-rupee"></i> 
                                    <span id="grand_price">
                                        <?php
                                        echo indianFormatNumber(round(preg_replace('/[^0-9 . \-]/', '', $get_emi[0]['emi_amount'])));
                                        ?>   
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <tr>
                            <td width="65%" style="padding: 2px;">
                                Booking Amount &nbsp;<span class="text-maroon info-span" data-toggle="modal" data-target="#reundable-modal"><i class="fa fa-info-circle"></i></span>   
                            </td>
                            <td width="35%" style="padding: 2px;" >
                                <i class="fa fa-rupee"></i> 
                                5,000&nbsp;
                            </td>
                        <div class="modal fade" id="reundable-modal" tabindex="-1" role="dialog" aria-labelledby="price-modal-label" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h5 id="price-modal-label" style="color:white;">Booking Amount Details</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12"> 
                                            100% refundable within 48 hours of booking (excluding Public holiday and Sunday)  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </tr>
                        <tr>
                            <td width="65%" style="padding: 2px;">
                                Delivery Period &nbsp;<span class="text-maroon info-span" data-toggle="modal" data-target="#delivery-modal"><i class="fa fa-info-circle"></i></span>
                            </td>
                            <td width="35%" style="padding: 2px;" >
                                <?php
                                if ($final_result['exshowroom_price']['0']['delivery_date'] == '') {
                                    echo '2 - 5 weeks';
                                } else {
                                    echo $final_result['exshowroom_price']['0']['delivery_date'];
                                }
                                ?>

                            </td>
                        <div class="modal fade" id="delivery-modal" tabindex="-1" role="dialog" aria-labelledby="price-modal-label" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h5 id="price-modal-label" style="color:white;">Delivery Period Details</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12"> 
                                                Subject to variant availability and color options selected.    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </tr>

                    </table>

                </div>
            </div>
        </div>
    </div>
    <?php
//    echo $content;
    if ($content == 'carIntroduction') {
        $redirect = base_url() . 'car/' . $main_id;
    } else if ($content == 'configureCarVariant') {
        $redirect = base_url() . 'car-introduction/' . $main_id;
    } else if ($content == 'carExteriorSelection') {
        $redirect = base_url() . 'configure-variant/' . $main_id;
    } else if ($content == 'carInteriorSelection') {
        $redirect = base_url() . 'exterior-selection/' . $main_id;
    } else if ($content == 'configureCarAccessory') {
        $redirect = base_url() . 'interior-selection/' . $main_id;
    } else if ($content == 'accessory_services') {
        $redirect = base_url() . 'exterior-selection/' . $main_id;
    } else if ($content == 'accessory_services') {
        $redirect = base_url() . 'accessory-services/' . $main_id;
    }
    else if ($content == 'carBookingPage') {
        $redirect = base_url() . 'accessory-services/' . $main_id;
        $redirect_payment = base_url() . 'payment-page/' . $main_id;
        $button_title='Book My New Car';
    }
    else if($content == 'paymentpage') {
    $hide='display:none';    
    }
    ?>
    <?php
    if($user_id>0 && $user_id!='')
    {    
    ?>
    <div class="row" id="configuration_button" style="margin:-6px -11px;">
        <div role="alert" class="alert alert-warning alert-dismissible fade in" style="margin-bottom:10px;">
            <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
            <strong>Hello!</strong> configuration of the car is automatically saved to the user profile.
        </div>
    </div>    
    <?php
    }
    ?>
    <div class="row" id="configuration_button" style="margin:-6px -11px;<?php if($content=='paymentpage'){ echo 'display:block;';} else { echo 'display:none;';}?>">
        <div role="alert" class="alert alert-danger alert-dismissible fade in" style="margin-bottom:10px;">
            <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
            <strong>Hello!</strong>Please check city selection of product before checkout.
        </div>
    </div>
    
    <div class="row" id="configuration_button" style="<?php echo $hide;?>" >
        <div class="col-md-12" align="right">

            <button class="btn btn-grey" type="button" onclick="window.location.href = '<?php echo $redirect; ?>'">Back</button>    
            <button class="btn btn-maroon" type="button" <?php if ($redirect_payment == '') { ?> onclick="$('.confuration').click();" <?php } else { ?>  onclick="window.location.href = '<?php echo $redirect_payment; ?>'"<?php } ?> >
               <?php
               if(!empty($button_title))
               {
               echo $button_title;    
               }    
               else
               {
               echo 'Save &amp; Continue';    
               }    
               ?>
                </button>    
        </div>       
    </div>

    <?php
    $tot = count($car_intro_gallery_detail);
    if ($tot > 1) {
        ?>
        <div class="row">
            <div class="col-md-12">
                <div id="car-collection" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner container-fluid" style="padding-right: 10px;padding-left: 10px;">
                        <?php
                        $tot = count($car_intro_gallery_detail);
                        $div = $tot / 4;
                        $exp = explode('.', $div);
                        if ($exp[1] > 0) {
                            $final = $exp[0] + 1;
                        } else {
                            $final = $exp[0];
                        }
                        $final;
                        //                    echo '<pre>';
                        //                    foreach($car_intro_gallery_detail as $car_intro_gallery_detail_data)
                        //                    {
                        for ($j = 1, $i = 1; $j <= $tot; $j = $j + 4) {
                            if ($j == 1) {
                                $class = 'active';
                            } else {
                                $class = '';
                            }
                            ?>
                            <div class="item <?php echo $class; ?> row" style="margin-left:10px;">
                                <?php
                                for ($i; $i <= ($j + 3); $i++) {
                                    $filename = base_url() . 'admin/uploads/gallery/' . $car_intro_gallery_detail[$i - 1]['image'];
                                    if ($car_intro_gallery_detail[$i - 1]['image'] != '') {
                                        ?>
                                        <div class="col-sm-3">
                                            <a onclick="set_config_main_img('<?php echo $i; ?>');">
                                            <!--<a class="carcollection-thumb" onclick="set_config_main_img('<?php echo $i; ?>');" rel="carcollection-thumb" href="<?php echo $filename; ?>" title="This is the title">-->
                                                <img src="<?php echo $filename; ?>" id="set_config_main_img<?php echo $i; ?>"  class="img-thumbnail img-responsive" style="height: 150px;width: 100%;" >
                                            </a>  
                                        </div>                    
                                        <?php
                                    }
                                }
                                ?>
                            </div>                    
                            <?php
                        }
                        ?>    
                    </div>
                </div>
                <!-- Controls -->
                <div class="pull-right controls slider-controller">
                    <a href="#car-collection" data-slide="prev">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-chevron-left fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="#car-collection" data-slide="next">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-chevron-right fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                </div>
                <div class="clearfix"></div>
                <!-- Controls -->
            </div> <!-- /#car-collection -->
        </div>
        <?php
    }
    ?>

</div>
</div>

<script>
    function set_config_main_img(id)
    {
        //alert(id); 
//    var src = $(img).attr('src'); 
        var img_src = document.getElementById("set_config_main_img" + id).src;
//    var img=$("#set_config_main_img"+id ).html();
        //alert(img_src);    
        document.getElementById("conf_main_image").src = img_src;
    }
    function setdiscount()
    {
        var boxes = $('input[id="main_discount"]:checked').val();
        var dis_price = $('#dis_price').html();

	var dis_price=dis_price.replace(/[^0-9 ]/g, "");
	
        var tot_price = $('#tot_price').html();
	var tot_price=tot_price.replace(/[^0-9 ]/g, "");
        var days_data = $('#days_data').html();
	var days_data=days_data.replace(/[^0-9 ]/g, "");
	
        var grand_price = $('#grand_price').html();

	var grand_price=grand_price.replace(/[^0-9 ]/g, "");
		


        if (boxes == 1)
        {
            var final = grand_price - dis_price;
            var d = new Date();
            d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = 'discount_price=' + dis_price + ';' + expires + ';path=/';
            $('#grand_price').html(final);
            $('#grand_total').val(final);
            $('#days').val(days_data);
        }
        else
        {
	    var grand_price = parseInt(grand_price) + parseInt(dis_price);	
            var d = new Date();
            d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = 'discount_price=;' + expires + ';path=/';
            $('#grand_price').html(grand_price);
            $('#grand_total').val(grand_price);
            $('#days').val('');
        }
    }
</script>
