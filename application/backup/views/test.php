<?php
function get_feature_type_name($feature_id){
    $feature_type_label = NULL;
    switch ($feature_id) {
        case 1:
            $feature_type_label = 'Exteriors';
            break;
        case 2:
            $feature_type_label = 'Interiors';
            break;
        case 3:
            $feature_type_label = 'Comfort';
            break;
        case 4:
            $feature_type_label = 'Safety Security';
            break;
        case 5:
            $feature_type_label = 'Audio Communication';
            break;
        case 5:
            $feature_type_label = 'Instrument Panel';
            break;
        default:
            break;
    }
    return $feature_type_label;
}


//print_r($onroad_price_breakup);
?>
<style>
ul
{
display: block;list-style-type: disc;margin-top: 1em;margin-bottom: 1 em;margin-left: 0;margin-right: 0;padding-left: 40px;    
}
</style>
<div style="width:80%;margin:10%;">
<!--<table style="border-collapse:collapse;width:100%;margin-bottom:10px">
                
            </table>-->
<!--<p style="margin-top:0px;margin-bottom:10px;font-weight:bold;text-align:center">5, Nav Meghdoot Building, 535, Linking Road, Khar (W), Mumbai - 400 052</p>
            <p style="margin-top:0px;margin-bottom:10px">Thank you for your interest in <span class="il">Prrems</span> products. Your <span class="il">order</span> has been received and will be processed once payment has been confirmed.</p>
                        <p style="margin-top:0px;margin-bottom:10px">To view your <span class="il">order</span> click on the link below:</p>
            <p style="margin-top:0px;margin-bottom:10px"><a href="http://mandrillapp.com/track/click/30066688/107.170.213.155?p=eyJzIjoiaFhod05vMHBJSDMzM19sTzBjZG84UUotcnNFIiwidiI6MSwicCI6IntcInVcIjozMDA2NjY4OCxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvMTA3LjE3MC4yMTMuMTU1XFxcL2luZGV4LnBocD9yb3V0ZT1hY2NvdW50XFxcL29yZGVyXFxcL2luZm8mb3JkZXJfaWQ9MTNcIixcImlkXCI6XCJlNTkxNWRiYjhjNjM0ZjhmOTM5Y2Q0M2QxNWFlOTMwNVwiLFwidXJsX2lkc1wiOltcImYwMzNiODI3ZmUzMDBlYjhiZTEyMWNjMTVkZDg4MDE3NzMwNmQwN2FcIl19In0" target="_blank">http://107.170.213.155/index.<wbr></wbr>php?route=account/<span class="il">order</span>/info&amp;<wbr></wbr>order_id=13</a></p>-->
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td style="font-size:12px;border-right:0px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <div style="float:left;">
                        <a href="" style="text-align:center" target="_blank">
                            <img class="CToWUd" src="<?php echo base_url(); ?>uploads/logo2.png" style="margin-bottom:10px;border:none;width:30%;">
                        </a> 
                    </div >    

                </td>
                <td style="font-size:12px;border-right:0px solid #dddddd;border-bottom:1px solid #dddddd;text-align:center;padding:7px">
                    <div style="font-size:13px;text-transform:uppercase;" align="center">Booking Confirmation</div>   
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    <a href="" title="Prrems" style="text-align:center" target="_blank">
                        <img class="CToWUd" src="<?php echo base_url(); ?>assets/img/home/contact-img.png" alt="Prrems" style="margin-top:15px;border:none;width:30%;;">
                    </a>    
                </td>
            </tr>
        </thead>
    </table>
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
    
        <thead>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222" colspan="2">
                    <span class="il">Order</span> Details</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <b><span class="il">Booking Order No:</span></b> <?php echo $car_configuration[0]['invoice_no'] ?><br>
                    <!--                    <b>Date Added:</b> 11/12/2014<br>
                                        <b>Payment Method:</b> Secure Payment Gateway (CCAVENUE)<br>
                                        <b><span class="il">Shipping</span> Method:</b> India VAT Zone  (Weight: 1.00kg)-->
                </td>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <b>Customer Name:</b> <?php echo $customer_details[0]['firstname'] . ' ' . $customer_details[0]['lastname']; ?><br>
                </td>
            </tr>
            <tr>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <b><span class="il">Date of Booking:</span></b> <?php 
                    $added_date=substr($car_configuration[0]['added_date'], 0, 10); 
                    $exp=explode('-',$added_date);
                    echo $exp[2].'-'.$exp[1].'-'.$exp[0];
                    ?><br>
                </td>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px" >
                    <b>Registration City:</b><span style="text-transform:capitalize;"> <?php echo $order_result['city_name']; ?></span><br>
                </td>
            </tr>
        </tbody>


    </table>


    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Vehicle Information</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <div style="float:left;width:50%;">
                        <?php
//                        echo base_url() . '/admin/uploads/product/' . $car_into_details[0]['pro_image'];
                        ?>
                        <img style="margin-bottom:10px;border:none;width:20%;" src="<?php echo $pro_image = img_url('/uploads/product/' . $car_into_details[0]['pro_image']); ?>" class="CToWUd">    
                    </div>
                    <div style="float:left;width:50%;">
                        <?php echo $car_into_details[0]['pro_name_comp']; ?><br>
                        <?php 
//                        print_r($car_configuration);
                        
                        echo $booking_varaint[1]['fuel_type']; ?><br>
                        <?php echo $booking_exterior[0]['feature_desc']; ?>
                    </div>                   
                </td>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <p> Ex-Showroom Price: <?php echo 'Rs. ';?><?php echo  $booking_varaint[1]['mnc_exshowroom_price']; ?></p>
                    <p> Comprehensive Insurance : <?php echo 'Rs. '.$onroad_price_breakup[0]['comp_insurance'];?></p>
                    <p> RTO and Handling Charges : <?php echo 'Rs. '.$onroad_price_breakup[0]['road_tax'];?></p>
                    <?php
                    if($car_configuration[0]['insurance_id']==1)
                    {    
                    ?>
                    <p> Zero Depreciation Insurance (Optional) : <?php if($onroad_price_breakup[0]['zero_depreciation']>0) { echo '+ Rs.'.$onroad_price_breakup[0]['zero_depreciation'];}?></p>
                    <?php
                    }
                    ?>
                    <?php
                    if($car_configuration[0]['extended_id']==1)
                    {    
                    ?>
                    <p> Extended Warranty : <?php echo 'Rs. '.$onroad_price_breakup[0]['extended_warranty'];?></p>
                    <?php
                    }
                    ?>
                    <p> Total Savings: <?php echo 'Rs. '.$car_configuration[0]['total_saving'];?></p>
                    <p> On-Road Price: <?php  echo 'Rs. ' . $car_configuration[0]['on_road_price']; ?></p>
                    <p>Refundable Booking Amount: Rs. <?php echo $order_result['paid_amount'];?></p>   
                    <p>
                    <?php 
//                    echo  '<pre>';
                    if(!empty($coupon_used))
                    {    
                    if($coupon_used['coupon_type']=='Material')
                    {
                    echo '<span style="color:#FF4F27">Note: </span>'.$coupon_used['coupon_material'];     
                    }
                    else
                    {
                    echo '<span style="color:#FF4F27">'.$coupon_used['coupn_code'].' Coupon code used & '.$coupon_used['amount_percent'].' Off in '.$car_into_details[0]['pro_name_comp'].'!</span>';    
                    }    
                    }    
                    ?>                    
                    </p>    
                </td>
            </tr>
        </tbody>
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Additional Services</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <p> Car Finance: <?php
                        if ($car_configuration[0]['finance_id'] != '' && $car_configuration[0]['finance_id'] != '-') {
                            echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                        } else {
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
                        }
                        ?></p>

                    <p> Zero Depreciation Insurance: <?php
                        if ($car_configuration[0]['insurance_id'] != '' && $car_configuration[0]['insurance_id'] != '-') {
                            echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                        } else {
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
                        }
                        ?></p>
                    <p> Car Exchange: <?php
                        if ($car_configuration[0]['used_id'] != '' && $car_configuration[0]['used_id'] != '-') {
                            echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                        } else {
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
                        } 
                        ?> </p>


                </td> 
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">

                    
                    <p> Free Home Delivery: <?php
                        if ($car_configuration[0]['home_delivery'] != '' && $car_configuration[0]['home_delivery'] != '-') {
                            echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                        } else {
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
                        }
                        ?> </p>
                    <p> Corporate Discount: <?php
                        if ($car_configuration[0]['corporate_discount'] != '' && $car_configuration[0]['corporate_discount'] != '-') {
                            echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                        } else {
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
                        }
                        ?> </p>
                    <p> Loyalty Discount: <?php
                        if ($car_configuration[0]['loyality_bonus'] != '' && $car_configuration[0]['loyality_bonus'] != '-') {
                            echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                        } else {
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
                        }
                        ?> </p>

                </td> 
            </tr>
            <tr>
                <td colspan="2" style="font-size:10px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px;">
                    <b> Our Customer Care will contact you for special deals on Car Insurance, Finance, Car Exchange Genuine Accessories and other related services. </b>
                </td>
            </tr>
        </tbody>
        <thead>
            
        </thead>
    </table>


    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td  style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    <span class="il">Correspondence</span>  Address</td>
                <td  style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    <span class="il">Registration</span>  Address</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <?php echo $customer_details[0]['firstname'].' '.$customer_details[0]['lastname'];?>
                    <br>
                    <?php echo $customer_details[0]['address'];?>
                    <br>                    
                    <?php echo $customer_details[0]['city'];?>
                    <br>
                    <?php echo $customer_details[0]['state'];?>
                    <br>                    
                    <?php echo $customer_details[0]['pincode'];?>
                    <br>                    
                    <?php echo $customer_details[0]['email'];?>
                    <br>                    
                    <?php echo $customer_details[0]['telephone'];?>
                    <br>                                        
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <?php echo $registeration_details[0]['name'];?>
                    <br>
                    <?php echo $registeration_details[0]['address'];?>
                    <br>                    
                    <?php echo $registeration_details[0]['city'];?>
                    <br>
                    <?php echo $registeration_details[0]['state'];?>
                    <br>                    
                    <?php echo $registeration_details[0]['pincode'];?>
                    <br>                    
                    <?php echo $registeration_details[0]['email'];?>
                    <br>                    
                    <?php echo $registeration_details[0]['telephone'];?>
                    <br>         
                </td>
            </tr>

        </tbody>


    </table>

    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td  colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">
                    <span class="il">Important </span> Information</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2" style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <ul >
                        <li style="font-size:10px;">Full Manufacturer Warranty</li>    
                        <li style="font-size:10px;">After Sales Service provided by our partnered authorized dealer</li>    
                        <li style="font-size:10px;">Special Deals on car accessories,insurance,finance,car exchange and other related services can be availed only on purchase of a new car through www.mynewcar.in</li>    
                        <li style="font-size:10px;">Tentative delivery period of 2 - 5 weeks is based on variant and color availability</li>    
                        <li style="font-size:10px;">All the offers are promoted by our respective partners</li>    
                        <li style="font-size:10px;">It is mandatory to have a Photo Identity along with Address Proof for your vehicle registration.This can include Passport, Adhar Card, Ration Card, Telephone Bill,
                    Deed, Pan Card or any other Identification Proof issued by The Government of India.</li>    
                        <li style="font-size:10px;">Color change may attract additional charges as applicable by manufacturers</li>    
                        <li style="font-size:10px;">Specification and features are subject to change without any notice</li>    
                        <li style="font-size:10px;">Accessories shown are not part of standard equipment. Features vary from model to model. Colours are indicative only.</li>    
                        <li style="font-size:10px;">Please refer to our website for Terms of Offer for Sale.</li>    
                        
                    </ul>
                    <div style="margin-left:10px;">
                        1. Taxes & other charges as applicable.<br>
                        2. On-Road Price indicated is based on on Taxes & Local Levies as applicable (subject to change)<br>
                        <!--3. Additional Savings via Group Discount can be availed in case the number of car bookings for the specified car brand reaches its target within the specified deal period<br>-->
                        <!--3. Total Savings include the Additional Group Discount Option<br>-->
                        3. Fully refundable within 48 hours of booking<br>
                    </div>    
                </td>

            </tr>

        </tbody>


    </table>
<!--<pagebreak />-->
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td  style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222;">

                    <img style="margin-bottom:10px;border:none;width:100%;" alt="Prrems" src="<?php echo base_url() . 'uploads/booking_flow.png'; ?>"  class="CToWUd">    
                    <!--<img style="margin-bottom:10px;border:none;" alt="Prrems" src="<?php echo $pro_image = base_url() . 'uploads/booking_flow.png' . $car_into_details[0]['pro_image']; ?>" class="CToWUd">-->    


                </td>                
            </tr>
        </thead>
        
    </table>
    
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222;">
                    <b>Our customer care will get in touch with you within the next 48 hours to confirm the delivery details along with offers on other related services and
                        accessories requested by you. You could also reach our customer care for any clarification by emailing at contact@mynewcar.in or on +91 22 45
                        02 03 04 (Mon-Sat l 10 am - 7 pm)</b>
                </td>
            </tr>
        </thead>
    </table>





<!--    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td style="font-size:12px;border-right:0px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <div style="float:left;">
                        <a href="" title="Prrems" style="text-align:center" target="_blank">
                            <img class="CToWUd" src="<?php echo base_url(); ?>uploads/logo2.png" alt="Prrems" style="margin-bottom:10px;border:none;width:30%;">
                        </a> 
                    </div >    

                </td>
                <td style="font-size:12px;border-right:0px solid #dddddd;border-bottom:1px solid #dddddd;text-align:center;padding:7px">
                    <div style="font-size:13px;text-transform:uppercase;" align="center">Booking Confirmation</div>   
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">
                    <a href="" title="Prrems" style="text-align:center" target="_blank">
                        <img class="CToWUd" src="<?php echo base_url(); ?>assets/img/home/contact-img.png" alt="Prrems" style="margin-bottom:10px;border:none;width:30%;;">
                    </a>    
                </td>
            </tr>
        </thead>
    </table>-->




    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"><?php echo $car_into_details[0]['pro_name_comp']; ?></td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <div style="float:left;width:50%;">
                        <img style="margin-bottom:10px;border:none;width:20%;" alt="Prrems" src="<?php echo $pro_image=img_url('/uploads/product/' . $car_into_details[0]['pro_image']); ?>" class="CToWUd">    
                    </div>
                    <div style="float:left;width:50%;display:none;">
                        <?php echo $car_into_details[0]['pro_name_comp']; ?><br>
                        <?php echo $booking_varaint[1]['fuel_type']; ?>,<?php echo $booking_exterior[0]['feature_desc']; ?>
                    </div>
                    <!--dinesh kumar<br>365,amar nagar,ggs road,mulund(w)<br>mumbai 400082<br>Maharashtra<br>India-->

                </td>
                <td style="width:50%;font-size:12px;border-right:1px solid #dddddd;text-align:left;padding:7px">
                    
                </td>

            </tr>
        </tbody>
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Specification</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px" colspan="2">
                    <p>Fuel Type: <?php  echo $booking_varaint[1]['fuel_type'];?></p>
                    <p>Displacement (cc): <?php  echo $pro_specification[0]['displacement'];?></p>
                    <p>Power (bhp): <?php  echo $pro_specification[0]['engine_maximum_power'];?></p>
                    <p>Transmission Type: <?php  echo $booking_varaint[1]['tramission_type'];?></p>
                    <p>Top Speed (kmph): <?php  echo $pro_specification[0]['top_speed'] . ' KMPH';?></p>
                    <p>Acceleration ( 0-100 kmph): <?php  echo $pro_specification[0]['acceleration'];?></p>
                    <p>Drivetrain: <?php  echo $pro_specification[0]['drivetrain'];?></p>
                    <p>Seating Capacity: <?php  echo $booking_varaint[1]['seating_capacity'];?></p>
                    <p>Standard Warranty: <?php  echo $pro_specification[0]['standard_warranty'];?></p>
                    
                    <br>
                </td>
            </tr>
        </tbody>
    </table>   
    
    <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Features</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <strong>Interiors</strong>
                    <br>
                    <?php
                    foreach($pro_feture as $pro_feture_data)
                    {
                    if(get_feature_type_name($pro_feture_data['feature_type'])=='Interiors')
                    {
                    echo '<p>'.$pro_feture_data['feature_name'].': ';
                                        if (!empty($pro_feture_data['feature_img'])) {
                                            ?>
                                            <img width="50" src="<?php echo img_url('uploads/feature/' . $pro_feture_data['feature_img']); ?>"/>
                                            <br>
                                            <?php
                                            echo $pro_feture_data['feature_desc'];
                                        } else {
                                            if (strtoupper($pro_feture_data['feature_desc']) == 'Y') {
                                               echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                                            } else if (strtoupper($pro_feture_data['feature_desc']) == 'N') {
                                               echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
                                            } else {
                                                echo $pro_feture_data['feature_desc'];
                                            }
                                        }
                        
                    echo '</p>';
                    
                    }    
                    }
                    ?>
                    
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <strong>Exteriors</strong>
                    <br>
                    <?php
                    foreach($pro_feture as $pro_feture_data)
                    {
                    if(get_feature_type_name($pro_feture_data['feature_type'])=='Exteriors')
                    {
                    echo '<p>'.$pro_feture_data['feature_name'].': ';
                                        if (!empty($pro_feture_data['feature_img'])) {
                                            ?>
                                            <img width="50" src="<?php echo img_url('uploads/feature/' . $pro_feture_data['feature_img']); ?>"/>
                                            <br>
                                            <?php
                                            echo $pro_feture_data['feature_desc'];
                                        } else {
                                            if (strtoupper($pro_feture_data['feature_desc']) == 'Y') {
                                                echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
//                                                echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
//                                                echo 'Yes';
                                            } else if (strtoupper($pro_feture_data['feature_desc']) == 'N') {
//                                                echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                                                    echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
                                            } else {
                                                echo $pro_feture_data['feature_desc'];
                                            }
                                        }
                    echo '</p>';
                    
                    }    
                    }
                    ?>
                    
                </td>
            </tr>
            <tr>
                <td style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <strong>Comfort</strong>
                    <br>
                    <?php
                    foreach($pro_feture as $pro_feture_data)
                    {
                    if(get_feature_type_name($pro_feture_data['feature_type'])=='Comfort')
                    {
                    echo '<p>'.$pro_feture_data['feature_name'].': ';
                                        if (!empty($pro_feture_data['feature_img'])) {
                                            ?>
                                            <img width="50" src="<?php echo img_url('uploads/feature/' . $pro_feture_data['feature_img']); ?>"/>
                                            <br>
                                            <?php
                                            echo $pro_feture_data['feature_desc'];
                                        } else {
                                            if (strtoupper($pro_feture_data['feature_desc']) == 'Y') {
                                                echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
//                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
//                                                echo 'Yes';
                                            } else if (strtoupper($pro_feture_data['feature_desc']) == 'N') {
//                                                echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
//                                                echo 'No';
                                            } else {
                                                echo $pro_feture_data['feature_desc'];
                                            }
                                        }
                    echo '</p>';
                    
                    }    
                    }
                    ?>
                    
                </td>
                <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <strong>Safety Security</strong>
                    <br>
                    <?php
                    foreach($pro_feture as $pro_feture_data)
                    {
                    if(get_feature_type_name($pro_feture_data['feature_type'])=='Safety Security')
                    {
                    echo '<p>'.$pro_feture_data['feature_name'].': ';
                                        if (!empty($pro_feture_data['feature_img'])) {
                                            ?>
                                            <img width="50" src="<?php echo img_url('uploads/feature/' . $pro_feture_data['feature_img']); ?>"/>
                                            <br>
                                            <?php
                                            echo $pro_feture_data['feature_desc'];
                                        } else {
                                            if (strtoupper($pro_feture_data['feature_desc']) == 'Y') {
                                                echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
//                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
//                                                echo 'Yes';
                                            } else if (strtoupper($pro_feture_data['feature_desc']) == 'N') {
//                                                echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
//                                                echo 'No';
                                            } else {
                                                echo $pro_feture_data['feature_desc'];
                                            }
                                        }
                    echo '</p>';
                    
                    }    
                    }
                    ?>
                    
                </td>
            </tr>
            <tr>
                <td style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    <strong>Audio Communication</strong>
                    <br>
                    <?php
                    foreach($pro_feture as $pro_feture_data)
                    {
                    if(get_feature_type_name($pro_feture_data['feature_type'])=='Audio Communication')
                    {
                    echo '<p>'.$pro_feture_data['feature_name'].': ';
                                        if (!empty($pro_feture_data['feature_img'])) {
                                            ?>
                                            <img width="50" src="<?php echo img_url('uploads/feature/' . $pro_feture_data['feature_img']); ?>"/>
                                            <br>
                                            <?php
                                            echo $pro_feture_data['feature_desc'];
                                        } else {
                                            if (strtoupper($pro_feture_data['feature_desc']) == 'Y') {
                                                echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
//                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
//                                                echo 'Yes';
                                            } else if (strtoupper($pro_feture_data['feature_desc']) == 'N') {
//                                                echo '<span style="font-size:12px;color:green;">&#x2713;</span>';
                            echo '<span style="font-size:12px;color:red;">&#x2717;</span>';
//                                                echo 'No';
                                            } else {
                                                echo $pro_feture_data['feature_desc'];
                                            }
                                        }
                    echo '</p>';
                    
                    }    
                    }
                    ?>
                    
                </td>
                <td style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    
                </td>
                
            </tr>

        </tbody>


    </table>
        

        <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:10px">
        <thead>
            <tr>
                <td colspan="2" style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">MYNEWCAR.IN Support</td>

            </tr>
        </thead>
        
        
        <tbody>
            <tr>
                <td colspan="2" style="width:47.5%;font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">
                    Web: https://mynewcar.in/contact-us
                    <br>
                    FAQ's: https://mynewcar.in/faq
                    <br>
                    Telephone: +022 45 02 03 04  (Monday-Saturday l 10 am - 7 pm)
                    <br>
                    Email: contact@mynewcar.in
                    <br>
                    Visit Us on Facebook or Follow Us on Twitter
                    <br>
                    DREAMZ MYNEWCAR INDIA Pvt. Ltd. Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.

                </td>

            </tr>
        </tbody>
    </table>


</div>
