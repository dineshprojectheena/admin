<?php
if(count($pro_res)>0)
{
?>
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped car-selection-table" width="100%">
    <thead>
        <tr>
            <th>Brand</th>
            <th>Car Variant</th>
            <th>Photo</th>
            <th style="width:170px">Ex-Showroom Price&nbsp;&nbsp;</th>
            <th style="width:120px;">Rating</th>
            <th></th>
        </tr>
    </thead>
    <tbody> 
        <?php 
            
        foreach ($pro_res as $pro_res_data) {
            ?>
            <tr>
                <td valign="middle">
                <img class="brand-logo" alt="<?php echo $car_variant_data[$i]['pro_name_comp']; ?>" alt="<?php echo $pro_res_data['brand_image']; ?>" src="<?php echo img_url('uploads/brand/' . $pro_res_data['brand_image']); ?>">
                <span><?php $pro_res_data['brand_name']; ?></span>
                </td>
                <td valign="middle" class="car-details">
                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_res_data['pro_name_comp']) . '-' . $pro_res_data['variant_id']) ?>" >
                        <?php echo $pro_res_data['pro_name_comp']; ?>
                    </a>    
                </td>
                <td valign="middle">
                    <?php
                    if(!empty($pro_res_data['total_benefits_perc']))
                    {    
                    ?>
                    <div class="ribbin">	
                        <div class="corner"><h2 class="search_strip_data"><?php echo $pro_res_data['total_benefits_perc'];?></h2>
                            <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_res_data['pro_name_comp']) . '-' . $pro_res_data['variant_id']) ?>" >
                                <img src="<?php echo img_url('uploads/product/' . $pro_res_data['pro_image']); ?>" alt="<?php echo str_replace('.jpg', '',str_replace('/', ' ',$pro_res_data['pro_image'])); ?>" style="width:150px;"/>
                            </a>
                        </div>
                    </div>
                    <?php
                    }
                    else
                    {    
                    ?>
                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_res_data['pro_name_comp']) . '-' . $pro_res_data['variant_id']) ?>" >
                                <img src="<?php echo img_url('uploads/product/' . $pro_res_data['pro_image']); ?>"  alt="<?php echo $pro_res_data['pro_image']; ?>" style="width:150px;"/>
                            </a>                   
                    <?php
                    }
                    ?>
                </td>
                <td valign="middle" class="car-price">
                   
                                        <?php
                                        if (indianFormatNumber($pro_res_data['mnc_exshowroom_price']) > 0) {
                                            ?>
                                            <span class="text-maroon">
                                                 <i class="fa fa-rupee"  style="font-size:14px;"></i>
                                                <?php
                                                if (indianFormatNumber($pro_res_data['mnc_exshowroom_price']) == 0) {
                                                    echo '0';
                                                } else {
                                                    echo indianFormatNumber($pro_res_data['mnc_exshowroom_price']);
                                                }
                                                ?>   
                                            </span>
                                            <?php
                                        } else {
                                            ?>
                                            <span class="text-maroon">
                                                
                                                <?php
                                                echo Not_applicable;
                                                ?>    
                                            </span>
                                            <?php
                                        }
                                        ?>
                    
                    
                    
                </td>
                <td class="rating-td" valign="middle">
                    <div class="rating-div">
                        <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="User Ratings"><i class="fa fa-users"></i></span>
                        <span class="text-golden">
                            <?php
                            $star = $pro_res_data['usr_total'];
                            if (isset($star)) {
                                $exp = explode('.', $star);
                                $point = $exp[1];
                                if ($point > 00) {
                                    $half = '-half-full';
                                } else if ($point == 00) {
                                    $half = '-o';
                                } else {
                                    $half = '';
                                }

                                if ($exp[0] == 1) {
                                    ?>
                                    <i class="fa fa-star"></i><?php ?>
                                    <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                <?php } else if ($exp[0] == 2) { ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                <?php } else if ($exp[0] == 3) { ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                } else if ($exp[0] == 4) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                } else if ($star == 5) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <?php
                                }
                            } else {
                                ?>
                                <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                <?php
                            }
                            ?>  
                        </span>
                    </div>
                    <div class="rating-div">
                        <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="Expert Ratings"><i class="fa fa-user"></i></span>
                        <span class="text-maroon">

                            <?php
                            $star2 = $pro_res_data['exp_total'];
                            if (isset($star2)) {
                                $exp = explode('.', $star2);
                                $point = $exp[1];
                                if ($point > 00) {
                                    $half = '-half-full';
                                } else if ($point == 00) {
                                    $half = '-o';
                                } else {
                                    $half = '';
                                }

                                if ($exp[0] == 1) {
                                    ?>
                                    <i class="fa fa-star"></i><?php ?>
                                    <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                    <?php
                                } else if ($exp[0] == 2) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                } else if ($exp[0] == 3) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                } else if ($exp[0] == 4) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                } else if ($star2 == 5) {
                                    ?>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <?php
                                }
                            } else {
                                ?>
                                <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                <?php
                            }
                            ?>  
                        </span>
                    </div>

                </td>
                <td valign="middle">
                    <label>
                        <input type="checkbox" class="check1" name="compare_car" data-name="<?php echo preg_replace('/\s+/', '-', $pro_res_data['pro_name_comp']) . '-' . $pro_res_data['variant_id']; ?>" value="<?php echo $pro_res_data['variant_id']; ?>">
                    </label>
                </td>
            </tr>
        <?php }
            
        ?>
    </tbody>
</table>
<?php
}
else 
        {
        ?>
        <div class="grey-border filter-box">
        <?php
        echo '<center>No results found</center>';    
        ?>
        </div>
        <?php
        }
?>

<?php echo $this->pagination->create_links(); ?>
<script>
    var trans = '<?php echo empty($post_value['trans']) ? "" : $post_value['trans']; ?>';
    var pro_type = '<?php echo $post_value['pro_type'] ?>';
    var brand = '<?php echo $post_value['brand'] ?>';
    var fuel_id = '<?php echo $post_value['fuel_id'] ?>';
    var min = '<?php echo $post_value['min'] ?>';
    var min2 = '<?php echo $post_value['min2'] ?>';
    var pric_min = '<?php echo $post_value['pric_min'] ?>';
    var pric_max = '<?php echo $post_value['pric_max'] ?>';
    var city = '<?php echo $post_value['city'] ?>';
    var model_id = '<?php echo $post_value['model_id'] ?>';
    $('a.ajax_pagination').on("click", function (e) {
        e.preventDefault();
        var data = {
            trans: trans,
            pro_type: pro_type,
            brand: brand,
            model_id: model_id,
            fuel_id: fuel_id,
            min: min,
            min2: min2,
            pric_min: pric_min,
            pric_max: pric_max,
            city: city,
        }
        $(".overlay-div").toggleClass("active");
        $.ajax({
            type: "GET",
            url: $(this).get(),
            data: data,
            success: function (html) {
                $('#filter_result').html(html);
                $(".overlay-div").toggleClass("active");
            }
        });
        return false;
    });
</script>
