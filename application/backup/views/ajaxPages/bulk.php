<?php

class Bulk extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function bulkrating() {
        $data['title'] = 'Product Gallery';
        $this->load->view('bulk_category', $data);
    }

    function bulk_upload_data() {
        if (isset($_POST['submit'])) {
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            $del_record=$_POST['del_record'];            
            if($del_record==1)
            {    
            $truncate=$this->db->query("truncate expert_rating");                          
            }
            
            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                $added_date = date('Y-m-d h:i:s');
                if ($i > 2) {
                    $brand_name = trim($data[0]);
                    $model_name = trim($data[1]);
                    $variant_name = trim($data[2]);
                    $fuel = trim($data[3]);
                    $rating = trim($data[4]);
                    $profile = trim($data[5]);
                    $source = trim($data[6]);
                    $jornalist_name = trim($data[7]);
                    $heading = trim($data[8]);
                    $what_i_like = trim($data[9]);
                    $what_i_dont_like = trim($data[10]);
                    $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->result_array();
                    $brand_id = $brand[0]['brand_id'];
                    $model = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->result_array();
                    $model_id = $model[0]['model_id'];
                    $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->result_array();
                    $fuel_type_id = $fuel_data[0]['fuel_type_id'];
                    $added_date = date('Y-m-d h:i:s');

                    if ($brand_id != '' && $model_id != "") {
                        $variant = $this->db->query("SELECT `variant_id` FROM `variant` WHERE `model_id`='$model_id'"
                                        . " and `brand_id`='$brand_id' and pro_name='$variant_name' and fuel_type='$fuel_type_id'")->result_array();
                        $ids = '';
                        foreach ($variant as $variant_type_data) {
                            $variant_id = $variant_type_data['variant_id'];
                            $succ = $i;
                            $expert_add = $this->db->query("INSERT INTO `expert_rating`(`expert_rating_id`,`pro_id`,`source`,`profile_id`, "
                                    . "`user_id`, `journilist_name`, `heading`, `what_i_like`, `what_i_dont_like`, `total`, `added_date`, `date`) "
                                    . "VALUES ('','$variant_id','$source','$profile','','$jornalist_name','$heading','$what_i_like','$what_i_dont_like','$rating','$added_date','$added_date')");

                            $insert_id = $this->db->insert_id();
                        }
                        $sucess = $succ . ',' . $sucess;
                    } else {
                        $error = $i . ',' . $error;
                    }
                }
                $i++;
            }
            $error_exp = explode(',', $error);
            $error_exp = array_unique($error_exp);
            $error = implode(",", $error_exp);

            $sucess_exp = explode(',', $sucess);
            $sucess_exp = array_unique($sucess_exp);
            $sucess = implode(",", $sucess_exp);

//            $sucess_exp=explode(',',$sucess);

            $this->session->set_flashdata('Error', "Following Rows of data can't inserted: " . $error);
            $this->session->set_flashdata('Success', "Following Rows of data inserted successfully: " . $sucess);
//            exit;
            fclose($handle);
            ?>
            <script>
                alert('Rating Uploaded Successfully');
            </script>
            <?php

        }
        redirect(base_url() . 'bulkrating');
    }

    function bulkprice() {
        $data['title'] = 'Bulk Price';
        $this->load->view('bulkprice', $data);
    }

    function bulk_upload_price_data() {           
//        echo 'sdssd';
//        exit;        
        if (isset($_POST['submit'])) {
            $del_record=$_POST['del_record'];            
            if($del_record==1)
            {    
            $truncate = $this->db->query("truncate exshowroom_price");
            $truncate = $this->db->query("truncate mnc_exshowroom_price");
            $truncate = $this->db->query("truncate onroad_price");                     
            }
            
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            $chck=0;
            $msg='Error Results:-';
            while(($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                if($i>0){
                    $brand_name=trim($data[0]);
                    $model_name=trim($data[1]);
                    $variant_name=trim($data[2]);
                    $fuel=trim($data[3]);
                    $exshowroom_price = trim($data[4]);
                    $discount_type = 1;
                    
                    $discount_amount = trim($data[5]);
                    
                    $mnc_price = trim($data[6]);
                    $insuranc_price = trim($data[7]);
                    
                    $insuranc_dis_price = trim($data[8]);
                    
                    $lbt_tax = trim($data[9]);
                    $rto = trim($data[10]);                    
                    $pro_name_comp=$brand_name.' '.$model_name.' '.$variant_name;    
                    $handling_cost = trim($data[11]);
                    $onroad_price = trim($data[12]);
                    $grp_discount = trim($data[13]);
//                  $special_rewards = trim($data[14]);
                    $car_exchange_price = trim($data[14]);
//                  $loyalty_bonus = trim($data[16]);
                    $amc = trim($data[15]);
//                  $extened_warranty_cost = trim($data[18]);
                    $delivery_time = trim($data[16]);
                    $sts = 1;
                    $city=trim($data[18]);                    
                    
//                  $cash_discount=trim($data[22]);                    
                    $finance_benefit=trim($data[19]);
                    $exchange_benefit=trim($data[20]);
                    
                    $loyality_bonus=trim($data[21]);
                    
                    $corporate_discount=trim($data[22]);
                    
                    $waivier_handling_charges=trim($data[23]);
                    $accessory_package=trim($data[24]);
                    
                    $accessory_discount=trim($data[25]);
                    
                    $insurance_benefits=trim($data[26]);
                    $extended_warranty=trim($data[27]);
                    $liquidation_support=trim($data[28]);
                    $additional_liquidation_support=trim($data[29]);
//                    $onroad_price=trim($data[34]);
                    $zero_depreciation=trim($data[30]);
                    $special_reward=trim($data[31]);
                    $acc_benefits=trim($data[32]);
                    $total_benefits_per=trim($data[33]);
                    $total_savings=trim($data[34]);
                    $mnc_on_road_price=trim($data[36]);
                    $extended_warranty_beneits=trim($data[37]);
                    $remarks=trim($data[35]);
                    
                    $total_benefits=$finance_benefit+$exchange_benefit+$loyality_bonus+$corporate_discount+$waivier_handling_charges+$accessory_package+$accessory_discount
                            +$insurance_benefits+$extended_warranty+$liquidation_support+$additional_liquidation_support;
                    $new_mnc_price=$exshowroom_price-$discount_amount;
                    
                    $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->row_array();
                    
                    $brand_id = $brand['brand_id'];
                    
                    $model = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->row_array();
                    $model_id = $model['model_id'];
                    
                    $pro_name_comp=$brand_name.' '.$model_name.' '.$variant_name;
                    
                    
//                    $fuel_data = "select fuel_type_id from fuel_type where fuel_type='$fuel'";
                    $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->row_array();
                    $fuel_type_id = $fuel_data['fuel_type_id'];
                        
                    $que="select variant_id from variant where fuel_type='$fuel_type_id' and pro_name_comp='$pro_name_comp'";
                    $variant_res = $this->db->query($que)->row_array();
                    $variant_id=$variant_res['variant_id'];
                    
                    
                    
                    
                    $added_date = date('Y-m-d h:i:s');
                    if(!empty($variant_id)){
                        $check_city = $this->db->query("SELECT `city_id` FROM `city` WHERE `name`='$city'")->row_array();
                        $city_id=$check_city['city_id'];                        
                        if($fuel=='LPG')
                        {//                      
                        }                        
                        $check_mnc_price_upd=$this->db->query("SELECT `city_id` FROM `mnc_exshowroom_price` WHERE `product_id`='$variant_id' and city_id='$city_id'")->num_rows();
                                    if($check_mnc_price_upd == 0 && !is_null($variant_id)){
                                    $temp_array = array(
                                        'product_id' => $variant_id,
                                        'city_id' => $city_id,
                                        'mnc_exshowroom_price' => $mnc_price,
                                        'discount_rs' => $discount_amount,
                                        'status' => $sts,
                                        'row_id' => $i,
                                        'cash_discount' => $discount_amount,
                                        'finance_benefit' => $finance_benefit,
                                        'exchange_benefit' => $exchange_benefit,
                                        'exchange_bonus' => $car_exchange_price,
                                        'loyality_bonus' => $loyality_bonus,
                                        'corporate_discount' => $corporate_discount,
                                        'waivier_handling_charges' => $waivier_handling_charges,
                                        'accessory_package' => $accessory_package,
                                        'accessory_discount' => $accessory_discount,
                                        'insurance_benefits' => $insurance_benefits,
                                        'extended_warranty' => $extended_warranty,
                                        'extended_warranty_benefits' => $extended_warranty_beneits,
                                        'liquidation_support' => $liquidation_support,
                                        'additional_liquidation_support' => $additional_liquidation_support,
                                        'total_benefits' => $total_benefits,
                                        'total_benefits_perc' => $total_benefits_per,
                                        'zero_depreciation' => $zero_depreciation,
                                        'special_reward' => $special_reward,
                                        'acc_benefits' => $acc_benefits,
                                        'total_savings' => $total_savings,
                                        'remarks' => $remarks,
                                        'insurance' => $insuranc_price,
                                        'insurance_discount' => $insuranc_dis_price,
                                        'row_id' => $i,
                                        'added_date' => date('Y-m-d h:i:s'),
                                        'delivery_date' => $delivery_time,
                                    );

                                    $this->db->insert('mnc_exshowroom_price', $temp_array);
                                }
                                else {
                                 $msg.='Sr No. '.$i.'........Product Name '.$pro_name_comp.'<br>';    
                                }                       
                        
                                $check_exprice_upd = $this->db->query("SELECT `city_id` FROM `exshowroom_price` WHERE `product_id`='$variant_id' and city_id='$city_id'")->num_rows();
                                if ($check_exprice_upd == 0 && !is_null($variant_id)) {
                                    $temp_array2 = array(
                                        'product_id' => $variant_id,
                                        'city_id' => $city_id,
                                        'exshowroom_price' => $exshowroom_price,
                                        'status' => $sts,
                                        'last_update' => date('Y-m-d h:i:s'),
                                        'added_date' => date('Y-m-d h:i:s'),
                                        'delivery_date' => $delivery_time,
                                    );

                                    $this->db->insert('exshowroom_price', $temp_array2);
                                }

                                $check_onroad_upd = $this->db->query("SELECT `city_id` FROM `onroad_price` WHERE `variant_id`='$variant_id' and city_id='$city_id'")->num_rows();
                                if ($check_onroad_upd == 0 && !is_null($variant_id)) {
                                    $temp_array3 = array(
                                        'variant_id' => $variant_id,
                                        'city_id' => $city_id,
                                        'comp_insurance' => $insuranc_price,
                                        'lbt' => $lbt_tax,
                                        'registeration_charges' => $rto,
                                        'road_tax' => $rto,
                                        'handling_price' => $handling_cost,
                                        'onroad_price' => $onroad_price,
                                        'mnc_on_road_price' => $mnc_on_road_price,
                                        'added_date' => date('Y-m-d h:i:s')
                                    );

                                    $this->db->insert('onroad_price', $temp_array3);
                                }                        
                    $chck++;    
                    } else {                        
                        $msg.='Sr No. '.$i.'........Product Name '.$pro_name_comp.'<br>';
////                        echo '<br>';
//                        $error = $msg . ',' . $error;
                    }
                    
                }

                $i++;
                
            }
            
            $error_exp = explode(',', $error);
            $tot_error = count($error_exp);
            $error_exp = array_unique($error_exp);
            $error = implode(",", $error_exp);

            $sucess_exp = explode(',', $sucess);
            $tot_sucess = count($sucess_exp);
            $sucess_exp = array_unique($sucess_exp);
            $sucess = implode(",", $sucess_exp);
            
            echo $msg;
//            echo 'Success:- ' . $sucess;
////            
            exit;
            
//            $this->session->set_flashdata('Total_Result', "Following Rows of data can't inserted: " . $i);
//            $this->session->set_flashdata('Total_Success', "Following Rows of data can't inserted: " . $tot_sucess);
//            $this->session->set_flashdata('Total_Fail', "Following Rows of data can't inserted: " . $tot_error);
            $this->session->set_flashdata('Error', "Following Rows of data can't inserted:<br> " . $msg);
//            $this->session->set_flashdata('Success', "Following Rows of data inserted successfully: " . $sucess);
            fclose($handle);
            
        }
        redirect(base_url() . 'bulkprice');
    }

    function bulkhotdeal() {
        $data['title'] = 'Bulk hot Deals';
        $this->load->view('bulkhotdeals', $data);
    }

    function bulkhotdealupload() {

        if (isset($_POST['submit'])) {
            
            
            $del_record=$_POST['del_record'];            
            if($del_record==1)
            {    
            $truncate = $this->db->query("truncate hot_deals");
            }
            
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                $brand_name = trim($data[0]);
                $model_name = trim($data[1]);
                $variant_name = trim($data[2]);
                $fuel = trim($data[3]);
                $city = trim($data[4]);
                $sort = trim($data[5]);
                $percent = trim($data[6]);
                $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->row_array();
                $brand_id=$brand['brand_id'];
                $models = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->row_array();
                $model_id = $models['model_id'];
                $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->row_array();
                $fuel_type_id = $fuel_data['fuel_type_id'];
                $check_city = $this->db->query("SELECT `city_id` FROM `city` WHERE `name`='$city'")->row_array();
                $city_id = $check_city['city_id'];
                if ((!empty($brand_id) && !empty($model_id) && !empty($variant_name))) {
                    echo $variant_name = $variant_name;
                    // echo $variant_name = htmlspecialchars(str_replace('+', '-', $variant_name));
                    
					$variant = $this->db->query("SELECT `variant_id` FROM `variant` WHERE `fuel_type`='$fuel_type_id' and `pro_name`='$variant_name' and `model_id`='$model_id'"
                                    . " and `brand_id`='$brand_id'")->row_array();
                    $variant_id = $variant['variant_id'];
                    $insert_hot_deals=$this->db->query("INSERT INTO `hot_deals`(`hot_deal_id`, `variant_id`, `city_id`, `added_date`,`sort`,`percent`) "
                            . "VALUES ('','$variant_id','$city_id',NOW(),'$sort','$percent')");
                }
            }
        }
        redirect(base_url() . 'bulkhotdeal');
    }

    function bulkgroupdeal() {
        $data['title'] = 'Bulk Group Deals';
        $this->load->view('bulkgrouodeals', $data);
    }

    function bulkgroupdealupload() {
        if (isset($_POST['submit'])) {
            $truncate = $this->db->query("truncate deal");
            $truncate = $this->db->query("truncate brand_deal");
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                echo '<pre>';
//              print_r($data);                
                $brand_name = trim($data[0]);
                $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->row_array();
                $brand_id = $brand['brand_id'];

                $model_name = trim($data[1]);
                $models = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->row_array();
                $model_id = $models['model_id'];


                $variant_name = trim($data[2]);
                $fuel = trim($data[3]);
                $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->row_array();
                $fuel_type_id = $fuel_data['fuel_type_id'];


                $city = trim($data[4]);
                $check_city = $this->db->query("SELECT `city_id` FROM `city` WHERE `name`='$city'")->row_array();
                $city_id = $check_city['city_id'];


                $brand_disc = trim($data[5]);
                $start_date = trim($data[6]);
                $end_date = trim($data[7]);
                $variant_disc = trim($data[8]);
                $disc_heading = trim($data[9]);
                $variant_start_data = trim($data[10]);
                $variant_end_data = trim($data[11]);
                $term_condition = trim($data[12]);
                $date_added = date('Y-m-d');
//                
                if ((!empty($brand_id) && !empty($model_id) && !empty($variant_name))) {
                    $variant_name = htmlspecialchars(str_replace('+', '-', $variant_name));
                    
                    
                    $variant = $this->db->query("SELECT `variant_id` FROM `variant` WHERE `fuel_type`='$fuel_type_id' and `pro_name`='$variant_name' and `model_id`='$model_id'"
                            . " and `brand_id`='$brand_id'");

                    $variant_result = $variant->row_array();
                    $variant_count = $variant->num_rows();
                    $variant_id = $variant_result['variant_id'];
                    
                    
                        $check_variant_deal=$this->db->query("select deal_id from deal where brand_id='$brand_id' and variant_id='$variant_id' and city_id='$city_id'")->num_rows();
                        if($check_variant_deal==0)
                        {    
                        if(strpos($variant_disc, '%') !== false) {
                            $type='1'; 
                        }
                        else
                        {
                            $type='2'; 
                        }
                    
                        
                        $v_g_d=$this->db->query("INSERT INTO `deal`"
                                . "(`deal_id`, `variant_id`, `brand_id`, `city_id`, `discount_type`, `deals_heading`, `amount_per`,"
                                . " `start_date`, `end_date`, `status`, `term_condition`, `added_date`) VALUES "
                                . "('','$variant_id','$brand_id','$city_id','$type','$disc_heading','$variant_disc','$variant_start_data',"
                                . "'$variant_end_data','1','$term_condition','$date_added')");
                        
                        } 
                        
                    $brand_count = $this->db->query("select brand_id from brand_deal where brand_id='$brand_id'")->num_rows();

                    if($brand_count == 0) {
                        if(strpos($brand_disc, '%') !== false) {
                            $brand_type='1'; 
                        }
                        else
                        {
                            $brand_type='2'; 
                        }
                        $brand_group_deal = $this->db->query("INSERT INTO `brand_deal`(`deal_id`, `brand_id`, `city_id`, `discount_type`, `deals_heading`, `amount_per`,"
                                        . " `start_date`, `end_date`, `status`, `term_condition`, `added_date`) "
                                        . "VALUES ('','$brand_id','$city_id','$brand_type','$disc_heading','$brand_disc','$start_date','$end_date','1','$term_condition','$date_added')");
                    }
                        
                        
                        
                    
                }
            }
            echo '</pre>';
        }

        redirect(base_url() . 'bulkgroupdeal');
    }

}
