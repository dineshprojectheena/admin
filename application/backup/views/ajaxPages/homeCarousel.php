<?php
$count_data = count($data);
$division = $count_data / 3;
$exp_res = explode('.', $division);
if (!empty($data)) {
    $remaing = $exp_res[0];
    if (isset($exp_res[1]) > 0) {
        $remaing++;
    }
    $data['session'] = $this->session->userdata("logged_in_user");
    $user_id = $data['session']['user_id'];
    if ($user_id == '') {
        $user_id = '0';
    }
    for ($j = 1, $i = 0, $k = 1, $f = 2; $j <= $remaing; $j++) {
        if ($j == 1) {
            $class = 'active';
        } else {
            $class = '';
        }
        ?>        
        <div class="item change_car_res <?php echo $class; ?>" >
            <?php
            for ($i; $i <= $f; $i++, $k++) {
                if (isset($data[$i]['pro_image'])) {
                    $mnc_exshow_room = $this->home_feature->home_page_carousel_exshow($data[$i]['variant_id']);
                    $total_saving = $this->home_feature->get_total_saving($data[$i]['variant_id'], $user_id);
                    ?>
                    <div class="col-md-4 col-xs-12 col-cust-both">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="ribbin">	
                                    <div class="corner">
                                        <?php
                                        if (!empty($data[$i]['total_benefits_perc'])) {
                                            ?>
                                            <h2><?php echo $data[$i]['total_benefits_perc'] . '&nbsp;&nbsp;&nbsp;&nbsp;'; ?></h2>
                                            <?php
                                        }
                                        ?>
                                        <div class="group-item">
                                            <div class="col-md-12 ">
                                                <div class="img-holder col-md-12 col-cust-both">
                                                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $data[$i]['pro_name_comp']) . '-' . $data[$i]['variant_id']) ?>" >        
                                                        <img class="img-responsive" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $data[$i]['pro_image'])); ?>"  src="<?php echo img_url('uploads/product/' . $data[$i]['pro_image']); ?>"/>
                                                    </a>                                                    
                                                </div>
                                            </div>    
                                            <div class="group-content">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4 class="hot_dealreview_title"> 
                                                                <?php
                                                                echo $data[$i]['pro_name_comp'];
                                                                ?>
                                                            </h4>
                                                        </div> 
                                                        <div class="col-md-12">
                                                            <div class="group-price">
                                                                <div class="mnc-price">
                                                                    <span class="price-span">
                                                                        Ex-Showroom Price
                                                                    </span>
                                                                    <span class="text-maroon">
                                                                        <i class="fa fa-rupee"></i>
                                                                        <?php
                                                                        if (indianFormatNumber($mnc_exshow_room[0]['mnc_exshowroom_price']) == 0) {
                                                                            echo Not_applicable;
                                                                        } else {
                                                                            echo indianFormatNumber($mnc_exshow_room[0]['mnc_exshowroom_price']);
                                                                        }
                                                                        ?>  
                                                                    </span>
                                                                </div>     
                                                            </div>
                                                        </div>    
                                                        <div class="col-md-12" >
                                                            <span class="price-span">Total Savings </span>
                                                            <span class="price-span text-maroon"><i class="fa fa-rupee"></i>
                                                                <?php
                                                                if (indianFormatNumber($mnc_exshow_room[0]['total_savings']) == 0) {
                                                                    echo '0';
                                                                } else {
                                                                    echo indianFormatNumber($mnc_exshow_room[0]['total_savings']);
                                                                }
                                                                ?> 
                                                            </span>
                                                        </div>  
                                                        <div class="col-md-12">
                                                            <span class="price-span">Valid Until </span>
                                                            <span class="price-span text-maroon">
                                                                <?php
                                                                if (!empty($data[$i]['valid_until'])) {
                                                                    echo $data[$i]['valid_until'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?> 
                                                            </span>
                                                        </div>  
                                                        <div class="col-md-12">
                                                            <div class="pull-left">
                                                                <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carinsurance_hotdeals.png' ?>" data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Insurance Offers"/>
                                                                <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carfinance_hotdeals.png' ?>"  data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Finance Offers"/>			
                                                                <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carexchange.png' ?>" data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Exchange  Offers"/>                       
                                                                <img src="<?php echo base_url() . 'assets/img/product/otherbenefits.png' ?>"data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="top" title="<?php echo $data[$i]['accessory_package']; ?>"/>
                                                            </div>
                                                            <div class="pull-right">
                                                                <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $data[$i]['pro_name_comp']) . '-' . $data[$i]['variant_id']) ?>" class="btn btn-xs carsuel_book">Book</a>
                                                            </div>
                                                        </div>
                                                    </div>      
                                                </div>
                                            </div>
                                        </div>
                                    </div>      
                                </div>      
                            </div>
                        </div>
                    </div>

                    <?php
                }
            }
            ?>
        </div>
        <?php
        $f = $f + 3;
    }
} else {
    ?>
    <div align="center" class="no_carousel col-xs-12">
        <img src="<?php echo base_url() . 'assets/img/ComingSoon.png' ?>" />
    </div>
    <?php
}
?>




