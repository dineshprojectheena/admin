<div class="table-responsive">
    <?php
//    echo '<pre>';
//    print_r($pro_feture);
    $colors = array();
    $exteriors = array();
    $interiors = array();
    $comfort = array();
    $safety = array();
    $entertainment = array();
    $service = array();
    $other = array();
    foreach ($pro_feture as $pro_feture_data) {

        if ($pro_feture_data['feature_name'] != 'Color') {

            if ($pro_feture_data['feature_type'] == '1') {
                $exteriors[] = $pro_feture_data;
            }
            if ($pro_feture_data['feature_type'] == '2') {
                $interiors[] = $pro_feture_data;
            }
            if ($pro_feture_data['feature_type'] == '3') {

                $comfort[] = $pro_feture_data;
            }
            if ($pro_feture_data['feature_type'] == '4') {
                $safety[] = $pro_feture_data;
            }
            if ($pro_feture_data['feature_type'] == '5') {
                $entertainment[] = $pro_feture_data;
            }
            if ($pro_feture_data['feature_type'] == '6') {
                $service[] = $pro_feture_data;
            }
            if ($pro_feture_data['feature_type'] == '7') {
                $other[] = $pro_feture_data;
            }
        } else {
//            print_r($pro_feture_data);
            $colors[] = $pro_feture_data;
        }
    }
//    echo '</pre>';
    ?>    
    <div class="table-responsive">
        <table class="table table-striped table-condensed">
            <tr>
                <td class="mob_head" colspan="2">Interiors</td>
            </tr>
            <?php
            foreach ($interiors as $interiors_data) {
                ?>
                <tr>
                    <td class="tbl_wid_1"><?php print_r($interiors_data['feature_name']); ?></td>
                    <td class="tbl_wid_2">
                        <?php
                        echo strtoupper($interiors_data['feature_desc']);
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

            <tr>
                <td class="mob_head" colspan="2">Exteriors</td>
            </tr>
            <?php
            foreach ($exteriors as $exteriors_data) {
                ?>
                <tr>
                    <td class="tbl_wid_1"><?php print_r($exteriors_data['feature_name']); ?></td>
                    <td class="tbl_wid_2">
                        <?php
                        echo strtoupper($exteriors_data['feature_desc']);
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

            <tr>
                <td class="mob_head" colspan="2">Comfort</td>
            </tr>
            <?php
            foreach ($comfort as $comfort_data) {
                ?>
                <tr>
                    <td class="tbl_wid_1"><?php print_r($comfort_data['feature_name']); ?></td>
                    <td class="tbl_wid_2">
                        <?php
                        echo strtoupper($comfort_data['feature_desc']);
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

            <tr>
                <td class="mob_head" colspan="2">Safety</td>
            </tr>
            <?php
            foreach ($safety as $safety_data) {
                ?>
                <tr>
                    <td class="tbl_wid_1"><?php print_r($safety_data['feature_name']); ?></td>
                    <td class="tbl_wid_2">
                        <?php
                        echo strtoupper($safety_data['feature_desc']);
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

            <tr>
                <td class="mob_head" colspan="2">Entertainment</td>
            </tr>
            <?php
            foreach ($entertainment as $entertainment_data) {
                ?>
                <tr>
                    <td class="tbl_wid_1"><?php print_r($entertainment_data['feature_name']); ?></td>
                    <td class="tbl_wid_2">
                        <?php
                        echo strtoupper($entertainment_data['feature_desc']);
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>


            <tr>
                <td class="mob_head" colspan="2">Entertainment</td>
            </tr>
            <?php
            foreach ($entertainment as $entertainment_data) {
                ?>
                <tr>
                    <td class="tbl_wid_1"><?php print_r($entertainment_data['feature_name']); ?></td>
                    <td class="tbl_wid_2">
                        <?php
                        echo strtoupper($entertainment_data['feature_desc']);
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>



<!--            <tr>
                <td class="mob_head" colspan="2">Safety</td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Side Door Impact Beams</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Seatbelt Reminder</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">ESP</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">ABS</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">EBD</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Brake Assist</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Parking Sensors</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Number of AirBags</td>
                <td class="tbl_wid_2">
                    4
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Seatbelt Pretensioners</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Engine Immobilizer</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Traction Control</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Parking Camera</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>

            <tr>
                <td class="mob_head" colspan="2">Exteriors</td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Adaptive Headlights</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Daytime Running Lights</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">LED Lights</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Front Fog Lamps</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Turn Indicators on ORVMs</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Wheel Cover</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Alloy Wheels</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Metallic Paint</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Body Coloured Bumpers</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Body Coloured Mirrors</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Rear Wiper with Washer</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Body Coloured Door Handles</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Chrome Door Handles</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>

            <tr>
                <td class="mob_head" colspan="2">Entertainment</td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Touchscreen Display</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Audio System (with Radio AM/FM)</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">USB Port</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Aux Port</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Bluetooth &amp; Streaming</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Multi-CD Player</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Satellite Navigation</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Single CD Player</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">MP3 player</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Steering Mounted Audio Controls</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Hands Free Telephony</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">iPod Compatability</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Speakers</td>
                <td class="tbl_wid_2">
                    4
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Internal Hard Drive Memory</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Voice Activated Controls</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>

            <tr>
                <td class="mob_head" colspan="2">Comfort</td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Air Conditioning</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Climate Control</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Power Windows</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Power Adjustable ORVMs</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Tilt Adjustable Steering Column-Rake</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Tilt Adjustable Steering Column-Reach</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Central Locking</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Remote Locking</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Keyless Entry</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Key Reminder</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Driver Seat Adjust (Electrically)</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Driver Seat Adjust (Manual)</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Passenger Seat Adjust (Manual)</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Driver Lumbar Support</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Co-Driver Lumbar Support</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Bottle Holder</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Total Cupholders</td>
                <td class="tbl_wid_2">
                    2
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Front Power Outlet</td>
                <td class="tbl_wid_2">
                    Yes
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Cigarette Lighter</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Rear Defogger</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Rear AC Vents</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Rear Seat Centre Armrest</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Rain Sensing Wipers</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Adaptive Suspension</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Hill Start Assist</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Automatic Start-Stop Function</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Electric Handbrake</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Headlamp Washers</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Mechanical Sunroof</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Electric Sunroof</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Cruise Control</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>
            <tr>
                <td class="tbl_wid_1">Paddle Shift</td>
                <td class="tbl_wid_2">
                    No
                </td>
            </tr>-->
            </tbody>
        </table>

    </div>
