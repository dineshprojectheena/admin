<div class="table-responsive">
    <?php    
//    echo '<pre>';
//    print_r($pro_specification); 
//    echo '</pre>';
    ?>    
    <table class="table table-striped table-condensed">
        <tr>
            <td class="mob_head" colspan="2">Engine</td>
        </tr>

        <tr>
            <td class="tbl_wid_3">Fuel type</td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['fuel_type'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Displacement (cc)</td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['displacement'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">No of Cylinders</td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['cylinders'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Emission Norms</td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['engine_emission'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Power (bhp)
            </td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['engine_maximum_power'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Torque (Nm)</td>
            <td class="tbl_wid_3">
                 <?php echo $pro_specification['engine_maximum_torque'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">IDC Mileage (km/litre)</td>
            <td class="tbl_wid_3">
                 <?php echo $pro_specification['mileage'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Dimensions</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Length (mm)
            </td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['dimensions_mm_length'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Width (mm)
            </td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['dimensions_mm_width'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Height (mm)
            </td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['dimensions_mm_width'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Wheelbase (mm)
            </td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['dimensions_mm_weelbase'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Rear seat legroom (cm)
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['rear_seat_legroom'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Headroom (cm)
            </td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['headroom'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Seat Width (cm)
            </td>
            <td class="tbl_wid_3">
            <?php echo $pro_specification['seat_width'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Ground clearance (mm)
            </td>
            <td class="tbl_wid_3">
            <?php echo $pro_specification['dimensions_mm_ground_clearance'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Steering</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Steering Type
            </td>
            <td class="tbl_wid_3">
             <?php echo $pro_specification['streeing_type'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Turning Radius (m)
            </td>
            <td class="tbl_wid_3">
            <?php echo $pro_specification['streeing_turn_radius'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Transmission</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Transmission Type
            </td>
            <td class="tbl_wid_3">
            <?php echo $pro_specification['tramission_type'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">No of gears
            </td>
            <td class="tbl_wid_3">
             <?php echo $pro_specification['no_gear'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Drivetrain
            </td>
            <td class="tbl_wid_3">
             <?php echo $pro_specification['drivetrain'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Suspension</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Suspension front
            </td>
            <td class="tbl_wid_3">
            <?php echo $pro_specification['suspension_front'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Suspension rear
            </td>
            <td class="tbl_wid_3">
             <?php echo $pro_specification['suspension_rear'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Tyres</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Tire Size
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['tyre_size'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Wheel Size
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['wheel_size'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Spare Tyre
            </td>
            <td class="tbl_wid_3">
              <?php echo $pro_specification['spare_type'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Brakes</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Brakes Front
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['brakes_front'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Brakes Rear
            </td>
            <td class="tbl_wid_3">
                Drum
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Performance</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Top Speed (KMPH)
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['top_speed'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Acceleration ( 0-100 kmph)
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['acceleration'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Capacity</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Seating capacity
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['seating_capacity'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Trunk Volume (l)
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['trunk_volume'];?>
            </td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Fuel Tank (l)
            </td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['fuel_tank_capacity1'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Weight</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Kerb Weight (Kgs)
            </td>
            <td class="tbl_wid_3">
                <?php echo $pro_specification['kerb_weight'];?>
            </td>
        </tr>

        <tr>
            <td class="mob_head" colspan="2">Warranty</td>
        </tr>
        <tr>
            <td class="tbl_wid_3">Standard Warranty
            </td>
            <td class="tbl_wid_3">
               <?php echo $pro_specification['standard_warranty'];?>
            </td>
        </tr>
    </table>

</div>
