<script>
    function check_fuels()
    {
        fuel_name = $('input.fuel-checkbox:checked').val();
        fuel_details = $('input.fuel-checkbox:checked').data('value');
        $("#fuel_id").val(fuel_name);
        $("#fuel_id_data").val(fuel_details);
        car_type_selection();
    }

    function reset_price()
    {
        car_type_selection('0');
        $('#collapseSix').removeClass();
        $('#collapseSix').addClass('collapse');
    }



    function check_price()
    {
        var regX = /(<([^>]+)>)/ig;
        var html3 = $('#pric_min').html();
        var pric_min = html3.replace(/[^0-9]/g, '');
        var regX = /(<([^>]+)>)/ig;
        var html4 = $('#pric_max').html();
        var pric_max = html4.replace(/[^0-9]/g, '');
        $('#s3').css({"display": "block", "float": "left", "margin-left": "5px"});
        $("#s4").html('Min-' + pric_min + ' Max-' + pric_max);
        $('#f_price').val(pric_min + '-' + pric_max);
        car_type_selection(i = '2');
    }

    loadcss();
    function loadcss()
    {
        var low = '<?php print_r($exp_price[0]) ?>';
        var high = '<?php print_r($exp_price[1]) ?>';
        var model1 = $("#model_id").val();
        var brand_id2 = $("#brand_id2").val();
        var brand = "";
        var model = "";
        var brand = $('input.brand-checkbox:checked').data('name');
        if (brand_id2 == brand)
        {
            var model2 = $("input.model-checkbox:checked").data('name');
            if (typeof model2 === 'undefined') {
                var model = model1;
//    $("#model_id").val('');
            }
            else
            {
                var model = model2;
                $("#model_id").val(model);
            }
        }
        else
        {
            $("#model_id").val('');
        }
//    alert(brand);
//    alert(model);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('search/car_type_selection_model') ?>",
            data: {brand: brand, model: model}
        }).done(function (html) {
//            alert(html);
//            exit;
            if (typeof brand !== 'undefined') {
                $("#car_brand_result").html(html);
            }
            else
            {
                $("#car_brand_result").html("Select Brand First");
            }
        });
    }

    car_type_selection();
    function car_type_selection(i)
    {
        $(".overlay-div").toggleClass("active");
        var cond = '<?php echo $cond; ?>';
        var pro_type = $('#pro_type').val();
        var f_price = $('#f_price').val();
        var brand = $('#brand_id').val();
        var def_model_id = $('#model_id').val();
        var min = '';
        var min2 = '';

        if (i == '1')
        {
            var regX = /(<([^>]+)>)/ig;
            var html = $('#min').html();
            var min = html.replace(/[^0-9]/g, '');
            var regX = /(<([^>]+)>)/ig;
            var html2 = $('#max').html();
            var min2 = html2.replace(/[^0-9]/g, '');
            $('#s1').css({"display": "block", "float": "left", "margin-left": "5px"});
            $("#s2").html('Min-' + min + ' Max-' + min2);
        }

        if (pro_type != '')
        {
            $('#t1').css({"display": "block", "float": "left", "margin-left": "5px"});
            $("#t2").html('Car Type-' + pro_type);
        }

        if (i == '2' || f_price != '')
        {
            if (i == '2')
            {
                var regX = /(<([^>]+)>)/ig;
                var html3 = $('#pric_min').html();
                var pric_min = html3.replace(/[^0-9]/g, '');
                var regX = /(<([^>]+)>)/ig;
                var html4 = $('#pric_max').html();
                var pric_max = html4.replace(/[^0-9]/g, '');
                $('#s3').css({"display": "block", "float": "left", "margin-left": "5px"});
                $("#s4").html('Min-' + pric_min + ' Max-' + pric_max);
                $('#f_price').val(pric_min + '-' + pric_max);
            }
            if (f_price != '')
            {
                var pro_price_split = f_price.split("-");
                var pric_min = (pro_price_split[0]);
                var pric_max = (pro_price_split[1]);
                $('#s3').css({"display": "block", "float": "left", "margin-left": "5px"});
                $("#s4").html('Min-' + pric_min + ' Max-' + pric_max);
                $('#f_price').val(pric_min + '-' + pric_max);
            }
        }
        var trans = '';
        if (i == '4')
        {
            var trans_data = "";
            trans = $("input.trans_type:checked").data('name');
            trans_data = $("input.trans_type:checked").val();
            $('#s6').css({"display": "block", "float": "left", "margin-left": "5px"});
            if (trans == 0)
            {
                trans = '';
            }
//            alert(trans);
            $("#trans_data_res").val(trans);
            $("#s7").html(trans_data);
            var trans = $("#trans_data_res").val();
        }
        else
        {
            var trans_data = "";
            trans = $("input.trans_type:checked").data('name');
            trans_data = $("input.trans_type:checked").val();
//            alert(trans);

            if ((typeof trans !== 'undefined') || (typeof trans !== 'null'))
            {
                $('#s6').css({"display": "none", "float": "left", "margin-left": "5px"});
                if (trans == 0)
                {
                    trans = '';
                }
                $("#trans_data_res").val(trans);
                $("#s7").html(trans_data);
                var trans = $("#trans_data_res").val();
            }
        }

        if (brand != '' || i == '5')
        {
            var brand_name = "";
            var brands = "";
            brands = $('input.brand-checkbox:checked').data('name');
            brand_name = $('input.brand-checkbox:checked').val();
            $('#b1').css({"display": "block", "float": "left", "margin-left": "5px"});
            $("#b2").html(brand_name.replace(/<(?:.|\n)*?>/gm, ''));
            $("#brand_id").val(brands);
            var brand = brands;
            
        }
       
        var model_id = $('input.model-checkbox:checked').data('name');   
        
       if(typeof model_id === 'undefined'){
           model_id=def_model_id;
        }; 
        
        var fuel_id = $('#fuel_id').val();
        var fuel_id_data = $('#fuel_id_data').val();
        if (fuel_id != '')
        {
            $('#f1').css({"display": "block", "float": "left", "margin-left": "5px"});
            $("#f2").html(fuel_id_data);
        }
        else
        {
            $('#f1').css({"display": "none", "float": "left", "margin-left": "0px"});
            $("#f2").html('');
        }
        var city = "<?php echo $main_city = $this->input->cookie('main_city', TRUE); ?>";

//        alert(model_id);
//        alert(pro_type);


        $.ajax({
            type: "POST",
            url: "<?php echo site_url('search/car_type_selection_data') ?>",
            data: {trans: trans, pro_type: pro_type, brand: brand, fuel_id: fuel_id, min: min, min2: min2, city: city, pric_min: pric_min, pric_max: pric_max, model_id: model_id}
        }).done(function (html) {
//            alert(html);
            $("#filter_result").removeData();
            $("#filter_result").html(html);
//            alert(html);
            $(".overlay-div").toggleClass("active");
        });
    }

    $('#example_length').css("display", "none");
</script>