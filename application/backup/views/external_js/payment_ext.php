<script>
    $('#left_sidebar').css("display", "none");
    $('.alert').css("display", "none");
    $('.slider-sidebar').css("display", "none");
    $('.step-div').css("display", "none");
//        $("#left_sidebar").removeClass("col-md-9 col-sm-6 col-xs-12");
//        $("#left_sidebar").addClass('col-md-12 col-sm-12 col-xs-12');
    
    function gateway()
    {
//    alert('1');  
//    $(".payment_gateway").toggle();
    $("#checkout").click();
    }
    
    function same_data()
    {
        var check = $('input[name="shipping_diff"]:checked').val();
        if (check == '1')
        {
            $("[name='name']").val($("[name='ship_name']").val());
            $("[name='l_name']").val($("[name='ship_l_name']").val());
            $("[name='address']").val($("[name='ship_address']").val());
            $("[name='city']").val($("[name='ship_city']").val());
            $("[name='state']").val($("[name='ship_state']").val());
            $("[name='postal_code']").val($("[name='ship_postal_code']").val());
            $("[name='country']").val($("[name='ship_country']").val());
            $("[name='email']").val($("[name='ship_email']").val());
            $("[name='phone']").val($("[name='ship_phone']").val());
        }
        else
        {
            $("[name='name']").val('');
            $("[name='l_name']").val('');
            $("[name='address']").val('');
            $("[name='city']").val('');
            $("[name='state']").val('');
            $("[name='postal_code']").val('');
            $("[name='country']").val('');
            $("[name='email']").val('');
            $("[name='phone']").val('');
        }

    }
    loadamount();
    function loadamount()
    {
        $('#configuration_button').css("display", "none");
        var grand_total = $('#grand_total').val();
        var tenperct = grand_total * 10 / 100;
//        $('#tot_price1').val('<?php echo trans_amt;?>');
//        $('#fewamaount').val('<?php echo trans_amt;?>');
//        $('#ten_perct').html('<?php echo trans_amt;?>');
//        $('#amount').val('<?php echo trans_amt;?>');
//        $('#on_road_price').val($('#grand_total').val());
        var days_data = $('#days_data').html();
        var dis_price = $('#dis_price').html();
        var group_discount = dis_price.replace(/[^0-9]/g, '');
        $('#group_discount').val(group_discount);
        //        alert(group_discount);
    }

    function check_price()
    {
        var days = $('#days').val();
        var grand_total = $('#grand_total').val();
        $('#tot_price1').val(grand_total);
        $('#tot_days1').val(days);
    }

    function backtoconfigure()
    {
        window.location.href = "<?php echo base_url() . 'car-booking/' . $main_id; ?>";
    }
    
    /*
    function validate() {

        var frm = document.frmTransaction;
        var aName = Array();
        aName['account_id'] = 'Account ID';
        aName['reference_no'] = 'Reference No';
        aName['amount'] = 'Amount';
        aName['description'] = 'Description';
        aName['name'] = 'Billing Name';
        aName['address'] = 'Billing Address';
        aName['city'] = 'Billing City';
        aName['state'] = 'Billing State';
        aName['postal_code'] = 'Billing Postal Code';
        aName['country'] = 'Billing Country';
        aName['email'] = 'Billing Email';
        aName['phone'] = 'Billing Phone Number';
        aName['ship_name'] = 'Shipping Name';
        aName['ship_address'] = 'Shipping Address';
        aName['ship_city'] = 'Shipping City';
        aName['ship_state'] = 'Shipping State';
        aName['ship_postal_code'] = 'Shipping Postal code';
        aName['ship_country'] = 'Shipping Country';
        aName['ship_phone'] = 'Shipping Phone';
        aName['return_url'] = 'Return URL';


        for (var i = 0; i < frm.elements.length; i++) {
//            if ((frm.elements[i].value.length == 0)) {
//                if ((frm.elements[i].name == 'country'))
//                    alert("Select the " + aName[frm.elements[i].name]);
//                else
//                    alert("Enter the " + aName[frm.elements[i].name]);
//                frm.elements[i].focus();
//                return false;
//            }
            if (frm.elements[i].name == 'account_id') {

                if (!validateNumeric(frm.elements[i].value)) {
                    alert("Account Id must be NUMERIC");
                    frm.elements[i].focus();
                    return false;
                }
            }

            if (frm.elements[i].name == 'amount') {
                if (!validateNumeric(frm.elements[i].value)) {
                    alert("Amount should be NUMERIC");
                    frm.elements[i].focus();
                    return false;
                }
            }
            if ((frm.elements[i].name == 'postal_code'))
            {
                if (!validateNumeric(frm.elements[i].value)) {
                    alert("Postal code should be NUMERIC");
                    frm.elements[i].focus();
                    return false;
                }
            }

            if ((frm.elements[i].name == 'phone')) {
                if (!validateNumeric(frm.elements[i].value)) {
                    alert("Enter a Valid CONTACT NUMBER");
                    frm.elements[i].focus();
                    return false;
                }
            }



            if ((frm.elements[i].name == 'name'))
            {

                if (validateNumeric(frm.elements[i].value)) {
                    alert("Enter your Name");
                    frm.elements[i].focus();
                    return false;
                }
            }


            if (frm.elements[i].name == 'ship_postal_code') {
                if (!validateNumeric(frm.elements[i].value)) {
                    alert("Postal code should be NUMERIC");
                    frm.elements[i].focus();
                    return false;
                }
            }



            if (frm.elements[i].name == 'email') {
                if (!validateEmail(frm.elements[i].value)) {
                    alert("Invalid input for " + aName[frm.elements[i].name]);
                    frm.elements[i].focus();
                    return false;
                }
            }

        }
        return true;
    }
    */

    function validateNumeric(numValue) {
        if (!numValue.toString().match(/^[-]?\d*\.?\d*$/))
            return false;
        return true;
    }

    function validateEmail(email) {
        //Validating the email field
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //"
        if (!email.match(re)) {
            return (false);
        }
        return(true);
    }


    Array.prototype.inArray = function (value)
// Returns true if the passed value is found in the
// array.  Returns false if it is not.
    {
        var i;
        for (i = 0; i < this.length; i++) {
            // Matches identical (===), not just similar (==).
            if (this[i] === value) {
                return true;
            }
        }
        return false;
    };

</script>