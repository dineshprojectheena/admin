<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    function set_redirection(a)
    {
        var d=new Date();
        d.setTime(d.getTime()+(1 * 24 * 60 * 60 * 1000 * 30));
        
        
        
        var expires = "expires=" + d.toUTCString();
        document.cookie = 'redirect=' + a + ';' + expires + ';path=/';
    }
    
    function check_before_change()
    {
        var brand_data = $("#brand_data").val();
        var brand_model = $("#brand_model").val();
        var car_variant = $("#car_variant").val();
        if (brand_data != '' && brand_model != '' && car_variant != '')
        {
            $('#chgane_car_button').click();
        }
        else
        {
            alert('Please Select Brand,Model & variant');
        }
    }
    function delete_config(a)
    {
        if (confirm("Are you sure you want to delete this configuration?") == true) {
            window.location.href = a;
        }
    }
    $(function (){
        $("#test_date").datepicker({dateFormat: 'dd-mm-yy'});
    });
    function check_before_change2()
    {
        var brand_data = $("#test_brand_data").val();
        var brand_model = $("#test_brand_model").val();
        var test_full_name = $("#test_full_name").val();
        var test_date = $("#test_date").val();
        var test_fuel_type = $("#test_fuel_type").val();
        var test_email = $("#test_email").val();
        var test_phone = $("#test_phone").val();
        var test_location = $("#test_location").val();
        if (brand_data != '' && brand_model != '' && test_full_name != '' && test_email != '' && test_phone != '' && test_fuel_type != '' && test_date != '' && test_location != '')
        {
            return;
        }
        else
        {
            alert('Please fill all details!');
            return false;
            exit;
        }
    }
    function select_fianance()
    {
        var mnc_ex_price = $("#mnc_ex_price").val();
        var mnc_ex_price = mnc_ex_price.replace(/[^0-9]+/gi, '');
        var max_finn = $("#max_finn").html();
        var max_finn_data = max_finn.replace(/<\/?(span|div|img|p...)\b[^<>]*>/g, "")
        var max_finn1 = max_finn_data.replace(/[^0-9]+/, '');
        var max_finn1 = max_finn_data.replace(/[^0-9]+/, '');
        var res = parseInt(max_finn1.replace("%", ""));
        var doun_pay = mnc_ex_price * (res / 100);
        var doun_pay = doun_pay.toFixed(0);
        var loan1 = price_conver(doun_pay);
        $("#loan1").val(loan1);
        var loan_amount = mnc_ex_price - doun_pay;
        var loan_amount = loan_amount.toFixed(0);
        var loan_amount = price_conver(loan_amount);
        $("#down_payment").val(loan_amount);
        emi();
    }

    function price_conver(x)
    {
        x = x.toString();
        var afterPoint = '';
        if (x.indexOf('.') > 0)
            afterPoint = x.substring(x.indexOf('.'), x.length);
        x = Math.floor(x);
        x = x.toString();
        var lastThree = x.substring(x.length - 3);
        var otherNumbers = x.substring(0, x.length - 3);
        if (otherNumbers != '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;

        return res;
    }

    function setbankper()
    {
        var bank = $("#bank").val();
        $("#rate1").val(bank);
        emi();
    }
    function emi() {
        if (document.getElementById('loan1').value == null || document.getElementById('loan1').value.length == 0 || document.getElementById('months1').value == null || document.getElementById('months1').value.length == 0 || document.getElementById('rate1').value == null || document.getElementById('rate1').value.length == 0) {
            document.getElementById('pay1').value = "All Data Required.....";
        } else {
            var pay1 = '';
            var princ1 = document.getElementById('loan1').value;
            var princ1 = princ1.replace(/[^0-9]+/gi, '');
            var term1 = document.getElementById('months1').value;

            var intr1 = document.getElementById('rate1').value / 1200;
            var EMIPart1 = [princ1 * intr1 * (Math.pow((1 + intr1), term1))];
            var EMIPart2 = ((Math.pow((1 + intr1), term1)) - 1);
            var EMI = EMIPart1 / EMIPart2;
            var EMIs = round2Fixed(EMI);
            //alert(EMIs);
            var EMIs = price_conver(EMIs);
            $('#pay1').val(EMIs);
        }
    }

    function round2Fixed(value) {
        value = +value;

        if (isNaN(value))
            return NaN;

        // Shift
        value = value.toString().split('e');
        value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + 2) : 2)));

        // Shift back
        value = value.toString().split('e');
        return (+(value[0] + 'e' + (value[1] ? (+value[1] - 2) : -2))).toFixed(2);
    }


    function setemi()
    {
        var emi = $("#pay1").val();
        var down_payment = $("#down_payment").val();
        var loan1 = $("#loan1").val();
        var bank = $("#bank").val();
        var rate1 = $("#rate1").val();
        var months1 = $("#months1").val();
        var pay1 = $("#pay1").val();
        var pro_id = $("#rating_variant").val();
        if (emi == 'All Data Required.....')
        {
            alert('Please set all emi related data....');
        }
        else
        {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('common/emi_pro') ?>",
                data: {emi: emi, pro_id: pro_id, down_payment: down_payment, loan1: loan1, bank: bank, rate1: rate1, months1: months1}
            }).done(function (html) {
                location.reload();
            });
        }
    }
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.flexslider-min.js"></script>

<?php //if (!isMobile()): ?>
    <script type="text/javascript">
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                z._.push(c)
            }, $ = z.s =
                    d.createElement(s), e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set.
                        _.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute('charset', 'utf-8');
            $.src = '//v2.zopim.com/?2eUW1w2TRcnrEhA598PHZNPSw2o0ggMn';
            z.t = +new Date;
            $.
                    type = 'text/javascript';
            e.parentNode.insertBefore($, e)
        })(document, 'script');
    </script>
<?php //endif; ?>
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 959088565;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:none;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/959088565/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script>
    function checkemi()
    {
        var rate1 = $('#rate1').val();
//    alert(rate1);
        if (rate1 > 20)
        {
            alert("Interest Rate cannot be above 20%");
            $('#rate1').val('');
            $("#rate1").keypress();
            emi();
        }
        else
        {

            $("#rate1").keypress();
            emi();
        }
    }

//    (function () {
//        var _fbq = window._fbq || (window._fbq = []);
//        if (!_fbq.loaded) {
//            var fbds = document.createElement('script');
//            fbds.async = true;
//            fbds.src = '//connect.facebook.net/en_US/fbds.js';
//            var s = document.getElementsByTagName('script')[0];
//            s.parentNode.insertBefore(fbds, s);
//            _fbq.loaded = true;
//        }
//    })();
//    window._fbq = window._fbq || [];
//    window._fbq.push(['track', '6020039302860', {'value': '0.01', 'currency': 'USD'}]);
</script>
<!--<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6020039302860&amp;cd[value]=0.01&amp;cd[currency]=USD&amp;noscript=1" /></noscript>-->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-57496907-1', 'auto');
    ga('send', 'pageview');

    $('.carousel[data-type="multi"].item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });
    $(window).load(function () {
<?php if ("http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] == base_url()) { ?>
            blink_city('1');
            function blink_city(a)
            {
                if (a <= 4)
                {
                    $(".selected_city").fadeToggle(1000);
                    //setInterval("$('.selected_city').fadeOut().fadeIn();",1500);
                    var count = 1 + parseInt(a);
                    //alert(count);
                    blink_city(count);
                }
            }
<?php } ?>
        var city = "<?php echo $main_city; ?>";
        if (city == '')
        {
            var city = '1';
            $("#main_city option[value='1']").attr('selected', 'selected');
        }
        //alert(city);  
        //alert("<?php echo $this->input->cookie('main_city', TRUE); ?>");
        if (city == '')
        {
            $('#cityModalload').modal('show');
        }
    });
    if ($('#noUi-handle noUi-handle-lower').length) {
        document.getElementById('noUi-handle noUi-handle-lower').className = '';
    }
    setTimeout(function () {
        var a = document.createElement("script");
        var b = document.getElementsByTagName("script")[0];
        a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0032/3288.js?" + Math.floor(new Date().getTime() / 3600000);
        a.async = true;
        a.type = "text/javascript";
        b.parentNode.insertBefore(a, b)
    }, 1);
</script>
<a data-target="#cityModalload" id="city_selection2" data-toggle="modal" style='display:none;' href="#">
</a>
<?php
$main_city = $this->input->cookie('main_city', TRUE);
?>