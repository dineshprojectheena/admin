<?php
$featured_type = 'car_comparison';
$res3 = $this->home_feature->featured_compare($featured_type);
//print_r($res3);
?>
<input type="hidden" id="compare_data" value="<?php echo base_url().'compare-new-cars-online/'.preg_replace('/\s+/', '-', $res3[0]['pro_name_comp'].'-'.$res3[0]['variant_id']) . '-VS-' . preg_replace('/\s+/', '-', $res3[1]['pro_name_comp'].'-'.$res3[1]['variant_id']);?>" />

<?php
$user_login_sucess = $this->session->flashdata('user_login_sucess');
$account_registered = $this->session->flashdata('account_registered');
$sucess = $this->session->flashdata('sucess_msg');
$newsletter_flag = FALSE;
if ($this->session->flashdata('newsletter_msg')) {
    $newsletter_flag = TRUE;
    $newsLetterMsg = $this->session->flashdata('newsletter_msg');
    $newsLetterTitle = 'News Letter Subscribtion';
}
$facebook = $this->session->flashdata('facebook_error');
$test_enquiry = $this->session->flashdata('test_enquiry');
$feedback_success = $this->session->flashdata('feedback_success');
$coming_success = $this->session->flashdata('coming_success');
$enquiry_alert_msg = $this->session->flashdata('enquiry_alert_msg');
$login_failed = $this->session->flashdata('login_failed');
$user_rating_sucess = $this->session->flashdata('user_rating_sucess');
$landing_enquiry = $this->session->flashdata('landing_enquiry');
$leasing_success = $this->session->flashdata('leasing_success');
$main_city = $this->input->cookie('main_city', TRUE);
$main_city_cond = $main_city;
$userdata = $this->session->userdata('logged_in_user');
if (empty($userdata['user_id'])) {
    $this->session->set_userdata('logged_in', FALSE);
    $this->session->unset_userdata('logged_in_user');
    $this->session->sess_destroy();
}
?>
<script>(function () {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6031982220118', {'value': '0.00', 'currency': 'INR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6031982220118&amp;cd[value]=0.00&amp;cd[currency]=INR&amp;noscript=1" />
</noscript>

<script>
    $(window).load(function () {
        var user_login_sucess = "<?php echo $user_login_sucess; ?>";
        var facebook_error = "<?php echo $facebook; ?>";
        var user_rating_sucess = "<?php echo $user_rating_sucess; ?>";
        var account_registered = "<?php echo $account_registered; ?>";
        var login_failed = "<?php echo $login_failed; ?>";
        var enquiry_alert_msg = "<?php echo $enquiry_alert_msg; ?>";
        var test_enquiry_sucess = "<?php echo $test_enquiry; ?>";
        var request_success = "<?php echo $feedback_success; ?>";
        var coming_success = "<?php echo $coming_success; ?>";
        var landing_enquiry = "<?php echo $landing_enquiry; ?>";
        var leasing_success = "<?php echo $leasing_success; ?>";        
        if (leasing_success != '')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Thank you for your request. We will call you back in the next 48 hours.");
            $('#mess_heading').html("");
            $('.landing_data').css('display', 'none');
            $('.landing_data2').css('display', 'block');
        }
        if (landing_enquiry != '')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Thank you for your request. We will call you back in the next 48 hours.");
            $('#mess_heading').html("Enquiry Filled Sucessully");
            $('.landing_data').css('display', 'none');
            $('.landing_data2').css('display', 'block');
            setTimeout(function () {
                window.location.href = "<?php echo base_url(); ?>";
            }, 10000);
            
        }
        if (request_success != '')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Thank you for your request. We will call you back in the next 48 hours.");
            $('#mess_heading').html("Request a call back");
        }
        if (coming_success != '')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("We will alert you with information about Upcoming Cars regularly");
            $('#mess_heading').html("Thank you");
        }
        if (test_enquiry_sucess != '')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("We have received your request. We will check the exact availability and confirm with you within the next 48 hours.");
            $('#mess_heading').html("Book a Test Drive");
        }

<?php if ($newsletter_flag):
    ?> {
                $('#login_errors').modal('show');
                $('#mess_heading').html("Newsletter Subscription");
                $('#error_message').html("<?php echo $newsLetterMsg ?>");
                //delete <?php echo $newsLetterMsg ?>;
            }
<?php endif; ?>
        if (enquiry_alert_msg != '')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Email sent successfully. You will hear from us soon!");
            $('#mess_heading').html("Enquiry Send Successully!");
            setTimeout(function () {
                window.location.href = "<?php echo base_url(); ?>";
            }, 1500);
        }

        if (login_failed == 'login_failed')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Please check username and password careully!");
            $('#mess_heading').html("Login Failed!");
        }


        if (facebook_error == 'Facebook')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Sorry, We were not able to receive the appropriate details to create a user account. Please use a different method to login!");
        }

        if (user_rating_sucess == 'user_rating_sucess')
        {

            $('#login_errors').modal('show');
            $('#error_message').html("Thank you for your feedback!");
            $('#mess_heading').html("User Rating");
        }

        if (user_login_sucess == 'login_sucess')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Please proceed to book your new car.");
            $('#mess_heading').html("Login Successful!");
            var id="<?php echo $this->input->cookie('redirect', TRUE);?>"   
            if(id==2){
            $('#book_now').click();
            }
        }
        if (account_registered == 'login_sucess')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Account registered successfully, please check your email to activate account!");
            $('#mess_heading').html("Login Sucessfull");
            $('#book_now').click();
        }
        if (account_registered == 'custom_account')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Account registered successfully, please check your email to activate account!");
            $('#mess_heading').html("Account Registered");
        }
        if (account_registered == 'account_activated_successfully')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("You Account Acivated Successfully!");
            $('#mess_heading').html("Account Activated...");
        }
        if (account_registered == 'password_reset_successfully')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("please check your mail to reset your password!");
            $('#mess_heading').html("Check your email...");
        }

        if (account_registered == 'password_changed_successfully')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Password Changed Successfully !");
            $('#mess_heading').html("Password Changed...");
        }
        if (feedback_success == 'feedback_success')
        {
            $('#login_errors').modal('show');
            $('#error_message').html("Enquiry/Callback Sent sucessfully!");
            $('#mess_heading').html("Enquiry/Callback...");
        }

    });
</script> 