<script type="text/javascript">
    function getvairiant() {
        var model = $('#model2').val();
        var brand_id = $('#brand_id').val();
        var id = '1';
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/get_model_wise_variant') ?>", data: {model: model, brand_id: brand_id, id: id}
        }).done(function (htmls) {
            getvairiantres();
        });
    }
    function getvairiant2() {
        var model = $('#model2').val();
        var brand_id = $('#brand_id').val();
        var id = '2';
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/get_model_wise_variant') ?>",
            data: {model: model, brand_id: brand_id, id: id}
        }).done(function (htmls) {
            $("#variant_res2").html(htmls);
        });
    }
    function getvairiantres() {
        var model = $('#model2').val();
        var brand_id = $('#brand_id').val();
        var variant_id = $('#variant').val();
        var brand_name = $('#brand_name').val();
        var city = "<?php echo $main_city = $this->input->cookie('main_city', TRUE); ?>";
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/get_model_wise_variantres') ?>",
            data: {model: model, brand_id: brand_id, variant_id: variant_id, brand_name: brand_name, city: city}
        }).done(function (htmls) {
            $("#car_res").html(htmls);
        });
    }

    function getvideo()
    {
        $('#video_link').html('<img src="<?php echo base_url(); ?>assets/img/loading.gif"/>');
        var variant_video = $('#variant_video').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/get_product_video') ?>",
            data: {variant_video: variant_video}
        }).done(function (htmls) {
            $("#video_link").html(htmls);
        });
    }
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    function load_offer_cond(id)
    {
        var count = '<?php echo $i - 1; ?>';
        for (var i = 1; i <= count; i++)
        {
            if (i == id)
            {
                $('#cond_data' + i).css("display", "block");
            }
            else
            {
                $('#cond_data' + i).css("display", "none");
            }
        }
    }
    function get_model_wise_varaint()
    {
        var model_id = $('#model2 option:selected').val();
        var zero_ids = $('#zero_ids').val().split(",");
//        alert(zero_ids);
        for (var i = 0; i < zero_ids.length; i++) {
//            alert(zero_ids[i]);
            $('.home_brand_box' + zero_ids[i]).css("display","none");
//            $('.home_brand_box' + zero_ids[i]).html('');
        }
        var count_model_data = $('#brand_count' + model_id).val();


//        alert(count_model_data);
        if (model_id != '' && model_id != 'All')
        {
            $('.all_brand').css('display', 'none');
            $('.model_' + model_id).css('display', 'block');
        }
        else if (model_id == 'All')
        {
            $('.all_brand').css('display', 'block');
//        $('.model_'+model_id).css('display','block');    
        }
        if (count_model_data == '0')
        {
//            alert(count_model_data);
            $('.home_brand_box' + model_id).css("display","block");
            $('.brand_result_' + model_id).html("<div align='center' class='home_brand_box home_brand_box"+model_id+"'"+">No Result Found!</div>");
        }
        else
        {
//        alert(count_model_data);    
        }
//        else if(count_model_data>'0')
//        {
//        $('.brand_result_'+model_id).html("<div align='center' class='home_brand_box'>No Result Found!</div>");
//        } 


        exit;

    }
</script>
