<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    function book_now_check()
    {
        $('#submit_product').submit();
    }
    function getCookieData(name) {
        var pairs = document.cookie.split("; "),
                count = pairs.length, parts;
        while (count--) {
            parts = pairs[count].split("=");
            if (parts[0] === name)
                return parts[1];
        }
        return false;
    }

    function zero_depreciation(a)
    {
        var pro_id = '<?php echo $id ?>';
        var zero_depreciation_price = $('#zero_depreciation_price').val();
        var car_exchange_price = $('#car_exchange_price').val();
        var corporate_discount_price = $('#corporate_discount_price').val();
        var loyality_bonus_price = $('#loyality_bonus_price').val();
        var grand_on_road_price = $('#grand_on_road_price').html();
        var total_saving_price = $('#total_saving_price').html();
        var extended_waranty_price = $('#extended_waranty_price').val();
        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();


        if (a == 1)
        {
            if ($('#car_insurance').is(':checked') == true)
            {

                var zero_depreciation_price = zero_depreciation_price.replace(/[^\d]/g, '');

                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');

                var final = parseInt(grand_on_road_price) + parseInt(zero_depreciation_price);

                $('#grand_on_road_price').html(intToFormat(final));
                $('#f_onroad_price').html(intToFormat(final));

                $('#onroad_zero_insurance_id').css('display', 'block');
                

                var onroad_comp_insurance = $('#onroad_comp_insurance').html();
                var deducted_comp_ins = onroad_comp_insurance.replace(/[^\d]/g, '');
                $('#onroad_comp_insurance').html(intToFormat(deducted_comp_ins));
                $('#onroad_zero_insurance_price').html(intToFormat(zero_depreciation_price));

            }
            else if ($('#car_insurance').is(':checked') != true)
            {

                var zero_depreciation_price = zero_depreciation_price.replace(/[^\d]/g, '');

                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');

                var final = parseInt(grand_on_road_price) - parseInt(zero_depreciation_price);

                $('#grand_on_road_price').html(intToFormat(final));
                $('#f_onroad_price').html(intToFormat(final));

                $('#onroad_zero_insurance_id').css('display', 'none');

                var onroad_comp_insurance = $('#onroad_comp_insurance').html();
                var deducted_comp_ins = onroad_comp_insurance.replace(/[^\d]/g, '');
                $('#onroad_comp_insurance').html(intToFormat(deducted_comp_ins));
                $('#onroad_zero_insurance_price').html(intToFormat(zero_depreciation_price));

            }

        }
        if (a == 2)
        {
            if ($('#extended_waranty').is(':checked') == true)
            {
                var extended_waranty_price = extended_waranty_price.replace(/[^\d]/g, '');
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(extended_waranty_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
                $('#extended_waranty_price1').css('display', 'block');
                $('#extended_waranty_price_price').html('' + intToFormat(extended_waranty_price));
            }
            else if ($('#extended_waranty').is(':checked') != true)
            {
                var extended_waranty_price = extended_waranty_price.replace(/[^\d]/g, '');
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(extended_waranty_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
                $('#extended_waranty_price1').css('display', 'none');
                $('#extended_waranty_price_price').html('' + intToFormat(extended_waranty_price));
            }
        }

        if (a == 3)
        {
            if ($('#car_exchange_data').is(':checked') == true)
            {
                var car_exchange_price = car_exchange_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) + parseInt(car_exchange_price);
                $('#total_saving_price').html(intToFormat(final));
                $('#save_saving_price').html(intToFormat(final));
                $('#exchange_id').css("display", "block");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(car_exchange_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
                $('#onroad_exchange_bonus_id').css('display', 'block');
                $('#onroad_exchange_bonus_price').html('(-)' + intToFormat(car_exchange_price));

            }
            else if ($('#car_exchange_data').is(':checked') != true)
            {
                var car_exchange_price = car_exchange_price.replace(/[^\d]/g, '');
                if (car_exchange_price == '')
                {
                    car_exchange_price = 0;
                }
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');//                                                                                                                                      
                var final = parseInt(total_saving_price) - parseInt(car_exchange_price);
                $('#total_saving_price').html(intToFormat(final));
                $('#save_saving_price').html(intToFormat(final));
                
                $('#exchange_id').css("display", "none");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(car_exchange_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
                $('#onroad_exchange_bonus_id').css('display', 'none');
            }



        }



        if (a == 4)
        {
            if ($('#corporate_discount').is(':checked') == true)
            {
                var corporate_discount_price = corporate_discount_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) + parseInt(corporate_discount_price);
                $('#total_saving_price').html(intToFormat(final));
                $('#save_saving_price').html(intToFormat(final));
                $('#corp_id').css("display", "block");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(corporate_discount_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
//                document.cookie = 'corporate_discount_price=' + corporate_discount_price + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_corporate_dicount_id').css('display', 'block');
                $('#onroad_corporate_dicount_price').html('(-)' + intToFormat(corporate_discount_price));

            }
            else if ($('#corporate_discount').is(':checked') != true)
            {
                var corporate_discount_price = corporate_discount_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) - parseInt(corporate_discount_price);
                $('#total_saving_price').html(intToFormat(final));
                $('#save_saving_price').html(intToFormat(final));
                $('#corp_id').css("display", "none");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(corporate_discount_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
//                document.cookie = 'corporate_discount_price=' + '' + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_corporate_dicount_id').css('display', 'none');
            }
        }

        if (a == 5)
        {
            if ($('#loyality_bonus').is(':checked') == true)
            {
                var loyality_bonus_price = loyality_bonus_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) + parseInt(loyality_bonus_price);
                $('#total_saving_price').html(intToFormat(final));
                $('#save_saving_price').html(intToFormat(final));

                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(loyality_bonus_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
//                document.cookie = 'loyality_bonus_price=' + loyality_bonus_price + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_loyalty_bonus_id1').css('display', 'block');
                $('#onroad_onroad_price_price').html('(-)' + intToFormat(loyality_bonus_price));
            }
            else if ($('#loyality_bonus').is(':checked') != true)
            {
                var loyality_bonus_price = loyality_bonus_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) - parseInt(loyality_bonus_price);

               $('#total_saving_price').html(intToFormat(final));
                $('#save_saving_price').html(intToFormat(final));
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(loyality_bonus_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
                $('#onroad_loyalty_bonus_id1').css('display', 'none');
            }
        }


        if (a == 5)
        {
            if ($('#loyality_bonus').is(':checked') == true)
            {
                var loyality_bonus_price = loyality_bonus_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) + parseInt(loyality_bonus_price);
               $('#total_saving_price').html(intToFormat(final));
                $('#save_saving_price').html(intToFormat(final));
                                                                                                                                                  $('#corp_id').css("display","block");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(loyality_bonus_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
//                document.cookie = 'loyality_bonus_price=' + loyality_bonus_price + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_loyalty_bonus_id1').css('display', 'block');
                $('#onroad_onroad_price_price').html('(-)' + intToFormat(loyality_bonus_price));
            }
            else if ($('#loyality_bonus').is(':checked') != true)
            {
                var loyality_bonus_price = loyality_bonus_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) - parseInt(loyality_bonus_price);
                
                $('#total_saving_price').html(intToFormat(final));
                $('#save_saving_price').html(intToFormat(final));
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(loyality_bonus_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                $('#f_onroad_price').html(intToFormat(final2));
//                document.cookie = 'loyality_bonus_price=' + '' + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_loyalty_bonus_id1').css('display', 'none');
            }


        }
        
        var final_grand_on_road_price = ($('#grand_on_road_price').html()).replace(/[^\d]/g, '');
        //                                                                                                                                    alert(final_grand_on_road_price);
        $('#f_onroad_price').html(intToFormat(final_grand_on_road_price));
        var total_saving_price = ($('#total_saving_price').html()).replace(/[^\d]/g, '');
//        $('#save_saving_price').html(intToFormat(total_saving_price));
        $('#save_saving_price2').html(intToFormat(total_saving_price));
        $('#final_total_saving_price').val(total_saving_price);
        $('#final_mnc_on_road_price').val(final_grand_on_road_price);
    }
    
//    load_car_conf();
//    function load_car_conf()
//    {
//    var car_configuration_id="<?php echo $pro_conf['car_configuration_id'];?>"    
//    var on_road_price="<?php echo $pro_conf['on_road_price'];?>"    
//    var total_saving="<?php echo $pro_conf['total_saving'];?>"    
//    $('#f_onroad_price').html(intToFormat(on_road_price));
//    $('#save_saving_price').html(intToFormat(total_saving));
//    $('#total_saving_price').html(intToFormat(total_saving));
//    $('#final_total_saving_price').val(intToFormat(total_saving);
//    $('#final_mnc_on_road_price').val(on_road_price);        
//    }
    






    function zero_depreciation1(a)
    {

        var pro_id = '<?php echo $id ?>';

        var zero_depreciation_price = $('#zero_depreciation_price').val();
        var car_exchange_price = $('#car_exchange_price').val();
        var corporate_discount_price = $('#corporate_discount_price').val();
        var loyality_bonus_price = $('#loyality_bonus_price').val();
        var grand_on_road_price = $('#grand_on_road_price').html();
        var total_saving_price = $('#total_saving_price').html();
        var extended_waranty_price = $('#extended_waranty_price').val();
        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        if (a == 1)
        {
            if ($('#car_insurance').is(':checked') == true)
            {
                var zero_depreciation_price = zero_depreciation_price.replace(/[^\d]/g, '');
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final = parseInt(grand_on_road_price) + parseInt(zero_depreciation_price);
                $('#grand_on_road_price').html(intToFormat(final));
                document.cookie = 'zero_dep_insurance=' + intToFormat(final) + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_zero_insurance_id').css('display', 'block');
                var onroad_comp_insurance = $('#onroad_comp_insurance').html();
                var deducted_comp_ins = onroad_comp_insurance.replace(/[^\d]/g, '');
                $('#onroad_comp_insurance').html(intToFormat(deducted_comp_ins));
                $('#onroad_zero_insurance_price').html(intToFormat(zero_depreciation_price));

            }
            else if ($('#car_insurance').is(':checked') != true)
            {
                var zero_depreciation_price = zero_depreciation_price.replace(/[^\d]/g, '');
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final = parseInt(grand_on_road_price) - parseInt(zero_depreciation_price);
                $('#grand_on_road_price').html(intToFormat(final));
                document.cookie = 'zero_dep_insurance=' + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_zero_insurance_id').css('display', 'none');

                $('#onroad_zero_insurance_id').css('display', 'none');
                var onroad_comp_insurance = ($('#onroad_comp_insurance').html()).replace(/[^\d]/g, '');
                var onroad_zero_insurance_price = ($('#onroad_zero_insurance_price').html()).replace(/[^\d]/g, '');
                if (onroad_zero_insurance_price == '')
                {
                    $('#onroad_zero_insurance_price').html(intToFormat(onroad_comp_insurance));
                }
                else
                {
                    var total_price = parseInt(onroad_comp_insurance.replace(/[^\d]/g, ''));

                    $('#onroad_comp_insurance').html(intToFormat(total_price));
                }

            }


            if ($('#car_exchange_data').is(':checked') == true)
            {
                var car_exchange_price = car_exchange_price.replace(/[^\d]/g, '');
                if (car_exchange_price == '')
                {
                    car_exchange_price = 0;
                }
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) + parseInt(car_exchange_price);
                $('#total_saving_price').html(intToFormat(final));
                $('#exchange_id').css("display", "block");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(car_exchange_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                document.cookie = 'car_exchange_price=' + car_exchange_price + '_' + pro_id + ';' + expires + ';path=/';

                $('#onroad_exchange_bonus_id').css('display', 'block');
                $('#onroad_exchange_bonus_price').html('(-)' + intToFormat(car_exchange_price));

            }
            else if ($('#car_exchange_data').is(':checked') != true)
            {
                var car_exchange_price = car_exchange_price.replace(/[^\d]/g, '');

                if (car_exchange_price == '')
                {
                    car_exchange_price = 0;
                }

                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');//                                                                                                                                      

                var final = parseInt(total_saving_price) - parseInt(car_exchange_price);

                $('#total_saving_price').html(intToFormat(final));
                $('#exchange_id').css("display", "none");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(car_exchange_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                document.cookie = 'car_exchange_price=' + '' + '_' + pro_id + ';' + expires + ';path=/';
                //                alert(car_exchange_price);
                $('#onroad_exchange_bonus_id').css('display', 'none');

            }
        }
        else if (a == 3)
        {
            if ($('#corporate_discount').is(':checked') == true)
            {
                var corporate_discount_price = corporate_discount_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) + parseInt(corporate_discount_price);
                $('#total_saving_price').html(intToFormat(final));
                $('#corp_id').css("display", "block");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(corporate_discount_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                document.cookie = 'corporate_discount_price=' + corporate_discount_price + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_corporate_dicount_id').css('display', 'block');
                $('#onroad_corporate_dicount_price').html('(-)' + intToFormat(corporate_discount_price));

            }
            else if ($('#corporate_discount').is(':checked') != true)
            {
                var corporate_discount_price = corporate_discount_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) - parseInt(corporate_discount_price);
                $('#total_saving_price').html(intToFormat(final));
                $('#corp_id').css("display", "none");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(corporate_discount_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                document.cookie = 'corporate_discount_price=' + '' + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_corporate_dicount_id').css('display', 'none');
            }
        }
        else if (a == 4)
        {
            if ($('#loyality_bonus').is(':checked') == true)
            {
                var loyality_bonus_price = loyality_bonus_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) + parseInt(loyality_bonus_price);
                $('#total_saving_price').html(intToFormat(final));
                //                                                                                                                                        $('#corp_id').css("display","block");
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(loyality_bonus_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                document.cookie = 'loyality_bonus_price=' + loyality_bonus_price + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_loyalty_bonus_id1').css('display', 'block');
                $('#onroad_onroad_price_price').html('(-)' + intToFormat(loyality_bonus_price));
            }
            else if ($('#loyality_bonus').is(':checked') != true)
            {
                var loyality_bonus_price = loyality_bonus_price.replace(/[^\d]/g, '');
                var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                var final = parseInt(total_saving_price) - parseInt(loyality_bonus_price);
                //                                                                                                                                            alert(total_saving_price);
                //                                                                                                                                            alert(loyality_bonus_price);
                $('#total_saving_price').html(intToFormat(final));
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(loyality_bonus_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                document.cookie = 'loyality_bonus_price=' + '' + '_' + pro_id + ';' + expires + ';path=/';
                $('#onroad_loyalty_bonus_id1').css('display', 'none');
            }
        }
        else if (a == 5)
        {
            if ($('#car_finance_data').is(':checked') == true)
            {
                document.cookie = 'car_finance_price=' + $('#car_finance_price').val() + '_' + pro_id + ';' + expires + ';path=/';
            }
            else
            {
                document.cookie = 'car_finance_price=' + '' + '_' + pro_id + ';' + expires + ';path=/';
            }
        }
        else if (a == 6)
        {
            if ($('#home_delivery').is(':checked') == true)
            {
                document.cookie = 'home_delivery_price=' + home_delivery_price + '_' + pro_id + ';' + expires + ';path=/';
            }
            else
            {
                document.cookie = 'home_delivery_price=' + '' + '_' + pro_id + ';' + expires + ';path=/';
            }
        }
        else if (a == 7)
        {
            if ($('#extended_waranty').is(':checked') == true)
            {
                var extended_waranty_price = extended_waranty_price.replace(/[^\d]/g, '');
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) + parseInt(extended_waranty_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                document.cookie = 'extended_waranty_price=' + extended_waranty_price + '_' + pro_id + ';' + expires + ';path=/';
                $('#extended_waranty_price1').css('display', 'block');
                $('#extended_waranty_price_price').html('' + intToFormat(extended_waranty_price));
            }
            else if ($('#extended_waranty').is(':checked') != true)
            {
                var extended_waranty_price = extended_waranty_price.replace(/[^\d]/g, '');
                //                                                                                                                                            var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
                //                                                                                                                                            var final = parseInt(total_saving_price) - parseInt(extended_waranty_price);
                //                                                                                                                                            alert(total_saving_price);
                //                                                                                                                                            alert(loyality_bonus_price);
                //                                                                                                                                            $('#total_saving_price').html(intToFormat(final));
                var grand_on_road_price = grand_on_road_price.replace(/[^\d]/g, '');
                var final2 = parseInt(grand_on_road_price) - parseInt(extended_waranty_price);
                $('#grand_on_road_price').html(intToFormat(final2));
                document.cookie = 'extended_waranty_price=' + '' + '_' + pro_id + ';' + expires + ';path=/';
                $('#extended_waranty_price1').css('display', 'none');
            }
        }

        //                                                                                                                                    f_onroad_price
        var final_grand_on_road_price = ($('#grand_on_road_price').html()).replace(/[^\d]/g, '');
        //                                                                                                                                    alert(final_grand_on_road_price);
        $('#f_onroad_price').html(intToFormat(final_grand_on_road_price));
        var total_saving_price = ($('#total_saving_price').html()).replace(/[^\d]/g, '');
        $('#save_saving_price').html(intToFormat(total_saving_price));
        //                                                                                                                                    alert(total_saving_price);
        $('#final_total_saving_price').val(total_saving_price);
        //                                                    alert(total_saving_price);
        $('#final_mnc_on_road_price').val(final_grand_on_road_price);
        document.cookie = 'grand_on_road_price=' + $('#grand_on_road_price').html() + '_' + pro_id + ';' + expires + ';path=/';
        document.cookie = 'total_saving_price=' + total_saving_price + '_' + pro_id + ';' + expires + ';path=/';

        //        alert('sdsdsd');

    }


    /*
     
     
     disc_price();
     function disc_price()
     {
     
     var disc_price = $('#disc_price').val();
     //                                                    alert(disc_price);
     if (disc_price != '')
     {
     var total_saving_price = $('#total_saving_price').html();
     var total_saving_price = total_saving_price.replace(/[^\d]/g, '');
     var new_total_sav = parseInt(total_saving_price) + parseInt(disc_price);
     $('#total_saving_price').html(intToFormat(new_total_sav));
     $('#coupon_side_details').css('display', 'block');
     $('#coupon_side_details2').css('display', 'block');
     $('#show_coupon_price').html(intToFormat(disc_price));
     $('#show_coupon_price2').html(intToFormat(disc_price));
     $('#f_onroad_price').html($('#grand_on_road_price').html());
     }
     var total_saving_price = ($('#total_saving_price').html()).replace(/[^\d]/g, '');
     var grand_on_road_price = ($('#grand_on_road_price').html()).replace(/[^\d]/g, '');
     //                                                alert(total_saving_price);
     //                                                alert(grand_on_road_price);
     $('#final_total_saving_price').val(total_saving_price);
     $('#final_mnc_on_road_price').val(grand_on_road_price);
     }
     
     
     
     extended_waranty_process();
     function extended_waranty_process()
     {
     var pro_id = '<?php echo $id ?>';
     var extended_pro_id = '<?php
$extended_waranty_price = explode('_', $this->input->cookie('extended_waranty_price', TRUE));
echo $extended_waranty_price[1];
?>';
     if (pro_id != extended_pro_id)
     {
     if ($('#extended_waranty').is(':checked') == true)
     {
     $('#extended_waranty').click();
     }
     }
     var d = new Date();
     d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
     var expires = "expires=" + d.toUTCString();
     
     var car_exchange_price = $('#car_exchange_price').val();
     
     var get_car_exchange_price = getCookieData("car_exchange_price");
     var pairs = get_car_exchange_price.split("_");
     
     if (pairs[1] != pro_id)
     {
     document.cookie = 'car_exchange_price=' + car_exchange_price + '_' + pro_id + ';' + expires + ';path=/';
     }
     else
     {
     
     
     if (pairs[0] == '')
     {
     
     if ($('#car_exchange_data').is(':checked') == true)
     {
     $('#car_exchange_data').click();
     }    
     }
     }
     
     var total_saving_price = getCookieData("total_saving_price");
     //        alert(total_saving_price);
     var save = total_saving_price.split("_");
     //        alert(save[0]);
     $('#total_saving_price').html(intToFormat(save[0]))
     $('#save_saving_price').html(intToFormat(save[0]))
     
     
     }
     
     
     */

    function intToFormat(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        var z = 0;
        var len = String(x1).length;
        var num = parseInt((len / 2) - 1);

        while (rgx.test(x1))
        {
            if (z > 0)
            {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            else
            {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
                rgx = /(\d+)(\d{2})/;
            }
            z++;
            num--;
            if (num == 0)
            {
                break;
            }
        }
        return x1 + x2;
    }

    function color_selection()
    {
        var color = $('#color option:selected').text();

        var rating_variant = $('#rating_variant').val();

        var color_selected = rating_variant + '_' + color;

        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = 'main_color=' + color_selected + ';' + expires + ';path=/';
        location.reload();
    }

    function reset_coupon()
    {
        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = 'coupon_id=' + '' + ';' + expires + ';path=/';
        location.reload();
    }



    function coupon_apply()
    {
        var coupon_code = $('#coupon_code').val();
        if ($('#color').val() != '')
        {
            var color = $('#color option:selected').text();
        }
        else
        {
            color = '';
        }
        var pro_id = '<?php echo $id; ?>';
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('product/coupon_code_apply') ?>",
            data: {coupon_code: coupon_code, pro_id: pro_id, color: color}
        }).done(function (html) {
//            alert(html);
//            exit;

            if (html == 0)
            {
//                $('.close').click();
//                $('#apply_button').css("display", "none");
                $('#apply_msg').html("<div class='alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + 'Invalid or Expired Coupon Code Entered. Kindly recheck and try again!! This page will refresh automatically.' + '</div>');
//                $('#coupon_message').html("<br>Invalid or Expired Coupon Code Entered. Kindly recheck and try again!<br>This page will refresh automatically.");
//                                                            location.reload();
            }
            else if (html == 1)
            {
//                $('.close').click();
//                $('#apply_button').css("display", "none");
                $('#apply_msg').html("<div class='alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + 'All coupons with this code is already availed! This page will refresh automatically.' + '</div>');
//                $('#coupon_message').html("<br>All coupons with this code is already availed!<br>This page will refresh automatically.");
//                                                            location.reload();
            }
            else if (html == 5)
            {
//                $('.close').click();
//                $('#apply_button').css("display", "none");
                $('#apply_msg').html("<div class='alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + 'Invalid or Expired Coupon Code Entered. Kindly recheck and try again! This page will refresh automatically.' + '</div>');
//                $('#coupon_message').html("<br>Invalid or Expired Coupon Code Entered. Kindly recheck and try again!<br>This page will refresh automatically.");
//                                                            location.reload();
            }

            else if (html == 4)
            {
//                $('.close').click();
//                $('#apply_button').css("display", "none");
                $('#apply_msg').html("<div class='alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + 'Sorry! coupon is not applicable for this combination! This page will refresh automatically.' + '</div>');
//                $('#coupon_message').html("<br>Sorry! coupon is not applicable for this combination!<br>This page will refresh automatically.");
//                                                            location.reload();
            }
            else if (html == 3)
            {
//                $('.close').click();
//                $('#apply_button').css("display", "none");
                $('#apply_msg').html("<div class='alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + 'Coupon Code applied successfully! This page will refresh automatically.' + '</div>');
//                $('#coupon_message').html("<div align='center'><br><div style='color:green;'>Coupon Code applied successfully!<br>This page will refresh automatically.</div></div>");
//                                                            location.reload();
            }
//            exit;
            setTimeout(function () {
                //do what you need here
                location.reload();
            }, 3000);
        });
    }


    function user_reviews()
    {
        var url = '<?php echo $exp[2]; ?>';

        window.location.href = "<?php ?><?php echo base_url() . 'user-review/' ?>" + '<?php echo url_struct(preg_replace('/\s+/', '-', ($pro_main_details[0]['pro_name_comp'] . '-' . $pro_main_details[0]['variant_id']))) ?>';
    }
    user_review('5');
    expert_review('5');
    function user_review(pag)
    {
        var pro_id = $('#pro_id').val();
        var user_sort = $('#user_sort').val();
        var pag;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('reviews/load_reviews') ?>",
            data: {pro_id: pro_id, user_sort: user_sort, pag: pag}
        }).done(function (html) {
//            alert(html);
            var arr = html.split('-Total');
            setInterval(function () {
                $("#tot_user_review").html(arr[0]);
                $("#user_review_count").html(arr[0]);
                $("#user-review-data").html(arr[1]);
            }, 3000);
        });
    }

    function expert_review(pag)
    {

        var pro_id = $('#pro_id').val();
        var user_sort = $('#expert_sort').val();
        var pag;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('reviews/load_exp_reviews') ?>",
            data: {pro_id: pro_id, user_sort: user_sort, pag: pag}
        }).done(function (html) {
            var arr = html.split('-Total');
            $("#tot_exp_review").html(arr[0]);
            $("#exp_review_count").html(arr[0]);
            $("#expert-review-data").html(arr[1]);
        });
    }
    $('.user-comment-box2').click(function () {
        //                    alert('sdsdsd');
        $(this).find('.user-comment').toggleClass('hide');
        if ($(this).find('.user-comment').hasClass('hide')) {
            $(this).find('.user-comment-heading span').html('[ + More ]');
        } else {
            $(this).find('.user-comment-heading span').html('[ - Less ]');
        }

    });
    $('.user-comment-box3').click(function () {
        //                    alert('sdsdsd');
        $(this).find('.user-comment').toggleClass('hide');
        if ($(this).find('.user-comment').hasClass('hide')) {
            $(this).find('.user-comment-heading span').html('[ + More ]');
        } else {
            $(this).find('.user-comment-heading span').html('[ - Less ]');
        }

    });



</script>