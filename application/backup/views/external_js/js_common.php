<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
    function set_exp_price()
    {
        var mnc_ex_price = ($('#main_exp_price').html()).replace(/[^\d]/g, '');
        $('.emi_ex_price').val(mnc_ex_price);
        $('.noUi-connect').css("display", "none");

    }

    function check_others_city()
    {
        var others_city = $('#request_main_city option:selected').val();
//        alert(others_city);
        if (others_city == 'Others')
        {
            $('#request_main_city2').css("display", "block");
        }
        else
        {
            $('#request_main_city2').css("display", "none");
        }
    }

    function check_others_city2()
    {
        var others_city = $('#city option:selected').val();
//        alert(others_city);
        if (others_city == 'Other Cities')
        {
//            alert('sdsdsd');
            $('#city2').css("display", "block");
        }
        else
        {
            $('#city2').css("display", "none");
        }
    }



    load_affiliate();
    function load_affiliate()
    {
        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000 * 30));
        var expires = "expires=" + d.toUTCString();
        var peramter = "<?php echo $_GET['ref_code']; ?>";
        if (peramter != '')
        {
//        alert(expires);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('common/affiliate_id') ?>",
                data: {peramter: peramter}
            }).done(function (html) {
                document.cookie = 'ref_id=' + html + ';' + expires + ';path=/';
//                $("#divResult").html(html).show();
            });
        }
    }




    function car_comparision_click()
    {
        $('.news_home_readmore').click();
    }
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    function select_your_car(a)
    {
        if (a == 1)
        {
            var carbrands = $("#carbrands").find('option:selected').text();
            var carbrands = carbrands.trim();
            if (carbrands == 'Select Brand')
            {
                var carbrands = '';
            }
            $(".p3").val(carbrands);
            $(".p4").val('');
        }
        if (a == 2)
        {
            var brand_model = $("#brand_model").find('option:selected').val();
            var brand_model = brand_model.trim();
            if (brand_model == 'Select Model')
            {
                var brand_model = '';
            }
            $(".p4").val(brand_model);
        }
        if (a == 3)
        {
            var cartype = $("#cartype").find('option:selected').text();
            var cartype = cartype.trim();
            var cartype = cartype.replace(/ /g, "-");
            $(".p5").val(cartype);
        }
        else if (a == 4)
        {
            var minvalue = ($('.minvalue').html()).replace(/[^0-9]/g, '');
            var maxvalue = ($('.maxvalue').html()).replace(/[^0-9]/g, '');
            $(".p1").val(minvalue);
            $(".p2").val(maxvalue);
        }
    }
    function validateEmail(emailField) {
//        alert(emailField);
        if (emailField.value != '')
        {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

            if (reg.test(emailField.value) == false)
            {
                $(emailField).val('');
                alert('Invalid Email Address');
                return false;
            }

            return true;
        }
    }
    function validateNewsletterEmail() {
        var emailField = $('#email').val();
//        alert(emailField);
//        if (emailField != '')
//        {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField) == false)
        {
            $('#email').val('');
            alert('Invalid Email Address');
            return false;
        }
        $("#newsletter_form").submit();
        return true;
//        }
    }
    function validateno(noField) {
        if (isNaN(noField.value)) {
            alert('Not a valid number');
            $(noField).val('');
            return false;
        }
        return true;
    }
    function validatedate() {
        var selected_date = $('#test_date').val();
        var today_date = '<?php echo date('d-m-y'); ?>';
        if (selected_date <= today_date)
        {
            alert("Date should be greater than today's date");
            $('#test_date').val('');
            return false;
        }
        return true;
    }

    function goprice()
    {
        var a1 = '<?php echo $max_price_main; ?>';
        var a2 = '<?php echo $min_price_main; ?>';
        if (a1 != '' && a2 != '')
        {
            var data = 'price-' + a2 + '-' + a1;
            window.location.href = "<?php echo site_url('car-search-by-type'); ?>/" + data + "/brand-" + "/type-" + "/model-";
        }
        else
        {
            var brand_model = $("#brand_model").val();
            var min_price = $("#min_price").html();
            var min_price2 = min_price.replace(/[^0-9]/g, '');
            var max_price = $("#max_price").html();
            var max_price2 = max_price.replace(/[^0-9]/g, '');
            var carbrands = $("#carbrands option:selected").text();
            var carbrands_sel = $("#carbrands").val();
//                                        alert(carbrands_sel);
            var carbrands1 = carbrands.replace(/ /g, "-");
            var cartype = $("#cartype").val();
            var cartype1 = cartype.replace(/ /g, "-");
            var data = '';
            if (min_price2 != '' && max_price2 != '')
            {
                var data = 'price-' + min_price2 + '-' + max_price2;
            }
            else
            {
                var data = 'price-';
            }
            if (carbrands_sel != '')
            {
                var data = data + '/brand-' + carbrands1;
            }
            else
            {
                var data = data + '/brand-';
            }

            if (cartype != '')
            {
                var data = data + '/type-' + cartype1;
            }
            else
            {
                var data = data + '/type-';
            }

            if (brand_model != '')
            {
                var data = data + '/model-' + brand_model;
            }
            else
            {
                var data = data + '/model-';
            }
            window.location.href = "<?php echo site_url('car-search-by-type'); ?>/" + data;
        }
    }

    function search_product_data()
    {

        var inputSearch = $('#inputSearch').val();
        if (inputSearch != '')
        {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('common/search_car') ?>",
                data: {inputSearch: inputSearch}
            }).done(function (html) {
//                alert(html);
                $("#divResult").html(html).show();
            });

        }
        return false;
    }


    function joingroup()
    {
        var status = '1';
        var brand_id = $('#brand_id').val();
        var user_id = '<?php echo $user_id; ?>';
        if (user_id == '')
        {
            var user_id = '<?php
$data['session'] = $this->session->userdata("logged_in_user");
echo $user_id = $data['session']['user_id'];
?>';
        }
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('authorization/join_group') ?>",
            data: {brand_id: brand_id, user_id: user_id, status: status}
        }).done(function (htmls) {
            if (htmls == 0)
            {
                $('#login_errors').modal('show');
                $('#error_message').html("Group joined successfully!");
                $('#mess_heading').html("Success");
                location.reload();
            }
            else
            {
                alert('You are already a part of this group');
            }
        });
    }
    function checkuser(a)
    {
//        alert(a);
        var regis_username = $("#regis_username").val();
        var signin_email = $("#signin_email").val();
        var regis_email = $("#regis_email").val();
        var regis_password = $("#regis_password").val();
        if (a == 1)
        {

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('authorization/checkuser') ?>",
                data: {regis_username: regis_username, regis_email: regis_email, regis_password: regis_password, a: a}
            }).done(function (html) {
//            alert(html);
                $("#inv_name").html(html);
            });
        }
        if (a == 4)
        {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('authorization/checkuser') ?>",
                data: {signin_email: signin_email, a: a}
            }).done(function (html) {
                if (html > 0)
                {
                    $("#signin_email").css("border", "1px solid green");
                }
                else
                {
                    $("#signin_email").css("border", "1px solid red");
                }

//                $("#inv_name").html(html);
            });
        }
        if (a == 2)
        {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('authorization/checkuser') ?>",
                data: {regis_username: regis_username, regis_email: regis_email, regis_password: regis_password, a: a}
            }).done(function (html) {
//            alert(html);
                $("#inv_email").html(html);
            });
        }
//9833282666
        if (a == 3)
        {
            var inv_name = $("#inv_name").html();
            var inv_pass = $("#inv_pass").html();
            var inv_email = $("#inv_email").html();
//            alert(inv_pass);
            if (regis_username == '')
            {
                $("#inv_name").html('');
                $("#inv_name").html('Please insert username..');
                $("#inv_pass").html('');
                $("#inv_email").html('');
            }
            else if (regis_email == '')
            {
                $("#inv_email").html('Please insert email..');
                $("#inv_name").html('');
                $("#inv_pass").html('');
            }
            else if (regis_password == '')
            {

                $("#inv_pass").html('');
                $("#inv_pass").html('Please insert password..');
                $("#inv_name").html('');
//                $("#inv_email").html('');
            }

            else
            {
                $("#inv_pass").html('');
                if ($("#inv_name").html() != '')
                {
                    alert('Please solve the user name error!');
                }
                else if ($("#inv_email").html() != '')
                {
                    alert('Please solve the email error!');
                }
                else if ($("#inv_pass").html() != '')
                {
                    alert('Please solve the password error!');
                }
                else
                {
                    $("#reg__sub").click();
                }
            }


        }
    }

    function rating_variant_change()
    {
        var rating_variant = $('#rating_variant').val();
        if (rating_variant != '')
        {
            var rating_variant_text = $("#rating_variant option:selected").text();
            var value = rating_variant_text.replace(/ /g, "-");
            var con_var = value + '-' + rating_variant;
            var con_var=con_var.replace(/[^a-zA-Z0-9 \-\ \.\ \\\ ]/g, "");
//            alert(con_var);
            window.location.href = "<?php echo site_url('car'); ?>/" + con_var;
        }
    }
    function compare_car_type_selection()
    {

        var checked_count = $('input[name="compare_car"]:checked').length;
        if (checked_count < 2) {
            alert('You can compare only 3 cars at one time.');
            e.preventDefault();
        } else if (checked_count > 3) {
            alert('You cannot book multiple cars simultaneously');
            e.preventDefault();
        }
        var trimline = '';
        $("input.check1:checked").each(function () {
            var data=($(this).data('name'));
            var data=data.replace(/[^a-zA-Z0-9 \-\ \.\ \\\ ]/g, "");
//            alert(data);
            trimline += data + '-VS-';
        });
        
        var selected_product = trimline.substring(0, trimline.length - 4);
//        alert(selected_product);
//        exit;
        window.location.href = "<?php echo site_url('compare-new-cars-online'); ?>/" + selected_product;
    }

    function confiure_car_type_selection()
    {

        var checked_count = $('input[name="compare_car"]:checked').length;
//        if()
        if (checked_count < 1) {
            alert('You need to select atleast 1 cars to configure');
            e.preventDefault();
        } else if (checked_count > 1) {
            alert('You cannot book multiple cars simultaneously');
            e.preventDefault();
        }
        var trimline = '';
        $("input.check1:checked").each(function () {
            var data=($(this).data('name'));
            var data=data.replace(/[^a-zA-Z0-9 \-\ \.\ \\\ ]/g, "");
            trimline += data;
            window.location.href = "<?php echo site_url('car'); ?>/" + trimline;
//            window.location.href="<?php // echo site_url('car-introduction');           ?>/"+trimline;
        });

        window.location.href = "<?php echo site_url('car'); ?>/" + trimline;
//        window.location.href = "<?php echo site_url('car-introduction'); ?>/" + trimline;
//    alert(trimline);

    }
    function date_cookie()
    {
    var d = new Date();
    d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    return expires;
    }
    

    function setcitycookie(id)
    {
//        alert(id);
        var main_city = $("#main_citys :selected").val();
//        alert(main_city);
        var expires=date_cookie();
        
        
//        alert(main_city);
//        var d = new Date();
//        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
//        var expires = "expires=" + d.toUTCString();
////        if (id != '')
////        {
            document.cookie = 'main_city=' + main_city + ';' + expires + ';path=/';
////            if (url_main_city == '')
////            {
                location.reload();
////            }
//            else
//            {
//                window.location.href = '?city=' + main_city;
//            }
//        }


    }  

    $(document).ready(function () {

        $(".configure-checkbox").click(function () {
            var feature_name = "";
            $("input.configure-checkbox:checked").each(function () {
                feature_name += $(this).data('name') + ', ';
            });
            document.getElementById('selected_vairant').value = feature_name;
        });





    });
    function loadvariant()
    {
        $('#count').val("0");
        var trimline = '';
        $("input.f1:checked").each(function () {
            trimline += $(this).data('name') + ',';
        });
        var engine = '';
        $("input.f2:checked").each(function () {
            engine += $(this).data('name');
        });
        var transmission = '';
        $("input.f3:checked").each(function () {
            transmission += $(this).data('name');
        });

        var pro_id = $("#pro_id").val();
        var selected_vairant = $("#selected_vairant").val();
//        $("#compare-variant").css("display","none");
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/configure_car_variant_filter') ?>",
            data: {trimline: trimline, engine: engine, transmission: transmission, pro_id: pro_id, selected_vairant: selected_vairant}
        }).done(function (html) {
            $("#result").html(html);
//            $("#compare-variant2").css("display","block");
        });
    }

    function total_mondey_saved()
    {
        var brand_data = $("#brand_data").val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/load_saved_price') ?>",
            data: {brand_data: brand_data}
        }).done(function (html) {
//            alert(html);
            $("#save_amount").html(html);
        });

    }
    total_mondey_saved();
    function expert_review_result(id)
    {
//        alert(a);
        if (id == 1)
        {
//        var id ='1';
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('stitcher/get_review_brand') ?>",
                data: {id: id}
            }).done(function (html) {
//            alert(html);            
                $("#exp_brand_res").html(html);
                brand_model();
            });
        }

        if (id == 2)
        {
            var brand_data = $('#brand_data').val();
//        var id ='1';
//        alert(brand_data);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('stitcher/get_review_brand') ?>",
                data: {id: id, brand_data: brand_data}
            }).done(function (html) {
//            alert(html);            
                $("#exp_model_res").html(html);
//            brand_model();
            });
        }
        if (id == 3)
        {
            var model_data = $('#model_data').val();
            var brand_data = $('#brand_data').val();
//        var id ='1';
//        alert(brand_data);
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('stitcher/get_review_brand') ?>",
                data: {id: id, model_data: model_data, brand_data: brand_data}
            }).done(function (html) {
//            alert(html);            
                $("#exp_var_res").html(html);
//            brand_model();
            });
        }

    }

    function usernamexist()
    {
        //$('#update_button').hide();
        $('#update_button').attr({type: 'button'});
        var username = $('#username').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/checkusername') ?>",
            data: {username: username}
        }).done(function (html) {
            //alert(html);	
            var arr = html.split('-total');
            document.getElementById('error_msg').innerHTML = arr[1];
            if (arr[0] == 1)
            {
                var username2 = $('#username2').val();
                $('#username').val(username2);
            }
            $('#update_button').attr({type: 'submit'});

        });
    }


    function setmodel()
    {
//        $("#brand_model").toggle();
//        $("#car_model").append('<div class="loading-div"><i class="fa fa-circle-o-notch fa-spin"></i> Loading</div>');
        var brand_data = $('#brand_data').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('common/comparison_model_data') ?>",
            data: {brand_data: brand_data}
        }).done(function (html) {
//            alert(html);
//            $(".loading-div").remove();
            $("#brand_model").html(html);

//            alert(html);
//            $("#brand_model").html(html);
        });
    }

    function testdrive_setmodel()
    {
        var brand_data = $('#test_brand_data').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('common/comparison_model_data') ?>",
            data: {brand_data: brand_data}
        }).done(function (html) {
//            alert(html);
            $("#test_brand_model").html(html);
        });
    }

    testdrive_fuel();
    function testdrive_fuel()
    {
        var brand_data = '1';
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('common/fuel_type') ?>",
            data: {brand_data: brand_data}
        }).done(function (html) {
//            alert(html);
            $("#test_fuel_type").html(html);
        });
    }



    function setmodel2()
    {
        var brand_data = $('#carbrands').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('common/comparison_model_data') ?>",
            data: {brand_data: brand_data}
        }).done(function (html) {
//            if(html!=''){
            $("#brand_model").html(html);
//            }
        });
    }
    function set_lease_model2()
    {
        var brand_data = $('#carbrands').val();
//        alert(brand_data);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('common/comparison_lease_model_data') ?>",
            data: {brand_data: brand_data}
        }).done(function (html) {
//            alert(html);
            if (html == '')
            {
            $("#brand_model").html('<option value="">Select Model</option>');
            }
            else
            {
            $("#brand_model").html(html);
            }
            

        });
    }
    function setvariant()
    {
        var brand_data = $('#brand_data').val();
        var brand_model = $('#brand_model').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('common/comparison_variant_data') ?>",
            data: {brand_data: brand_data, brand_model: brand_model}
        }).done(function (html) {
            $("#car_variant").html(html);
        });
    }
    function testdrive_setvariant()
    {
        var brand_data = $('#test_brand_data').val();
        var brand_model = $('#test_brand_model').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('stitcher/comparison_variant_data') ?>",
            data: {brand_data: brand_data, brand_model: brand_model}
        }).done(function (html) {
            $("#test_car_variant").html(html);
        });
    }
    function change_car()
    {
        var car_variant = $('#car_variant').val();
        var car_name = $("#car_variant>option:selected").html();
        var sel_car_name=car_name .replace(/[^a-zA-Z0-9 \-\ \.\ \\\ ]/g, "-") + '-' + car_variant;
        alert(sel_car_name);
        var pera = "<?php echo $_SERVER['REQUEST_URI'] ?>";
        var position = $('#position').val();
//        alert(position);
<?php
$url = $_SERVER['REQUEST_URI'];
$exp = explode('/', $url);
$count = count($exp);
?>
        var pera = "<?php echo $_SERVER['REQUEST_URI'] ?>";
        var arr = pera.split('/');
        var url = arr[<?php echo $count - 1; ?>];
        window.location.href = "<?php echo base_url() . 'common/change_car_res/'; ?>" + url + '-dineshk-' + sel_car_name + '-dineshk-' + position;
    }

    function removeDupes(arr) {
        var nonDupes = [];
        arr.forEach(function (value) {
            if (nonDupes.indexOf(value) == -1) {
                nonDupes.push(value);
            }
        });
        return nonDupes;
    }

    function cleanArray(actual)
    {
        var newArray = new Array();
        for (var i = 0; i < actual.length; i++)
        {
            if (actual[i])
            {
                newArray.push(actual[i]);
            }
        }
        return newArray;
    }


    function news(pag)
    {
        var id = '<?php echo $news_id; ?>';
        if (id == '')
        {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('stitcher/all_news') ?>",
                data: {pag: pag}
            }).done(function (html) {
                $("#res").html(html);
            });
        }
    }


    function tot_user_review(type)
    {
        var pro_id = $('#pro_id').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('reviews/tot_exp_reviews') ?>",
            data: {pro_id: pro_id, type: type}
        }).done(function (html) {
        });
    }

    function select_brand()
    {
        var brand_id = $("#brand_data option:selected").text();
//        alert(brand_id);
        //exit;
        window.location.href = "car-selection/" + brand_id;
    }


    home_brand_product();
    function home_brand_product()
    {
        var brand_data = $("#brand_data").val();
//        alert(brand_data);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('common/home_brand_product') ?>",
            data: {brand_data: brand_data}
        }).done(function (html) {
//            alert(html);
//            exit;
            $("#carousel_result").html(html);
        });
    }

    $(function () {
        $("#divResult").mouseleave(function () {
            $(this).hide();
        });

        $(document).on("click", "#divResult", function () {
            var $clicked = $(e.target);
            var $name = $clicked.find('.name').html();
            var decoded = $("<div/>").html($name).text();
            $('#inputSearch').val(decoded);
        });

//        $("#divResult").live("click", function(e) {
//            var $clicked = $(e.target);
//            var $name = $clicked.find('.name').html();
//            var decoded = $("<div/>").html($name).text();
//            $('#inputSearch').val(decoded);
//        });

        $(document).on("click", function (e) {
            var $clicked = $(e.target);
            if (!$clicked.hasClass("search")) {
                jQuery("#divResult").fadeOut();
            }
        });
//        $(document).live("click", function(e) {
//            var $clicked = $(e.target);
//            if (!$clicked.hasClass("search")) {
//                jQuery("#divResult").fadeOut();
//            }
//        });
        $('#inputSearch').click(function () {
            jQuery("#divResult").fadeIn();
        });
    });

    function search_users()
    {
        var inputSearch = $('#tagged_user').val();
        if (inputSearch != '')
        {

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('stitcher/search_users') ?>",
                data: {inputSearch: inputSearch}
            }).done(function (html) {
                $(".divResult").html(html).show();
            });

        }
        return false;
    }
    $(".divResult").mouseleave(function () {
        $(this).hide();
    });

    landing_page();
    function landing_page()
    {
        var laning_page = ('<?php echo $content; ?>');
        var menu_status = ('<?php print_r($landing_data['menu_status']); ?>');
//    alert(menu_status);
        if (laning_page == 'landing_page')
        {
            $("#ftr").css("display", "none");
        }
        if (menu_status != '')
        {
            if (menu_status == '1')
            {
                $("#menu-container").css("display", "block");
            }
            else if (menu_status == '2')
            {
                $(".menu-container").css("display", "none");
            }
        }
    }
    $(window).load(function () {
//        alert('sdsdsd');
//    $('.hide').css('display','block');
    $(".hide").removeClass('hide');
    $("#search_home").css('display','block');
    });


</script>




