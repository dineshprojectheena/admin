<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h5 class="page-heading"><i class="fa fa-bullhorn"></i> Frequently Asked Questions</h5>
<!--            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>-->
        </div>
    </div>


    <div class="panel-group" id="selection-list">
        <div class="panel">
            <div class="accordian-header">
                <a data-toggle="collapse" data-parent="#selection-list" href="#cat1">
                    <h5>
                        <i class="fa fa-tags"></i>
                        <?php
                        echo 'Buyer FAQs';
                        ?>
                        <span class="accordian-open-close">
                            <i class="fa fa-plus"></i>
                        </span>
                    </h5>
                </a>
            </div>
            <div id="cat1" class="panel-collapse collapse" style="padding:15px;">
                <br>
                <p>
                <h4>How do I search for cars on MyNewCar?</h4>
                </p> 
                <p align="justify">
                    You have different search options on the MyNewCar website. You can either choose a city and do a simple keyword search or use under “Home” or “Car Search
                    &amp; Buy” the search options to find cars based on brand, price or type. You are also invited to visit our car brand pages under “Brands” to inform
                    yourself about specific cars or can go directly to best offers under “Hot Deals”.
                </p>
                <br>
                <p align="justify">
                <h4>Why should you use MyNewCar to buy your new car?</h4>
                </p>
                <p align="justify">
                    MyNewCar combines the advantages of an online e-commerce experience and an on road car dealership. Our one-stop solution offers you the following benefits:
                </p>
                <ol>
                    <li>
                        <p align="justify" lang="en-US">
                        <h4>Safe and Secure</h4>
                        - Trustful and transparent transaction
                        </p>
                    </li>
                    <li>
                        <p align="justify" lang="en-US">
                        <h4>Hassle Free and Convenient</h4>
                        - Quick and easy online one-stop-solution
                        </p>
                    </li>
                    <li>
                        <p align="justify" lang="en-US">
                        <h4>Best Price</h4>
                        – Online deals and group discounts
                        </p>
                    </li>
                    <li>
                        <p align="justify" lang="en-US">
                        <h4>Touch and Feel</h4>
                        - Test drive, delivery and after-sales service by authorized dealers
                        </p>
                    </li>
                    <li>
                        <p align="justify" lang="en-US">
                        <h4>Guarantee</h4>
                        - 100% manufacturer's warranty
                        </p>
                    </li>
                </ol>
                <br>
                <p>
                <h4>How does MyNewCar work?</h4>
                </p>
                <p align="justify">
                    MyNewCar offers you a large range of independent and trustful information on car brands, models and variants. You will benefit from expert and user car
                    reviews, a convenient car comparison tool and further information sources. You can search on our website for special new car promotions in the “Hot Deals”
                    section or select and configure a desired car according to your requirements. During your car configuration, you can add accessories and MyNewCar will
                    provide you offers for car insurance, car finance, annual maintenance contracts and extended warranties. You can purchase your car directly or
                    alternatively opt for an available group deal for your car brand. In case a group deal is available you can join it and receive further discounts, but need
                    to wait until the required group size has been reached. Every group deal has a specific duration time that is displayed during the booking process. To
                    conclude your purchasing process, you are required to provide your credit or debit card details to the approved payment gateways while making the payment.
                    The information provided by you will not be utilized or shared with any third party. To ensure a fast and safe payment, MyNewCar operates an integrated
                    industry standard and approved payment gateway. Your new car will be delivered hassle-free to your doorstep in partnership with the nearest authorized
                    MyNewCar dealer. All after-sales services are provided by your respective dealer and you will benefit from the standard dealer warranty.
                </p>
                
                <br>
                <p align="justify">
                <h4>How much discount may I expect on my chosen car?</h4>
                </p>
                <p align="justify">
                    MyNewCar offers you special promotions and hot deals on new cars. Further discounts are available through group deals. The exact discount will be always
                    displayed during the booking process before the final car purchase. Please have a look at the Terms of Offers for Sale and the Website User Terms as
                    published on our website.
                </p>
                <br>
                <p align="justify">
                <h4>How do I get my new car?</h4>
                </p>
                <p>
                    All the offers are promoted by our participating authorized partners. In cooperation with the dealer we will provide you a doorstep car delivery and after
                    sales service.
                </p>
                <br>
                <p>
                <h4>What is the waiting period after ordering the car?</h4>
                </p>
                
                <p align="justify">
                    The waiting period after ordering a new car is depending on the car’s availability at our authorized dealers and our respective dealers’ terms and
                    conditions. The delivery date and waiting period is part of the contract. You will be regularly informed about the order status and progress.
                </p>
                <br>
                <p align="justify">
                <h4>How can I get a car insurance and/or car finance offer?</h4>
                </p>
                <p align="justify">
                    During your car configuration, MyNewCar partners will provide you offers for car insurance, car finance, annual maintenance contracts and extended
                    warranties that you can add to your car purchase. You have the choice to complete your car purchase with the desired accessories and services. Please have
                    a look at the Terms of Offers for Sale and the Website User Terms as published on our website.
                </p>
                <br>
                <p align="justify">
                <h4>How can I purchase accessories at the MyNewCar website?</h4>
                </p>
                <p align="justify">
                    During your car configuration, you can add accessories to your car purchase to meet your individual requirements. Please have a look at the Terms of Offers
                    for Sale and the Website User Terms as published on our website.
                </p>
                <br>
                <p>
                <h4>Who will provide the after sales service for my car?</h4>
                </p>
                <p>
                    After-sales services are provided by our authorized dealers in your city. You will receive the same services as the offline car buyer at the respective car
                    dealership.
                </p>
                <br>
                <p>
                <h4>Do I lose the warranty or after sales service options that come with buying from a dealer when I purchase a vehicle through MyNewCar?</h4>
                </p>
                <p align="justify">
                    You will have the same warranty rights and after sales service options when you purchase a new car through MyNewCar as if you purchase it directly from the
                    dealership. The final contract for the car purchase will be between you and our authorized dealer. All your rights and the dealer’s duties and liabilities
                    are contractually secured by the contract. Please have a look at the Terms of Offers for Sale and the Website User Terms as published on our website.
                </p>
                <br>
                <p align="justify">
                <h4>How can I contact MyNewCar for Customer Support?</h4>
                </p>
                <p align="justify">
                    Our customers can contact us between 10am to 7pm (Monday to Saturday) by phone +91 22 45 02 03 04, live chat or email (    <a href="mailto:contact@mynewcar.in">contact@mynewcar.in</a>) for any advice or questions.
                </p>
                <br>
                <p>
                <h4>Do I have to pay a fee for MyNewCar’s services? </h4>
                </p>
                <p align="justify">
                    All our services and the use of the MyNewCar website are free-of-charge for you.
                </p>
                <br>
                <p align="justify">
                <h4>What is the token amount that I have to pay to book a car?</h4>
                </p>
                <p align="justify">
                    To confirm you booking, you are requested to pay a token amount of 5.000 INR. This amount will be deducted from your final invoice that you receive from
                    the car dealer to finalize your car purchase.
                </p>
                <br>
                <p>
                <h4>Cancellation charges? Do I get the refund of the token amount?</h4>
                </p>
                <p align="justify">
                    There are no cancellation charges. Your paid token will be fully refunded in case you cancel your order with us or the dealer. Please have a look at the
                    Terms of Offers for Sale and the Website User Terms as published on our website as well as the respective car dealer’s Terms and Conditions.
                </p>
                <br>
                <p>
                <h4>How do I pay for purchased products and services?</h4>
                
                </p>
                <p align="justify">
                    To ensure a fast and safe payment, MyNewCar operates an integrated industry standard and approved payment gateway. When purchasing new cars, accessories
                    and/or services, you are required to provide your credit or debit card details to the approved payment gateways while making the payment. The information
                    provided by you will not be utilized or shared with any third party.
                </p>
                <br>
                <p>
                <h4>What should I do, if I have not received my confirmation email?</h4>
                </p>
                <p align="justify">
                    Following your successful car booking, you will get an order confirmation. Our confirmation emails should arrive directly in your email in-box. Sometimes,
                    it can take up to several minutes. If you have not received a confirmation email at all, you may have entered your email address incorrectly on our website
                    or it may have been filtered out by your spam or junk email filter. In such a case, please check your 'Junk Email’ or 'Spam' folder and mark MyNewCar as a
                    trusted sender and source for future communication.
                </p>
                <p align="justify">
                    If you have still not received our email after checking the above, please contact our Customer Care Center between 10am to 7pm (Monday to Saturday) by
                    phone +91 22 45 02 03 04, live chat or email us <a href="mailto:contact@mynewcar.in">contact@mynewcar.in</a>.
                </p>
                <br>
                <p>
                <h4>Why do you need my email address? </h4>
                </p>
                <p align="justify">
                    MyNewCar requires your email address to send you necessary information during your car purchase process (order status, tracking number) and communicate
                    with you. If requested, you can also receive updates on MyNewCar offerings and special promotions. Please be assured that your information will be treated
                    with caution. We fully respect your privacy and adhere to our stated Privacy Policy.
                </p>
                <br>
                <p>
                <h4>How do I unsubscribe from emails? </h4>
                </p>
                <p>
                    You have to click on the link 'Unsubscribe' which you can find in every email that you receive from us.
                </p>
                <br>
                <p>
                <h4>How do you handle trade-ins?</h4>
                </p>
                <p align="justify">
                    MyNewCar offers the option of a car exchange. During the booking process you can opt for this and our car exchange partner will get in contact with you. If
                    you have any queries concerning this point, please contact our Customer Care Center between 10am to 7pm (Monday to Saturday) by phone +91 22 45 02 03 04,
                    live chat or email us <a href="mailto:contact@mynewcar.in">contact@mynewcar.in</a>.
                </p>
                <br>
                <p>
                <h4>What parts of the country and which cities are served by MYNEWCAR? </h4>
                </p>
                <p>
                    MyNewCar offers at the moment services in Mumbai, Navi Mumbai, Bangalore, Pune and Thane. Further cities and regions will added in the upcoming weeks and
                    months. Subscribe to our Newsletter to be informed of any updates on new available cities.
                </p>
                <p lang="en-GB">
                    <a name="_GoBack"></a>
                    <br/>
                    <br/>
                </p>
                <br>
                <p>
                <h4>Can I access the website through my mobile device?</h4>
                </p>
                <p>
                    MyNewCar offers a mobile optimized design. For the best experience we still recommend to access the page through your laptop or tablet. Stay tuned for the
                    upcoming “MyNewCar” App.
                </p>
                <br>
                <p>
                <h4>How do I book a car to get a group discount?</h4>
                </p>
                <p>
                    If a group discount is available, it will be displayed with the car offer. You can opt for the group deal at the end of the purchasing process. A group
                    deal offers further discounts, but requires a waiting period until the group size has been reached. Every group deal has a specific duration time that is
                    displayed during the booking process.
                </p>
                <br>
                <p>
                <h4>What happens when the group size for a group is not met during a defined time period?</h4>
                </p>
                <p>
                    If a group size is not met, MyNewCar will either extend the group deal or you can still purchase the desired and configured car as an individual without
                    the extra group benefits.
                </p>
                <br>
                <p>
                <h4>How can I do a test drive?</h4>
                </p>
                <p>
                    The test drive will be provided by our authorized dealers. Time and date will be confirmed by mail of phone through our Customer Support. If you have any
                    queries or want to book directly a test drive, please contact our Customer Care Center between 10am to 7pm (Monday to Saturday) by phone +91 22 45 02 03 04,
                    live chat or email us <a href="mailto:contact@mynewcar.in">contact@mynewcar.in</a>.
                </p>
                <br>
                <p>
                <h4>Which car should I buy?</h4>
                </p>
                <p>
                    You can inform yourself through expert and user ratings on MyNewCar. A car comparison tool is also provided that allows to compare up to three cars at the
                    same time. For further support you can also contact our Customer Care Center between 10am to 7pm (Monday to Saturday) by phone +91 22 45 02 03 04, live chat
                    or email <a href="mailto:contact@mynewcar.in">contact@mynewcar.in</a>.
                </p>
                <br>
                <p>
                <h4>How do I book a car on your website?</h4>
                </p>
                <p align="justify">
                    You can search on MyNewCar for special new car promotions in the “Hot Deals” section or select and configure a desired car according to your requirements.
                    During your car configuration, you can add accessories and MyNewCar will provide you car insurance, car finance, annual maintenance contracts and extended
                    warranties offers. You can purchase your car directly or alternatively opt for an available group deal for your car brand. In case a group deal is
                    available you can join it and receive further discounts, but need to wait until the required group size has been reached. Every group deal has a specific
                    duration time that is displayed during the booking process. To conclude your purchasing process, you are required to provide your credit or debit card
                    details to the approved payment gateways while making the payment. The information provided by you will not be utilized or shared with any third party. To
                    ensure a fast and safe payment, MyNewCar operates an integrated industry standard and approved payment gateway. Your new car will be delivered hassle-free
                    to your doorstep in partnership with the nearest authorized MyNewCar dealer. All after sales services are provided by your respective dealer and you will
                    benefit from the standard dealer warranty.
                </p>
                <br><p>
                <h4>How can I customize my car?</h4>
                </p>
                <p>
                    During your car configuration, you can add accessories and MyNewCar will provide you car insurance, car finance, annual maintenance contracts and extended
                    warranties offers. You can add these by simply selecting them. Before the car purchase is confirmed you will receive an overview on your configured car
                    with the chosen selections.
                </p>
                <br><p>
                <h4>I don’t have a credit card, can I pay by cheque?</h4>
                </p>
                <p>
                    Yes, you also opt for a bank transfer or use your debit card. If you have any queries on payments, please contact our Customer Care Center between 10am to
                    7pm (Monday to Saturday) by phone +91 22 45 02 03 04, live chat or email us <a href="mailto:contact@mynewcar.in">contact@mynewcar.in</a>.
                </p>
                <br><p>
                <h4>I just paid online to book a Car. Have you received it?</h4>
                </p>
                <p align="justify">
                    Following your successful car booking, you will get an order confirmation. Our confirmation emails should arrive directly in your email in-box. Sometimes,
                    it can take up to several minutes. If you have not received a confirmation email at all, you may have entered your email address incorrectly on our website
                    or it may have been filtered out by your spam or junk email filter. In such a case, please check your 'Junk Email’ or 'Spam' folder and mark MyNewCar as a
                    trusted sender and source for future communication.
                </p>
                <p align="justify">
                    If you have still not received our email after checking the above, please contact our Customer Care Center between 10am to 7pm (Monday to Saturday) by
                    phone +91 22 45 02 03 04, live chat or email us <a href="mailto:contact@mynewcar.in">contact@mynewcar.in</a>.
                </p>
                <br><p>
                <h4>Can I buy/get my own insurance and/or finance contract?</h4>
                </p>
                <p align="justify">
                    Yes, you can also arrange your insurance and/or finance contract by yourself without the support of MyNewCar. Just do not choose an insurance and/or
                    finance option during the configuration process on MyNewCar.
                </p>
                <br><p>
                <h4>When I book a car with some accessories, will they all be already fitted when delivered?</h4>
                </p>
                <p align="justify">
                    Yes, all accessories will be fitted by the car manufacturer into your configured car. The final contract is between you and our authorized dealer. Some
                    accessories that do not need a fitting into the car are delivered separately to you. All your rights are contractually secured by the contract. Please have
                    a look at the Terms of Offers for Sale and the Website User Terms as published on our website.
                </p>



                <?php
                /*
                  foreach ($result as $res) {
                  $type = $res['type'];
                  ?>

                  <div class="panel-body">
                  <div class="row">
                  <div class="col-md-12">
                  <ul class="faq-ul">
                  <li>
                  <div class="question-div">
                  <h4>
                  Q. <?php echo $res['main_heading']; ?>
                  </h4><br>
                  </div>
                  <div class="answer-div">
                  <p>
                  <?php echo $res['descrtiption']; ?>
                  </p>
                  </div>
                  </li>

                  </ul>
                  </div>
                  </div>
                  </div>

                  <?php
                  }
                 * 
                 */
                ?>
            </div>
        </div>  


        <div class="panel">
            <div class="accordian-header">
                <a data-toggle="collapse" data-parent="#selection-list" href="#cat2">
                    <h5>
                        <i class="fa fa-tags"></i>
                        <?php
                        echo 'MNC FAQs';
                        ?>
                        <span class="accordian-open-close">
                            <i class="fa fa-plus"></i>
                        </span>
                    </h5>
                </a>
            </div>
            <div id="cat2" class="panel-collapse collapse collapse" style="padding: 15px; height: auto;">
                <br><p>
                <h4>What is MyNewCar?</h4>
                </p>
                <p align="justify">
                    Car buying is an emotional journey that should be accompanied by joy and happiness. MyNewCar will offer customers multi-brand new cars, accessories and
                    related services combined with an easy and convenient purchasing experience. Our mission is to simplify the car buying process to a joyful experience.
                    Customers can compare, select and configure their car anytime and anywhere. MyNewCar provides unbiased information at every step and assists you in getting
                    the best deals, group discounts, as well as the best offers for car loans and insurances. Your new car will be delivered hassle-free in partnership with
                    the nearest dealer to your doorstep.
                </p>
                <br>
                <p align="justify">
                <h4>How does MyNewCar help me?</h4>

                </p>
                <p align="justify">
                    MyNewCar wants to achieve the highest level of joy and satisfaction possible when purchasing your new car. Our online platform enables you to select,
                    configure and purchase a desired car, accessories and related services …
                </p>
                <ul>
                    <ul>
                        <li>
                            <p align="justify">
                            <h4>Efficiently </h4>
                            with a one-stop-solution
                            </p>
                        </li>
                        <li>
                            <p align="justify">
                            <h4>Conveniently</h4>
                            by purchasing online
                            </p>
                        </li>
                        <li>
                            <p align="justify">
                            <h4>Securely </h4>
                            with refundable booking amount while
                            </p>
                        </li>
                        <li>
                            <p align="justify">
                            <h4>Saving Money </h4>
                            with online discounts and group deals
                            </p>
                        </li>
                    </ul>
                </ul>
                <br>
                <p align="justify">
                <h4>How does MyNewCar differ from other Internet car portals?</h4>
                </p>
                <p align="justify">
                    At MyNewCar you can always be sure to get an unrivaled best price with our online discounts and group deals. We also strive to realize the great vision of
                    providing you an online platform that informs, guides and supports you in all relevant phases of your car’s life, an one-stop ecosystem solution with a
                    completely new level of car configuration in India.
                </p>
                <br><p>
                <h4>Does MyNewCar list new and used cars for sale?</h4><br>
                </p>
                <p align="justify">
                    MyNewCar offerings are limited to new cars. Besides that, we also offer car exchanges to our customers.
                </p>


                <?php
                /*
                  foreach ($result2 as $res2) {
                  $type = $res2['type'];
                  ?>

                  <div class="panel-body">
                  <div class="row">
                  <div class="col-md-12">
                  <ul class="faq-ul">
                  <li>
                  <div class="question-div">
                  <h4>
                  Q. <?php echo $res2['main_heading']; ?>
                  </h4><br>
                  </div>
                  <div class="answer-div">
                  <p>
                  <?php echo $res2['descrtiption']; ?>
                  </p>
                  </div>
                  </li>

                  </ul>
                  </div>
                  </div>
                  </div>

                  <?php
                  }
                 * 
                 */
                ?>
            </div>
        </div>         

        <div class="panel">
            <div class="accordian-header">
                <a data-toggle="collapse" data-parent="#selection-list" href="#cat3">
                    <h5>
                        <i class="fa fa-tags"></i>
                        <?php
                        echo 'Dealer FAQs';
                        ?>
                        <span class="accordian-open-close">
                            <i class="fa fa-plus"></i>
                        </span>
                    </h5>
                </a>
            </div>
            <div id="cat3" class="panel-collapse collapse collapse" style="padding:15px;">
                <br><p align="justify">
                    <h4>How can I become a partner of MyNewCar?</h4>
                </p>
                <p align="justify">
                    We are more than happy to welcome all potential partners that help us to complete our vision of a one-stop solution for car buyers. Please send an email to    <a href="mailto:mmanpuria@mynewcar.in">mmanpuria@mynewcar.in</a> with your contact details and we will we will revert to you as soon as possible.
                </p>
                <br>
                <p align="justify">
                    <h4>What are my benefits from cooperating with MyNewCar?</h4>
                </p>
                <p align="justify">
                    MyNewCar aims at generating a win-win-situation with its partners. We want to be an additional sales and marketing channel that creates a quantifiable
                    value add to both parties:
                </p>
                <ul>
                    <li>
                        <p align="justify" lang="en-US">
                            <h4>New cost-efficient distribution channel</h4>
                            to increase the customer reach at variable cost
                        </p>
                    </li>
                    <li>
                        <p align="justify" lang="en-US">
                            <h4>Increased sales</h4>
                            through integration with existing sales channels
                        </p>
                    </li>
                    <li>
                        <p align="justify" lang="en-US">
                            <h4>Additional revenues and increased margins</h4>
                            per car via online car configurator
                        </p>
                    </li>
                    <li>
                        <p align="justify" lang="en-US">
                            <h4>Brand building exercise</h4>
                            for loyalty and customer retention
                        </p>
                    </li>
                </ul>
                <br>
                <p align="justify">
                    <h4>How can I contact MyNewCar?</h4>
                </p>
                <p align="justify">
                    Please send an email to <a href="mailto:mmanpuria@mynewcar.in">mmanpuria@mynewcar.in</a> with your contact details and we will we will revert to you as
                    soon as possible.
                </p>


                <?php
                /*
                  foreach ($result3 as $res3) {
                  $type = $res3['type'];
                  ?>

                  <div class="panel-body">
                  <div class="row">
                  <div class="col-md-12">
                  <ul class="faq-ul">
                  <li>
                  <div class="question-div">
                  <h4>
                  Q. <?php echo $res3['main_heading']; ?>
                  </h4><br>
                  </div>
                  <div class="answer-div">
                  <p>
                  <?php echo $res3['descrtiption']; ?>
                  </p>
                  </div>
                  </li>

                  </ul>
                  </div>
                  </div>
                  </div>

                  <?php
                  }
                 * 
                 */
                ?>
            </div>
        </div>   

<!--        <div class="panel">
            <div class="accordian-header">
                <a data-toggle="collapse" data-parent="#selection-list" href="#cat4">
                    <h5>
                        <i class="fa fa-tags"></i>
                        <?php
                        echo 'Other coopertaion partners FAQs';
                        ?>
                        <span class="accordian-open-close">
                            <i class="fa fa-plus"></i>
                        </span>
                    </h5>
                </a>
            </div>
            <div id="cat4" class="panel-collapse collapse collapse">

                <?php
                foreach ($result3 as $res3) {
                    $type = $res3['type'];
                    ?>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="faq-ul">
                                    <li>
                                        <div class="question-div">
                                            <h4> 
                                                Q. <?php echo $res3['main_heading']; ?>
                                            </h4><br>
                                        </div>
                                        <div class="answer-div">
                                            <p>
                                                <?php echo $res3['descrtiption']; ?>
                                            </p>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                ?>
            </div>
        </div>         -->

    </div>
</div> <!-- /.container -->