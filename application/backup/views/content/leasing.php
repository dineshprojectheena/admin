<div class="container" id="leasing">
    <div class="row">
        <div class="col-md-12 col-cust-both">
            <div class="col-md-12 col-cust-both">
                <div class="col-md-12 col-cust-both">
                    <img src="<?php echo base_url() . 'uploads/newbanner_template_leasing.png' ?>" class="img-responsive"/>
                </div>
                
                <div class="col-md-12 home_brand_box lease_content" style="margin-top:10px;">
                    <div class="col-md-12"> 
                    <div class="col-md-7" style="text-align:justify;">
                        <p class="lease_points_head">
                        <h1>We Offer Multi Brand Leasing with</h1>
                        </p>
                        <p>                        
                        <ul class="leaf"> 
                            <li>    
                        Zero Down Payment 
                            </li>
                            <li>
                         No Monthly Fleet Management Charge
                            </li>
                            <li>
                         Optional Ownership – No Depreciation or Devaluation Risk
                            </li>
                            <li>
                         Predetermined Purchase Price after the Lease period
                            </li>
                            <li>
                         Annual Maintenance included in the monthly rental (Premium Brands)
                            </li>
                        </ul>
                        </p>
                        <div class="lease_common">
                        <p class="lease_after">
                        So is it better to Lease or Purchase a Car? 
                        </p>
                        <p>
                        Simply said, Leasing and Car Loans are two different methods of automobile financing. During Lease, a fixed monthly rental is paid by you based on vehicle's total value. The monthly rental is exempted from Income Tax.  
                        </p>
                        <p>
                        When the Lease ends, you have two options. Either you go-ahead and purchase the car at the redetermined residual value, or you simply return the vehicle to the Lessor.
                        </p>
                        <p>
                        MYNEWCAR.IN is proud to be a partner with ALPHERA Financial Services, an important component of the BMW Group Financial Services and one of the world's leading financial service providers. ALPHERA Financial Services was launched in India in January 2011 and is already a leading provider of multi-brand vehicle finance products.
                        </p>
                        <p>
                        Still too much jargon? Go ahead, request a call back and we will be glad to discuss the advantages Leasing a car holds for you!
                        </p>
                        </div>

                    </div>
                    
                    <div class="col-md-5">
                        <form id="contact-us" class="form-horizontal" role="form" action="<?php echo site_url("lease_upload"); ?>" method="POST">
                            <?php if (isset($alert_msg)) { ?>
                                <div role="alert" id="msg" class="alert alert-success">
                                    <div class="twelve columns mobile-four alert-box secondary">
                                        <p class="success"><?php echo 'Email sent successfully. You will hear from us soon'; ?></p>
                                        <a onclick="$('#msg').hide();" class="close">&times;</a>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <label for="name" class="col-sm-1 control-label"><span style="color:maroon;"></span></label>
                                <div class="col-sm-11">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Full Name"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company_name" class="col-sm-1 control-label"></label>
                                <div class="col-sm-11">
                                    <input type="text" class="form-control" name="company_name" placeholder="Company Name"  id="company_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-1 control-label"> <span style="color:maroon;"></span></label>
                                <div class="col-sm-11">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="number" class="col-sm-1 control-label"><span style="color:maroon;"></span></label>
                                <div class="col-sm-11">
                                    <input type="text" class="form-control" name="number" id="number" required placeholder="Phone Number ">
                                </div>
                            </div>
                            

                            <div class="form-group">
                                <label for="carbrands" class="col-sm-1 control-label"></label>
                                <div class="col-sm-11">
                                    <select class="form-control" name="carbrands" id="carbrands" onchange="set_lease_model2();" required>
                                        <option value="">Select Brand</option>
                                        <?php
                                        foreach ($get_lease_brands as $all_brands_data) {
                                            ?>                            
                                            <option value="<?php echo $all_brands_data['brand_id']; ?>"><?php echo $all_brands_data['brand_name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="brand_model" class="col-sm-1 control-label"></label>
                                <div class="col-sm-11">
                                    <select class="form-control" name="brand_model" id="brand_model" required >
                                        <option value="">Select Model</option>
                                        <?php
                                        foreach ($city_res as $city_res_data) {
                                            ?>                            
                                            <option><?php echo $city_res_data['name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="city" class="col-sm-1 control-label"></label>
                                <div class="col-sm-11">
                                    <select class="form-control" name="city" id="city" onchange="check_others_city2();" required>
                                        <option value="">Select City</option>
                                        <?php
                                        foreach ($all_Citys as $city_res_data) {
                                            ?>                            
                                            <option value="<?php echo $city_res_data['city_id']; ?>"><?php echo $city_res_data['name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                        <option option="Others">Other Cities</option>
                                    </select>
                                    <input type="text" id="city2" name="city2" style="display: none; margin-top: 5px; margin-bottom: 5px;" class="form-control" placeholder="">
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label for="Model" class="col-sm-1 control-label"></label>
                                <div class="col-sm-9 pull-left">
                                    <span class="col-sm-7 col-xs-12 control-label"  style="text-align:left;"><input type="radio" name="i_am" id="comp_prof" value="company professional"><label for="comp_prof">&nbsp;Company Professional</label></span>    
                                    <span class="col-sm-5  col-xs-12 control-label"  style="text-align:left;"><input type="radio" name="i_am" id="self_prof" value="Self employed"><label for="self_prof">&nbsp;Self Employed</label></span>    
                                    <!--<span  class="col-sm-12 control-label"  style="text-align:left;"><input type="radio" name="i_am" id="own_prof"  value="Business Owner"><label for="own_prof">Business Owner</label></span>-->    
                                </div>

                            </div>



                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-maroon">Submit</button>
                                </div>
                            </div>
                        </form>  
                    </div>
                        </div>
                </div>    
                <!--            <div class="col-md-12">
                                <div class="review-box">
                                    <h4 class="review-title">
                                        In Associate with
                                    </h4>                
                                </div>
                            </div>-->
                <!--            <div class="col-md-3">
                            <p>
                                <img src="<?php echo base_url() . 'assets/img/alphera logo.png' ?>" class="img-responsive" style="width:70%;"/>
                            </p>            
                            </div>
                -->


            </div>
        </div>
    </div>
</div>
