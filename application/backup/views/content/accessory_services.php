<?php
$this->load->view('templates/responsiveSlider');
$features = array('1' => 'Exteriors',
    '2' => 'Interiors',
    '3' => 'Safety & Technology',
    '4' => 'Audio & Communication',
    '5' => 'Services',
    '6' => 'Equipment Package',
    '7' => 'Is-Services');

$subcategory = array(
    '1' => 'Headlights',
    '2' => 'Sunroofs',
    '3' => 'Tyres',
    '4' => 'Steering Wheels',
    '5' => 'Seats',
    '6' => 'Navigation System',
    '7' => 'Audio System',
    '8' => 'Speakers',
    '9' => 'Telephone',
    '11' => 'Finance',
    '12' => 'AMC Contract',
    '13' => 'Insurance for Car',
    '14' => 'Extended Warranty',
    '15' => 'Used Cars',
    '10' => 'Others'
);
$count = count($subcategory);
?>
<style>
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td
    {
        border: 0px solid #DDDDDD !important;    
    }
</style>
<div class="container">
    <?php
    $accessory_cook = $car_selected_data[0]['accessory'];
    $exp_acc = explode(",", $accessory_cook);
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped car-accessory-table">
                        <thead>
                            <tr>
                                <th width="5%" align="center"></th>
                                <th width="95%">Car Finance</th>
                                <th width="5%" style="display:none;">Estimate Price</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($finance)) {
                                foreach ($finance as $finance_res) {
                                    $finance_id = $finance_res['id'];
                                    ?>
                                    <tr>
                                        <td width="5%" align="center">
                                            <input type="checkbox" <?php if ($config_breadcrub[0]['finance_id'] == '-' || $config_breadcrub[0]['finance_id'] == '') {
                                echo '';
                            } else {
                                echo 'checked';
                            } ?>  id="finance" onclick="set_data('1');"  data-name="<?php echo $finance_id; ?>" value="<?php echo $finance_id; ?>"  >   
                                        </td>
                                        <td width="95%">
                                            <div >
                                                <label>
                                                    <div><?php echo $finance_res['accessory_name']; ?></div>
                                                </label>
                                            </div>
                                        </td>
                                        <td width="5%" style="display:none;">
                                            <div id="finacne_amt">
                                                <a href="#" data-toggle="modal" data-target="#emi_caulculator" onclick="$('#rate1').val('<?php echo $finance_res['price']; ?>'), $('#finance').val('<?php echo str_replace("%", "", $finance_res['id']); ?>')">Set EMI :</a>
        <?php
        $get_emi[0]['emi_amount'];
        echo indianFormatNumber($get_emi[0]['emi_amount']);
        ?> 
                                            </div>    
                                        </td>                                    
                                    </tr>                                            
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>    
                                    <td style="display:none;"></td>
                                    <td colspan="2">
                                        <div align="center" style="width:200px;"><label >Not Available</label></div>     
                                    </td>    
                                </tr>
    <?php
}
?>
                            <!--tr>
                                    <td width="50%">
                                        <div >
                                            <label style="float:right;">
                                            Finance EMI Amount
                                            </label>
                                        </div>
                                    </td>
                                    <td width="10%" id="emi_amount" colspan="2">
<?php echo $get_emi[0]['emi_amount']; ?>    
                                    </td>
<!--                                    <td width="10%">
                                        <input type="hidden" id="finance" onclick="set_data('1');" value="" >    
                                    </td>>
                            </tr-->    
                        </tbody>

                        <thead>
                            <tr>
                                <th width="5%"></th>
                                <th width="95%">Annual Maintenance Contract</th>
                                <th width="5%" style="display:none;">Estimate Price</th>                                

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($amc_contract)) {
                                foreach ($amc_contract as $amc_contract_res) {
                                    ?>
                                    <tr>
                                        <td width="5%" align="center">
                                            <?php ?>
                                            <input type="checkbox" <?php if ($config_breadcrub[0]['amc_id'] == '-' || $config_breadcrub[0]['amc_id'] == '') {
                                                echo '';
                                            } else {
                                                echo 'checked';
                                            } ?>  onclick="set_data('2');" id="amc" data-name="<?php echo $amc_contract_res["id"] ?>" value="<?php echo $amc_contract_res["id"] ?>"  >
                                        </td>
                                        <td width="95%">
                                            <div >
                                                <label>
                                                    <div ><?php echo $amc_contract_res['accessory_name']; ?></div>
                                                </label>
                                            </div>

                                        </td>
                                        <td width="5%" style="display:none;">
                                            <div id="amc_contract"><?php
                                            $amc_contract_res['price'];
                                            echo indianFormatNumber($amc_contract_res['price']);
                                            ?></div>    
                                        </td>

                                    </tr>                                            
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>    
                                    <td  colspan="2">
                                        <div align="center" style="width:200px;"><label >Not Available</label></div>     
                                    </td>    
                                    <td style="display:none;">                               
                                    </td>    
                                </tr>
    <?php
}
?>

                        </tbody>
                        <thead>
                            <tr>
                                <th width="5%" align="center"></th>
                                <th width="95%">Car Insurance</th>
                                <th width="5%" style="display:none;">Estimate Price</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($ins_car)) {
                                foreach ($ins_car as $ins_car_res) {
                                    ?>
                                    <tr>
                                        <td width="5%" align="center">
                                            <input type="checkbox" <?php if ($config_breadcrub[0]['insurance_id'] == '-' || $config_breadcrub[0]['insurance_id'] == '') {
                                        echo '';
                                    } else {
                                        echo 'checked';
                                    } ?>  onclick="set_data('3');" id="ins" data-name="<?php echo $ins_car_res["id"] ?>"  value="<?php echo $ins_car_res["id"] ?>" >
                                        </td>
                                        <td width="95%">
                                            <div >
                                                <label>


                                                    <div><?php echo $ins_car_res['accessory_name']; ?></div>
                                                </label>
                                            </div>

                                        </td>
                                        <td width="5%" style="display:none;">
                                            <div><?php
                                    echo indianFormatNumber($ins_car_res['price']);
                                    $ins_car_res['price'];
                                    ?>
                                            </div>    
                                        </td>                                    
                                    </tr>                                            
        <?php
    }
} else {
    ?>
                                <tr>    
                                    <td  style="display:none;">

                                    </td>    
                                    <td  colspan="2">
                                        <div align="center" style="width:200px;"><label >Not Available</label></div>     
                                    </td>    
                                </tr>
    <?php
}
?>

                        </tbody>
                        <thead>
                            <tr>
                                <th width="5%" align="center"></th>
                                <th width="95%">Extended Warranty</th>
                                <th width="5%" style="display:none;">Estimate Price</th>

                            </tr>
                        </thead>
                        <tbody>
<?php
if (!empty($extended_waranty)) {
    foreach ($extended_waranty as $extended_waranty_res) {
        ?>
                                    <tr>
                                        <td width="5%" align="center">
                                            <input type="checkbox" <?php if ($config_breadcrub[0]['extended_id'] == '-' || $config_breadcrub[0]['extended_id'] == '') {
            echo '';
        } else {
            echo 'checked';
        } ?> onclick="set_data('4');" id="extended" data-name="<?php echo $extended_waranty_res["id"] ?>"  value="<?php echo $extended_waranty_res["id"] ?>">
                                        </td>
                                        <td width="95%">
                                            <div >
                                                <label>


                                                    <div><?php echo $extended_waranty_res['accessory_name']; ?></div>
                                                </label>
                                            </div>

                                        </td>
                                        <td width="5%" style="display:none;">
                                            <div><?php
                                    echo indianFormatNumber($extended_waranty_res['price']);
                                    $extended_waranty_res['price'];
                                    ?></div>    
                                        </td>

                                    </tr>                                            
        <?php
    }
} else {
    ?>
                                <tr>    
                                    <td style="display:none;">

                                    </td>    
                                    <td  colspan="2">
                                        <div align="center" style="width:200px;"><label >Not Available</label></div>     
                                    </td>    
                                </tr>
    <?php
}
?>

                        </tbody>
                        <thead>
                            <tr>
                                <th width="5%" align="center"></th>
                                <th width="95%">Car Exchange</th>
                                <th width="5%" style="display:none;">Estimate Price</th>

                            </tr>
                        </thead>
                        <tbody>
<?php
//                            print_r($used_cars);
if (!empty($used_cars)) {
    foreach ($used_cars as $used_cars_res) {
        ?>
                                    <tr>
                                        <td width="5%" align="center">
                                            <input type="checkbox" <?php if ($config_breadcrub[0]['used_id'] == '-' || $config_breadcrub[0]['used_id'] == '') {
            echo '';
        } else {
            echo 'checked';
        } ?>  onclick="set_data('5');" id="used_car_id" data-name="<?php echo $used_cars_res["id"] ?>"  value="<?php echo $used_cars_res["id"] ?>">
                                        </td>
                                        <td width="95%">
                                            <div >
                                                <label>


                                                    <div><?php echo $used_cars_res['accessory_name']; ?></div>
                                                </label>
                                            </div>

                                        </td>
                                        <td width="5%" style="display:none;">
                                            <div><?php
                                    echo indianFormatNumber($used_cars_res['price']);
                                    $used_cars_res['price'];
                                    ?></div>    
                                        </td>

                                    </tr>                                            
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>    
                                    <td style="display:none;"></td>    
                                    <td  colspan="2">
                                        <div align="center" style="width:200px;"><label >Not Available</label></div>     
                                    </td>    
                                </tr>
    <?php
}
?>

                        </tbody>
                        <thead>
                            <tr>
                                <th width="5%" align="center"></th>
                                <th width="95%">Home Delivery</th>
                                <th width="5%" style="display:none;">Estimate Price</th>                                
                            </tr>
                        </thead>
                        <tbody>
<?php
//                            print_r($home_delivery);
if (!empty($home_delivery)) {
    foreach ($home_delivery as $home_delivery_res) {
        ?>
                                    <tr>
                                        <td width="5%" align="center">
                                            <input type="checkbox" <?php if ($config_breadcrub[0]['home_delivery'] == '-' || $config_breadcrub[0]['home_delivery'] == '') {
            echo '';
        } else {
            echo 'checked';
        } ?>  onclick="set_data('6');" id="home_delivery_id" data-name="<?php echo $home_delivery_res["id"] ?>"  value="<?php echo $home_delivery_res["id"] ?>">
                                        </td>
                                        <td width="95%">
                                            <div >
                                                <label>


                                                    <div><?php echo $home_delivery_res['accessory_name']; ?></div>
                                                </label>
                                            </div>

                                        </td>
                                        <td width="5%" style="display:none;">
                                            <div><?php
                            echo indianFormatNumber($home_delivery_res['price']);
                            $home_delivery_res['price'];
                            ?></div>    
                                        </td>

                                    </tr>                                            
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>    
                                    <td style="display:none;"></td>    
                                    <td  colspan="2">
                                        <div align="center" style="width:200px;"><label >Not Available</label></div>     
                                    </td>    
                                </tr>
    <?php
}
?>

                        </tbody>
                        <script>
                            function set_data(a)
                            {

                                if (a == 1)
                                {
                                    var finance_id = $('input[id="finance"]:checked').val();
//                         alert(finance_id);
//                         var finance_amt= $('#emi_amount').html();    
//                         $('#finance').val(finance_id);    
                                    $('#finance_id').val(finance_id);
                                }
                                if (a == 2)
                                {
                                    var amc = $('input[id="amc"]:checked').val();
//                         alert(amc);
                                    $('#amc_contract1').val(amc);

                                }
                                if (a == 3)
                                {
                                    var ins = $('input[id="ins"]:checked').val();

                                    $('#insurance_car').val(ins);

                                }
                                if (a == 4)
                                {
                                    var extended = $('input[id="extended"]:checked').val();

                                    $('#extended_war').val(extended);
                                }
                                if (a == 5)
                                {
                                    var extended = $('input[id="used_car_id"]:checked').val();

                                    $('#used_car').val(extended);
                                }

                                if (a == 6)
                                {
                                    var extended = $('input[id="home_delivery_id"]:checked').val();

                                    $('#home_delivery').val(extended);
                                }

                                //
                            }
                        </script>

                    </table>
                </div>
            </div>
        </div>
        <input type="hidden" id="headlights" value="<?php echo $accessory_cook; ?>"/>
        <div class="col-md-12">
            <div class="container-fluid">


                <div class="row">
                    <div class="col-md-12">
                        <!--<div class="clearfix">-->
                        <!--<div class="pull-right" style="padding-right: 15px;">-->
                        <form action="<?php echo base_url('car-booking/' . $main_id) ?>" method="POST" style="display:none;">
                            <input type="text" id="pro_id" name="pro_id" value="<?php echo $main_id; ?>"/>
                            <input type="text" id="finance_id" name="finance_id" value="<?php echo $config_breadcrub[0]['finance_id']; ?>"/>
                            <input type="text" id="amc_contract1" name="amc_contract1" value="<?php echo $config_breadcrub[0]['amc_id']; ?>"/>
                            <input type="text" id="insurance_car" name="insurance_car" value="<?php echo $config_breadcrub[0]['insurance_id']; ?>"/>
                            <input type="text" id="extended_war" name="extended_war" value="<?php echo $config_breadcrub[0]['extended_id']; ?>"/>
                            <input type="text" id="used_car" name="used_car" value="<?php echo $config_breadcrub[0]['used_id']; ?>"/>
                            <input type="text" id="home_delivery" name="home_delivery" value="<?php echo $config_breadcrub[0]['home_delivery']; ?>"/>

                            <button type="submit" id="select_data" name="submit" class="btn btn-maroon confuration" style="display:block;">Save and Continue</button>
                        </form>                        
                        <!--</div>-->
                        <script>
                            function saveconfiure()
                            {
                                var selected_ids = $('#selected_ids').val();
                                if (selected_ids == '')
                                {
                                    exit;
                                }
                                else
                                {
                                    var first = "<div align='center'><img src='<?php echo base_url() . 'assets/img/loader.gif'; ?>'/></div>";
                                    $('#accessory_mess').html(first);
                                    document.getElementById('error_model_link').click();
                                    var selected_ids = $('#selected_ids').val();
                                    var info_details = $('#info_details').val();
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo site_url('stitcher/check_acc_confiuration') ?>",
                                        data: {selected_ids: selected_ids, info_details: info_details}
                                    }).done(function(html) {
                                        $('#accessory_mess').html(html);
                                        var count = $('#count').val();
                                        if (count == 0)
                                        {
                                            document.getElementById('select_data').click();
                                        }
                                    });
                                    return false;
                                }
                            }
                        </script> 
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /.container  -->
<script>
    $(document).ready(function() {
        $(".high1").click(function() {
            var feature_name = "";
            var final_details2 = "";
            $("input.high1:checked").each(function() {
                var id = $(this).data('name');
                feature_name += $(this).data('name') + ', ';
                var details = document.getElementById('results' + id).innerHTML;
                var details2 = details.trim();
                var final_details = details2 + ',-,' + id;
                final_details2 += final_details + '---';
            });
            document.getElementById('selected_ids').value = feature_name;
            document.getElementById('info_details').value = final_details2;
        });
    });
    function allexterior()
    {
        var headlights = $('#headlights').val();
        document.cookie = "accessories=" + headlights;
    }

</script>
<!-- Login Modal-->
<a data-target="#error_model" data-toggle="modal" id="error_model_link" style="display:none;" href="#">Error</a>
<div class="modal fade" id="error_model">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Accessory Configuration Process</h4>
            </div>
            <div class="modal-body" id="accessory_mess">                
            </div>
        </div> 
    </div>
</div>
