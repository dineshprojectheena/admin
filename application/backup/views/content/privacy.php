<div class="container">
    <div class="row">
        <div class="col-md-12">
<p align="center">
    <strong>Privacy Policy</strong>
</p>
<p align="justify">
    <strong>1. Privacy</strong>
</p>
<p align="justify">
    1.1 The website <a href="http://www.mynewcar.in">http://www.mynewcar.in</a> (the “<strong>Website</strong>”) is operated by Dreamz Mynewcar India Private
    Limited (“<strong>Company</strong>”, “<strong>we</strong>”, “<strong>us</strong>”, “<strong>our</strong>”). We are committed to protecting your privacy and
    we will only use the information we collect about you lawfully and in accordance with the Information Technology Act, 2011, the Information Technology
    (Reasonable security practices and procedures and sensitive personal data or information) Rules, 2011 and other applicable laws of the Republic of India.
</p>
<p align="justify">
    1.2 This Privacy Policy (the “<strong>Policy</strong>”) relates solely to the information collection and use practices of our Website, and any individual
    websites or merchant-specific, city-specific, or state-specific websites now or hereinafter contained within or otherwise available through external
hyperlinks with our Website (the “<strong>Minisites</strong>”). We recognize that many visitors and users of this Website and/or the Minisites (“    <strong>User(s</strong>)”) are concerned about the information they provide to us, and how we treat that information. This Policy, which may be updated
    from time to time, has been developed to address those concerns. We encourage you to review the Privacy Policy, and become familiar with it.
</p>
<p align="justify">
    1.3 Your use of this Website indicates to us that you have read and accept our privacy practices, as outlined in this Policy.
</p>
<p align="justify">
    <strong>2. Types of Information Collected</strong>
</p>
<p align="justify">
    2.1 We collect two types of information about our Website Users: Personally Identifiable Information and Non-Personally Identifiable Information.
</p>
<p align="justify">
    2.2 Our Policy ensures that any information you provide us remains private and secure. To reassure you, below we provide details of the information you
    provide to us, how it will be used and more importantly, will not be used. We will never collect sensitive information about you without your explicit
    consent. The information we hold will be accurate and up to date. You shall also have the right to review the information you have provided and ensure that
    that any personal information or sensitive personal data or information is accurate. In the event, you find the personal information or sensitive personal
    data to be inaccurate or deficient, you shall bring such inaccuracy or deficiency to our attention and we will carry out such corrections or amendments as
    feasible. However, we shall not be responsible for the authenticity of the personal information or sensitive personal data supplied by you.
</p>
<p align="justify">
    <strong>3. Personally Identifiable Information</strong>
</p>
<p align="justify">
    3.1 Personally Identifiable Information (“<strong>PII</strong>”) is information that identifies a specific end user. When you engage in certain activities
    on the Website, such as creating an account, ordering a product or service from the vendors or service provider, submitting content and/or posting content
    in discussion forums, entering a promotion sponsored by the vendors/service providers, filling out a survey, posting a review, sending feedback to us or
    the vendors or the service providers, requesting information about our services (collectively, “<strong>Identification Activities</strong>”), we may ask
    you to provide certain information about yourself. It is optional for you to engage in an Identification Activity.
</p>
<p align="justify">
    3.2 If you elect to engage in an Identification Activity, we may ask you to provide us with certain personal information about yourself, such as your first
    and last name, mailing address, email address, cell number, telephone number and date of birth.
</p>
<p align="justify">
    3.3 When you order products or services using our Website, we may also ask you to provide us with financial information, including but not limited to, your
    credit card or debit card number, expiration date and authentication codes or related information.
</p>
<p align="justify">
    3.4 Depending on the activity, some of the information we ask you to provide is identified as mandatory and some is identified as voluntary. If you do not
    provide the mandatory information for a particular activity that requires it, you will not be permitted to engage in that activity.
</p>
<p align="justify">
    3.5 We use PII to provide products and services to you, administer promotions, enhance the operation of the Website, improve our marketing and promotional
    efforts, analyze Website use, improve our product and service offerings, and to tailor your experience with the vendors or the service providers. We may
    also use PII to troubleshoot, resolve disputes, accomplish administrative tasks, contact you, enforce our agreements with you, including our Terms of Use,
    and this Policy, comply with applicable law, and cooperate with law enforcement activities.
</p>
<p align="justify">
    3.6 In the event there is any change in your PII like your address, phone number or email ID, you may send us intimation about the changes and we will make
    the necessary amendment in our records.
</p>
<p align="justify">
    3.7 You may also withdraw your consent, authorizing the Company to use and collect, disseminate and analyze your PII. Such withdrawal of consent must be
    sent to the Company in writing. However, once such consent is withdrawn, the Company will no longer provide the services to the user.
</p>
<p align="justify">
    <strong>4. Non-Personally Identifiable Information</strong>
</p>
<p align="justify">
    4.1 Non-Personally Identifiable Information (“<strong>NPII</strong>”) is information that does not identify a specific end user. This type of information
    may include things like the Uniform Resource Locator (“<strong>URL</strong>”) of the website you visited before coming to our Website, the URL of the
    website you visit after leaving our Website, the type of browser you are using and your Internet Protocol (“<strong>IP</strong>”) address.
</p>
<p align="justify">
    4.2 We, and/or our authorized vendors and the third party service providers that perform function on our behalf like selling of products or services, may
    automatically collect this information when you visit our Website through the use of electronic tools like Cookies and Web beacons or Pixel tags, as
    described elsewhere in this Policy.
</p>
<p align="justify">
    4.3 We use NPII to troubleshoot, administer the Website, analyze trends, gather demographic information, comply with applicable law, and cooperate with law
    enforcement activities. We may also share this information with third party service providers to measure the overall effectiveness of our online
    advertising, content, and programming.
</p>
<p align="justify">
    <strong>5. Sources of Information</strong>
</p>
<p align="justify">
    5.1 If you choose to become a registered member of this Website, you must provide your name, address, telephone or cell phone number, e-mail address, a
    unique login name, password, and password validation, and a password hint to help you remember your password. This information is collected on the
    registration form for several reasons including but not limited to the following:
</p>
<ol type="i">
    <li>
        <p align="justify">
            Personal identification purposes.
        </p>
    </li>
    <li>
        <p align="justify">
            Completing purchases.
        </p>
    </li>
    <li>
        <p align="justify">
            Allowing us or the vendors to contact you for customer service purposes.
        </p>
    </li>
    <li>
        <p align="justify">
            If necessary, customizing the content of this Website to strive to meet your specific needs.
        </p>
    </li>
    <li>
        <p align="justify">
            For making product or other improvements to our site.
        </p>
    </li>
</ol>
<p align="justify">
    5.2 In addition, we need your e-mail address to confirm your new member registration and each transaction you conduct on our site. As a registered member
    of this Website, you will be entitled to savings and benefits on deals. You could, in addition, look forward to receiving by e-mail monthly newsletters,
    daily promotions offering special deals, updates for new services, and other noteworthy items. However, you may choose at any time to no longer receive
    these types of e-mail messages. Please see our Opt-Out Policy described below for details.
</p>
<p align="justify">
    <strong>6. Member Profile</strong>
</p>
<p align="justify">
    6.1 As a registered member of this Website, you can choose to complete your online profile by providing us with personal preferences and other personal
    information. This information is primarily used to assist you in making purchases quickly without having to type in the same information repeatedly.
    Further, please note that neither the Website nor the Company stores any of your credit/debit card or billing information.
</p>
<p align="justify">
    <strong>7. Online Surveys</strong>
</p>
<p align="justify">
    7.1 We value opinions and comments from members, so we may conduct online surveys. Participation in these surveys is entirely optional. Typically, the
    information is aggregated, and used to make improvements to our Website and the services and to develop appealing content, features and promotions for our
    Website members. Survey participants are anonymous unless otherwise stated in the survey.
</p>
<p align="justify">
    <strong>8. Promotions</strong>
</p>
<p align="justify">
    8.1 We may sponsor promotions to give our Website members the opportunity to win prizes. Information collected by us for such activities may include
    contact information and survey questions. We use contact information to notify the winners and survey information to develop promotions and product
    improvements to our Website.
</p>
<p align="justify">
    <strong>9. Automatic Logging of Session Data</strong>
</p>
<p align="justify">
    9.1 We automatically log generic information about your computer’s connection to the Internet, which we call “session data”, that is anonymous and not
    linked to any personal information. Session data consists of things such as the IP address, operating system and type of browser software being used and
    the activities conducted by the user while on our Website.
</p>
<p align="justify">
    9.2 We collect session data because it helps us analyze such things as what items visitors are likely to click on most, the way visitors are clicking
    through the Website, how many visitors are surfing to various pages on the Website, how long visitors to the Website are staying and how often they are
    visiting. It also helps us diagnose problems with our servers and lets us better administer our systems. Although such information does not identify any
    visitor personally, it is possible to determine from an IP address a visitor’s Internet Service Provider (ISP), and the approximate geographic location of
    his or her point of connectivity.
</p>
<p align="justify">
    <strong>10. Cookies</strong>
</p>
<p align="justify">
    10.1 “Cookies” are small pieces of information that are stored by your browser on your computer’s hard drive. The use of cookies is very common on the
    Internet and our Website’s use of cookies is similar to that of other reputable online companies.
</p>
<p align="justify">
    10.2 Our Website uses cookies to personalize your experience on the Website, and with respect to advertisements. As to the former, these types of cookies
    allow you to log in without having to type your log - in name each time (only your password is needed). We may also use such cookies to display an
    advertisement to you while you are on the Website.
</p>
<p align="justify">
    10.3 Our Website supports your right to block any unwanted internet activity, especially that of unscrupulous websites. However, blocking our Website’s
    cookies may disable certain features on the Website, and may make it impossible to purchase or use certain services available on the Website. Please note
    that it is possible to block cookie activity from certain websites while permitting cookies from sites you trust.
</p>
<p align="justify">
    <strong>11. Compliance with law</strong>
</p>
<p align="justify">
    11.1 In addition to the circumstances described above, we may disclose our Website member information if required to do so by law, court order, as
    requested by other government or law enforcement authority, or in the good faith belief that disclosure is otherwise necessary or advisable including,
    without limitation, to protect the rights or properties of the Company or any or all of its subsidiaries and associates, and its employees, directors or
    officers, or when we have reason to believe that disclosing the information is necessary to identify, contact or bring legal action against someone who may
    be causing interference with our rights or properties, whether intentionally or otherwise, or when anyone else could be harmed by such activities. In
    addition, if the Company or substantially all of its assets are acquired, our customer information will most likely also be transferred in connection with
    such acquisition.
</p>
<p align="justify">
    11.2 Apart from the above, we may collect information from you for providing our complete range of services, interacting with Website Users for customer
    feedback and resolving customer queries and issues, processing requests for signing up for special offers and maintenance of Website.
</p>
<p align="justify">
    <strong>12. Sharing of PII</strong>
</p>
<p align="justify">
    We will not share your Personally Identifiable Information with other parties except as provided below:
</p>
<p align="justify">
    12.1 We provide our services through third parties vendors and service providers. The said third party vendors and service providers perform functions on
    our behalf, like sending out and distributing the product and services.
</p>
<p align="justify">
    12.2 We may share your PII with the said third party vendors and/or service providers to fulfil orders, deliver packages, send post, text messages or
    email, administer promotions, remove repetitive information on customer lists, analyze data, provide marketing assistance, provide search results and
    links, process credit / debit card payments, operate the Website, troubleshoot, and provide customer service.
</p>
<p align="justify">
    12.3 Please note that as part of any PII sharing which we list above, we may send your information internationally. Some places outside of India may not
    have adequate data protection laws at all or may offer differing levels of protection of personal information which are not as high as in India.
</p>
<p align="justify">
    12.4 We may also collect personal information from individuals and entities with whom we have business relationships (“<strong>Business Associates</strong>
    ”) and may have to also share their information with third party vendors and service providers to accomplish our administrative tasks. For example, when
    you order a service, we release your financial (credit card, debit card, net banking, mobile payment, etc) information to the card-issuing bank/provider to
    confirm payment for the service and, if applicable, release your address to the vendors to deliver the product or the service. We encourage third party
    vendors and service providers to adopt and post privacy policies. Nonetheless, any third party vendor or service provider, or business associate receiving
    the PII from us will not disclose it further.
</p>
<p align="justify">
    12.5 We may also disclose PII when we believe release is appropriate to comply with the law or a court order to enforce or apply this Policy, our Terms of
    Use or other agreements; or protect the rights, property or safety of the Website, its Users or others.
</p>
<p align="justify">
    <strong>13. Opt-Out policy</strong>
</p>
<p align="justify">
    13.1 At any time, you may unsubscribe from our alert email or text message or other newsletter or similar services through the link on our Website. As a
    member, you will occasionally receive e-mail updates from us about special offers, new services, and other noteworthy items. If you do not wish to receive
    them, please click on the “unsubscribe” link or follow the instructions in each email message. We reserve the right to limit membership to those who will
    accept such emails. Members will be notified via email prior to any actions taken.
</p>
<p align="justify">
    <strong>14. Data Security</strong>
</p>
<p align="justify">
    14.1 We take reasonable steps to maintain the security of the PII that we collect, including limiting the number of people who have physical access to our
    database servers, as well as installing electronic security systems that guard against unauthorized access. We have a comprehensive document information
    security program and information security policies that contain managerial, technical, operational and physical security control measures that are
    commensurate with the information assets being protected. However, no data transmission over the internet can be guaranteed to be completely secure.
    Accordingly, we cannot ensure or warrant the security of any information that you transmit to us, so you do so at your own risk.
</p>
<p align="justify">
    14.2 The PII we hold will be held within our systems in accordance with our internal security policy and the law. We hold data until 3 years after it is
    last accessed or used by us.
</p>
<p align="justify">
    <strong>15. Payment Security</strong>
</p>
<p align="justify">
    15.1 All payments on our Website are secured. This means all personal information you provide on the Website is transmitted using SSL (Secure Socket Layer)
    encryption. SSL is a proven coding system that lets your browser automatically encrypt, or scramble, data before you send it to us. The same process is
    replicated when you make purchases on our Website.
</p>
<p align="justify">
    <strong>16. Disclaimer</strong>
</p>
<p align="justify">
    16.1 Our Website contains links to other web sites. Please note that when you click on one of these links, you are entering another web site for which we
    have no responsibility. We encourage you to read the privacy statements of all such sites as their policies may be materially different from this Policy.
    Of course, you are solely responsible for maintaining the secrecy of your passwords, and your Website membership account information.
</p>
<p align="justify">
    16.2 This Policy is effective as of 08<sup>th</sup> of December, 2014. Any material changes in the way we use personal information will be described in
    future versions of this Policy. Of course, you may always submit concerns regarding our Policy or our privacy practices via email to contact@mynewcar.in
    Please reference the policy in your subject line. We will attempt to respond to all of your reasonable concerns or inquiries within 10 business days of
    receipt of your email.
</p>


        </div>
    </div>
</div>