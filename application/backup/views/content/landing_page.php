
<?php
$end_date = explode('-', $landing_data['end_date']);

$monthNum = $end_date[1];
$monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
$end_date2 = explode(' ', $end_date['2']);
//print_r($end_date2);
?>



<div class="container">
    <div class="row" style="margin:1px -11px;">
        <div class="col-md-12 landing_data2" style="<?php if (!empty($landing_data['background_image'])) { ?>background-image:url(<?php echo img_url('uploads/back_image/' . $landing_data['background_image']) ?>);<?php } else { ?> background-color:<?php
            echo $landing_data['background_color'];
        }
        ?>;background-repeat: no-repeat;height:100%;background-size:100% 100%;">
            <div class="col-md-12" style="background-color: white;opacity: 0.9;margin-bottom: 5px;margin-top: 5px;border:0px solid #DDDDDD;">
                <h5 class="page-heading2" style="text-align: center;
                    color:<?php
                    if (!empty($landing_data['page_heading_color'])) {
                        echo $landing_data['page_heading_color'];
                    } else {
                        echo 'black';
                    }
                    ?>;
                    font-size:<?php
                    if (!empty($landing_data['page_heading_Size'])) {
                        echo $landing_data['page_heading_Size'] . 'px';
                    } else {
                        echo '20px';
                    }
                    ?>;
                    "><?php echo $landing_data['title']; ?>
                </h5>
            </div>
            <div class="col-md-12" style="margin-bottom: 5px;margin-top: 5px;">
                <div class="col-md-8">
                    <div align="center">
                        <?php
                        if (!empty($landing_data['main_video'])) {
                            ?>

                            <iframe width="615" height="350" style="padding-top:10px;"  src="<?php echo $landing_data['main_video']; ?>" frameborder="0" allowfullscreen></iframe>
                            <?php
                        } else {
                            ?>                    
                            <img class="img-responsive"  src="<?php echo img_url('uploads/landing_main_image/' . $landing_data['main_image'])  ?>">
                            <?php
                        }
                        ?>

                    </div>    
                </div>    
                <div class="col-md-4" style="background-color:white;opacity: 0.9;border:2px solid #DDDDDD;">
                    <div class="offset-client">
                        <div class="countdown styled"></div>
                    </div>
                    <div>
                        <form class="form-inline" action="<?php echo base_url(); ?>landing_page/landing_data" method="POST" >
                            <input type="hidden" placeholder="" id="url" name="url" class="form-control" value="<?php echo $landing_data['name']; ?>">
                            <?php
                            if (!empty($landing_data['form_heading'])) {
                                ?>
                            <div class="col-md-12 show-grid " style="padding:1px 10px">    
                                    <h5 class="page-heading"><?php echo $landing_data['form_heading']; ?></h5>
                                </div>

                                <?php
                            }
                            ?>
                            <?php
                            $exp_text_fields = explode(',', $landing_data['text_fields']);
                            foreach ($exp_text_fields as $exp_text_fields_data) {
                                ?>
                                <?php
                                if ($exp_text_fields_data == 1) {
                                  $d=1;  
                                    ?>
                                    <div class="col-md-12 show-grid ">    
                                        <div class="col-md-5">    
                                            <label for="name">First Name</label>
                                        </div>
                                        <div class="col-md-7">        
                                            <input type="text" placeholder="" id="text1" name="text1"  class="form-control col-md-4" required>
                                        </div>
                                    </div>

                                    <?php
                                }
                                ?>
                                <?php
                                if ($exp_text_fields_data == 2) {
                                    $d=2;
                                    ?>
                                    <div class="col-md-12 show-grid ">    
                                        <div class="col-md-5">    
                                            <label for="name">Last Name</label>
                                        </div>
                                        <div class="col-md-7">        
                                            <input type="text" placeholder="" id="text01" name="text01"  class="form-control col-md-4" required>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?php
                                if ($exp_text_fields_data == 3) {
                                    $d=3;
                                    ?>
                                    <div class="col-md-12 show-grid ">    
                                        <div class="col-md-5">    
                                            <label for="name">Contact Number</label>
                                        </div>
                                        <div class="col-md-7">        
                                            <input type="text" placeholder="" id="text2" name="text2"  class="form-control col-md-4" required onChange='checkNum(this)'>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?php
                                if ($exp_text_fields_data == 4) {
                                    $d=4;
                                    ?>
                                    <div class="col-md-12 show-grid ">    
                                        <div class="col-md-5">    
                                            <label for="name">Email ID</label>
                                        </div>
                                        <div class="col-md-7">        
                                            <input type="text" placeholder="" id="text3" name="text3"  class="form-control col-md-4" required onblur="checkEmail(this.value)">
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>



                                <?php
                                if ($exp_text_fields_data == 5) {
                                    $d=5;
                                    ?>

                                    <?php
                                }
                                ?>






                                <?php
                            }
                            ?>
                            
                            
                            
                            
                            <div class="col-md-12 show-grid ">    
                                <div class="col-md-5">    
                                    <label for="name">City</label>
                                </div>
                                <div class="col-md-7">  
                                    <?php
                                    
                                    ?>
                                    <select class="form-control col-md-4" style="min-width: 180px;" id="landing_city" name="landing_city" required>
                                        <option value="">Select City</option>
                                        <?php
                                        foreach ($all_Citys as $all_Citys_res) {
                                            ?>
                                            <option <?php
                                            if ($_GET['city'] != '') {
                                                setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
                                                $main_city = $_GET['city'];
                                            }

                                            if ($main_city == '') {
                                                $main_city = '1';
                                                if ($main_city == $all_Citys_res['city_id']) {
                                                    echo 'Selected';
                                                }
                                            } else {
                                                if ($main_city == $all_Citys_res['city_id']) {
                                                    echo 'Selected';
                                                }
                                            }
                                            $main_city_name = $all_Citys_res['name'];
                                            ?> value="<?php echo $all_Citys_res['name']; ?>"><?php echo $all_Citys_res['name']; ?></option>
                                                <?php
                                            }
                                            ?>

                                    </select>
                                </div>
                            </div>
                            <?php
                            if ($d==5) {
                                ?>
                                <div class="col-md-12 show-grid ">    
                                    <div class="col-md-5">    
                                        <label for="name">Book A Test Drive</label>
                                    </div>
                                    <div class="col-md-7">        
                                        <input type="checkbox" placeholder="" id="text5" name="text5"  class="form-control col-md-4" required >
                                    </div>
                                </div>
                                <?php
                            }
                            ?>


                            <div class="col-md-12 show-grid " align='center' style="margin-bottom: 10px;"> 
                                <?php
                                $exp_button_fields = explode('--', $landing_data['button_fields']);
//                                print_r($exp_button_fields);
                                $i = 1;
                                foreach ($exp_button_fields as $exp_button_fields_data) {
                                    ?>    
                                    <?php
                                    if ($i == 1 && $exp_button_fields_data != '') {
                                        ?>
                                        <button class="btn btn-maroon" type="submit"><?php echo $exp_button_fields_data; ?></button>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if ($i == 2 && $exp_button_fields_data != '') {
                                        ?>

                                        <button class="btn btn-maroon" type="reset"><?php echo $exp_button_fields_data; ?></button>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    $i++;
                                }
                                ?>                                
                            </div>
                        </form>  
                    </div>                     
                </div>     
            </div>

            <div class="col-md-12">
                <div class="col-md-8">

                </div>
                <div class="col-md-4" align="right"  style="/* background-color:white; *//* opacity: 0.9; *//* border:0px solid #DDDDDD; */">
                    <?php
                    $exp_icon = explode(',', $landing_data['icons']);
                    foreach ($exp_icon as $exp_icon_data) {
                        ?>    
                        <?php
                        if ($exp_icon_data == 1) {
                            ?>    
                            <img style="/* border-radius: 20px; */background-color: white;padding: 3px;opacity: 0.8;" src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carinsurance_hotdeals.png'; ?>" data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Insurance Offers" />    
                            <?php
                        }
                        ?>

                        <?php
                        if ($exp_icon_data == 2) {
                            ?>    
                            <img style="/* border-radius: 20px; */background-color: white;padding: 3px;opacity: 0.8;" src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carfinance_hotdeals.png' ?>"  data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Finance Offers"/>			
                            <?php
                        }
                        ?>

                        <?php
                        if ($exp_icon_data == 3) {
                            ?>    
                            <img style="/* border-radius: 20px; */background-color: white;padding: 3px;opacity: 0.8;" src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carexchange.png' ?>" data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Exchange  Offers"/>                       
                            <?php
                        }
                        ?>

                        <?php
                        if ($exp_icon_data == 4) {
                            ?>    
                            <img style="/* border-radius: 20px; */background-color: white;padding: 3px;opacity: 0.8;" src="<?php echo base_url() . 'assets/img/product/otherbenefits.png' ?>"data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="top" title="Car Gift"/>
                            <?php
                        }
                        ?>

                        <?php
                    }
                    ?>
                </div>
            </div>          

            <div class="col-md-12" style="background-color: white;opacity: 0.9;margin-bottom: 5px;margin-top: 5px;border:0px solid #DDDDDD;">
                <p class="page-heading2" style="font-size:15px;color:<?php
                    if (!empty($landing_data['page_heading_color'])) {
                        echo $landing_data['page_heading_color'];
                    } else {
                        echo 'black';
                    }
                    ?>;">
                    <?php echo $landing_data['Description'];?>
<!--                <div id="i1" style="margin-bottom: 30px;">    
                    <?php
                    $exp_desc = explode('.', $landing_data['Description']);
                    echo $exp_desc[0] . '.' . $exp_desc[1] . '......<a href="#" onclick="show(1);">( + More )</a>';
                    ?>
                </div>    
                <div id="i2" style="display:none;margin-bottom: 30px;">    
                    <?php
                    echo $landing_data['Description'];
                    echo '.<a href="#" onclick="show(2);">( - Less )</a>';
                    ?>
                </div>    -->
                </p>
            </div>






        </div>


        <?php
        if ($landing_data['testimonials_status'] == 1) {
            $dis = 'block;';
            $align = 'right';
        } else {
            $dis = 'none;';
            $align = 'left';
        }

        if ($landing_data['video_status'] == 1) {
            $dis2 = 'block;';
            $div = 'col-md-6';
        } else {
            $dis2 = 'none;';
            $div = 'col-md-12';
        }
        ?>
        <div class="col-md-12" style="background-color:white;opacity:0.8;margin-bottom: 5px;margin-top:5px;border:0px solid #DDDDDD;">
            <div class="<?php echo $div; ?>" style="height:auto;display: <?php echo $dis; ?>;">            
                <div class="col-md-4">
                    <h5 class="page-heading2" style="font-size:16px;color:<?php
        if (!empty($landing_data['page_heading_color'])) {
            echo $landing_data['page_heading_color'];
        } else {
            echo 'black';
        }
        ?>;">
                        <i class="fa fa-weixin" style="font-size:16px;float:left;"></i>
                        <span style="float:left;">&nbsp;Testimonials</span>
                    </h5>                            
                </div>
                <!--                    <div class="col-md-8">
                                        <div class="pull-right controls" style="padding:0;  margin: 10px;">
                                            <a href="#carousel-example-generic" data-slide="prev" id="prev">
                                                <i class="fa fa-chevron-circle-left fa-2x"></i>
                                            </a>
                                            <a href="#carousel-example-generic" data-slide="next" id="next">
                                                <i class="fa fa-chevron-circle-right fa-2x"></i>
                                            </a>
                                        </div>
                                    </div>-->


                <div class="col-md-12 ">                        
                    <div data-ride="carousel" class="carousel slide" id="carousel-example-generic">                    
                        <div role="listbox" class="carousel-inner ">  
                            <div class="item" align="center">
                                <div class="col-md-12">
                                    <div class="col-md-2 col-sm-2 text-center" style="top: 15px;">
                                        <a href="https://www.facebook.com/mynewcar.in/photos/pb.346076378891506.-2207520000.1427369012./450744768424666/?type=3&amp;theater" style="color:black;">
                                            <img class="img-circle img-center img-responsive" style="width:100px;" src="http://localhost/mynewcar//assets/img/testimonial/customer.jpg"></a>
                                    </div>    
                                    <div class="col-md-10 col-sm-10">
                                        <p class="page-heading2" style="text-align:justify;margin-bottom: 30px;font-size:15px;">
                                            I am very happy with the experience I had on MyNewCar website. Easily booked my dream car by just paying a small booking amount! The team was very responsive and had several options to choose from.
                                            <br>
                                            <strong class="et_pb_testimonial_author"  style="float:left;"><a href="https://www.facebook.com/mynewcar.in/photos/pb.346076378891506.-2207520000.1427369012./450744768424666/?type=3&amp;theater" style="color:black;">Anil Sharma</a></strong>
                                        </p>

                                    </div>    
                                </div>
                            </div>
                            <div class="item active" align="center">
                                <div class="col-md-12">
                                    <div class="col-md-2 col-sm-2 text-center"  style="top: 15px;">
                                        <a href="https://www.facebook.com/mynewcar.in/photos/a.377688679063609.1073741829.346076378891506/456344374531372/?type=1" style="color:black;">
                                            <img class="img-circle img-center img-responsive" style="width:100px;" src="http://localhost/mynewcar//assets/img/testimonial/Nishanth Hiremath.jpg"></a>
                                    </div>    
                                    <div class="col-md-10 col-sm-10">
                                        <p class="page-heading2" style="text-align:justify;margin-bottom: 30px;font-size:15px;">
                                            I am very happy with the experience I had on MyNewCar website. Easily booked my dream car by just paying a small booking amount! The team was very responsive and had several options to choose from.
                                            <br>
                                            <strong class="et_pb_testimonial_author"  style="float:left;"><a href="https://www.facebook.com/mynewcar.in/photos/pb.346076378891506.-2207520000.1427369012./450744768424666/?type=3&amp;theater" style="color:black;">Anil Sharma</a></strong>
                                        </p>
                                    </div>    
                                </div>      
                            </div>



                        </div>

                    </div>
                </div>            

            </div>
            <div class="<?php echo $div; ?>" style="height:auto;display:<?php echo $dis2; ?>"> 
                <div style="padding:8px 5px 2px 10px;" align="<?php echo $align; ?>">
                    <object>
                        <param name="movie" value="//www.youtube.com/v/7Rxm0L-OsoE?hl=en_GB&amp;version=3&amp;rel=0">
                        <param name="allowFullScreen" value="true">
                        <param name="allowscriptaccess" value="always">
                        <embed height="200" width="100%"  src="//www.youtube.com/v/7Rxm0L-OsoE?hl=en_GB&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true">
                    </object>
                </div>   

            </div>    
        </div>

        <div class="col-md-12" style="background-color:white;opacity:0.8;margin-bottom: 5px;border:0px solid #DDDDDD;">
            <a href="<?php echo base_url(); ?>/brand-deal">
                <img class="img-responsive" style="background-color:white;" src="<?php echo base_url(); ?>/assets/img/slider/6USP.png">
            </a>   

        </div>
    </div> <!-- /.container -->

    <div class="row" style="padding:10px;">
        <div class="col-md-12" >                         
            <div class="social" style="float:right;" >
                <a href="https://www.facebook.com/pages/MYNEWCAR/346076378891506" style="margin: 10px;" class="fb tiphere col-md-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a href="https://twitter.com/mynewcarindia" class="tw tiphere col-md-2" style="margin: 10px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a href="https://plus.google.com/109934705518060003143/about" style="margin: 10px;" class="gp tiphere col-md-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a href="https://www.linkedin.com/company/mynewcar-india" style="margin: 10px;" class="ln tiphere col-md-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="LinkedIn">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
            </div>
        </div>            

    </div> 
    
    <style>
    .show-grid {
        /*border: 1px solid rgba(86, 61, 124, 0.2);*/
        padding:10px;
    }

    .page-heading2 {
        font-size: 20px;
        /*font-weight: bold;*/
        padding: 10px 5px 10px 5px;
    }


    .offset-client
    {
        color: #000;
        background-color: #DDDDDD;
        border-color: #A81E27;
        letter-spacing: 0.0px;
        padding: 10px 00px;
        margin-bottom: 5px;
        margin-top: 5px;
        opacity: 0.7;
        font-size: 15px;
        font-weight: bold;
        /*border-radius: 5px;*/
        text-align: center;   
    }
    .form-control
    {
        border-radius: 0px !important;    
    }


</style>


<script type="text/javascript">
    function show(a)
    {
        if (a == 1)
        {
            $('#i1').css('display', 'none');
            $('#i2').css('display', 'block');
        }
        if (a == 2)
        {
            $('#i1').css('display', 'block');
            $('#i2').css('display', 'none');
        }
    }



    $(function () {
        var endDate = "<?php echo $monthName; ?> <?php echo $end_date2[0]; ?>, <?php echo $end_date[0]; ?> <?php echo $end_date2[1]; ?>";

                $('.countdown.simple').countdown({date: endDate});

                $('.countdown.styled').countdown({
                    date: endDate,
                    render: function (data) {
                        $(this.el).html("" + this.leadingZeros(data.days, 2) + " <span> days </span>" + this.leadingZeros(data.hours, 2) + " <span>hrs </span>" + this.leadingZeros(data.min, 2) + "  <span>mins </span>" + this.leadingZeros(data.sec, 2) + "<span> seconds left</span>");

                        if (this.leadingZeros(data.days, 2) == 0 && this.leadingZeros(data.hours, 2) == 0 && this.leadingZeros(data.min, 2) == 0 && this.leadingZeros(data.sec, 2) == 0)
                        {
                            $('.offset-client').css('display', 'none');
                        }
                    }
                });

                $('.countdown.callback').countdown({
                    date: +(new Date) + 10000,
                    render: function (data) {
                        $(this.el).text(this.leadingZeros(data.sec, 2) + " sec");
                    },
                    onEnd: function () {
                        $(this.el).addClass('ended');
                    }
                }).on("click", function () {
                    $(this).removeClass('ended').data('countdown').update(+(new Date) + 10000).start();
                });

                // End time for diff purposes
                var endTimeDiff = new Date().getTime() + 15000;
                // This is server's time
                var timeThere = new Date();
                // This is client's time (delayed)
                var timeHere = new Date(timeThere.getTime() - 5434);
                // Get the difference between client time and server time
                var diff_ms = timeHere.getTime() - timeThere.getTime();
                // Get the rounded difference in seconds
                var diff_s = diff_ms / 1000 | 0;

                var notice = [];
                notice.push('Server time: ' + timeThere.toDateString() + ' ' + timeThere.toTimeString());
                notice.push('Your time: ' + timeHere.toDateString() + ' ' + timeHere.toTimeString());
                notice.push('Time difference: ' + diff_s + ' seconds (' + diff_ms + ' milliseconds to be precise). Your time is a bit behind.');

                $('.offset-notice').html(notice.join('<br />'));

                $('.offset-server .countdown').countdown({
                    date: endTimeDiff,
                    offset: diff_s * 1000,
                    onEnd: function () {
                        $(this.el).addClass('ended');
                    }
                });

                $('.offset-client .countdown').countdown({
                    date: endTimeDiff,
                    onEnd: function () {
                        $(this.el).addClass('ended');
                    }
                });

            });
</script>



    <script>
        float_req_hide();
        function float_req_hide()
        {
            var dis="<?php echo $landing_data['timmer'];?>";
//            alert(dis);
            if(dis==1)
            {
            $('.offset-client').css('display', 'block');    
            }
            else if(dis==2)
            {
            $('.offset-client').css('display', 'none');    
            }
            
            $('#flip').css('display', 'none')
            $('.selected_city').css('display', 'none')
        }
        function checkNum(x)
        {
            var s_len = x.value.length;
            var s_charcode = 0;
            for (var s_i = 0; s_i < s_len; s_i++)
            {
                s_charcode = x.value.charCodeAt(s_i);
                if (!((s_charcode >= 48 && s_charcode <= 57)))
                {
                    //alert("Only Numeric Values Allowed");

                    x.value = '';
                    x.focus();
                    return false;
                }
            }
            return true;
        }
        function checkEmail(str)
        {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(str))
            {
                $('#text3').val('');
//                $('#text3').focus();
//                str.value = '';
//                str.focus();
                return false;
            }
            return true;

        }
    </script>
