<?php
foreach ($pro_details as $pro_details_res)
    $data['session'] = $this->session->userdata("logged_in_user");
?>
<?php
if (!empty($coupon_data)) {
    $coupon_id = $coupon_data['coupon_id'];
    $amount_percent = $coupon_data['amount_percent'];
    if (!empty($amount_percent)) {
        $amount_percent;
    }
}
?>
<div itemscope itemtype="http://schema.org/Product">
    <div class="container" id="product_page">
        <div class="row" >
            <div class="col-md-12 col-cust-both">

                <div class="col-md-9 col-cust-left">
                    <?php
                    $exp_data = explode("/", $pro_details_res['pro_image']);

                    $pro_image = img_url("uploads/product/" . $exp_data[0] . '/' . $exp_data[1] . '/ext.jpg');
                    $pro_image2 = $exp_data[0] . '/' . $exp_data[1] . '/ext.jpg';
                    if (file_exists($pro_image)) {
                        $pro_image;
                    }
                    foreach ($product_details as $product_details_data)
                        
                        ?>
                    <div class="ribbin">
                        <div class="corner">
                            <h2 itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <?php
                                if ($pro_details_res['upcoming'] == 'y') {
                                    echo 'Coming Soon';
                                } else {
                                    echo $product_details_data[0]['total_benefits_perc'] . '&nbsp;&nbsp;&nbsp;&nbsp;';
                                }
                                ?>
                            </h2>
                            <img class="img-left img-responsive" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $pro_image2)); ?>" src="<?php echo $pro_image; ?>">   
                        </div>
                    </div>
                    <?php
//                    }
                    ?>
                </div>
                <div class="col-md-3 col-cust-right" align="left">
                    <h3 class="top_right" itemprop="name">
                        <?php
                        echo $pro_main_details[0]['pro_name_comp'];
                        ?>
                    </h3>
                    <?php
                    if (!empty($pro_main_details[0]['launching_date'])) {
                        ?>
                        <h3  align="left" class="top_right2" >                                
                            <?php
                            echo 'Expected Launch:' . $pro_main_details[0]['launching_date'];
                            ?>
                        </h3>
                        <?php
                    }
                    ?>


                    <div class="col-md-12 col-cust-both">
                        <div itemprop="aggregateRating"    itemscope itemtype="http://schema.org/AggregateRating">
                            <div class="col-md-7 col-cust-left">                                    
                                <a href="#user-review" onclick="$('#usr_id').click();"  data-toggle="tab">User Review (<span id="user_review_count" itemprop="ratingValue"></span>)</a>
                            </div>
                            <div class="col-md-5 col-cust-right">
                                <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="User Ratings"><i class="fa fa-users"></i></span>
                                <span class="text-golden" >
                                    <?php
                                    foreach ($user_review_count as $user_pro_count)
                                        $user_star = $user_pro_count['total'];
                                    $exp = explode('.', $user_star);
                                    $user_point = $exp[1];
                                    if ($user_point > 00) {
                                        $half = '-half-full';
                                    } else if ($user_point == 00) {
                                        $half = '-o';
                                    } else {
                                        $half = '';
                                    }
                                    if (empty($user_star)) {
                                        ?>
                                        <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                        <?php
                                    }

                                    if ($exp[0] == 1) {
                                        ?>
                                        <i class="fa fa-star"></i><?php ?>
                                        <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                        <?php
                                    } else if ($exp[0] == 2) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                } else if ($exp[0] == 3) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                } else if ($exp[0] == 4) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                } else if ($user_star == 5) {
                                        ?>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <?php
                                    }
                                    ?>
                                </span>
                            </div>
                        </div>
                        <div >
                            <div class="col-md-7 col-cust-left">
                                <a href="#expert-review"  onclick="$('#exp_id').click();" data-toggle="tab">
                                    Expert Review (<span id="exp_review_count" itemprop="ratingValue"></span>)</a>
                            </div>
                            <div class="col-md-5 col-cust-right">
                                <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="Expert Ratings"><i class="fa fa-user"></i></span>
                                <span class="text-maroon">
                                    <?php
                                    foreach ($exp_review_count as $exp_pro_count)
                                        $exp_star = $exp_pro_count['total'];
                                    $exp = explode('.', $exp_star);
                                    $exp_point = $exp[1];
                                    if ($exp_point > 00) {
                                        $half = '-half-full';
                                    } else if ($exp_point == 00) {
                                        $half = '-o';
                                    } else {
                                        $half = '';
                                    }

                                    if (empty($exp_star)) {
                                        ?>
                                        <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                        <?php
                                    }

                                    if ($exp[0] == 1) {
                                        ?>
                                        <i class="fa fa-star"></i><?php ?>
                                        <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                        <?php
                                    } else if ($exp[0] == 2) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                } else if ($exp[0] == 3) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                } else if ($exp[0] == 4) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                } else if ($exp_star == 5) {
                                        ?>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <?php
                                    }
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-cust-both">
                        <?php
                        if ($pro_details_res['upcoming'] == 'y' || $pro_details_res['upcoming'] == 'Y') {
                            
                        } else {
                            ?>
                            <?php
                            $data['session'] = $this->session->userdata("logged_in_user");
                            if (!empty($data['session'])) {
                                ?>
                                <a class="btn padding-0" style="padding:0;" href="<?php echo url_struct('user-review/' . preg_replace('/\s+/', '-', $pro_main_details[0]['pro_name_comp']) . '-' . $id) ?>">
                                    Rate the Car
                                </a>
                                <?php
                            } else {
                                ?>
                        <a id="a1" href="#" onclick="set_redirection('1');" data-toggle="modal"  data-target="#loginModal" >Rate the Car</a>
                                <?php
                            }
                            ?>

                            <span>|</span>
                            <a  href="<?php echo url_struct('compare-new-cars-online/' . preg_replace('/\s+/', '-', ($pro_main_details[0]['pro_name_comp'] . '-' . $pro_main_details[0]['variant_id']))); ?>" >Compare Car</a>    
                            <?php
                        }
                        ?>        

                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12 col-cust-both pad_top">
                <?php
                if ($pro_details_res['upcoming'] == 'y' || $pro_details_res['upcoming'] == 'Y') {
                    ?>
                    <div class="col-md-4 col-cust-left">
                        <div class="product_left_css">
                            <div align="center">
                                <br>
                                <?php
                                if ($pro_details_res['upcoming'] == 'y' || $pro_details_res['upcoming'] == 'Y') {
                                    ?>
                                    <a href="#" id="book_now" data-toggle="modal" data-target="#coming_enqury" class="btn btn-xs search-by-price-button carsuel_book" onclick="$('#enq_com_product').val('<?php echo $pro_details_res['pro_name_comp']; ?>')">
                                        Alert Me
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>    
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="col-md-4 col-cust-left">
                        <form action="<?php echo base_url() . 'payment-page'; ?>" method="POST" id="submit_product">
                            <div class="product_left_css">
                                <h3  class="product_save_title" align="center">
                                    Save Rs. <b id="save_saving_price2"><?php
                foreach ($product_details as $pro_details)
                    if (!empty($pro_conf['total_saving'])) {
                        echo $total_savings = indianFormatNumber($pro_conf['total_saving'] + $amount_percent);
                    } else {
                        echo $total_savings = indianFormatNumber($pro_details[0]['total_savings'] + $amount_percent);
                    }
                    ?> 
                                    </b> on <br><?php
                                    echo $pro_main_details[0]['pro_name_comp'];
                    ?> 
                                </h3>

                            </div>


                            <div class="product_left_css2">
                                <div align="center">
                                    <?php
                                    if ($pro_details_res['upcoming'] == 'y') {
                                        ?>
                                        <a href="#" id="book_now" data-toggle="modal" data-target="#coming_enqury" class="btn btn-xs search-by-price-button carsuel_book">
                                            Alert Me
                                        </a>
                                        <?php
                                    } else {
                                        if (indianFormatNumber($pro_details[0]['mnc_price']) <= 0) {
                                            ?>
                                            <a href="#" id="book_now"  onclick="alert('This vehicle is currently unavailable for booking');" class="btn btn-xs search-by-price-button carsuel_book">
                                                Book Now
                                            </a>
                                            <?php
                                        } else {

                                            if (!empty($data['session'])) {
                                                ?>
                                                <a href="#" id="book_now"  onclick="book_now_check();" class="btn btn-xs search-by-price-button carsuel_book">
                                                    <!--<a href="#" style="font-size:14px;" onclick="book_now_check();$('#submit_product').submit();" class="btn btn-sm btn-maroon ">-->
                                                    Book Now
                                                </a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="#" onclick="set_redirection('2');" id="book_now"  data-toggle="modal" data-target="#loginModal" class="btn btn-xs search-by-price-button carsuel_book">
                                                    Book Now
                                                </a>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>

                                    <br>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <select class="form-control"  id="rating_variant" name="rating_variant" onchange="rating_variant_change();" title='Select Variant'>
                                            <option value="">Select Variant</option>
                                            <?php
                                            foreach ($pro_ext as $pro_ext_res) {
                                                ?>
                                                <option <?php
                                        if ($id == $pro_ext_res['variant_id']) {
                                            echo 'selected';
                                        }
                                                ?> value="<?php echo $pro_ext_res['variant_id']; ?>"><?php echo $pro_ext_res['pro_name_comp']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <select class="form-control"  id="color" name="color" onchange="color_selection();">
                                            <option value="">Select Color</option>
                                            <?php
                                            foreach ($car_color as $car_color_res) {
                                                ?>
                                                <option <?php
                                        if ($id == $main_color[0] && $car_color_res['feature_desc'] == $main_color[1]) {
                                            echo 'selected';
                                        }
                                                ?> value="<?php echo $car_color_res['feature_id']; ?>"><?php echo $car_color_res['feature_desc']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="pro_id" id="pro_id" value="<?php
                                            if (isset($id)) {
                                                echo $id;
                                            }
                                                ?>" />  

                                <div class="col-md-12 col-cust-both exshow_price_pad">
                                    <div class="pro_left_side col-md-12 col-cust-both exshow_price_marg">                                
                                        <?php
                                        if (indianFormatNumber($pro_details[0]['mnc_price']) > 0) {
                                            ?>
                                            <div class="col-md-12 col-cust-both">
                                                <h4 class="product_heading">
                                                    <span class="pull-left">
                                                        Ex-Showroom Price *
                                                    </span>
                                                    <span class="pull-left">
                                                        <i class="fa fa-rupee product_heading_icon text-maroon pull-left"></i>
                                                    </span>  
                                                    <span class="text-maroon product_heading pull-left" id="main_exp_price">
                                                        <?php
                                                        echo indianFormatNumber($pro_details[0]['mnc_price']);
                                                        ?>
                                                    </span>                                       
                                                </h4>    
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <h4 class="product_heading">Ex-Showroom Price-*</h4>
                                            <span class="text-maroon pull-left">    
                                                <?php echo Not_applicable; ?>    
                                            </span>    
                                            <?php
                                        }
                                        ?>

                                    </div>

                                    <div class="pro_left_side col-md-12 col-cust-both"  align="left">
                                        <div class="col-sm-12 col-xs-12 col-cust-both">
                                            <img src="<?php echo base_url(); ?>assets/img/product/carinsurance_hotdeals_trans.png">
                                            <span class="pro_text">
                                                Zero Depreciation Insurance 
                                                <a href="#" data-toggle="modal" data-target="#zero_deprecation">
                                                    <span class="more">More...</span>
                                                </a>
                                            </span>
                                            <?php
                                            if ($pro_conf['insurance_id'] != '') {
                                                $check01 = 'checked';
                                            } else if ($pro_conf['insurance_id'] == '') {
                                                $check01 = '';
                                            }
                                            ?>  
                                            <input type="checkbox" <?php
                                        echo $check01;
                                            ?> class="pull-right" id="car_insurance" name="car_insurance" <?php if (indianFormatNumber($ins_car[0]['price'] + $pro_price_details['zero_depreciation']) > 0) { ?> onclick="zero_depreciation('1');" <?php } else { ?> disabled readonly  <?php } ?>value="1"/>       
                                        </div>
                                        <span class="pro_text2">
                                            +&nbsp;
                                            <i class="fa fa-rupee"></i>&nbsp;
                                            <?php echo indianFormatNumber($ins_car[0]['price'] + $pro_price_details['zero_depreciation']); ?> 
                                            <input type="hidden" value="<?php echo $ins_car[0]['price'] + $pro_price_details['zero_depreciation']; ?> " id="zero_depreciation_price"/>
                                            <div aria-hidden="true" aria-labelledby="price-modal-label" role="dialog" tabindex="-1" id="zero_deprecation" class="modal fade" style="display:none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4  class="modal-title text-left">Zero Depreciation Insurance</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    Full claim without any deduction for depreciation of the parts replaced in case of an accident. 
                                                                    <!--A zero depreciation cover ensures that in case of an accident, you will receive the full claim without any deduction for depreciation on the value of the parts replaced.-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>

                                    </div>





                                    <div class="pro_left_side col-md-12 col-cust-both">
                                        <img src="<?php echo base_url(); ?>assets/img/product/EXTENDED-WARRANTY.png" class="product_icon">
                                        <span class="pro_text">Extended Warranty&nbsp;</span>
                                        <?php
                                        if (!empty($pro_conf)) {
                                            if ($pro_conf['extended_id'] != '') {
                                                $check02 = 'checked';
                                            } else if ($pro_conf['extended_id	'] == '') {
                                                $check02 = '';
                                            } else {
                                                $check02 = 'checked';
                                            }
                                        }
                                        ?> 
                                        <input type="checkbox" <?php
                                    echo $check02;
                                        ?> class="pull-right" id="extended_waranty" name="extended_waranty" value="1" <?php if (indianFormatNumber($pro_price_details['extended_warranty']) != '0') { ?> onclick="zero_depreciation('2');" <?php } else { ?> disabled readonly <?php } ?> />       
                                        <input type="hidden"  class="pull-right" id="extended_waranty_price" name="extended_waranty_price" value="<?php echo $pro_price_details['extended_warranty']; ?>" />       
                                        <br>
                                        <span class="pro_text2">
                                            +&nbsp;
                                            <i class="fa fa-rupee"></i>&nbsp;
                                            <?php
                                            if (indianFormatNumber($pro_price_details['extended_warranty']) != '0') {
                                                echo indianFormatNumber($pro_price_details['extended_warranty']);
                                            } else {
                                                echo 'Unavailable';
                                            }
                                            ?> 
                                        </span> 

                                    </div>


                                    <div class="pro_left_side col-md-12 col-cust-both">
                                        <?php
                                        $exp_car_exchange_price = explode('_', $this->input->cookie('car_exchange_price', TRUE));
                                        ?>
                                        <img src="<?php echo base_url(); ?>assets/img/product/carexchange_trans.png">
                                        <span class="pro_text">Car Exchange</span>
                                        <a href="#" data-toggle="modal" data-target="#car_exchange">
                                           <!--<i class="fa fa-info-circle text-maroon"></i>-->     
                                            <span class="more">More...</span>
                                        </a>
                                        <?php
                                        $check1 = 'checked';
                                        if (empty($pro_conf)) {
                                            if (!empty($pro_conf)) {
                                                if ($pro_conf['used_id'] != '') {
                                                    $check1 = 'checked';
                                                } else if ($pro_conf['used_id'] == '') {
                                                    $check1 = '';
                                                }
                                            }
                                        }
                                        ?>                     
                                        <input type="checkbox" <?php echo $check1; ?> class="pull-right" id="car_exchange_data"  name="car_exchange_data" onclick="zero_depreciation('3');" value="1"/>       
                                        <input type="hidden" value="<?php echo $pro_price_details['exchange_bonus']; ?> " id="car_exchange_price"/>
                                        <div aria-hidden="true" aria-labelledby="price-modal-label" role="dialog" tabindex="-1" id="car_exchange" class="modal fade" style="display:none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                        <h4 align="left" class="modal-title">Car Exchange</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                Get your old car valuation done for free with attractive car exchange options.
                                                                <!--Get your old car valuation done for free with attractive car exchange options.-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>




                                    <div class="pro_left_side col-md-12 col-cust-both">

                                        <img src="<?php echo base_url(); ?>assets/img/product/corporate.png" class="product_icon">
                                        <span class="pro_text">Corporate Discount&nbsp;</span>
                                        <?php
                                        $check2 = 'checked';
                                        if (empty($pro_conf)) {
                                            if (!empty($pro_conf)) {
                                                if ($pro_conf['corporate_discount'] != '') {
                                                    $check2 = 'checked';
                                                } else if ($pro_conf['corporate_discount'] == '') {
                                                    $check2 = '';
                                                } else {
                                                    $check2 = 'checked';
                                                }
                                            }
                                        }
                                        ?>
                                        <input type="checkbox" <?php echo $check2; ?> class="pull-right" id="corporate_discount" name="corporate_discount" onclick="zero_depreciation('4');" value="1"/>       
                                        <input type="hidden" value="<?php echo $pro_price_details['corporate_discount']; ?> " id="corporate_discount_price" id="corporate_discount_price"/>

                                    </div>


                                    <div class="pro_left_side col-md-12 col-cust-both">

                                        <img src="<?php echo base_url(); ?>assets/img/product/brand.png" class="product_icon">
                                        <span class="pro_text">Loyalty Bonus&nbsp;
                                            <a href="#" data-toggle="modal" data-target="#loyalty_bonus_detail">                                        
                                                <span class="more">More...</span>
                                            </a></span>
                                        <div aria-hidden="true" aria-labelledby="price-modal-label" role="dialog" tabindex="-1" id="loyalty_bonus_detail" class="modal fade" style="display:none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                        <h4 align="left" class="modal-title">Loyalty Bonus</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12"> 
                                                                I own a <?php print_r($car_gallery[0]['Brand2']); ?> Brand Car&nbsp;
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $check3 = 'checked';
                                        if (empty($pro_conf)) {
                                            if (!empty($pro_conf)) {
                                                if ($pro_conf['loyality_bonus'] != '') {
                                                    $check3 = 'checked';
                                                } else if ($pro_conf['loyality_bonus'] == '') {
                                                    $check3 = '';
                                                } else {
                                                    $check3 = 'checked';
                                                }
                                            }
                                        }
                                        ?>
                                        <input type="checkbox"  <?php echo $check3; ?> class="pull-right" id="loyality_bonus" name="loyality_bonus" onclick="zero_depreciation('5');" value="1"/>       
                                        <input type="hidden" value="<?php echo $pro_price_details['loyality_bonus']; ?> " id="loyality_bonus_price" id="loyality_bonus_price"/>

                                    </div>


                                    <div class="pro_left_side col-md-12 col-cust-both">

                                        <img src="<?php echo base_url(); ?>assets/img/product/carfinance_hotdeals_trans.png" class="product_icon">
                                        <a class="decoration_remove"><span class="pro_text">Car Finance</span></a>
                                        <div aria-hidden="true" aria-labelledby="price-modal-label" role="dialog" tabindex="-1" id="car_finance" class="modal fade" style="display:none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                        <h4 align="left" class="modal-title">Car Finance</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                Avail attractive car finance offers.
                                                                <!--Avail attractive car finance offers.-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $check4 = 'checked';
                                        if (empty($pro_conf)) {
                                            if (!empty($pro_conf)) {
                                                if ($pro_conf['finance_id'] != '') {
                                                    $check4 = 'checked';
                                                } else if ($pro_conf['finance_id'] == '') {
                                                    $check4 = '';
                                                } else {
                                                    $check4 = 'checked';
                                                }
                                            }
                                        }
                                        ?>


                                        <input type="checkbox"  <?php echo $check4; ?>  class="pull-right" id="car_finance_data" name="car_finance_data" onclick="zero_depreciation('0');" value="1"/>       
                                        <!--<span class="pro_text2">-->
                                        <a href="#" data-toggle="modal" data-target="#emi_caulculator" class="more" class="btn" onclick="set_exp_price();"> EMI Calculator </a>
                                        <span class="pro_text2">
                                            <?php
                                            if ($get_emi[0]['emi_amount'] != '') {
                                                ?>
                                                <br>    
                                                <i class="fa fa-rupee"></i> 
                                                <?php
                                                echo $get_emi[0]['emi_amount'];
                                            }
                                            ?>
                                        </span>
                                        <input type="hidden" value="<?php echo $get_emi[0]['emi_amount']; ?> " id="car_finance_price" name="car_finance_price"/>        
                                        <input type="hidden" value="<?php echo $get_emi[0]['emi_id']; ?> " id="emi_id" name="emi_id"/>       
                                        <!--</span>-->    

                                    </div>  



                                    <div class="pro_left_side col-md-12 col-cust-both">

                                        <img src="<?php echo base_url(); ?>assets/img/product/homedelivery.png">
                                        <span class="pro_text">Doorstep Delivery&nbsp;
                                            <a href="#" data-toggle="modal" data-target="#doorstep-delivery">
                                                <span class="more">More...</span>
                                            </a></span>
                                        <div aria-hidden="true" aria-labelledby="price-modal-label" role="dialog" tabindex="-1" id="doorstep-delivery" class="modal fade" style="display:none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                        <h4 align="left" class="modal-title">Doorstep Delivery</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                Free doorstep delivery available in Mumbai, Pune and Bangalore.
                                                                <!--Free doorstep delivery available in Mumbai, Pune and Bangalore.-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $check5 = 'checked';
                                        if (empty($pro_conf)) {
                                            if (!empty($pro_conf)) {
                                                if ($pro_conf['home_delivery'] != '') {
                                                    $check5 = 'checked';
                                                } else if ($pro_conf['home_delivery'] == '') {
                                                    $check5 = '';
                                                } else {
                                                    $check5 = 'checked';
                                                }
                                            }
                                        }
                                        ?>
                                        <input type="checkbox"  <?php echo $check5; ?> class="pull-right" id="home_delivery"  onclick="zero_depreciation('6');" name="home_delivery" value="<?php echo $home_delivery[0]['id']; ?>" value="1"/>  
                                        <input type="hidden"  checked="true" class="pull-right" id="home_delivery_price"  name="home_delivery_price" value="<?php ?>"/>  
                                        <!--<input type="hidden" value="<?php echo $get_emi[0]['emi_amount']; ?> " id="car_finance_price" id="car_finance_price"/>--> 
                                    </div>




                                    <div class="col-md-12 col-cust-both">
                                        <div class="product_sav_heading col-md-7 pull-left col-sm-6 col-xs-6 col-cust-left">
                                            <span class="pull-left">
                                                Total Savings 
                                            </span>
                                            <a href="#" data-target="#total_saving_data" data-toggle="modal" class="pull-left">
                                               <!--<i class="fa fa-info-circle text-maroon"></i>-->
                                                <span class="more">More...</span>
                                            </a>
                                        </div>
                                        <div class="product_sav_heading text-maroon pull-right col-md-5 col-sm-6 col-xs-6 col-cust-right">
                                            <i class="fa fa-rupee" class="product_sav_heading"></i>                                              
                                            <span id="total_saving_price">
                                                <?php
                                                if (!empty($pro_conf['total_saving'])) {
                                                    echo $total_savings = indianFormatNumber($pro_conf['total_saving'] + $amount_percent);
                                                } else {
                                                    echo $total_savings = indianFormatNumber($pro_details[0]['total_savings'] + $amount_percent);
                                                }
                                                ?></span>
                                                <?php
                                                
//                                                print_r($pro_price_details['accessory_package']);
                                                
                                               if(!empty($pro_price_details['accessory_package'])){
                                               $accessory_package=$pro_price_details['accessory_package'];
                                               ?> 
                                                +
                                                <img src="<?php echo base_url() . 'assets/img/product/otherbenefits.png' ?>" data-toggle="tooltip" class="main_tooltip" data-placement="right" title="" data-original-title="<?php echo $accessory_package;?>" style="margin-top: -10px;"/>
                                                <!--<img src="<?php echo base_url() . 'assets/img/product/otherbenefits.png' ?>" data-toggle="tooltip" class="main_tooltip"  data-placement="top" title="<?php echo $total_saving[0]['accessory_package']; ?>" style="margin-top: -10px;"/>-->
                                               <?php
                                               }
                                               ?>
                                        </div>
                                        <input type="hidden" value="<?php echo $pro_details[0]['total_savings']; ?> " id="final_total_saving_price" name="final_total_saving_price"/>
                                    </div>


                                    <div class="col-md-12 col-cust-both  col-sm-12 col-xs-12" align="center" id="apply_msg">
                                        <?php
                                        if (!empty($coupon_data)) {
                                            ?>    
                                            <div class="alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" onclick="reset_coupon();" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <?php
                                                $exp = explode('_', $coupon_id);
                                                ?>
                                                Coupon "<?php echo $coupon_data['coupn_code']; ?>" Applied Successfully                                        
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <br>
                                        <?php
                                        if ($exp_coup[0] == $id && !empty($exp_coup[1])) {
                                            
                                        } else {
                                            ?>
                                            <div class="col-md-6">
                                                <input type="text" required="" placeholder="Coupon Code" class="form-control" name="coupon_code" id="coupon_code">    
                                            </div>
                                            <div class="col-md-6">
                                                <?php
                                                if (!empty($data['session'])) {
                                                    ?>
                                                    <a href="#"  onclick="coupon_apply();" id="apply_button"  class="btn btn-xs search-by-price-button carsuel_book">
                                                        Apply Coupon
                                                    </a>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <a href="#"  data-toggle="modal" id="apply_button" data-target="#loginModal" class="btn btn-xs search-by-price-button carsuel_book">
                                                        Apply Coupon
                                                    </a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>                       
                                    </div>



                                </div>

                                <div aria-hidden="true" aria-labelledby="price-modal-label" role="dialog" tabindex="-1" id="total_saving_data" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                <h4 align="left" class="modal-title">Total Savings</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table>
                                                            <tr >
                                                                <td > Cash Discount</td>
                                                                <td>
                                                                    <i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    echo indianFormatNumber($total_saving['cash_discount']);
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td > Insurance Discount </td>
                                                                <td>
                                                                    <i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    echo indianFormatNumber($total_saving['insurance_discount']);
                                                                    ?>                                                             
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td >Special Reward </td>
                                                                <td><i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    echo indianFormatNumber($total_saving['special_reward']);
                                                                    ?>                                                               
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td >Exchange Bonus</td>
                                                                <td><i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    echo indianFormatNumber($total_saving['exchange_bonus']);
                                                                    ?>                                                         
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td > Corporate Discount</td>
                                                                <td><i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    echo indianFormatNumber($total_saving['corporate_discount']);
                                                                    ?>                                                             
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td >Loyalty Bonus</td>
                                                                <td><i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    echo indianFormatNumber($total_saving['loyality_bonus']);
                                                                    ?>                                                            
                                                                </td>
                                                            </tr>
                                                            <tr >
                                                                <td > Accessories Discount</td>
                                                                <td>
                                                                    <i class="fa fa-rupee"></i> 
                                                                    <?php
                                                                    echo indianFormatNumber($total_saving['accessory_discount']);
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr style="display:<?php
                                                                if (!empty($amount_percent)) {
                                                                    echo 'block';
                                                                } else {
                                                                    echo 'none';
                                                                }
                                                                    ?>" id="coupon_side_details2">
                                                                <td > Coupon Discount</td>
                                                                <td>
                                                                    <i class="fa fa-rupee"></i> 
                                                                    <span id="show_coupon_price2">
                                                                        <?php echo '(-) ' . $amount_percent; ?>   
                                                                    </span>    
                                                                </td>
                                                            </tr>                                                            
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="price-modal" tabindex="-1" role="dialog" aria-labelledby="price-modal-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                            <h4 align="left" class="modal-title">On-Road Price Breakup</h4>
                                        </div>

                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-sx-12">
                                                    <table class="table-responsive">
                                                        <tr >
                                                            <td >Ex-Showroom Price</td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div class="strike-through pull-left">                                                            
                                                                    <?php
                                                                    if (!empty($pro_conf['on_road_price'])) {
                                                                        echo $on_road_price = indianFormatNumber($pro_conf['on_road_price']);
                                                                    } else {
                                                                        echo $on_road_price = indianFormatNumber($onroad_price_breakup[0]['exshowroom_price']);
                                                                    }


//                                                            print_r($onroad_price_breakup);
//                                                                echo indianFormatNumber($onroad_price_breakup[0]['exshowroom_price']);
                                                                    ?>
                                                                </div>
                                                                <div   class="text-maroon pull-left">   
                                                                    &nbsp;&nbsp;<i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    $f_ex_showroom_price = $onroad_price_breakup[0]['exshowroom_price'] - $total_saving['cash_discount'];
                                                                    echo indianFormatNumber($f_ex_showroom_price);
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr >
                                                            <td >Comprehensive Insurance</td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div id="onroad_comp_insurance" class="strike-through pull-left">                                                            
                                                                    <?php
                                                                    echo indianFormatNumber($onroad_price_breakup[0]['comp_insurance']);
                                                                    ?>
                                                                </div>
                                                                <div class="pull-left text-maroon">   
                                                                    &nbsp;&nbsp;<i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    $f_comp_insurance = $onroad_price_breakup[0]['comp_insurance'] - $pro_price_details['insurance_discount'];
                                                                    echo indianFormatNumber($f_comp_insurance);
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr id="onroad_zero_insurance_id" style="display:<?php
                                                                if (!empty($zero_dep_insurance[0]) && $zero_dep_insurance[1] == $id) {
                                                                    echo 'block';
                                                                } else {
                                                                    echo 'none';
                                                                }
                                                                    ?>" >
                                                            <td >Zero Depreciation Insurance</td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div id="onroad_zero_insurance_price" class="pull-left">  
                                                                    <?php
                                                                    if ($zero_dep_insurance[0] != '' && $zero_dep_insurance[1] == $id) {
                                                                        //                                                         echo 'checked'; 
                                                                        $f_zero_depreciation = $onroad_price_breakup[0]['zero_depreciation'];
                                                                        echo indianFormatNumber($f_zero_depreciation);
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr >
                                                            <td >RTO</td>
                                                            <td><i class="fa fa-rupee"></i>
                                                                <?php
                                                                $f_road_tax = $onroad_price_breakup[0]['road_tax'];
                                                                echo indianFormatNumber($onroad_price_breakup[0]['road_tax']);
                                                                ?>                                                         
                                                            </td>
                                                        </tr>
                                                        <tr >
                                                            <td >Handling Charges</td>
                                                            <td><i class="fa fa-rupee"></i>
                                                                <?php
                                                                $f_handling_price = $onroad_price_breakup[0]['handling_price'];
                                                                echo indianFormatNumber($onroad_price_breakup[0]['handling_price']);
                                                                ?>                                                            
                                                            </td>
                                                        </tr>
                                                        <tr >
                                                            <td > LBT Tax</td>
                                                            <td><i class="fa fa-rupee"></i>
                                                                <?php
                                                                $f_lbt = $onroad_price_breakup[0]['lbt'];
                                                                echo indianFormatNumber($onroad_price_breakup[0]['lbt']);
                                                                ?>                                                             
                                                            </td>
                                                        </tr>
                                                        <tr id="onroad_special_reward_id" >
                                                            <td >Special Reward</td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div id="onroad_special_reward_price" class="pull-left">
                                                                    <?php
                                                                    if (!empty($pro_price_details['special_reward'])) {
                                                                        $f_special_reward = $pro_price_details['special_reward'];
                                                                        echo ' (-) ' . indianFormatNumber($pro_price_details['special_reward']);
                                                                    } else {
                                                                        echo ' (-) 0';
                                                                    }
                                                                    ?>   
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr id="onroad_exchange_bonus_id" >
                                                            <td >Exchange Bonus</td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div id="onroad_exchange_bonus_price" class="pull-left">
                                                                    <?php
                                                                    if (!empty($pro_price_details['exchange_bonus'])) {
                                                                        $f_exchange_bonus = $pro_price_details['exchange_bonus'];
                                                                        echo ' (-) ' . indianFormatNumber($pro_price_details['exchange_bonus']);
                                                                    } else {
                                                                        echo ' (-) 0';
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr id="onroad_corporate_dicount_id" >
                                                            <td >Corporate Discount</td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div id="onroad_corporate_dicount_price" class="pull-left">
                                                                    <?php
                                                                    if (!empty($pro_price_details['corporate_discount'])) {
                                                                        $f_corporate_discount = $pro_price_details['corporate_discount'];
                                                                        echo ' (-) ' . indianFormatNumber($pro_price_details['corporate_discount']);
                                                                    } else {
                                                                        echo ' (-) 0';
                                                                    }
                                                                    ?>    
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr id="onroad_loyalty_bonus_id1" >
                                                            <td >Loyalty Bonus</td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div id="onroad_loyalty_bonus_price" class="pull-left">
                                                                    <?php
                                                                    if (!empty($pro_price_details['loyality_bonus'])) {
                                                                        $f_loyality_bonus = $pro_price_details['loyality_bonus'];
                                                                        echo ' (-) ' . indianFormatNumber($pro_price_details['loyality_bonus']);
                                                                    } else {
                                                                        echo ' (-) 0';
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>


                                                        <tr style="display:<?php
                                                                if (!empty($amount_percent)) {
                                                                    echo 'block';
                                                                } else {
                                                                    echo 'none';
                                                                }
                                                                    ?>" id="coupon_side_details2">
                                                            <td > Coupon Disocunt</td>
                                                            <td>
                                                                <i class="fa fa-rupee"></i> 
                                                                <span id="show_coupon_price2">
                                                                    <?php echo '(-) ' . $amount_percent; ?>   
                                                                </span>    
                                                            </td>
                                                        </tr>
                                                        <tr id="extended_waranty_price1" style="display:<?php
                                                                    if (!empty($extended_waranty_price[0]) && $extended_waranty_price[1] == $id) {
                                                                        echo 'block';
                                                                    } else {
                                                                        echo 'none';
                                                                    }
                                                                    ?>" >
                                                            <td >Extended Warranty</td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div id="extended_waranty_price_price"class="pull-left">  
                                                                    <?php
//                                                            print_r($onroad_price_breakup);                                                       
                                                                    echo indianFormatNumber($onroad_price_breakup[0]['extended_warranty']);
//                                                             $f_extended_waranty_price = $onroad_price_breakup[0]['extended_warranty'];
                                                                    if ($extended_waranty_price[0] == '') {
                                                                        $f_extended_waranty = '';
                                                                    } else {
                                                                        $f_extended_waranty = $onroad_price_breakup[0]['extended_warranty'];
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr class="tr_border">
                                                            <td ></td>
                                                            <td>
                                                                <div class="pull-left">
                                                                    <i class="fa fa-rupee"></i>&nbsp;
                                                                </div>
                                                                <div  class="pull-left" id="f_onroad_price">
                                                                    <?php
                                                                    if (empty($amount_percent)) {
                                                                        $amount_percent = 0;
                                                                        ;
                                                                    } else {
                                                                        $amount_percent;
                                                                    }
//                                                                    echo $amount_percent;
//                                                                    echo $amount_percent;
                                                                    $f_onroad_price = ($f_ex_showroom_price + $f_zero_depreciation + $f_extended_waranty + $f_comp_insurance + $f_road_tax + $f_handling_price + $f_lbt - $f_special_reward - $f_exchange_bonus - $f_corporate_discount - $f_loyality_bonus) - $amount_percent;

                                                                    echo indianFormatNumber($f_onroad_price);
//                                                                    unset($f_onroad_price);
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <table class="car-review-table table table-bordered table-striped table-condensed" id="extra_benefits" style="color:grey;border:1px solid #7B7776;margin-bottom:-10px; margin-top:0px;display:none;">
                                                        <tbody>
                                                            <tr>
                                                                <td> Cash Discount</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['cash_discount'];
                                                                        ?>  
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Finance Benefit</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['finance_benefit'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['finance_benefit'];
                                                                        ?>  
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr  id="exchange_id">
                                                                <td> Exchange Benefit</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['exchange_benefit'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['exchange_benefit'];
                                                                        ?>  
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Loyality Bonus</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['loyality_bonus'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['loyality_bonus'];
                                                                        ?> 
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="corp_id"> Corporate Discount</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['corporate_discount'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['corporate_discount'];
                                                                        ?>  
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Waivier of Handling charges</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['waivier_handling_charges'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['waivier_handling_charges'];
                                                                        ?>  
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Accessories Package</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['accessory_package'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['accessory_package'];
                                                                        ?> 
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Accessories Discount</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['accessory_discount'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['accessory_discount'];
                                                                        ?> 
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Insurance Benefit</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['insurance_benefits'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['insurance_benefits'];
                                                                        ?> 
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Extended Warranty</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['extended_warranty'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['extended_warranty'];
                                                                        ?>  
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Liquidation Support by OEM</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['liquidation_support'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['liquidation_support'];
                                                                        ?>  
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> Special Reward</td>
                                                                <td>
                                                                    <span><i class="fa fa-rupee"></i> 
                                                                        <?php
                                                                        echo $final_result['exshowroom_price'][0]['additional_liquidation_support'];
                                                                        $total = $total + $final_result['exshowroom_price'][0]['additional_liquidation_support'];
                                                                        ?>  
                                                                    </span> 
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product_left_css3">
                                <div class="col-md-12 col-sm-12 col-xs-12 pro_onroad_box">
                                    <span class="pull-left">
                                        On Road Price 
                                        <a data-target="#price-modal" data-toggle="modal" href="#" style="text-align:left;">
                                           <!--<i class="fa fa-info-circle" style="color:white;"></i>-->
                                            <span class="more2">More...</span>
                                        </a>
                                    </span>
                                    <span class="pull-right">
                                        <span class="product_onroad_heading" > 
                                            <i class="fa fa-rupee product_onroad_heading" itemprop="priceCurrency" content="INR"></i>
                                            <?php
//                                            echo $f_onroad_price;
//                                        print_r($coupon_data);
//                                            if (!empty($pro_conf['on_road_price'])) {
//                                                $on_road_price = $pro_conf['on_road_price'];
//                                            } else {
//                                                echo $on_road_price = $f_onroad_price;
//                                            }
//
//                                            if (!empty($coupon_data)) {
//                                                $coupon_id = $coupon_data['coupon_id'];
//                                                $amount_percent = $coupon_data['amount_percent'];
//                                                if (!empty($amount_percent)) {
//                                                    $on_road_price = $on_road_price - $amount_percent;
//                                                }
//                                            } else {
//                                                $on_road_price;
//                                            }
                                            ?>
                                            <span id="grand_on_road_price" itemprop="price" content="<?php echo $on_road_price; ?>">
                                                <?php echo indianFormatNumber($f_onroad_price); ?>    
                                            </span>
                                        </span>
                                        <input type="hidden" id="disc_price" value="<?php echo $dis_on_road_price; ?>" /> 
                                    </span>    
                                    <?php ?>
                                    </span>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 pro_onroad_box">   
                                    <span class="pull-left">
                                        Refundable Booking Amount
                                        <span data-target="#refundable-modal" data-toggle="modal"  class="text-white info-span">
                                           <!--<i class="fa fa-info-circle"></i>-->
                                            <span class="more2">More...</span>
                                        </span>
                                    </span>
                                    <span class="pull-right">

                                        <i class="fa fa-rupee"></i>                                        
                                        <?php
                                        if ($pro_details_res['booking_amount'] > 0) {
                                            echo $booking_price = indianFormatNumber($pro_details_res['booking_amount']);
                                        } else {
                                            echo $booking_price = indianFormatNumber(trans_amt);
                                        }
                                        ?>
                                    </span>                                                            
                                </div>                                                                                 
                                <input type="hidden" value="<?php
                                    if (!empty($zero_dep_insurance[0]) && $zero_dep_insurance[1] == $id) {
                                        echo $zero_dep_insurance[0];
                                    } else {
                                        echo $onroad_detail[0]['mnc_on_road_price'];
                                    }
                                        ?> " id="final_mnc_on_road_price" name="final_mnc_on_road_price"/>
                            </div>
                            <div aria-hidden="true" aria-labelledby="price-modal-label" role="dialog" tabindex="-1" id="refundable-modal" class="modal fade" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                            <h4  class="modal-title pull-left">Booking Amount Details</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    100% refundable within 48 hours of booking (excluding Public holidays and Sundays).
                                                    <!--100% refundable within 48 hours of booking (excluding Public holiday and Sunday)-->  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
//                                            print_r($coupon_data);
                            if ($exp[0] == $id && !empty($exp[1])) {
                                if ($coupon_data['coupon_type'] == 'Material') {
                                    ?>   

                                    <div class="coupon_material_des">    
                                        <?php
                                        print_r($coupon_data['coupon_material']);
                                        ?>
                                    </div>    

                                    <?php
                                }
                            }
                            ?>                            
                        </form>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-8 col-cust-right">
                    <div class="mnc-tab">
                        <ul class="nav nav-tabs nav-pills">
                            <?php
                            if (!empty($exp_select)) {
                                $act2 = 'class="active"';
                                $act02 = 'active';
                            } else {
                                $act3 = 'class="active"';
                                $act03 = 'active';
                            }
                            ?>
                            <li  <?php echo $act3; ?>>
                                <a href="#specifications" data-toggle="tab" class="pull-left" >
                                    Specifications
                                </a>
                            </li>
                            <li>
                                <a href="#features" data-toggle="tab"  class="pull-left"  >
                                    Features
                                </a>
                            </li>
                            <li>
                                <a href="#user-colors" data-toggle="tab" class="pull-left"   >
                                    Colors<span class="badge pull-right" ></span>
                                </a>
                            </li>
                            <li>
                                <a href="#user-gallery" data-toggle="tab" class="pull-left"  >
                                    Gallery<span class="badge pull-right" ></span>
                                </a>
                            </li>
                            <li  <?php echo $act2; ?>>
                                <a id="exp_id" href="#expert-review" data-toggle="tab" class="pull-left" >
                                    Expert Review<span class="badge pull-right" id="tot_exp_review" itemprop="reviewCount"></span>
                                </a>
                            </li>
                            <li >
                                <a  id="usr_id" href="#user-review" data-toggle="tab" class="pull-left" style="min-width:139px;" >
                                    User Review<span class="badge pull-right" id="tot_user_review" itemprop="reviewCount"></span>
                                </a>
                            </li>


                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane" id="features">
                                <div class="table-responsive" >
                                    <div class="col-md-12 product_feature">
                                        <div class="container-fluid">
                                            <table class="table2 pull-left">
                                                <tbody>
                                                    <?php
                                                    $colors = array();
                                                    $exteriors = array();
                                                    $interiors = array();
                                                    $comfort = array();
                                                    $safety = array();
                                                    $entertainment = array();
                                                    $service = array();
                                                    $other = array();
                                                    foreach ($pro_feture as $pro_feture_data) {
                                                        if ($pro_feture_data['feature_name'] != 'Color') {
                                                            if ($pro_feture_data['feature_type'] == '1') {
                                                                $exteriors[] = $pro_feture_data;
                                                            }
                                                            if ($pro_feture_data['feature_type'] == '2') {
                                                                $interiors[] = $pro_feture_data;
                                                            }
                                                            if ($pro_feture_data['feature_type'] == '3') {
                                                                $comfort[] = $pro_feture_data;
                                                            }
                                                            if ($pro_feture_data['feature_type'] == '4') {
                                                                $safety[] = $pro_feture_data;
                                                            }
                                                            if ($pro_feture_data['feature_type'] == '5') {
                                                                $entertainment[] = $pro_feture_data;
                                                            }
                                                            if ($pro_feture_data['feature_type'] == '6') {
                                                                $service[] = $pro_feture_data;
                                                            }
                                                            if ($pro_feture_data['feature_type'] == '7') {
                                                                $other[] = $pro_feture_data;
                                                            }
                                                            ?>

                                                            <?php
                                                        } else {
                                                            $colors[] = $pro_feture_data;
                                                        }
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td colspan="3" class="tbl_heading">
                                                            <b>Interiors</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <?php
                                                                    foreach ($interiors as $interiors_data) {
                                                                        ?>
                                                                        <tr>
                                                                            <td class="tbl_wid_1"><?php print_r($interiors_data['feature_name']); ?></td>
                                                                            <td class="tbl_wid_2">
                                                                                <?php
                                                                                if (!empty($interiors_data['feature_img'])) {
                                                                                    ?>
                                                                                    <img width="50" src="<?php echo img_url("uploads/feature/" . $interiors_data['feature_img']); ?>"/>
                                                                                    <br>
                                                                                    <?php
                                                                                    echo $interiors_data['feature_desc'];
                                                                                } else {
                                                                                    if (strtoupper($interiors_data['feature_desc']) == 'Y') {
                                                                                        ?>
                                                                                        <i class="fa fa-check"></i>
                                                                                        <?php
                                                                                    } else if (strtoupper($interiors_data['feature_desc']) == 'N') {
                                                                                        ?>
                                                                                        <i class="fa fa-times"></i>
                                                                                        <?php
                                                                                    } else {
                                                                                        echo $interiors_data['feature_desc'];
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" class="tbl_heading">
                                                            <b>Exteriors</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <?php
                                                                    foreach ($exteriors as $exteriors_data) {
                                                                        ?>
                                                                        <tr>
                                                                            <td class="tbl_wid_1"><?php print_r($exteriors_data['feature_name']); ?></td>
                                                                            <td class="tbl_wid_2">
                                                                                <?php
                                                                                if (!empty($exteriors_data['feature_img'])) {
                                                                                    ?>
                                                                                    <img width="50" src="<?php echo img_url("uploads/feature/" . $exteriors_data['feature_img']); ?>"/>
                                                                                    <br>
                                                                                    <?php
                                                                                    echo $exteriors_data['feature_desc'];
                                                                                } else {
                                                                                    if (strtoupper($exteriors_data['feature_desc']) == 'Y') {
                                                                                        ?>
                                                                                        <i class="fa fa-check"></i>
                                                                                        <?php
                                                                                    } else if (strtoupper($exteriors_data['feature_desc']) == 'N') {
                                                                                        ?>
                                                                                        <i class="fa fa-times"></i>
                                                                                        <?php
                                                                                    } else {
                                                                                        echo $exteriors_data['feature_desc'];
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" class="tbl_heading">
                                                            <b>Comfort</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <?php
                                                                    foreach ($comfort as $comfort_data) {
                                                                        ?>
                                                                        <tr>
                                                                            <td class="tbl_wid_1"><?php print_r($comfort_data['feature_name']); ?></td>
                                                                            <td class="tbl_wid_2">
                                                                                <?php
                                                                                if (!empty($comfort_data['feature_img'])) {
                                                                                    ?>
                                                                                    <img width="50" src="<?php echo img_url("uploads/feature/" . $comfort_data['feature_img']); ?>"/>
                                                                                    <br>
                                                                                    <?php
                                                                                    echo $comfort_data['feature_desc'];
                                                                                } else {
                                                                                    if (strtoupper($comfort_data['feature_desc']) == 'Y') {
                                                                                        ?>
                                                                                        <i class="fa fa-check"></i>
                                                                                        <?php
                                                                                    } else if (strtoupper($comfort_data['feature_desc']) == 'N') {
                                                                                        ?>
                                                                                        <i class="fa fa-times"></i>
                                                                                        <?php
                                                                                    } else {
                                                                                        echo $comfort_data['feature_desc'];
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                            <!-- 2 -->
                                            <table class="table2 pull-right" >
                                                <tbody>

                                                    <tr>
                                                        <td colspan="3" class="tbl_heading">
                                                            <b>Safety</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <?php
                                                                    foreach ($safety as $safety_data) {
                                                                        ?>
                                                                        <tr>
                                                                            <td class="tbl_wid_1"><?php print_r($safety_data['feature_name']); ?></td>
                                                                            <td class="tbl_wid_2">
                                                                                <?php
                                                                                if (!empty($safety_data['feature_img'])) {
                                                                                    ?>
                                                                                    <img width="50" src="<?php echo img_url("uploads/feature/" . $safety_data['feature_img']); ?>"/>
                                                                                    <br>
                                                                                    <?php
                                                                                    echo $safety_data['feature_desc'];
                                                                                } else {
                                                                                    if (strtoupper($safety_data['feature_desc']) == 'Y') {
                                                                                        ?>
                                                                                        <i class="fa fa-check"></i>
                                                                                        <?php
                                                                                    } else if (strtoupper($safety_data['feature_desc']) == 'N') {
                                                                                        ?>
                                                                                        <i class="fa fa-times"></i>
                                                                                        <?php
                                                                                    } else {
                                                                                        echo $safety_data['feature_desc'];
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" class="tbl_heading">
                                                            <b>Entertainment</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <?php
                                                                    foreach ($entertainment as $entertainment_data) {
                                                                        ?>
                                                                        <tr>
                                                                            <td class="tbl_wid_1"><?php print_r($entertainment_data['feature_name']); ?></td>
                                                                            <td class="tbl_wid_2">
                                                                                <?php
                                                                                if (!empty($entertainment_data['feature_img'])) {
                                                                                    ?>
                                                                                    <img width="50" src="<?php echo img_url("uploads/feature/" . $entertainment_data['feature_img']); ?>"/>
                                                                                    <br>
                                                                                    <?php
                                                                                    echo $entertainment_data['feature_desc'];
                                                                                } else {
                                                                                    if (strtoupper($entertainment_data['feature_desc']) == 'Y') {
                                                                                        ?>
                                                                                        <i class="fa fa-check"></i>
                                                                                        <?php
                                                                                    } else if (strtoupper($entertainment_data['feature_desc']) == 'N') {
                                                                                        ?>
                                                                                        <i class="fa fa-times"></i>
                                                                                        <?php
                                                                                    } else {
                                                                                        echo $entertainment_data['feature_desc'];
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>   

                            </div>
                            <div class="tab-pane <?php echo $act03; ?>" id="specifications"  >
                                <div class="table-responsive" >
                                    <div class="col-md-12  product_feature">
                                        <div class="container-fluid">
                                            <table class="table2 pull-left">
                                                <tbody>
                                                    <?php
                                                    foreach ($pro_specification as $variant_details_data)
                                                        
                                                        ?>
                                                    <tr>
                                                        <td colspan="3" class="tbl_heading">
                                                            <b>Engine</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Fuel type</td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            foreach ($fuel as $fuel_data) {
                                                                                if ($fuel_data['fuel_type_id'] == $pro_details[0]['fuel_type']) {
                                                                                    echo $fuel_data['fuel_type'];
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Displacement (cc)</td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['displacement'])) {
                                                                                if (strtoupper($variant_details_data['displacement']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['displacement']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['displacement'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">No of Cylinders</td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['cylinders'])) {
                                                                                if (strtoupper($variant_details_data['cylinders']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['cylinders']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['cylinders'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Emission Norms</td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['engine_emission'])) {
                                                                                if (strtoupper($variant_details_data['engine_emission']) == 'y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['engine_emission']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['engine_emission'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Power (bhp)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['engine_maximum_power'])) {
                                                                                if (strtoupper($variant_details_data['engine_maximum_power']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['engine_maximum_power']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['engine_maximum_power'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['engine_maximum_power'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Torque (Nm)</td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['engine_maximum_torque'])) {
                                                                                if (strtoupper($variant_details_data['engine_maximum_torque']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['engine_maximum_torque']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['engine_maximum_torque'];
                                                                                }

                                                                                //                                                        echo $variant_details_data['engine_maximum_torque'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">IDC Mileage (km/litre)</td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['mileage'])) {
                                                                                if (strtoupper($variant_details_data['mileage']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['mileage']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['mileage'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['mileage'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- 2 -->
                                            <table class="table2 pull-right">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="3" class="tbl_heading">
                                                            Dimensions
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Length (mm)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['dimensions_mm_length'])) {
                                                                                if (strtoupper($variant_details_data['dimensions_mm_length']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['dimensions_mm_length']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['dimensions_mm_length'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Width (mm)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['dimensions_mm_width'])) {
                                                                                if (strtoupper($variant_details_data['dimensions_mm_width']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['dimensions_mm_width']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['dimensions_mm_width'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Height (mm)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['dimensions_mm_height'])) {
                                                                                if (strtoupper($variant_details_data['dimensions_mm_height']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['dimensions_mm_height']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['dimensions_mm_height'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Wheelbase (mm)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['dimensions_mm_weelbase'])) {
                                                                                //                                                        echo $variant_details_data['dimensions_mm_weelbase'];
                                                                                if (strtoupper($variant_details_data['dimensions_mm_weelbase']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['dimensions_mm_weelbase']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['dimensions_mm_weelbase'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Rear seat legroom (cm)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['rear_seat_legroom'])) {
                                                                                if (strtoupper($variant_details_data['rear_seat_legroom']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['rear_seat_legroom']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['rear_seat_legroom'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Headroom (cm)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['headroom'])) {
                                                                                if (strtoupper($variant_details_data['headroom']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['headroom']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['headroom'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Seat Width (cm)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['seat_width'])) {
                                                                                if (strtoupper($variant_details_data['seat_width']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['seat_width']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['seat_width'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Ground clearance (mm)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['dimensions_mm_ground_clearance'])) {
                                                                                if (strtoupper($variant_details_data['dimensions_mm_ground_clearance']) == 'y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['dimensions_mm_ground_clearance']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['dimensions_mm_ground_clearance'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- 3 -->
                                        <div class="container-fluid">
                                            <table class="table2 pull-left">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="3"  class="tbl_heading">
                                                            Steering
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Steering Type
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['streeing_type'])) {
                                                                                echo $variant_details_data['streeing_type'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Turning Radius (m)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['streeing_turn_radius'])) {
                                                                                if (strtoupper($variant_details_data['streeing_turn_radius']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['streeing_turn_radius']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['streeing_turn_radius'];
                                                                                }
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table2 pull-right">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="3"  class="tbl_heading">
                                                            Transmission
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Transmission Type
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($pro_details[0]['tramission_type'])) {
                                                                                echo $pro_details[0]['tramission_type'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">No of gears
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['no_gear'])) {
                                                                                if (strtoupper($variant_details_data['no_gear']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['no_gear']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['no_gear'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['no_gear'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Drivetrain
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['drivetrain'])) {
                                                                                if (strtoupper($variant_details_data['drivetrain']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['drivetrain']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['drivetrain'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['drivetrain'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- 4 -->
                                        </div>
                                        <!-- 5	 -->
                                        <div class="container-fluid">
                                            <table class="table2 pull-left">
                                                <tbody>
                                                    <tr>
                                                        <td  colspan="3"  class="tbl_heading">
                                                            Suspension
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Suspension front
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['suspension_front'])) {
                                                                                if (strtoupper($variant_details_data['suspension_front']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['suspension_front']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['suspension_front'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['suspension_front'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Suspension rear
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['suspension_rear'])) {
                                                                                if (strtoupper($variant_details_data['suspension_rear']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['suspension_rear']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['suspension_rear'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['suspension_rear'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table2 pull-right">
                                                <tbody>
                                                    <tr>
                                                        <td  colspan="3"  class="tbl_heading">
                                                            Tyres
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Tire Size
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['tyre_size'])) {
                                                                                if (strtoupper($variant_details_data['tyre_size']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['tyre_size']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['tyre_size'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['tyre_size'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Wheel Size
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['wheel_size'])) {
                                                                                if (strtoupper($variant_details_data['wheel_size']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['wheel_size']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['wheel_size'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['wheel_size'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Spare Tyre
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['spare_type'])) {
                                                                                if (strtoupper($variant_details_data['spare_type']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['spare_type']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['spare_type'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['spare_type'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- 5	 -->
                                        </div>
                                        <!-- 5	 -->
                                        <div class="container-fluid">
                                            <table class="table2 pull-left">
                                                <tbody>
                                                    <tr>
                                                        <td  colspan="3"  class="tbl_heading">
                                                            Brakes
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Brakes Front
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['brakes_front'])) {
                                                                                if (strtoupper($variant_details_data['brakes_front']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['brakes_front']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['brakes_front'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['brakes_front'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Brakes Rear
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['brakes_rear'])) {
                                                                                if (strtoupper($variant_details_data['brakes_rear']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['brakes_rear']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['brakes_rear'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['brakes_rear'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table2 pull-right">
                                                <tbody>
                                                    <tr>
                                                        <td  colspan="3"  class="tbl_heading">
                                                            Performance
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Top Speed (KMPH)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['top_speed'])) {
                                                                                if (strtoupper($variant_details_data['top_speed']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['top_speed']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['top_speed'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['top_speed'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Acceleration ( 0-100 kmph)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['acceleration'])) {
                                                                                if (strtoupper($variant_details_data['acceleration']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['acceleration']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['acceleration'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['acceleration'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- 5	 -->
                                        </div>
                                        <!-- 5	 -->
                                        <div class="container-fluid">
                                            <table class="table2 pull-left">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="3"  class="tbl_heading">
                                                            Capacity
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Seating capacity
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($pro_details[0]['seating_capacity'])) {

                                                                                echo $pro_details[0]['seating_capacity'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Trunk Volume (l)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['trunk_volume'])) {
                                                                                if (strtoupper($variant_details_data['trunk_volume']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['trunk_volume']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['trunk_volume'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['trunk_volume'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Fuel Tank (l)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['fuel_tank_capacity1'])) {
                                                                                if (strtoupper($variant_details_data['fuel_tank_capacity1']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['fuel_tank_capacity1']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['fuel_tank_capacity1'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['fuel_tank_capacity1'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table2 pull-right">
                                                <tbody>
                                                    <tr>
                                                        <td  colspan="3"  class="tbl_heading">
                                                            Weight
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Kerb Weight (Kgs)
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['kerb_weight'])) {

                                                                                echo $variant_details_data['kerb_weight'];

                                                                                //                                                        echo $variant_details_data['kerb_weight'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="container-fluid">
                                            <table class="table2 pull-left">
                                                <tbody>
                                                    <tr>
                                                        <td  colspan="3"  class="tbl_heading">
                                                            Warranty
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table class="table table-striped table-condensed">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="tbl_wid_3">Standard Warranty
                                                                        </td>
                                                                        <td class="tbl_wid_3">
                                                                            <?php
                                                                            if (!empty($variant_details_data['standard_warranty'])) {


                                                                                if (strtoupper($variant_details_data['standard_warranty']) == 'Y') {
                                                                                    ?>
                                                                                    <i class="fa fa-check"></i>
                                                                                    <?php
                                                                                } else if (strtoupper($variant_details_data['standard_warranty']) == 'N') {
                                                                                    ?>
                                                                                    <i class="fa fa-times"></i>
                                                                                    <?php
                                                                                } else {
                                                                                    echo $variant_details_data['standard_warranty'];
                                                                                }
                                                                                //                                                        echo $variant_details_data['standard_warranty'];
                                                                            } else {
                                                                                echo '-';
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane <?php echo $act02; ?>" id="expert-review" >
                                <div class="user-comments">
                                    <div class="product_feature">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-4 pull-right">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                Sort By
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select id="expert_sort" name="expert_sort" onchange="expert_review();" class="selectpicker">
                                                                    <option value="">Select Sort</option>
                                                                    <option value="Rating">Rating</option>
                                                                    <option value="Date">Date</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="example" class="table table-striped car-selection-table dataTable no-footer" role="grid" aria-describedby="example_info" >
                                                <thead style="display: none;">
                                                    <tr role="row">
                                                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 0px;">Brand</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="expert-review-data">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="user-review">
                                <div class="user-comments">
                                    <div  class="product_feature" >
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-4 pull-right">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                Sort By
                                                            </div>
                                                            <div class="col-md-8">
                                                                <select id="user_sort" name="user_sort" onchange="user_review();" class="selectpicker">
                                                                    <option value="">Select Sort</option>
                                                                    <option value="Rating">Rating</option>
                                                                    <option value="Date">Date</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="example2" class="table table-striped car-selection-table dataTable no-footer" role="grid" aria-describedby="example_info">
                                                <thead style="display: none;">
                                                    <tr role="row">
                                                        <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 0px;">Brand</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="user-review-data" >
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="user-colors">
                                <div class="user-comments">
                                    <div  class="product_feature" >
                                        <div  class="product_feature">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <table>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <div>
                                                                        <?php
                                                                        $c = 1;
                                                                        foreach ($colors as $colors_data) {
                                                                            ?>
                                                                            <div class="color_box">
                                                                                <?php
                                                                                if (!empty($colors_data['feature_img'])) {
                                                                                    ?>
                                                                                    <img width="50" src="<?php echo img_url("uploads/feature/" . $colors_data['feature_img']); ?>"/>

                                                                                    <?php
                                                                                    echo $colors_data['feature_desc'];
                                                                                } else {
                                                                                    if (strtoupper($colors_data['feature_desc']) == 'Y') {
                                                                                        ?>
                                                                                        <i class="fa fa-check"></i>
                                                                                        <?php
                                                                                    } else if (strtoupper($colors_data['feature_desc']) == 'N') {
                                                                                        ?>
                                                                                        <i class="fa fa-times"></i>
                                                                                        <?php
                                                                                    } else {
                                                                                        echo $colors_data['feature_desc'];
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <?php
                                                                            $c++;
                                                                        }
                                                                        ?>    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="user-gallery">
                                <div class="user-comments">
                                    <div  class="product_feature">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12 col-cust-both" >
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="gallery col-md-12 col-cust-both">
                                                                <?php
                                                                $i = 1;
                                                                $count = 1;
                                                                foreach ($car_gallery as $car_gallery_data) {
                                                                    if ($count == 1) {
                                                                        ?>
                                                                        <!--<div class="row">-->                                                                                                                                                                
                                                                        <div class="col-md-12 col-cust-both">
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                        <div class="col-md-3" class="pull-left">        
                                                                            <a href="<?php echo img_url("uploads/portfolio/" . $car_gallery_data['Brand'] . '/' . $car_gallery_data['Model'] . '/' . $car_gallery_data['Image']); ?>" rel="prettyPhoto[gallery1]"><img src="<?php echo img_url('uploads/portfolio/' . $car_gallery_data['Brand'] . '/' . $car_gallery_data['Model'] . '/' . $car_gallery_data['Image']); ?>" class="thumbnail img-responsive"/></a>
                                                                        </div>
                                                                        <?php
                                                                        if ($count == 4) {
                                                                            ?>                                                                                                                                                                        
                                                                        </div>
                                                                        <!--</div>-->                                                                                                                                                                    
                                                                        <?php
                                                                    }
                                                                    if ($count == 4) {
                                                                        $count = 1;
                                                                    } else {
                                                                        $count++;
                                                                    }
                                                                }
                                                                $i++;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <script>
                                                            $(document).ready(function () {
                                                                $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed: 'fast', slideshow: 5000, autoplay_slideshow: false, opacity: 0.80, show_title: true, allow_resize: true, default_width: 500, default_height: 344, counter_separator_label: '/', theme: 'pp_default', horizontal_padding: 20, hideflash: false, wmode: 'opaque', autoplay: false, modal: false, deeplinking: true, overlay_gallery: true, keyboard_shortcuts: true, ie6_fallback: true});
                                                            });
                                                        </script>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row"> 
        <div class="col-md-12 col-cust-both">            
            <div id="static-message" >
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Promise.png">
                        Our Promise
                        <p>Buy Easily and Securely</p>
                    </h4>  
                </div>
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Car Finance.png">
                        Car Finance
                        <p>Finance & Lease Offers</p>
                    </h4>  
                </div>  

                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Assurance.png">
                        Our Assurance
                        <p>Refundable Booking Amount</p>
                    </h4>  
                </div>  
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Trust.png">
                        Our Trust
                        <p>Honest & Friendly Customer Care</p>
                    </h4>  
                </div>  


                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Warranty.png">
                        Warranty
                        <p>After Sales & Service</p>
                    </h4>  
                </div>  
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Car Exchange.png">
                        Car Exchange
                        <p>Free Car Evaluation & Exchange Offers</p>
                    </h4>  
                </div>  

            </div>

        </div>
    </div>
</div>
<!--</div>-->
<div id="book_your_car" class="modal fade in" style="display: none;" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title"> Continue Book your car..</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('external_js/product_ext'); ?>