<?php $this->load->view('templates/responsiveSlider'); ?>
<div class="container">
    <div class="row car-exterior-selection">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed car-exterior-selection-table">
                    <tbody>
                        <tr>
                            <td class="feildset-td">
                                <i class="fa fa-caret-down"></i> Upholstery
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="table table-condensed">
                                    <?php
                                    $leather_cook = $config_selected[0]['leather'];
                                    $cloth_cook = $config_selected[0]['cloth'];
                                    $dashboard_cook = $config_selected[0]['dashboard'];
                                    ?>
                                    <tr class="row-title">
                                        <td>
                                        <div align="center" style="width:900px;font-size: 14px;">
                                        <h4>Check with our customer support for upholstery options!</h4>
                                        <!--<img style="height:100px;margin-bottom:10px; " src="<?php echo base_url(); ?>/assets/img/ComingSoon.png">-->  
                                        </div>
                                            <div style="padding: 20px;">
                                                <img class="img-center" src="<?php echo base_url().'assets/img/ComingSoon.png' ?>" style="height:100px;">
                                            </div>
                                        </td>
                                    </tr>


                                    <!--- Section commented for sometime  --> 

<!--                                    <tr class="row-title">
    <td>
        <h5>Leather:</h5>
    </td>
</tr>
<tr>
    <td>
                                    <?php //$leather_array = array('Fine Nappa leather Chestnut brown', 'Fine Nappa leather Black', 'Fine Nappa leather Pashmina beige');  ?>
        <div class="clearfix">
            <div class="pull-left leather-selection">
                <ul class="asset-selector">
                                    <?php
                                    $cr1 = 1;
                                    if (!empty($car_leather)) {
                                        foreach ($car_leather as $v) {
                                            ?>
                                            <?php
                                            $color_count = $k1 + 1;
                                            $img = $v['feature_img'];
                                            if ($config_selected[0]['leather'] == $v['feature_id']) {
                                                $class = "selected";
                                            } else {
                                                $class = "";
                                            }
                                            ?>
                                            <li data-wheel="<?= base_url("admin/uploads/feature/" . $img); ?>"  data-cost="13000" data-cost-inlcuded="1">
                                                <div class="image-wrapper-div <?php echo $class; ?>" onclick="document.getElementById('leather').value = '<?php echo $v['feature_id']; ?>';">
                                                    <img class="" src="<?php echo base_url("admin/uploads/feature/" . $img); ?>">
                                                    <div>
                                                        <i class="fa fa-check"></i>
                                                    </div>
                                                </div>
                                                <div> 
                                            <?php echo $v['feature_desc'] ?>
                                                    <br/>
                                            <?php
                                            if (isset($v['price'])) {
                                                echo indianFormatNumber($v['price']);
//                                                                            echo 'Rs.' . $v['price'];
                                            }
                                            ?>
                                                </div>
                                            </li>
                                            <?php
                                            $cr1++;
                                        }
                                    } else {
                                        ?>
                                <li>
                                    <div align="center" style="width:900px;font-size: 14px;">
                                     <img style="height:100px;" src="<?php echo base_url(); ?>/assets/img/ComingSoon.png">   
                                        <label >Not applicable for this product because we do not have leather information right now.</label></div> 
                                    <div align="center" style="width:200px;"><label >Not Available</label></div> 
                                </li>                                                        
                                        <?php
                                    }
                                    ?>

                </ul>                                                  

            </div>
        </div>
    </td>
</tr>

<tr class="row-title">
    <td >
        <hr/>
    </td>
</tr>
<tr class="row-title">
    <td >
        <h5>Cloth</h5>
    </td>
</tr>

<tr>
    <td>
        <div class="clearfix">
            <div class="pull-left seat-selection">
                <ul class="asset-selector">
                                    <?php
                                    $cr1 = 1;
                                    if (!empty($car_cloth)) {
                                        foreach ($car_cloth as $v2) {
                                            ?>
                                            <?php
                                            $color_count = $k1 + 1;
                                            $img = $v2['feature_img'];
                                            if ($config_selected[0]['cloth'] == $v2['feature_id']) {
                                                $class = "selected";
                                            } else {
                                                $class = "";
                                            }
                                            ?>
                                            <li data-wheel="<?= base_url("admin/uploads/feature/" . $img); ?>"  data-cost="13000" data-cost-inlcuded="1">
                                                <div class="image-wrapper-div <?php echo $class; ?>" onclick="document.getElementById('cloth').value = '<?php echo $v2['feature_id']; ?>';">
                                                    <img class="" src="<?php echo base_url("admin/uploads/feature/" . $img); ?>">
                                                    <div>
                                                        <i class="fa fa-check"></i>
                                                    </div>
                                                </div>
                                                <div> 
                                            <?php echo $v2['feature_desc'] ?>
                                                    <br/>
                                            <?php
                                            if (isset($v2['price'])) {
                                                echo indianFormatNumber($v2['price']);
//                                                                            echo 'Rs.' . $v2['price'];
                                            }
                                            ?>
                                                </div>
                                            </li>
                                            <?php
                                            $cr1++;
                                        }
                                    } else {
                                        ?>
                                <li>
                                    <div align="center" style="width:900px;font-size: 14px;">
                                        <img style="height:100px;" src="<?php echo base_url(); ?>/assets/img/ComingSoon.png">
                                        <label >Not applicable for this product because we do not have cloth information rite now.</label></div> 
                                    <div align="center" style="width:200px;"><label >Not Available</label></div> 
                                </li>                                                        
                                        <?php
                                    }
                                    ?>                                                        
                </ul>

            </div>
        </div>
    </td>
</tr>-->
                                    <!--- Section commented for sometime  --> 



                                </table>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

            <!--- Section commented for sometime  --> 

            <!--            <div class="table-responsive">
                            <table class="table table-condensed car-exterior-selection-table">
                                <tbody>
                                    <tr>
                                        <td class="feildset-td" colspan="2">
                                            <i class="fa fa-caret-down"></i> Dashboard
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="clearfix">
                                                <div class="pull-left dashboard-selection">
            <?php // $car_dashboard=array('Honey Burl', 'Brushed Aluminium', 'Dark Gray');     ?>
                                                    <ul class="asset-selector">
            <?php
            $cr1 = 1;
            if (!empty($car_dashboard)) {
                foreach ($car_dashboard as $v3) {
//                                                echo '<pre>';
//                                            
//                                            echo '</pre>';
                    ?>
                    <?php
                    $color_count = $k1 + 1;
                    $img = $v3['feature_img'];
                    if ($config_selected[0]['dashboard'] == $v3['feature_id']) {
                        $class = "selected";
                    } else {
                        $class = "";
                    }
                    ?>
                                                                                <li data-wheel="<?= base_url("admin/uploads/feature/" . $img); ?>"  data-cost="13000" data-cost-inlcuded="1">
                                                                                    <div class="image-wrapper-div <?php echo $class; ?>"onclick="document.getElementById('dashboard').value = '<?php echo $v3['feature_id']; ?>', allexterior();">
                                                                                        <img class="" src="<?php echo base_url("admin/uploads/feature/" . $img); ?>">
                                                                                        <div>
                                                                                            <i class="fa fa-check"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div> 
                    <?php echo $v3['feature_desc'] ?>
                                                                                        <br/>
                    <?php
                    if (isset($v3['price'])) {
                        echo indianFormatNumber($v3['price']);
//                                                                echo 'Rs.' . $v3['price'];
                    }
                    ?>
                                                                                    </div>
                                                                                </li>
                    <?php
                    $cr1++;
                }
            } else {
                ?>
                                                                    <li>
                                                                        <div align="center" style="width:900px;font-size: 14px;">
                                                                            <img style="height:100px;" src="<?php echo base_url(); ?>/assets/img/ComingSoon.png">
                                                                            <label >Not applicable for this product because we do not have dashboard information rite now.</label></div> 
                                                                    </li>                                                        
                <?php
            }
            ?>
            
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
            
                                </tbody>
                            </table>
                        </div>-->

            <!--- Section commented for sometime  --> 
            <div class="clearfix">
                <div class="pull-right">
                    <form action="<?php echo base_url('configure-accessory/' . $main_id) ?>" method="POST" style="display:none;">
                        <input type="hidden" id="pro_id" name="pro_id" value="<?php echo $main_id; ?>"/>
                        <input type="hidden" id="dashboard" name="dashboard" value="<?php
                        if (!empty($dashboard_cook)) {
                            echo $dashboard_cook;
                        }
                        ?>"/>    
                        <input type="hidden" id="leather" name="leather" value="<?php
                        if (!empty($leather_cook)) {
                            echo $leather_cook;
                        }
                        ?>"/>   
                        <input type="hidden" id="cloth" name="cloth" value="<?php
                        if (!empty($cloth_cook)) {
                            echo $cloth_cook;
                        }
                        ?>"/>                            
                        <button type="submit" class="btn btn-maroon confuration" name="submit">Save and Continue</button>
                    </form>
                    <!--<a href="<?php echo site_url('configure-accessory/car-' . $id); ?>" class="btn btn-maroon">Save and Continue</a>-->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
//allexterior();    
    /*
     function allexterior()
     {
     var leather = $('#leather').val();
     var cloth = $('#cloth').val();
     var dashboard = $('#dashboard').val();
     document.cookie = "leather=" + leather;
     document.cookie = "cloth=" + cloth;
     document.cookie = "dashboard=" + dashboard;
     //document.cookie="wheel="+wheel;
     //alert(document.cookie);
     //alert(leather);    
     //alert(cloth);    
     //alert(dashboard);    
     }
     */
</script>    
