<?php $this->load->view('templates/responsiveSlider'); 
$data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        if (empty($user_id)) {
            redirect('');
        }
?>
<div class="container" id="payment_page">
    <div class="row" >
        <div class="col-md-12 col-cust-both">
            <div class="container-fluid">
                <div class="row user-review-content">
                    <div class="col-md-9">
                        <?php
                        $car_into_details[0]['pro_image'];
                        $image_data = explode('/', $car_into_details[0]['pro_image']);
                        $cnt = count($image_data);
                        $img_name = $image_data[$cnt - 1];
                        $model_name = $image_data[$cnt - 2];
                        $brand_name = $image_data[$cnt - 3];
                        if ($content == 'carInteriorSelection') {
                            $image = img_url('uploads/product/' . $brand_name . '/' . $model_name . '/int.jpg') ;
                        } else {
                            $image = img_url('uploads/product/' . $brand_name . '/' . $model_name . '/ext.jpg') ;
                        }
                        if (!empty($product_details_data[0]['total_benefits_perc'])) {
                            ?>
                            <div class="ribbin">
                                <div class="corner"><h2>
                                        <?php
                                        echo $product_details_data[0]['total_benefits_perc'] . '&nbsp;&nbsp;&nbsp;&nbsp;';
                                        ?>
                                    </h2>
                                    <img class="img-left img-responsive"  src="<?php echo $image; ?>">   
                                </div>

                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="ribbin">
                                <div class="corner">    
                                    <img class="img-left img-responsive"  src="<?php echo $image; ?>">   
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                    <div class="col-md-3 car-overview-div" align="left">
                        <div class="col-md-12" align="left">
                            <h3 >
                                <span><?php
//                                echo 'pre>';
//                                print_r($car_into_details);
//                                echo '/pre>';
                                echo $full_name=$car_into_details[0]['brand_name'] . ' ' . $car_into_details[0]['model_name'] . ' ' . $car_into_details[0]['pro_name']; ?></span>
                            </h3>
                        </div>    
                        <div class="col-md-12 col-cust-both">
                            <div>
                                <div class="col-md-7">                                    
                                    <span class="text-maroon">Booking Amount</span>
                                </div>
                                <div class="col-md-5">
                                    <i class="fa fa-rupee product_heading"></i><?php 
//                                    print_r($booking_amount);
                                    if($id==$booking_amount['variant_id'])
                                    {
                                    if($booking_amount['booking_amount']>0)
                                    {
                                    echo $booking_price=indianFormatNumber($booking_amount['booking_amount']);    
                                    }
                                    else {
                                    echo $booking_price=indianFormatNumber(trans_amt);    
                                    }
                                    }
                                    ?>
                                </div>
                            </div>                            
                        </div>  
                        <div class="col-md-12 col-cust-both">
                            <div>                                
                                <div class="col-md-7">                                    
                                    <span class="text-maroon">On Road Price</span>
                                </div>
                                <div class="col-md-5">
                                    <i class="fa fa-rupee product_heading"></i>
                                    <?php
//                                    print_r($product_configuration);
                                    if (indianFormatNumber($product_configuration['on_road_price']) == 0) {
                                        echo Not_applicable;
                                    } else {
                                        echo indianFormatNumber($product_configuration['on_road_price']);
                                    }
                                    ?> 

                                </div>
                            </div>                            
                        </div>  
                        <div class="col-md-12 col-cust-both">
                            <div>
                                <div class="col-md-7">                                    
                                    <span class="text-maroon">Total Savings</span>
                                </div>
                                <div class="col-md-5">
                                    <i class="fa fa-rupee product_heading"></i>
                                    <?php
                                    if (indianFormatNumber($product_configuration['total_saving']) == 0) {
                                        echo Not_applicable;
                                    } else {
                                        echo indianFormatNumber($product_configuration['total_saving']);
                                    }
                                    ?> 

                                </div>
                            </div>                            
                        </div>  

                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-cust-both">
                        <form method="post" action="<?php echo base_url() . 'secure'; ?>" name="frmTransaction" id="contact-us" onSubmit="return validate()">
                        <div class="panel-group payment-list" id="selection-list" style="display:block;">
                            <div class="panel">
                                <div class="accordian-header">
                                    <a  data-parent="#selection-list" href="#booking-details">
                                        <h5>
                                            <i class="fa fa-tags"></i>
                                            Payment Details

                                        </h5>
                                    </a>
                                </div>
                                <div id="booking-details" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12" align="center">                                                
                                                <?php
                                                $billing_details = $user_details;
                                                $shipping_details = $shipping_details;
                                                $account_id = '15685';
                                                $invoice_no = $product_configuration['invoice_no'];
                                                $return_url = base_url() . 'booking-success';
                                                ?>
                                                
                                                    <input type="hidden" id="on_road_price" name="on_road_price" value="<?php echo $product_configuration['on_road_price'];?>"/>
                                                    <input type="hidden" id="fewamaount" name="fewamaount"  value="<?php echo preg_replace('/[^0-9\-]/', '',$booking_price);?>"/>
                                                    <input type="hidden" id="pro_id" name="pro_id" value="<?php echo $id; ?>"/>
                                                    <input type="hidden" id="tot_days" name="tot_days" value="0"/>
                                                    <input type="hidden" id="group_discount" name="group_discount" value="0"/>
                                                    <div class="col-md-6" align="left" style="display:block;" id="shipping_add">    
                                                        <table  class="table table-bordered">
                                                               
                                                            <tr >
                                                                <th colspan="2" style="height: 40px;"><span class="style2">Vehicle Registration Address</span>
                                                                    <!--                                                                    <br> <br>   
                                                                                                                                        Your car will be registered for the below address-->
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span> Name</td>
                                                                <td align="left">
                                                                    <input name="ship_name" type="text"  required class="form-control col-md-12" value="<?php echo $shipping_details[0]['name']; ?>" style="margin:5px 0px;" placeholder="firstname"/> 
                                                                    <input name="ship_l_name" type="text" required class="form-control col-md-12" value="<?php echo $shipping_details[0]['lastname']; ?>"   placeholder="lastname" /> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>Address</td>
                                                                <td align="left">
                                                                    <textarea name="ship_address" class="regis_add form-control" required><?php echo $shipping_details[0]['address']; ?></textarea>
                                                                    <!--<input name="ship_address" class="regis_add" value="<?php echo $shipping_details[0]['address']; ?>"  type="text"  required  class="form-control"/>-->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>City</td>
                                                                <td align="left"><input name="ship_city" type="text"  required  class="form-control"value="<?php echo $shipping_details[0]['city']; ?>" />    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>State</td>
                                                                <td align="left"><input name="ship_state" class="regis_add form-control" type="text"  required value="<?php echo $shipping_details[0]['state']; ?>" />    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>PIN Code</td>
                                                                <td align="left"><input name="ship_postal_code" class="regis_add form-control" type="text"  required value="<?php echo $shipping_details[0]['pincode']; ?>" maxlength="6" onkeypress="return isNumberKey(event)"/>    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>Country</td>
                                                                <td align="left">
                                                                    <select name="ship_country" style="width:155px;" class="regis_add form-control">
                                                                        <option value="Select Country" selected="">Select Country</option>
                                                                        <option selected value="IND">India</option>
                                                                    </select>    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>Email</td>
                                                                <td align="left"><input name="ship_email" class="regis_add form-control" value="<?php echo $shipping_details[0]['email']; ?>" type="email" required/>    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>Telephone</td>
                                                                <td align="left"><input name="ship_phone" class="regis_add form-control" value="<?php echo $shipping_details[0]['telephone']; ?>" type="text" required/></td>
                                                            </tr>

                                                        </table>
                                                    </div>



                                                    <div class="col-md-6" align="left" style="display:block;" id="billing_add">
                                                        <table  class="table table-bordered">
                                                            <tr style="display:none;">
                                                                <td class="fieldName" width="100%"><span class="error">*</span>Account Id</td>
                                                                <td  align="left" width="100%">
                                                                    <input name="account_id"type="text" value="<?php echo $account_id; ?>" class="form-control">
                                                                </td>
                                                            </tr>
                                                            <tr style="display:none;">
                                                                <td class="fieldName"><span class="error">*</span>Return Url</td>
                                                                <td align="left">
                                                                    <input name="return_url" type="text"  class="form-control"size="60" value="<?php echo $return_url; ?>" class="form-control"/>
                                                                </td>
                                                            </tr>
                                                            <!--<tr style="display:none;">-->
                                                            <tr style="display:none;">
                                                                <td class="fieldName"><span class="error">*</span>Mode</td>
                                                                <td align="left">
                                                                    <select name="mode" >
                                                                        <option value="LIVE">LIVE</option> 
                                                                        <option value="TEST">TEST</option>                                                                        
                                                                        
                                                                        
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr style="display:none;">
                                                                <th colspan="2"><div align="center"><span class="style2">Transaction Details</span></div></th>
                                                            </tr>
                                                            <tr style="display:none;">
                                                                <td class="fieldName" width="100%"><span class="error">*</span>Reference No</td>
                                                                <td  align="left" width="100%"><input name="reference_no" type="text"   class="form-control"value="<?php echo $invoice_no; ?>"  /></td>
                                                            </tr>
                                                            <tr style="display:none;" title="Enter the Price of the product that is offered for sale">
                                                                <td class="fieldName" width="100%"><span class="error">*</span>Sale Amount</td>
                                                                <td  align="left" width="100%"><input name="amount" id="amount" type="text"/>
                                                                    <!--<strong>INR</strong>-->
                                                                </td>
                                                            </tr>
                                                            <tr style="display:none;" title="Displays the description of the selected / ordered product.">
                                                                <td class="fieldName" width="100%"><span class="error">*</span>Comments</td>
                                                                <td  align="left" width="100%"><input name="description" type="text"  class="form-control"value="Just Same product" /> </td>
                                                            </tr>

                                                            <tr>
                                                                <th colspan="2">Correspondence Address  
                                                                    <input name="shipping_diff" id="shipping_diff" onclick="same_data();" style="width:15px;height:15px;" value="1"  type="checkbox"/>
                                                                    Same as Vehicle Registration Address
                                                                </th>
                                                            </tr>

                                                            <tr>

                                                                <td class="fieldName"><span class="error">*</span>Name</td>
                                                                <td align="left">

                                                                    <input name="name" type="text"  required class="form-control col-md-12" value="<?php echo $billing_details[0]['firstname']; ?>" style="margin:5px 0px;" placeholder="firstname"/> 
                                                                    <input name="l_name" type="text"  required class="form-control col-md-12" value="<?php echo $billing_details[0]['lastname']; ?>"  placeholder="lastname"/> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>Address</td>
                                                                <td align="left">
                                                                    <textarea name="address" required class="form-control"><?php echo $billing_details[0]['address']; ?></textarea>                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>City</td>
                                                                <td align="left"><input name="city" required value="<?php echo $billing_details[0]['city']; ?>" type="text" class="form-control" />    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>State</td>
                                                                <td align="left"><input name="state" value="<?php echo $billing_details[0]['state']; ?>" type="text"  required  class="form-control" />    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>PIN Code</td>
                                                                <td align="left"><input name="postal_code" type="text"  required  class="form-control" value="<?php echo $billing_details[0]['pincode']; ?>" maxlength="6" onkeypress="return isNumberKey(event)"/>    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>Country</td>
                                                                <td align="left"><select name="country" style="width:155px;"  class="form-control" >
                                                                        <option value="Select Country" selected="">Select Country</option>
                                                                        <option selected value="IND">India</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>Email</td>
                                                                <td align="left"><input name="email"  value="<?php echo $billing_details[0]['email']; ?>" type="email" required  class="form-control"/>    </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="fieldName"><span class="error">*</span>Telephone</td>
                                                                <td align="left"><input name="phone" value="<?php echo $billing_details[0]['telephone']; ?>" type="text"  required  class="form-control" maxlength="20"/></td>
                                                            </tr>

                                                        </table>
                                                    </div> 
                                                    
                                                    <table class="table table-striped configure-variant-table">
                                                        <tbody><tr>
                                                            <td valign="top" align="right" colspan="2">
                                                                <div class="col-md-6 left" align="left">
                                                                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $full_name) . '-' . $id) ?>" class="btn btn-xs carsuel_book_gray">
                                                                        Back
                                                                    </a>

                                                                </div>
                                                                <div class="col-md-6 left" align="right">
                                                                    <?php
                                                                    $data['session'] = $this->session->userdata("logged_in_user");
                                                                     if (empty($data['session'])) {
                                                                    ?>
                                                                    <a data-target="#loginModal" data-toggle="modal" class="btn btn-xs carsuel_book">
                                                                        Checkout
                                                                    </a>
                                                                    <?php
                                                                     }
                                                                     else {
                                                                        ?>
                                                                    <button type="submit" class="btn btn-xs  carsuel_book" onclick="check_payment_validate()">Checkout</button>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
<!--                                                                <div class="col-md-6 right" align="right">
                                                                    
                                                                     <?php
                                                                    $data['session'] = $this->session->userdata("logged_in_user");
                                                                    if (empty($data['session'])) {
                                                                        ?>
                                                                        <a data-target="#loginModal" data-toggle="modal" href="#" class="btn btn-xs search-by-price-button carsuel_book">
                                                                            Checkout
                                                                        </a>
                                                                        
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <button type="submit" class="btn btn-xs search-by-price-button carsuel_book checkout" >Checkout</button>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>-->
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                    </table>

<!--                                                    <table  class="table table-striped configure-variant-table">
                                                        <tr>
                                                            <td valign="top" align="right" colspan="2" >
                                                                <div class="col-md-6 left" align="left">
                                                                    <a onclick="history.go(-1);" class="btn btn-xs carsuel_book_gray">
                                                                        Back
                                                                    </a>

                                                                </div>
                                                                <div class="col-md-12 right" align="right">
                                                                    <?php
                                                                    $data['session'] = $this->session->userdata("logged_in_user");
                                                                    if (empty($data['session'])) {
                                                                        ?>
                                                                        <a data-target="#loginModal" data-toggle="modal" href="#" class="btn btn-xs search-by-price-button carsuel_book">
                                                                            Proceed
                                                                        </a>
                                                                        <a style="font-size:16px;padding:8px 20px;" data-target="#loginModal" data-toggle="modal" href="#"  class="btn btn-maroon" >Checkout</a>    
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <button type="button" onclick="gateway();" class="btn btn-xs search-by-price-button carsuel_book" >Proceed</button>        
                                                                        <button type="submit" class="btn btn-xs search-by-price-button carsuel_book" >Checkout</button>        
                                                                        <input name="submitted"  type="submit" class="btn btn-xs search-by-price-button carsuel_book" value="Checkout" />        
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>

                                                        </tr>
                                                    </table>-->
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>





                            </div>


                        </div>


                            <div class="panel-group payment-list payment_gateway" id="selection-list" style="display:none;">
                            <div class="panel">
                                <div class="accordian-header">
                                    <a  data-parent="#selection-list" href="#booking-details">
                                        <h5>
                                            <i class="fa fa-tags"></i>
                                            Payment Gateway

                                        </h5>
                                    </a>
                                </div>
                                <div id="booking-details" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12" align="center">      
                                                <table class="table table-bordered">
                                                    <tbody>                                                        
                                                        <tr>
                                                            <th colspan="2"> 
                                                                <label for="ebs">
                                                                    <span>
                                                                        <a>
                                                                            <img class="img-center fuel_icon pull-left" src="<?php echo base_url();?>/assets/img/ebs.png" style="width:100px">
                                                                            <input type="radio" class="fuel-checkbox pull-left" align="center" name="payment"  id="ebs" value="ebs"  style="margin-top:15px;margin-left:15px;">
                                                                        </a>
                                                                    </span>
                                                                </label>
                                                            </th>
                                                        </tr>                                                    
                                                                                                           
                                                        <tr>
                                                            <th colspan="2"> 
                                                                <label for="paytm">
                                                                    <span>
                                                                        <a>
                                                                            <img class="img-center fuel_icon pull-left" src="<?php echo base_url();?>/assets/img/paytm.png" style="width:100px">
                                                                            <input type="radio" checked="checked" class="fuel-checkbox pull-left" align="center" name="payment"  id="paytm" value="paytm"  style="margin-top:15px;margin-left:15px;">
                                                                        </a>
                                                                    </span>
                                                                </label>
                                                            </th>
                                                        </tr> 
                                                        
                                                                                                           
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>

<?php $this->load->view('external_js/payment_ext'); ?>
