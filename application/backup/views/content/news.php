<style>
    .user-comments .primary > .row > .user-details > .review-count{
        font-size: 12px;
        line-height: 6px;
    }

    .user-star-rating .star-rating s{
        color: #F1C40F !important;
    }

    .user-star-rating .star-rating-active s:hover:before, .user-star-rating .star-rating s.rated:before {
        text-shadow: none!important;
        color: #F1C40F !important;
    }

    .user-comment-box{
        cursor: pointer;
        border-bottom: 1px solid #DDDDDD;
        padding-top: 12px;
        padding-bottom: 6px;
    }
    .user-comment-box:first-child{
        padding-top: 0px;
    }

    .user-comment-box p.date-p{
        font-size: 12px;
    }

    .user-comment-box p.date-p span{
        display: block;
        line-height: 14px;
    }

    .user-comment-box .user-comment-heading span{
        font-size: 13px; margin-left: 6px; border-bottom: 1px dotted;
    }

    .user-comment-box:last-child{
        border-bottom: 0;
    }
</style>
<script>
    $(document).ready(function() {
        news('5');
    });
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50e9a5fc3051d3a9" async></script>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <img src="<?php echo base_url();?>assets/img/ads/Renault-adv.png" class="img-responsive img-center">
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <?php
            if(isset($news)){
                    foreach($news as $news_res){
                    $news_image = $news_res['image'];
                    if($news_image == '') {
                        $news_image = 'news_default.png';
                    }
                    $new_image = base_url() . 'admin/uploads/news/' . $news_image;
                    if (file_exists($new_image)) {
                        $new_image;
                    }
                    ?>
                    <div class="col-md-12" >
                        
                        <div class="primary" style="padding:40px;" >
                            <h4 class="page-heading"><i class="fa fa-bullhorn"></i> <?php echo $news_res['news_title']; ?></h4> 
                            <div class="container-fluid">
                                <div class="row brand-details">
                                    <!--<div class="col-sm-2">-->
                                    <img src="<?php echo $new_image; ?>" style="width:670px;height:auto;">
                                    <!--<img src="<?php echo $new_image; ?>" style="" class="img-center group-img">-->
                                    <!--</div>-->
                                    <div class="col-sm-12">
                                        <p style="text-align:justify;width:100%;height:auto;padding-top:20px;">
                                            <?php echo $news_res['details']; ?>

                                        </p>
                                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                        <div class="addthis_native_toolbox" style="float:right;"></div>
                                       


                                    </div>
                                </div>
                                <div class="row cta1">
                                    <div class="col-md-12 cta1a text-left">
                                        <!--span class="">Author </span><?php echo $news_res['author']; ?>-->
                                    </div>
                                </div>

                            </div>
                        </div> <!-- /.primary -->
                    </div>
                    <?php
                }
            } else {
                ?>
                <table cellpadding="0" cellspacing="0" border="0" class="col-md-9 table table-striped car-selection-table" id="example">
                    <thead style="display: none;">
                        <tr>
                            <th >Brand</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($allnews as $news_res) {
                            $news_image = $news_res['image'];
                            if ($news_image == '') {
                                $news_image = 'news_default.png';
                            }
                            $new_image = base_url() . 'admin/uploads/news/' . $news_image;
                            if (file_exists($new_image)) {
                                $new_image;
                            }
                            ?>
                            <tr>
                                <td>
                                    <div class="col-md-12">
                                        <div class="primary" style="padding:40px;width:100%;height:auto;min-height:280px;">
                                            <div style="float:left;width:40%;">                                            
                                            <a href="<?php echo base_url().'news/'.preg_replace('/\s+/','-',$news_res['news_title']).'-'.$news_res['latest_news_id']; ?>">
					            <img src="<?php echo $new_image; ?>" style="width:100%;height:200px;">
                                            </a>                                                
                                            </div>
                                            
                                            <div style="float:left;width:58%;margin-left:2%">                                            
                                            <a style="float:left;" href="<?php echo base_url().'news/'.preg_replace('/\s+/','-',$news_res['news_title']).'-'.$news_res['latest_news_id']; ?>"><?php echo $news_res['news_title']; ?></a>
                                            <br>
                                            <p style="text-align:justify;height:auto;">
                                                            <?php $details=$news_res['details'];
                                                            $exp=explode('.',$details);
                                                            echo $exp[0].'.';
                                                            echo $exp[1].'.';
                                                            echo $exp[2].'.';
                                                            echo $exp[3].'.';
                                                            ?>
                                            </p>                                             
                                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                            </div>                                           
                                            <div class="container-fluid">
                                                <div class="row brand-details">                                                    
                                                    <div class="col-sm-10">
                                                        
                                                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                                       
                                                    </div>
                                                </div>                       
                                            </div>  
                                        </div>  
                                    </div>     

                                </td>                            
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>  
                </table>  

                <div >                                     
                </div>
                <?php
            }
            ?>

        </div>
        <div class="col-sm-3">
                <div style="padding: 15px 15px 13px;" class="white-bg">
                    <img src="<?php echo base_url();?>assets/img/ads/Duster AD.png" class="img-center img-responsive">
                </div>
            </div>
        
<!--        <div class="col-md-3">
            <div class="group-member-box container-fluid">

                <div class="row grp-banner">
                    <div class="col-md-12">
                        <h5 style="font-size: 16px;" class="review-title">
                            <i class="fa fa-users"></i> 
                            &nbsp;Advertisent
                        </h5>
                    </div>
                </div>
                <div class="row user-box">
                    <div class="col-md-12">
                        <div class="col-md-3 ad-container">
                            <img src="http://107.170.74.203/assets/img/ads/width2.jpeg" style="height: 100px;width: 230px;">
                        </div>  
                    </div>
                </div>


            </div>
        </div>-->

    </div>

    <style>  
    #example_length
    {
    display:none;    
    }
    #example_filter
    {
    display:none;    
    }
    </style>  
