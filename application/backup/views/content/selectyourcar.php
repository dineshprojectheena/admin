<?php
$t_date = date('Y-m-d');
$d1 = new DateTime($t_date);
$end_date = $brand_offer[0]['end_date'];
$end_date_exp = explode('-', $end_date);
$End_day = $end_date_exp[2];
$End_month = $end_date_exp[1];
$End_year = $end_date_exp[0];
$d2 = new DateTime($brand_offer[0]['end_date']);
$days = $d1->diff($d2)->days;
foreach ($car_selection as $car_selection_data)
    $brand_image = $car_selection_data['brand_image'];
$data['session'] = $this->session->userdata("logged_in_user");
$user_id = $data['session']['user_id'];
//print_r($brand_offer);
?>
<input type="hidden" id="brand_id" name="brand_id" value="<?php echo $car_selection_data['brand_id']; ?>"/>
<input type="hidden" id="brand_name" name="brand_name" value="<?php echo $car_selection_data['brand_name']; ?>"/>

<div class="container" id="brand_page_data">    
    <div class="row">
            <?php
            $mnc = 1;
            $zero_count='';
            foreach ($car_variant_data as $car_variant_data_res) {
                ?>
                <?php                
                $count_data = count($car_variant_data_res)-2;
                ?>
                <input type="hidden" value="<?php echo $count_data;?>" id="brand_count<?php echo $car_variant_data_res['model_id'];?>" />
                
                <div class="col-md-12 col-cust-both brand_result_<?php echo $car_variant_data_res['model_id'];?>" >
                <?php
                $mnc++;
                if ($mnc == 2) {
                    ?>
                    <div class="col-md-12 col-xs-12 col-cust-both"> 
                    <div class="col-md-1 col-xs-3  col-sm-3 col-cust-left"> 
                        <img class="img-responsive brand_main_image" src="<?php echo img_url('uploads/brand/' . $brand_image) ; ?>"/>
                    </div>
                    <div class="col-md-3 col-xs-9 col-sm-9 ">
                        <select class="form-control pull-left" id="model2" onchange="get_model_wise_varaint();">
                            <!--<option value="">Select Model</option>-->    
                            <option value="All">All Models</option>    

                            <?php
                            foreach ($car_model as $car_model_res) {
                                ?>  
                                <option <?php
                                if (!empty($_GET['model'] == $car_model_res['model_name'])) {
                                    echo 'selected';
                                }
                                ?> value="<?php echo $car_model_res['model_id']; ?>"><?php echo $car_model_res['model_name']; ?></option>
                                    <?php
                                }
                                ?>
                        </select> 
                    </div>
                    </div>    
                    <?php
                }
                
                
                    
                if ($count_data > 0) {
                    $zero_count=$zero_count.','.$car_variant_data_res['model_id'];
                    ?>
                    <div class="col-md-12 col-xs-12 col-cust-both model_<?php echo $car_variant_data_res['model_id']; ?> all_brand">
                        
                            <div class="pull-right controls">
                                <a href="#myCarousel<?php echo $mnc; ?>" data-slide="prev">
                                    <i class="fa fa-chevron-circle-left fa-2x"></i>
                                </a>
                                <a  href="#myCarousel<?php echo $mnc; ?>" data-slide="next">
                                    <i class="fa fa-chevron-circle-right fa-2x"></i>
                                </a>
                            </div>                       
                    </div>

                    <div id="myCarousel<?php echo $mnc; ?>" class="col-md-12 col-cust-both col-xs-12 carousel<?php echo $mnc; ?> slide model_<?php echo $car_variant_data_res['model_id']; ?> all_brand" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <?php
//                        if($count_data>0)
//                        {    
                            $division = $count_data / 3;
                            $exp_res = explode('.', $division);
                            $remaing = $exp_res[0];
                            if (isset($exp_res[1]) > 0) {
                                $remaing++;
                            }
                            for ($j = 1, $i = 0, $f = 2; $j <= $remaing; $j++) {
                                if ($j == 1) {
                                    $class = 'active';
                                } else {
                                    $class = '';
                                }
                                ?>        
                                <div class="item <?php echo $class; ?> ">
                                    <?php
                                    for ($i; $i <= $f; $i++) {
                                        if (isset($car_variant_data_res[$i]['pro_image'])) {
                                            ?>
                                            <div class="col-md-4 col-xs-12 col-cust-both">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-12 bran_div_carousel col-cust-both">
                                                            <div class="ribbin">	
                                                                <div class="corner">
                                                                    <?php
                                                                    if (!empty($car_variant_data_res[$i]['total_benefits_perc'])) {
                                                                        ?>
                                                                        <h2><?php echo $car_variant_data_res[$i]['total_benefits_perc']; ?></h2>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <div class="group-item">
                                                                        <div class="col-md-12">
                                                                            <div class="img-holder col-md-12">
                                                                                <?php //echo url_struct('car/' . preg_replace('/\s+/', '-', $car_variant_data_res[$i]['pro_name_comp']) . '-' . $car_variant_data_res[$i]['variant_id']) ?>
                                                                                <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $car_variant_data_res[$i]['pro_name_comp']) . '-' . $car_variant_data_res[$i]['variant_id']) ?>" >
                                                                                    <img alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $car_variant_data_res[$i]['pro_image'])); ?>" src="<?php echo img_url('uploads/product/' . $car_variant_data_res[$i]['pro_image']) ; ?>" alt="<?php echo $car_variant_data_res[$i]['pro_image']; ?>"  class="img-responsive">
                                                                                </a>                                                                                
                                                                            </div>
                                                                        </div>    
                                                                        <div class="group-content">
                                                                            <div class="container-fluid">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <h4 class="hot_dealreview_title"> <?php echo $car_variant_data_res[$i]['pro_name_comp']; ?> </h4>
                                                                                    </div> 
                                                                                    <div class="col-md-12">
                                                                                        <div class="group-price">
                                                                                            <div class="mnc-price" >
                                                                                                <span class="price-span">
                                                                                                    Ex-Showroom Price
                                                                                                </span>
                                                                                                <?php
                                                                                                if (indianFormatNumber($car_variant_data_res[$i]['mnc_exshowroom_price']) > 0) {
                                                                                                    ?>
                                                                                                    <span class="price-span text-maroon" >
                                                                                                        <i class="fa fa-rupee"></i>
                                                                                                        <?php
                                                                                                        if (indianFormatNumber($car_variant_data_res[$i]['mnc_exshowroom_price']) > 0) {
                                                                                                            echo indianFormatNumber($car_variant_data_res[$i]['mnc_exshowroom_price']);
                                                                                                        }
                                                                                                        ?> 
                                                                                                    </span>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                            </div>
                                                                                            <div class="mnc-price" >
                                                                                                <span class="price-span">Total Savings </span>
                                                                                                <span class="price-span text-maroon" ><i class="fa fa-rupee"></i>
                                                                                                    <?php
                                                                                                    echo indianFormatNumber($car_variant_data_res[$i]['total_savings']);
                                                                                                    ?> 
                                                                                                </span>
                                                                                            </div>
                                                                                            <div class="mnc-price" >
                                                                                                <div class="col-md-12">                                                   
                                                                                                    <div class="pull-left">                                                   
                                                                                                        <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carinsurance_hotdeals.png' ?>" data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Insurance Offers"/>
                                                                                                        <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carfinance_hotdeals.png' ?>"  data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Finance Offers"/>			
                                                                                                        <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carexchange.png' ?>" data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="right" title="Car Exchange  Offers"/>                       
                                                                                                        <img src="<?php echo base_url() . 'assets/img/product/otherbenefits.png' ?>"data-toggle="tooltip" class="main_tooltip car_icon_css"  data-placement="top" title="<?php echo $data[$i]['accessory_package']; ?>"/>
                                                                                                    </div>
                                                                                                    <div class="pull-right">
                                                                                                        <a  href="<?php echo site_url('car/' . preg_replace('/\s+/', '-', $car_variant_data_res[$i]['pro_name_comp']) . '-' . $car_variant_data_res[$i]['variant_id']) ?>" class="btn btn-xs  carsuel_book">Book</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>    

                                                                                    <!--                                                                                    <div class="col-md-3">
                                                                                                                                                                            <div class="clearfix" style="margin-top:0px;">
                                                                                                                                                                                
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>    -->
                                                                                    <!--                                                                                    <div class="col-md-12">
                                                                                                                                                                            
                                                                                                                                                                        </div>  -->

                                                                                </div>      
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>      
                                                            </div>      
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                                $f = $f + 3;
                            }
//                        }
                            ?>
                        </div> 
                    </div>
                    <?php
                }
                ?>
                <script>
                    $(document).ready(function () {
                        $('#myCarousel<?php echo $mnc; ?>').carousel({
                            interval: false
                        })

                        $('#myCarousel<?php echo $mnc; ?>').on('slid.bs.carousel<?php echo $mnc; ?>', function () {
                        });
                    });
                </script>
                </div>
                <?php
            }
//            echo $zero_count;
            ?>
        <input type="hidden" id="zero_ids" value="<?php echo $zero_count;?>"/>   
    </div>
    <br>

</div>
<div class="container">
    <div class="row"> 
        <div class="col-md-12 col-cust-both">            
            <div id="static-message" >
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Promise.png">
                        Our Promise
                        <p>Buy Easily and Securely</p>
                    </h4>  
                </div>
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Car Finance.png">
                        Car Finance
                        <p>Finance & Lease Offers</p>
                    </h4>  
                </div>  

                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Assurance.png">
                        Our Assurance
                        <p>Refundable Booking Amount</p>
                    </h4>  
                </div>  
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Trust.png">
                        Our Trust
                        <p>Honest & Friendly Customer Care</p>
                    </h4>  
                </div>  


                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Warranty.png">
                        Warranty
                        <p>After Sales & Service</p>
                    </h4>  
                </div>  
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Car Exchange.png">
                        Car Exchange
                        <p>Free Car Evaluation & Exchange Offers</p>
                    </h4>  
                </div>  

                </div>

            </div>
        </div>
    </div>
<div class="container">
    <div class="row">

        <div class="col-md-12 col-cust-both ">
            <div class="col-md-9 col-cust-both">
                <div class="pull-right">
                    <p class="text-center compact-p"><strong>Share on</strong></p>
                    <ul class="share-buttons-ul brand-page-share">
                        <li>
                            <a href="<?php echo FB_link; ?>">
                                <img src="<?= base_url() . 'assets/img/buttons/facebook.png' ?>" />
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo GM_link; ?>">
                                <img src="<?= base_url() . 'assets/img/buttons/google.png' ?>" />
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo LD_link; ?>">
                                <img src="<?= base_url() . 'assets/img/buttons/linkedin.png' ?>" />
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo GM_link; ?>">
                                <img src="<?= base_url() . 'assets/img/buttons/twitter.png' ?>" />
                            </a>
                        </li>                           
                    </ul>
                </div> 
            </div> 
            <div class="col-md-3 col-cust-both">
                <?php
                if (!empty($data['session'])) {
                    $conditions = 'onclick="joingroup();"';
                } else {
                    $conditions = 'data-toggle="modal" data-target="#loginModal"';
                }
                ?>

                <div class="join-grp-div pull-right">
                    <img class="img-center" style="" src="<?= base_url() . 'uploads/brand_join.png' ?>" />
                    <a href="#" <?php echo $conditions; ?> >
                        <?php
                        if (empty($join_status)) {
                            ?>
                            <button type="button" name="joing_button" class="join btn btn-sm btn-maroon">Join the <?php echo str_replace("-", " ", $car_selection_data['brand_name']); ?> brand group</button>
                            <?php
                        } else {
                            ?>
                            <button type="button" name="joing_button" class="join btn btn-sm btn-yellow" style="margin-top: 10px;font-size: 14px;color:black;">Already part of the group</button>
                            <?php
                        }
                        ?>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-cust-both">
            <h3 class="club_mem"><?php echo str_replace("-", " ", $car_selection_data['brand_name']); ?>  Club Members</h3>
    <!--            <p class="compact-p">
                    This is the Lorem Ipsum which will be shown to the user on homepage. This is the Lorem Ipsum which will be shown to the user on homepage.
                </p>   -->
            <div class="pull-right controls">
                <a href="#user-carousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a href="#user-carousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div> 
        <div class="col-md-12 col-cust-both">
            <div id="user-carousel"  class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active container-fluid">
                        <div class="row">
                            <?php
//                            print_r($brand_user);
                            foreach ($brand_user as $brand_user_data) {
                                ?>
                                <div class="col-sm-1">
                                    <?php
                                    $data['session'] = $this->session->userdata("logged_in_user");
                                    if (!empty($data['session'])) {
                                        ?>
                                                                                                                                                    <!--<a href="<?php echo site_url('networking/' . preg_replace('/\s+/', '-', $car_selection_data['brand_name'])) ?>">-->
                                        <a href="#">
                                        <?php } else { ?>
                                            <a  href="#" data-toggle="modal" data-target="#loginModal">
                                            <?php } ?>

                                            <?php
                                            $file = 'uploads/user/' . $brand_user_data['fb_id'] . '.jpg';
                                            if (file_exists($file)) {
                                                $file = 'uploads/user/' . $brand_user_data['fb_id'] . '.jpg';
                                            } else {
                                                $file = 'uploads/user/no-image.png';
                                            }
                                            ?>
                                            <img class="img-center img-responsive" src="<?= base_url() . $file; ?>">
                                        </a>
                                </div>                                
                                <?php
                            }
                            ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

    </div>
</div>
<?php
$this->load->view('external_js/brand.php');
?>

