<div class="container" id="user_page">
    <div class="row">
        <?php
        $data['session'] = $this->session->userdata("logged_in_user");
        $user_id = $data['session']['user_id'];
        if (empty($user_id)) {
            redirect('');
        }
        foreach ($user_details as $user_details_data)
            
            ?>
        <div class="col-md-12 position-relative col-cust-both">
            <a href="#" onclick="$('#cover_profile').click();">
                <?php
//                if (!empty($user_details_data['cover'])) {
//                    $cover = $user_details_data['cover'];
//                } else {
//                    $cover = 'cover_photo.png';
//                }
//                echo $cover;
//                if (strpos($user_details_data['cover'], 'are') !== false) {
//                    echo $user_details_data['cover'];
//                }
//                 $cover = $user_details_data['cover'].'.png';
//                print_r($user_details_data);
                $image_path_medium = site_url('uploads/cover/');
//                $image_not_found_medium = $image_path_medium . "/" . "not-found.jpg";
                $image_name_with_path = $image_path_medium . "/" . $user_details_data['cover']; //this is your image url
                $image_file_path = site_url('uploads/cover/' . $user_details_data['cover']); //this is your file path

                if (file_exists($image_file_path . '.png')) {
                    $image = $image_name_with_path . '.png';
                } else if (file_exists($image_file_path . '.jpg')) {
                    $image = $image_name_with_path . '.jpg';
                }
//                echo $image;
//                print_r($user_details_data);
                ?>
                <img src="<?php echo $image; ?>" class="img-responsive user_image" />
            </a>
            <div class="container-fluid back_cover">
                <div class="row">
                    <div class="col-md-12 back_cover_color">
                        <div class="container-fluid">
                            <div class="row">                                
                                <div class="col-md-12">
                                    <div class="col-md-2 col-xs-2">
                                        <?php
                                        $file = site_url('uploads/user/' . $user_details_data['fb_id']);
                                        if (file_exists($file.'.png')) {
                                            $file = $file.'.png';
                                        }
                                        if(file_exists($file.'.jpg')) {
                                            $file = $file.'.jpg';
                                        }
//                                        echo $file; 
                                        ?>
                                        <form enctype="multipart/form-data" class="form-horizontal" role="form" action="<?php echo site_url("authorization/upload_profile"); ?>" method="POST">
                                            <img class="img-circle img-center img-thumbnail user_profile_img"  src="<?php echo $file; ?>"/>
                                            <div class="fileUpload btn btn-primary">
                                                <span>Change profile image</span>
                                                <input type="file" accept="image/*" capture="camera"  name="profile" id="profile" value="change profile" onchange="$('#submit').click();" class="upload" />
                                            </div>
                                            <input type="submit" name="submit" id="submit" style="display:none;" >
                                        </form>

                                    </div>
                                    <div class="col-md-10 col-xs-10">    
                                        <form  enctype="multipart/form-data" class="form-horizontal" role="form" action="<?php echo site_url("authorization/upload_cover"); ?>" method="POST">
                                            <div class="fileUpload btn btn-primary pull-right" style="top:100px;">
                                                <span>Change Cover image</span>
                                                <input type="file" accept="image/*" capture="camera"  name="cover_profile" id="cover_profile" value="change cover" onchange="$('#submit2').click();" class="upload" />
                                            </div>
                                            <!--<input type="file" name="cover_profile" id="cover_profile" onchange="$('#submit2').click();"  class="upload" />-->
                                            <input type="submit" name="submit2" id="submit2" style="display:none;"   >
                                        </form>
                                    </div>
                                    <!--                                <div class="col-md-10">
                                                                        <div class="container-fluid">
                                                                            <div class="row">
                                    
                                                                            </div>
                                                                        </div>
                                                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>        
        </div>


    </div>

    <div class="row">
        <div class="col-md-5 col-cust-left">
            <div class="table-responsive">
                <table class="table configure-options-table">
                    <tbody>
                        <tr class="hidden">
                            <td colspan="3" class="headline">
                                <p class="text-left">
                                    SETTINGS
                                </p>

                            </td>
                        </tr>

                        <tr class="hidden">
                            <td colspan="4">
                                <div class="pull-left text-center">
                                    <p>
                                        <a href="#" class="normal-text">
                                            <i class="fa fa-cog fa-2x"></i>
                                        </a>
                                    </p>
                                </div>
                                <div class="pull-left">
                                    <p>
                                        <a href="<?php echo base_url(); ?>edit-profile" class="normal-text">
                                            ACCOUNT SETTING
                                        </a>
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <tr class="hidden">
                            <td colspan="4">
                                <div class="pull-left text-center">
                                    <p>
                                        <a href="#" class="normal-text">
                                            <i class="fa fa-lock fa-2x"></i>
                                        </a>
                                    </p>
                                </div>
                                <div class="pull-left">
                                    <p>
                                        <a href="#" class="normal-text">
                                            PRIVACY SETTING
                                        </a>
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="headline">
                                <p class="text-left">
                                    MY CLUB BRANDS
                                </p>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="col-md-12">
<?php
if (!empty($user_groups)) {
    foreach ($user_groups as $user_groups_data) {
        ?>
                                            <div class="pull-left" align="center">  
                                                <a href="<?php echo url_struct('brand/' . preg_replace('/\s+/', '-', $user_groups_data['brand_name'])) ?>">
                                                    <img src="<?= img_url('uploads/brand/' . $user_groups_data['brand_image']); ?>" class="img-center user_brand_image">
                                                </a>
                                                <p >
                                                    <a class="normal-text" href="<?php echo url_struct('brand/' . preg_replace('/\s+/', '-', $user_groups_data['brand_name'])) ?>">
        <?php echo $user_groups_data['brand_name']; ?>
                                                    </a>
                                                </p>
                                            </div>   
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                        <b>No Group joined!</b>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3" class="headline">
                                <p class="text-left">
                                    MY RATINGS
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <div class="col-md-12" align="center" >
<?php
if (!empty($user_rating)) {
    foreach ($user_rating as $user_rating_data) {
        ?>
                                            <div align="left" class="pull-left col-md-12">    
                                                <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $user_rating_data['pro_name_comp']) . '-' . $user_rating_data['variant_id']) ?>">
                                                    <img class="img-left user_brand_car_image" src="<?php echo img_url('uploads/product/' . $user_rating_data['pro_image']); ?>">
                                                </a>

                                                <div class="my-car-rating">
                                                    <span>
                                                        <a class="car-name" href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $user_rating_data['pro_name_comp']) . '-' . $user_rating_data['variant_id']) ?>">
        <?php echo $user_rating_data['pro_name_comp']; ?>
                                                        </a>
                                                    </span>
                                                    <br>
                                                    <span class="text-golden">
        <?php
        $star = $user_rating_data['total'];
        if (isset($star)) {
            $exp = explode('.', $star);
            $point = $exp[1];
            if ($point > 00) {
                $half = '-half-full';
            } else if ($point == 00) {
                $half = '-o';
            } else {
                $half = '';
            }

            if ($exp[0] == 1) {
                ?>
                                                                <i class="fa fa-star"></i><?php ?>
                                                                <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                                <?php
                                                            } else if ($exp[0] == 2) {
                                                                ?>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                                            } else if ($exp[0] == 3) {
                                                                ?>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                                            } else if ($exp[0] == 4) {
                                                                ?>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                            <?php
                                                        }
                                                        ?>  
                                                    </span>
                                                </div>
                                            </div>   
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                        <div align="left" class="pull-left col-md-12">  
                                            <b>No Cars Rated!</b>
                                        </div>  
                                        <?php
                                    }
                                    ?>
                                </div>

                            </td>                            
                            <td></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-7 col-cust-right">

            <table class="table configure-options-table">
                <tbody>
                    <tr>
                        <td colspan="4" class="headline">
                            <p class="text-left">
                                CONTACT DETAILS
                            </p>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <p>Full Name: <?php echo $user_details_data['firstname'] . ' ' . $user_details_data['lastname']; ?>
                                <p>Email: <?php echo $user_details_data['email']; ?></p>                                    
                                <p>Phone Number: <?php echo $user_details_data['telephone']; ?></p>                                    
                                <p>Address: <?php echo $user_details_data['address']; ?></p>                                    

                            </div>
                            <div class="col-md-12">
                                <a class="btn btn-sm carsuel_book pull-right" href="<?php echo base_url(); ?>edit-profile">Edit Profile</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="headline">
                            <p class="text-left">
                                BOOKING INFORMATION

                            </p>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="container-fluid">
<?php
foreach ($user_book_confiuration as $user_book_confiuration_data) {
    ?>                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $user_book_confiuration_data['pro_name_comp']) . '-' . $user_book_confiuration_data['product_id']) ?>">
                                                <img class="img-left img-responsive user_brand_car_image" src="<?php echo img_url('uploads/product/' . $user_book_confiuration_data['pro_image']); ?>">
                                            </a>
                                        </div>
                                        <div class="col-md-9">
                                            <p>
                                                <b>
                                                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $user_book_confiuration_data['pro_name_comp']) . '-' . $user_book_confiuration_data['product_id']) ?>"><?php echo $user_book_confiuration_data['pro_name_comp']; ?></a>
                                                    <!--a href="<?php echo site_url('car-introduction/' . preg_replace('/\s+/', '-', $user_book_confiuration_data['pro_name']) . '-' . $user_book_confiuration_data['product_id']) ?>"><?php echo $user_book_confiuration_data['pro_name_comp']; ?></a-->
                                                </b> 
                                            </p>
                                            <p>
                                                <b>
                                                    Invoice No:- <?php echo $user_book_confiuration_data['invoice_no']; ?></a>
                                                    <span> <a href="<?php echo base_url('payment/download_sucess_pdf/' . $user_book_confiuration_data['order_id']); ?>"><i class="fa fa-file-pdf-o"></i> Download as pdf</a> </span>	
                                                </b> 
                                            </p>
                                            <p>
    <?php
    echo $start = limit_words($user_book_confiuration_data['pro_detail'], 2) . '...';
    ?>    
                                            </p>                                            
                                        </div>
                                    </div>
                                                <?php
                                            }
                                            ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="headline">
                            <p class="text-left">
                                MY SHORTLISTED CARS

                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="container-fluid">
<?php
foreach ($user_car_confiuration as $user_car_confiuration_data) {
    ?>                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $user_car_confiuration_data['pro_name_comp']) . '-' . $user_car_confiuration_data['product_id']) ?>">
                                                <img class="img-left img-responsive user_brand_car_image" src="<?php echo img_url('uploads/product/' . $user_car_confiuration_data['pro_image']); ?>">
                                                <br>
                                                <div align="center">
                                                    <a href="#" onclick=delete_config("<?php echo base_url() . 'authorization/delete_configuration/' . $user_car_confiuration_data['car_configuration_id']; ?>");>Delete</a>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-9">
                                            <p>
                                                <b>
                                                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $user_car_confiuration_data['pro_name_comp']) . '-' . $user_car_confiuration_data['product_id']) ?>"><?php echo $user_book_confiuration_data['pro_name_comp']; ?></a>
                                                    <!--a href="<?php echo site_url('car-introduction/' . preg_replace('/\s+/', '-', $user_car_confiuration_data['pro_name']) . '-' . $user_car_confiuration_data['product_id']) ?>"><?php echo $user_book_confiuration_data['pro_name_comp']; ?></a-->
                                                </b> 
                                            </p>
                                            <p>
    <?php
    echo $start = limit_words($user_book_confiuration_data['pro_detail'], 2) . '...';
    ?>    
                                            </p>                                            
                                        </div>
                                    </div>
                                                <?php
                                            }
                                            ?>
                            </div>
                                <?php

                                function limit_words($string, $word_limit) {
                                    $words = explode(".", $string);
                                    return implode(".", array_splice($words, 0, $word_limit));
                                }
                                ?>
                        </td>
                    </tr>         
                </tbody>
            </table>

        </div>
    </div>
</div>
</script>