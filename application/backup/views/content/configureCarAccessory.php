<?php
$this->load->view('templates/responsiveSlider');
$features = array(
    '1' => 'Exteriors',
    '2' => 'Interiors',
    '3' => 'Safety & Technology',
    '4' => 'Audio & Communication',
    '5' => 'Services',
    '6' => 'Equipment Package');
$subcategory = array(
    '1' => 'Headlights',
    '2' => 'Sunroofs',
    '3' => 'Tyres',
    '4' => 'Steering Wheels',
    '5' => 'Seats',
    '6' => 'Navigation System',
    '7' => 'Audio System',
    '8' => 'Speakers',
    '9' => 'Telephone',
    '10' => 'Others'
);
$count = count($subcategory);
?>
<div class="container">
    <div class="row" style="display:none;">
        <div class="col-md-12 mnc-tab text-center">
            <ul class="nav nav-tabs">

                <li class="active"><a href="#exterior" data-toggle="tab">Exterior</a></li>
                <li><a href="#interior" data-toggle="tab">Interior</a></li>
                <li><a href="#safety" data-toggle="tab">Safety & Technology</a></li>
                <li><a href="#audio" data-toggle="tab">Audio & Communication</a></li>
                <!--<li><a href="#warranty" data-toggle="tab">Services</a></li>-->
                <li><a href="#equipment" data-toggle="tab">Equipment Package</a></li>
            </ul>
        </div>
    </div>
    <?php
    $accessory_cook = $car_selected_data[0]['accessory'];
    $accessory_total = $car_selected_data[0]['accessory_total'];
    $exp_acc = explode(",", $accessory_cook);
    ?>
    
    <div class="row" style="display:none;">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane" id="equipment">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped car-accessory-table">
                            <thead>
                                <tr>
                                    <th width="50%">Description</th>
                                    <th width="10%">Image</th>
                                    <th width="10%">MRP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $grand_total_amt = '';
                                if (!empty($car_equipment)) {
                                    foreach($car_equipment as $car_exterior_data){
                                        if ($car_exterior_data['accessory_sub_cat'] == 10){
                                            if(in_array($car_exterior_data["accessory_id"],$exp_acc)){
                                                $class = 'checked';
                                            }else {
                                                $class = '';
                                            }
                                            $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                            if(file_exists($file)) {
//                                                $img;
//                                            } else {
//                                                $img = 'uploads/user/no-image.png';
//                                            }

                                            $img = base_url().$img;
                                            
                                            
                                            $price = $car_exterior_data['price'];
                                            ?>
                                            <tr>
                                                <td width="50%">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" <?php echo $class; ?> onclick="set_total_price('<?php echo $price; ?>')" class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>" >
                                                            <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>"><?php echo $car_exterior_data['accessory_name']; ?></div>
                                                        </label>
                                                    </div>
                                                    <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                    <!--                                                    <div class="category-details">
                                                                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                    
                                                                                                        </div>-->
                                                </td>
                                                <td width="10%">
                                                    <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                </td>
                                                <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                    
                                                    <?php
                                                    echo $car_exterior_data['price'];
                                                    $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                    ?>
                                                </td>
                                            </tr>                                            
                                            <?php
                                        }
                                    }
                                } else {
                                    ?>    
                                    <tr>
                                        <td colspan="3">  
                                            <div align="center" style="width:200px;"><label >Not Available</label></div>    
                                        </td>    
                                    </tr> 
                                    <?php
                                }
                                ?>

                            </tbody>

                        </table>
                    </div>
                </div>

                <div class="tab-pane active" id="exterior">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped car-accessory-table">
                            <thead>
                                <tr>
                                    <th width="50%">Description</th>
                                    <th width="10%">Image</th>
                                    <th width="10%">MRP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Headlights
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach($car_exterior as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 1) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    
                                                    $img = 'admin/uploads/accessory/' . $car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)) {
//                                                        $img;
//                                                    } else {
//                                                        $img = 'uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
                                                    $price = $car_exterior_data['price'];
                                                    $accessory_id = $car_exterior_data['accessory_id'];
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" <?php echo $class; ?> id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" class="high1" onclick="price_calculation(<?php echo $car_exterior_data['accessory_id']; ?>);"  data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['accessory_id'] ?>">
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <?php
                                            foreach ($car_exterior as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 2) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    
                                                    $img = 'admin/uploads/accessory/' . $car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)) {
//                                                        $img;
//                                                    } else {
//                                                        $img = 'uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" type="checkbox" <?php echo $class; ?> onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>">
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>                                                                 
                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                                                                                <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?>        
                                                    
                                                    
                                        </table>
                                    </td>
                                </tr>
                                <?php
//                                            foreach ($car_exterior as $car_exterior_data) 
//                                            {
//                                                if(!empty($car_exterior_data['accessory_sub_cat']))
//                                                {
//                                                $dis_div='none';    
//                                                }
//                                            }
                                
                                ?>
                                <!--<tr style="display:<?php echo $dis_div;?>">-->
<!--                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_exterior as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 2) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    
                                                    $img = 'admin/uploads/accessory/' . $car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)) {
//                                                        $img;
//                                                    } else {
//                                                        $img = 'uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" type="checkbox" <?php echo $class; ?> onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>">
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>                                                                 
                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                                                                                <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?>



                                        </table>
                                    </td>
                                </tr>-->
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Tyres
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_exterior as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 3) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    
                                                    $img = 'admin/uploads/accessory/' . $car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)) {
//                                                        $img;
//                                                    } else {
//                                                        $img = 'uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" type="checkbox" <?php echo $class; ?> onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>"> 
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>                                                                        
                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?> 
                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?>



                                        </table>
                                    </td>
                                </tr>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Other
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_exterior as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 10) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    
                                                    $img = 'admin/uploads/accessory/' . $car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)) {
//                                                        $img;
//                                                    } else {
//                                                        $img = 'uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" type="checkbox" <?php echo $class; ?> onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>">
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>

                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?>


                                        </table>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="interior">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped car-accessory-table">
                            <thead>
                                <tr>
                                    <th width="50%">Description</th>
                                    <th width="10%">Image</th>
                                    <th width="10%">MRP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Steering Wheels
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_interior as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 4) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    
                                                    $img = 'admin/uploads/accessory/' . $car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)) {
//                                                        $img;
//                                                    } else {
//                                                        $img = 'uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>"> 
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>                                                                            
                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?>


                                        </table>
                                    </td>
                                </tr>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Seats
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_interior as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 5) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    
                                                    $img = 'admin/uploads/accessory/' . $car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)) {
//                                                        $img;
//                                                    } else {
//                                                        $img = 'uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>"> 
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>                                                                            
                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?>   
                                        </table>
                                    </td>
                                </tr>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Other
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_interior as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 10) {
                                                    if(in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    }else{
                                                        $class = '';
                                                    }
                                                    
                                                    
                                                    $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)){
//                                                        $img;
//                                                    }else{
//                                                        $img='uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>">
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>

                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </table>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="safety">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped car-accessory-table">
                            <thead>
                                <tr>
                                    <th width="50%">Description</th>
                                    <th width="10%">Image</th>
                                    <th width="10%">MRP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            If (!empty($car_safety)) {
                                                foreach ($car_safety as $car_exterior_data) {
                                                    if ($car_exterior_data['accessory_sub_cat'] == 10) {
                                                        if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                            $class = 'checked';
                                                        } else {
                                                            $class = '';
                                                        }
                                                        $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)){
//                                                        $img;
//                                                    }else{
//                                                        $img='uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                        
                                                        
//                                                        if ($car_exterior_data['accessory_img'] == '') {
//                                                            $img = 'no-image.png';
//                                                        } else {
//                                                            $img = $car_exterior_data['accessory_img'];
//                                                        }
//                                                        $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                        ?>
                                                        <tr>
                                                            <td width="50%">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>"> 
                                                                        <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                            <?php echo $car_exterior_data['accessory_name']; ?>
                                                                        </div>                                                                    
                                                                    </label>
                                                                </div>
                                                                <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                                <!--                                                    <div class="category-details">
                                                                                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                                
                                                                                                                    </div>-->
                                                            </td>
                                                            <td width="10%">
                                                                <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                            </td>
                                                            <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                                <?php
                                                                echo $car_exterior_data['price'];
                                                                $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                                ?>   

                                                            </td>
                                                        </tr>                                            
                                                        <?php
                                                    }
                                                }
                                            } else {
                                                ?>    
                                                <tr>
                                                    <td colspan="3">  
                                                        <div align="center" style="width:200px;"><label >Not Available</label></div>    
                                                    </td>    
                                                </tr> 
                                                <?php
                                            }
                                            ?> 



                                        </table>
                                    </td>
                                </tr>    
                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="audio">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped car-accessory-table">
                            <thead>
                                <tr>
                                    <th width="50%">Description</th>
                                    <th width="10%">Image</th>
                                    <th width="10%">MRP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Navigation systems
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_audio as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 6) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)){
//                                                        $img;
//                                                    }else{
//                                                        $img='uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>"> 
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>    

                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?>                                           


                                        </table>
                                    </td>
                                </tr>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Audio systems
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_audio as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 7) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)){
//                                                        $img;
//                                                    }else{
//                                                        $img='uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>">
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>

                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?> 

                                        </table>
                                    </td>
                                </tr>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Speakers
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_audio as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 8) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)){
//                                                        $img;
//                                                    }else{
//                                                        $img='uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>"> 
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>   

                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?> 

                                        </table>
                                    </td>
                                </tr>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Telephone
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_audio as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 9) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)){
//                                                        $img;
//                                                    }else{
//                                                        $img='uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>"> 
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>                                                                            
                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?> 

                                        </table>
                                    </td>
                                </tr>
                                <tr class="fieldset-tr">
                                    <td class="feildset-td" colspan="4">
                                        <i class="fa fa-caret-down"></i> Other
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="table table-hover table-striped nested-table">
                                            <?php
                                            foreach ($car_audio as $car_exterior_data) {
                                                if ($car_exterior_data['accessory_sub_cat'] == 10) {
                                                    if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                        $class = 'checked';
                                                    } else {
                                                        $class = '';
                                                    }
//                                                    if ($car_exterior_data['accessory_img'] == '') {
//                                                        $img = 'no-image.png';
//                                                    } else {
//                                                        $img = $car_exterior_data['accessory_img'];
//                                                    }
//                                                    $img = base_url() . 'admin/uploads/accessory/' . $img;
                                                    $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                                    if(file_exists($file)){
//                                                        $img;
//                                                    }else{
//                                                        $img='uploads/user/no-image.png';
//                                                    }

                                                    $img = base_url().$img;
                                                    
                                                    
                                                    
                                                    ?>
                                                    <tr>
                                                        <td width="50%">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>"> 
                                                                    <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                        <?php echo $car_exterior_data['accessory_name']; ?>
                                                                    </div>                                                                            
                                                                </label>
                                                            </div>
                                                            <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                            <!--                                                    <div class="category-details">
                                                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                            
                                                                                                                </div>-->
                                                        </td>
                                                        <td width="10%">
                                                            <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                        </td>
                                                        <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                            <?php
                                                            echo $car_exterior_data['price'];
                                                            $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                            ?>   

                                                        </td>
                                                    </tr>                                            
                                                    <?php
                                                }
                                            }
                                            ?> 

                                        </table>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
<!--                <div class="tab-pane" id="warranty">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped car-accessory-table">
                            <thead>
                                <tr>
                                    <th width="50%">Description</th>
                                    <th width="10%">Image</th>
                                    <th width="10%">MRP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($car_services)) {
                                    foreach ($car_services as $car_exterior_data) {
                                        if ($car_exterior_data['accessory_sub_cat'] == 10) {
                                            if (in_array($car_exterior_data["accessory_id"], $exp_acc)) {
                                                $class = 'checked';
                                            } else {
                                                $class = '';
                                            }
                                                    $img='admin/uploads/accessory/'.$car_exterior_data['accessory_img'];
//                                                    loadvariant
                                                    $img = base_url().$img;
                                            
//                                            if($car_exterior_data['accessory_img']=='') {
//                                                $img = 'no-image.png';
//                                            } else {
//                                                $img = $car_exterior_data['accessory_img'];
//                                            }
//                                            $img = base_url() . 'admin/uploads/accessory/' . $img;
                                            ?>
                                            <tr>
                                                <td width="50%">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input id="chk<?php echo $car_exterior_data['accessory_id']; ?>" onclick="set_total_price(<?php echo $car_exterior_data['accessory_id']; ?>)" type="checkbox" <?php echo $class; ?> class="high1" data-name="<?php echo $car_exterior_data["accessory_id"] ?>"  value="<?php echo $car_exterior_data['id'] ?>">
                                                            <div id="<?php echo 'results' . $car_exterior_data["accessory_id"]; ?>">
                                                                <?php echo $car_exterior_data['accessory_name']; ?>
                                                            </div> 


                                                        </label>
                                                    </div>
                                                    <p><?php echo $car_exterior_data['accessory_information']; ?> [Part No.] <span class="info-span text-maroon"><i class="fa fa-info-circle"></i></span></p>
                                                                                                        <div class="category-details">
                                                                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                    
                                                                                                        </div>
                                                </td>
                                                <td width="10%">
                                                    <img class="img-center img-thumbnail" style="max-width: 100px;" src="<?php echo $img; ?>">
                                                </td>
                                                <td width="10%" id="price<?php echo $car_exterior_data['accessory_id']; ?>">
                                                    <?php
                                                    echo $car_exterior_data['price'];
                                                    $grand_total_amt = $grand_total_amt + $car_exterior_data['price'];
                                                    ?>   

                                                </td>
                                            </tr>                                            
                                            <?php
                                        }
                                    }
                                } else {
                                    ?>    
                                    <tr>
                                        <td colspan="3">  
                                            <div align="center" style="width:200px;"><label >Not Available</label></div>    
                                        </td>    
                                    </tr> 
                                    <?php
                                }
                                ?> 
                            </tbody>

                        </table>

                    </div>
                </div>-->
            </div>
            
            <table class="table table-hover table-bordered table-striped car-accessory-table">
                <thead>
                    <tr>
                        <td colspan="4" class="text-center"><strong>Total</strong></td>
                        <td class="text-center"><strong><i class="fa fa-rupee"></i>
                                <span id="g_total"><?php
                                    if (!empty($accessory_total)) {
                                        echo $accessory_total;
                                    } else {
                                        echo '0';
                                    }
                                    ?></span>

                            </strong></td>
                    </tr>
                </thead>   
            </table>
        </div>


        <input type="hidden" id="headlights" value="<?php echo $accessory_cook; ?>"/>  

        <div class="row">
            <div class="col-md-12">
                <div class="clearfix">
                    <div class="pull-right" style="padding-right: 15px;">
                        <form action="<?php echo base_url('accessory-services/' . $main_id) ?>" method="POST" style="display:none;">
                            <input type="hidden" id="pro_id" name="pro_id" value="<?php echo $main_id; ?>"/>
                            <input type="hidden" id="headlights" name="headlights" value=""/>
                            <input type="hidden" id="selected_ids" name="selected_ids" value="<?php echo $accessory_cook; ?>"/>
                            <input type="hidden" id="info_details" name="info_details"/>
                            <input type="hidden" value="" id="g_totals" name="g_totals"/>
                            <button type="submit" class="btn btn-maroon confuration" onclick="saveconfiure();">Save and Continue</button>
                            <!--<button type="submit" class="btn btn-maroon confuration" onclick="">Save and Continue</button>-->
                            <button type="submit" id="select_data" name="submit" class="btn btn-maroon" style="display:none;">Save and Continue</button>
                        </form>                        
                    </div>
                    <script>
                        function saveconfiure()
                        {
                            var selected_ids = $('#selected_ids').val();
                            if (selected_ids == '')
                            {
                                return;
//                                exit;
                            }
                            else
                            {
                                var first = "<div align='center'><img src='<?php echo base_url() . 'assets/img/loader.gif'; ?>'/></div>";
                                $('#accessory_mess').html(first);
                                document.getElementById('error_model_link').click();
                                var selected_ids = $('#selected_ids').val();
                                var info_details = $('#info_details').val();
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo site_url('stitcher/check_acc_confiuration') ?>",
                                    data: {selected_ids: selected_ids, info_details: info_details}
                                }).done(function(html) {
                                    $('#accessory_mess').html(html);
                                    var count = $('#count').val();
                                    if (count == 0)
                                    {
                                        document.getElementById('select_data').click();
                                    }
                                });
                                return false;
                            }
//                            return;
                        }
                    </script> 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
     <div class="tab-pane" id="equipment">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped car-accessory-table">
                            <thead>
                                <tr>
                                    <th width="100%">Accessories</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td>   
<div class="container" align="center">
           <div class="table-responsive img-responsive img-thumbnail img-center" style="padding: 20px;">
                        <img style="height:100px;" src="<?php echo base_url();?>/assets/img/ComingSoon.png">
                    </div>
    
</div>
                                    </td>                    
                        </tbody>
                        </table>
                    </div>
</div>   
    </div>   
</div> <!-- /.container  -->


<script>
    $(document).ready(function() {
        $(".high1").click(function() {
            var feature_name = "";
            var final_details2 = "";
            $("input.high1:checked").each(function() {
                //alert('sdsdsd');
                var id = $(this).data('name');
                feature_name += $(this).data('name') + ', ';
                var details = document.getElementById('results' + id).innerHTML;
                var details2 = details.trim();
                var final_details = details2 + ',-,' + id;
                final_details2 += final_details + '---';
            });
            document.getElementById('selected_ids').value = feature_name;
            document.getElementById('info_details').value = final_details2;
        });
    });
    function allexterior()
    {
        var headlights = $('#headlights').val();
        document.cookie = "accessories=" + headlights;
    }
    function set_total_price(id)
    {
        var g_total_price = document.getElementById("g_total").innerHTML;
        var g_total_price = g_total_price.replace(/\s/g, "");
        var g_total = document.getElementById("price" + id).innerHTML;
        var g_total_data = g_total.replace(/\s/g, "");
        if (document.getElementById('chk' + id).checked)
        {
            var final = parseInt(g_total_price) + parseInt(g_total_data);
            $('#g_totals').val(final);
            document.getElementById("g_total").innerHTML = final;
            //document.getElementById("g_totals").valueL=final;
        } else {
            var final = parseInt(g_total_price) - parseInt(g_total_data);
            $('#g_totals').val(final);
            document.getElementById("g_total").innerHTML = final;
            //document.getElementById("g_totals").valueL=final;
        }
    }

</script>
<!-- Login Modal-->
<a data-target="#error_model" data-toggle="modal" id="error_model_link" style="display:none;" href="#">Error</a>
<div class="modal fade" id="error_model">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Accessory Configuration Process</h4>
            </div>
            <div class="modal-body" id="accessory_mess">                
            </div>
        </div> 
    </div>
</div>
