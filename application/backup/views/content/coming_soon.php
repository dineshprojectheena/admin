<div class="container" id="upcoming_cars">
    
    <div class="row"> 
        <div class="col-md-12">
            <h1>Upcoming Cars in India</h1>    
        </div>
        <table class="table table-striped table-condensed comparison-table">       
            <tbody>
                <tr class="car-name">
                    <td>    
                        <div class="col-md-12 col-cust-both" class="car-brand"  >    
                            <div class="col-md-4">
                                <?php
                                $exp_prices = explode('-', $_GET['estimated_price']);
//                                print_r($exp_prices);
//                                print_r($get_estimated_price);
                                ?>
                                <select  onchange="check_coming_soon();" id="brand_id" name="brand_id" class="form-control">
                                    <option value="">All Brands</option>
                                    <?php
                                    foreach ($all_brands as $all_brands_data) {
                                        ?>
                                        <option <?php
                                    if ($_GET['brand_id'] == $all_brands_data['brand_id']) {
                                        echo 'selected';
                                    }
                                        ?> value="<?php echo $all_brands_data['brand_id']; ?>"><?php echo $all_brands_data['brand_name'] ?></option>                    
                                            <?php
                                        }
                                        ?>
                                </select> 
                                <br>
                            </div>

                            <div class="col-md-2"> 
                                
                                <select id="estimated_price" onchange="price_checking2();" name="estimated_price" class="form-control">
                                    <option value="0">Min Value</option>
                                    <?php
                                    foreach ($get_estimated_price as $get_estimated_price_data) {
                                        ?>
                                        <option <?php
                                        if(str_replace('_', '', $exp_prices[0]) == $get_estimated_price_data[0]) {
                                            echo 'selected';
                                        }
                                        ?> value="<?php echo preg_replace('/\s+/', '_', ($get_estimated_price_data[0])); ?>"><?php echo $get_estimated_price_data[0].' Lakhs'?></option>                    
                                            <?php
                                        }
                                        ?>
                                </select>
                                <br>
                            </div>
                            <div class="col-md-2 ">  
                                <select id="estimated_max_price" onchange="price_checking2();"  name="estimated_max_price" class="form-control">
                                    <option value="0">Max Value</option>
                                    <?php
                                    foreach ($get_estimated_max_price as $get_estimated_max_price_data) {
                                        ?>
                                        <option <?php
                                        if(str_replace('_', ' ', $exp_prices[1]) == $get_estimated_max_price_data[0]) {
                                            echo 'selected';
                                        }
                                        ?> value="<?php echo preg_replace('/\s+/', '_', ($get_estimated_max_price_data[0])); ?>"><?php echo $get_estimated_max_price_data[0].' Lakhs'; ?></option>                    
                                            <?php
                                        }
                                        ?>
                                </select>  
                                <br>
                            </div>

                            <div class="col-md-4"> 
                                <select onchange="check_coming_soon();" id="launching_date" name="launching_date" class="form-control">
                                    <option value="">Expected Launch</option>   
                                    <?php
                                    foreach ($get_estimated_lauch_date as $get_estimated_lauch_date_data) {
                                        ?>
                                        <option <?php
                                    if ($_GET['launching_date'] == preg_replace('/\s+/', '_', ($get_estimated_lauch_date_data['launching_date']))) {
                                        echo 'selected';
                                    }
                                        ?> value="<?php echo preg_replace('/\s+/', '_', ($get_estimated_lauch_date_data['launching_date'])); ?>"><?php echo $get_estimated_lauch_date_data['launching_date'] ?></option>                    
                                            <?php
                                        }
                                        ?>
                                </select> 
                                <br>
                            </div>


                        </div>
                    </td>    
                </tr>    
            </tbody>    
        </table>    
        <div class="col-md-12 col-cust-both  col-sx-12 col-sm-12" >    
            <?php
            if (count($all_coming_soon) > 0) {
                foreach ($all_coming_soon as $all_coming_soon_res) {
                    ?>
                    <!--<div class="item active">-->
                    <div class="col-md-4  col-sx-6 col-sm-6">
                        <div class="ribbin">	
                            <div class="corner">
                                <h2><?php echo 'Coming Soon'; ?></h2>
                                <div class="group-item">
                                    <div class="col-md-12">
                                        <div class="img-holder col-md-12">
                                            <?php
                                            $image_path_medium =img_url('uploads/product/');
                                            $image_not_found_medium = $image_path_medium . "/" . "not-found.jpg";
                                            $image_name_with_path = $image_path_medium . "/" . $all_coming_soon_res['pro_image']; //this is your image url
                                            $image_file_path = img_url('uploads/product/'.$all_coming_soon_res['pro_image']); //this is your file path

                                            if (file_exists($image_file_path)) {
                                                $image = $image_name_with_path;
                                            } else {
                                                $image = $image_not_found_medium;
                                            }
                                            ?>


                                            <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $all_coming_soon_res['pro_name_comp']) . '-' . $all_coming_soon_res['variant_id']) ?>" >        
                                                <img class="img-responsive upcoming_image" src="<?php echo $image; ?>" />
                                            </a>
                                        </div>
                                    </div>    
                                    <div class="group-content">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4 class="hot_dealreview_title"> <?php echo $all_coming_soon_res['pro_name_comp'] ?> </h4>
                                                </div> 
                                                <div class="col-md-12">
                                                <div class="col-md-9 col-cust-both">
                                                    <div class="group-price">
                                                        <div class="mnc-price">
                                                            <span class="price-span">
                                                                Expected Price:
                                                            </span>
                                                            <span class="price-span text-maroon">
                                                                <i class="fa fa-rupee"></i>
                                                                <?php echo $all_coming_soon_res['extimated_price'].' Lakhs' . '-' . $all_coming_soon_res['extimated_price_max'].' Lakhs' ?> 
                                                            </span>
                                                        </div>     

                                                    </div>
                                                    <div class="group-price">
                                                            <div class="mnc-price">
                                                                <span class="price-span">
                                                                    Expected Launch:
                                                                </span>
                                                                <span class="price-span text-maroon">

                                                                    <?php echo $all_coming_soon_res['launching_date'] ?> 
                                                                </span>
                                                            </div>     
                                                        </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="pull-left" style="padding-top: 8px;">
                                                            <a data-toggle="modal" data-target="#coming_enqury" onclick="$('#enq_com_product').val('<?php echo $all_coming_soon_res['pro_name_comp']; ?>'), $('#pro_link').val('<?php echo site_url('car/' . preg_replace('/\s+/', '-', $all_coming_soon_res['pro_name_comp']) . '-' . $all_coming_soon_res['variant_id']) ?>')" class="btn btn-xs carsuel_book_upcoming">Alert Me</a>                                                
                                                        </div>
                                                </div>    
                                                </div>    
<!--                                                <div class="col-md-12">
                                                    <div class="col-md-9 col-cust-left">
                                                        
                                                    </div>
                                                    
                                                </div>-->
                                            </div>      
                                        </div>
                                    </div>
                                </div>
                            </div>      
                        </div>      

                    </div>                    
                    <!--</div>-->

                    <?php
                    $ii++;
                }
            } else {
                ?>
                <div class="col-md-12 col-cust-both" align="center">         
                    <div align="center" class="no_carousel col-xs-12">
                        <img src="<?php echo base_url();?>/assets/img/ComingSoon.png">
                    </div>   
                </div>
                <?php
            }
            ?>
        </div>
    </div>




</div>

<?php $this->load->view('external_js/coming_soon_js'); ?>

</div> <!-- /.container -->
