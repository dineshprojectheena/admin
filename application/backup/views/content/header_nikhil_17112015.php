<!DOCTYPE html>
<?php
if($_GET['city']!='') {
    setcookie('main_city', $_GET['city'], time() + (86400 * 30), "/");
}
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php
if (!empty($seo)) {
    ?>
        <meta name="description" content="<?php echo $seo[0]['seo_description'] ?>" />
        <meta name="keywords" content="<?php echo $seo[0]['seo_keyword'] ?>" />
        <title><?php echo $seo[0]['seo_title'] ?></title>
    <?php
} else {
    ?>
        <title>My New Car</title>
        <?php
    }
    ?>

    <link href='//fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url("assets/css/font-awesome.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/font-awesome.min.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/thirdparty/rs-plugin/css/settings.css") ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap.min.css") ?>" rel="stylesheet">

    <link href="<?php echo base_url('assets/thirdparty/flexSlider/flexslider.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/thirdparty/dataTable/css/jquery.dataTables.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/thirdparty/dataTable/css/jquery.dataTables_themeroller.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/thirdparty/prettyphoto/css/prettyPhoto.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url("assets/thirdparty/nouislider/nouislider.min.css") ?>" rel="stylesheet">

    <link href="<?php echo base_url("assets/thirdparty/btselect/bootstrap-select.min.css") ?>" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="//silviomoreto.github.io/bootstrap-select/stylesheets/scrollYou.css">-->
    <link href="<?php echo base_url('assets/thirdparty/jQuery.toolbar/jquery.toolbars.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/presentation.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/jquery-ui.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/flexslider.css'); ?>" rel="stylesheet">
    <!-- Custom Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/home/32X32.png" />

    <script src="<?php echo base_url() . "assets/js/jquery.js" ?>"></script>
    <!--<meta http-equiv="refresh" content="600" URL="<?php echo base_url(); ?>">-->
<?php
$user_login_sucess = $this->session->flashdata('user_login_sucess');
$account_registered = $this->session->flashdata('account_registered');
$sucess = $this->session->flashdata('sucess_msg');

$newsletter_flag = FALSE;
if ($this->session->flashdata('newsletter_msg')) {
    $newsletter_flag = TRUE;
    $newsLetterMsg = $this->session->flashdata('newsletter_msg');
    $newsLetterTitle = 'News Letter Subscribtion';
}

//$sucess=$this->session->flashdata('req_uri');   
$facebook = $this->session->flashdata('facebook_error');
$enquiry_alert_msg = $this->session->flashdata('enquiry_alert_msg');
$login_failed = $this->session->flashdata('login_failed');
$user_rating_sucess = $this->session->flashdata('user_rating_sucess');
$main_city = $this->input->cookie('main_city', TRUE);
$main_city_cond = $main_city;
$userdata = $this->session->userdata('logged_in_user');
if (empty($userdata['user_id'])) {
    $this->session->set_userdata('logged_in', FALSE);
    $this->session->unset_userdata('logged_in_user');
    $this->session->sess_destroy();
}
?>

    <script>
//        alert('<?php echo $enquiry_alert_msg; ?>');
        $(window).load(function() {

            var user_login_sucess = "<?php echo $user_login_sucess; ?>";
            var facebook_error = "<?php echo $facebook; ?>";
            var user_rating_sucess = "<?php echo $user_rating_sucess; ?>";
            var account_registered = "<?php echo $account_registered; ?>";
            var login_failed = "<?php echo $login_failed; ?>";
            var enquiry_alert_msg = "<?php echo $enquiry_alert_msg; ?>";
//            alert(login_failed);

<?php if ($newsletter_flag):
    ?>{
                    $('#login_errors').modal('show');
                    $('#mess_heading').html("Newsletter Subscription");
                    $('#error_message').html("<?php echo $newsLetterMsg ?>");
                    //delete <?php echo $newsLetterMsg ?>;
                }
<?php endif; ?>


            if (enquiry_alert_msg != '')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("Email sent successfully. You will hear from us soon!");
                $('#mess_heading').html("Enquiry Send Successully!");
                setTimeout(function() {
                    window.location.href = "<?php echo base_url(); ?>";
                }, 1500);
            }

            if (login_failed == 'login_failed')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("Please check username and password careully!");
                $('#mess_heading').html("Login Failed!");

            }


            if (facebook_error == 'Facebook')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("Sorry, We were not able to receive the appropriate details to create a user account. Please use a different method to login!");
            }
            if (user_rating_sucess == 'user_rating_sucess')
            {

                $('#login_errors').modal('show');
                $('#error_message').html("Thank you for providing valuable rating!");
                $('#mess_heading').html("User Rating");
            }
            if (user_login_sucess == 'login_sucess')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("Please proceed to Select, Configure and Book your car online.");
                $('#mess_heading').html("Login Successful!");
            }
            if (account_registered == 'login_sucess')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("Account registered successfully, please check your email to activate account!");
                $('#mess_heading').html("Login Sucessfull");
            }
            if (account_registered == 'custom_account')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("Account registered successfully, please check your email to activate account!");
                $('#mess_heading').html("Account Registered");
            }
            if (account_registered == 'account_activated_successfully')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("You Account Acivated Successfully!");
                $('#mess_heading').html("Account Activated...");
            }
            if (account_registered == 'password_reset_successfully')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("please check your mail to reset your password!");
                $('#mess_heading').html("Check your email...");
            }
            if (account_registered == 'password_changed_successfully')
            {
                $('#login_errors').modal('show');
                $('#error_message').html("Password Changed Successfully !");
                $('#mess_heading').html("Password Changed...");
            }
        });


    </script>
    <style>
        #inputSearch
        {
            padding:3px;
        }
        #divResult
        {
            position:absolute;
            width:215px;
            display:none;
            margin-top:-1px;
            border:solid 1px #dedede;
            border-top:0px;
            overflow:hidden;
            border-bottom-right-radius: 6px;
            border-bottom-left-radius: 6px;
            -moz-border-bottom-right-radius: 6px;
            -moz-border-bottom-left-radius: 6px;
            box-shadow: 0px 0px 5px #999;
            border-width: 3px 1px 1px;
            border-style: solid;
            border-color: #333 #DEDEDE #DEDEDE;
            background-color: white;
            z-index: 9999;
        }
        .display_box
        {
            padding:4px; border-top:solid 1px #dedede; 
            font-size:12px; height:50px;
        }
        .display_box:hover
        {
            background:#3bb998;
            color:#FFFFFF;
            cursor:pointer;
        }    
    </style>
    <style>
        .dataTables_filter
        {
            display:none;    
        }
        #example_previous
        {
            padding:5px;
            border: 1px solid #DDDDDD;
            margin-right:5px; 
        }
        #example_next
        {
            padding:5px;
            border: 1px solid #DDDDDD;
            margin-left:5px;
        }
        #example2_previous
        {
            padding:5px;
            border: 1px solid #DDDDDD;
            margin-right:5px; 
        }
        #example2_next
        {
            padding:5px;
            border: 1px solid #DDDDDD;
            margin-left:5px;
        }
        .dataTables_info
        {
            display:none;    
        }
        .example_length
        {
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;    
        }
        .loginOptions
        {
            background-color: #EFEFEF;    
        }
        .top_search
        {
            background-color: white;    
        }

    </style>

</head>
<!--<div class="header_strip sticky-row">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <h4 class="page-heading"><i class="fa fa-bullhorn" style="font-size:13px; "></i> 24X7 @ 123123</h4>
                    <ul class="top-menu" >
                        <li><a style="color:white;" href="mailto:contact@mynewcar.in">contact@mynewcar.in</a></li>
<?php if ($userdata['logged_in']): ?>
                                    <li <?php echo $d; ?>><a href="<?= site_url('user') ?>" style="color:white;">Profile</a></li>
                                <li><a href="<?= site_url('authorization/logout') ?>" style="color:white;">Logout</a></li>
<?php else: ?>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#loginModal" data-toggle="dropdown" style="color:white;">Sign in
                                    </a>
                                </li>
<?php endif; ?>                        
                        <li><a style="color:white;" href="<?php echo base_url() . 'contact-us'; ?>">Help</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <h4 class="page-heading" style="color:white;font-size:13px;float:left; "><i class="fa fa-bullhorn" style="font-size:13px; "></i> 24X7 @ 123123</h4>
        <h4 class="page-heading" style="color:white;font-size:13px;float:right; "> Email us:- contact@mnc.in</h4>
    </div>    
</div>-->
<a style="display:none;" data-target="#login_errors" data-toggle="modal" id="login_errors_button" href="#">Error_login</a>

<div class="modal fade" id="login_errors" style="width:400px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="mess_heading">Login Errors!</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row" id="error_message">
                        Facebook Errors  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="expert_review_selection">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="mess_heading">Expert Review Search</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4" id="exp_brand_res">

                                </div>
                                <div class="col-md-4" id="exp_model_res">

                                </div>
                                <div class="col-md-4" id="exp_var_res">

                                </div>

                            </div>
                            <script>
                                function search_exp_review()
                                {
                                    var variant_name = $("#variant_data option:selected").text();
                                    var variant_id = $("#variant_data option:selected").val();
//                                alert(variant_name);
//                                alert(variant_id);
                                    var variant_name = variant_name.trim().replace(/[^a-z0-9]+/gi, '-');
//                                alert(variant_name)
                                    if (variant_name != '')
                                    {
                                        var final = variant_name + '-' + variant_id;
                                        window.location.href = "<?php echo base_url() . 'car/' ?>" + final + '/expert_review';
                                    }
                                }

                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
//alert('<?php echo $account_registered; ?>');    
</script>
<?php
//echo $account_registered;
?>
