<div class="container" id="about_us">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <p align="justify" style="margin-top: 0.11cm; margin-bottom: 0.21cm; line-height: 150%">
            <h1>
                Welcome to MYNEWCAR.IN. Online New Car Seller
            </h1>            
            </p>
            <p align="justify" >
                Car buying is an emotional journey that should be accompanied by joy and happiness. MYNEWCAR.IN offers its customers new cars, accessories and related services combined with an easy and convenient purchasing experience. Our mission is to simplify the car buying process to a joyful experience. Customers can compare, select and configure their car anytime and anywhere. MYNEWCAR.IN provides unbiased information at every step and assists you in getting the best deals. Your new car will be delivered hassle-free to your doorstep in partnership with the authorized dealer.
            </p>
            

            <h1>
                Our Promise
            </h1>            
            </p>
            <p>
                At <b>MYNEWCAR.IN,</b> we have a vision: <font color="#c00000"><b>Kharidiye Easily! Bachaiye Simply!</b></font>
            </p>
            <p >
                MYNEWCAR.IN strives to
                    achieve the highest level of your joy and satisfaction possible when
                    purchasing your new car. Our online platform enables you to select,
                    configure and purchase a desired car, accessories.</p>
            <ul>
                <ul class="leaf">
                    <li><p align="justify" >
                            Efficiently with a one-stop-solution
                        </p>
                    </li>
                    <li><p align="justify">
                            Conveniently by purchasing online
                        </p>
                    </li><li><p align="justify">
                            Securely with refundable booking amount while
                        </p>
                    </li><li><p align="justify">
                            Saving Money with online discounts and group deals</p>
                </li></ul>
            </ul>
            
            


            <div class="col-md-12 col-xs-12" style="float:left;"><h1>Meet the Team</h1>
                <p>MYNEWCAR.IN's team consist of dedicated professionals with more than 100 years of experience in Automotive and Technology sector along with top level advisors having Automotive, Media, E-commerce and Technology background.</p>
                

            </div>

            <div>

                <div class="col-md-12 col-cust-both">
                    <div class="col-md-4">                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"   src="<?php echo base_url() . 'uploads/team/Madhukar Manpuria.jpg' ?>"    itemprop="image">
                            </div>
                            <div class="col-md-12 col-cust-both col-xs-12">
                                <div class="col-md-12  col-cust-left col-xs-12" align="left">                                    
                                    <h3 class="team-member-name col-xs-12">
                                    <span class="pull-left">Madhukar Manpuria</span>    
                                    <a href="https://de.linkedin.com/in/madhukarmanpuria"  target="_blank" class="linkdin ln tiphere pull-left">
                                        <img class="img-responsive linkdin"  src="<?php echo base_url() . 'uploads/link.png' ?>"/>
                                    </a>
                                    </h3>
                                    
                                    <div class="team-member-job-title col-xs-12">Founder</div>                                    
                                </div>    
                            </div>    
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">He is the driving force behind MYNEWCAR.IN. His mission is to make car buying a joyful and hassle-free experience.
                            </div>                        
                        
                    </div>


                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                            <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/Arvind Sinha.jpg' ?>"    itemprop="image"></div>
                            
                            <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12" >
                                        <span class="pull-left" >Arvind Sinha</span>
                                        <a href="https://in.linkedin.com/pub/arvind-sinha/8/b73/b31"   target="_blank" class="linkdin ln tiphere pull-left">
                                        <img class="img-responsive linkdin"  src="<?php echo base_url() . 'uploads/link.png' ?>"/>
                                    </a>                                        
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Managing Director</div>                                    
                                </div> 
                               
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">He is the personification of the Indian automotive industry with over 27 years of multi-functional senior management expertise at leading car manufacturers.
                            </div>                        
                        
                    </div>


                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/Snigdha Bhuia.jpg' ?>"    itemprop="image"></div>
                            
                            <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12" >
                                        <span class="pull-left" >Snigdha Bhuia</span>
                                        <a href="https://de.linkedin.com/pub/snigdha-bhuia/20/b83/a1"   target="_blank" class="linkdin ln tiphere pull-left">
                                        <img class="img-responsive linkdin"  src="<?php echo base_url() . 'uploads/link.png' ?>"/>
                                    </a>                                        
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Operations Manager</div>                                    
                                </div>     
                        
                        
                              
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">She is the soul of MYNEWCAR.IN's operations and ensures a smooth customer care experience for our customers.
                            </div>                        
                        
                    </div>
                </div>

                <div class="col-md-12 col-cust-both">
                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/Surendhar Raj.jpg' ?>"    itemprop="image"></div>
                            
                            <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12" >
                                        <span class="pull-left" >Surendhar Raj</span>
                                       <a href="https://in.linkedin.com/pub/surendhar-raj/27/661/701"   target="_blank" class="linkdin ln tiphere pull-left">
                                        <img class="img-responsive linkdin"  src="<?php echo base_url() . 'uploads/link.png' ?>"/>
                                    </a>                                        
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Sales Head</div>                                    
                                </div>     
                        
                            
                              
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">With over 15 years of experience in sales in the Indian automotive industry, he ensures a close relationship with our channel partners.
                            </div>                        
                        
                    </div>


                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/ankit momaya.jpg' ?>"    itemprop="image"></div>
                            
                            <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12" >
                                        <span class="pull-left" >Ankit Momaya</span>
                                       <a href="https://in.linkedin.com/in/ankitdmomaya"  target="_blank" class="linkdin ln tiphere pull-left">
                                        <img class="img-responsive linkdin"  src="<?php echo base_url() . 'uploads/link.png' ?>"/>
                                    </a>                                        
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Marketing Manager</div>                                    
                                </div>     
                        
                            
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">The "chief in place" of our digital presence. A car enthusiast by heart, who brings our message across people and is the central element of our online community.
                            </div>                        
                        
                    </div>


                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/Chetan Mota.jpg' ?>"    itemprop="image"></div>
                            <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12" >
                                        <span class="pull-left" >Chetan Mota</span>
                                       <a href="https://in.linkedin.com/in/chetanpmota"  target="_blank" class="linkdin ln tiphere pull-left">
                                        <img class="img-responsive linkdin"  src="<?php echo base_url() . 'uploads/link.png' ?>"/>
                                    </a>                                        
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Product Manager</div>                                    
                                </div>     
                          
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">With over 15 years experience at the interface between Marketing & IT, he keeps the back-end operations running smoothly.
                            </div>                        
                        
                    </div>

                </div>    

                <div class="col-md-12 col-cust-both">

                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/Konica Dutta.jpg' ?>"    itemprop="image"></div>
                            <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12" >
                                        <span class="pull-left" >Konica Lishoy</span>                                                                               
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Creative Designer</div>                                    
                                </div> 
                                
                            
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">If you have to choose one word to describe her, it is Creativity. She comes up with our wonderful MYNEWCAR.IN designs.
                            </div>                        
                        
                    </div>


                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/Mukund Vasudev.JPG' ?>"    itemprop="image"></div>
                            
                            <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12" >
                                        <span class="pull-left" >Mukund Vasudev</span>                                                                               
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Customer Care Manager</div>                                    
                                </div>    
                        
                             
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">He is the Automotive Guru. He leaves no stones unturned to ensure that our Customers have a great experience with the help of his expert team.
                            </div>                        
                        
                    </div>


                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/Chintan Kansara.jpg' ?>"    itemprop="image"></div>
                            
                            
                            <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12" >
                                        <span class="pull-left" >Chintan Kansara</span>                                                                               
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Customer Care Advisor</div>                                    
                                </div>    
                        
                              
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">His product knowledge and eagerness to share the knowledge with the team, makes him an asset of MYNEWCAR.IN
                            </div>                        
                        
                    </div>

                </div>    

                <div class="col-md-12 col-cust-both">
                    <div class="col-md-4">
                        
                            <div class="col-md-12 col-xs-12" align="left">
                                <img class="img-responsive about_img"  src="<?php echo base_url() . 'uploads/team/Shaubik Bhuia.jpg' ?>"    itemprop="image"></div>
                             <div class="col-md-12  col-cust-left col-xs-12" align="left">
                                    <h3 class="team-member-name col-xs-12">
                                        <span class="pull-left" >Shaubik Bhuia</span>                                                                               
                                    </h3>
                                    <div class="team-member-job-title col-xs-12">Customer Care Advisor</div>                                    
                                </div>    
                            
                              
                            <div class="team-member-description col-md-12 col-xs-12" itemprop="description">A combination of Calm, Enthusiasm, Determination and Patience. His motivation is contagious.
                            </div>                        
                        
                    </div>
                </div>



            </div>
            
            
            <p align="justify" class="lease_after col-md-12 col-xs-12">
                
                Let us stay in contact
                    and subscribe to our newsletter. You will be personally informed
                    about upcoming events and great new car promotions. We
                    would be more than happy to see you again on our site.
            </p>
        </div>
    </div>
</div>