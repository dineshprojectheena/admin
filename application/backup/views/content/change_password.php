<?php
foreach($user_details as $user_details_data)
?>
<div class="container">
    <div class="row">
    <div class="col-md-12">
        <div class="col-sm-4 col-cust-left">
            <div>
                <h3 style="margin-top:0px; padding-top: 0px;">Change Password</h3>                
            </div>
        </div>
        
        
        <div class="col-md-8">
            <form id="contact-us" autocomplete="off" style="height:240px;" class="form-horizontal" role="form" action="<?php echo site_url("authorization/update_password"); ?>" method="POST">
                <?php if (isset($alert_msg)) { ?>
                    <div role="alert" class="alert alert-success">
                        <div class="twelve columns mobile-four alert-box secondary">
                            <p style="color:red;font-weight: bold"><?= $alert_msg ?></p>
                            <a href="" class="close">&times;</a>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label for="new_password" class="col-sm-3 control-label">New Password</label>
                    <div class="col-sm-9">
                        <input type="hidden" name="user_id" class="form-control" id="user_id" value="<?php echo $user_id;?>" required>
                        <input type="password" name="new_password" class="form-control" id="new_password" value="" required>
                         <span class="error"><p id="new_pss"></p></span> 
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="re_new_password" class="col-sm-3 control-label">Re-Enter Password</label>
                    <div class="col-sm-9">
                        <input type="password" name="re_new_password" class="form-control" id="re_new_password" value="" required>
                         <span class="error"><p id="re_new_pss"></p></span> 
                    </div>
                </div>
                
 
                <div class="col-sm-12 text-right">
                    <span class="error"><p id="change_pass"></p></span> 
                    <button type="button" onclick="change_pass();" style="float:right;" id="update_button" class="btn btn-maroon">Change Password</button>
                    <button type="submit" style="display: none;float:right;" id="change_password" class="btn btn-maroon">Change Password</button>
                </div>
                <script>
                function change_pass()
                {
                $("#change_pass").html('');    
                $("#new_pss").html('');
                $("#re_new_pss").html('');
                var new_password=$('#new_password').val();    
                var re_new_password=$('#re_new_password').val();    
                if(new_password=='')
                {
                $("#new_pss").html('Please Insert New Password');        
                return false;
                }
                else if(re_new_password=='')
                {
                $("#re_new_pss").html('Please Insert New repeat password');        
                return false;
                }
                else if(new_password!=re_new_password)
                {
                 $("#change_pass").html('New & repeat password should be same....');    
                 return false;
                }    
                else
                {
                $("#change_password").click();    
                }
                }
                </script>

            </form>
        </div>
        
    </div>
    </div>
</div>