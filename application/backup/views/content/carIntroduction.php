<style>
    .introduction-img-box{
        margin: 14px auto;
    }

    .introduction-img-box img{
        max-height: 160px;
        border: 1px solid;
    }
</style>
<?php $this->load->view('templates/responsiveSlider'); ?>
<input type="hidden" value="<?php if(!isset($id)){ echo $id; }?>" id="pro_id" name="pro_id"/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="review-box" style="border:1px solid #DDDDDD;">
                            <h5 class="sidebar-title">
                                <i class="fa fa-bullhorn"></i>
                                Highlights
                            </h5>
                            <div class="review-content" style="height:auto;min-height: 290px;">
                                <p>
                                    <?php                                    
                                    print_r($car_into_details[0]['pro_detail']);
                                    ?>
                                </p>                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="review-box expert-review-box" style="border:1px solid #DDDDDD;">
                            <h5 class="sidebar-title">
                                <i class="fa fa-tag"></i>
                                Reviews
                            </h5>
                            <div class="review-content">
                                <?php
                                if(!empty($expert_reviews))
                                {
                                ?>                                
                                <strong>Expert Review</strong>
                                <p class="quote">
                                    <i class="fa fa-quote-left fa-2x pull-left"></i>
                                                                      
                                    <?php print_r($expert_reviews[0]['what_i_like']);?>
                                </p>
                                <p class="star-rating-p">
                                    <span class="text-maroon">
                                        <?php
                                        $star = $expert_reviews[0]['total'];
                                        if (!empty($star)){
                                            $exp = explode('.', $star);
                                            $point = $exp[1];
                                            if ($point > 00) {
                                                $half = '-half-full';
                                            } else if ($point == 00) {
                                                $half = '-o';
                                            } else {
                                                $half = '';
                                            }

                                            if ($exp[0] == 1) {
                                                ?>
                                                <i class="fa fa-star"></i><?php ?>
                                                <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                <?php
                                            } else if ($exp[0] == 2) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                            } else if ($exp[0] == 3) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                            }
                                            else if ($exp[0] == 4) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                            }
                                            else if ($star == 5) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                            <?php
                                            }
                                        } else {
                                            ?>
                                            <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        }
                                        ?>                                     
                                    </span>
                                    - <?php print_r($expert_reviews[0]['journilist_name']);?>
                                    
                                </p>
                                <?php
                                }
                                else{
                                ?>
                                <div align="left" style="margin-top:20px;">
                                <strong >No Expert Review Found</strong>
                                </div>
                                <?php
                                    
                                }
                                
                                if(!empty($user_reviews))
                                {
                                ?>
                                <strong>User Review</strong>
                                <p class="quote">
                                    <i class="fa fa-quote-left fa-2x pull-left"></i>
                                    <?php 
                                    print_r($user_reviews[0]['what_i_like']);
                                    ?>
                                    
                                </p>
                                <p class="star-rating-p">
                                    <span class="text-golden">
                                        <?php
                                        $star = $user_reviews[0]['total'];
                                        if (!empty($star)) {
                                            $exp = explode('.', $star);
                                            $point = $exp[1];
                                            if ($point > 00) {
                                                $half = '-half-full';
                                            } else if ($point == 00) {
                                                $half = '-o';
                                            } else {
                                                $half = '';
                                            }
					    if ($exp[0] == 0) {
                                                ?>
                                                <i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
                                                <?php
                                            }
                                            else if ($exp[0] == 1) {
                                                ?>
                                                <i class="fa fa-star"></i><?php ?>
                                                <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                <?php
                                            } else if ($exp[0] == 2) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                            } else if ($exp[0] == 3) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                            } else if ($exp[0] == 4) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                            }
					    else if ($star == 5) {
                                                ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                            <?php
                                            }	
                                        } else {
                                            ?>
                                            <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        }
                                        ?>  
                                    </span>
                                    - <?php 
                                    print_r($user_reviews[0]['firstname']);
                                    ?>
                                </p>
                                <?php
                                }
                                 else{
                                ?>
                                <div align="left" style="margin-top:20px;">
                                <strong >No User Review Found</strong>
                                </div>
                                <?php
                                    
                                }
                                
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-12 text-right">
                    <form action="<?php echo base_url('configure-variant/'.$main_id) ?>" method="POST" style="display:none;">
                    <input type="hidden" id="pro_id" name="pro_id" value="<?php echo $main_id; ?>"/>
                    <input type="hidden" id="selected_vairant" name="selected_vairant" value="<?php echo $accessory_cook; ?>"/>
                    <button type="submit" class="btn btn-maroon confuration"  >Configure & Buy</button>
                    <!--<a class="btn btn-maroon" href="<?php // echo site_url('configure-variant/'.$main_id) ?>">Configure & Buy</a>-->
                    </form>  
                        
                        
                    </div>
                </div>

            </div> <!-- /.container-fluid -->
        </div>
    </div>

</div><!-- /.container -->
