<?php $this->load->view('templates/responsiveSlider'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="table-responsive">
                <table class="table table-bordered configure-options-table">
                    <tbody>
                        <!--
                        <tr>
                            <td class="headline">
                                Trimline
                            </td>
                        </tr>
                        <?php
                        foreach ($car_variant_details as $trimline_data) {
                            ?>
                                <tr>
                                    <td>
                                        <label>
                                            <input type="radio" name="trimline" class="f1" onclick="loadvariant();" id="radio" data-name="<?php print_r($trimline_data['variant_id']); ?>" value="<?php print_r($trimline_data['variant_id']); ?>">
                            <?php print_r($trimline_data['pro_name']); ?>
                                        </label>   
                                    </td>
                                </tr>    
                            <?php
                        }
                        ?>
                        -->    
                        <tr>
                            <td class="headline">
                                <span aria-hidden="true" class="glyphicon glyphicon-search"></span> Fuel Type
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="radio" name="engine" class="f2" onclick="loadvariant();" data-name="1"> Petrol
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="radio" name="engine" class="f2" onclick="loadvariant();" data-name="2" > Diesel
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="radio" name="engine" class="f2" onclick="loadvariant();" data-name="3" > CNG
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="radio" name="engine" class="f2" onclick="loadvariant();" data-name="4" > Electric
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!--<button type="button" class="btn btn-sm btn-maroon save-variant" data-toggle="modal" data-target="#fuel-modal">Help Me</button>-->
                                <!--<button type="button" class="btn btn-sm btn-maroon save-variant" id="help-me">Help Me</button>-->
                            </td>
                        </tr>
                        <tr>
                            <td class="headline"><span aria-hidden="true" class="glyphicon glyphicon-search"></span> Transmission</td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="radio" name="radio" id="radio" class="f3" onclick="loadvariant();" data-name="" > All
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="radio" name="radio" id="radio" class="f3" onclick="loadvariant();" data-name="Manual" > Manual
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="radio" name="radio" id="radio" class="f3" onclick="loadvariant();" data-name="Automatic" > Automatic
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <input type="radio" name="radio" id="radio" class="f3" onclick="loadvariant();" data-name="Semi-Automatic"> Semi-Automatic
                                </label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <input type="hidden" id="pro_id" value="<?php echo $id; ?>"/>
        <div class="col-md-9">
            <div class="table-responsive">
                <table class="table table-striped configure-variant-table">
                    <thead>
                        <tr>
                            <th>Variant</th>
                            <th >Power <span>(bhp)</span></th>
                            <th>IDC Mileage <span>(kmpl)</span></th>
                            <th>Ex Showroom Price</th>
                        </tr>
                    </thead>
                    <tbody id="result">
                        <?php
                        $accessory_cook = $config_selected[0]['variant_type'];
                        if ($accessory_cook == '') {
                            $accessory_cook = $id;
                        }
                        $i=1;
                        $exp_acc = explode(",", $accessory_cook);
                        foreach ($car_variant_details as $car_variant_details_data) {
                            if (in_array($car_variant_details_data["variant_id"], $exp_acc)) {
                                $class = 'checked';
                            } else {
                                $class = '';
                            }
                            ?>
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" id="search_id" value=""/> 
                                            <input class="configure-checkbox check1" <?php echo $class; ?> data-name="<?php echo $car_variant_details_data["variant_id"] ?>" name="<?php echo preg_replace('/\s+/', '-', $car_variant_details_data['pro_name']) . '-' . $car_variant_details_data["variant_id"] ?>"  type="checkbox" onclick="update_price('<?php echo $i; ?>');"> 
                                            <div style="display:none;" id="<?php echo 'car' . $car_variant_details_data["variant_id"]; ?>"><?php echo preg_replace('/\s+/', '-', $car_variant_details_data['pro_name']) . '-' . $car_variant_details_data["variant_id"] ?></div>
                                            <div style="display:none;" id="<?php echo 'car2' . $car_variant_details_data["variant_id"]; ?>"></div>
                                            <?php echo $car_variant_details_data['pro_name_comp']; ?>
                                        </label>
                                    </div>
                                </td>

                                <td > <?php echo $car_variant_details_data['engine_maximum_power']; ?></td>
                                <td style=width:150px;"> <?php echo $car_variant_details_data['mileage']; ?></td>
                                <td>
                                    <div>
                                         <span class="strike-through"><i class="fa fa-rupee"></i>
                                         <?php 
                                        echo indianFormatNumber($car_variant_details_data[0]['exshowroom_price']);
                                        $car_variant_details_data[0]['exshowroom_price']; ?></span>
                                        &nbsp;
                                        <span class="text-maroon" id="mnc_price<?php echo $i; ?>"><?php 
                                        echo indianFormatNumber($car_variant_details_data[1]['mnc_exshowroom_price']);
                                        $car_variant_details_data[1]['mnc_exshowroom_price']; ?></span>
                                    </div>                                    
                                </td>
                            </tr>
                            <?php
                        $i++;
                        }
                        ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <form action="<?php echo base_url('exterior-selection/' . $main_id) ?>" method="POST" style="display:none;">
                    <input type="hidden" id="pro_idss" name="pro_id" value="<?php echo $main_id; ?>"/>
                    <input type="hidden" id="selected_vairant" name="selected_vairant" value="<?php echo $main_id; ?>"/>
                    <!--<input type="text" id="selected_ids" name="selected_ids" value="<?php echo $accessory_cook; ?>"/>-->
                    <a  id="compare-variant" class="btn btn-maroon" onclick="compare_variant();">Compare Variant</a>
                    <a  id="compare-variant2" style="display:none;" class="btn btn-maroon" onclick="compare_variant2();">Compare Variant</a>
                    <button type="submit" class="btn btn-maroon confuration" onclick="return checkselection();">Save and Continue</button>
                    <button type="submit" id="submit" name="submit" class="btn btn-maroon" style="display:none;">Save and Continue</button>
                    <!--<a href="<?php echo site_url('exterior-selection/car-' . $main_id) ?>" class="btn btn-maroon save-variant">Save & Continue</a>-->
                </form>
            </div>
        </div>
    </div>
</div><!-- /.container -->
<input type="hidden" id="count" name="count" value="0" />
<input type="hidden" id="count2" name="count2" value="0" />
<script>
$(function() { 
  $('.configure-checkbox').bind('click',function() {
    $('.configure-checkbox').not(this).prop("checked", false);
  });
  $('.check2').bind('click',function() {
    $('.check2').not(this).prop("checked", false);
  });
});


    function update_price(id)
    {
    var price=$('#mnc_price'+id).html();    
    var price=price.replace(/[^0-9 ]/g, "")
    var grand_price=$('#grand_price').html();    
    var grand_price=grand_price.replace(/[^0-9 ]/g, "")
    var tot_price=$('#tot_price').html();    
    var tot_price=tot_price.replace(/[^0-9 ]/g, "")
    $('#tot_price').html(price_conver(price));
    var final_price=parseInt(price)-parseInt(tot_price);
    var new_total_price=parseInt(grand_price)+parseInt(final_price);
    $('#grand_price').html(price_conver(new_total_price));
    }

    function select_config()
    {    
        var feature_name = "";
        $("input.configure-checkbox2:checked").each(function() {
            feature_name += $(this).data('name') + ', ';
        });
        document.getElementById('selected_vairant').value = feature_name;
    }

    function checkselection()
    {

        $('#count').val('0');
        $('#count2').val('0');
        var count = parseInt($('#count').val());
        var trimline = '';

        $("input.check1:checked").each(function() {
            trimline += $(this).data('name') + ',';
            count += 1;
            $('#count').val(count);
        });

        $("input.check2:checked").each(function() {
            trimline += $(this).data('name') + ',';
            count += 1;
//        alert(count);
            $('#count2').val(count);
        });
//        alert(trimline);
        var count = parseInt($('#count').val());
        var count2 = parseInt($('#count2').val());
//        alert(count);
//        var count2=parseInt($('#count2').val());
        if (count == 1 && count2 == 0)
        {
            var str = trimline.substring(0, trimline.length - 1);
            var ids = document.getElementById('car' + str).innerHTML;
//            alert(ids);
            $('#selected_vairant').val(ids);
            var seleced="<?php echo base_url() . 'exterior-selection/'; ?>" + ids;
            window.location.href=seleced;
            exit;
//            return;
        }
        else if (count == 0 && count2 == 1)
        {
//        alert('sssd');    
            var str = trimline.substring(0, trimline.length - 1);
//           alert(str);
            var ids = document.getElementById('car' + str).innerHTML;
            //alert(ids);
            $('#selected_vairant').val(ids);
            var seleced="<?php echo base_url() . 'exterior-selection/'; ?>" + ids;
            window.location.href=seleced;
            exit;
            //window.location.href = "<?php echo base_url() . 'exterior-selection/'; ?>" + ids;
        }
        else if (count > 1)
        {
            alert('You can select only one variant at a time');
        }
        else
        {
            alert('You select one variant at a time');
        }
//        alert($('#count').val());
        return false;
    }

    function compare_variant()
    {
        var search_id = $('#search_id').val();
        if (search_id == '')
        {
            var selected_vairant = $('#selected_vairant').val();
            var res = selected_vairant.split(",");
            var count = (res.length) - 1;
            if (count >= 2)
            {
                var columns = "";
                for (var i = 0; i < count; i++)
                {
                    var str = res[i];
                    var str2 = str.trim();
                    var datas = document.getElementById('car' + str2).innerHTML;
                    //alert(str2);
                    var com_car = datas + '_';
                    columns += com_car;
                }
                var newString = columns.substr(0, columns.length - 1);
                window.location.href = "<?php echo base_url() . 'comparision/' ?>" + newString;
            }
        }
        else
        {
            var selected_vairant = $('#selected_vairant').val();
            var res = selected_vairant.split(",");
            var count = (res.length) - 1;
            if (count >= 2)
            {
                var columns = "";
                for (var i = 0; i < count; i++)
                {
                    var str = res[i];
                    var str2 = str.trim();
                    var datas = document.getElementById('car2' + str2).innerHTML;
                    var com_car = datas + '_';
                    columns += com_car;
                }
                var newString = columns.substr(0, columns.length - 1);
                window.location.href = "<?php echo base_url() . 'comparision/' ?>" + newString;
            }
        }

    }
</script>

<div class="modal fade" id="fuel-modal" tabindex="-1" role="dialog" aria-labelledby="fuel-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="fuel-modal-label"><i class="fa fa-tint"></i> Fuel Selection</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 model-slider-header">
                        <h4>Choose Your Engine: 
                            <span class="label label-maroon car-variant-label" id="diesel-label"><i class="fa fa-check"></i> Diesel - 1.3 litre</span>
                            <span class="label label-maroon car-variant-label" id="petrol-label"><i class="fa fa-check hide"></i> Petrol - 1.2 litre</span>
                        </h4>
                    </div>

                </div>
                <div class="model-slider-container">
                    <div class="container-fluid">
                        <div class="row model-slider-div">
                            <div class="col-md-12">
                                Daily drive distance in kilometer(KM) - <span id="distance-span">0km</span>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-1 text-right">
                                            0 km
                                        </div>
                                        <div class="col-md-10">
                                            <div class="model-slider" id="distance-calculator-slider"></div>
                                        </div>
                                        <div class="col-md-1 text-left">
                                            400km
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row model-slider-div">
                            <div class="col-md-12">
                                Daily Time Spent on Road - <span id="time-spent-span">0 min</span>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-1 text-right">
                                            0
                                        </div>
                                        <div class="col-md-10">
                                            <div class="model-slider" id="time-spent-slider"></div>
                                        </div>
                                        <div class="col-md-1 text-left">
                                            4hr
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-5 column-centered">
                            <div class="text-center">
                                <button class="btn btn-sm btn-maroon" style="font-weight: bold;" data-dismiss="modal">Choose - <span id="fuel-select-option" style="padding-left: 10px;">Petrol - 1.2 litre</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- /#fuel-modal -->



