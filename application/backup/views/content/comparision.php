<?php
$subcategory = array(
    '1' => 'Headlights',
    '2' => 'Sunroofs',
    '3' => 'Tyres',
    '4' => 'Steering Wheels',
    '5' => 'Seats',
    '6' => 'Navigation System',
    '7' => 'Audio System',
    '8' => 'Speakers',
    '9' => 'Telephone',
    '10' => 'Others'
);
?>
<div class="container" id="car_comparision">
    <div class="row">
        <div class="col-md-12 col-cust-both">
            <h1>Compare New Cars</h1>    
        </div>
        <div class="col-md-12 mnc-tab text-center col-cust-both">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>                
                <li><a href="#specifications" data-toggle="tab">Specifications</a></li>
                <li><a href="#features" data-toggle="tab">Features</a></li>
                <li><a href="#colors" data-toggle="tab">Colors</a></li>
            </ul>
        </div>
    </div>
    <!-- Tab panes -->
    <div class="row">
        <div class="col-md-12 col-cust-both">
            <div class="tab-content">
                <input type="hidden" id="position" />
                <?php
                $count_res = count($product_details);
//                print_r($product_details);
                ?>
                <div class="tab-pane active" id="overview">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed comparison-table">
                            <thead>
                                <tr>
                                    <td ></td>
                                    <?php
                                    $dk = 1;
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">

                                            <?php
                                            if (!empty($pro_details[0]['total_benefits_perc'])) {
                                                ?>
                                                <div class="ribbin">	
                                                    <div class="corner"><h2><?php echo $pro_details[0]['total_benefits_perc']; ?></h2>
                                                        <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                            <img src="<?php echo img_url('uploads/product/' . $pro_details[0]['pro_image']); ?>" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $pro_details[0]['pro_image'])); ?>" />
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                    <img src="<?php echo img_url('uploads/product/' . $pro_details[0]['pro_image']); ?>" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $pro_details[0]['pro_image'])); ?>"  />
                                                </a>
                                                <?php
                                            }
                                            ?>
                                            <a data-toggle="modal" href="#" data-target="#change-car-modal" onclick="$('#position').val('<?php echo $dk; ?>');" >Change Car</a>
                                        </td>
                                        <?php
                                        $dk++;
                                    }
                                    if ($count_res < 3) {
                                        ?>
                                        <td  class="text-left">
                                            <img src="<?php echo base_url() . 'assets/img/fwdswift/outlineofswift.png' ?>" class="no_image" />
                                            <h5>&nbsp;</h5>                                            
                                        </td>
                                        <?php
                                    }
                                    ?>
                                </tr>                                
                            </thead>
                            <thead>
                                <tr>
                                    <td></td>
                                    <?php
                                    $data['session'] = $this->session->userdata("logged_in_user");
                                    $main_city = $this->input->cookie('main_city', TRUE);
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <a class="btn btn-xs search-by-price-button carsuel_book" href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                Book
                                            </a>

                                        </td>                                    
                                        <?php
                                    }
                                    ?>

                                    <td>
                                        <?php
                                        if (count($product_details) < 3) {
                                            ?>
                                            <a class="btn btn-xs search-by-price-button carsuel_book" data-toggle="modal" data-target="#change-car-modal" onclick="$('#position').val('3');">Add Car</a>    
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>    
                            </thead>
                            <tbody>
                                <tr class="car-brand">
                                    <td>Brand</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['brand_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>                                    
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>
                                            <td></td>
                                        <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <tr class="car-name">
                                    <td>Model</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['model_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>                                    
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <tr class="car-model ">
                                    <td id="td2">Variant</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <strong><?php echo $pro_details[0]['pro_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr>  
                                <tr>
                                    <td>Rating</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <div class="rating-div">
                                                <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="User Ratings"><i class="fa fa-users"></i></span>
                                                <span class="text-golden">
                                                    <?php
                                                    $star = ($pro_details[0]['user_rating']);
                                                    if (isset($star)) {
                                                        $exp = explode('.', $star);
                                                        $point = $exp[1];
                                                        if ($point > 00) {
                                                            $half = '-half-full';
                                                        } else if ($point == 00) {
                                                            $half = '-o';
                                                        } else {
                                                            $half = '';
                                                        }

                                                        if ($exp[0] == 1) {
                                                            ?>
                                                            <i class="fa fa-star"></i><?php ?>
                                                            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                            <?php
                                                        } else if ($exp[0] == 2) {
                                                            ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                                        } else if ($exp[0] == 3) {
                                                            ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                                        } else if ($exp[0] == 4) {
                                                            ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                                        } else if ($star == 5) {
                                                            ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>	
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                </span>

                                            </div>
                                            <div class="rating-div">
                                                <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="Expert Ratings"><i class="fa fa-user"></i></span>
                                                <span class="text-maroon">
                                                    <?php
                                                    $star = ($pro_details[0]['exp_rating']);
                                                    if (isset($star)) {
                                                        $exp = explode('.', $star);
                                                        $point = $exp[1];
                                                        if ($point > 00) {
                                                            $half = '-half-full';
                                                        } else if ($point == 00) {
                                                            $half = '-o';
                                                        } else {
                                                            $half = '';
                                                        }

                                                        if ($exp[0] == 1) {
                                                            ?>
                                                            <i class="fa fa-star"></i><?php ?>
                                                            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                            <?php
                                                        } else if ($exp[0] == 2) {
                                                            ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                                        } else if ($exp[0] == 3) {
                                                            ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                                        } else if ($exp[0] == 4) {
                                                            ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                                        } else if ($star == 5) {
                                                            ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>	
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                                        <?php
                                                    }
                                                    ?>    


                                                </span>
                                            </div>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <td>Ex-Showroom Price</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <div class="text-maroon mnc-price pull-left">                                            
                                                <i class="fa fa-rupee"></i> 
                                                <?php
                                                if (indianFormatNumber($pro_details[0]['mnc_price']) == 0) {
                                                    echo Not_applicable;
                                                } else {
                                                    echo indianFormatNumber($pro_details[0]['mnc_price']);
                                                }
                                                ?>
                                            </div>
                                            <br>                                            
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                </tr>

                                <tr>
                                    <td>Fuel Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td><?php
                                            if ($pro_details[0]['fuel_type'] == 1) {
                                                echo 'Petrol';
                                            } else if ($pro_details[0]['fuel_type'] == 2) {
                                                echo 'Diesel';
                                            } else if ($pro_details[0]['fuel_type'] == 3) {
                                                echo 'CNG';
                                            } else if ($pro_details[0]['fuel_type'] == 4) {
                                                echo 'Electric';
                                            }
                                            ?></td>
                                        <?php
                                    }
                                    ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                </tr>

                                <tr>
                                    <td>Transmission Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[0]['tramission_type'])) {
                                                echo $pro_details[0]['tramission_type'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                </tr>
                                <tr>
                                    <td>Body Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[0]['pro_type'])) {
                                                echo $pro_details[0]['pro_type'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                            <?php //echo $pro_details[0]['tramission_type'];    ?>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                </tr>



                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="features">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed comparison-table">
                            <thead>
                                <tr>
                                    <td ></td>
                                    <?php
                                    unset($dk);
                                    $dk = 1;
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <div class="ribbin">	
                                                <div class="corner"><h2><?php echo $pro_details[0]['total_benefits_perc']; ?></h2>
                                                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                        <img src="<?php echo img_url('uploads/product/' . $pro_details[0]['pro_image']) ?>" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $pro_details[0]['pro_image'])); ?>"   />
                                                    </a>
                                                </div>
                                            </div> 
                                            <a data-toggle="modal" href="#" data-target="#change-car-modal" onclick="$('#position').val('<?php echo $dk; ?>');" >Change Car</a>
                                        </td>
                                        <?php
                                        $dk++;
                                    }
                                    if ($count_res < 3) {
                                        ?>
                                        <td  class="text-left">
                                            <img src="<?php echo base_url() . 'assets/img/fwdswift/outlineofswift.png' ?>"  class="no_image"  />
                                            <h5>&nbsp;</h5>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                </tr>                                
                            </thead>
                            <thead>
                                <tr>
                                    <td></td>
                                    <?php
                                    $data['session'] = $this->session->userdata("logged_in_user");
                                    $main_city = $this->input->cookie('main_city', TRUE);
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>

                                            <a class="btn btn-xs search-by-price-button carsuel_book" href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                Book
                                            </a>

                                        </td>                                    
                                        <?php
                                    }
                                    ?>

                                    <td>
                                        <?php
                                        if (count($product_details) < 3) {
                                            ?>
                                            <a class="btn btn-xs search-by-price-button carsuel_book" data-toggle="modal" data-target="#change-car-modal" onclick="$('#position').val('3');">Add Car</a>    
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>    
                            </thead>
                            <tbody>
                                <tr class="car-brand">
                                    <td >Brand</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['brand_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>                                    
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>
                                            <td></td>
                                        <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <tr class="car-name">

                                    <td>Model</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <strong><?php echo $pro_details[0]['model_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>  
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                        <?php
                                    }
                                    ?>                                        

                                </tr>
                                <tr class="car-model ">
                                    <td id="td2" >Variant</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['pro_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr> 
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle5').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Exteriors
                                    </td>
                                </tr> 
                                <tr class='toggle5 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>
                                <?php
//                                echo '<pre>';
//                                print_r($features_names);
//                                echo '</pre>';

                                foreach ($features_names[0] as $product_details_data) {
                                    ?>
                                    <tr class='toggle5'>
                                        <td><?php echo $product_details_data['feature_name']; ?></td>
                                        <?php
                                        for ($i = 0; $i < count($product_details2); $i++) {
                                            ?>
                                            <td>
                                                <?php
                                                for ($j = 0; $j < count($product_details2[$i]); $j++) {
                                                    if ($product_details2[$i][$j]['feature_name'] == $product_details_data['feature_name']) {
                                                        if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'Y') {
                                                            ?>
                                                            <i class="fa fa-check"></i>
                                                            <?php
                                                        } else if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'N') {
                                                            ?>
                                                            <i class="fa fa-times"></i>
                                                            <?php
                                                        } else {
                                                            echo $product_details2[$i][$j]['feature_desc'];
                                                        }

                                                        unset($product_details2[$i][$j]['feature_desc']);
//                                                        echo $product_details2[$i][$j]['feature_desc'];
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>
                                            </td>     

                                            <?php
                                        }
                                        ?>

                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td class="feildset-td"   colspan="6" onclick="$('.toggle6').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Interiors
                                    </td>
                                </tr> 
                                <tr class='toggle6 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>
                                <?php
                                foreach ($features_names[1] as $product_details_data) {
                                    ?>
                                    <tr class='toggle6'>
                                        <td><?php echo $product_details_data['feature_name']; ?></td>
                                        <?php
                                        for ($i = 0; $i < count($product_details2); $i++) {
                                            ?>
                                            <td>
                                                <?php
                                                for ($j = 0; $j < count($product_details2[$i]); $j++) {
                                                    if ($product_details2[$i][$j]['feature_name'] == $product_details_data['feature_name']) {
//                                                        echo $product_details2[$i][$j]['feature_desc'];
                                                        if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'Y') {
                                                            ?>
                                                            <i class="fa fa-check"></i>
                                                            <?php
                                                        } else if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'N') {
                                                            ?>
                                                            <i class="fa fa-times"></i>
                                                            <?php
                                                        } else {
                                                            echo $product_details2[$i][$j]['feature_desc'];
                                                        }

                                                        unset($product_details2[$i][$j]['feature_desc']);
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>
                                            </td>     

                                            <?php
                                        }
                                        ?>

                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?>  
                                <tr>
                                    <td class="feildset-td"   colspan="6" onclick="$('.toggle7').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Comfort & Convenience
                                    </td>
                                </tr>  

                                <tr class='toggle7 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <?php
                                foreach ($features_names[2] as $product_details_data) {
                                    ?>
                                    <tr class='toggle7'>
                                        <td><?php echo $product_details_data['feature_name']; ?></td>
                                        <?php
                                        for ($i = 0; $i < count($product_details2); $i++) {
                                            ?>
                                            <td>
                                                <?php
                                                for ($j = 0; $j < count($product_details2[$i]); $j++) {
                                                    if ($product_details2[$i][$j]['feature_name'] == $product_details_data['feature_name']) {
//                                                        echo $product_details2[$i][$j]['feature_desc'];
                                                        if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'Y') {
                                                            ?>
                                                            <i class="fa fa-check"></i>
                                                            <?php
                                                        } else if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'N') {
                                                            ?>
                                                            <i class="fa fa-times"></i>
                                                            <?php
                                                        } else {
                                                            echo $product_details2[$i][$j]['feature_desc'];
                                                        }

                                                        unset($product_details2[$i][$j]['feature_desc']);
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>
                                            </td>     

                                            <?php
                                        }
                                        ?>

                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?> 





                                <tr>
                                    <td class="feildset-td"   colspan="6" onclick="$('.toggle8').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Safety
                                    </td>
                                </tr>                                 
                                <tr class='toggle8 toggle_none'>
                                    <td>                                        
                                    </td>    
                                </tr>


                                <?php
                                foreach ($features_names[3] as $product_details_data) {
                                    ?>
                                    <tr class='toggle8'>
                                        <td><?php echo $product_details_data['feature_name']; ?></td>
                                        <?php
                                        for ($i = 0; $i < count($product_details2); $i++) {
                                            ?>
                                            <td>
                                                <?php
                                                for ($j = 0; $j < count($product_details2[$i]); $j++) {
                                                    if ($product_details2[$i][$j]['feature_name'] == $product_details_data['feature_name']) {
//                                                        echo $product_details2[$i][$j]['feature_desc'];
                                                        if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'Y') {
                                                            ?>
                                                            <i class="fa fa-check"></i>
                                                            <?php
                                                        } else if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'N') {
                                                            ?>
                                                            <i class="fa fa-times"></i>
                                                            <?php
                                                        } else {
                                                            echo $product_details2[$i][$j]['feature_desc'];
                                                        }

                                                        unset($product_details2[$i][$j]['feature_desc']);
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>
                                            </td>     

                                            <?php
                                        }
                                        ?>

                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?> 


                                <tr>
                                    <td class="feildset-td"   colspan="6"  onclick="$('.toggle9').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Entertainment & Communication
                                    </td>
                                </tr> 

                                <tr class='toggle9 toggle_none'>
                                    <td>                                        
                                    </td>    
                                </tr>

                                <?php
                                foreach ($features_names[4] as $product_details_data) {
                                    ?>
                                    <tr class='toggle9'>
                                        <td><?php echo $product_details_data['feature_name']; ?></td>
                                        <?php
                                        for ($i = 0; $i < count($product_details2); $i++) {
                                            ?>
                                            <td>
                                                <?php
                                                for ($j = 0; $j < count($product_details2[$i]); $j++) {
                                                    if ($product_details2[$i][$j]['feature_name'] == $product_details_data['feature_name']) {
//                                                        echo $product_details2[$i][$j]['feature_desc'];
                                                        if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'Y') {
                                                            ?>
                                                            <i class="fa fa-check"></i>
                                                            <?php
                                                        } else if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'N') {
                                                            ?>
                                                            <i class="fa fa-times"></i>
                                                            <?php
                                                        } else {
                                                            echo $product_details2[$i][$j]['feature_desc'];
                                                        }

                                                        unset($product_details2[$i][$j]['feature_desc']);
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>
                                            </td>     

                                            <?php
                                        }
                                        ?>

                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?> 


                                <tr>
                                    <td class="feildset-td"   colspan="6" onclick="$('.toggle10').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Service
                                    </td>
                                </tr> 

                                <tr class='toggle10 toggle_none'>
                                    <td>                                        
                                    </td>    
                                </tr>


                                <?php
                                foreach ($features_names[5] as $product_details_data) {
                                    ?>
                                    <tr class='toggle10'>
                                        <td><?php echo $product_details_data['feature_name']; ?></td>
                                        <?php
                                        for ($i = 0; $i < count($product_details2); $i++) {
                                            ?>
                                            <td>
                                                <?php
                                                for ($j = 0; $j < count($product_details2[$i]); $j++) {
                                                    if ($product_details2[$i][$j]['feature_name'] == $product_details_data['feature_name']) {
//                                                        echo $product_details2[$i][$j]['feature_desc'];
                                                        if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'Y') {
                                                            ?>
                                                            <i class="fa fa-check"></i>
                                                            <?php
                                                        } else if (strtoupper($product_details2[$i][$j]['feature_desc']) == 'N') {
                                                            ?>
                                                            <i class="fa fa-times"></i>
                                                            <?php
                                                        } else {
                                                            echo $product_details2[$i][$j]['feature_desc'];
                                                        }

                                                        unset($product_details2[$i][$j]['feature_desc']);
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>
                                            </td>     

                                            <?php
                                        }
                                        ?>

                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                    </tr>
                                    <?php
                                }
                                ?> 

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="specifications">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed comparison-table">
                            <thead>
                                <tr>
                                    <td  ></td>
                                    <?php
                                    unset($dk);
                                    $dk = 1;
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <div class="ribbin">	
                                                <div class="corner"><h2><?php echo $pro_details[0]['total_benefits_perc']; ?></h2>
                                                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                        <img src="<?php echo img_url('uploads/product/' . $pro_details[0]['pro_image']);?>" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $pro_details[0]['pro_image'])); ?>"  />
                                                    </a>
                                                </div>
                                            </div> 
                                            <a data-toggle="modal" href="#" data-target="#change-car-modal" onclick="$('#position').val('<?php echo $dk; ?>');" >Change Car</a>
                                        </td>
                                        <?php
                                        $dk++;
                                    }
                                    if ($count_res < 3) {
                                        ?>
                                        <td  class="text-left" >
                                            <img src="<?php echo base_url() . 'assets/img/fwdswift/outlineofswift.png' ?>" class="no_image"  />
                                            <h5>&nbsp;</h5>
                                            <!--<button class="btn btn-maroon btn-xs" data-toggle="modal" data-target="#change-car-modal" onclick="$('#position').val('3');">Add Car</button>-->
                                        </td>
                                        <?php
                                    }
                                    ?>
                                </tr>                                
                            </thead>
                            <thead>
                                <tr>
                                    <td></td>
                                    <?php
                                    $data['session'] = $this->session->userdata("logged_in_user");
                                    $main_city = $this->input->cookie('main_city', TRUE);
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>

                                            <a class="btn btn-xs search-by-price-button carsuel_book" href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                Book
                                            </a>

                                        </td>                                    
                                        <?php
                                    }
                                    ?>

                                    <td>
                                        <?php
                                        if (count($product_details) < 3) {
                                            ?>
                                            <a class="btn btn-xs search-by-price-button carsuel_book" data-toggle="modal" data-target="#change-car-modal" onclick="$('#position').val('3');">Add Car</a>    
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>    
                            </thead>
                            <tbody>
                                <tr class="car-brand">
                                    <td >Brand</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['brand_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>                                    
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>
                                            <td></td>
                                        <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <tr class="car-name">

                                    <td>Model</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <strong><?php echo $pro_details[0]['model_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>  
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                        <?php
                                    }
                                    ?>                                        

                                </tr>
                                <tr class="car-model ">
                                    <td id="td2" >Variant</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['pro_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            <tbody>

                                                          
                                <tr>
                                    <td class="feildset-td"  colspan="6" onclick="$('.toggle2').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Capacity
                                    </td>
                                </tr>
                                <tr class='toggle2 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>
                                <tr class='toggle2'>
                                    <td >Seating Capacity</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['body_seating_capacity'])) {
                                                echo $pro_details[1]['body_seating_capacity'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>


                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>
                                <tr class='toggle2'>
                                    <td>Trunk Volume (l)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['trunk_volume'])) {
                                                if (strtoupper($pro_details[1]['trunk_volume']) == 'Y') {
                                                    ?>
                                                    <i class="fa fa-check"></i>
                                                    <?php
                                                } else if (strtoupper($pro_details[1]['trunk_volume']) == 'N') {
                                                    ?>
                                                    <i class="fa fa-times"></i>
                                                    <?php
                                                } else {
                                                    echo $pro_details[1]['trunk_volume'];
                                                }
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        </td>
                                        <?php
                                    }
                                    ?>                                                  
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle2'>
                                    <td>Doors</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[1]['body_no_of_doors'])) {
                                                echo $pro_details[1]['body_no_of_doors'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle2'>
                                    <td>Number of Seating Rows</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['no_seating_rows'])) {
                                                echo $pro_details[1]['no_seating_rows'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle2'>
                                    <td>Bootspace (liters)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['bootspace'])) {
                                                echo $pro_details[1]['bootspace'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle2'>
                                    <td>Fuel Tank Capacity (liters)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[1]['fuel_tank_capacity1'])) {
                                                echo $pro_details[1]['fuel_tank_capacity1'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>

                                
                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle5').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Steering
                                    </td>
                                </tr>


                                <tr class='toggle5 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr class='toggle5'>
                                    <td >Steering Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['streeing_type'])) {
                                                echo $pro_details[1]['streeing_type'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>


                                <tr class='toggle5'>
                                    <td >Turning Radius (m)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['streeing_turn_radius'])) {
                                                echo $pro_details[1]['streeing_turn_radius'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>




                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle4').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Suspension
                                    </td>
                                </tr>




                                <tr class='toggle4 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr class='toggle4'>
                                    <td >Suspension Front</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['suspension_front'])) {
                                                echo $pro_details[1]['suspension_front'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>
                                <tr class='toggle4'>
                                    <td>Suspension Rear</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[1]['suspension_rear'])) {
                                                echo $pro_details[1]['suspension_rear'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <!--tr class='toggle4'>
                                    <td>Front Brake Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['front_tyres'])) {
                                                echo $pro_details[1]['front_tyres'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>

                                <tr class='toggle4'>
                                    <td>Streeing Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[1]['streeing_type'])) {
                                                echo $pro_details[1]['streeing_type'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle4'>
                                    <td>Minimum Turning Radius (meters)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['streeing_turn_radius'])) {
                                                echo $pro_details[1]['streeing_turn_radius'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>


                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle4'>
                                    <td>Front Tyres</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['front_tyres'])) {
                                                echo $pro_details[1]['front_tyres'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle4'>
                                    <td>Rear Tyres</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['rear_tyre'])) {
                                                echo $pro_details[1]['rear_tyre'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>                                           
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr-->
                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle20').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Transmission
                                    </td>
                                </tr>


                                <tr class='toggle20 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr class='toggle20'>
                                    <td >Transmission Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[0]['tramission_type'])) {
                                                echo $pro_details[0]['tramission_type'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>

                                <tr class='toggle20'>
                                    <td >No of gears</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['no_gear'])) {
                                                echo $pro_details[1]['no_gear'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>

                                <tr class='toggle20'>
                                    <td >Drivetrain</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['drivetrain'])) {
                                                echo $pro_details[1]['drivetrain'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>
                                
                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle21').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Tyres
                                    </td>
                                </tr>


                                <tr class='toggle21 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr class='toggle21'>
                                    <td >Tire Size</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['tyre_size'])) {
                                                echo $pro_details[1]['tyre_size'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>

                                <tr class='toggle21'>
                                    <td >Wheel Size</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['wheel_size'])) {
                                                echo $pro_details[1]['wheel_size'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>
                                <tr class='toggle21'>
                                    <td >Spare Tyre</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['spare_type'])) {
                                                echo $pro_details[1]['spare_type'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>
                                
                                
                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle22').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Brakes
                                    </td>
                                </tr>


                                <tr class='toggle22 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr class='toggle22'>
                                    <td >Brakes Front</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['brakes_front'])) {
                                                echo $pro_details[1]['brakes_front'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>

                                <tr class='toggle22'>
                                    <td >Brakes Rear</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['brakes_rear'])) {
                                                echo $pro_details[1]['brakes_rear'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>


                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle23').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Performance
                                    </td>
                                </tr>


                                <tr class='toggle23 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr class='toggle23'>
                                    <td >Top Speed</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['top_speed'])) {
                                                echo $pro_details[1]['top_speed'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>

                                <tr class='toggle23'>
                                    <td >Acceleration ( 0-100 kmph)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['acceleration'])) {
                                                echo $pro_details[1]['acceleration'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>


                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle24').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Weight
                                    </td>
                                </tr>


                                <tr class='toggle24 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr class='toggle24'>
                                    <td >Kerb Weight</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['kerb_weight'])) {
                                                echo $pro_details[1]['kerb_weight'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>

                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle25').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Warranty
                                    </td>
                                </tr>


                                <tr class='toggle25 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr class='toggle25'>
                                    <td >Standard Warranty</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['standard_warranty'])) {
                                                echo $pro_details[1]['standard_warranty'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <td ></td>
                                </tr>
                                
                                
                                
                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle3').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Engine
                                    </td>
                                </tr>

                                <tr class='toggle3 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>

                                <tr>
                                <tr class='toggle3'>
                                    <td >Engine Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['engine_types'])) {
                                                echo $pro_details[1]['engine_types'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <td ></td>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Displacement (cc)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td >
                                            <?php
                                            if (!empty($pro_details[1]['displacement'])) {
                                                echo $pro_details[1]['displacement'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Fuel Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if ($pro_details[0]['fuel_type'] == 1) {
                                                echo 'Petrol';
                                            } else if ($pro_details[0]['fuel_type'] == 2) {
                                                echo 'Diesel';
                                            } else if ($pro_details[0]['fuel_type'] == 3) {
                                                echo 'CNG';
                                            } else if ($pro_details[0]['fuel_type'] == 4) {
                                                echo 'Electric';
                                            }
                                            ?></td>
                                        <?php
                                    }
                                    ?>
                                        <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Power (bhp@rpm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[1]['engine_maximum_power'])) {
                                                echo $pro_details[1]['engine_maximum_power'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Torque (Nm@rpm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[1]['engine_maximum_torque'])) {
                                                echo $pro_details[1]['engine_maximum_torque'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>


                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>IDC Mileage (kmpl)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['mileage'])) {
                                                echo $pro_details[1]['mileage'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>



                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Alternate Fuel</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['alternate_fuel'])) {
                                                echo $pro_details[1]['alternate_fuel'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>


                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Bore (mm) x Stroke (mm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['bore_stroke'])) {
                                                echo $pro_details[1]['bore_stroke'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Compression Ratio</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['comparision_ratio'])) {
                                                echo $pro_details[1]['comparision_ratio'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>



                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Valves Per Cylinder</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[1]['engine_no_of_valves'])) {
                                                echo $pro_details[1]['engine_no_of_valves'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>


                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle2'>
                                    <td>Emission Norms</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['engine_emission'])) {
                                                if (strtoupper($pro_details[1]['engine_emission']) == 'Y') {
                                                    ?>
                                                    <i class="fa fa-check"></i>
                                                    <?php
                                                } else if (strtoupper($pro_details[1]['engine_emission']) == 'N') {
                                                    ?>
                                                    <i class="fa fa-times"></i>
                                                    <?php
                                                } else {
                                                    echo $pro_details[1]['engine_emission'];
                                                }
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        </td>
                                        <?php
                                    }
                                    ?>                                                  
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                
                                
                                
                                <tr class='toggle3'>
                                    <td>Cylinders</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[1]['cylinders'])) {
                                                echo $pro_details[1]['cylinders'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Fuel System</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['fuel_system'])) {
                                                echo $pro_details[1]['fuel_system'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>                                            
                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Transmission Type</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($pro_details[0]['tramission_type'])) {
                                                echo $pro_details[0]['tramission_type'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Number of Gears</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['no_gear'])) {
                                                echo $pro_details[1]['no_gear'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Dual Clutch</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if ($pro_details[1]['dual_cluth'] == 1) {
                                                ?>
                                                <i class="fa fa-check"></i>
                                                <?php
                                            } else if ($pro_details[1]['dual_cluth'] == 2) {
                                                ?>
                                                <i class="fa fa-times"></i>
                                                <?php
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Sport Mode</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if ($pro_details[1]['sport_mode'] == 1) {
                                                ?>
                                                <i class="fa fa-check"></i>
                                                <?php
                                            } else if ($pro_details[1]['sport_mode'] == 2) {
                                                ?>
                                                <i class="fa fa-times"></i>
                                                <?php
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle3'>
                                    <td>Drivetrain</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['drivetrain'])) {
                                                echo $pro_details[1]['drivetrain'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>                                            
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                
                                
                                <tr>
                                    <td class="feildset-td" colspan="6"  onclick="$('.toggle1').fadeToggle(500);">
                                        <i class="fa fa-caret-down"></i> Dimensions
                                    </td>
                                </tr>
                                <tr class='toggle1 toggle_none'>
                                    <td>

                                    </td>    
                                </tr>
                                <tr class='toggle1'>
                                    <td >Length (mm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['dimensions_mm_length'])) {
                                                echo $pro_details[1]['dimensions_mm_length'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <td ></td>
                                </tr>
                                <tr class='toggle1'>
                                    <td>Width (mm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['dimensions_mm_width'])) {
                                                echo $pro_details[1]['dimensions_mm_width'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>                                            <?php } ?>
                                </tr>
                                <tr class='toggle1'>
                                    <td>Height (mm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['dimensions_mm_height'])) {
                                                echo $pro_details[1]['dimensions_mm_height'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle1'>
                                    <td>Wheelbase (mm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['dimensions_mm_weelbase'])) {
                                                echo $pro_details[1]['dimensions_mm_weelbase'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?>                                                  
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle1'>
                                    <td>Rear seat legroom (cm)</td>
                                    <?php
//                                    echo '<pre>';
//                                    print_r($product_details);
//                                    echo '</pre>';
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['rear_seat_legroom'])) {
                                                if (strtoupper($pro_details[1]['rear_seat_legroom']) == 'Y') {
                                                    ?>
                                                    <i class="fa fa-check"></i>
                                                    <?php
                                                } else if (strtoupper($pro_details[1]['rear_seat_legroom']) == 'N') {
                                                    ?>
                                                    <i class="fa fa-times"></i>
                                                    <?php
                                                } else {
                                                    echo $pro_details[1]['rear_seat_legroom'];
                                                }
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        </td>
                                        <?php
                                    }
                                    ?>                                                  
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle1'>
                                    <td>Headroom (cm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['headroom'])) {
                                                if (strtoupper($pro_details[1]['headroom']) == 'Y') {
                                                    ?>
                                                    <i class="fa fa-check"></i>
                                                    <?php
                                                } else if (strtoupper($pro_details[1]['headroom']) == 'N') {
                                                    ?>
                                                    <i class="fa fa-times"></i>
                                                    <?php
                                                } else {
                                                    echo $pro_details[1]['headroom'];
                                                }
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        </td>
                                        <?php
                                    }
                                    ?>                                                  
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle1'>
                                    <td>Seat Width (cm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['seat_width'])) {
                                                if (strtoupper($pro_details[1]['seat_width']) == 'Y') {
                                                    ?>
                                                    <i class="fa fa-check"></i>
                                                    <?php
                                                } else if (strtoupper($pro_details[1]['seat_width']) == 'N') {
                                                    ?>
                                                    <i class="fa fa-times"></i>
                                                    <?php
                                                } else {
                                                    echo $pro_details[1]['seat_width'];
                                                }
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        </td>
                                        <?php
                                    }
                                    ?>                                                  
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle1'>
                                    <td>Ground Clearance (mm)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['dimensions_mm_ground_clearance'])) {
                                                echo $pro_details[1]['dimensions_mm_ground_clearance'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                        <?php
                                    }
                                    ?> 
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>
                                <tr class='toggle1'>
                                    <td>Kerb Weight (Kg)</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left">
                                            <?php
                                            if (!empty($pro_details[1]['kerb_weight'])) {
                                                echo $pro_details[1]['kerb_weight'];
                                            } else {
                                                echo '-';
                                            }
                                            ?>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>  

                                

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="colors">
                    <div class="table-responsive">
                        <?php
                        $count_res = count($product_details);
                        ?>
                        <table class="table table-striped table-condensed comparison-table">
                            <thead>
                                <tr>
                                    <td  ></td>
                                    <?php
                                    unset($dk);
                                    $dk = 1;
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <div class="ribbin">	
                                                <div class="corner"><h2><?php echo $pro_details[0]['total_benefits_perc']; ?></h2>
                                                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                        <img src="<?php echo img_url('uploads/product/' . $pro_details[0]['pro_image']);?>" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $pro_details[0]['pro_image'])); ?>"  />
                                                    </a>
                                                </div>
                                            </div> 

                                            <a data-toggle="modal" href="#" data-target="#change-car-modal" onclick="$('#position').val('<?php echo $dk; ?>');" >Change Car</a>
                                        </td>
                                        <?php
                                        $dk++;
                                    }
                                    if ($count_res < 3) {
                                        ?>
                                        <td  class="text-left" >
                                            <img src="<?php echo base_url() . 'assets/img/fwdswift/outlineofswift.png' ?>" class="no_image"  />
                                            <h5>&nbsp;</h5>

                                        </td>
                                        <?php
                                    }
                                    ?>
                                </tr>                                
                            </thead>
                            <thead>
                                <tr>
                                    <td></td>
                                    <?php
                                    $data['session'] = $this->session->userdata("logged_in_user");
                                    $main_city = $this->input->cookie('main_city', TRUE);
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td>

                                            <a class="btn btn-xs search-by-price-button carsuel_book" href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $pro_details[0]['pro_name_comp']) . '-' . $pro_details[0]['variant_id']) ?>">
                                                Book
                                            </a>

                                        </td>                                    
                                        <?php
                                    }
                                    ?>

                                    <td>
                                        <?php
                                        if (count($product_details) < 3) {
                                            ?>
                                            <a class="btn btn-xs search-by-price-button carsuel_book" data-toggle="modal" data-target="#change-car-modal" onclick="$('#position').val('3');">Add Car</a>    
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>    
                            </thead>
                            <tbody>
                            <tbody>
                                <tr class="car-brand">
                                    <td >Brand</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['brand_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>                                    
                                    <?php
                                    if (count($product_details) < 3) {
                                        ?>
                                        <?php if (count($product_details) < 3) { ?>
                                            <td></td>
                                        <?php } ?>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <tr class="car-name">

                                    <td >Model</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['model_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>                                    
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>

                                <tr class="car-model ">
                                    <td id="td2" >Variant</td>
                                    <?php
                                    foreach ($product_details as $pro_details) {
                                        ?>
                                        <td class="text-left" >
                                            <strong><?php echo $pro_details[0]['pro_name']; ?></strong>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    <?php if (count($product_details) < 3) { ?>                                            <td></td>                                            <?php } ?>
                                </tr>  


                                <tr id="feature_img">
                                    <td class="comp_availabe_color">Available Colors</td>
                                    <?php
                                    foreach ($product_details as $product_details_color) {
                                        $cr = 0;
                                        ?>
                                        <td class="text-left">
                                            <table>
                                                <?php
                                                foreach ($product_details_color as $product_details_color_res) {

                                                    if ($cr > 1) {
//                                                    echo '<pre>';
//                                                    print_r(count($product_details_color_res));
                                                        foreach ($product_details_color_res as $product_details_color_res_data) {
                                                            ?>
                                                            <tr>
                                                                <td class="text-left">
                                                                    <div class="pull-left">
                                                                        <img  src="<?php echo img_url('uploads/feature/' . $product_details_color_res_data['feature_img']) ?>"/>    
                                                                    </div>
                                                                    <div class="available-color-name">
                                                                        <?php
                                                                        print($product_details_color_res_data['feature_desc']);
                                                                        ?>
                                                                    </div>
                                                                </td>
                                                            </tr>                                           
                                                            <?php
                                                        }
                                                    }
                                                    $cr++;
                                                }
                                                ?>
                                            </table> 
                                        </td>
                                        <?php
                                    }
                                    ?>                                
                                    <td style="width:10%;"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div> <!-- /.container -->
<!--change-car-modal-->
<div class="modal fade" id="change-car-modal" tabindex="-1" role="dialog" aria-labelledby="change-car-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="change-car-modal-label"><i class="fa fa-dashboard"></i> Select Car</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 select-brand-container col-sx-12 col-sm-12 col-cust-both">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <select class="form-control" id="brand_data" onchange="setmodel();">
                                    <option value="">Select Brand</option>
                                    <?php
                                    if (!empty($get_brands)) {
                                        ####### home Choose your desired Brand to buy start here ######
                                        foreach ($get_brands as $all_brands_res) {
                                            ?>    
                                            <option value="<?php echo $all_brands_res['brand_id']; ?>" title="none" data-content="<div class='text-left' ><img src="<?php echo img_url('uploads/brand/' . $all_brands_res['brand_image']); ?>" class='brand-img' style='color:black;height:25px;width:30px;'> <span class='brand-name'><?php echo $all_brands_res['brand_name']; ?></span></div>"><?php echo $all_brands_res['brand_name']; ?></option>
                                            <?php
                                        }
                                    }
                                    ####### home Choose your desired Brand to buy End here ######
                                    ?>                                
                                </select>
                                <br>
                            </div>
                            <div id="car_model" class="col-md-4 col-sm-12 col-xs-12" onchange="setvariant();">
                                <select id="brand_model" class="form-control">    
                                    <option>Select Model</option>   
                                </select>
                                <br>    
                            </div>
                            <div id="car_varinant_loading" class="col-md-4  col-sm-12 col-xs-12 ">
                                <select class="form-control" id="car_variant">
                                    <option>Select Variant</option>

                                </select>
                                <br>
                            </div>
                        </div>
                        <div class="col-md-12 select-brand-container col-sx-12 col-sm-12">
                            <div class="pull-right">                            
                                <a class="btn btn-xs search-by-price-button carsuel_book" onclick="check_before_change();">Submit</a>
                                <button type="button" class="btn btn-maroon btn-sm" style="display:none;" id="chgane_car_button" onclick="change_car();">Select</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12 select-brand-container col-sx-12 col-sm-12 col-cust-both"> 
                                        <button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>
                                        <a class="btn btn-xs search-by-price-button carsuel_book" onclick="check_before_change();">Submit</a>
                                        <button type="button" class="btn btn-maroon btn-sm" style="display:none;" id="chgane_car_button" onclick="change_car();">Select</button>
                                        <br><br>
                                    </div>
                                </div>
                            </div>
                        </div>-->

        </div>
    </div>
</div>
