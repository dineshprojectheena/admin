<div class="container" >
    <div class="row">
        <div class="col-md-12 col-cust-both">
            <div class="col-md-6 col-cust-both">
            <div class="review-box">
                <h4 class="review-title">
                    <i class="glyphicon glyphicon-film"></i> Search results for "<?php echo $name; ?>"
                </h4>
            </div> 
            </div> 
            <div class="col-md-6">
            <div class="review-box">
                <h4 class="review-title pull-right">
                    Total results : <?php echo count($search_data); ?>
                </h4>
            </div> 
            </div> 
            <div class="col-md-12 col-cust-both" id="group-item-carousel">    
                <?php
                foreach ($search_data as $c => $key) {
                    $sort_mnc_exshowroom_price[] = $key['exshowroom_price'];
                }
                array_multisort($sort_mnc_exshowroom_price, ASC, $search_data);
                
                if (!empty($search_data)) {
                    $cnt = 0;
                    foreach ($search_data as $search_data_res) {
                        
                        ?>
                        <?php
                        if (!empty($search_data_res['exshowroom_price'])) {
                            ?>
                            <?php
                               
                                if($cnt==0)
                                {
                                ?>
                                <!--<div class="container" >-->
                                <div class="col-md-12 col-cust-both">
                                
                                
                                <?php
                                }    
                                ?>
                            <div class="col-md-4 col-xs-12 col-cust-both ">
                                <?php
//                                 echo $cnt;
                                ?>
                                
                                <!--<div class="container-fluid">-->
                                <!--<div class="row">-->
                                <div class="ribbin">	
                                    <div class="corner">
                                        <!--<h2><?php echo $hot_deals_data_res['total_benefits_perc']; ?></h2>-->
                                        <div class="group-item">
                                            <div class="col-md-12 col-cust-both" align="center">
                                                <a href="<?php echo site_url('car/' . preg_replace('/\s+/', '-', $search_data_res['pro_name_comp']) . '-' . $search_data_res['variant_id']) ?>" >        
                                                    <img alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $search_data_res['pro_image'])); ?>" src="<?php  echo img_url('/uploads/product/' . $search_data_res['pro_image']); ?>" class="img-responsive">
                                                </a>                                                                        
                                            </div>
                                            <div class="group-content">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4 class="hot_dealreview_title"> 
                                                                <?php echo $search_data_res['pro_name_comp']; ?>                                                        </h4>
                                                        </div> 
                                                        <div class="col-md-12">
                                                            <div class="group-price">
                                                                <div class="mnc-price">
                                                                    <span class="price-span">
                                                                        Ex-Showroom Price
                                                                    </span>
                                                                    <span class="text-maroon">
                                                                        <i class="fa fa-rupee"></i>
                                                                        <?php echo $search_data_res['exshowroom_price']; ?>  

                                                                    </span>
                                                                </div>     
                                                            </div>
                                                        </div>    
                                                        <div class="col-md-12">
                                                            <span class="price-span">Total Savings </span>
                                                            <span class="price-span text-maroon"><i class="fa fa-rupee"></i>
                                                                <?php echo $search_data_res['total_savings']; ?>   
                                                            </span>
                                                        </div>
                                                        <?php
                                                        if (!empty($search_data_res['valid_until'])) {
                                                            ?>
                                                            <div class="col-md-12">
                                                                <span class="price-span">Valid Until </span>
                                                                <span class="price-span text-maroon">
                                                                    <?php echo $search_data_res['valid_until']; ?>   
                                                                </span>
                                                            </div>  
                                                            <?php
                                                        }
                                                        ?>
                                                        <div class="col-md-12">
                                                            <div class="pull-right">
                                                                <a href="<?php echo site_url('car/' . preg_replace('/\s+/', '-', $search_data_res['pro_name_comp']) . '-' . $search_data_res['variant_id']); ?>" class="btn btn-xs carsuel_book">Book</a>
                                                            </div>
                                                        </div>
                                                    </div>      
                                                </div>
                                            </div>

                                        </div>
                                    </div>      
                                </div>
                                
                                <!--</div>--> 
                                <!--</div>-->
                                <!--</div>-->
                            </div>
                                    <?php
                                if($cnt==2)
                                {
                                ?>
                                
                                <!--</div>-->
                                </div>
                                <?php
                                $cnt='-1';
                                }    
                                ?>

                            <?php
                            $cnt++;
                        }
//                        $ii++;
                        
                    }
                } else {
                    ?>
                    <div align="center" class="no_carousel">
                        <img src="<?php echo base_url(); ?>/assets/img/ComingSoon.png">
                    </div>
                    <?php
                }
                ?>
            </div>            
        </div>
    </div>



</div>
