RewriteEngine on
RewriteBase /

# Redirect to https
RewriteCond %{HTTPS} off
RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
# End https redirect

# START Remove trailing slash
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)(/+)$ $1 [L,R=301]
# END Remove trailing slash

RewriteCond %{HTTP_HOST} ^www\.projectheena\.com [NC]
RewriteRule ^(.*)$ https://projectheena.com/$1 [R=301,L]

RewriteCond $1 !^(index\.php|ref_doc|media|images|robots\.txt|favicon\.ico)
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php/$1 [L]