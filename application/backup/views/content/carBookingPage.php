<?php $this->load->view('templates/responsiveSlider'); ?>
<script>
    $('#configuration_button').css('display', 'none');
</script>
<div class="container">
    <!--    <div class="row">
            <div class="col-md-12">
                <div class="clearfix">
                    <div class="pull-right">
                        <button onclick="window.location.href = '<?php echo base_url() . 'accessory-services/' . $main_id; ?>'" type="button" class="btn btn-grey">Back</button>
                        <a class="btn btn-maroon" href="<?php echo site_url('payment-page/' . $main_id); ?>">Book My New Car</a>
                    </div> 
                </div>
            </div>        
        </div>-->
    <div class="row">
        <div class="col-md-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 mnc-tab text-center">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#summary" data-toggle="tab">Summary</a></li>
                            <li><a href="#technical-data" data-toggle="tab">Specifications</a></li>
                            <li><a href="#std-equipment" data-toggle="tab">Features</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="summary">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered table-striped car-accessory-table">
                                        <thead>
                                            <tr>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h5> <?php
                                                        print_r($car_into_details[0]['pro_name_comp']);
                                                        ?>
                                                    </h5>
                                                    <p>
                                                        <?php
                                                        $pro_detail = $car_into_details[0]['pro_detail'];
                                                        $pro_detail_data = explode('.', $pro_detail);
                                                        echo $pro_detail_data[0] . '.' . $pro_detail_data[1] . '.';
                                                        ?>    
                                                    </p>
                                                    <div style="float:left;display:none;"><a href="#" data-toggle="modal" data-target="#email_to_friend">Save and Email</a></div>                                                    
                                                </td>
                                            </tr>
                                            <tr class="fieldset-tr">
                                                <td class="feildset-td" colspan="3">
                                                    <i class="fa fa-caret-right"></i> Variant
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="table table-hover table-bordered table-striped nested-table">
                                                        <?php
                                                        $i = 0;
                                                        foreach ($booking_varaint as $car_variant_details_data) {
                                                            if ($i > 0) {
//                                                      
                                                                ?>
                                                                <tr>
                                                                    <td style="width:60%;"> 
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input class="configure-checkbox" data-name="<?php echo $car_variant_details_data["variant_id"] ?>"  type="hidden"> <?php echo $car_variant_details_data['pro_name_comp']; ?>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <!--td> <?php echo $car_variant_details_data['mileage']; ?></td-->
                                                                    <td style="width:10%;"> 
                                                                        <div class="image-wrapper-div ">	
                                                                            <img src="<?php echo img_url('uploads/product/' . $car_variant_details_data['pro_image']); ?>"  class=""></div>
                                                                    </td>
                                                                    <td style="width:30%;">
                                                                        <div class="col-md-7">
                                                                            Ex-Showroom Price: &nbsp; 
                                                                        </div>

                                                                        <div class="col-md-5">
                                                                            <span class="strike-through">
                                                                                <i class="fa fa-rupee"></i> <?php
                                                                                echo indianFormatNumber($car_variant_details_data['exshowroom_price']);
                                                                                $car_variant_details_data[0]['exshowroom_price']
                                                                                ?>
                                                                            </span>&nbsp;
                                                                        </div>
                                                                        <br><br>    
                                                                        <div class="col-md-7">
                                                                        </div>

                                                                        <div class="col-md-5" style="margin-top:-15px">    
                                                                            <i class="fa fa-rupee"></i><span class="text-maroon"> <?php
                                                                                echo indianFormatNumber($car_variant_details_data['mnc_exshowroom_price']);
                                                                                $car_variant_details_data[1]['mnc_exshowroom_price'];
                                                                                ?></span>
                                                                        </div>    

                                                                        <!--                                                                        <div class="ex-showroom-price">
                                                                                                                                                    Ex Showroom - <span class="strike-through"> <i class="fa fa-rupee"></i><?php
                                                                        //echo indianFormatNumber($car_variant_details_data[0]['exshowroom_price']);
                                                                        //$car_variant_details_data[0]['exshowroom_price']
                                                                        ?></span>
                                                                                                                                                </div>
                                                                                                                                                <div class="text-maroon mnc-price">
                                                                                                                                                    MNC Price - <i class="fa fa-rupee"></i><?php
                                                                        //echo indianFormatNumber($car_variant_details_data[1]['mnc_exshowroom_price']);
                                                                        //$car_variant_details_data[1]['mnc_exshowroom_price'];
                                                                        ?>
                                                                                                                                                </div>-->
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            $i++;
                                                        }
                                                        ?>                                                        

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="fieldset-tr">
                                                <td class="feildset-td" colspan="3">
                                                    <i class="fa fa-caret-right"></i> Exterior
                                                </td>
                                            </tr>
                                            <tr >
                                                <td>
                                                    <table class="table table-hover table-bordered table-striped nested-table">
                                                        <?php
                                                        $i = 0;
                                                        foreach ($booking_exterior as $car_variant_details_data) {
                                                            ?>
                                                            <tr>
                                                                <td style="width:60%;"> <?php echo $car_variant_details_data['feature_name'] . ' : ' . $car_variant_details_data['feature_desc']; ?></td>
                                                                <td style="width:10%;"> 
                                                                    <div class="image-wrapper-div  ">
                                                                        <img src="<?php echo img_url('uploads/feature/' . $car_variant_details_data['feature_img']); ?>" 
                                                                              class="">
                                                                        <div>
                                                                            <i class="fa fa-check"></i>
                                                                        </div>                                                        
                                                                    </div>
                                                                </td> 
                                                                <td style="width:30%;"> <i class="fa fa-rupee"></i>
                                                                    <?php
                                                                    echo indianFormatNumber($car_variant_details_data[1]['price']);
                                                                    $car_variant_details_data['price'];
                                                                    ?></td>                                                                
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>  
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="fieldset-tr">
                                                <td class="feildset-td" colspan="3">
                                                    <i class="fa fa-caret-right"></i> Interior
                                                </td>
                                            </tr>
                                            <tr >
                                                <td>
                                                    <table class="table table-hover table-bordered table-striped nested-table">
                                                        <?php
                                                        $i = 0;
                                                        foreach ($booking_interior as $car_variant_details_data) {
                                                            ?>
                                                            <tr>
                                                                <td style="width:60%;"> <?php echo $car_variant_details_data['feature_name'] . ' : ' . $car_variant_details_data['feature_desc']; ?></td>
                                                                <td style="width:10%;">  
                                                                    <div class="image-wrapper-div  ">
                                                                        <img src="<?php echo img_url('uploads/feature/' . $car_variant_details_data['feature_img']); ?>" 
                                                                              class="">
                                                                        <div>
                                                                            <i class="fa fa-check"></i>
                                                                        </div>                                                        
                                                                    </div>
                                                                </td> 
                                                                <td style="width:30%;"> <i class="fa fa-rupee"></i><?php
                                                                    echo indianFormatNumber($car_variant_details_data['price']);
                                                                    $car_variant_details_data['price'];
                                                                    ?></td>

                                                            </tr>
                                                            <?php
                                                        }
                                                        ?> 
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="fieldset-tr">
                                                <td class="feildset-td" colspan="3">
                                                    <i class="fa fa-caret-right"></i> Accessories
                                                </td>
                                            </tr>
                                            <tr >
                                                <td>
                                                    <table class="table table-hover table-bordered table-striped nested-table">
                                                        <?php
                                                        $i = 0;
                                                        foreach ($booking_accessory as $car_variant_details_data) {
//                                                        print_r($car_variant_details_data);    
                                                            if ($car_variant_details_data['accessory_img'] == '' || $car_variant_details_data['accessory_img'] == '0') {
                                                                $img = 'no-image.png';
                                                            } else {
                                                                $img = $car_variant_details_data['accessory_img'];
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td style="width:60%;"> <?php echo $car_variant_details_data['accessory_name'] . ' : ' . $car_variant_details_data['accessory_information']; ?></td>
                                                                <td style="width:10%;"> 
                                                                    <div class="image-wrapper-div  ">
                                                                        <img src="<?php echo img_url('uploads/accessory/' . $img); ?>" class="">
                                                                        <div>
                                                                            <i class="fa fa-check"></i>
                                                                        </div>                                                        
                                                                    </div>
                                                                </td> 
                                                                <td style="width:30%;"> <i class="fa fa-rupee"></i><?php
                                                                    echo indianFormatNumber($car_variant_details_data['price']);
                                                                    $car_variant_details_data['price'];
                                                                    ?></td>

                                                            </tr>
                                                            <?php
                                                        }
                                                        ?> 
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="fieldset-tr">
                                                <td class="feildset-td" colspan="3">
                                                    <i class="fa fa-caret-right"></i> Services

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="table table-hover table-bordered table-striped nested-table">
                                                        <tr>
                                                            <td style="width:60%;">Car Finance</td>
                                                            <td style="width:10%;"> 
                                                            </td> 
                                                            <td style="width:20%;">
                                                                <?php
                                                                if ($config_breadcrub[0]['finance_id'] != '' && $config_breadcrub[0]['finance_id'] != '-') {
                                                                    ?>
                                                                    <i class="fa fa-check"></i>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>        
                                                        <tr>
                                                            <td style="width:60%;">Annual Maintenance Contract</td>
                                                            <td style="width:10%;"> 
                                                            </td> 
                                                            <td style="width:30%;">
                                                                <?php
                                                                if ($config_breadcrub[0]['amc_id'] != '' && $config_breadcrub[0]['amc_id'] != '-') {
                                                                    ?>
                                                                    <i class="fa fa-check"></i>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>        
                                                        <tr>
                                                            <td style="width:60%;">Car Insurance</td>
                                                            <td style="width:10%;"> 
                                                            </td> 
                                                            <td style="width:3%;">
                                                                <?php
                                                                if ($config_breadcrub[0]['insurance_id'] != '' && $config_breadcrub[0]['insurance_id'] != '-') {
                                                                    ?>
                                                                    <i class="fa fa-check"></i>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>        
                                                        <tr>
                                                            <td style="width:60%;">Extended Warranty</td>
                                                            <td style="width:10%;"> 
                                                            </td> 
                                                            <td style="width:30%;">
                                                                <?php
                                                                if ($config_breadcrub[0]['extended_id'] != '' && $config_breadcrub[0]['extended_id'] != '-') {
                                                                    ?>
                                                                    <i class="fa fa-check"></i>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>        
                                                        <tr>
                                                            <td style="width:60%;">Car Exchange</td>
                                                            <td style="width:10%;"> 
                                                            </td> 
                                                            <td style="width:30%;">
                                                                <?php
                                                                if ($config_breadcrub[0]['used_id'] != '' && $config_breadcrub[0]['used_id'] != '-') {
                                                                    ?>
                                                                    <i class="fa fa-check"></i>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>        
                                                        <tr>
                                                            <td style="width:60%;">Home Delivery</td>
                                                            <td style="width:10%;"> 
                                                            </td> 
                                                            <td style="width:30%;">
                                                                <?php
//                                                                print_r($config_breadcrub);
//                                                                echo $config_breadcrub[0]['home_delivery'];

                                                                if ($config_breadcrub[0]['home_delivery'] != '' && $config_breadcrub[0]['home_delivery'] != '-') {
                                                                    ?>
                                                                    <i class="fa fa-check"></i>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>        
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="technical-data">
                                <div class="table-responsive">
                                    <table class="table table-striped table-condensed comparison-table">
                                        <?php
                                        foreach ($pro_specification as $variant_details_data)
                                            
                                            ?>

                                        <tbody>
                                            <tr>
                                                <td class="feildset-td" colspan="4">
                                                    <i class="fa fa-caret-down"></i> Dimensions & Weight
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table class="table table-striped table-condensed">
                                                        <tbody>
                                                            <tr>
                                                                <td width="35%">Length(mm)</td>
                                                                <td width="15%">
                                                                    <?php
                                                                    if (!empty($variant_details_data['dimensions_mm_length'])) {
                                                                        echo $variant_details_data['dimensions_mm_length'];
                                                                    } else {
                                                                        echo '-';
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td width="25%"></td>
                                                            </tr>
                                                            <tr>
                                                                <td >Width(mm)</td>
                                                                <td class="text-left">
                                                                    <?php
                                                                    if (!empty($variant_details_data['dimensions_mm_width'])) {
                                                                        echo $variant_details_data['dimensions_mm_width'];
                                                                    } else {
                                                                        echo '-';
                                                                    }
                                                                    ?>

                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Height(mm)</td>
                                                                <td class="text-left">
                                                                    <?php
                                                                    if (!empty($variant_details_data['dimensions_mm_height'])) {
                                                                        echo $variant_details_data['dimensions_mm_height'];
                                                                    } else {
                                                                        echo '-';
                                                                    }
                                                                    ?>

                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Wheelbase(mm)</td>

                                                                <td class="text-left">
                                                                    <?php
                                                                    if (!empty($variant_details_data['dimensions_mm_weelbase'])) {
                                                                        echo $variant_details_data['dimensions_mm_weelbase'];
                                                                    } else {
                                                                        echo '-';
                                                                    }
                                                                    ?>

                                                                </td>

                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Ground Clearance(mm)</td>
                                                                <td class="text-left">
                                                                    <?php
                                                                    if (!empty($variant_details_data['dimensions_mm_ground_clearance'])) {
                                                                        echo $variant_details_data['dimensions_mm_ground_clearance'];
                                                                    } else {
                                                                        echo '-';
                                                                    }
                                                                    ?>

                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Kerb Weight(kg)</td>
                                                                <td class="text-left">
                                                                    <?php
                                                                    if (!empty($variant_details_data['kerb_weight'])) {
                                                                        echo $variant_details_data['kerb_weight'];
                                                                    } else {
                                                                        echo '-';
                                                                    }
                                                                    ?>

                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="feildset-td" colspan="4">
                                                    <i class="fa fa-caret-down"></i> Capacity
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table class="table table-striped table-condensed">
                                                        <tr>
                                                            <td width="25%">Seating Capacity(Person)</td>
                                                            <td width="25%">
                                                                <?php
                                                                if (!empty($variant_details_data['body_seating_capacity'])) {
                                                                    echo $variant_details_data['body_seating_capacity'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td width="25%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Doors(Doors)</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['body_no_of_doors'])) {
                                                                    echo $variant_details_data['body_no_of_doors'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Number of Seating Rows(Rows)</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['no_seating_rows'])) {
                                                                    echo $variant_details_data['no_seating_rows'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>


                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bootspace(liters)</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['bootspace'])) {
                                                                    echo $variant_details_data['bootspace'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fuel Tank Capacity(liters)</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['fuel_tank_capacity1'])) {
                                                                    echo $variant_details_data['fuel_tank_capacity1'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="feildset-td" colspan="4">
                                                    <i class="fa fa-caret-down"></i> Engine & Transmission
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table class="table table-striped table-condensed">
                                                        <tr>
                                                            <td width="25%">Engine Type</td>
                                                            <td width="25%">
                                                                <?php
                                                                if (!empty($variant_details_data['engine_types'])) {
                                                                    echo $variant_details_data['engine_types'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td width="25%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Displacement(cc)</td>
                                                            <td >
                                                                <?php
                                                                if (!empty($variant_details_data['displacement'])) {
                                                                    echo $variant_details_data['displacement'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fuel Type</td>

                                                            <td><?php
                                                                if ($pro_details[0]['fuel_type'] == 1) {
                                                                    echo 'Petrol';
                                                                } else if ($pro_details[0]['fuel_type'] == 2) {
                                                                    echo 'Diesel';
                                                                } else if ($pro_details[0]['fuel_type'] == 3) {
                                                                    echo 'CNG';
                                                                } else if ($pro_details[0]['fuel_type'] == 4) {
                                                                    echo 'Electric';
                                                                } else {
                                                                    
                                                                }
                                                                ?></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Power(bhp@rpm)</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['engine_maximum_power'])) {
                                                                    echo $variant_details_data['engine_maximum_power'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Max Torque(Nm@rpm)</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['engine_maximum_torque'])) {
                                                                    echo $variant_details_data['engine_maximum_torque'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>                                                
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Mileage(ARAI)(kmpl)</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['mileage'])) {
                                                                    echo $variant_details_data['mileage'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Alternate Fuel</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['alternate_fuel'])) {
                                                                    echo $variant_details_data['alternate_fuel'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bore(mm) x Stroke(mm)</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['bore_stroke'])) {
                                                                    echo $variant_details_data['bore_stroke'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Compression Ratio</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['comparision_ratio'])) {
                                                                    echo $variant_details_data['comparision_ratio'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>


                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Valve/Cylinder (Configuration)</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['engine_no_of_valves'])) {
                                                                    echo $variant_details_data['engine_no_of_valves'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Cylinders</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['engine_no_of_cylinder'])) {
                                                                    echo $variant_details_data['engine_no_of_cylinder'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fuel System</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['fuel_system'])) {
                                                                    echo $variant_details_data['fuel_system'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Transmission Type</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['tramission_type'])) {
                                                                    echo $variant_details_data['tramission_type'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>No of gears(Gears)</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['no_gear'])) {
                                                                    echo $variant_details_data['no_gear'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Dual Clutch</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if ($variant_details_data['dual_cluth'] == 1) {
                                                                    ?>
                                                                    <i class="fa fa-check"></i>
                                                                    <?php
                                                                } else if ($variant_details_data['dual_cluth'] == 2) {
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <?php
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Sport Mode</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if ($variant_details_data['sport_mode'] == 1) {
                                                                    ?>
                                                                    <i class="fa fa-check"></i>
                                                                    <?php
                                                                } else if ($variant_details_data['sport_mode'] == 2) {
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <?php
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>
                                                            </td>                                                
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Drivetrain</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['drivetrain'])) {
                                                                    echo $variant_details_data['drivetrain'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="feildset-td" colspan="4">
                                                    <i class="fa fa-caret-down"></i> Suspensions, Brakes, Steering & Tyres
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table class="table table-striped table-condensed">
                                                        <tr>
                                                            <td width="25%">Suspension Front</td>
                                                            <td width="25%">
                                                                <?php
                                                                if (!empty($variant_details_data['suspension_front'])) {
                                                                    echo $variant_details_data['suspension_front'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td width="25%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Suspension Rear</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['suspension_rear'])) {
                                                                    echo $variant_details_data['suspension_rear'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Front Brake Type</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['front_tyres'])) {
                                                                    echo $variant_details_data['front_tyres'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>


                                                            </td>
                                                            <td></td>
                                                        </tr>

                                                        <tr>
                                                            <td>Streeing Type</td>
                                                            <td>
                                                                <?php
                                                                if (!empty($variant_details_data['streeing_type'])) {
                                                                    echo $variant_details_data['streeing_type'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Minimum Turning Radius(metres)</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['streeing_turn_radius'])) {
                                                                    echo $variant_details_data['streeing_turn_radius'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Front tyres</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['front_tyres'])) {
                                                                    echo $variant_details_data['front_tyres'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Rear tyres</td>
                                                            <td class="text-left">
                                                                <?php
                                                                if (!empty($variant_details_data['rear_tyre'])) {
                                                                    echo $variant_details_data['rear_tyre'];
                                                                } else {
                                                                    echo '-';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="std-equipment">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered table-striped car-booking-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($pro_feture as $pro_feture_data) {
                                                if ($pro_feture_data['feature_name'] != 'Color' && $pro_feture_data['feature_name'] != 'leather' && $pro_feture_data['feature_name'] != 'wheel' && $pro_feture_data['feature_name'] != 'cloth' && $pro_feture_data['feature_name'] != 'dashboard') {
                                                    ?>
                                                    <tr>
                                                        <td><?php print_r($pro_feture_data['feature_name']); ?></td>
                                                        <td>
                                                            <?php
                                                            if (!empty($pro_feture_data['feature_img'])) {
                                                                ?>
                                                                <img width="50" src="<?php echo img_url('uploads/feature/' . $pro_feture_data['feature_img']); ?>"/>
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php
                                                            if (strtoupper($pro_feture_data['feature_desc']) == 'Y') {
                                                                ?>
                                                                <i class="fa fa-check"></i>
                                                                <?php
                                                            } else if (strtoupper($pro_feture_data['feature_desc']) == 'N') {
                                                                ?>
                                                                <i class="fa fa-times"></i>
                                                                <?php
                                                            } else {
                                                                echo $pro_feture_data['feature_desc'];
                                                            }

//                                                            print_r($pro_feture_data['feature_desc']);
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <?php
//                                            echo '<pre>';
//                                            print_r($pro_features);
//                                            echo '</pre>';
                                            ?>
                                            <?php
                                            foreach ($pro_feture as $pro_feture_data) {
                                                if (in_array($pro_feture_data['feature_id'], $exp_services)) {
                                                    ?>
                                                    <tr>
                                                        <td><?php print_r($pro_feture_data['feature_name']); ?></td>
                                                        <td>
                                                            <?php
                                                            if (!empty($pro_feture_data['feature_img'])) {
                                                                ?>
                                                                <img width="50" src="<?php echo img_url('uploads/feature/' . $pro_feture_data['feature_img']); ?>" />
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php print_r($pro_feture_data['feature_desc']); ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="clearfix">
                <div class="pull-right" style="display:none;">
                    <a class="btn btn-lg btn-maroon" href="<?php echo site_url('payment-page/' . $main_id); ?>">Book My New Car</a>
                </div> 
            </div>
        </div>
    </div>
</div>

<div id="social-share" class="hide">
    <a href="#"><i class="fa fa-facebook"></i></a>
    <a href="#"><i class="fa fa-twitter"></i></a>
    <a href="#"><i class="fa fa-linkedin"></i></a>
    <a href="#"><i class="fa fa-google-plus"></i></a>
</div>
<div class="modal fade" id="email_to_friend">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Send PDF to email</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form role="search" class="navbar-form navbar-left topbar-search" method="POST" action="<?php echo site_url('stitcher/save_mail_pdf'); ?>">
                            <div class="form-group">
                                <input type="text" placeholder="Email Id" name="email_id" class="form-control">
                                <input type="hidden" placeholder="" name="pro_id" value="<?php echo $id; ?>" class="form-control">
                            </div>
                            <button class="btn text-center" type="submit">Send to mail!</i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
