<div class="container">
    <div class="row">
        <div class="col-md-12 no_carousel" align="center">
            <p align="center"><span style="font-size: x-large;"><b>THANK YOU!</b></span></p>
            <p align="center"><span style="font-size: large;">We appreciate your efforts to enquire with MYNEWCAR.IN. <br>Our Customer Advisor will call you shortly to help you book your new car.</span></p>
            <p align="center">Have a minute? Help us share the love! Follow us on <a href="https://twitter.com/mynewcarindia">Twitter</a> or like us on <a href="https://www.facebook.com/mynewcar.in">Facebook</a> to keep you up to date with all our news and announcements.</p>
            
        </div>
    </div>
</div>
<script>
    thank_you();
    function thank_you()
    {
        setTimeout(function () {
            window.location.href = '<?php echo base_url(); ?>'; // the redirect goes here
        }, 60000);
    }
</script>