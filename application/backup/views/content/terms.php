<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Terms Of Offer</h1>    
        </div>
        <div class="col-md-12">
<p align="justify">
These terms of offer for sale (<strong>"Terms of Offer For Sale"</strong>) between Dreamz Mynewcar India Private Limited (hereinafter referred to as the<strong>"Company"</strong>) and the users of the Website (<strong>"You"</strong> or <strong>"Your"</strong> or <strong>"Yourself"</strong> or<strong>"User"</strong>) describe, inter alia, the terms of offer for sale, purchase of goods and services on the Website, www.mynewcar.in (    <strong>"Website"</strong>).
    <br/>
    <br/>
    PLEASE READ THE TERMS OF OFFER FOR SALE CAREFULLY BEFORE PURCHASING ANY PRODUCTS OR AVAILING ANY SERVICES ON THE WEBSITE. ANY PURCHASE MADE BY YOU THROUGH
    THE WEBSITE OF THE PRODUCTS/ SERVICES SHALL SIGNIFY YOUR ACCEPTANCE OF THE TERMS OF OFFER FOR SALE AND YOUR AGREEMENT TO BE LEGALLY BOUND BY THE SAME.
</p>
<p align="justify">
    <br/>
    IN ADDITION TO THE FOREGOING, YOU SHALL ALSO BE BOUND BY THE TERMS OF USE OF THE WEBSITE, TERMS OF OFFER FOR SALE ISSUED BY THE VENDOR OR ADDITIONAL TERMS
    OF SERVICE WHICH ARE DISPLAYED WITH THE SELECTION OF THE PRODUCT, IF ANY (<strong>"ADDITIONAL TERMS"</strong>) AT THE WEBSITE. IF THERE IS ANY CONFLICT
    BETWEEN THE TERMS OF OFFER FOR SALE AND THE ADDITIONAL TERMS, THE ADDITIONAL TERMS SHALL TAKE PRECEDENCE IN RELATION TO THAT SALE.
</p>
<p align="justify">
    <br/>
    <strong>IF YOU DO NOT AGREE WITH THE TERMS OF OFFER FOR SALE, PLEASE DO NOT ACCESS THE WEBSITE FOR ANY PURCHASE.</strong>
</p>
<ol>
    <li>
        <p align="justify">
            <strong>Business </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
The Website is a platform that facilitates the online sale and purchase of cars, accessories, products and merchandise (“                <strong>Product/s</strong>”) and other services (<strong>"Services"</strong>) offered by the Company's various registered
                merchants/vendors/service providers (<strong>"Vendor/s"</strong>). The Vendors are the sellers of products and services on the Website and will
                be solely responsible to You for the products sold through the Website by the Vendors. You should take all responsibility for your own actions
                in purchasing the Product/s and Services by You from the Vendor/s and the Company shall not be liable for any such action.
            </p>
        </li>
        <li>
            <p align="justify">
                You acknowledge that the Products or Services being offered or displayed on the Website are not owned by the Company as the Company is only an
                inter-mediator between You and the Vendors. Orders of Products and Services are solely processed, transmitted and delivered by the respective
                Vendor who has the final right of accepting or rejecting the orders. The Company, being an aggregator, shall not be liable for any such action.
            </p>
        </li>
        <li>
            <p align="justify">
                You acknowledge that the Company has the right to change or discontinue any Product or Service at any time, without notice. You further
                acknowledge that the Company may add or modify the procedures, modes, processes or conditions of purchase at any time to adapt to changes that
                the Company may make to the Product or Services. You agree that the Company shall not be liable to You or to any third party for any
                modification, suspension or discontinuance of any aspect of the Product or Services.
            </p>
        </li>
    </ol>
</ol>
<ol start="2">
    <li>
        <p align="justify">
            <strong>Product </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                The Company, through the Website, makes available a variety of Products of the Vendors at an agreed price to You. Purchase of such Product/s
                will be subject to the Terms of Offer For Sale and such other additional terms as specified by the Vendor/s.
            </p>
        </li>
        <li>
            <p align="justify">
                The Vendors offer shipping for all the Products on the Website as per their policy, which may be changed by the them without any notice to You.
                The Company, being an aggregator, shall not be responsible for any shipping of the Product/s.
            </p>
        </li>
        <li>
            <p align="justify">
                All the products are governed by the terms of warranties provided by the respective manufacturer/brands/Vendors. However, in case any
                Product/Service is covered under seller warranty, it shall be specifically mentioned under the product details given by the respective Vendor.
            </p>
        </li>
        <li>
            <p align="justify">
                The price of Products offered on the Website is either equal to or lesser than the Maximum Retail Price (<strong>"MRP"</strong>) i.e., the
                discounted rate prescribed for those Products. The MRP and other statutory declarations shall be mentioned on the Products and/or on its
                packaging in accordance with applicable laws.
            </p>
        </li>
        <li>
            <p align="justify">
                In the event You purchase any electronic product/s from the Vendors, the Company shall not be liable for any fault arising from these
                electronic product/s. You are requested to visit the nearest service station as per user manual, if any or as indicated by the Vendor/s for any
                default in the electronic product/s. In case if there is no service station located close to you, then, in such circumstances, the Company
                shall recall the product/s upon your request and send it to the service station on Your behalf. The personnel of service station shall directly
                get in touch with You for the cost incurred.
            </p>
        </li>
        <li>
            <p align="justify">
                The Company does not offer any refunds against any Product already purchased from the Website unless, any error has occurred during the
                purchase of such Product which is directly attributable to the Company.
            </p>
        </li>
    </ol>
</ol>
<ol start="3">
    <li>
        <p align="justify">
            <strong>Services </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                You should take all responsibility for your own actions in utilizing the Services purchased by You and the Company shall not be liable for any
                such action.
            </p>
        </li>
        <li>
            <p align="justify">
                You represents that You are of legal age to form a binding contract with the Vendor and are not a person barred from receiving Services under
                the laws as applicable in India.
            </p>
        </li>
        <li>
            <p align="justify">
                You confirm that the Company shall not be responsible for any deficiency in payment of consideration payable towards the Services purchased
                from the Website.
            </p>
        </li>
        <li>
            <p align="justify">
                The Company does not offer any refunds against any Services already purchased from the Website unless, any error has occurred during the
                purchase of such Services which is directly attributable to the Company.
            </p>
        </li>
    </ol>
</ol>
<ol start="4">
    <li>
        <p align="justify">
            <strong>Pricing Information</strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                The Company strives to provide You with the best prices possible on Products and Services You buy or avail of from the Website. However, the
                Company does not guarantee that the price will be the lowest in the city, region or geography. Prices and availability are subject to change
                without notice or any consequential liability to You.
            </p>
        </li>
        <li>
            <p align="justify">
                While the Company strives to provide accurate Products, Services and pricing information, typographical and other errors may occur. In the
                event that a Product or Service is listed at an incorrect price or with incorrect information due to an error in pricing or product or service
                information, the Vendor may, at its discretion, either contact You for instructions or cancel Your order and notify You of such cancellation.
                The Company and/or the Vendor will have the right to modify the price of the Product or Service and contact You for further instructions via
                e-mail address provided by You at the time of registration, or cancel the order and notify You of such cancellation. If the Vendor cancels the
                order after the payment has been processed, the said amount will be remitted by the Vendor to Your account from which the payment was made.
            </p>
        </li>
    </ol>
</ol>
<ol start="5">
    <li>
        <p align="justify">
            <strong>Credit/Debit Card Details</strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                In certain cases, specifically with regard to particular Products/ Services, You might be required to provide Your credit or debit card details
                to the approved payment gateways while making the payment. In this regard You agree to provide correct and accurate credit/debit card details
                to the approved payment gateways for availing services on the Website. You shall not use the credit/ debit card which is not lawfully owned by
                You, i.e. in any transaction, You must use Your own credit/debit card. The information provided by You will not be utilized or shared with any
                third party unless required in relation to fraud verifications or by law, regulation or court order. You will be solely responsible for the
                security and confidentiality of Your credit/debit card details. The Company expressly disclaims all liabilities that may arise as a consequence
                of any unauthorized use of Your credit/ debit card.
            </p>
        </li>
    </ol>
</ol>
<ol start="6">
    <li>
        <p align="justify">
            <strong>Delivery of the Product </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                Your shipping address, pin code will be verified with the database of the Company before You proceed to pay for Your purchase. In the event
                case Your order is not serviceable by our delivery partners or the merchant or the area is not covered, we would request You to provide us with
                an alternate shipping address which we expect to have on our Vendor's delivery list. In case there is any dispute regarding the shipment of the
                Product or Services for the area not covered by the Vendor, in such cases the Company will not be responsible for the non-delivery of the
                Product or Service. The Company/the Vendor do not deliver items internationally. However, You can make purchases on the Website from anywhere
                in the world but at the same time ensuring the shipping address is within India.
            </p>
        </li>
        <li>
            <p align="justify">
                In case You book multiple order for the Products and Services in one transaction, the Vendor would endeavour to ship all Products together.
                However, this may not always be possible due to some Product characteristics and/or logistics' issues. If You purchase multiple Products in
                single transaction, then all the Products would be dispatched to a single shipping address given by You. If You wish to ship Products to
                different addresses, then You should book the orders separately based on the delivery addresses.
            </p>
        </li>
    </ol>
</ol>
<ol start="7">
    <li>
        <p align="justify">
            <strong>Return Policy </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                In the event You receive a damaged/defective Product or a Product that does not comply with the specifications as per your original order, You
                are required to get in touch directly with the Vendor.
            </p>
        </li>
        <li>
            <p align="justify">
                Upon receiving Your complaint, the Vendor shall verify the authenticity and the nature of the complaint and if the Vendor is convinced that the
                complaint is genuine, the Vendor will be responsible for a replacement. However, in the event of frivolous and baseless complaints regarding
                the quality and content of the Products, the Vendor reserves the right to take necessary legal actions against You and You will be solely
                liable for all costs incurred by the Vendor in this regard. You expressly acknowledge that the Vendor selling the defective Product/Service
                will be solely responsible to You for any claims that You may have in relation to such defective product/service and the Company shall not in
                any manner be held liable for the same.
            </p>
        </li>
        <li>
            <p align="justify">
                Before accepting shipment of any Product, kindly ensure that the Product's packaging is not damaged or tampered. If You observe that the
                package is damaged or tampered, request You to refuse to accept delivery and inform the respective Vendor or the Company at the earliest. The
                return process of the Product may be restricted by the Vendor depending on the nature and category of the Product.
            </p>
        </li>
        <li>
            <p align="justify">
                In order to return any Products sold through the Website, You are required to comply with the below mentioned conditions, viz:
            </p>
        </li>
    </ol>
</ol>
<ol type="i">
    <li>
        <p align="justify">
            Please notify the Company or the respective Vendor of receipt of a damaged/defective product/service within 48-hours of delivery to You. If You are
            unable to do so within 48-hours, the Company shall not be held liable for the failure to replace the order;
        </p>
    </li>
    <li>
        <p align="justify">
            Products should be unused;
        </p>
    </li>
    <li>
        <p align="justify">
            The Vendor may arrange pick-up of the damaged/defective product through its own logistics partner. In the event the Vendor is unable to do so, the
            Vendor will notify You regarding the same and You will be required to dispatch the product using a reputed courier in Your respective area within
            one (1) day from receipt of such notice. Courier freight charges will be reimbursed in such form as is determined by the logistics team upon prior
            consultation with the Vendor;
        </p>
    </li>
    <li>
        <p align="justify">
            Products should be returned in their original packaging along with the original price tags, labels, barcodes, user manual, warranty card and
            invoices etc.;
        </p>
    </li>
    <li>
        <p align="justify">
            It is advised that the return packets should be strongly and adequately packaged so that there is no further damage of products during transit;
        </p>
    </li>
    <li>
        <p align="justify">
            The returned products are subject to verification and checks by the Vendor in order to determine the legitimacy of the complaint/ return.
        </p>
    </li>
</ol>
<ol>
    <ol start="5">
        <li>
            <p align="justify">
                In the event the return of a Product is duly accepted by the Vendor, the value of such product, as originally paid by You during acceptance of
                delivery of product or otherwise, will be refunded to You by the Vendor. Refund will be processed based on the mode of payment and the Vendor
                or the approved payment gateway will credit Your refunds directly into Your debit/credit card or online accounts or provide You with a cheque
                in this regard. Refunds will be subject to the following:
            </p>
        </li>
    </ol>
</ol>
<ol type="i">
    <li>
        <p align="justify">
            Orders paid online will be refunded within 7-21 working days through the online account or via cheque, depending on the then current circumstances
            as determined by the Vendor;
        </p>
    </li>
</ol>
<ol start="2" type="i">
    <li>
        <p align="justify">
            For Cash on Delivery payments, You will be provided with a refund cheque.
        </p>
    </li>
</ol>
<ol start="3" type="i">
    <li>
        <p align="justify">
            If the Product can be repaired by the service centre, the Vendor would get the same repaired and send it back to You.
        </p>
    </li>
</ol>
<ol start="4" type="i">
    <li>
        <p align="justify">
            A promo code, once used shall not be refunded in case of cancellation of order either by customer or the Vendor.
        </p>
    </li>
</ol>
<ol start="8">
    <li>
        <p align="justify">
            <strong>Cancellation </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                <strong>Cancellation by the Vendor:</strong>
                There may be certain orders that the Vendor is unable to accept and must cancel. The Vendor reserves the right, at its sole discretion, to
                refuse or cancel any order for any reason whatsoever. Some situations that may result in Your order being cancelled include, without
                limitation, non-availability of the Product or quantities ordered by You, non-availability of the Service, inaccuracies or errors in pricing
                information, or problems identified by the Company's credit and fraud avoidance department. The Company or the Vendor as the case may be may
                also require additional verifications or information before accepting any order. The Vendor will contact You if all or any portion of Your
                order is cancelled or if additional information is required to accept Your order. If Your order is cancelled after Your credit card has been
                charged, the said amount will be reversed back in Your credit card account. A promo code, once used shall not be refunded in case of
                cancellation of order either by Customer or the Vendor.
            </p>
        </li>
        <li>
            <p align="justify">
                <strong>Cancellation by the User</strong>
                : In case of requests for order cancellations, the Vendor/s reserve the right to accept or reject requests for order cancellations for any
                reason whatsoever. As part of usual business practice, if the Vendor receives a cancellation notice and the order has not been
                processed/approved by the Vendor, the said Vendor shall cancel the order and refund the entire amount to You within a reasonable period of
                time. The Vendor may not be able to cancel orders that have already been processed. The Vendor has the full right to decide whether an order
                has been processed or not. You agree not to dispute the decision made by the Vendor and accept Vendor's decision regarding the cancellation.
            </p>
        </li>
        <li>
            <p align="justify">
                The Vendor reserves the right to cancel any orders that classify as 'Bulk Order' as determined by the Vendor as per certain criteria. Any
                reward point or promo code used for placing the 'Bulk Order' will not be refunded as per this cancellation policy. An order can be classified
                as 'Bulk Order' if it meets with the below mentioned criteria, which may not be exhaustive, viz:
            </p>
        </li>
    </ol>
</ol>
<ol type="i">
    <li>
        <p align="justify">
            Products ordered are not for self-consumption but for commercial resale
        </p>
    </li>
    <li>
        <p align="justify">
            Multiple orders placed for same product at the same address
        </p>
    </li>
    <li>
        <p align="justify">
            Bulk quantity of the same product ordered
        </p>
    </li>
    <li>
        <p align="justify">
            Invalid address given in order details
        </p>
    </li>
    <li>
        <p align="justify">
            Any malpractice used to place the order
        </p>
    </li>
</ol>
<ol>
    <ol start="4">
        <li>
            <p align="justify">
                <strong>Failure to Sell by the Vendor: </strong>
                Unless due to Your negligence, in the event the Vendor fails to complete a sale of a Product or a Service when presented by You with a valid
                voucher or otherwise, You acknowledge that the Vendor will be solely liable to reimburse such amount as has already been paid by You for the
                purchase of such order. Further, You agree that the Company will not be held liable for failure of the Vendor to make available any product or
                service therein.
            </p>
        </li>
        <li>
            <p align="justify">
                <strong>Links to Third Party sites: </strong>
                The Website may contain links to other websites ("Linked Sites"). The Linked Sites are not under the control of the Company or the Website and
                the Company is not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any
                changes or updates to a Linked Site. The Company is not responsible for any form of transmission, whatsoever, received by you from any Linked
                Site. The Company is providing these links to You only as a convenience, and the inclusion of any link does not imply endorsement by the
                Company or the Website of the Linked Sites. The users are requested to verify the accuracy of all information on their own before undertaking
                any reliance on such information.
            </p>
        </li>
    </ol>
</ol>
<ol start="9">
    <li>
        <p align="justify">
            <strong>Disclaimer </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                THE COMPANY DOES NOT CONTROL, ENDORSE OR ACCEPT RESPONSIBILITY FOR ANY PRODUCT (INCLUDING BUT NOT LIMITED TO PRODUCT CATALOGUES) OR SERVICES
                OFFERED BY VENDORS OR BY THIRD PARTIES ACCESSIBLE THROUGH THE WEBSITE OR ANY LINKED SITES. THE COMPANY MAKES NO REPRESENTATIONS OR WARRANTIES
                WHATSOEVER ABOUT, AND SHALL NOT BE LIABLE FOR, THE VENDOR OR ANY SUCH THIRD PARTIES, THEIR PRODUCTS OR SERVICES, INCLUDING REPRESENTATIONS
                RELATING TO NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY RIGHTS. ANY TRANSACTIONS THAT YOU MAY HAVE WITH SUCH THIRD PARTIES ARE AT
                YOUR OWN RISK. THE PRODUCTS AND SERVICES SHALL BE SUBJECT TO VENDOR'S TERMS AND CONDITIONS FOR WARRANTY, SERVICE AND MAINTENANCE, AND THE
                COMPANY DOES NOT ACCEPT ANY RESPONSIBILITY FOR THE SAME. THE COMPANY ALSO DOES NOT ACCEPT ANY RESPONSIBILITY FOR THE USAGE OF THE PRODUCTS OR
                SERVICES BY YOU.
            </p>
        </li>
        <li>
            <p align="justify">
                THE COMPANY DISCLAIMS ANY LIABILITY WITH REGARD TO ANY DEFECTIVE OR COUNTERFEIT PRODUCTS PURCHASED OR SERVICE AVAILED BY YOU FROM THE VENDOR/S
                AND THE COMPANY SHALL NOT ASSUME ANY LIABILITY IF THE PRODUCT PURCHASED OR SERVICE AVAILED BY YOU FROM THE VENDOR/S IS NOT EXACTLY AS PER
                SPECIFICATIONS DETAILED IN THE PURCHASE CONFIRMATION ORDER.
            </p>
        </li>
    </ol>
</ol>
<ol start="10">
    <li>
        <p align="justify">
            <strong>Indemnification and Limitation of Liability </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                You agree to indemnify, defend and hold harmless the Company from and against any and all losses, liabilities, claims, damages, demands, costs
                and expenses (including legal fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred by
                the Company that arise out of, result from, or may be payable by virtue of, any breach of any representation, warranty, covenant or agreement
                made or obligation to be performed by You pursuant to these Terms of Offer For Sale or any additional terms applicable to the purchase of
                products and services on the Website.
            </p>
        </li>
        <li>
            <p align="justify">
                In no event shall the Company, its officers, shareholders, directors, employees, partners or vendors be liable to You, the Vendor or any third
                party for any special, incidental, indirect, consequential or punitive damages whatsoever, including those resulting from loss of use, data or
                profits, whether or not foreseeable or whether or not the Company has been advised of the possibility of such damages, or based on any theory
                of liability, including breach of contract or warranty, negligence or other tortious action, or any other claim arising out of or in connection
                with Your purchase of the products and services herein.
            </p>
        </li>
        <li>
            <p align="justify">
                The Company shall not assume any liability for the non-availability of the Product, delivery of the Product and the installation of the Product
                where required or any action or inaction that might be taken by the Vendor after issue of the voucher to the user.
            </p>
        </li>
    </ol>
</ol>
<ol start="11">
    <li>
        <p align="justify">
            <strong>Governing Law </strong>
        </p>
    </li>
</ol>
<ol>
    <ol>
        <li>
            <p align="justify">
                These Terms of Offer For Sale and the relationship between You and the Company shall be
                governed in accordance with the laws of India without reference to conflict of laws principles.
            </p>
        </li>
        <li>
            <p align="justify">
                You agree that all claims, differences and disputes arising under or in connection with or in relation to the Terms of Offer For Sale or any
                transactions entered into on or through the Website or the relationship between You and the Company shall be subject to the exclusive
                jurisdiction of the courts at Gurgaon, NCR – Delhi and You hereby accede to and accept the jurisdiction of such courts.
            </p>
        </li>
    </ol>
</ol>
<p align="justify">
    <br/>
    ALL LINKED TERMS AND CONDITIONS HEREUNDER ARE ASSUMED TO BE READ, UNDERSTOOD AND AGREED BY YOU.
</p>


        </div>
    </div>
</div>