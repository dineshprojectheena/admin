<div class="container" id="product_filter">
    <div class="row">
        <div class="col-md-12">
            <h1>Car Search By Type or Price</h1>    
        </div>        
        <div class="col-md-3 col-cust-left">            
            <div class="panel-group" id="selection-list">
                <div class="panel">
                    <div class="accordian-header">
                        <a data-toggle="collapse" href="#collapseOne">
                            <h5>
                                <i class="fa fa-tags"></i>
                                Brand
                                <span class="accordian-open-close">
                                    <i class="fa fa-<?php
                                    if ($pro_brand != '') {
                                        echo 'minus';
                                    } else {
                                        echo 'plus';
                                    }
                                    ?>"></i>
                                </span>
                            </h5>
                        </a>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse <?php
                    if ($pro_brand != '') {
                        echo 'in';
                    }
                    ?>">
                        <div class="panel-body brand-list" >
                            <ul class="col-sx-12 col-md-12">
                                <li class="col-sx-12 col-md-12">
                                    <label>
                                        <span>
                                            <a  id="brand_check" onclick="$('#brand_id').val(''), $('#model_id').val(''), $('#model_id').val(''), $('#b1').css('display', 'none'), car_type_selection(i = '0'), $('#car_brand_result').html(''), $('#model_id').val(), $('#car_brand_result').html('Specific Brand First');" >
                                                <input type="radio" class="brand-checkbox" align="center" name="brand_id" id="brand_all"  value="0" data-name="0">
                                            </a>
                                        </span>
                                    </label>    
                                    <span >
                                        All
                                    </span>
                                </li>
                                <?php
                                foreach ($brand_res as $brand_res_data) {
                                    ?>
                                    <li class="col-sx-12 col-md-12">
                                        <?php
                                        if ($pro_brand == $brand_res_data['brand_name']) {
                                            $main_brand_id = "'" . $brand_res_data['brand_id'] . "'";
                                            $check_brand = 'checked';
                                        } else {
                                            $check_brand = '';
                                        }
                                        ?>

                                        <label>
                                            <span>
                                                <a  onclick="car_type_selection(i = '5');
                                                        loadcss();">
                                                    <input type="radio" <?php echo $check_brand; ?> class="brand-checkbox" align="center" name="brand_id" value="<?php echo $brand_res_data['brand_name']; ?>" data-name="<?php echo $brand_res_data['brand_id']; ?>">
                                                </a>
                                            </span>
                                        </label>    
                                        <span id="brand_data<?php echo $brand_res_data['brand_id']; ?>" >
                                            <?php echo $brand_res_data['brand_name']; ?>
                                        </span>

                                    </li>
                                    <?php
                                }
                                ?>
                                <input type="hidden" id="brand_id" name="brand_id" value="<?php echo $main_brand_id; ?>"/>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="panel">
                    <div class="accordian-header">
                        <a data-toggle="collapse" href="#collapseZeroOne" <?php
                        if(empty($pro_model)) {
                            echo 'class="collapsed"';
                        }
                        ?>>
                            <h5>
                                <i class="fa fa-tags"></i>
                                Model
                                <span class="accordian-open-close">
                                    <i class="fa fa-<?php
//                                    echo $pro_model;
                                    if ($pro_model != '') {
                                        echo 'minus';
                                    } else {
                                        echo 'plus';
                                    }
                                    ?>"></i>
                                </span>
                            </h5>
                        </a>
                    </div>
                    <div id="collapseZeroOne" class="panel-collapse collapse <?php
                    if (!empty($pro_model)) {
                        echo ' in';
                    }
                    ?>">
                        <div class="panel-body brand-list" >
                            <div id="car_brand_result"></div>
                            <input type="hidden" id="model_id" name="model_ids" value="<?php echo $pro_model; ?>"/>
                            <input type="hidden" id="brand_id2" name="brand_id2" value="<?php echo preg_replace('/[^0-9\-]/', '', $main_brand_id); ?>"/>
                        </div>
                    </div>
                </div>
                
                <div class="panel">
                    <div class="accordian-header">
                        <a data-toggle="collapse"  href="#collapseTwo">
                            <h5>
                                <i class="fa fa-filter"></i>
                                Fuel Type
                                <span class="accordian-open-close">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </h5>
                        </a>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row clickable-div">
                                <div class="col-md-12 col-sm-12 col-xs-12" >
                                <div class="col-md-12 col-sm-12 col-xs-12 hidden-center" align="center">
                                            <label for="fuel">
                                            <span>
                                                <a onclick="$('#b1').css('display', 'none'), $('#fuel_id').val(''), car_type_selection(i = '0');">
                                                    <input type="radio" name="fuel" id="fuel_type_data" class="fuel-checkbox" align="center">
                                                </a>
                                            </span>
                                        </label>
                                        <label>&nbsp;<span>All</span></label>                                        
                                        
                                </div>
                                <?php
                                $pet_types = array(array('id' => '1', 'type' => 'Petrol'), array('id' => '2', 'type' => 'Diesel'),
                                    array('id' => '3', 'type' => 'CNG'), array('id' => '4', 'type' => 'Electric'), array('id' => '5', 'type' => 'LPG'));
                                for ($i = 0; $i <= count($pet_types) - 1; $i++) {
                                ?>
                                <div class="col-md-6 col-sm-6 col-xs-6" align="<?php if($i%2==0) { echo 'left';} else { echo 'right';} ?>">
                                        <label>
                                            <span>
                                                <a onclick="$('#fuel_id').val(<?php echo $pet_types[$i]['id']; ?>), check_fuels();">
                                                    <img class="img-center fuel_icon" src="<?php echo base_url() . 'assets/img/fuel/petrol-pump-symbol-md.png' ?>">
                                                    <input type="radio"  class="fuel-checkbox pull-left" align="center" name="fuel" data-name="<?php echo $pet_types[$i]['id']; ?>" data-value="<?php echo $pet_types[$i]['type']; ?>" align="center" id="<?php echo 'fuel' . $pet_types[$i]['id']; ?>" value="<?php echo $pet_types[$i]['id']; ?>">
                                                </a>
                                            </span>
                                        </label>
                                        <br>
                                        <label >&nbsp;<span id="<?php echo 'fuel_data' . $pet_types[$i]['id']; ?>"><?php echo $pet_types[$i]['type']; ?></span></label>                                        
                                </div>                                   
                                <?php
                                }
                                ?>
                                    
                                <input type="hidden" id="fuel_id" name="fuel_id" />
                                <input type="hidden" id="fuel_id_data" name="fuel_id_data" />                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel">
                    <div class="panel-heading accordian-header">
                        <a data-toggle="collapse" href="#collapseThree">
                            <h5>
                                <i class="fa fa-leaf"></i>
                                Car Type
                                <span class="accordian-open-close">
                                    <i class="fa fa-<?php
                                    if ($pro_type != '') {
                                        echo 'minus';
                                    } else {
                                        echo 'plus';
                                    }
                                    ?>"></i>
                                </span>
                            </h5>
                        </a>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse <?php
                    if ($pro_type != '') {
                        echo 'in';
                    }
                    ?>">
                        <div class="panel-body">
                            <div class="row clickable-div">
                            <div class="col-md-12 col-sm-12 col-xs-12" >    
                                <div class="col-md-12 col-sm-12 col-xs-12 hidden-center" align="center">
                                    <a class="car-type-thumb-a" onclick="$('#pro_type').val(''), $('#t1').css('display', 'none'), car_type_selection(i = '0');">                                            
                                        <label>
                                            <input type="radio" name="body_type" class="fuel-checkbox" data-name="0" data-value="0" align="center" id="All_body_type">
                                            <span>All</span>
                                        </label>
                                    </a>
                                </div> 
                                <?php
                                $types = array('Hatch Back', 'Sedan', 'Van-Mini-Van', 'SUV-MUV', 'Coupe', 'Convertible', 'Station Wagon');
                                for ($i = 1; $i <= count($types); $i++) {
//                                    if (count($types) == $i) {
//                                        $cls = 'col-md-12';
//                                    } else {
//                                        $cls = 'col-md-6';
//                                    }
                                    $cls='col-md-6';
                                    ?>
                                    <div class="col-md-6 col-sm-6 col-xs-6" align="<?php if($i%2==0) { echo 'right';} else { echo 'left';} ?>">
                                        <a onclick="$('#pro_type').val('<?php echo preg_replace('/[^a-zA-Z0-9]+/', '-', $types[$i - 1]); ?>'), car_type_selection(i = '3');" class="car-type-thumb-a">
                                            <img class="car-type-thumb clickable-img img-left clicked car_type" src="<?php echo base_url(); ?>assets/img/home/type/<?php echo $i; ?>.png">
                                            <br>
                                            <label>
                                                <input type="radio" name="body_type" <?php
                                                if (preg_replace('/[^a-zA-Z0-9]+/', '-', $types[$i - 1]) == $pro_type) {
                                                    echo 'checked';
                                                }
                                                ?> class="fuel-checkbox" data-name="<?php echo $pet_types[$i]['id']; ?>" data-value="<?php echo $pet_types[$i]['type']; ?>" align="center" id="<?php echo 'fuel' . $pet_types[$i]['id']; ?>" value="<?php $pet_types[$i]['id']; ?>">
                                                <span><?php echo $types[$i - 1]; ?>
                                                </span>
                                            </label>
                                        </a>
                                    </div>                                   
                                    <?php
                                }
                                ?>
                            </div>
                                <input type="hidden" id="pro_type" name="pro_type" value="<?php echo $pro_type; ?>"/>
                                <input type="hidden" id="pro_price" name="pro_price" value="<?php echo $pro_price; ?>"/>
                            </div>                                 
                        </div>
                    </div>
                </div>
                
                
                <div class="panel">
                    <div class="accordian-header">
                        <a data-toggle="collapse" href="#collapseFour" id="collapseFour_1">
                            <h5>
                                <i class="fa fa-group"></i>
                                Seating Capacity
                                <span class="accordian-open-close">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </h5>
                        </a>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="container-fluid">
                                <div class="seat-capacity-selection-box" onchange="car_type_selection(i = '1');">
                                    <div class="row">
                                        <div class="col-xs-12 slider-div">
                                            <div class="seat-capacity-selection"  data-min-value="2" data-max-value="12" data-initial-min="2" data-initial-max="8" data-step="1"></div>
                                            <!--<div class="seat-capacity-selection"  data-min-value="2" data-max-value="12" data-initial-min="2" data-initial-max="8" data-step="1"></div>-->
                                        </div>
                                    </div>
                                    <div class="row search_mar">
                                        <div class="col-xs-6 hidden-left" id="min">
                                            Min: <span class="min-seater">2</span> Seats
                                        </div>
                                        <div class="col-xs-6 hidden-right" id="max">
                                            Max: <span class="max-seater">11</span> Seats
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel">
                    <div class="accordian-header">
                        <a data-toggle="collapse" href="#collapseSix">
                            <h5>
                                <i class="fa fa-rupee"></i>
                                Car Price
                                <span class="accordian-open-close">
                                    <i class="fa fa-<?php
                                    if ($pro_price != '') {
                                        echo 'minus';
                                    } else {
                                        echo 'plus';
                                    }
                                    ?>"></i>
                                </span>
                            </h5>
                        </a>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse <?php
                    if ($pro_price != '') {
                        echo 'in';
                    }
                    ?>">
                        <div class="panel-body">
                            <div class="container-fluid">
                                <div class="budget-slider-container" onchange="check_price();">
                                    <div class="row">
                                        <div class="col-xs-12 slider-div">
                                            <?php
                                            $exp_price = explode('-', $pro_price);
                                            $prices = trim($max_price_res[0]['max_price']);
                                            $min_price = $max_price_res['min_price'];
                                            $max_price = $max_price_res['max_price'];
                                            ?>
                                            <div class="price-range2"  data-min-value="<?php echo $min_price; ?>" data-max-value="<?php echo $max_price; ?>" 
                                                 data-initial-min="0" data-initial-max="<?php echo $max_price; ?>" data-step="1000"></div>
                                        </div>
                                    </div>
                                    <div class="row search_mar">
                                        <div class="col-xs-6 hidden-left" id="pric_min">
                                            Min<div><i class="fa fa-rupee"></i> <span class="minvalue"></span></div>
                                        </div>
                                        <div class="col-xs-6 hidden-right" id="pric_max">
                                            Max<div><i class="fa fa-rupee"></i> <span class="maxvalue"></span></div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="s_price" id="s_price" value="<?php echo $exp_price[0]; ?>" />
                                    <input type="hidden" name="e_price" id="e_price" value="<?php echo $exp_price[1]; ?>" />
                                    <input type="hidden" name="f_price" id="f_price" value="<?php echo $pro_price; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel">
                    <div class="accordian-header">
                        <a data-toggle="collapse" href="#collapseFive">
                            <h5>
                                <i class="fa fa-dashboard"></i>
                                Transmission Type
                                <span class="accordian-open-close">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </h5>
                        </a>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="container-fluid transmission-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul>
                                            <li><label><span><a onclick="$('#s6').css('display', 'none'), $('#trans_data_res').val(''), car_type_selection();"><input type="radio" name="auto" data-name="0" value="0" class="trans_type" id="all_transmission_type"></a></span></label><span  style="padding:3px">All</span></li>
                                            <li><label><span><a onclick="car_type_selection(i = '4');"><input type="radio" name="auto" data-name="Automatic" value="Automatic" class="trans_type"></a></span></label><span  style="padding:3px">Automatic</span></li>
                                            <li><label><span><a onclick="car_type_selection(i = '4');"><input type="radio" name="auto" data-name="Semi-Automatic" value="Semi-Automatic" class="trans_type"></a></span></label><span  style="padding:3px">Semi-Automatic</span></li>
                                            <li><label><span><a onclick="car_type_selection(i = '4');"><input type="radio" name="auto" data-name="Manual" value="Manual" class="trans_type"></a></span></label><span  style="padding:3px">Manual</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <input type="hidden" id="trans_data_res" name="trans_data_res" />
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>


            
        </div>

        <div class="col-md-9 col-cust-right search_strip">
            <div class="grey-border filter-box">
                <h5>

                    <div id="show_type" class="pull-left">
                        Filters : 
                    </div>
                    <div class="search_strip_div"  id="t1">
                        <a onclick="$('#All_body_type').click(), $('#pro_type').val(''), car_type_selection('0'), $('#t1').css('display', 'none'), $('#collapseThree').removeClass(), $('#collapseThree').addClass('collapse');"><i class="fa fa-times"></i></a>
                        <span class="label label-maroon" id="t2">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>
                    <div class="search_strip_div" id="f1">
                        <a onclick="$('#fuel_type_data').click(), $('#f1').css('display', 'none'), $('#fuel_id').val(''), car_type_selection(), $('#collapseTwo').removeClass(), $('#collapseTwo').addClass('collapse');"><i class="fa fa-times"></i></a>
                        <span class="label label-maroon" id="f2">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>
                    <div class="search_strip_div" id="b1">
                        <a onclick="$('#brand_id').val(''), $('#brand_all').click(), $('#b1').css('display', 'none'), car_type_selection('0'), $('#collapseOne').removeClass(), $('#collapseOne').addClass('collapse'), $('#collapseZeroOne').removeClass(), $('#collapseZeroOne').addClass('collapse')"><i class="fa fa-times"></i></a>
                        <span class="label label-maroon" id="b2">
                            <i class="fa fa-times"></i>

                        </span>
                    </div>
                    <div class="search_strip_div" id="s1">
                        <a onclick="$('#s1').css('display', 'none'), car_type_selection('0'), $('#collapseFour_1').click(), $('.noUi-connect').css('left', '0%'), $('.noUi-background').css('left', '100%');"><i class="fa fa-times"></i></a>
                        <span class="label label-maroon" id="s2">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>
                    <div class="search_strip_div" id="s3">
                        <a onclick="$('#f_price').val(''), $('#s3').css('display', 'none'), reset_price();"><i class="fa fa-times"></i></a>
                        <span class="label label-maroon" id="s4">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>

                    <div class="search_strip_div" id="s6">
                        <a onclick="$('#all_transmission_type').click(), $('#trans_data_res').val(''), $('#s6').css('display', 'none'), car_type_selection('0'), $('#collapseFive').removeClass(), $('#collapseFive').addClass('collapse');"><i class="fa fa-times"></i></a>
                        <span class="label label-maroon" id="s7">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>
                </h5>
            </div>
            <div align="right">
                <table>                
                    <tfoot>
                        <tr>
                            <td colspan="6" class="hidden-right">                                    
                                <a class="btn btn-xs search-by-price-button-gray carsuel_book_gray" onclick="compare_car_type_selection();" > Compare </a>
                                <a class="btn btn-xs search-by-price-button carsuel_book" onclick="confiure_car_type_selection();"> Book </a>                                
                            </td>
                        </tr>
                    </tfoot> 
                </table>                
            </div> 
            <div class="overlay-wrapper">
                <div class="overlay-div"></div>
                <div class="table-responsive" id="filter_result" ></div>
            </div>                
        </div>
    </div>

</div>
<?php $this->load->view('external_js/car_filter'); ?>


