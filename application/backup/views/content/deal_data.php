<div class="container" id="hot-deals">
    <div class="row">
        <div class="col-md-12">
            <h1>New Car Deals And Discounts</h1>    
        </div>
        
        
        <table class="table table-striped table-condensed comparison-table">
            <tbody>
                <tr class="car-name">
                    <td>    
                        <div class="col-md-12 col-xs-12 col-cust-both">
                            <div class="col-md-4 col-xs-12 col-cust-left">                                
                                <select  id="sort" onchange="check_brand();" name="sort" class="form-control">
                                    <option value="">Sort By Price</option>
                                    <option  value="ASC" <?php
                                    if ($_GET['sort'] == 'ASC') {
                                        echo 'selected';
                                    }
                                    ?> >Low</option>
                                    <option  value="DESC" <?php
                                    if ($_GET['sort'] == 'DESC') {
                                        echo 'selected';
                                    }
                                    ?>>High</option>
                                </select>
                            </div>
                            <div class="col-md-4 col-xs-12 col-cust-left">                                
                                <select  id="brand" onchange="check_brand();" name="sort" class="form-control">
                                    <option value="">Sort By Brand</option>
                                    <?php foreach ($all_brands as $all_brands_res) { ?>
                                        <option  value="<?php echo $all_brands_res['brand_name']; ?>" <?php
                                        if ($_GET['brand'] == $all_brands_res['brand_name']) {
                                            echo 'selected';
                                        }
                                        ?>  ><?php echo $all_brands_res['brand_name']; ?></option>
                                             <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-4 col-xs-12 col-cust-left">                               
                                <select  onchange="check_brand();" id="type" name="sort" class="form-control">
                                    <option value="">Sort By Body Type</option>
                                    <?php foreach ($all_car_type as $all_car_type_res) {
                                        if(!empty($all_car_type_res['pro_type']))
                                        {    
                                        ?>
                                        <option  value="<?php echo $all_car_type_res['pro_type']; ?>" <?php
                                        if($_GET['type'] == $all_car_type_res['pro_type']) {
                                            echo 'selected';
                                        }
                                        ?> ><?php echo $all_car_type_res['pro_type']; ?></option>
                                        <?php 
                                        }
                                        } ?>
                                </select>
                            </div>

                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-12 col-cust-both" id="group-item-carousel">    
                            <?php
//                            echo '<pre>';
//                            print_r($hot_deals_data);
//                            echo '</pre>';
                            foreach ($hot_deals_data as $c => $key) {
                                $sort_mnc_exshowroom_price[] = $key['mnc_exshowroom_price'];
                            }
//                            print_r($sort_mnc_exshowroom_price);
                            if ($_GET['sort'] == 'DESC') {
                                array_multisort($sort_mnc_exshowroom_price, SORT_DESC, $hot_deals_data);
                            } else {
                                array_multisort($sort_mnc_exshowroom_price, SORT_ASC, $hot_deals_data);
                            }
                            $ii = 1;
                            $data['userdata'] = $this->session->userdata("logged_in_user");
                            $user_id = $data['userdata']['user_id'];
                            if ($user_id == '') {
                                $user_id = '0'; 
                            }
                            if (!empty($hot_deals_data)) {
                                $cnt=0;
                                foreach ($hot_deals_data as $hot_deals_data_res) {
                                    if ($this->input->cookie('main_city', TRUE) == $hot_deals_data_res['city_id']) {
                                        $total_saving = $this->home_feature->get_total_saving($hot_deals_data_res['variant_id'], $user_id);
                                        
                                        ?>
                                        <?php
                                        if (indianFormatNumber($hot_deals_data_res['exshowroom_price']) > 0) {
                                            ?>
                                            <div class="col-md-4 col-xs-12 col-cust-both">
                                                <!--<div class="container-fluid">-->
                                                    <!--<div class="row">-->
                                                        <div class="ribbin">	
                                                            <div class="corner">
                                                                <h2><?php echo $hot_deals_data_res['total_benefits_perc']; ?></h2>
                                                                <div class="group-item">
                                                                    <div class="col-md-12 col-cust-both" align="center">
                                                                            <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $hot_deals_data_res['pro_name_comp']) . '-' . $hot_deals_data_res['variant_id']) ?>" >        
                                                                                <img alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $hot_deals_data_res['pro_image'])); ?>" src="<?php echo img_url('uploads/product/' . $hot_deals_data_res['pro_image']); ?>" class="img-responsive">
                                                                            </a>                                                                        
                                                                    </div>    
                                                                    <div class="group-content">
                                                                        <div class="container-fluid">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <h4 class="hot_dealreview_title"> <?php echo $hot_deals_data_res['pro_name_comp'] ?></h4>
                                                                                </div> 
                                                                                <div class="col-md-12">
                                                                                    <div class="group-price">
                                                                                        <div class="mnc-price">
                                                                                            <span class="price-span">
                                                                                                Ex-Showroom Price
                                                                                            </span>
                                                                                            <?php
                                                                                            if (indianFormatNumber($hot_deals_data_res['exshowroom_price']) > 0) {
                                                                                                ?>
                                                                                                <span class="text-maroon">
                                                                                                    <i class="fa fa-rupee"></i>
                                                                                                    <?php
                                                                                                    if (indianFormatNumber($hot_deals_data_res['mnc_exshowroom_price']) == 0) {
                                                                                                        echo '0';
                                                                                                    } else {
                                                                                                        echo indianFormatNumber($hot_deals_data_res['mnc_exshowroom_price']);
                                                                                                    }
                                                                                                    ?>   
                                                                                                </span>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <span class="text-maroon">

                                                                                                    <?php
                                                                                                    echo Not_applicable;
                                                                                                    ?>    
                                                                                                </span>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                        </div>     
                                                                                    </div>
                                                                                </div> 
                                                                               
                                                                                <div class="col-md-12">
                                                                                    <span class="price-span">Total Savings </span>
                                                                                    <span class="price-span text-maroon"><i class="fa fa-rupee"></i>
                                                                                        <?php
                                                                                        if (indianFormatNumber($hot_deals_data_res['total_savings']) == 0) {
                                                                                            echo '0';
                                                                                        } else {
                                                                                            echo indianFormatNumber($hot_deals_data_res['total_savings']);
                                                                                        }
                                                                                        ?> 
                                                                                    </span>
                                                                                </div>  
                                                                                <div class="col-md-12">
                                                                                    <span class="price-span">Valid Until </span>
                                                                                    <span class="price-span text-maroon">
                                                                                        <?php
                                                                                        if (!empty($hot_deals_data_res['valid_until'])) {
                                                                                            echo '- ' . $hot_deals_data_res['valid_until'];
                                                                                        } else {
                                                                                            echo '-';
                                                                                        }
                                                                                        ?> 
                                                                                    </span>
                                                                                </div>  
                                                                                <div class="col-md-12">
                                                                                    <div class="pull-left">                                                                    
                                                                                        <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carinsurance_hotdeals.png' ?>" data-toggle="tooltip" class="main_tooltip"  data-placement="right" title="Car Insurance Offers"/>
                                                                                        <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carfinance_hotdeals.png' ?>"  data-toggle="tooltip" class="main_tooltip"  data-placement="right" title="Car Finance Offers"/>			
                                                                                        <img src="<?php echo base_url() . 'assets/img/fwdhotdealsicons/carexchange.png' ?>" data-toggle="tooltip" class="main_tooltip"  data-placement="right" title="Car Exchange  Offers"/>
                                                                                        <img src="<?php echo base_url() . 'assets/img/product/otherbenefits.png' ?>" data-toggle="tooltip" class="main_tooltip"  data-placement="top" title="<?php echo $hot_deals_data_res['accessory_package']; ?>"/>
                                                                                    </div>
                                                                                    <div class="pull-right">
                                                                                        <a href="<?php echo site_url('car/' . preg_replace('/\s+/', '-', $hot_deals_data_res['pro_name_comp']) . '-' . $hot_deals_data_res['variant_id']) ?>" class="btn btn-xs carsuel_book" >Book</a>
                                                                                    </div>
                                                                                </div>

                                                                            </div>      
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>      
                                                        </div>      
                                                        <!--</div>--> 
                                                    <!--</div>-->
                                                <!--</div>-->
                                            </div>

                                            <?php
                                            $ii++;
                                        }
                                       
                                        $cnt++;
                                    }
                                }
                            } else {
                                ?>
                                <div align="center" class="no_carousel">
                                    <img src="<?php echo base_url(); ?>/assets/img/ComingSoon.png">
                                </div>
                                <?php
                            }
                            ?>
                        </div>            

                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    
    
</div>
<!--</div>  /.container -->
<?php
$this->load->view('external_js/hot_deal.php');
?>
