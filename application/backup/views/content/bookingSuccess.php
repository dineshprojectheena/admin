<script>
    var d = new Date();
    d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = 'discount_price=;' + expires + ';path=/';
</script>
<div class="container" id="booking_success">
    <?php
//    $secret_key = "Your Secret Key";	 // Pass Your Registered Secret Key from EBS secure Portal
//    if(isset($_REQUEST)){
//        $response = $_REQUEST;	 
//    }
//    print_r($response);
//    exit;
    
    if($fail=='fail')
    {
    ?>    
    <div class="row">
        <div class="col-md-12">
            <h5 class="page-heading"> <strong class="text-danger"><i class="fa fa-times"></i>Booking Failed!</strong></h5>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2">
                        <img class="flow_icon"  alt="Prrems" src="<?php echo base_url() . 'assets/img/fail.png'; ?>" style="width:100%;">                            
                    </div>    
                    <div class="col-sm-10">
                        <p>
                            <?php
                            echo $order_result['pro_detail'];
                            ?>
                        </p>
                        <p>
                          Sorry! Transaction failed. Requesting you to please book the car again or call 022-45020304 !
                        </p>
                        
                    </div>
                </div>                
            </div>
            
            
            
        </div>
    </div>    
    <?php    
    }        
    else
    {    
    ?>
    <div class="row">
        <div class="col-md-12">
            <h5 class="page-heading"> <strong><span class="success_strong">&#x2713;</span>Booking Confirmed!</strong></h5>
            <!--<h5 class="page-heading">Congratulations <strong><?php echo $order_result['firstname']; ?></strong></h5>-->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3 box">
                        <div class="box sample"></div>
                        <img class="img-center img-responsive img-holder" src="<?php echo img_url('uploads/product/' . $order_result['pro_image']); ?>" />
                        <br>
                        <div align="center">
                            <a href="<?php echo base_url('payment/download_sucess_pdf/' . $order_result['order_id']) ?>" class="btn btn-maroon">
                                Download Receipt</a>
                        </div>    
                    </div>
                    <div class="col-sm-9">
                        <p>
                            <?php
//                           print_r($order_result);

                            echo $order_result['pro_detail'];
                            ?>

                        </p>

                        <p >
                            Congratulations! Your brand new <span class="tag_bold"><?php echo $order_result['pro_name_comp']; ?></span> is on it's way. We have received Rs. <?php echo $order_result['paid_amount']; ?> from you as a booking amount. 
                            <br><br>

                            Your Booking Order number is <span class="tag_bold"><?php echo $order_result['invoice_no']; ?></span>. Please download the Order Confirmation Receipt for your reference. Our Customer Care will get in touch with you soon to confirm the delivery details along with deals on selected services and accessories!

                        </p>
                        <img class="flow_icon" alt="Prrems" src="<?php echo base_url() . 'uploads/booking_flow.png'; ?>">    
                    </div>
                </div>

                <div class="row" style="display:none;">
                    <div class="col-md-12">
                        <!--<h5 class="page-heading">Whats Next</h5>-->
                        <div class="panel-group" id="selection-list">
                            <div class="panel">
                                <div class="accordian-header">
                                    <a data-toggle="collapse" data-parent="#selection-list" href="#booking-details">
                                        <h5>
                                            <i class="fa fa-tags"></i>
                                            Booking Details
                                            <span class="accordian-open-close">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                        </h5>
                                    </a>
                                </div>
                                <div id="booking-details" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>
                                                    <span> <a href="<?php echo base_url('stitcher/download_sucess_pdf/' . $order_result['order_id']) ?>"><i class="fa fa-file-pdf-o"></i> Download as pdf</a> </span>

                                                </p>
                                                <p>
                                                    Booking Number : <span class="label label-maroon"> <?php echo $order_result['invoice_no']; ?> </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="accordian-header">
                                    <a data-toggle="collapse" data-parent="#selection-list" href="#delivary-details">
                                        <h5>
                                            <i class="fa fa-tags"></i>
                                            Order Details
                                            <span class="accordian-open-close">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                        </h5>
                                    </a>
                                </div>
                                <div id="delivary-details" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>
                                                <table class="car-review-table table table-bordered table-striped table-condensed">
                                                    <thead>
                                                        <tr>

                                                            <th>Booking Address</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>                                                           
                                                            <td>Customer Name</td>
                                                            <td>
                                                                <?php
                                                                print_r($order_user_detail[0]['firstname']);
                                                                ?>                                                             
                                                            </td>
                                                        </tr>
                                                        <tr>                                                           
                                                            <td>Customer Email</td>
                                                            <td>
                                                                <?php
                                                                print_r($order_user_detail[0]['email']);
                                                                ?>                                                             
                                                            </td>
                                                        </tr>
                                                        <tr>                                                           
                                                            <td>Customer Address</td>
                                                            <td>
                                                                <?php
                                                                print_r($order_user_detail[0]['address']);
                                                                ?>                                                             
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <?php
    }
    ?>
    
</div>
<!-- Google Code for Leads Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 959088565;
    var google_conversion_language = "en";
    var google_conversion_format = "2";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "KwOHCP2BllkQtY-qyQM";
    var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/959088565/?label=KwOHCP2BllkQtY-qyQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
