<div class="container"  id="contactus">
    <div class="col-md-12 col-cust-both">
    <h1>
        Car Enquiry
    </h1>
    </div>    
    <div class="col-md-12">
        <div class="col-md-8">
            <form id="contact-us" class="form-horizontal" role="form" action="<?php echo site_url("conatct_upload"); ?>" method="POST">
                <?php if (isset($alert_msg)) { ?>
                    <div role="alert" id="msg" class="alert alert-success">
                        <div class="twelve columns mobile-four alert-box secondary">
                            <p class="success"><?php echo 'Email sent successfully. You will hear from us soon'; ?></p>
                            <a onclick="$('#msg').hide();" class="close">&times;</a>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-3 control-label">City</label>
                    <div class="col-sm-3">
                        <select class="selectpicker" name="city">
                            <option>Select City</option>
                            <?php
                            foreach ($city_res as $city_res_data) {
                                ?>                            
                                <option><?php echo $city_res_data['name']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                </div>
                <div class="form-group">
                    <label for="number" class="col-sm-3 control-label">Phone Number</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" name="number" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="text" name="email" class="form-control" id="email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="body_type" class="col-sm-3 control-label">My Preferred Car is:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="body_type">
                    </div>
                </div>
                <div class="form-group">
                    <label for="enquiry" class="col-sm-3 control-label">Area of Enquiry</label>
                    <div class="col-sm-9">
                        <select class="selectpicker" name="area_enquiry"  required >
                            <option value="">Area of Enquiry</option>
                            <option>Offers</option>
                            <option>Test Drive</option>
                            <option>Cars</option>
                            <option>Accessories</option>
                            <option>Insurance</option>
                            <option>Finance</option>
                            <option>Partnerships</option>
                            <option>Careers</option>
                            <option>Others</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="enquiry" class="col-sm-3 control-label">Enquiry</label>
                    <div class="col-sm-9">
                        <textarea name="enquiry" class="form-control" rows="3" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-maroon">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-4 contact-details-div">
            <div>
                <!--                <div class="review-box">
                                    <h5>
                                        Contact Us
                                    </h5>
                                </div>-->
                <?php
                echo MNC_ADDRESS;
                ?>
            </div>
            <div class="social">
                <div class="social">
                    <a href="#" class="fb tiphere" data-toggle="tooltip" data-placement="top" title="Facebook">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="#" class="tw tiphere" data-toggle="tooltip" data-placement="top" title="Twitter">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="#" class="gp tiphere" data-toggle="tooltip" data-placement="top" title="Google Plus">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="#" class="ln tiphere" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                </div>
            </div>

            <div>
                <?php
                echo MNC_google_map;
                ?>
            </div>


        </div>
    </div>
</div>