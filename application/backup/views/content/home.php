<div class="container no_padding_container">
<div class="row ">
<div id="slider1_container" style="display: none; position: relative; margin: 0 auto;
     top: 0px; left: 0px; width: 1024px; height: 328px; overflow: hidden;">
    <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width:1024px; height: 328px; overflow: hidden;">
        <?php
        $total_slider = count($main_slider);
        foreach ($main_slider as $slider_data) {
            ?>
            <div>               
           <a href="<?php echo $slider_data['link']; ?>">    
                    <img  u="image" src2="<?php echo img_url('uploads/slider/' . $slider_data['image_name']); ?>" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', ($slider_data['image_name']))); ?>" class="home_slider img-responsive">
                </a> 
            </div>    
            <?php
        }
        ?>
    </div>
    <style>
        .jssorb21 {
            position: absolute;
        }
        .jssorb21 div, .jssorb21 div:hover, .jssorb21 .av {
            position: absolute;
            width: 19px;
            height: 19px;
            text-align: center;
            line-height: 19px;
            color: white;
            font-size: 12px;
            background: url(<?php echo base_url(); ?>slider/img/b21.png) no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb21 div { background-position: -5px -5px; }
        .jssorb21 div:hover, .jssorb21 .av:hover { background-position: -35px -5px; }
        .jssorb21 .av { background-position: -65px -5px; }
        .jssorb21 .dn, .jssorb21 .dn:hover { background-position: -95px -5px; }
    </style>
    <style>
        .jssora21l, .jssora21r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 55px;
            height: 55px;
            cursor: pointer;
            background: url(<?php echo base_url(); ?>slider/img/a21.png) center center no-repeat;
            overflow: hidden;
        }
        .jssora21l { background-position: -3px -33px; }
        .jssora21r { background-position: -63px -33px; }
        .jssora21l:hover { background-position: -123px -33px; }
        .jssora21r:hover { background-position: -183px -33px; }
        .jssora21l.jssora21ldn { background-position: -243px -33px; }
        .jssora21r.jssora21rdn { background-position: -303px -33px; }
        .slider_div
        {
            z-index: 1;
            position: absolute;
            width: 100%;
            text-align: center;
            top: 300px;   
        }
    </style>
    <span u="arrowleft" class="jssora21l" style="top: 123px; left: 8px;">
    </span>
    <!-- Arrow Right -->
    <span u="arrowright" class="jssora21r" style="top: 123px; right: 8px;">
    </span>
</div>
</div>
</div>


<script>
    jQuery(document).ready(function ($) {
        var options = {
            $FillMode: 2, //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
            $AutoPlay: true, //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
            $AutoPlayInterval: 4000, //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
            $PauseOnHover: 1, //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

            $ArrowKeyNavigation: true, //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
            $SlideEasing: $JssorEasing$.$EaseOutQuint, //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
            $SlideDuration: 800, //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
            $MinDragOffsetToSlide: 20, //[Optional] Minimum drag offset to trigger slide , default value is 20
            //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
            //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
            $SlideSpacing: 0, //[Optional] Space between each slide in pixels, default value is 0
            $DisplayPieces: 1, //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
            $ParkingPosition: 0, //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
            $UISearchMode: 1, //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
            $PlayOrientation: 1, //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
            $DragOrientation: 1, //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

            $BulletNavigatorOptions: {//[Optional] Options to specify and enable navigator or not
                $Class: $JssorBulletNavigator$, //[Required] Class to create navigator instance
                $ChanceToShow: 2, //[Required] 0 Never, 1 Mouse Over, 2 Always
                $AutoCenter: 1, //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                $Steps: 1, //[Optional] Steps to go for each navigation request, default value is 1
                $Lanes: 1, //[Optional] Specify lanes to arrange items, default value is 1
                $SpacingX: 8, //[Optional] Horizontal space between each item in pixel, default value is 0
                $SpacingY: 8, //[Optional] Vertical space between each item in pixel, default value is 0
                $Orientation: 1, //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                $Scale: false                                   //Scales bullets navigator or not while slider scale
            },
            $ArrowNavigatorOptions: {//[Optional] Options to specify and enable arrow navigator or not
                $Class: $JssorArrowNavigator$, //[Requried] Class to create arrow navigator instance
                $ChanceToShow: 1, //[Required] 0 Never, 1 Mouse Over, 2 Always
                $AutoCenter: 2, //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
            }
        };

        //Make the element 'slider1_container' visible before initialize jssor slider.
        $("#slider1_container").css("display", "block");
        var jssor_slider1 = new $JssorSlider$("slider1_container", options);

        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizes
        function ScaleSlider() {
            var bodyWidth = document.body.clientWidth;
            if (bodyWidth)
                jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1024));
            else
                window.setTimeout(ScaleSlider, 30);
        }
        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
    });
</script>    
<div class="container hide">
    <div class="row ">
        <div class="col-md-12 col-cust-both home-search  col-xs-12 col-sm-12 hidden-sm visible-md visible-lg" >
            <form action="<?php echo site_url('car-search-by-type'); ?>" method="POST">
                
                <div class="col-md-2 col-cust-left  col-xs-12 col-sm-12" onchange="setmodel2(), select_your_car('1');">
                    <select class="form-control" id="carbrands" name="carbrands" class="search_feald">
                        <option value="">Select Brand</option>
                        <?php
                        if (!empty($all_brands)) {
                            foreach ($all_brands as $all_brands_res) {
                                ?>    
                                <option value="<?php echo $all_brands_res['brand_id']; ?>"  class='brand-img'> 
                                    <?php echo $all_brands_res['brand_name']; ?>
                                </option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <br>
                </div>
                <div class="col-md-2 col-cust-left  col-xs-12 col-sm-12">
                    <select class="form-control" id="brand_model" onchange="select_your_car('2')" name="brand_model">
                        <option value="">Select Model</option>                            
                    </select>
                    <br>
                </div>
                <div class="col-md-2  col-cust-left  col-xs-12 col-sm-12">
                    <select class="form-control" id="cartype" name="cartype" onchange="select_your_car('3')">
                        <option value="">Select Car Type</option>
                        <?php
                        $types = array('Hatch Back', 'Sedan', 'Station Wagon', 'Van/Mini Van', 'SUV-MUV', 'Coupe', 'Convertible');
                        foreach ($types as $types_data) {
                            ?>
                            <option value="<?php echo str_replace('/', '-', $types_data); ?>"><?php echo $types_data; ?></option>                                    
                            <?php
                        }
                        ?>
                    </select>
                    <br>
                </div>
                <?php
                $prices = trim($max_price_res['max_price']);
                $min_prices = trim($max_price_res['min_price']);
                $exp_price = explode('.', $prices);
                $prices = $exp_price['0'];
                $exp_min_prices = explode('.', $min_prices);
                $min_price = $exp_min_prices['0'];
                ?>
                <div class="col-md-6">
                    <div class="col-md-6  col-xs-12 col-sm-12" >
                        <!--<label for="carprice" class="home_select_car_padd">Car Price</label>-->
                        <div class="price-range " onchange="select_your_car('4');" data-min-value="<?php echo $min_price; ?>" data-max-value="<?php echo $prices; ?>" data-step="1" ></div>
                        <input id="carprice" type="hidden" readonly/>
                        <div class="price-wrapper home_select_car_padd">
                            <div class="price-div" id="min_price" style="float:left"><span>Min : </span><i class="fa fa-rupee"></i> <span class="minvalue"></span></div>
                            <div class="price-div" id="max_price" style="float:right"><span>Max : </span><i class="fa fa-rupee"></i> <span class="maxvalue"></span></div>
                        </div>
                        <br>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-12">                                
                        <button class="btn btn-xs carsuel_book col-md-12 col-xs-12 col-sm-12" type="button" onclick="$('#search_submit').click();">Search</button>
                    </div>                    
                </div>
                
            </form> 
        </div>
        <div class="col-md-12 col-cust-both col-xs-12 col-sm-12 hidden-lg hidden-md home-search-sm">
        <button class="btn btn-xs carsuel_book col-md-12 col-xs-12 col-sm-12" type="button" onclick="$('#search_submit').click();">Search</button>    
        </div>
        


    </div>
</div>

<div class="container" id="home_hot_deal">
    <div class="row">
        <div class="col-md-12 col-cust-both">
            <h1>New Car Offers & Hot Deals</h1>    
        </div>
        <div class="col-md-12 col-cust-both">            
            <div class="col-md-1 col-xs-3 col-cust-left">
                <div class="review-box">
                    <h3 class="review-title">
                        Hot Deals
                    </h3>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <select class="form-control" id="brand_data" onchange="home_brand_product();">
                    <?php
                    if (!empty($all_brands)) {
                        foreach ($all_brands as $all_brands_res) {
                            ?>    
                            <option style="margin:10px;" <?php
                            if ($all_brands_res['brand_name'] == 'Maruti-Suzuki') {
                                echo 'selected';
                            }
                            ?> value="<?php echo $all_brands_res['brand_id']; ?>" data-content="<div class='text-left text-maroon'><span class='brand-name'><?php echo str_replace("-", " ", $all_brands_res['brand_name']); ?></span></div>"><?php echo $all_brands_res['brand_name']; ?></option>
                                    <?php
                                }
                            }
                            ?>                                
                </select>
            </div>
            <div class="col-md-8 pull-right col-xs-3 col-cust-right">
                <div class="pull-right controls">
                    <a href="#group-item-carousel" data-slide="prev" id="prev">
                        <i class="fa fa-chevron-circle-left fa-2x"></i>
                    </a>
                    <a href="#group-item-carousel" data-slide="next" id="next">
                        <i class="fa fa-chevron-circle-right fa-2x"></i>
                    </a>
                </div>
            </div>
        </div>  
        <div class="col-md-12 col-xs-12 col-cust-both">                        
            <div id="group-item-carousel"  class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner" id="carousel_result" >
                    <!--carousel result show here-->
                </div>
            </div>
        </div>

    </div>
</div>



<div class="container">
    <div class="row">
        <div class="col-md-4 col-cust-both">
            <div class="review-box" >
                <h4 class="review-title">
                    <i class="glyphicon glyphicon-film"></i> How MYNEWCAR.IN Works
                </h4>
            </div>    
            <div class="col-md-12 col-cust-left" align="center">                    
                <div class="video"  align="center">
                    <iframe id="VideoPlayer"  src="https://www.youtube.com/embed/7Rxm0L-OsoE?enablejsapi=1&amp;controls=0&amp;amp;showinfo=0&amp;rel=0&amp;autoplay=0&amp;wmode=opaque&amp;vq=hd720" frameborder="0"></iframe>
                </div>
            </div>
        </div>

        <div class="col-md-4 div-right-border">
                        <div class="review-box">                
                            <h4 class="review-title">
                                <i class="fa fa-bullhorn"></i> News
                            </h4>
            
                        </div> 
<!--            <a  href="#" data-toggle="modal" data-target="#comingsoon">
                <img src="<?php echo base_url('assets/blog.png'); ?>" class="img-responsive"/>
            </a>
            <div class="modal fade" id="comingsoon">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">
                                Coming Soon
                            </h4>
                        </div>
                        <div class="modal-body" align="center">
                            Coming Soon...    
                        </div>
                    </div>
                </div>
            </div>-->
            
            <?php foreach ($feed_res_data as $item): ?>
                        <div class="home_item">
                            <h4 class="text-maroon"><a class="text-maroon" href="<?php echo $item['link']; ?>"><?php echo $item['title'] ?></a></h4>
                <?php echo substr($item['desc'], 0, 100) . '...'; ?>
                        </div>
            <?php endforeach; ?>
            <a class="text-maroon news_home_readmore" href="http://blog.mynewcar.in/" >More News</a>            
        </div>
        <div class="col-md-4 col-cust-right">
            <div class="review-box" >
                <h4 class="review-title">
                    <img src="<?php echo base_url() . 'uploads/taraju.png'; ?>" class="icon_image">
                    <div>Car Comparision</div>
                </h4>
            </div>

            <div class="col-md-12 col-xs-12 col-cust-both">
                <div class="col-md-6 col-xs-6 col-cust-left">
                    <?php
                    ?>                    
                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $car_comp_1[0]['pro_name_comp']) . '-' . $car_comp_1[0]['variant_id']) ?>" >
                        <img src="<?php echo img_url('uploads/product/' . $car_comp_1[0]['pro_image']); ?>" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $car_comp_1[0]['pro_image'])); ?>" class="img-responsive comparisonLeft">
                    </a>
                    <div class="comp_head"><?php echo $car_comp_1[0]['pro_name_comp']; ?></div>
                    <?php
                    if (!empty($car_comp_1)) {
                        $car_comp_1[0]['variant_id']
                        ?>
                        <div class="col-md-12 col-cust-both">
                            <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="" data-original-title="User Ratings"><i class="fa fa-users"></i></span>
                            <span class="text-golden">
                                <?php
                                $star = $car_comp_1[2]['total'];
                                if (isset($star)) {
                                    $exp = explode('.', $star);
                                    $point = $exp[1];
                                    if ($point > 00) {
                                        $half = '-half-full';
                                    } else if ($point == 00) {
                                        $half = '-o';
                                    } else {
                                        $half = '';
                                    }

                                    if ($exp[0] == 1) {
                                        ?>
                                        <i class="fa fa-star"></i><?php ?>
                                        <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                        <?php
                                    } else if ($exp[0] == 2) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                    } else if ($exp[0] == 3) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                    } else if ($exp[0] == 4) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                    } else if ($star == 5) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                    <?php
                                }
                                ?>
                            </span>
                        </div>
                        <div class="col-md-12 col-cust-both">            
                            <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="" data-original-title="Expert Ratings"><i class="fa fa-user"></i></span>
                            <span class="text-maroon">
                                <?php
                                $star = $car_comp_1[1]['total'];
                                if (isset($star)) {
                                    $exp = explode('.', $star);
                                    $point = $exp[1];
                                    if ($point > 00) {
                                        $half = '-half-full';
                                    } else if ($point == 00) {
                                        $half = '-o';
                                    } else {
                                        $half = '';
                                    }

                                    if ($exp[0] == 1) {
                                        ?>
                                        <i class="fa fa-star"></i><?php ?>
                                        <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                        <?php
                                    } else if ($exp[0] == 2) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                    } else if ($exp[0] == 3) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                    } else if ($exp[0] == 4) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                    } else if ($star == 5) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                    <?php
                                }
                                ?>
                            </span>
                        </div>
                        <?php
                        $com_car1 = $car_comp_1[0]['pro_name_comp'] . '-' . $car_comp_1[0]['variant_id'];
                    }
                    ?>

                </div>

                <div class="col-md-6 col-xs-6 col-cust-right">

                    <a href="<?php echo url_struct('car/' . preg_replace('/\s+/', '-', $car_comp_2[0]['pro_name_comp']) . '-' . $car_comp_2[0]['variant_id']) ?>" >
                        <img src="<?php echo img_url('uploads/product/' . $car_comp_2[0]['pro_image']); ?>" alt="<?php echo str_replace('.jpg', '', str_replace('/', ' ', $car_comp_2[0]['pro_image'])); ?>" class="img-responsive comparisonLeft">
                    </a>
                    <div class="comp_head"><?php echo $car_comp_2[0]['pro_name_comp']; ?></div>
                    <?php
                    if (!empty($car_comp_2)) {
                        ?>
                        <div class="col-md-12 col-cust-both">
                            <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="" data-original-title="User Ratings"><i class="fa fa-users"></i></span>
                            <span class="text-golden">
                                <?php
                                $star = $car_comp_2[2]['total'];
                                if (isset($star)) {
                                    $exp = explode('.', $star);
                                    $point = $exp[1];
                                    if ($point > 00) {
                                        $half = '-half-full';
                                    } else if ($point == 00) {
                                        $half = '-o';
                                    } else {
                                        $half = '';
                                    }

                                    if ($exp[0] == 1) {
                                        ?>
                                        <i class="fa fa-star"></i><?php ?>
                                        <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                        <?php
                                    } else if ($exp[0] == 2) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                    } else if ($exp[0] == 3) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                    } else if ($exp[0] == 4) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                    } else if ($star == 5) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                    <?php
                                }
                                ?>   
                            </span>
                        </div>
                        <div class="col-md-12 col-cust-both"> 
                            <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="" data-original-title="Expert Ratings"><i class="fa fa-user"></i></span>
                            <span class="text-maroon">
                                <?php
                                $star = $car_comp_2[1]['total'];
                                if (isset($star)) {
                                    $exp = explode('.', $star);
                                    $point = $exp[1];
                                    if ($point > 00) {
                                        $half = '-half-full';
                                    } else if ($point == 00) {
                                        $half = '-o';
                                    } else {
                                        $half = '';
                                    }

                                    if ($exp[0] == 1) {
                                        ?>
                                        <i class="fa fa-star"></i><?php ?>
                                        <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                        <?php
                                    } else if ($exp[0] == 2) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                    } else if ($exp[0] == 3) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                    } else if ($exp[0] == 4) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                    } else if ($star == 5) {
                                        ?>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                    <?php
                                }
                                ?>
                            </span>
                        </div>
                        <?php
                        $com_car2 = $car_comp_2[0]['pro_name_comp'] . '-' . $car_comp_2[0]['variant_id'];
                    }
                    if ($com_car1 != '' || $com_car2 != '') {
                        $compare_cars = preg_replace('/\s+/', '-', $com_car1) . '-VS-' . preg_replace('/\s+/', '-', $com_car2);
                    }
####### home car comparison second end here######
                    ?>
                </div>

                <?php
                if (!empty($car_comp_1)) {
                    ?>
                    <a href="<?php echo site_url('compare-new-cars-online/' . $compare_cars); ?>" id="review_link_data" class="text-maroon news_home_readmore">Compare Cars</a>
                    <?php
                }
                ?>
            </div>



        </div>
    </div>
</div>



<div  class="container">
    <div class="row">
        <div class="col-md-12 col-cust-both">            
            <div class="col-md-3 col-xs-5 col-sm-5 col-cust-both">
                <div class="review-box">
                    <h4 class="review-title">
                        <i class="fa fa-comments"></i> Customer Testimonials
                    </h4>                
                </div>
            </div>
            <div class="col-md-9 pull-right  col-xs-7 col-sm-7 col-cust-both">
                <div class="pull-right controls">
                    <a id="prev" data-slide="prev" href="#test_carousel">
                        <i class="fa fa-chevron-circle-left fa-2x"></i>
                    </a>
                    <a id="next" data-slide="next" href="#test_carousel">
                        <i class="fa fa-chevron-circle-right fa-2x"></i>
                    </a>
                </div>
            </div>

        </div>  
        <?php
        $testimonial = array(
            array("image" => "", "details" => "It was great dealing with you and I wish Mynewcar.in all the success in future. Keep up the Spirit!", "name" => "Yash Rathi"),
            array("image" => "", "details" => "It was nice experience to book car through you. I will certainly recommend my near and dear ones to book car from you.", "name" => "Kavidas Mate"),
            array("image" => "customer.jpg", "details" => "I am very happy with the experience I had on MyNewCar website. Easily booked my dream car by just paying a small booking amount!", "name" => "Anil Sharma"),
            array("image" => "Nishanth Hiremath.jpg", "details" => "I truly appreciate you guys for the Immediate response shown and I could get my car delivery with in 2 days of Booking the car and more importantly 'Hassle free' process", "name" => "Dr. Nishanth Hiremath"),
//            array("image" => "", "details" => "Hi Mukund. Appreciate all the support. And the feedback is super. Will for surely recommend my friends.", "name" => "Ankit")
        );
//        echo '<pre>';
        ?>


        <div class="col-md-12 col-xs-12 col-cust-both">                        
            <div data-interval="false" data-ride="carousel" class="test_carousel slide" id="test_carousel">
                <div class="carousel-inner" id="testinomial">
                    <?php
                    $tot_test = count($testimonial);
                    if ($tot_test % 2 != 0) {
                        $tot_test = $tot_test + 1;
                    } else {
                        $tot_test;
                    }
//                    echo $tot_test; 
                    for ($i = 0, $j = 0; $i <= $tot_test - 1; $i = $i + 2) {
                        ?>    
                        <div class="test_div item <?php
                        if ($i == 0) {
                            echo 'active';
                        } else {
                            echo '';
                        }
                        ?>">
                            <div class="col-md-6 col-cust-left col-xs-12 col-sm-12"> 
                                <div class="testinomial_effect"> 
                                    <div class="col-md-12 text-center testinomial_effect_icon">
                                        <span class="fa-stack fa-lg">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fa fa-quote-right fa-stack-1x fa-inverse"></i>
                                        </span>                      
                                    </div>
                                    <div class="col-md-12 col-sm-12 text-center">
                                        <a>
                                            <?php
                                            $details = $testimonial[$j]['details'];
                                            $image = $testimonial[$j]['image'];
                                            if ($image == '') {
                                                $image = 'user.png';
                                            }
                                            ?>
                                            <img class="img-circle img-center img-responsive" src="<?php echo base_url(); ?>/assets/img/testimonial/<?php echo $image; ?>"/></a>
                                    </div>    
                                    <div class="col-md-12 col-sm-12 col-cust-both">
                                        <p><?php
                                            echo $details;
                                            ?>
                                            -
                                            <strong class="et_pb_testimonial_author"><?php echo $testimonial[$j++]['name']; ?></strong>
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-cust-right col-xs-12 col-sm-12"> 
                                <?php
                                $details2 = $testimonial[$j]['details'];
                                if (!empty($details2)) {
                                    ?>
                                    <div class="testinomial_effect">
                                        <div class="col-md-12 text-center testinomial_effect_icon">
                                            <span class="fa-stack fa-lg">
                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                <i class="fa fa-quote-right fa-stack-1x fa-inverse"></i>
                                            </span>                      
                                        </div>
                                        <div class="col-md-12 col-sm-12 text-center">
                                            <a>
                                                <?php
                                                $image2 = $testimonial[$j]['image'];
                                                if ($image2 == '') {
                                                    $image2 = 'user.png';
                                                }
                                                ?>
                                                <img class="img-circle img-center img-responsive" src="<?php echo base_url(); ?>/assets/img/testimonial/<?php echo $image2; ?>"/></a>
                                        </div>    
                                        <div class="col-md-12 col-sm-12 col-cust-both">
                                            <p><?php
                                                echo $details2;
                                                ?>
                                                -
                                                <strong class="et_pb_testimonial_author"><?php echo $testimonial[$j++]['name']; ?></strong>
                                            </p>

                                        </div> 


                                    </div>
                                    <?php
                                }
                                ?>
                            </div>


                        </div> 
                        <?php
                    }
                    ?>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-cust-both">
            <div class="review-box">
                <h4 class="review-title">
                    <i class="fa fa-tags"></i> Brands
                </h4>
            </div>    
        </div>    
        <div class="col-md-12 col-cust-both">
            <?php
            if (!empty($all_brands)) {
                for ($i = 1, $j = 0; $i <= count($all_brands); $i++) {
                    ?>
                    <?php
                    if (!empty($all_brands[$j]['brand_image'])) {
                        ?>
                        <div class="col-md-2 col-xs-6 col-cust-left home_brand">                                                            
                            <div class="home_brand_box" align="center">
                                <a href="<?php echo base_url() . 'brand/' . $all_brands[$j]['brand_name']; ?>">
                                    <img class="img-center " src="<?php echo img_url('uploads/brand/' . $all_brands[$j]['brand_image']) ?>" />
                                </a>

                                <a href="<?php echo base_url() . 'brand/' . $all_brands[$j]['brand_name']; ?>">
                                    <?php echo $all_brands[$j++]['brand_name']; ?>
                                </a>                                                                    
                            </div>                                                            
                        </div>    
                        <?php
                    }
                    ?>
                    <?php
                }
            }
            ?>        
        </div>
    </div>
</div>
<div class="container">
    <div class="row"> 
        <div class="col-md-12 col-cust-both">            
            <div id="static-message" >
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Promise.png">
                        Our Promise
                        <p>Buy Easily and Securely</p>
                    </h4>  
                </div>
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Car Finance.png">
                        Car Finance
                        <p>Finance & Lease Offers</p>
                    </h4>  
                </div>  

                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Assurance.png">
                        Our Assurance
                        <p>Refundable Booking Amount</p>
                    </h4>  
                </div>  
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Our Trust.png">
                        Our Trust
                        <p>Honest & Friendly Customer Care</p>
                    </h4>  
                </div>  


                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Warranty.png">
                        Warranty
                        <p>After Sales & Service</p>
                    </h4>  
                </div>  
                <div class="col-md-4 col-xs-6" >
                    <h4 class="review-title">
                        <img src="<?php echo base_url(); ?>assets/img/USP/Car Exchange.png">
                        Car Exchange
                        <p>Free Car Evaluation & Exchange Offers</p>
                    </h4>  
                </div>  

            </div>

        </div>
    </div>
</div>