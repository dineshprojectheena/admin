<?php $this->load->view('templates/responsiveSlider'); ?>
<div class="container">
    <div class="row car-exterior-selection">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed car-exterior-selection-table">
                    <tbody>
                        <tr>
                            <td class="feildset-td">
                                <i class="fa fa-caret-down"></i>Colors
                            </td>
                        </tr>
                        <tr >
                            <td>
                                <div class="clearfix">
                                    <div class="pull-left color-selection">
                                        <ul class="asset-selector">
                                                <?php
                                                $color_cook=$config_selected[0]['color'];
                                                $wheel_cook=$config_selected[0]['wheel'];
                                                $cr=1;
                                                if(!empty($car_color))
                                                {    
                                                foreach($car_color as $car_exterior_color_data){
                                                ?>
                                                <li>
                                                   <?php
                                                    $class='';    
//                                                    if($color_cook=='' && $cr==1)
//                                                    {
//                                                    $class="selected";  
//                                                    $default_color=$car_exterior_color_data['feature_id'];                                                    
//                                                    }    
//                                                    else 
//                                                    {    
                                                    if($color_cook==$car_exterior_color_data['feature_id'])
                                                    {
                                                    $class="selected";
                                                    }                                                   
                                                    else
                                                    {
                                                    $class="";
                                                    }
//                                                  }
                                                    
//                                                    echo $car_exterior_color_data['feature_id'].'-'.$color_cook;                                                
                                                    ?>
                                                    <label> 
                                                        <div class="image-wrapper-div <?php echo $class; ?>" onclick="document.getElementById('color').value='<?php echo $car_exterior_color_data['feature_id'];?>';">
                                                        <img class="" src="<?php echo base_url("admin/uploads/feature/".$car_exterior_color_data['feature_img']);?>">
                                                        <div>
                                                            <i class="fa fa-check"></i>
                                                        </div>                                                        
                                                    </div>
                                                        
                                                    </label>
                                                    <div data-cost="25000" data-cost-inlcuded="1">
                                                        <?php echo $car_exterior_color_data['feature_desc']; ?>
                                                        <br/>
                                                        <?php
                                                        if (isset($car_exterior_color_data['price'])) {
							   if(indianFormatNumber($car_exterior_color_data['price'])>0)
								{
                                                            echo indianFormatNumber($car_exterior_color_data['price']);
								}
//                                                            echo 'Rs.' . $car_exterior_color_data['price'];
                                                        }
                                                        ?>
                                                    </div>
                                                </li>
                                                <?php
                                                $cr++;
                                                }
                                                }
                                                else 
                                                {
                                                 ?>
                                                    <li>
                                                        <div align="center" style="width:900px;font-size: 14px;">
                                                        <!--<label >Not applicable for this product because we do not have colors information rite now.</label>-->
                                                        <img style="height:100px;" src="<?php echo base_url();?>/assets/img/ComingSoon.png">
                                                        </div> 
                                                    </li>                                                        
                                                <?php            
                                                }    
                                            ?>
                                        </ul>
                                       
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <?php
            ####### WHeel Hided for few time ######
            /*
            ?>            
            <div class="table-responsive" >
                <table class="table table-condensed car-exterior-selection-table">
                    <tbody>
                        <tr>
                            <td class="feildset-td">
                                <i class="fa fa-caret-down"></i> Wheels
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>
                                <div class="clearfix" >
                                    <div class="pull-left color-selection">
                                        <ul class="asset-selector">
                                            <?php
                                            $wl = 1;
                                            if(!empty($car_wheel))
                                            {    
                                            foreach ($car_wheel as $car_exterior_wheel_data) {
                                                $img = $car_exterior_wheel_data['feature_img'];
//                                                  print_r($car_exterior_wheel_data);
                                                    $class='';    
                                                    if($wheel_cook=='' && $wl==1)
                                                    {
                                                    $class="selected";  
                                                    $default_week=$car_exterior_wheel_data['feature_id'];                                                    
                                                    }    
                                                    else 
                                                    {    
                                                    if($wheel_cook==$car_exterior_wheel_data['feature_id'])
                                                    {
                                                    $class="selected";
                                                    }                                                   
                                                    else
                                                    {
                                                    $class="";
                                                    }
                                                    } 
                                                ?>
                                                <li  data-wheel="<?= base_url("admin/uploads/feature/" . $img); ?>" data-cost="13000" data-cost-inlcuded="1" onclick="document.getElementById('wheel').value='<?php echo $car_exterior_wheel_data['feature_id'];?>';">
                                                    <div class="image-wrapper-div <?php echo $class; ?>">
                                                        <img class="" src="<?php echo base_url("admin/uploads/feature/" . $img); ?>">
                                                        <div>
                                                            <i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div class="text-center"> 
                                                        <?php
                                                        if (isset($car_exterior_wheel_data['feature_desc'])) {
                                                            echo $car_exterior_wheel_data['feature_desc'];
                                                        }
                                                        ?>
                                                        <br/>
                                                        <?php
                                                        if (isset($car_exterior_wheel_data['price'])) {
                                                            echo indianFormatNumber($car_exterior_wheel_data['price']);
//                                                            echo 'Rs.' . $car_exterior_wheel_data['price'];
                                                        }
                                                        ?>
                                                    </div>
                                                </li>

                                                <?php
                                                $wl++;
                                            }
                                            }
                                            else 
                                            {
                                             ?>
                                                <li>
                                                    <div align="center" style="width:900px;font-size: 14px;">
                                                        <img style="height:100px;" src="<?php echo base_url();?>/assets/img/ComingSoon.png">
                                                        <label>Not applicable for this product because we do not have wheel information rite now.</label></div> 
                                                </li>                                                        
                                            <?php            
                                            }    
                                            ?>
                                                
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <?php
            */
            ######## ENd Here #########            
            ?>

            <div class="clearfix">
                <div class="pull-right">
                    <form action="<?php echo base_url('interior-selection/'.$main_id)?>" method="POST" style="display:none;">
                        <input type="text" id="pro_id" name="pro_id" value="<?php echo $main_id;?>"/>
                        <input type="text" id="wheel" name="wheel" value="<?php if($default_week!='') { echo $default_week;} else { echo $config_selected[0]['wheel']; }?>"/>
                        <input type="hidden" id="color" name="color" value="<?php if($default_color!='') { echo $default_color;} else { echo $config_selected[0]['color'];}?>"/>
                        <button type="submit" name="submit" class="btn btn-maroon confuration">Save and Continue</button>
                    </form>
                    <!--<a href="<?php // echo site_url('interior-selection/car-' . $id) ?>" class="btn btn-maroon">Save and  Continue</a>-->
                </div>
            </div>
        </div>
    </div>
</div><!-- /.container -->
<script>
//allexterior();    
/*
function allexterior()    
{
var color=$('#color').val();    
var wheel=$('#wheel').val();    
document.cookie="color="+color;
document.cookie="wheel="+wheel;
//alert(document.cookie);
//alert(color);    
//alert(wheel);    
}
*/
</script>    
