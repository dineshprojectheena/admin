<?php
foreach ($user_details as $user_details_data)
//print_r($user_details_data);    
    
    ?>
<div class="container" id="edit_profile">
    <div class="row">
    <div class="col-md-12">
        <div class="col-md-4 col-cust-left">
                <h3>
                    <!--<i class="fa fa-location-arrow"></i>-->
                    User Profile</h3>                
            <?php
            $file = base_url() . 'uploads/user/' . $user_details_data['fb_id'] . '.jpg';
            ?>
            <img src="<?php echo $file; ?>" class="img-responsive" />
<!--            <div class="social">
                <div class="social">
                    <a href="#" class="fb tiphere" data-toggle="tooltip" data-placement="top" title="Facebook">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="#" class="tw tiphere" data-toggle="tooltip" data-placement="top" title="Twitter">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="#" class="gp tiphere" data-toggle="tooltip" data-placement="top" title="Google Plus">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                    <a href="#" class="ln tiphere" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                        <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                </div>
            </div>-->


        </div>


        <div class="col-md-8">
            <form id="contact-us" class="form-horizontal" role="form" action="<?php echo site_url("edit_profile_upload"); ?>" method="POST">
                <?php if (isset($alert_msg)) { ?>
                    <div role="alert" class="alert alert-success">
                        <div class="twelve columns mobile-four alert-box secondary">
                            <p class="error"><?= $alert_msg ?></p>
                            <a href="" class="close">&times;</a>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group" style="display:none;">
                    <label for="username" class="col-sm-4 control-label">Username</label>
                    <div class="col-sm-8">
                        <input type="text" name="username"  readonly="true" class="form-control" id="username" value="<?php echo $user_details_data['username']; ?>" onblur="usernamexist11();" placeholder="Name" required>
                        <input type="hidden" name="username2" class="form-control" id="username2" value="<?php echo $user_details_data['username']; ?>"  placeholder="Name" required>
                        <input type="hidden" name="id"  id="id" value="<?php echo $user_details_data['user_id']; ?>" placeholder="id" >
                        <div class="error" id="error_msg"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-4 control-label">First Name</label>
                    <div class="col-sm-8">

                        <input type="text" name="f_name" class="form-control" id="f_name" value="<?php echo $user_details_data['firstname']; ?>" placeholder="First name" required>
                    </div>

                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-4 control-label" >Last Name</label>
                    <div class="col-sm-8">		

                        <input type="text" name="l_name" class="form-control" id="l_name" value="<?php echo $user_details_data['lastname']; ?>" placeholder="Last name" required>
                    </div>	

                </div>

                <div class="form-group">
                    <label for="number" class="col-sm-4 control-label">Phone Number</label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" name="number" value="<?php echo $user_details_data['telephone']; ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-4 control-label">Email</label>
                    <div class="col-sm-8">
                        <input type="text" name="email" class="form-control" id="email" value="<?php echo $user_details_data['email']; ?>" required>
                    </div>
                </div>


                <div class="form-group">
                    <label for="enquiry" class="col-sm-4 control-label">Address</label>
                    <div class="col-sm-8">
                        <textarea name="enquiry" class="form-control" rows="3" ><?php echo $user_details_data['address']; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <?php
                    if (isset($alert_msgs)) {
                        ?>
                        <div class="success"><?php print_r($alert_msgs); ?></div>    
                        <?php
                    }
                    ?>
                    <div class="col-sm-12">
                        <button type="submit"  id="update_button" class="btn btn-maroon pull-right">Update Profile</button>
                    </div>
                </div>

<!--                <div class="col-sm-12 text-right">
                    <?php
                    if (isset($alert_msgs)) {
                        ?>
                        <div class="success"><?php print_r($alert_msgs); ?></div>    
                        <?php
                    }
                    ?>

                    <button type="submit"  id="update_button" class="btn btn-maroon pull-right">Update Profile</button>
                </div>-->
            </form>
        </div>
    </div>
    </div>
</div>
