<div class="container user-review-content">
    <div class="row">
        <?php
        if(isset($id)){
            ?>
            <div class="col-md-12">
                <?php
                error_reporting(0);
                $results = validation_errors();
                if (!empty($results)) {
                    ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <strong>
                            <i class="icon-remove"></i>
                            Warning:
                        </strong>
                        Please check the form carefully for errors!       
                    </div>
                    <?php
                }
                ?>
                <p>Hi <strong><?php print_r($session['firstname']); ?></strong>, give us your review.</p>
                <div class="container-fluid">
                    <form class="form-horizontal" role="form" action="<?php echo site_url("review"); ?>" method="POST" id="my_form">
                        <div class="row">
                            <?php
                            foreach ($pro_details as $pro_details_res)                                
                            ?>
                            <div class="col-md-5 car-img-div">
                                <?php 
                                $pro_image= img_url('uploads/product/' . $pro_details_res['pro_image']);
                                ?>
                                <img class="img-left img-responsive img-holder" src="<?php echo $pro_image; ?>">
                                <div class="rating-div">
                                    <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="User Ratings"><i class="fa fa-users"></i></span>
                                    <span class="text-golden">
                                        <?php
                                        foreach ($user_review_count as $user_pro_count)
                                            $user_star = $user_pro_count['total'];
                                        $exp = explode('.', $user_star);
                                        $user_point = $exp[1];
                                        if ($user_point > 00) {
                                            $half = '-half-full';
                                        } else if ($user_point == 00) {
                                            $half = '-o';
                                        } else {
                                            $half = '';
                                        }

                                        if (empty($user_star)) {
                                            ?>
                                            <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        }

                                        if ($exp[0] == 1) {
                                            ?>
                                            <i class="fa fa-star"></i><?php ?>
                                            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        } else if ($exp[0] == 2) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 3) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 4) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                        }
					else if ($user_star == 5) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
					    <i class="fa fa-star"></i>
					    <i class="fa fa-star"></i>
					    <i class="fa fa-star"></i>	
					<?php
                                        }
                                        ?>

                                    </span>
                                </div>
                                <div class="rating-div">
                                    <span class="rater-span tiphere" data-toggle="tooltip" data-placement="right" title="Expert Ratings"><i class="fa fa-user"></i></span>
                                    <span class="text-maroon">
                                        <?php
                                        foreach ($exp_review_count as $exp_pro_count)
                                            $exp_star = $exp_pro_count['total'];
                                        $exp = explode('.', $exp_star);
                                        $exp_point = $exp[1];
                                        if ($exp_point > 00) {
                                            $half = '-half-full';
                                        } else if ($exp_point == 00) {
                                            $half = '-o';
                                        } else {
                                            $half = '';
                                        }

                                        if (empty($exp_star)) {
                                            ?>
                                            <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        }

                                        if ($exp[0] == 1) {
                                            ?>
                                            <i class="fa fa-star"></i><?php ?>
                                            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        } else if ($exp[0] == 2) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 3) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 4) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                        }
					else if ($exp_star == 5) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
					    <i class="fa fa-star"></i>
					    <i class="fa fa-star"></i>
					    <i class="fa fa-star"></i>	
					<?php
                                        }
                                        ?>    
                                    </span>
                                </div>
                            </div>
                                <div class="col-md-6 pull-left">

                                <div class="clearfix">
                                    <div class="pull-left">
                                        <h4 class="car-heading"><?php echo $pro_ext[0]['brand_name'] . ' ' . $pro_ext[0]['model_name']; ?></h4></h4>
                                    </div>
                                    <div class="pull-left" style="padding-top: 5px;">
                                        <input type="hidden" name="user_id" id="user_id" value="<?php print_r($session['user_id']); ?>" />
                                        <input type="hidden" name="pro_id" id="pro_id" value="<?php
                                        if ($_POST['pro_id']) {
                                            echo $_POST['pro_id'];
                                        } else {
                                            echo $id;
                                        }
                                        ?>" />
                                        <input type="hidden" name="main_id" id="main_id" value="<?php
                                        if ($_POST['main_id']) {
                                            echo $_POST['main_id'];
                                        } else {
                                            echo $main_id;
                                        }
                                        ?>" />
                                        <?php
//                                        print_r($pro_ext);
                                        ?>
                                        <select class="selectpicker" style="display:inline;" id="rating_variant" onchange="rating_variants();" title='Select Variant'>
                                            <option value="">Select Variant</option>
                                            <?php
                                            foreach ($pro_ext as $pro_ext_res) {
                                                ?>
                                                <option <?php
                                                if ($id == $pro_ext_res['variant_id']) {
                                                    echo 'selected';
                                                }
                                                ?> value="<?php echo $pro_ext_res['variant_id']; ?>"><?php echo $pro_ext_res['pro_name_comp']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                        <script>
                                            function rating_variants()
                                            {
                                                  var rating_variant = $('#rating_variant').val();
//                                                
                                                if (rating_variant != '')
                                                {
                                                    var rating_variant_text = $("#rating_variant option:selected").text();
                                                    var value = rating_variant_text.replace(/ /g, "-");
                                                    var con_var = value + '-' + rating_variant;
                                                    window.location.href = "<?php echo site_url('user-review'); ?>/" + con_var;
                                                }
                                            }
                                        </script>
                                    </div>
                                </div>
                                <p>
                                    <?php echo $pro_details_res['pro_detail']; ?>
                                </p>
                            </div>
                                <div class="col-md-6 pull-left" align="left">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Heading</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="comment" id="comment" value="<?php
                                        if (isset($_POST['comment'])) {
                                            echo $_POST['comment'];
                                        }
                                        ?>" class="form-control" placeholder="Title of your comment">
                                        <span class="error" id="err_heading"><?php echo form_error('comment'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user_overview" class="col-sm-3 control-label">I Like <i class="fa fa-thumbs-up"></i></label>
                                    <div class="col-sm-8">
                                        <textarea type="text"  name="like" id="like" class="form-control" placeholder="I Like"><?php
                                            if (isset($_POST['like'])) {
                                                echo $_POST['like'];
                                            }
                                            ?></textarea>
                                        <span class="error" id="err_like"><?php echo form_error('like'); ?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user_overview" class="col-sm-3 control-label">I Don't Like  <i class="fa fa-thumbs-down"></i> </label>
                                    <div class="col-sm-8">
                                        <textarea type="text" name="dislike" id="dislike" class="form-control" placeholder="I Dont't Like"><?php
                                            if (isset($_POST['dislike'])) {
                                                echo $_POST['dislike'];
                                            }
                                            ?></textarea>
                                        <span class="error" id="err_dislike"><?php echo form_error('dislike'); ?></span>
                                    </div>
                                </div>
                                </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="car-review-table table table-bordered table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th width="10%"></th>
                                                <th>Category</th>
                                                <th>Rating</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $rtypes = array('', 'Comfort', 'Space', 'Safety', 'Design', 'Engine', 'Fuel Economy', 'Ride & Handling', 'Maintainence', 'Value For Money');
                                            for ($i = 1, $j = 0; $i < count($rtypes); $i++, $j++) {
                                                ?>
                                                <tr>
                                                    <td><?= $i ?></td>
                                                    <td><?= $rtypes[$i] ?></td>
                                                    <td>
                                                        <input type="number" name="rating[]" id="rating" autocomplete="off" data-show-clear="false" data-size="xs" data-show-caption="false" class="rating">
                                                    </td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 rating-rules">
                                <p><b>Rules & Guidelines:</b></p>
                                <p>When providing feedback, posting information, or discussing a car, whether it’s negative or positive, please make sure you are being relevant, constructive and polite.</p>
                                <p>Please note that we reserve the right to change, edit or delete any content at any time, if we feel it is inappropriate, abusive, or incorrect.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <?php
                                if (isset($session['username'])) {
                                    ?>
                                <button id="review_submit" name="review_submit" onclick="return checkreview();"  class="btn btn-maroon">Submit</button>
                                    <?php
                                } else {
                                    ?>
                                    <a data-target="#loginModal" data-toggle="modal" href="#" class="btn btn-maroon">Submit</a>
                                    <?php
                                }
                                ?>                            
                            </div>
                        </div>
                    </form>    

                </div>
            </div>
            <?php
        } else {
            redirect('404');
            ?>
            <?php
        }
        ?>    
    </div>

</div><!-- /.container -->
<script>
function checkreview()
{
var comment=$("#comment").val();
var like=$("#like").val();
var dislike=$("#dislike").val();
//var category=$("name='rating'").val();

//$('input[name="rating[]"]').each(function(){
//  
//});

if(comment=='')
{
$("#err_like").html('');    
$("#err_dislike").html('');    
$("#err_heading").html('The heading field is required.');    
return false;
}
else if(like=='')
{
    $("#err_heading").html('');    
$("#err_dislike").html('');    

$("#err_like").html('The I Like field is required.');    
return false;
}
if(dislike=='')
{
$("#err_heading").html('');    
$("#err_like").html('');  
$("#err_dislike").html('The I Dislike field is required.');    
return false;
}
return;

}
</script>
