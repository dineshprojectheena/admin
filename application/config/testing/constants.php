<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|--------------------------------------------------------------------------
| MNC Constants
|--------------------------------------------------------------------------
|
|
*/
//define('ADMIN_PANEL_LINK', 'http://localhost/mncAdmin/dashboard.php');

define('MNC_ADDRESS', 'MNC Corp,<br>Oshiwira,Mumbai');
define('MNC_ADMIN_EMAIL', 'webdeveloperdinesh@gmail.com');
define('MNC_SUPPORT_EMAIL', 'webdeveloperdiensh@gmail.com');
define('Mandrill_key', '90dPYBRmBMvozejaPj1vjA');
define('Extra_benefits', 'Extra benefits');


define('SPARKPOST_KEY', 'c41537a80aa53bcc307fd6d769e9ce43fc3c0e85');
define('SPARKPOST_URL', 'https://api.sparkpost.com/api/v1/transmissions/');

/* End of file constants.php */
/* Location: ./application/config/constants.php */