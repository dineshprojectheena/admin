<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "stitcher";
$route['404_override'] = '';

/* ----------------------------------------------------------------------
  STITCHER ROUTES
  ---------------------------------------------------------------------- */
$route['get_gallery'] = "gallery/all_gallery";
$route['coupon'] = "stitcher/coupon";
$route['affiliate'] = "stitcher/affiliate";

$route['accessory_master'] = "stitcher/accessory_master";
$route['upload_accessory_master'] = "stitcher/upload_accessory_master";


$route['top-affiliate'] = "stitcher/top_affiliate";
$route['upload_affiliate'] = "stitcher/upload_affiliate";
$route['upload_coupon_data'] = "stitcher/upload_coupon_data";
$route['change_password'] = "stitcher/change_password";


$route['landing_page'] = "stitcher/landing_page";

$route['upload_landing_data'] = "stitcher/upload_landing_data";
$route['delete_landing_data/(:any)'] = "stitcher/delete_landing_data/$1";

$route['slider_data'] = "stitcher/slider";
$route['get_brand'] = "stitcher/get_brand";
$route['del_brand'] = "stitcher/del_brand";
$route['banner_upload'] = "stitcher/banner_upload";


$route['gallery_data'] = "stitcher/gallery_data";


$route['faq'] = "stitcher/faq";
$route['faq_upload'] = "stitcher/faq_upload";


$route['news'] = "stitcher/news";
$route['news_upload'] = "stitcher/news_upload";
$route['get_update_news'] = "stitcher/get_update_news";

$route['brand_group'] = "stitcher/brand_group";
$route['upload_brand_group'] = "stitcher/upload_brand_group";

$route['group_upload'] = "stitcher/group_upload";

$route['dealer'] = "dealer_prices/dealer";
$route['dealer_upload'] = "dealer_prices/dealer_upload";
$route['dealer_list_prices'] = "dealer_prices/dealer_list_prices";
$route['dealer_hot_deals'] = "dealer_prices/dealer_hot_deals";
$route['dealer_prices'] = "dealer_prices/dealer_price";
$route['dealer_hot_deal'] = "dealer_prices/dealer_hot_deal";
$route['dealer-join-us'] = "dealer_prices/dealer_join_us";
$route['login'] = "stitcher/admin_login";
$route['home'] = "stitcher/home";





$route['news-update'] = "stitcher/update_news";
$route['brand_group'] = "stitcher/brand_group";
$route['brand'] = "stitcher/brand";
$route['information'] = "stitcher/information";
$route['stitcher/(:any)'] = "stitcher/$1";



$route['accessory_seller'] = "module/accessory_seller";
$route['acc_sell_upload'] = "module/acc_sell_upload";

$route['service_seller'] = "module/service_seller";
$route['ser_sell_upload'] = "module/ser_sell_upload";

$route['model'] = "module/model";
$route['model_upload'] = "module/model_upload";

$route['body_color'] = "module/body_color";
$route['body_color_upload'] = "module/body_color_upload";

$route['wheel'] = "module/wheel";
$route['wheel_upload'] = "module/wheel_upload";

$route['rating'] = "expert_rating/rating";
$route['rating_upload'] = "expert_rating/rating_upload";

### Body Dashboard
$route['body_dashboard']="module/body_dashboard";
$route['body_dashboard_upload'] = "module/body_dashboard_upload";


### product 
$route['product']="product/product_load";
$route['product_upload']="product/product_upload";
$route['product_video']="product/product_video";
$route['upcoming_variant']="product/upcoming_variant";
$route['upcoming_variant_order']="order/upcoming_variant_order";


### Content Added By Prateek -Price Controller
$route['exShowroom']="price/exShowroom";
$route['exShowroom_upload']="price/exShowroom_upload";

$route['mnc']="price/mnc";
$route['mnc_upload']="price/mnc_upload";

$route['onRoad']="price/onRoad";
$route['onRoad_upload']="price/onRoad_upload";

$route['lastestdeal']="deal/lastestdeal";
$route['lastestdeal_upload']="deal/deal_upload";

$route['groupdeal']="deal/groupdeal";
$route['brandeal_upload']="deal/brandeal_upload";


### featured car comaprison 
$route['home_content']="home_struture/index";
$route['car_comparison']="home_struture/car_comparison";
$route['get_car_comparison']="home_struture/get_car_comparison";


### featured expert reviews
$route['home_expert_reviews']="expert_reviews/index";
$route['expert_review_upload']="expert_reviews/expert_review_upload";
$route['get_expert_review']="expert_reviews/get_expert_review";

### featured latest arrivals
$route['home_latest_arrivals']="latest_arrivals/index";
$route['latest_arrivals_upload']="latest_arrivals/latest_arrivals_upload";
$route['get_latest_arrivals']="latest_arrivals/get_latest_arrivals";

### featured my new car works
$route['top_selling_cars']="top_selling_cars/index";
$route['top_selling_cars_upload']="top_selling_cars/top_selling_cars_upload";
//$route['get_my_new_car_works']="my_new_car_works/get_my_new_car_works";

### featured my new car works
$route['gallery']="product/gallery";
$route['gallery_upload']="product/gallery_upload";
$route['bulkproduct']="product/bulkproduct";
$route['bulkseo']="bulk/bulkseo";
$route['bulkrating']="bulk/bulkrating";
$route['bulkprice']="bulk/bulkprice";
$route['bulkcoupon']="bulk/bulkcoupon";
$route['bulkcouponupload']="bulk/bulkcouponupload";
$route['bulkhotdeal']="bulk/bulkhotdeal";
$route['bulk_hot_deal']="bulk/bulk_hot_deal";
$route['bulkgroupdeal']="bulk/bulkgroupdeal";
$route['bulk_upload_price_data']="bulk/bulk_upload_price_data";
$route['bulkaccessory']="bulk/bulkaccessory";
$route['bulkaccessoryupload']="bulk/bulkaccessoryupload";

$route['bulkaccessory_hotdeal']="bulk/bulkaccessory_hotdeal";
$route['bulk_hotdeal_accessory_upload']="bulk/bulk_hotdeal_accessory_upload";
$route['mosf']="bulk/bulk_mosf";
$route['crud']="mgo_crud/crud";
$route['mgo_upload']="mgo_crud/mgo_upload";

### featured my new car works
$route['order-detail/(:any)'] = "order/order_details/$1";
$route['update-status'] = "order/status_update";
$route['update-dealer-status'] = "order/update_dealer_status";
$route['lastestorder'] = "order/lastestorder";
$route['order-to-dealer'] = "order/order_to_dealer";
$route['order-to-dealer-map/(:any)']="order/order_dealer_map/$1";


$route['test-drive'] = "test_drive/test_drive_data";
$route['test_drive_assign/(:any)'] = "test_drive/test_drive_assign/$1";
//$route['order-detail']="order/order_details";






/* End of file routes.php */
/* Location: ./application/config/routes.php */
