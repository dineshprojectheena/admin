<?php

/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if(base_url()=='http://localhost/mynewcar/')
{
global $apiConfig;
$apiConfig = array(
    // True if objects should be returned by the service classes.
    // False if associative arrays should be returned (default behavior).
    'use_objects' => false,
    // The application_name is included in the User-Agent HTTP header.
    'application_name' => 'MNC-Local',
    // OAuth2 Settings, you can get these keys at https://code.google.com/apis/console
    'oauth2_client_id' => '355304607782-iequrbff70796gbpltsmkjpqkfktab6s.apps.googleusercontent.com',
    'oauth2_client_secret' => 'Ia4SivcKL-fMyz1VokjSOl3s',
    'oauth2_redirect_uri' => 'http://localhost/mynewcar/authorization/google_login',
    // The developer key, you get this at https://code.google.com/apis/console
    'developer_key' => '',
    // Site name to show in the Google's OAuth 1 authentication screen.
    'site_name' => 'AIzaSyCS0A1MXGQDO8V3a2mcCDnBDCo3bNekXH4',
    // Which Authentication, Storage and HTTP IO classes to use.
    'authClass' => 'Google_OAuth2',
    'ioClass' => 'Google_CurlIO',
    'cacheClass' => 'Google_FileCache',
    // Don't change these unless you're working against a special development or testing environment.
    'basePath' => 'https://www.googleapis.com',
    // IO Class dependent configuration, you only have to configure the values
    // for the class that was configured as the ioClass above
    'ioFileCache_directory' =>
    (function_exists('sys_get_temp_dir') ?
            sys_get_temp_dir() . '/Google_Client' :
            '/tmp/Google_Client'),
    // Definition of service specific values like scopes, oauth token URLs, etc
    'services' => array(
        'analytics' => array('scope' => 'https://www.googleapis.com/auth/analytics.readonly'),
        'calendar' => array(
            'scope' => array(
                "https://www.googleapis.com/auth/calendar",
                "https://www.googleapis.com/auth/calendar.readonly",
            )
        ),
        'books' => array('scope' => 'https://www.googleapis.com/auth/books'),
        'latitude' => array(
            'scope' => array(
                'https://www.googleapis.com/auth/latitude.all.best',
                'https://www.googleapis.com/auth/latitude.all.city',
            )
        ),
        'moderator' => array('scope' => 'https://www.googleapis.com/auth/moderator'),
        'oauth2' => array(
            'scope' => array(
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email',
            )
        ),
        'plus' => array('scope' => 'https://www.googleapis.com/auth/plus.me'),
        'siteVerification' => array('scope' => 'https://www.googleapis.com/auth/siteverification'),
        'tasks' => array('scope' => 'https://www.googleapis.com/auth/tasks'),
        'urlshortener' => array('scope' => 'https://www.googleapis.com/auth/urlshortener')
    )
);    
}    
else if(base_url()=='https://mncbeta.com/')
{
global $apiConfig;
$apiConfig = array(
    // True if objects should be returned by the service classes.
    // False if associative arrays should be returned (default behavior).
    'use_objects' => false,
    // The application_name is included in the User-Agent HTTP header.
    'application_name' => 'MNC-Beta',
    // OAuth2 Settings, you can get these keys at https://code.google.com/apis/console
    'oauth2_client_id' => '126618676985-0k3gcj1q2n7cqlnpkus1hriifc5t3ra1.apps.googleusercontent.com',
    'oauth2_client_secret' => 'zXBz09-ydA-M7E74gJIXta7M',
    'oauth2_redirect_uri' => 'https://mncbeta.com/authorization/google_login',
    // The developer key, you get this at https://code.google.com/apis/console
    'developer_key' => '',
    // Site name to show in the Google's OAuth 1 authentication screen.
    'site_name' => 'AIzaSyCS0A1MXGQDO8V3a2mcCDnBDCo3bNekXH4',
    // Which Authentication, Storage and HTTP IO classes to use.
    'authClass' => 'Google_OAuth2',
    'ioClass' => 'Google_CurlIO',
    'cacheClass' => 'Google_FileCache',
    // Don't change these unless you're working against a special development or testing environment.
    'basePath' => 'https://www.googleapis.com',
    // IO Class dependent configuration, you only have to configure the values
    // for the class that was configured as the ioClass above
    'ioFileCache_directory' =>
    (function_exists('sys_get_temp_dir') ?
            sys_get_temp_dir() . '/Google_Client' :
            '/tmp/Google_Client'),
    // Definition of service specific values like scopes, oauth token URLs, etc
    'services' => array(
        'analytics' => array('scope' => 'https://www.googleapis.com/auth/analytics.readonly'),
        'calendar' => array(
            'scope' => array(
                "https://www.googleapis.com/auth/calendar",
                "https://www.googleapis.com/auth/calendar.readonly",
            )
        ),
        'books' => array('scope' => 'https://www.googleapis.com/auth/books'),
        'latitude' => array(
            'scope' => array(
                'https://www.googleapis.com/auth/latitude.all.best',
                'https://www.googleapis.com/auth/latitude.all.city',
            )
        ),
        'moderator' => array('scope' => 'https://www.googleapis.com/auth/moderator'),
        'oauth2' => array(
            'scope' => array(
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email',
            )
        ),
        'plus' => array('scope' => 'https://www.googleapis.com/auth/plus.me'),
        'siteVerification' => array('scope' => 'https://www.googleapis.com/auth/siteverification'),
        'tasks' => array('scope' => 'https://www.googleapis.com/auth/tasks'),
        'urlshortener' => array('scope' => 'https://www.googleapis.com/auth/urlshortener')
    )
);      
}    
else
{    
global $apiConfig;
$apiConfig = array(
    // True if objects should be returned by the service classes.
    // False if associative arrays should be returned (default behavior).
    'use_objects' => false,
    // The application_name is included in the User-Agent HTTP header.
    'application_name' => 'Mynewcar',
    // OAuth2 Settings, you can get these keys at https://code.google.com/apis/console
    'oauth2_client_id' => '374702306029-kdj9legrisrh9eien1mc0g2vrocrqnka.apps.googleusercontent.com',
    'oauth2_client_secret' => 'NtXT0zWCGyPCUwvPlxcZN-ZE',
    'oauth2_redirect_uri' => 'https://mynewcar.in/authorization/google_login',
    // The developer key, you get this at https://code.google.com/apis/console
    'developer_key' => '',
    // Site name to show in the Google's OAuth 1 authentication screen.
    'site_name' => 'AIzaSyCS0A1MXGQDO8V3a2mcCDnBDCo3bNekXH4',
    // Which Authentication, Storage and HTTP IO classes to use.
    'authClass' => 'Google_OAuth2',
    'ioClass' => 'Google_CurlIO',
    'cacheClass' => 'Google_FileCache',
    // Don't change these unless you're working against a special development or testing environment.
    'basePath' => 'https://www.googleapis.com',
    // IO Class dependent configuration, you only have to configure the values
    // for the class that was configured as the ioClass above
    'ioFileCache_directory' =>
    (function_exists('sys_get_temp_dir') ?
            sys_get_temp_dir() . '/Google_Client' :
            '/tmp/Google_Client'),
    // Definition of service specific values like scopes, oauth token URLs, etc
    'services' => array(
        'analytics' => array('scope' => 'https://www.googleapis.com/auth/analytics.readonly'),
        'calendar' => array(
            'scope' => array(
                "https://www.googleapis.com/auth/calendar",
                "https://www.googleapis.com/auth/calendar.readonly",
            )
        ),
        'books' => array('scope' => 'https://www.googleapis.com/auth/books'),
        'latitude' => array(
            'scope' => array(
                'https://www.googleapis.com/auth/latitude.all.best',
                'https://www.googleapis.com/auth/latitude.all.city',
            )
        ),
        'moderator' => array('scope' => 'https://www.googleapis.com/auth/moderator'),
        'oauth2' => array(
            'scope' => array(
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email',
            )
        ),
        'plus' => array('scope' => 'https://www.googleapis.com/auth/plus.me'),
        'siteVerification' => array('scope' => 'https://www.googleapis.com/auth/siteverification'),
        'tasks' => array('scope' => 'https://www.googleapis.com/auth/tasks'),
        'urlshortener' => array('scope' => 'https://www.googleapis.com/auth/urlshortener')
    )
);
}