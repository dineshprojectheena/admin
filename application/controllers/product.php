<?php

class Product extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('product_data');
        $this->load->model('common');
        $this->load->model('experts_rating');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library("datatables");
//        $this->load->helper("datatables");
    }

    /* Product Page master data section start here */

    function upcoming_variant() {

        $data['title'] = 'Upcoming Cars';
        $this->load->view('upcoming_cars', $data);
    }

    function upcoming_variant_update(){
        $id=explode(",",$_GET['id']);
        $cond=$_GET['status'];

        $added_date=date("Y-m-d h:i:s");
        if(!empty($id))
        {
        foreach($id as $main_id)
        {
            
        $main_id;
        if($cond==3 || $cond==4)
        {    
        $pro=$this->db->query("select * from variant_upcoming_activation where variant_id='$main_id'")->result_array();
        if(count($pro)==1)
        {            
        if($cond==3)
        {    
        $update=$this->db->query("update variant_upcoming_activation set status='y' where variant_id='$main_id'");    
        }        
        if($cond==4)
        {    
        $update=$this->db->query("update variant_upcoming_activation set status='n' where variant_id='$main_id'");    
        }        
        }
        else
        {            
        if($cond==3)
        {    
        $insert=$this->db->query("INSERT INTO `variant_upcoming_activation`(`id`, `variant_id`, `status`, `added_date`) VALUES ('','$main_id','y','$added_date')");    
        }        
        if($cond==4)
        {    
        $insert=$this->db->query("INSERT INTO `variant_upcoming_activation`(`id`, `variant_id`, `status`, `added_date`) VALUES ('','$main_id','n','$added_date')");    
        }        
        }    
        }    
            
        $pro=$this->db->query("select * from variant where variant_id='$main_id'")->result_array();    
//        print_r($pro);
        if($cond==1)
        {
        if(count($pro)==1)
        {
        $pro=$this->db->query("update variant set status='3',upcoming='y' where variant_id='$main_id'");    
        }
        }
        if($cond==2)
        {
        if(count($pro)==1)
        {
        $pro=$this->db->query("update variant set status='2',upcoming='y' where variant_id='$main_id'");    
        }
        }
        
        }    
        }
        if($cond==3)
        {
        $this->session->set_flashdata('status', "Sale Activated Done");    
        }
        if($cond==4)
        {
        $this->session->set_flashdata('status', "Sale De-Activation Done");    
        }
        if($cond==1)
        {
        $this->session->set_flashdata('status', "Upcoming Variant Enabled Done.");    
        }
        if($cond==2)
        {
        $this->session->set_flashdata('status', "Upcoming Variant Disbaled Done.");    
        }
        redirect('upcoming_variant');
    }

    function upcoming_results() {
        $id=$_GET['id'];
        $actionLinkBar = $this->load->view("content/report/datatable/upcoming_activation", array(), TRUE);
        $this->datatables
                ->select("variant_id,pro_name_comp,launching_date,status,(select status from variant_upcoming_activation where variant_id=variant.variant_id) as active_status")                         
                ->from("variant")
                ->where('upcoming', 'y')
                ->edit_column('status', '$1', 'check_upcoming_status(status)')
                ->edit_column('active_status', '$1', 'check_activation(active_status)')
                ->add_column("Action", $actionLinkBar, 'variant_id,active_status');
        echo $this->datatables->generate();
    }

    function product_video() {

        $data['title'] = 'Product Video';
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }
        $data['city'] = $this->common->get_city();
        $this->load->view('product_video', $data);
    }

    function video_results() {

        $actionLinkBar = $this->load->view("content/report/datatable/video_action", array(), TRUE);
        $this->datatables
                ->select("variant_video_id,(select pro_name_comp from variant where variant_id=variant_video.variant_id) as pro_name_comp,"
                        . "variant_video")
                ->from("variant_video")
//                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action", $actionLinkBar, 'variant_video_id');
        echo $this->datatables->generate();
    }

    function video_upload() {
        $data['title'] = 'Product Video';
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('brand_id', 'Brand', 'trim|required|xss_clean');
            $this->form_validation->set_rules('model_id', 'Model', 'trim|required|xss_clean');
            $this->form_validation->set_rules('all_product_name[]', 'Product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('video', 'Video', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('status', "Please Fill All Require Data");
                $this->load->view('product_video', $data);
            } else {

                $all_product_name = $this->input->post('all_product_name');
                if ($all_product_name == 'All') {
                    $brand_id = $this->input->post('brand_id');
                    $model_id = $this->input->post('model_id');
                    $pro = $this->db->query("select variant_id from variant where brand_id='$brand_id' and model_id='$model_id'")->result_array();
                    foreach ($pro as $pro_res) {
                        $pro_id = $pro_res['variant_id'];
                        $date = date('Y-m-d h:i:s');
                        $data2 = array('variant_id' => $pro_id,
                            'variant_video' => $this->input->post('video'),
                            'status' => '1',
                            'added_date' => $date);
                        $query = $this->product_data->upload_video($data2);
                    }
                } else {
                    $date = date('Y-m-d h:i:s');
                    $data2 = array('variant_id' => $all_product_name,
                        'variant_video' => $this->input->post('video'),
                        'status' => '1',
                        'added_date' => $date);
                    $query = $this->product_data->upload_video($data2);
                }


                $this->db->trans_complete();
                if ($this->db->trans_status()) {
                    $this->session->set_flashdata('status', "Data Uploded Successfully");
                }
                //$data['sucess']='Your Detail added successfully';                
                redirect('product_video');
            }
        } else if (isset($_POST['update'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('brand_id', 'Brand', 'trim|required|xss_clean');
            $this->form_validation->set_rules('model_id', 'Model', 'trim|required|xss_clean');
            $this->form_validation->set_rules('all_product_name[]', 'Product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('video', 'Video', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('status', "Please Fill All Require Data");
                $this->load->view('product_video', $data);
            } else {

                $all_product_name = $this->input->post('all_product_name');
                $variant_video_id = $this->input->post('variant_video_id');
                $date = date('Y-m-d h:i:s');
                $data2 = array('variant_id' => $all_product_name,
                    'variant_video' => $this->input->post('video'),
                    'status' => '1',
                    'added_date' => $date);
                $query = $this->product_data->update_video($data2, $variant_video_id);
                $this->db->trans_complete();
                if ($this->db->trans_status()) {
                    $this->session->set_flashdata('status', "Data Updated Successfully");
                }
                //$data['sucess']='Your Detail added successfully';                
                redirect('product_video');
            }
        } else if (isset($_POST['edit'])) {
            $variant_id = $this->input->post('id');
            $data['single_res'] = $this->db->query("select *,(select brand_id from variant where variant_id=variant_video.variant_id) as brand_id,(select model_id from variant where variant_id=variant_video.variant_id) as model_id from variant_video")->row_array();
//            print_r($data['single_res']);
            $this->load->view('product_video', $data);
        } else if (isset($_POST['delete'])) {
            $variant_id = $this->input->post('id');
            $data['results_up'] = $this->db->query("delete from variant_video where variant_video_id='$variant_id'");
            $this->session->set_flashdata('status', "Data Deleted Successfully");
            redirect('product_video');
        } else {
            redirect('product_video');
        }
    }

    function video_delete() {
        $exp_id = explode(",", $_GET['id']);

        foreach ($exp_id as $id) {
            $data['results_up'] = $this->db->query("delete from variant_video where variant_video_id='$id'");
        }
        $this->session->set_flashdata('status', "Data Deleted Successfully");
        redirect('product_video');
    }

    function product_load() {

        $data['title'] = 'Product';
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        $data['body_color'] = $this->product_data->get_body_color();
        $data['body_wheel'] = $this->product_data->get_body_wheel();
        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }
        $data['city'] = $this->common->get_city();
        $this->load->view('product', $data);
    }

    function product_result() {
//        $this->load->library("datatables");
//        $this->load->helper("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/product_all_action", array(), TRUE);
        $this->datatables
                ->select("variant_id,pro_name_comp,(select model_name from model where model_id=variant.model_id) as model_name,"
                        . "(select brand_name from brand where brand_id=variant.brand_id) as brand_name,status")
                ->from("variant")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action", $actionLinkBar, 'variant_id');
        echo $this->datatables->generate();
    }

    function product_upload() {
        $data['city'] = $this->common->get_city();
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('model_id', 'Model', 'trim|required|xss_clean');
            $this->form_validation->set_rules('brand_id', 'Brand', 'trim|required|xss_clean');
            $this->form_validation->set_rules('pro_name', 'Product name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('pro_type', 'Product type', 'trim|required|xss_clean');
            $this->form_validation->set_rules('pro_detail', 'Product detail', 'trim|required|xss_clean');
            $this->form_validation->set_rules('fuel_type', 'Fuel type', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('body_seating_capacity', 'Seating Capacity', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->load->view('product', $data);
            } else {
                $date = date('Y-m-d h:i:s');
                foreach ($data['model'] as $model_data) {
                    if ($this->input->post('model_id') == $model_data['model_id']) {
                        $model_name = $model_data['model_name'];
                    }
                }
                foreach ($data['brand'] as $brand_data) {
                    if ($this->input->post('brand_id') == $brand_data['brand_id']) {
                        $brand_name = $brand_data['brand_name'];
                    }
                }
                $brand_name;
                $model_name;
                $pro_name_comp = $brand_name . ' ' . $model_name . ' ' . $this->input->post('pro_name');

                $data2 = array('body_no_of_doors' => $this->input->post('body_no_of_doors'),
                    'body_seating_capacity' => $this->input->post('body_seating_capacity'),
                    'engine_capacity' => $this->input->post('engine_capacity'),
                    'engine_no_of_cylinder' => $this->input->post('engine_no_of_cylinder'),
                    'engine_no_of_valves' => $this->input->post('engine_no_of_valves'),
                    'engine_bore_stroke' => $this->input->post('engine_bore_stroke'),
                    'engine_compression_ratio' => $this->input->post('engine_compression_ratio'),
                    'engine_maximum_power' => $this->input->post('engine_maximum_power'),
                    'engine_maximum_torque' => $this->input->post('engine_maximum_torque'),
                    'engine_fuel_distribution' => $this->input->post('engine_fuel_distribution'),
                    'engine_air_charging' => $this->input->post('engine_air_charging'),
                    'engine_emission' => $this->input->post('engine_emission'),
                    'transmission_drive' => $this->input->post('transmission_drive'),
                    'transmission_clutch_type' => $this->input->post('transmission_clutch_type'),
                    'tranmssion_first_gear_ratio' => $this->input->post('tranmssion_first_gear_ratio'),
                    'transmission_second_gear_ratio' => $this->input->post('transmission_second_gear_ratio'),
                    'transmission_third_gear_ratio' => $this->input->post('transmission_third_gear_ratio'),
                    'transmission_fourth_gear_ratio' => $this->input->post('transmission_fourth_gear_ratio'),
                    'transmission_fifth_gear_ratio' => $this->input->post('transmission_fifth_gear_ratio'),
                    'transmission_sixth_gear_ratio' => $this->input->post('transmission_sixth_gear_ratio'),
                    'transmission_reverse_gear_ratio' => $this->input->post('transmission_reverse_gear_ratio'),
                    'transmission_final_drive_ratio' => $this->input->post('transmission_final_drive_ratio'),
                    'dimensions_mm_length' => $this->input->post('dimensions_mm_length'),
                    'dimensions_mm_width' => $this->input->post('dimensions_mm_width'),
                    'dimensions_mm_height' => $this->input->post('dimensions_mm_height'),
                    'dimensions_mm_weelbase' => $this->input->post('dimensions_mm_weelbase'),
                    'dimensions_mm_tread_front' => $this->input->post('dimensions_mm_tread_front'),
                    'dimensions_mm_tread_rear' => $this->input->post('dimensions_mm_tread_rear'),
                    'dimensions_mm_ground_clearance' => $this->input->post('dimensions_mm_ground_clearance'),
                    'kerb_weight' => $this->input->post('kerb_weight'),
                    'gross_vehicle_weight' => $this->input->post('gross_vehicle_weight'),
                    'brakes_front' => $this->input->post('brakes_front'),
                    'brakes_rear' => $this->input->post('brakes_rear'),
                    'streeing_type' => $this->input->post('streeing_type'),
                    'streeing_power_type' => $this->input->post('streeing_power_type'),
                    'streeing_turn_radius' => $this->input->post('streeing_turn_radius'),
                    'suspension_front' => $this->input->post('suspension_front'),
                    'suspension_rear' => $this->input->post('suspension_rear'),
                    'tyre_size' => $this->input->post('tyre_size'),
                    'tyre_wheel_type' => $this->input->post('tyre_wheel_type'),
                    'fuel_tank_capacity1' => $this->input->post('fuel_tank_capacity1'),
                    'boot_capacity1' => $this->input->post('boot_capacity1'),
                    'no_seating_rows' => $this->input->post('no_seating_rows'),
                    'bootspace' => $this->input->post('bootspace'),
                    'engine_types' => $this->input->post('engine_types'),
                    'displacement' => $this->input->post('displacement'),
                    'alternate_fuel' => $this->input->post('alternate_fuel'),
                    'mileage' => $this->input->post('mileage'),
                    'bore_stroke' => $this->input->post('bore_stroke'),
                    'comparision_ratio' => $this->input->post('comparision_ratio'),
                    'valve_cylinder' => $this->input->post('valve_cylinder'),
                    'fuel_system' => $this->input->post('fuel_system'),
                    'drivetrain' => $this->input->post('drivetrain'),
                    'no_gear' => $this->input->post('no_gear'),
                    'sport_mode' => $this->input->post('sport_mode'),
                    'dual_cluth' => $this->input->post('dual_cluth'),
                    'rear_tyre' => $this->input->post('rear_tyre'),
                    'front_tyres' => $this->input->post('front_tyres'),
                    'cylinders' => $this->input->post('cylinders'),
                    'seat_width' => $this->input->post('seat_width'),
                    'wheel_size' => $this->input->post('wheel_size'),
                    'spare_type' => $this->input->post('spare_type'),
                    'trunk_volume' => $this->input->post('trunk_volume'),
                    'top_speed' => $this->input->post('top_speed'),
                    'acceleration' => $this->input->post('acceleration'),
                    'standard_warranty' => $this->input->post('standard_warranty'),
                    'rear_seat_legroom' => $this->input->post('rear_seat_legroom'),
                    'headroom' => $this->input->post('headroom'),
                    'added_date' => $date);
                $img_name = '';
                if (isset($_FILES['product_images'])) {
                    $config2['upload_path'] = 'uploads/product/';
                    $config2['allowed_types'] = 'gif|jpg|png';
                    $config2['max_size'] = '5200';
                    $this->load->library('upload', $config2);
                    $img_name = $this->upload->do_uploads('product_images');
                    if ($img_name == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php

                        exit;
                    }
                }

                $added_date = date('y-m-d H:i:s');
                $data = array(
                    'model_id' => $this->input->post('model_id'),
                    'pro_name' => $this->input->post('pro_name'),
                    'pro_name_comp' => $pro_name_comp,
                    'variant' => $this->input->post('vairant'),
                    'pro_type' => $this->input->post('pro_type'),
                    'pro_image' => $img_name,
                    'pro_detail' => $this->input->post('pro_detail'),
                    'fuel_type' => $this->input->post('fuel_type'),
                    'engine_name' => $this->input->post('engine_name'),
                    'body_color_id' => $this->input->post('total_color_id'),
                    'wheel_id' => $this->input->post('wheel_id'),
                    'tramission_type' => $this->input->post('tramission_type'),
                    'status' => $this->input->post('status'),
                    'brand_id' => $this->input->post('brand_id'),
                    'seating_capacity' => $this->input->post('body_seating_capacity'),
                    'city' => $this->input->post('city'),
                    'seo_keyword' => $this->input->post('seo_keyword'),
                    'seo_title' => $this->input->post('seo_tag'),
                    'seo_description' => $this->input->post('seo_description'),
                    'added_date' => $date);
                $query = $this->product_data->upload_product($data, $data2);
                $this->db->trans_complete();
                if ($this->db->trans_status()) {
                    $this->session->set_flashdata('success_msg', 'Product Successfully Created');
                } else {
                    $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
                }
                //$data['sucess']='Your Detail added successfully';                
                redirect('product');
            }
        } else if (isset($_POST['update'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('model_id', 'Model', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('city', 'city', 'trim|required|xss_clean');
            $this->form_validation->set_rules('brand_id', 'Brand', 'trim|required|xss_clean');
            $this->form_validation->set_rules('pro_name', 'Product name', 'trim|required|xss_clean');
            //$this->form_validation->set_rules('vairant', 'vairant', 'trim|required|xss_clean');
            $this->form_validation->set_rules('pro_detail', 'Product Details', 'trim|required|xss_clean');
            $this->form_validation->set_rules('fuel_type', 'Fuel type', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('body_seating_capacity', 'Seating Capacity', 'trim|required|xss_clean');
            $this->form_validation->set_rules('tramission_type', 'Transmission type', 'trim|required|xss_clean');
            $color_id = $this->input->post('total_color_id');
            $date = date('Y-m-d h:i:s');
            $exp_1 = explode(',', $color_id);
            $res = '';
            for ($i = 0; $i <= count($exp_1) - 1; $i++) {
                $res = $res . "'" . $exp_1[$i] . "',";
            }
            $color_data = rtrim($res, ",");

            $wheel_id = $this->input->post('wheel_id');
            $exp_2 = explode(',', $wheel_id);
            $res2 = '';
            for ($j = 0; $j <= count($exp_2) - 1; $j++) {
                $res2 = $res2 . "'" . $exp_2[$j] . "',";
            }
            $wheel_data = rtrim($res2, ",");
            if (!$this->form_validation->run()) {
                $variant_id = $this->input->post('variant_id');
                $data['body_color'] = $this->product_data->get_body_color();
                $data['results_up'] = $this->product_data->get_product($variant_id);
                $this->load->view('product', $data);
            } else {
//                echo '<pre>';
                foreach ($data['model'] as $model_data) {
                    if ($this->input->post('model_id') == $model_data['model_id']) {
                        $model_name = $model_data['model_name'];
                    }
                }

                foreach ($data['brand'] as $brand_data) {
                    if ($this->input->post('brand_id') == $brand_data['brand_id']) {
                        $brand_name = $brand_data['brand_name'];
                    }
                }
                $brand_name;
                $model_name;
                $pro_name_comp = $brand_name . ' ' . $model_name . ' ' . $this->input->post('pro_name');

                $data2 = array(
                    'body_no_of_doors' => $this->input->post('body_no_of_doors'),
                    'body_seating_capacity' => $this->input->post('body_seating_capacity'),
                    'engine_capacity' => $this->input->post('engine_capacity'),
                    'engine_no_of_cylinder' => $this->input->post('engine_no_of_cylinder'),
                    'engine_no_of_valves' => $this->input->post('engine_no_of_valves'),
                    'engine_bore_stroke' => $this->input->post('engine_bore_stroke'),
                    'engine_compression_ratio' => $this->input->post('engine_compression_ratio'),
                    'engine_maximum_power' => $this->input->post('engine_maximum_power'),
                    'engine_maximum_torque' => $this->input->post('engine_maximum_torque'),
                    'engine_fuel_distribution' => $this->input->post('engine_fuel_distribution'),
                    'engine_air_charging' => $this->input->post('engine_air_charging'),
                    'engine_emission' => $this->input->post('engine_emission'),
                    'transmission_drive' => $this->input->post('transmission_drive'),
                    'transmission_clutch_type' => $this->input->post('transmission_clutch_type'),
                    'tranmssion_first_gear_ratio' => $this->input->post('tranmssion_first_gear_ratio'),
                    'transmission_second_gear_ratio' => $this->input->post('transmission_second_gear_ratio'),
                    'transmission_third_gear_ratio' => $this->input->post('transmission_third_gear_ratio'),
                    'transmission_fourth_gear_ratio' => $this->input->post('transmission_fourth_gear_ratio'),
                    'transmission_fifth_gear_ratio' => $this->input->post('transmission_fifth_gear_ratio'),
                    'transmission_sixth_gear_ratio' => $this->input->post('transmission_sixth_gear_ratio'),
                    'transmission_reverse_gear_ratio' => $this->input->post('transmission_reverse_gear_ratio'),
                    'transmission_final_drive_ratio' => $this->input->post('transmission_final_drive_ratio'),
                    'dimensions_mm_length' => $this->input->post('dimensions_mm_length'),
                    'dimensions_mm_width' => $this->input->post('dimensions_mm_width'),
                    'dimensions_mm_height' => $this->input->post('dimensions_mm_height'),
                    'dimensions_mm_weelbase' => $this->input->post('dimensions_mm_weelbase'),
                    'dimensions_mm_tread_front' => $this->input->post('dimensions_mm_tread_front'),
                    'dimensions_mm_tread_rear' => $this->input->post('dimensions_mm_tread_rear'),
                    'dimensions_mm_ground_clearance' => $this->input->post('dimensions_mm_ground_clearance'),
                    'kerb_weight' => $this->input->post('kerb_weight'),
                    'gross_vehicle_weight' => $this->input->post('gross_vehicle_weight'),
                    'brakes_front' => $this->input->post('brakes_front'),
                    'brakes_rear' => $this->input->post('brakes_rear'),
                    'streeing_type' => $this->input->post('streeing_type'),
                    'streeing_power_type' => $this->input->post('streeing_power_type'),
                    'streeing_turn_radius' => $this->input->post('streeing_turn_radius'),
                    'suspension_front' => $this->input->post('suspension_front'),
                    'suspension_rear' => $this->input->post('suspension_rear'),
                    'tyre_size' => $this->input->post('tyre_size'),
                    'tyre_wheel_type' => $this->input->post('tyre_wheel_type'),
                    'fuel_tank_capacity1' => $this->input->post('fuel_tank_capacity1'),
                    'boot_capacity1' => $this->input->post('boot_capacity1'),
                    'no_seating_rows' => $this->input->post('no_seating_rows'),
                    'bootspace' => $this->input->post('bootspace'),
                    'engine_types' => $this->input->post('engine_types'),
                    'displacement' => $this->input->post('displacement'),
                    'alternate_fuel' => $this->input->post('alternate_fuel'),
                    'mileage' => $this->input->post('mileage'),
                    'bore_stroke' => $this->input->post('bore_stroke'),
                    'comparision_ratio' => $this->input->post('comparision_ratio'),
                    'valve_cylinder' => $this->input->post('valve_cylinder'),
                    'fuel_system' => $this->input->post('fuel_system'),
                    'drivetrain' => $this->input->post('drivetrain'),
                    'no_gear' => $this->input->post('no_gear'),
                    'sport_mode' => $this->input->post('sport_mode'),
                    'dual_cluth' => $this->input->post('dual_cluth'),
                    'rear_tyre' => $this->input->post('rear_tyre'),
                    'front_tyres' => $this->input->post('front_tyres'),
                    'cylinders' => $this->input->post('cylinders'),
                    'seat_width' => $this->input->post('seat_width'),
                    'wheel_size' => $this->input->post('wheel_size'),
                    'spare_type' => $this->input->post('spare_type'),
                    'trunk_volume' => $this->input->post('trunk_volume'),
                    'top_speed' => $this->input->post('top_speed'),
                    'acceleration' => $this->input->post('acceleration'),
                    'standard_warranty' => $this->input->post('standard_warranty'),
                    'rear_seat_legroom' => $this->input->post('rear_seat_legroom'),
                    'headroom' => $this->input->post('headroom'),
                    'added_date' => $date);



                if (!empty($_FILES['product_images']['name'])) {
                    $variant_id = $this->input->post('variant_id');
                    $config2['upload_path'] = 'uploads/product/';
                    $config2['allowed_types'] = 'gif|jpg|png';
                    $config2['max_size'] = '5200';
                    $this->load->library('upload', $config2);
                    $img_name = $this->upload->do_uploads('product_images');
                    if ($img_name == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php

                        exit;
                    }
                }







                if ($img_name == '') {
                    $img_name = $this->input->post('pro_images2');
                }

                $added_date = date('y-m-d H:i:s');
                $data = array(
                    'model_id' => $this->input->post('model_id'),
                    'pro_name' => $this->input->post('pro_name'),
                    'pro_name_comp' => $pro_name_comp,
                    'seating_capacity' => $this->input->post('body_seating_capacity'),
                    'variant' => $this->input->post('vairant'),
                    'pro_type' => $this->input->post('pro_type'),
                    'pro_image' => $img_name,
                    'pro_detail' => $this->input->post('pro_detail'),
                    'fuel_type' => $this->input->post('fuel_type'),
                    'engine_name' => $this->input->post('engine_name'),
                    'body_color_id' => $this->input->post('total_color_id'),
                    'wheel_id' => $this->input->post('wheel_id'),
                    'tramission_type' => $this->input->post('tramission_type'),
                    'status' => $this->input->post('status'),
                    'brand_id' => $this->input->post('brand_id'),
                    'city' => $this->input->post('city'),
                    'seo_keyword' => $this->input->post('seo_keyword'),
                    'seo_title' => $this->input->post('seo_tag'),
                    'seo_description' => $this->input->post('seo_description'),
                    'added_date' => $date);
                $query = $this->product_data->update_product($data, $data2, $variant_id);
                $this->db->trans_complete();
                if ($this->db->trans_status()) {
                    $this->session->set_flashdata('success_msg', 'Product Updated Successfully');
                } else {
                    $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
                }
                redirect('product');
            }
        } else if (isset($_POST['get'])) {
            $variant_id = $this->input->post('variant_id');
            $data['results_up'] = $this->product_data->get_product($variant_id);
            $data['body_color'] = $this->product_data->get_body_color();
            $data['body_wheel'] = $this->product_data->get_body_wheel();
            $this->load->view('product', $data);
        } else if (isset($_POST['copy'])) {
            $variant_id = $this->input->post('variant_id');
            $data['results_up_copy'] = $this->product_data->get_product($variant_id);
            $data['body_color'] = $this->product_data->get_body_color();
            $data['body_wheel'] = $this->product_data->get_body_wheel();
            $this->load->view('product', $data);
        } else if (isset($_POST['delete'])) {
            echo $variant_id = $this->input->post('variant_id');
//            exit;
            $this->product_data->delete_varaint($variant_id);

            redirect('product');
        } else {
            redirect('product');
        }
    }

    function gallery() {
        $data['title'] = 'Product Gallery';
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        $this->load->view('gallery', $data);
    }

    function gallery_result() {
        $this->load->library("datatables");
        $a = '1';
        $b = '2';
        $c = '3';
        $actionLinkBar = $this->load->view("content/report/datatable/gallery_all_action", array(), TRUE);
        $this->datatables
                ->select("product_gallery_id,(select pro_name from variant where variant_id=product_gallery.prduct_id) as pro_name,page")
                ->from("product_gallery")
                ->add_column("Action", $actionLinkBar, 'product_gallery_id,', $c);
        echo $this->datatables->generate();
    }

    function gallery_upload() {
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        $data[] = '';
        $img_name = '';
        if (isset($_POST['submit'])) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('all_product_name', 'Product name', 'trim|required|xss_clean');
            $types = $this->input->post('types');
            if ($types == 2) {
                $this->form_validation->set_rules('link', 'link', 'trim|required|xss_clean');
            }

            $this->form_validation->set_rules('page', 'page', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->load->view('gallery', $data);
            } else {
                $date = date('Y-m-d h:i:s');
                $product_name = $this->input->post('all_product_name');
                $config['upload_path'] = 'uploads/gallery/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                $added_date = date('y-m-d H:i:s');
                $img_name = $this->upload->do_uploads('pro_images');
                $data = array(
                    'prduct_id' => $this->input->post('all_product_name'),
                    'image' => $img_name,
                    'video' => $this->input->post('link'),
                    'page' => $this->input->post('page'),
                    'added_date' => $date);

                $query = $this->product_data->upload_gallery($data);
                $data['sucess'] = 'Your Detail added successfully';
                redirect('gallery');
            }
        } else if (isset($_POST['get'])) {
            $product_gallery_id = $this->input->post('product_gallery_id');

            $data['load_products'] = $this->product_data->load_gallery($product_gallery_id);
//            print_r($data['load_products'][0]['variant_id']);
            $data['single_res_detail'] = $this->experts_rating->show_rating_data($data['load_products'][0]['variant_id']);
//            print_r($data['single_res_detail']);
            $this->load->view('gallery', $data);
        } else if (isset($_POST['update'])) {
            $product_gallery_id = $this->input->post('product_gallery_id');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('all_product_name', 'Product name', 'trim|required|xss_clean');
            $types = $this->input->post('types');
            if ($types == 2) {
                $this->form_validation->set_rules('link', 'link', 'trim|required|xss_clean');
            }
            $this->form_validation->set_rules('page', 'page', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {

                $data['load_products'] = $this->product_data->load_gallery($product_gallery_id);
                $this->load->view('gallery', $data);
            } else {
                $date = date('Y-m-d h:i:s');
                $product_name = $this->input->post('all_product_name');
                $config['upload_path'] = 'uploads/gallery/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                $added_date = date('y-m-d H:i:s');
                if ($_FILES['pro_images']['name'] != '') {
                    $img_name = $this->upload->do_uploads('pro_images');
                } else {
                    $img_name = $this->input->post('product_gallery_img');
                }
                echo $img_name;
//                exit;
                $data = array(
                    'prduct_id' => $this->input->post('all_product_name'),
                    'image' => $img_name,
                    'video' => $this->input->post('link'),
                    'page' => $this->input->post('page'),
                    'added_date' => $date);

                $query = $this->product_data->update_gallery($data, $product_gallery_id);
                $data['sucess'] = 'Your Detail added successfully';
                redirect('gallery');
            }
        } else if (isset($_POST['delete'])) {
            $product_gallery_id = $this->input->post('product_gallery_id');
            $this->product_data->delete_gallery($product_gallery_id);
            redirect('gallery');
        } else {
            redirect('gallery');
        }
    }

    function bulkproduct() {
        $data['title'] = 'Product Gallery';
        $this->load->view('bulk_upload', $data);
    }

    function bulk_upload_data() {
        ini_set('max_execution_time', 3000000000);
        set_time_limit(0);
        $this->load->library('simple_html_dom');
        if (isset($_POST['submit'])) {
            $del_record = $_POST['del_record'];
            if (!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }

            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");



            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error_row = 0;
            $error_res = '';
            $update_res = '';
            $sucess_res = '';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                if ($j <= 5) {
                    $added_date = date('Y-m-d h:i:s');
                    $i++;
                    if ($i == 2) {
                        $array_category = $data;
                    } else if ($i == 3) {
                        $array_data_type = $data;
                    } else if ($i > 3) {
                        $real_data = $data;
                    }
                    $heading = array($array_data_type);
                    $category = array($array_category);
                    $main_data = array($real_data);
                    if ($i > 3) {
                        $final = array_merge($category, $heading, $main_data);
                    }


                    if ($i > 3) {

                        # Fetching brand id here            
                        $brand_name = trim($data[0]);
                        $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->result_array();
                        $brand_id = $brand[0]['brand_id'];


                        # Fetching model id here
                        $model_name = trim($data[1]);
                        $coming_type = strtolower($final[2][149]);
                        $model = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->result_array();
                        $model_id = $model[0]['model_id'];
                        if (empty($model_id)) {
                            if ($coming_type == 'y') {
                                $model_status = '2';
                            } else {
                                $model_status = '1';
                            }

                            # Inserting new model as per brand wise    
                            $model_name = addslashes($model_name);
                            $model_insert = $this->db->query("INSERT INTO `model`(`brand_id`,`model_name`,`status`) VALUES('$brand_id','$model_name','$model_status')");
                            $model_id = $this->db->insert_id();
                        }
                        $model_id;
                        $variant_name = trim($data[2]);

                        $error_row++;
                        if ($brand_id != '' && $model_id != "") {
                            # All variable define here    
                            $pro_main_image = ucfirst(strtolower($brand_name)) . '/' . ucfirst(strtolower($model_name)) . '/front_204.jpg';
                            $fuel_type = trim($data[4]);
                            $pro_type = trim($data[3]);
                            $pro_detail = (htmlspecialchars(trim($data[5])));
                            $seating_capacity = trim($data[28]);
                            $transmission_type = trim($data[12]);
                            $displacement = trim($data[6]);
                            $cylinders = trim($data[7]);
                            $power = trim($data[8]);
                            $torque = trim($data[9]);
                            $emssion = trim($data[10]);
                            $mileage = trim($data[11]);
                            $no_gear = trim($data[13]);
                            $Drivetrain = trim($data[14]);
                            $length = trim($data[15]);
                            $width = trim($data[16]);
                            $height = trim($data[17]);
                            $weelbase = trim($data[18]);
                            $ground_clearance = trim($data[19]);
                            $rear_seat = trim($data[20]);
                            $headroom = trim($data[21]);
                            $seat_width = trim($data[22]);
                            $tyre_size = trim($data[23]);
                            $wheel_size = trim($data[24]);
                            $spare_type = trim($data[25]);
                            $steering_type = trim($data[26]);
                            $turning_radius = trim($data[27]);
                            $trunk_volume = trim($data[29]);
                            $fuel_tank = trim($data[30]);
                            $top_speed = trim($data[31]);
                            $acceleration = trim($data[32]);
                            $brake_front = trim($data[33]);
                            $brake_rear = trim($data[34]);
                            $suspension_front = trim($data[35]);
                            $suspension_rear = trim($data[36]);
                            $kerb_wight = trim($data[37]);
                            $standard_warranty = trim($data[38]);
                            $video = trim($data[39]);
                            $air_cond = trim($data[40]);
                            $cliemt_cont = trim($data[41]);
                            $power_window = trim($data[42]);
                            $power_adjustable = trim($data[43]);
                            $tilt_Adjustable_Steering_Column_Rake = trim($data[44]);
                            $pro_title = $final[2][139];
                            $pro_tags = $final[2][140];
                            $pro_desc = $final[2][141];
                            $car_finance = $final[2][142];
                            $car_amc = $final[2][143];
                            $car_insurance = $final[2][144];
                            $car_ext_waranty = $final[2][145];
                            $car_exchange = $final[2][146];
                            $car_home_delivery = $final[2][147];
                            $car_home_delivery = $final[2][147];
                            $record_type = strtolower($final[2][148]);
                            $coming_type = strtolower($final[2][149]);
                            $launching_date = $final[2][150];
                            $extimated_price = $final[2][151];
                            $booking_amount = $final[2][152];
                            $seo_title = $final[2][153];
                            $exp_extimated_price = explode('-', $extimated_price);
                            $added_date = date('Y-m-d h:i:s');
                            $min_p = trim($exp_extimated_price[0]);
                            $max_p = trim($exp_extimated_price[1]);


                            # Fetching Fuel Id using fuel name here
                            $fuel = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel_type'")->result_array();
                            $fuel_type_id = $fuel[0]['fuel_type_id'];
                            $pro_name_comp = $brand_name . ' ' . $model_name . ' ' . $variant_name;
                            # Fetching product id using product name,brand,model here
                            $que = "select variant_id from variant where model_id='$model_id' and brand_id='$brand_id' and fuel_type='$fuel_type_id' and pro_name_comp='$pro_name_comp'";
                            $var_res = $this->db->query($que)->result_array();
                            $variant_main_id = $var_res[0]['variant_id'];
                            $count = count($var_res);

                            if ($record_type == 'a' || $record_type == 'A') {

                                if ($count == 0) {

                                    // product status section start here    
                                    if ($coming_type == 'y') {
                                        $real_status = '3';
                                    } else {
                                        $real_status = '1';
                                    }



                                    $pro_name_comp = $brand_name . ' ' . $model_name . ' ' . $variant_name;
                                    $insert = $this->db->query("INSERT INTO `variant`(`variant_id`, `model_id`, `pro_name`,`pro_name_comp`,`pro_type`, `variant`, `pro_detail`,"
                                            . " `pro_image`, `engine_name`, `fuel_type`, `seating_capacity`, `tramission_type`, `body_color_id`, `wheel_id`, "
                                            . "`status`, `brand_id`, `pro_video`, `seo_title`, `seo_description`, `seo_tags`, `added_date`, `city`,`upcoming`,`launching_date`,`extimated_price`,`extimated_price_max`,`booking_amount`)"
                                            . "VALUES ('','$model_id','$variant_name','$pro_name_comp','$pro_type','','$pro_detail','$pro_main_image','','$fuel_type_id',"
                                            . "'$seating_capacity','$transmission_type','','','$real_status','$brand_id','','$pro_title','$pro_desc','$pro_tags','$added_date','','$coming_type','$launching_date','$min_p','$max_p','$booking_amount')");
                                    $variant_id = $this->db->insert_id();
                                    $variant_img = $variant_id . ".jpg";
                                    $specfication = $this->db->query("INSERT INTO `specification` VALUES ('','$variant_id','-','-','-','-','-','-','-','$power','$torque','-','-','$emssion','-','-','-','-','-','-','-','-','-','-','$length','$width','$height','$weelbase','-','-','$ground_clearance','$kerb_wight','-','$brake_front','$brake_rear','$steering_type','-','$turning_radius','$suspension_front','$suspension_rear','$tyre_size','-','$fuel_tank','-','-','-','-','$displacement','-','$mileage','-','-','-','-','$Drivetrain','$no_gear','-','-','-','-','$cylinders','-','$seat_width','$wheel_size','$spare_type','$trunk_volume','$top_speed','$acceleration','$standard_warranty','$rear_seat','$headroom')");
                                    #product video code delerted here plz used prevois backup of code 31-3-2015
                                    ### variant accessory section here ###
                                    if ($car_finance != '') {
                                        $car_fianance = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='11'")->row_array();
                                        $accessory_id = $car_fianance['accessory_id'];
                                        $count_acc = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc == 0) {
                                            $acc_insert = $this->db->query("INSERT INTO `accessory_variant_mapping`(`id`, `accessory_id`, `variant_id`, `price`, `status`) VALUES "
                                                    . "('','$accessory_id','$variant_id','$car_finance','1')");
                                        }
                                    }

                                    if ($car_amc != '') {
                                        $car_amc_id = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='12'")->row_array();
                                        $accessory_id = $car_amc_id['accessory_id'];
                                        $count_acc1 = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc1 == 0) {
                                            $acc_insert = $this->db->query("INSERT INTO `accessory_variant_mapping`(`id`, `accessory_id`, `variant_id`, `price`, `status`) VALUES "
                                                    . "('','$accessory_id','$variant_id','$car_finance','1')");
                                        }
                                    }


                                    if ($car_insurance != '') {
                                        $car_ins = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='13'")->row_array();
                                        $accessory_id = $car_ins['accessory_id'];
                                        $count_acc2 = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc2 == 0) {
                                            $acc_insert = $this->db->query("INSERT INTO `accessory_variant_mapping`(`id`, `accessory_id`, `variant_id`, `price`, `status`) VALUES "
                                                    . "('','$accessory_id','$variant_id','$car_finance','1')");
                                        }
                                    }


                                    if ($car_ext_waranty != '') {
                                        $car_ext = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='14'")->row_array();
                                        $accessory_id = $car_ext['accessory_id'];
                                        $count_acc3 = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc3 == 0) {
                                            $acc_insert = $this->db->query("INSERT INTO `accessory_variant_mapping`(`id`, `accessory_id`, `variant_id`, `price`, `status`) VALUES "
                                                    . "('','$accessory_id','$variant_id','$car_finance','1')");
                                        }
                                    }


                                    if ($car_exchange != '') {
                                        $car_exc = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='15'")->row_array();
                                        $accessory_id = $car_exc['accessory_id'];
                                        $count_acc4 = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc4 == 0) {
                                            $acc_insert = $this->db->query("INSERT INTO `accessory_variant_mapping`(`id`, `accessory_id`, `variant_id`, `price`, `status`) VALUES "
                                                    . "('','$accessory_id','$variant_id','$car_finance','1')");
                                        }
                                    }


                                    if ($car_home_delivery != '') {
                                        $car_home = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='16'")->row_array();
                                        $accessory_id = $car_home['accessory_id'];
                                        $count_acc5 = $this->db->query("select id from accessory_variant_mapping where  variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc5 == 0) {
                                            $acc_insert = $this->db->query("INSERT INTO `accessory_variant_mapping`(`id`, `accessory_id`, `variant_id`, `price`, `status`) VALUES "
                                                    . "('','$accessory_id','$variant_id','$car_finance','1')");
                                        }
                                    }

                                    #imge gallery code delerted here plz used prevois backup of code 31-3-2015
                                    #product feature process here

                                    $final1 = $final[0];
                                    $final2 = $final[1];
                                    $final3 = $final[2];
                                    $total = count($final2);

                                    for ($ii = 1; $ii <= $total; $ii++) {
                                        if ($ii > 39 && $ii < 72) {
                                            $feature_type = '3';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));
                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();

                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                    $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_id','','1')");
                                                }
                                            }
                                        }

                                        if ($ii > 72 && $ii < 92) {
                                            $feature_type = '2';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));

                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();
                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                    $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_id','','1')");
                                                }
                                            }
                                        }

                                        if ($ii > 92 && $ii < 105) {
                                            $feature_type = '4';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));
                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();
                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                    $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_id','','1')");
                                                }
                                            }
                                        }


                                        if ($ii > 105 && $ii < 119) {
                                            $feature_type = '1';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));
                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();
                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                    $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_id','','1')");
                                                }
                                            }
                                        }
                                        if ($ii > 119 && $ii < 135) {
                                            $feature_type = '5';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));
                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();
                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                    $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_id','','1')");
                                                }
                                            }
                                        }
                                    }

                                    $color = $final[2][136] . ',,' . $final[2][137];
                                    $exp_color = explode(',', $color);
                                    $color_data = array_filter($exp_color);
                                    $feature_category = '7';
                                    $feature_name = 'Color';
                                    foreach ($color_data as $color_data_data) {
                                        $color_count = $this->db->query("select feature_name from `features` where `feature_desc`='$color_data_data' ")->num_rows();
                                        if ($color_count == 0) {
                                            $color_data_data = trim($color_data_data);
                                            $color_data_data = str_replace(' ', '-', $color_data_data);
                                            $color_image = 'Color/' . $color_data_data . '.jpg';
                                            $color_insert = $this->db->query("INSERT INTO `features`(`feature_type`, `feature_name`, `feature_desc`, `recomended`,`feature_img`) VALUES ('$feature_category','$feature_name','$color_data_data','0','$color_image')");
                                        }
                                        $color_id = $this->db->query("select feature_id from `features` where `feature_desc`='$color_data_data' and feature_type='$feature_category' and feature_name='$feature_name'")->result_array();
                                        $feature_id_data = $color_id[0]['feature_id'];
                                        if ($feature_id_data != '') {
                                            $color_id = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) "
                                                    . "VALUES ('','$feature_id_data','$variant_id','0','1')");
                                        }
                                    }
                                    $msg.='<div style="color:green;">Inserted Success Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';
//        $sucess_res=$sucess_res.','.$error_row;    
//        $this->session->set_flashdata('Insert',"Following Rows of data Inserted Successfully: ".$sucess_res);
                                } else {
                                    $msg2.='<div style="color:red;">Inserted Error Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';
//        $update_res=$update_res.','.$error_row;   
//        echo 'Added Record Status:<br>';    
//        echo 'Result Error: '.$brand_name.'-'.$model_name.'-'.$variant_name;        
//        $this->session->set_flashdata('Update',"Following Rows of data update: ".$update_res);         
                                }
                            } else if ($record_type == 'u' || $record_type == 'U') {
                                // product status section start here    
                                if ($coming_type == 'y') {
                                    $real_status = '3';
                                } else {
                                    $real_status = '1';
                                }

                                ## Record update section start here
                                $pro_name_comp = $brand_name . ' ' . $model_name . ' ' . $variant_name;
                                $que = "select variant_id from variant where model_id='$model_id' and brand_id='$brand_id' and fuel_type='$fuel_type_id' and pro_name_comp='$pro_name_comp'";
                                $var_res = $this->db->query($que)->result_array();
                                $variant_main_id = $var_res[0]['variant_id'];
                                $count = count($var_res);
                                $variant_main_id;

                                if ($count > 0) {
                                    ## Update variant details here##


                                    $upd_variant = $this->db->query("UPDATE `variant` SET `seo_title`='$pro_title',`seo_description`='$pro_desc',`seo_tags`='$pro_tags',`pro_type` = '$pro_type',`variant` = '0',`pro_detail` = '$pro_detail',
        `fuel_type` = '$fuel_type_id',`seating_capacity` = '$seating_capacity',`tramission_type` = '$transmission_type',"
                                            . "`upcoming`='$coming_type',`launching_date`='$launching_date',`status`='$real_status',`upcoming`='$coming_type',`launching_date`='$launching_date',`extimated_price`='$min_p',`extimated_price_max`='$max_p',`booking_amount`='$booking_amount'
         WHERE `variant_id` = '$variant_main_id'");


                                    $upd_variant_specification = $this->db->query("UPDATE `specification` SET `engine_maximum_power` = '$power',`engine_maximum_torque` = '$torque',`engine_emission` = '$emssion',`dimensions_mm_length` = '$length',
        `dimensions_mm_width` = '$width',`dimensions_mm_height` = '$height',`dimensions_mm_weelbase` = '$weelbase',`dimensions_mm_ground_clearance` = '$ground_clearance',
        `kerb_weight` = '$kerb_wight',`brakes_front` = '$brake_front',`brakes_rear` = '$brake_rear',`streeing_type` = '$steering_type',`streeing_turn_radius` = '$turning_radius',`suspension_front` = '$suspension_front',`suspension_rear` = '$suspension_rear',`tyre_size` = '$tyre_size',
        `fuel_tank_capacity1` = '$fuel_tank',`displacement` = '$displacement',`mileage` = '$mileage',`drivetrain` = '$Drivetrain',`no_gear` = '$no_gear',`cylinders` = '$cylinders',`seat_width` = '$seat_width',`wheel_size` = '$wheel_size',
        `spare_type` = '$spare_type',`trunk_volume` = '$trunk_volume',`top_speed` = '$top_speed',`acceleration` = '$acceleration',`standard_warranty` = '$standard_warranty',`rear_seat_legroom` = '$rear_seat',`headroom` = '$headroom'  WHERE `variant_id` = BINARY '$variant_main_id'");



                                    ### Product accessory section update here        
                                    if ($car_finance != '') {
                                        $car_fianance = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='11'")->row_array();
                                        $accessory_id = $car_fianance['accessory_id'];
                                        $count_acc = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc > 0) {
                                            $upd_acc_insert = $this->db->query("UPDATE `accessory_variant_mapping` set `accessory_id`='$accessory_id',`price`='$car_finance' where variant_id='$variant_main_id'");
                                        }
                                    }


                                    if ($car_amc != '') {
                                        $car_amc_id = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='12'")->row_array();
                                        $accessory_id = $car_amc_id['accessory_id'];
                                        $count_acc1 = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc1 > 0) {
                                            $upd_acc_insert = $this->db->query("UPDATE `accessory_variant_mapping` set `accessory_id`='$accessory_id',`price`='$car_amc' where variant_id='$variant_main_id'");
                                        }
                                    }


                                    if ($car_insurance != '') {
                                        $car_ins = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='13'")->row_array();
                                        $accessory_id = $car_ins['accessory_id'];
                                        $count_acc2 = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc2 > 0) {
                                            $upd_acc_insert = $this->db->query("UPDATE `accessory_variant_mapping` set `accessory_id`='$accessory_id',`price`='$car_insurance' where variant_id='$variant_main_id'");
                                        }
                                    }


                                    if ($car_ext_waranty != '') {
                                        $car_ext = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='14'")->row_array();
                                        $accessory_id = $car_ext['accessory_id'];
                                        $count_acc3 = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc3 > 0) {
                                            $upd_acc_insert = $this->db->query("UPDATE `accessory_variant_mapping` set `accessory_id`='$accessory_id',`price`='$car_ext_waranty' where variant_id='$variant_main_id'");
                                        }
                                    }


                                    if ($car_exchange != '') {
                                        $car_exc = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='15'")->row_array();
                                        $accessory_id = $car_exc['accessory_id'];
                                        $count_acc4 = $this->db->query("select id from accessory_variant_mapping where variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc4 == 0) {
                                            $upd_acc_insert = $this->db->query("UPDATE `accessory_variant_mapping` set `accessory_id`='$accessory_id',`price`='$car_exchange' where variant_id='$variant_main_id'");
                                        }
                                    }


                                    if ($car_home_delivery != '') {
                                        $car_home = $this->db->query("select accessory_id from accessory where accessory_cat='7' and accessory_sub_cat='16'")->row_array();
                                        $accessory_id = $car_home['accessory_id'];
                                        $count_acc5 = $this->db->query("select id from accessory_variant_mapping where  variant_id='$variant_id' and accessory_id='$accessory_id'")->num_rows();
                                        if ($count_acc5 > 0) {
                                            $upd_acc_insert = $this->db->query("UPDATE `accessory_variant_mapping` set `accessory_id`='$accessory_id',`price`='$car_home_delivery' where variant_id='$variant_main_id'");
                                        }
                                    }





                                    #product feature process here

                                    $final1 = $final[0];
                                    $final2 = $final[1];
                                    $final3 = $final[2];
                                    $total = count($final2);

                                    $delete_feature = $this->db->query("delete from `feature_variant_mapping` where `variant_id`='$variant_main_id'");

                                    echo $variant_main_id;

                                    for ($ii = 1; $ii <= $total; $ii++) {

                                        if ($ii > 39 && $ii < 72) {


                                            $feature_type = '3';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));




                                            if ($feature_desc != '') {

                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();




                                                if ($c1 == 0) {

                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                    $feature_id = $this->db->insert_id();
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                }
                                                $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_main_id','','1')");
                                            }
                                        }

                                        if ($ii > 72 && $ii < 92) {
                                            $feature_type = '2';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));

                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();
                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                    $feature_id = $this->db->insert_id();
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                }
                                                $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_main_id','','1')");
                                            }
                                        }

                                        if ($ii > 92 && $ii < 105) {
                                            $feature_type = '4';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));
                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();
                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                    $feature_id = $this->db->insert_id();
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                }
                                                $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_main_id','','1')");
                                            }
                                        }


                                        if ($ii > 105 && $ii < 119) {
                                            $feature_type = '1';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));
                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();
                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                    $feature_id = $this->db->insert_id();
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                }
                                                $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_main_id','','1')");
                                            }
                                        }
                                        if ($ii > 119 && $ii < 135) {
                                            $feature_type = '5';
                                            $feature_name = $final2[$ii];
                                            $feature_desc = (htmlspecialchars(trim($final3[$ii])));
                                            if ($feature_desc != '') {
                                                $c01 = $this->db->query("select feature_id from  `features` where `feature_type`='$feature_type' and `feature_name`='$feature_name' and `feature_desc`='$feature_desc'");
                                                $c1 = $c01->num_rows();
                                                if ($c1 == 0) {
                                                    $insert = $this->db->query("INSERT INTO `features`(`feature_id`, `feature_type`, `feature_name`, `feature_desc`, `feature_img`, `recomended`, `cost`, `added_by`, `datetime`) "
                                                            . "VALUES ('','$feature_type','$feature_name','$feature_desc','','','','','$added_date')");
                                                    $feature_id = $this->db->insert_id();
                                                } else {
                                                    $c1 = $c01->result_array();
                                                    $feature_id = $c1[0]['feature_id'];
                                                }

                                                $insert = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) VALUES ('','$feature_id','$variant_main_id','','1')");
                                            }
                                        }
                                    }


                                    $color = $final[2][136] . ',,' . $final[2][137];

                                    $exp_color = explode(',', $color);
                                    $color_data = array_filter($exp_color);
                                    $feature_category = '7';
                                    $feature_name = 'Color';

//        print_r($variant_id);
//        echo '<br>';echo '<br>';
                                    foreach ($color_data as $color_data_data) {


                                        $color_count = $this->db->query("select feature_name from `features` where `feature_desc`='$color_data_data' ")->num_rows();

                                        if ($color_count == 0) {
                                            $color_data_data = trim($color_data_data);
                                            $color_data_data = str_replace(' ', '-', $color_data_data);
                                            $color_image = 'Color/' . $color_data_data . '.jpg';
//        $color_image=trim($color_data_data).'.jpg';    
                                            $color_insert = $this->db->query("INSERT INTO `features`(`feature_type`, `feature_name`, `feature_desc`, `recomended`,`feature_img`) VALUES ('$feature_category','$feature_name','$color_data_data','0','$color_image')");
                                        }



                                        $color_id = $this->db->query("select feature_id from `features` where `feature_desc`='$color_data_data' and feature_type='$feature_category' and feature_name='$feature_name'")->result_array();
                                        $feature_id_data = $color_id[0]['feature_id'];
//        echo '<br>';
//        print_r($color_data_data);




                                        if ($feature_id_data != '') {
                                            $color_id = $this->db->query("INSERT INTO `feature_variant_mapping`(`id`, `feature_id`, `variant_id`, `price`, `status`) "
                                                    . "VALUES ('','$feature_id_data','$variant_main_id','0','1')");
                                        }
                                    }



                                    $msg3.='<div style="color:green;">Updated Success Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';
//         exit;
                                    ## Record update section start here
                                } else {
                                    $msg4.='<div style="color:red;">Updated Error Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';
//        echo 'Updated Record Status:<br>';    
//        echo 'Result Error: '.$brand_name.'-'.$model_name.'-'.$variant_name;
//        
//        echo '<br>';            
                                }
                            }
                            unset($v_link);
                        } else {
                            $variant_name;
                            $error_res = $error_res . ',' . $error_row;
                            $this->session->set_flashdata('Error', "Following Rows of data can't inserted: " . $error_res);
                        }
                    }
                }
            }
            echo '############ Inserted Record ##############';
            echo $msg;
            echo $msg2;

            echo '############ Updated Record ##############';
            echo $msg3;
            echo $msg4;





//        exit;
//        echo "Updated Res: ".$update_res;
//        echo "Error Result: ".$error_res;
//        
            exit;
            fclose($handle);
            redirect(base_url() . 'bulkproduct');
        }
    }

    /* Dealer Page master data section start here */
}
