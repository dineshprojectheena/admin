<?php

class Expert_reviews extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('home_struture_data');
        $this->load->library('form_validation');
    }

    function index() {
        $data['content'] = 'Featured Expert Reviews';
        $id = '';
        $type = 'expert_review';
//        $data = $this->home_struture_data->load_featured($id, $type);
//        $ress = array();
//        foreach ($data as $res) {
//            $data['status']=array('status'=> $res['status']);
//            $data1['prd_id_name1'] = $this->home_struture_data->get_product_detail($res['prd_id_1']);
//            $data1['featured_id'] = array('featured_id' => $res['featured_id']);
//            $final = array_merge($data1['featured_id'], $data1['prd_id_name1'],$data['status']);
//            $ress[] = $final;
//        }
//        $data['load_res'] = $ress;
        $this->load->view('home_expert_reviews', $data);
    }
    
    
    function expert_review_result(){
        $this->load->library("datatables");
        $this->load->helper("datatables");
//        $a='car_comparison';
        $actionLinkBar=$this->load->view("content/report/datatable/expert_review_action",array(), TRUE);
        $this->datatables
                ->select("featured_id,(select pro_name from variant where variant_id=featured.prd_id_1)  as pro_name,status")                       
                ->from("featured")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action",$actionLinkBar,'featured_id');
        echo $this->datatables->generate();
    }
    

    function product_load() {

        $id = $this->input->post('id');
        $searchid = $this->input->post('searchid');
        $searchid2 = $this->input->post('searchid2');
        $query = $this->home_struture_data->search_product($searchid, $searchid2);
        if (count($query) > 0) {
            foreach ($query as $res) {
                $name = $res->name;
                $product_id = $res->product_id;
                ?>
                <div class="show" align="left" onclick="setdata('<?php echo $name; ?>', '<?php echo $id; ?>', '<?php echo $product_id; ?>');">
                    <span class="name"><?php echo $name; ?></span>
                </div>
                <?php
            }
        }
    }

    function expert_review_upload() {

        if (isset($_POST['home_expert_submit'])) {

            $this->form_validation->set_rules('searchid', 'first product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->index();
            } else {
                $data = array(
                    'prd_id_1' => $this->input->post('pro_1'),
                    'status' => $this->input->post('status'),
                    'featured_type' => 'expert_review'
                );
                $query = $this->home_struture_data->upload_expert_review($data,$featured_type='expert_review');
//                exit;
                redirect('home_expert_reviews');
            }
        } else if (isset($_POST['expert_review_update'])) {

            $this->form_validation->set_rules('searchid', 'first product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->get_expert_review();
            } else {
                $type = 'expert_review';
                $featured_id = $this->input->post('featured_id');
                $status= $this->input->post('status');
                $data = array(
                    'prd_id_1' => $this->input->post('pro_1'),
                    'status' => $status,
                    'featured_type' => $type
                );

                $query = $this->home_struture_data->update_expert_reviews($data, $featured_id,$featured_type='expert_review',$status);
                redirect('home_expert_reviews');
            }
        } else if (isset($_POST['delete']))
        {
            $featured_id = $this->input->post('featured_id');
            $query = $this->home_struture_data->del_expert_reviews($featured_id);
            redirect('home_expert_reviews');
        } else {
            redirect('home_expert_reviews');
        }
    }

    function get_expert_review() {
        $featured_id = $this->input->post('featured_id');
        
        $type = 'expert_review';
        $data = $this->home_struture_data->load_featured($featured_id, $type);
        foreach ($data as $res)
        $data['featured_id'] = array('featured_id' => $featured_id,'status' => $res['status'],);
        $pro1 = $res['prd_id_1'];
        $data['prd_id_name1'] = $this->home_struture_data->get_product_detail($pro1);
        
        $this->load->view('home_expert_reviews', $data);
    }

}
