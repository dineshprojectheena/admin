<?php

/**
 * @author Nikhil Kataria <nikhil@projectheena.com>
 * @desc contains all feature related functionalities
 * 
 */
class Feature extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('feature_model');
        $this->load->model('product_data');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function index() {
        $this->load->library('form_validation');
        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }
        

//        $data['all_features'] = $this->feature_model->get_all_features();
        $data['features_sub_type'] = $this->feature_model->get_all_features_sub_type();
        $data['content'] = 'feature/feature';
        $this->load->view('index', $data);
    }
    
    function feature_result(){
        $this->load->library("datatables");
        $actionLinkBar=$this->load->view("content/report/datatable/feature_all_action",array(), TRUE);
        $this->datatables
                ->select("feature_id,(select feature_caegory from feature_category where feature_category_id=features.feature_type) as feature_caegory,feature_name,feature_desc")                              
                ->from("features")
                ->add_column("Action",$actionLinkBar,'feature_id');
        echo $this->datatables->generate();
    }

    function create_feature(){
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="help-block">', '</div>');
        $this->form_validation->set_rules('feature_type', 'Feature type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('feature_name', 'Feature Sub Type', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('feature_type', 'Feature Type', 'trim|required|xss_clean|integer|greater_than[0]|less_than[7]');
        $this->form_validation->set_rules('recomended', 'Recommended', 'trim|required|xss_clean|integer|exact_length[1]');
//        $this->form_validation->set_rules('cost', 'Cost', 'trim|required|integer|is_natural_no_zero|xss_clean');
        $this->form_validation->set_rules('feature_desc', 'Feature Description', 'trim|required|xss_clean');
        if($this->form_validation->run()) {
                 
                if($this->input->post('feature_name')=='Color')
                {
                if (!empty($_FILES['feature_img']['name'])) {
                $config2['upload_path'] = 'uploads/body_color/';
                $config2['allowed_types'] = 'gif|jpg|png';
                $config2['max_size'] = '2048';
                $this->load->library('upload', $config2);
                $img_name = $this->upload->do_uploads('feature_img');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
                }      
                 
                }
                else
                {
                if(!empty($_FILES['feature_img']['name'])){
                $config2['upload_path'] = 'uploads/feature/';
                $config2['allowed_types'] = 'gif|jpg|png';
                $config2['max_size'] = '2048';
                $this->load->library('upload', $config2);
                $img_name = $this->upload->do_uploads('feature_img');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
                }       
                    
                }
            
                
                $data=array(
                'feature_name' => $this->input->post('feature_name'),
                'feature_type' => $this->input->post('feature_type'),
                'recomended' => $this->input->post('recomended'),
                'cost' => $this->input->post('cost'),
                'feature_desc' => $this->input->post('feature_desc'),
                'feature_img' => $img_name,
                'added_by' => 0,
                'datetime' => date('Y-m-d H:i:s')
            );

            $feature_mapping_array['variant_id'] = $data['variant_id'];

            $inserted_id = $this->feature_model->create_feature($data);
            if ($inserted_id) {
                $this->session->set_flashdata('success_msg', 'Feature Successfully Created');
            } else {
                $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
            }
            redirect('feature');
        }else{
            $this->index();
        }
    }

    function delete_feature() {
        $feature_id = $this->input->post('feature_id');
        $result_json;
        $this->db->trans_start();
        $this->feature_model->delete_feature($feature_id);
        $this->feature_model->delete_feature_mapping($feature_id);
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            $result_json = array(
                'sts' => 1,
                'msg' => ''
            );
        } else {
            $result_json = array(
                'sts' => 0,
                'msg' => TRANSACTION_ERROR_MSG
            );
        }
        echo json_encode($result_json);
    }

    function update_feature() {
        $this->load->library('form_validation');
        $feature_id = $this->input->post('feature_id');
        $data['feature_details'] = $this->feature_model->get_feature_details($feature_id);
        $data['features_sub_type'] = $this->feature_model->get_all_features_sub_type();
        $data['content'] = 'feature/edit_feature';
        $this->load->view('index', $data);
    }

    function edit_feature() {      
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="help-block">', '</div>');
        $this->form_validation->set_error_delimiters('<div class="help-block">', '</div>');
        $this->form_validation->set_rules('feature_type', 'Feature type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('feature_name', 'Feature Sub Type', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('feature_type', 'Feature Type', 'trim|required|xss_clean|integer|greater_than[0]|less_than[7]');
        $this->form_validation->set_rules('recomended', 'Recommended', 'trim|required|xss_clean|integer|exact_length[1]');
//        $this->form_validation->set_rules('cost', 'Cost', 'trim|required|integer|is_natural_no_zero|xss_clean');
        $this->form_validation->set_rules('feature_desc', 'Feature Description', 'trim|required|xss_clean');
        if($this->form_validation->run()){
//                $config['upload_path']='uploads/feature/';
//                $config['allowed_types'] = 'gif|jpg|png';
//                $this->load->library('upload', $config);
//                $this->upload->initialize($config);
//                $this->upload->set_allowed_types('*');
//                $data['upload_data'] = '';     
                
                if($this->input->post('feature_name')=='Color')
                {
                if (!empty($_FILES['feature_img']['name'])) {
                $config2['upload_path'] = 'uploads/body_color/';
                $config2['allowed_types'] = 'gif|jpg|png';
                $config2['max_size'] = '2048';
                $this->load->library('upload', $config2);
                $img_name = $this->upload->do_uploads('feature_img');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
                }      
                 
                }
                else
                {
                if(!empty($_FILES['feature_img']['name'])){
                $config2['upload_path'] = 'uploads/feature/';
                $config2['allowed_types'] = 'gif|jpg|png';
                $config2['max_size'] = '2048';
                $this->load->library('upload', $config2);
                $img_name = $this->upload->do_uploads('feature_img');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
                }       
                    
                }
//                
//                
//                
//                
//                
//                
//                $img_name=$this->upload->do_uploads('feature_img');
                if($img_name=='')
                {
                $img_name=$this->input->post('feature_img2');                       
                }              
                $data=array(
                'feature_name' => $this->input->post('feature_name'),
                'feature_type' => $this->input->post('feature_type'),
                'recomended' => $this->input->post('recomended'),
                'cost' => $this->input->post('cost'),
                'feature_desc' => $this->input->post('feature_desc'),
                'feature_img' => $img_name,
                'added_by' => 0,
                'datetime' => date('Y-m-d H:i:s')
                );
                $data['feature_id']=$this->input->post('feature_id');
                
            $sts=$this->feature_model->update_feature($data, $data['feature_id']);
            if ($sts) {
                $this->session->set_flashdata('success_msg', 'Feature Successfully Updated');
            } else {
                $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
            }
            redirect('feature');
        } else {
            $this->update_feature();
        }
    }

    function map_feature() {
        $this->load->library('form_validation');
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();

        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }
//        echo '<pre>';
//        $data['all_features'] = $this->feature_model->get_all_map_features();
//        echo '</pre>';
        $data['content'] = 'feature/map_feature';
        $this->load->view('index', $data);
    }
    
    function map_feature_result(){
        $this->load->library("datatables");
        $actionLinkBar=$this->load->view("content/report/datatable/feature_map_all_action",array(), TRUE);
        $this->datatables
                ->select("id,(select pro_name from variant where variant_id=feature_variant_mapping.variant_id) as pro_name,(select feature_desc from features where feature_id=feature_variant_mapping.feature_id) as feature_desc,price")                              
                ->from("feature_variant_mapping")
                ->add_column("Action",$actionLinkBar,'id');
        echo $this->datatables->generate();
    }
    
    
    
    function del_feature_mapping()
    {
    $feature_id= $this->input->post('feature_id');    
    $this->db->query("delete from feature_variant_mapping where id='$feature_id'");
    redirect('feature/map_feature');
//    exit;
//    $url= $this->input->post('url');    
//    $data['delfeature'] = $this->feature_model->del_all_map_features($feature_id);
//    redirect($url);
    ?>
    <script>
//    window.location.href="<?php echo $url;?>";
//    </script>
    <?php    
    }
    

    function get_all_features() {
        
        $variant_id = $this->input->post('product_id');
        $data['variant_features'] = $this->feature_model->get_variant_features($variant_id);
        $data['features'] = $this->feature_model->get_all_features();
        echo $this->load->view('ajaxfiles/all_features', $data, TRUE);
    }

    function insert_variant_features() {
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="help-block">', '</div>');
        $this->form_validation->set_rules('all_product_name', 'Variant', 'trim|required|xss_clean');
        $this->form_validation->set_rules('feature_names', 'Feature Names', 'trim|required|xss_clean');
        $this->form_validation->set_rules('feature_ids', '', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            $variant_id = $this->input->post('all_product_name');
            $feature_ids = $this->input->post('feature_ids');
            $feature_id_array = explode(',', $feature_ids);
            $last_element_index = count($feature_id_array) - 1;

            if (empty($feature_id_array[$last_element_index])) {
                unset($feature_id_array[$last_element_index]);
            }

            $insert_array = array();
            $update_array = array();
            $temp_array = array();
            foreach ($feature_id_array as $f) {
                $temp_array = array(
                    'feature_id' => $f,
                    'status' => 1,
                    'price' =>$this->input->post('price'),
                    'variant_id' => $variant_id
                );

                array_push($insert_array, $temp_array);

                $temp_array['status'] = 0;

                array_push($update_array, $temp_array);
            }

            $this->db->trans_start();
//            $this->feature_model->delete_previous_variant_feature_mapping($variant_id, $update_array);
            $this->feature_model->create_feature_variant_mapping($insert_array);
            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                $this->session->set_flashdata('success_msg', 'Features Assigned Successfully Created');
            } else {
                log_message('Error', 'Transaction failed in controllers/feature/insert_variant_features for feature');
                $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
            }

            redirect('feature/map_feature');
        } else {
            $this->map_feature();
        }
    }

}