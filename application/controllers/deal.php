<?php

class Deal extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("common");
        $this->load->model("dealprice");
        $this->load->model("product_data");
        $this->load->model("experts_rating");
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');        
    }
    
    function lastestdeal(){
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();
        $data['city'] = $this->common->get_city();
        $data['all_brand'] = $this->dealprice->all_brands();        
        $data['all_res'] = $this->dealprice->get_deal($id = '');
        $data['content']='deal/latestdeal';
        $this->load->view('index',$data);
    }
    
    function deal_upload(){
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();
        $data['city'] = $this->common->get_city();  
        $data['all_brand'] = $this->dealprice->all_brands(); 
        if(isset($_POST['submit'])){            
            $this->form_validation->set_rules('all_product_name', 'Product name', 'required');
            $this->form_validation->set_rules('deals_heading', 'Deals heading', 'required');
            $this->form_validation->set_rules('city_id', 'City id', 'required');
            $this->form_validation->set_rules('discount_type', 'Discount type', 'required');
            $this->form_validation->set_rules('amount_per', 'Amount / Percent', 'required');
            $this->form_validation->set_rules('start_date', 'Start date', 'required');
            $this->form_validation->set_rules('end_date', 'End date', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('term_condition','Terms & condition','required');            
            $rec_data=array(
                'variant_id' => $this->input->post('all_product_name'),
                'deals_heading' => $this->input->post('deals_heading'),
//                'brand_id' => $this->input->post('brand_id'),
                'city_id' => $this->input->post('city_id'),
                'discount_type' => $this->input->post('discount_type'),
                'amount_per' => $this->input->post('amount_per'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'status' => $this->input->post('status'),
                'term_condition' => $this->input->post('term_condition')                
            );
            if(!$this->form_validation->run()) {
                $resp=array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->lastestdeal();
            }else{
                $query['uploded']=$this->dealprice->deal_upload($rec_data);
                redirect('lastestdeal');
            }
        }
        else if(isset($_POST['get'])){   
            
            $data['city'] = $this->common->get_city();  
            $data['all_brand'] = $this->dealprice->all_brands(); 
            $deal_id= $this->input->post('deal_id');   
            $data['sing_res']=$this->dealprice->get_deal($deal_id);
//            print_r();
            $data['single_res_detail'] = $this->experts_rating->show_rating_data($data['sing_res'][0]['variant_id']);
//            print_r($data['single_res_detail']);
            $data['content']='deal/latestdeal';
            $this->load->view('index',$data);
        }
        else if(isset($_POST['update'])){
            $this->form_validation->set_rules('all_product_name', 'Product name', 'required');
            $this->form_validation->set_rules('deals_heading', 'Deals heading', 'required');
//             $this->form_validation->set_rules('brand_id', 'Brand', 'required');
            $this->form_validation->set_rules('city_id', 'City id', 'required');
            $this->form_validation->set_rules('discount_type', 'Discount type', 'required');
            $this->form_validation->set_rules('amount_per', 'Amount / Percent', 'required');
            $this->form_validation->set_rules('start_date', 'Start date', 'required');
            $this->form_validation->set_rules('end_date', 'End date', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('term_condition','Terms & condition','required');
            $deal_id=$this->input->post('deal_id');
            $variant_id=$this->input->post('all_product_name');
            
            $data['single_res_detail'] = $this->experts_rating->show_rating_data($variant_id);
//            print_r($data['single_res_detail']);
            $rec_data=array(
                'variant_id' => $this->input->post('all_product_name'),
//                'brand_id' => $this->input->post('brand_id'),
                'deals_heading' => $this->input->post('deals_heading'),
                'city_id' => $this->input->post('city_id'),
                'discount_type' => $this->input->post('discount_type'),
                'amount_per' => $this->input->post('amount_per'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'status' => $this->input->post('status'),
                'term_condition' => $this->input->post('term_condition')                
            );
            
            if(!$this->form_validation->run()){
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );                
            
            $data['sing_res']=$this->dealprice->get_deal($deal_id);
            $data['content']='deal/latestdeal';
            $this->load->view('index',$data);
            }else{
                
                $query['exshowroom_res'] = $this->dealprice->deal_update($rec_data,$deal_id);
                redirect('lastestdeal');                
            }
        }    
        else if(isset($_POST['delete'])){
            $deal_id= $this->input->post('deal_id');            
            $query=$this->dealprice->deal_del($deal_id);
            redirect('lastestdeal');
        } else {
            redirect('lastestdeal');
        }
    }
    
    function brandeal_upload(){ 

	
        $data['city'] = $this->common->get_city();  
        $data['all_brand'] = $this->dealprice->all_brands();
        if(isset($_POST['submit'])){            
            $this->form_validation->set_rules('product_name', 'Brand name', 'required');
            //$this->form_validation->set_rules('deals_heading', 'Deals heading', 'required');
            $this->form_validation->set_rules('city_id', 'City id', 'required');
            $this->form_validation->set_rules('discount_type', 'Discount type', 'required');
            $this->form_validation->set_rules('amount_per', 'Amount / Percent', 'required');
            $this->form_validation->set_rules('start_date', 'Start date', 'required');
            $this->form_validation->set_rules('end_date', 'End date', 'required');
            //$this->form_validation->set_rules('status', 'Status', 'required');
            //$this->form_validation->set_rules('term_condition','Terms & condition','required');            
            $rec_data=array(
                'brand_id'=>$this->input->post('product_name'),
                'deals_heading' => $this->input->post('deals_heading'),
//                'brand_id' => $this->input->post('brand_id'),
                'city_id' => $this->input->post('city_id'),
                'discount_type' => $this->input->post('discount_type'),
                'amount_per' => $this->input->post('amount_per'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'status' => $this->input->post('status'),
                'term_condition' => $this->input->post('term_condition')                
            );
            if(!$this->form_validation->run()) {
                $resp=array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->groupdeal();
            }else{
//                echo 'sdsds';
//                exit;
                $query['uploded']=$this->dealprice->brandeal_upload($rec_data);
                redirect('groupdeal');
            }
        }
        else if(isset($_POST['get'])){        
            echo $deal_id= $this->input->post('deal_id');   
            $data['sing_res']=$this->dealprice->get_deal($deal_id);
            $data['content']='deal/latestdeal';
            $this->load->view('index',$data);
        }
        else if(isset($_POST['update'])){
           $this->form_validation->set_rules('product_name', 'Brand name', 'required');
            //$this->form_validation->set_rules('deals_heading', 'Deals heading', 'required');
            $this->form_validation->set_rules('city_id', 'City id', 'required');
            $this->form_validation->set_rules('discount_type', 'Discount type', 'required');
            $this->form_validation->set_rules('amount_per', 'Amount / Percent', 'required');
            $this->form_validation->set_rules('start_date', 'Start date', 'required');
            $this->form_validation->set_rules('end_date', 'End date', 'required');
            //$this->form_validation->set_rules('status', 'Status', 'required');
            //$this->form_validation->set_rules('term_condition','Terms & condition','required'); 
            $deal_id=$this->input->post('deal_id');
            $rec_data=array(
                'variant_id' => $this->input->post('product_name'),
//                'brand_id' => $this->input->post('brand_id'),
                'deals_heading' => $this->input->post('deals_heading'),
                'city_id' => $this->input->post('city_id'),
                'discount_type' => $this->input->post('discount_type'),
                'amount_per' => $this->input->post('amount_per'),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'status' => $this->input->post('status'),
                'term_condition' => $this->input->post('term_condition')                
            );
            
            if(!$this->form_validation->run()){
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );                
            
            $data['sing_res']=$this->dealprice->get_deal($deal_id);
            $data['content']='deal/latestdeal';
            $this->load->view('index',$data);
            }else{
                
                $query['exshowroom_res'] = $this->dealprice->deal_update($rec_data,$deal_id);
                redirect('lastestdeal');                
            }
        }    
        else if(isset($_POST['delete'])){
            $deal_id= $this->input->post('deal_id');    
            $query=$this->dealprice->brandeal_del($deal_id);
            redirect('groupdeal');
        } else {
            redirect('groupdeal');
        }
    }
    
    
     function groupdeal(){
        $data['city'] = $this->common->get_city();
        $data['all_brand'] = $this->dealprice->all_brands();        
        $data['all_res'] = $this->dealprice->get_branddeal($id = '');
        $data['content']='deal/groupdeal';
        $this->load->view('index',$data);
    }
    

}
