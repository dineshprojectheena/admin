<?php

class Price extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("common");
        $this->load->model("mncprice");
        $this->load->model("product_data");
        $this->load->model("experts_rating");
//        $this->load->model("common","price");
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    
    function onRoad() {
        $data['content'] = 'masters/onRoadPrice';
         $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        $data['city'] = $this->common->get_city();
//        $data['all_res'] = $this->mncprice->get_onRoad($exshowroom_price_id = '');
        $this->load->view('index', $data);
    }
    
     
    function onroad_result(){
        $this->load->library("datatables");
        $actionLinkBar=$this->load->view("content/report/datatable/onroad_all_action",array(), TRUE);
        $this->datatables
                ->select("onroad_price_id,(select pro_name from variant where variant_id=onroad_price.variant_id) as pro_name,(select name from city where city_id=onroad_price.city_id) as city_name,octroi,registeration_charges,handling_price,comp_insurance,other_charges")                              
                ->from("onroad_price")
                ->add_column("Action",$actionLinkBar,'onroad_price_id');
        echo $this->datatables->generate();
    }
    
    
    
    
    function onRoad_upload() { 
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();        
        $data['city'] = $this->common->get_city();
        if(isset($_POST['submit'])){     
            $this->form_validation->set_rules('all_product_name', 'product name', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('octroi', ' Local Body Tax (Octroi)', 'trim|required|max_length[15]');
//            $this->form_validation->set_rules('road_tax', 'Road tax', 'trim|required|xss_clean');
            $this->form_validation->set_rules('registeration_charges', 'registeration charges', 'trim|required|xss_clean|numeric');
//            $this->form_validation->set_rules('insurance_premium', 'insurance premium', 'trim|required|xss_clean|numeric');
            $this->form_validation->set_rules('handling_price', 'handling price', 'required|trim|xss_clean|numeric');
            $this->form_validation->set_rules('other_charges', 'other charges', 'required|trim|xss_clean|numeric');
            $this->form_validation->set_rules('comp_insurance', 'Comprehensive Insurance', 'required|trim|xss_clean|numeric');
//            $this->form_validation->set_rules('on_road_price', 'on road price', 'required|trim|xss_clean|numeric');
//            $this->form_validation->set_rules('home_delievery_charge', 'home delivery charge', 'required|trim|xss_clean|numeric');
//            $this->form_validation->set_rules('annual_maintanance_contact', 'annual maintanance contact', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('warranty_extension', 'warranty extension', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('accessories', 'accessories', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('total_price', 'total price', 'required|trim|xss_clean|numeric');
//            $this->form_validation->set_rules('booking_amount', 'booking amount', 'required|trim|xss_clean|numeric');
            $added_date=date('Y-m-d h:i:s');
            $rec_data=array(
                'variant_id' => $this->input->post('all_product_name'),
                'city_id' => $this->input->post('city'),
                'octroi' => $this->input->post('octroi'),
                'registeration_charges' => $this->input->post('registeration_charges'),
                'handling_price' => $this->input->post('handling_price'),
                'other_charges' => $this->input->post('other_charges'),
                'comp_insurance' => $this->input->post('comp_insurance'),
                'added_date' => $added_date
            );
            
            
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->onRoad();
            } else {
                $query['exshowroom_res']=$this->mncprice->onRoad_upload($rec_data);
                redirect('onRoad');

            }
        }
        else if(isset($_POST['get'])){        
            $onroad_price_id=$this->input->post('onroad_price_id');            
            $data['city'] = $this->common->get_city();
            $data['sing_res']=$this->mncprice->get_onRoad($onroad_price_id);
//            print_r($data['sing_res']['0']['variant_id']);
            
            $data['single_res_detail'] = $this->experts_rating->show_rating_data($data['sing_res']['0']['variant_id']);
            $data['content']='masters/onRoadPrice';
            $this->load->view('index',$data);
        }
        else if(isset($_POST['update'])){
//            $this->form_validation->set_rules('product_name', 'product name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('all_product_name', 'product name', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('octroi', ' Local Body Tax (Octroi)', 'trim|required|max_length[15]');
//            $this->form_validation->set_rules('road_tax', 'Road tax', 'trim|required|xss_clean');
            $this->form_validation->set_rules('registeration_charges', 'registeration charges', 'trim|required|xss_clean|numeric');
//            $this->form_validation->set_rules('insurance_premium', 'insurance premium', 'trim|required|xss_clean|numeric');
            $this->form_validation->set_rules('handling_price', 'handling price', 'required|trim|xss_clean|numeric');
            $this->form_validation->set_rules('other_charges', 'other charges', 'required|trim|xss_clean|numeric');
            $this->form_validation->set_rules('comp_insurance', 'Comprehensive Insurance', 'required|trim|xss_clean|numeric');
//            $this->form_validation->set_rules('on_road_price', 'on road price', 'required|trim|xss_clean|numeric');
//            $this->form_validation->set_rules('home_delievery_charge', 'home delivery charge', 'required|trim|xss_clean|numeric');
//            $this->form_validation->set_rules('annual_maintanance_contact', 'annual maintanance contact', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('warranty_extension', 'warranty extension', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('accessories', 'accessories', 'required|trim|xss_clean');
//            $this->form_validation->set_rules('total_price', 'total price', 'required|trim|xss_clean|numeric');
//            $this->form_validation->set_rules('booking_amount', 'booking amount', 'required|trim|xss_clean|numeric');
            $added_date=date('Y-m-d H:i:s');            
            $onroad_price_id= $this->input->post('onroad_price_id');
            $added_date = date('y-m-d h:i:s');
            $rec_data=array(
//                'product_name' => $this->input->post('product_name'),
                'variant_id' => $this->input->post('all_product_name'),
                'city_id' => $this->input->post('city'),
                'octroi' => $this->input->post('octroi'),
                'registeration_charges' => $this->input->post('registeration_charges'),
                'handling_price' => $this->input->post('handling_price'),
                'other_charges' => $this->input->post('other_charges'),
                'comp_insurance' => $this->input->post('comp_insurance'),
                'added_date' => $added_date
            );
            if(!$this->form_validation->run()){
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );                
            
            $data['sing_res']=$this->mncprice->get_onRoad($onroad_price_id);
            $data['content']='masters/onRoadPrice';
            $this->load->view('index', $data);
            }else{
                $query['exshowroom_res'] = $this->mncprice->onRoad_update($rec_data,$onroad_price_id);
                redirect('onRoad');                
            }
        }    
        else if (isset($_POST['delete'])) {
            $onroad_price_id= $this->input->post('onroad_price_id');
            $query=$this->mncprice->onRoad_del($onroad_price_id);
            redirect('onRoad');
        } else {
            redirect('onRoad');
        }
    }
    
    

    function search_product() {
        $query = $_GET['q'];
        $result = $this->common->get_product_detail($query);
        echo json_encode($result);
    }
    
    
    function search_brand() {
        $query = $_GET['q'];
        $result = $this->common->get_brand_detail($query);
        echo json_encode($result);
    }
    
    
     

    #### Ex showroom process goes here ####
    
    function exShowroom() {
        $data['city'] = $this->common->get_city();
        $data['all_res'] = $this->mncprice->get_exShowroom($exshowroom_price_id = '');
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        $data['content'] = 'masters/exshowroomPrice';
        $this->load->view('index', $data);
    }    


    function exShowroom_upload() {
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('all_product_name', 'variant', 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('ex_price', 'Ex showroom price', 'trim|required|numeric|max_length[15]');
//            $this->form_validation->set_rules('price_available', 'price applicable', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('source', 'source', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('author', 'author', 'required|trim|xss_clean');
            $product_id = $this->input->post('all_product_name');
            $city = $this->input->post('city');
            
            $added_date = date('y-m-d h:i:s');
            $rec_data = array(
                'product_id' => $product_id,
                'city_id' => $this->input->post('city'),
                'exshowroom_price' => $this->input->post('ex_price'),
                'source' => $this->input->post('source'),
                'author' => $this->input->post('author'),
                'price_applicable_since' => $this->input->post('price_available'),
                'status' => $this->input->post('status'),
                'added_date' => $added_date
            );
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->exShowroom();
            } else {
                $query['exshowroom_res'] = $this->mncprice->exShowroom_upload($rec_data, $product_id,$city);
//                if (empty($query['exshowroom_res'])) {
//                    $data['exshowroom_res'] = 'The product given already upload,try something new';
//                    $this->exShowroom();
//                } else {
//                    $data['exshowroom_res'] = 'The product uploaded successfully!';
                    redirect('exShowroom');
//                }
            }
        } else if(isset($_POST['get'])) {
            $exshowroom_price_id=$this->input->post('exshowroom_price_id');            
            $data['city'] = $this->common->get_city();
            $data['sing_res'] = $this->mncprice->get_exShowroom($exshowroom_price_id);
            $data['content'] = 'masters/exshowroomPrice';
            $this->load->view('index', $data);
        } else if (isset($_POST['update'])) {
            $this->form_validation->set_rules('all_product_name', 'product name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('ex_price', 'Ex showroom price', 'trim|required|numeric|max_length[15]');
//            $this->form_validation->set_rules('price_available', 'price applicable', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('source', 'source', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('author', 'author', 'required|trim|xss_clean');
            $product_id = $this->input->post('all_product_name');
            $city = $this->input->post('city');
            $exshowroom_price_id= $this->input->post('exshowroom_price_id');
            $added_date = date('y-m-d h:i:s');
            $rec_data = array(
                'product_id' => $product_id,
                'city_id' => $this->input->post('city'),
                'exshowroom_price' => $this->input->post('ex_price'),
                'source' => $this->input->post('source'),
                'author' => $this->input->post('author'),
                'price_applicable_since' => $this->input->post('price_available'),
                'status' => $this->input->post('status'),
                'added_date' => $added_date
            );
            if(!$this->form_validation->run()){
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
            );                
            $exshowroom_price_id=$this->input->post('exshowroom_price_id');            
            $data['city'] = $this->common->get_city();
            $data['sing_res'] = $this->mncprice->get_exShowroom($exshowroom_price_id);
            $data['content'] = 'masters/exshowroomPrice';
            $this->load->view('index', $data);
            }else{
                $query['exshowroom_res'] = $this->mncprice->exShowroom_update($rec_data,$exshowroom_price_id,$product_id,$city);
                redirect('exShowroom');                
            }
        } else if (isset($_POST['delete'])) {
            $exshowroom_price_id = $this->input->post('exshowroom_price_id');
            $query = $this->mncprice->exShowroom_del($exshowroom_price_id);
            $this->exShowroom();
        } else {
            redirect('exShowroom');
        }
    }
    
    function mnc() {
        $data['city'] = $this->common->get_city();
//        $data['all_res'] = $this->mncprice->get_mnc($exshowroom_price_id = '');
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        $data['content'] = 'masters/mncExshowroomPrice';
        $this->load->view('index', $data);
    }
    
    function mnc_result(){
        $this->load->library("datatables");
        $this->load->helper("datatables");
        $actionLinkBar=$this->load->view("content/report/datatable/mnc_all_action",array(), TRUE);
        $this->datatables
                ->select("mnc_exshowroom_price_id,(select pro_name from variant where variant_id=mnc_exshowroom_price.product_id) as pro_name,(select name from city where city_id=mnc_exshowroom_price.city_id) as city_name,mnc_exshowroom_price,discount_percent,discount_rs,status,added_date")                              
//                ->select("mnc_exshowroom_price_id,(select pro_name from variant where variant_id=mnc_exshowroom_price.product_id) as pro_name,(select name from city where city_id=mnc_exshowroom_price.city_id) as city_name,mnc_exshowroom_price,(select exshowroom_price from exshowroom_price where product_id=mnc_exshowroom_price.product_id) as exshowroom_price,discount_percent,discount_rs,status,added_date")                              
//                ->select("product_gallery_id,(select pro_name from variant where variant_id=product_gallery.prduct_id) as pro_name,page,image,video")                              
                ->from("mnc_exshowroom_price")
                ->edit_column('status', '$1', 'check_status(status)')
//                ->edit_column("image",$actionLinkBar,'image,'.$a)
//                ->edit_column("video",$actionLinkBar,'video,'.$b)
                ->add_column("Action",$actionLinkBar,'mnc_exshowroom_price_id');
//                ->edit_column('','','','','','','Gallery Image', '$1');

        echo $this->datatables->generate();
    }
    
    
    
    function mnc_upload(){
         $data['model'] = $this->product_data->get_model();
         $data['brand'] = $this->product_data->get_brand();
        if(isset($_POST['submit'])){
            $this->form_validation->set_rules('all_product_name', 'product name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('ex_price', 'MNC Ex showroom price', 'trim|required|numeric|max_length[15]');
//            $this->form_validation->set_rules('discount_percent', 'discount percent', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('discount_rs', 'discount rs', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('authors', 'authors', 'trim|required|xss_clean');
            $this->form_validation->set_rules('mnc_exshowroom_price', 'Ex showroom price', 'trim|required|xss_clean');
            $this->form_validation->set_rules('delivery_date', 'Delivery Date', 'trim|required|xss_clean');
            $product_id = $this->input->post('all_product_name');
//            exit;
            $data['single_res_detail'] = $this->experts_rating->show_rating_data($product_id);
            $city= $this->input->post('city');
            $added_date = date('y-m-d h:i:s');
            $rec_data = array(
                'product_id' => $product_id,
                'city_id' => $this->input->post('city'),
                'mnc_exshowroom_price' => $this->input->post('mnc_exshowroom_price'),
                'discount_percent' => $this->input->post('discount_percent'),
                'discount_rs' => $this->input->post('discount_rs'),
                'authors' => $this->input->post('authors'),
                'status' => $this->input->post('status'),
                'delivery_date' => $this->input->post('delivery_date'),
                'added_date' => $added_date
            );
            
            $rec_data2 = array(
                'product_id' => $product_id,
                'city_id' => $this->input->post('city'),
                'exshowroom_price' => $this->input->post('ex_price'),
                'source' => $this->input->post('source'),
                'author' => $this->input->post('author'),
                'price_applicable_since' => $this->input->post('price_available'),
                'status' => $this->input->post('status'),
                'delivery_date' => $this->input->post('delivery_date'),
                'added_date' => $added_date
            );
            
//           print_r($rec_data2); 
            
            
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->mnc();
            } else {
                $query['exshowroom_res']=$this->mncprice->mnc_upload($rec_data, $product_id,$city,$rec_data2);
//                if (empty($query['exshowroom_res'])) {
//                    $data['exshowroom_res'] = 'The product given already upload,try something new';
//                    $this->mnc();
//                } else {
                    $data['exshowroom_res'] = 'The product uploaded successfully!';
                    redirect('mnc');
//                }
            }
        } else if(isset($_POST['get'])) {
            $mnc_exshowroom_price_id=$this->input->post('mnc_exshowroom_price_id');            
            
            $data['city'] = $this->common->get_city();
//            echo '<pre style="margin-left:100px;">';
            $data['sing_res'] = $this->mncprice->get_mnc($mnc_exshowroom_price_id);
            $data['single_res_detail'] = $this->experts_rating->show_rating_data($data['sing_res'][0]['product_id']);
//            print_r($data['single_res_detail']);
//            echo '</pre>';
            $data['content'] = 'masters/mncExshowroomPrice';
            $this->load->view('index', $data);
        } else if (isset($_POST['update'])) {
            $this->form_validation->set_rules('all_product_name', 'product name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('ex_price', 'MNC Ex showroom price', 'trim|required|numeric|max_length[15]');
//            $this->form_validation->set_rules('discount_percent', 'discount percent', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('discount_rs', 'discount rs', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('authors', 'authors', 'trim|required|xss_clean');
            $this->form_validation->set_rules('mnc_exshowroom_price', 'Ex showroom price', 'trim|required|xss_clean');
            $this->form_validation->set_rules('delivery_date', 'Delivery Date', 'trim|required|xss_clean');
            $product_id = $this->input->post('all_product_name');
            $city = $this->input->post('city');
            $added_date = date('y-m-d h:i:s');
                       
             $rec_data = array(
                'product_id' => $product_id,
                'city_id' => $this->input->post('city'),
                'mnc_exshowroom_price' => $this->input->post('mnc_exshowroom_price'),
                'discount_percent' => $this->input->post('discount_percent'),
                'discount_rs' => $this->input->post('discount_rs'),
                'authors' => $this->input->post('authors'),
                'status' => $this->input->post('status'),
                 'delivery_date' => $this->input->post('delivery_date'),
                'added_date' => $added_date
            );
            
            $rec_data2 = array(
                'product_id' => $product_id,
                'city_id' => $this->input->post('city'),
                'exshowroom_price' => $this->input->post('ex_price'),
                'source' => $this->input->post('source'),
                'author' => $this->input->post('author'),
                'price_applicable_since' => $this->input->post('price_available'),
                'status' => $this->input->post('status'),
                'delivery_date' => $this->input->post('delivery_date'),
                'added_date' => $added_date
            );
            
            $mnc_exshowroom_price_id= $this->input->post('mnc_exshowroom_price_id');
            $exshowroom_price_id= $this->input->post('exshowroom_price_id');
            if(!$this->form_validation->run()){
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );                
//            $exshowroom_price_id=$this->input->post('exshowroom_price_id');            
            $data['city']=$this->common->get_city();
            $data['sing_res'] = $this->mncprice->get_mnc($exshowroom_price_id);
            $data['content'] = 'masters/mncExshowroomPrice';
            $this->load->view('index', $data);
            }else{
//                print_r($rec_data);
//                echo '<br>';
//                print_r($exshowroom_price_id);
//                echo '<br>';
//                print_r($product_id);
//                exit;
                $query['exshowroom_res'] = $this->mncprice->mnc_update($rec_data,$rec_data2,$mnc_exshowroom_price_id,$exshowroom_price_id,$city);
                redirect('mnc');                
            }
        } else if (isset($_POST['delete'])) {
            $mnc_exshowroom_price_id= $this->input->post('mnc_exshowroom_price_id');
            $query = $this->mncprice->mnc_del($mnc_exshowroom_price_id);
            $this->mnc();
        } else {
            redirect('mnc');
        }
    }

}
