<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reviews extends CI_Controller {

    public function __construct() {
        error_reporting(0);
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Reviews_mdl');
        $this->load->model('home_section');
    }

    function user_review($id = '') {
        $data['id'] = substr($id, -1);
        ;
        if (empty($data['id'])) {
            $data['id'] = $this->input->post('pro_id');
        }
        $data['content'] = 'userReviewPage';
        $data['exp_review_count'] = $this->home_section->expert_review_count($data['id'], $id = '1');
        $data['user_review_count'] = $this->home_section->expert_review_count($data['id'], $id = '2');
        $data['pro_details'] = $this->home_section->reting_product_detail($data['id']);
        $data['session'] = $this->session->userdata("logged_in_user");
        $data['pro_ext'] = $this->home_section->rating_details($data['id']);
        $this->load->view('index', $data);
    }

    function review() {
        if (isset($_POST['review_submit'])) {
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('comment', 'heading', 'required');
            $this->form_validation->set_rules('like', 'I like', 'trim|required|xss_clean');
            $this->form_validation->set_rules('dislike', "I Don't like", 'trim|required|xss_clean');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->user_review();
            } else {
                $rating = $this->input->post('rating');
                $tot = count($rating);
                $gt = ($rating[0] + $rating[1] + $rating[2] + $rating[3] + $rating[4] + $rating[5] + $rating[6] + $rating[7] + $rating[8]) / $tot;
                $rec_data = array(
                    'pro_id' => $this->input->post('pro_id'),
                    'pro_type' => $this->input->post('pro_type'),
                    'user_id' => $this->input->post('user_id'),
                    'date' => date('Y-m-d'),
                    'heading' => $this->input->post('comment'),
                    'what_i_like' => $this->input->post('like'),
                    'what_i_dont_like' => $this->input->post('dislike'),
                    'comfort' => $rating['0'],
                    'space' => $rating['1'],
                    'safety' => $rating['2'],
                    'design' => $rating['3'],
                    'engine' => $rating['4'],
                    'fuel_economy' => $rating['5'],
                    'ride_handling' => $rating['6'],
                    'maintainence' => $rating['7'],
                    'value_for_money' => $rating['8'],
                    'total' => $gt,
                    'added_date' => date('Y-m-d')
                );
                $query = $this->Reviews_mdl->user_review($rec_data);
                redirect('');
            }
        } else {
            redirect('user-review');
        }
    }

    function load_reviews() {
        $data['session'] = $this->session->userdata("logged_in_user");
//        $img = base_url() . '/uploads/user/' . $data['session']['fb_id'] . '.jpg';
        $pro_id = $this->input->post('pro_id');
        $user_sort = $this->input->post('user_sort');
        $res = $this->Reviews_mdl->get_review($pro_id, $user_sort, $id = '1');
        $tot = $this->Reviews_mdl->get_review($pro_id, $user_sort, $id = '2');
        echo $tot_rev = $tot . '-Total';
        if ($tot > 0) {
            $i = 1;
            foreach ($res as $data) {
                $get_image = $this->Reviews_mdl->get_user_review_img($data['user_id']);
                if ($get_image[0]['fb_id']==''){
                    $fb_id = 'no-image.png';
                }
                $img = base_url() . '/uploads/user/'.$fb_id;
                ?>
                <tr>
                    <td  style="text-align: left !important;">
                        <div class="row user-comment-box" >
                            <div  class="col-md-2 col-sm-2 text-center">
                                <img src="<?php echo $img; ?>" style="max-width: 85px;" class="img-circle img-thumbnail img-center">
                                <div class="rating-div">
                                    <span class="text-golden">
                                        <?php
                                        $star = $data['total'];
                                        $exp = explode('.', $star);
                                        $point = $exp[1];
                                        if ($point > 00) {
                                            $half = '-half-full';
                                        } else if ($point == 00) {
                                            $half = '-o';
                                        } else {
                                            $half = '';
                                        }

                                        if (empty($star)) {
                                            ?>
                                            <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        }

                                        if ($exp[0] == 1) {
                                            ?> 
                                            <i class="fa fa-star"></i><?php ?>
                                            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        } else if ($exp[0] == 2) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 3) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 4) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                        }
                                        ?>
                                    </span>
                                    <p class="date-p">Posted On: <span><?php
                                            echo $new_date = date('d F, Y', strtotime($data['date']));
                                            ?></span></p>
                                </div>
                            </div>


                            <div class="user-details col-md-10 col-sm-10">
                                <h4 class="user-comment-heading"> <?php echo $data['heading']; ?>
                                    <?php
                                    if ($i == 1) {
                                        $class = '';
                                        ?>
                                        <span>[ - Less ]</span>
                                        <?php
                                    } else {
                                        $class = 'hide';
                                        ?>    
                                        <span>[ + More ]</span>  
                                        <?php
                                    }
                                    ?>
                                </h4>
                                <div  class="user-comment <?php echo $class; ?>">
                                    <div class="clearfix">
                                        <div style="width: 4%;" class="pull-left">
                                            <i title="" data-placement="right" data-toggle="tooltip" class="fa fa-thumbs-up fa-2x tiphere" data-original-title="What User Liked"></i>
                                        </div>
                                        <div style="width: 96%;" class="pull-right">
                                            <p>
                                                <?php echo $data['what_i_like']; ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div style="width: 4%;" class="pull-left">
                                            <i title="" data-placement="right" data-toggle="tooltip" class="fa fa-thumbs-down fa-2x tiphere" data-original-title="What User Didnt Like"></i>
                                        </div>
                                        <div style="width: 96%;" class="pull-right">
                                            <p>
                                                <?php echo $data['what_i_dont_like']; ?>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <p class="review-count">
                                    <strong><?php echo $tot; ?></strong> Users found this review Helpful
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php
                $i++;
            }
        } else {
            $userdata = $this->session->userdata('logged_in_user');
            if ($userdata['logged_in'] == 'TRUE') {
                ?>  
                <div align="center">
                    <a id="a1" href="<?php echo site_url('user-review/car-' . $pro_id); ?>"  >There are no reviews yet.Be first one to review this car</a>
                </div>
                <?php
            } else {
                ?>
                <div align="center">
                    <a id="a1" href="#" data-toggle="modal" data-target="#socialLoginModal" >There are no reviews yet.Be first one to review this car</a>
                </div>
                <?php
            }
        }
    }

    function load_exp_reviews() {
        $pro_id = $this->input->post('pro_id');
        $user_sort = $this->input->post('user_sort');
        $pag = $this->input->post('pag');
        $res = $this->Reviews_mdl->get_exp_review($pro_id, $user_sort, $id = '1');
        $tot = $this->Reviews_mdl->get_exp_review($pro_id, $user_sort, $id = '2');
        echo $tot_rev = $tot . '-Total';
        if ($tot > 0) {
            $i = '1';
            ?>
            <?php
            foreach ($res as $data) {
                ?>
                <tr>
                    <td  style="text-align: left !important;">
                        <div class="row user-comment-box">
                            <div style="" class="col-md-2 col-sm-2 text-center">
                                <img src="<?php echo base_url() . 'admin/uploads/expert/' . $data['profile_id']; ?>" style="max-width: 85px;" class="img-circle img-thumbnail img-center">
                                <div class="rating-div">
                                    <span class="text-maroon">
                                        <?php
                                        $star = $data['total'];
                                        $exp = explode('.', $star);
                                        $point = $exp[1];
                                        if ($point > 00) {
                                            $half = '-half-full';
                                        } else if ($point == 00) {
                                            $half = '-o';
                                        } else {
                                            $half = '';
                                        }
                                        if (empty($star)) {
                                            ?>
                                            <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        }

                                        if ($exp[0] == 1) {
                                            ?>
                                            <i class="fa fa-star"></i><?php
                                            ?>
                                            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
                                            <?php
                                        } else if ($exp[0] == 2) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 3) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
                                        } else if ($exp[0] == 4) {
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
                                        }
                                        ?>
                                    </span>
                                    <p class="date-p">Posted On: <span><?php
                                            echo $new_date = date('d F, Y', strtotime($data['date']));
                                            ?></span></p>
                                </div>
                            </div>


                            <div class="user-details col-md-10 col-sm-10">
                                <h4 class="user-comment-heading"> 
                                    <?php echo $data['heading']; ?>
                                    <?php
                                    if ($i == 1) {
                                        $class = '';
                                        ?>
                                        <span>[ - Less ]</span>
                                        <?php
                                    } else {
                                        $class = 'hide';
                                        ?>    
                                        <span>[ + More ]</span>  
                                        <?php
                                    }
                                    ?>
                                </h4>
                                <div  class="user-comment <?php echo $class; ?>">
                                    <div class="clearfix">
                                        <div style="width: 4%;" class="pull-left">
                                            <i title="" data-placement="right" data-toggle="tooltip" class="fa fa-thumbs-up fa-2x tiphere" data-original-title="What User Liked"></i>
                                        </div>
                                        <div style="width: 96%;" class="pull-right">
                                            <p>
                                                <?php echo $data['what_i_like']; ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div style="width: 4%;" class="pull-left">
                                            <i title="" data-placement="right" data-toggle="tooltip" class="fa fa-thumbs-down fa-2x tiphere" data-original-title="What User Didnt Like"></i>
                                        </div>
                                        <div style="width: 96%;" class="pull-right">
                                            <p>
                                                <?php echo $data['what_i_dont_like']; ?>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <p class="review-count">
                                    <strong><?php echo $tot; ?></strong> Users found this review Helpful
                                </p>
                            </div>


                        </div>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>

            <?php
        } else {
            echo '<div align="center"><a href="#" style="text-decoration:none;">There are no expert reviews yet.</a></div>';
        }
    }

    function tot_exp_reviews() {
        $pro_id = $this->input->post('pro_id');
        $type = $this->input->post('type');
        $res = $this->Reviews_mdl->get_tot_review($pro_id, $type);
        echo count($res);
    }

    function tot_user_reviews() {
        $pro_id = $this->input->post('pro_id');
        $user_res = $this->Reviews_mdl->tot_user_reviews($pro_id, $id = '1');
        $exp_res = $this->Reviews_mdl->tot_user_reviews($pro_id, $id = '2');
        #### user wise product rating result
        foreach ($user_res as $data)
            $star = $data->total;
        ?>
        <?php
        $exp = explode('.', $star);
        $point = $exp[1];
        if ($point > 00) {
            $half = '-half-full';
        } else if ($point == 00) {
            $half = '-o';
        } else {
            $half = '';
        }

        if ($exp[0] == 1) {
            ?>
            <i class="fa fa-star"></i><?php
            ?>
            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
            <?php
        } else if ($exp[0] == 2) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
        } else if ($exp[0] == 3) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
        } else if ($exp[0] == 4) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
        }

        #### expert wise product rating result

        echo 'exp_result';

        foreach ($exp_res as $data2)
            $star2 = $data2->total;
        ?>
        <?php
        $exp = explode('.', $star2);
        $point = $exp[1];
        if ($point > 00) {
            $half = '-half-full';
        } else if ($point == 00) {
            $half = '-o';
        } else {
            $half = '';
        }

        if ($exp[0] == 1) {
            ?>
            <i class="fa fa-star"></i><?php
            ?>
            <i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
            <?php
        } else if ($exp[0] == 2) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><?php
        } else if ($exp[0] == 3) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><i class="fa fa-star-o"></i><?php
        } else if ($exp[0] == 4) {
            ?>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star<?php echo $half; ?>"></i><?php
        }
    }

}
