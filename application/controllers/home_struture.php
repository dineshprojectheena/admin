<?php

class Home_struture extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('home_struture_data');
        $this->load->library('form_validation');
    }
    function index(){
        $data['content'] = 'home structure';
        $id = '';
//        echo '<pre style="margin-left:100px;">';
        $type='car_comparison';
        $data['load_res']= $this->home_struture_data->load_featured($id,$type);
//        $ress=array();
//        foreach($data as $res)
//        {
//        $data1['prd_id_name1'] = $this->home_struture_data->get_product_detail($res['prd_id_1']);    
//        $data1['prd_id_name2'] = $this->home_struture_data->get_product_detail($res['prd_id_2']);    
//        $data1['featured_id'] = array('featured_id'=>$res['featured_id']);    
//        $data1['status'] = array('status'=>$res['status']);    
//        $final=  array_merge($data1['featured_id'],$data1['prd_id_name1'],$data1['prd_id_name2'],$data1['status']);
//        $ress[]=$final;
//        }
//        
//        $data['load_res']=$ress;
//        echo '</pre>';
        $this->load->view('home_struture', $data);
    }
    
    
    function home_structure_result(){
        $this->load->library("datatables");
        $this->load->helper("datatables");
//        $a='car_comparison';
        $actionLinkBar=$this->load->view("content/report/datatable/comparision_action",array(), TRUE);
        $this->datatables
                ->select("featured_id,(select pro_name from variant where variant_id=featured.prd_id_1)  as pro_name,(select pro_name from variant where variant_id=featured.prd_id_2) as pro_name2,status")                       
                ->from("featured")
                ->where('status', '$1', 'check_status(status)')
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action",$actionLinkBar,'featured_id');
        echo $this->datatables->generate();
    }
    
    
    
    function product_load() {
        $id = $this->input->post('id');
        $searchid = $this->input->post('searchid');
        $searchid2 = $this->input->post('searchid2');
        $query=$this->home_struture_data->search_product($searchid, $searchid2);
        if(count($query) > 0) {
            foreach ($query as $res) {
                $name = $res->pro_name_comp;
                $product_id=$res->variant_id;
                ?>
                <div class="show" align="left" onclick="setdata('<?php echo $name; ?>', '<?php echo $id; ?>', '<?php echo $product_id; ?>');">
                    <span class="name"><?php echo $name; ?></span>
                </div>
                <?php
            }
        }
    }

    function car_comparison() {
        if (isset($_POST['car_comp_submit'])) {
            $this->form_validation->set_rules('searchid', 'first product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('searchid2', 'second product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->index();
            } else {
                $data = array(
                    'status' => $this->input->post('status'),
                    'prd_id_1' => $this->input->post('pro_1'),
                    'prd_id_2' => $this->input->post('pro_2'),
                    'added_date' => date("Y-m-d h:i:s"),
                    'featured_type' => 'car_comparison'
                );


                $query = $this->home_struture_data->upload_car_comparison($data);
                redirect('home_content');
            }
        } else if (isset($_POST['car_comp_update'])) {
            
            $this->form_validation->set_rules('searchid', 'first product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('searchid2', 'second product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->get_car_comparison();
            } else {
                $featured_id = $this->input->post('featured_id');
                $data = array(
                    'prd_id_1' => $this->input->post('pro_1'),
                    'prd_id_2' => $this->input->post('pro_2'),
                    'status' => $this->input->post('status'),
                    'added_date' => date("Y-m-d h:i:s"),
                    'featured_type' => 'car_comparison'
                );

                $query = $this->home_struture_data->update_car_comparison($data, $featured_id);
                redirect('home_content');
            }
        } else if (isset($_POST['delete'])) {
            $featured_id = $this->input->post('featured_id');
            $query = $this->home_struture_data->del_car_comparison($featured_id);
            redirect('home_content');
        }
        else {
            redirect('home_content');   
        }
    }

    function get_car_comparison() {
//            echo '<pre style="margin-left:200px;">';
            $featured_id=$this->input->post('featured_id');
            $type='car_comparison';
            $data['final']=$this->home_struture_data->load_featured($featured_id,$type);
//            print_r($data['final']);
            
//            $final=array();
//            foreach($data as $res)
//            $data['featured_id'] = array('featured_id' => $featured_id,'status' => $res['status']);
//            $pro1=$res['prd_id_1'];
//            $data['prd_id_name1'] = $this->home_struture_data->get_product_detail($pro1);
////            $final[]=$data['prd_id_name1'];
//            $pro2=$res['prd_id_2'];
//            $data['prd_id_name2'] = $this->home_struture_data->get_product_detail($pro2);
//            $data['final']=  array_merge($data['prd_id_name1'],$data['prd_id_name2']);
////            $final[]=$data['prd_id_name2'];            
//            print_r($final);
            $this->load->view('home_struture', $data);
//            echo '</pre>';
        }        
}
