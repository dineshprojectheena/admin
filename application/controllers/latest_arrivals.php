<?php

class Latest_arrivals extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('home_struture_data');
        $this->load->library('form_validation');
    }

    function index() {
        $data['content'] = 'Featured Latest Arrivals';
        $id = '';
        $type = 'latest_arrivals';
//        $data = $this->home_struture_data->load_featured($id, $type);
//        $ress = array();
//        foreach ($data as $res) {
//            $data['status']=array('status'=>$res['status']);
//            $data1['prd_id_name1'] = $this->home_struture_data->get_product_detail($res['prd_id_1']);
//            $data1['featured_id'] = array('featured_id' => $res['featured_id']);
//            $final = array_merge($data1['featured_id'], $data1['prd_id_name1'],$data['status']);
//            $ress[] = $final;
//        }
//        $data['load_res'] = $ress;
        $this->load->view('home_latest_arrivals', $data);
    }
    
    function latest_arrivals_result(){
        $this->load->library("datatables");
        $this->load->helper("datatables");
//        $a='car_comparison';
        $actionLinkBar=$this->load->view("content/report/datatable/latest_arrivals_action",array(), TRUE);
        $this->datatables
                ->select("featured_id,(select pro_name from variant where variant_id=featured.prd_id_1)  as pro_name,status")                       
                ->from("featured")
//                ->where("featured_type='car_comparison'");
//              ->where("featured_id=car_comparison")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action",$actionLinkBar,'featured_id');
        echo $this->datatables->generate();
    }
    
    

    function product_load() {

        $id = $this->input->post('id');
        $searchid = $this->input->post('searchid');
        $searchid2 = $this->input->post('searchid2');
        $query=$this->home_struture_data->search_product($searchid,$searchid2);
        if (count($query) > 0) {
            foreach ($query as $res) {
                $name = $res->name;
                $product_id = $res->product_id;
                ?>
                <div class="show" align="left" onclick="setdata('<?php echo $name; ?>', '<?php echo $id; ?>', '<?php echo $product_id; ?>');">
                    <span class="name"><?php echo $name; ?></span>
                </div>
                <?php
            }
        }
    }

    function latest_arrivals_upload() {
        
        if (isset($_POST['latest_arrivals_submit'])) {
            $this->form_validation->set_rules('searchid', 'first product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            if(!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->index();
            } else {
                $type = 'latest_arrivals';
                $data = array(
                    'prd_id_1' => $this->input->post('pro_1'),
                    'status' => $this->input->post('status'),
                    'featured_type' => $type
                );
                $query = $this->home_struture_data->upload_latest_arrivals($data,$type);
                redirect('home_latest_arrivals');
            }
        } else if (isset($_POST['latest_arrivals_update'])) {
            $this->form_validation->set_rules('searchid', 'first product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->get_latest_arrivals();
            } else {
                $status=$this->input->post('status');
                $type = 'latest_arrivals';
                $pro_1=$this->input->post('pro_1');
                $featured_id=$this->input->post('featured_id');
                $data = array(
                    'prd_id_1'=>$pro_1,
                    'status' =>$status,
                    'featured_type'=> $type
                );                
                $query= $this->home_struture_data->update_latest_arrivals($data,$featured_id,$type,$status);
                redirect('home_latest_arrivals');
            }
        } else if (isset($_POST['delete'])) {
            $featured_id = $this->input->post('featured_id');
            $query = $this->home_struture_data->del_latest_arrivals($featured_id);
            redirect('home_latest_arrivals');
        } else {
            redirect('home_latest_arrivals');
        }
    }

    function get_latest_arrivals() {
        $featured_id = $this->input->post('featured_id');

        $type = 'latest_arrivals';
        $data = $this->home_struture_data->load_featured($featured_id, $type);
        foreach ($data as $res)
            $data['featured_id'] = array('featured_id' => $featured_id,'status' => $res['status']);
        $pro1 = $res['prd_id_1'];
        $data['prd_id_name1'] = $this->home_struture_data->get_product_detail($pro1);
        $this->load->view('home_latest_arrivals', $data);
    }

}
