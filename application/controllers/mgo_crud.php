<?php

class Mgo_crud extends CI_Controller {

    function __construct() {
        parent::__construct();
//        $this->load->model('mgo_crud_data');
        $this->load->model('author_model', 'author');
        $this->load->model('post_model', 'post');
    }

    function crud() {
        $id=  $_GET['id'];
        $del_id=  $_GET['del_id'];
        if(!empty($id))
        {
        $data['single'] = $this->author->single($id);    
//        print_r($data['authors']);
        }
        else if(!empty($del_id))
        {
        $data['single'] = $this->author->delete($del_id);    
        redirect('crud');
//        print_r($data['authors']);
        }
        else
        {    
        $data['user'] = $this->author->all();
        }
//        print_r($data);
        $data['title'] = 'Bulk SEO';
        $this->load->view('crud_mongo', $data);
    }

    function mgo_upload() {
        if(isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('dob', 'Date of birth', 'required');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->session->set_flashdata('error_message', validation_errors());
                $this->crud();
            } else {
                $this->author->save();
                $this->session->set_flashdata('message', 'User Details Uploaded Successfully');
                redirect('crud');
            }
        }
        else if(isset($_POST['update'])) {
             $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('dob', 'Date of birth', 'required');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->session->set_flashdata('error_message', validation_errors());
                $this->crud();
            } else {
                $this->author->update();
                
                $this->session->set_flashdata('message', 'User Details Updated Successfully');
                redirect('crud');
            }
        }
    }

}
