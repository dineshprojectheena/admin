<?php

class Test_drive extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("common");
        $this->load->model("mncprice");
        $this->load->model("product_data");
        $this->load->model("experts_rating");
//        $this->load->model("common","price");
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }
    
    function test_drive_data(){
//        $data['test_drive'] = $this->dashboard->dashboard_test_drive('1');
        $data['content'] = 'test_drive';
        $this->load->view('index', $data);
    }
    
    function all_test_drive(){
        $this->load->library("datatables");
        $actionLinkBar=$this->load->view("content/report/datatable/test_drive",array(), TRUE);
        $this->datatables
                ->select("test_drive_enquiry_id,brand_name,model_name,full_name,email,location,phone,"
                        . "(select fuel_type from fuel_type where fuel_type.fuel_type_id=test_drive_enquiry.fuel) as fuel_type,"
                        . "added_date")
                ->from("test_drive_enquiry")
                ->add_column("Action",$actionLinkBar,'test_drive_enquiry_id');

        echo $this->datatables->generate();
    }
    
    function test_drive_assign(){
//        $data['report'] = $this->report->dashborad_reports();
        $data['content'] = 'test_drive_assign';
        $this->load->view('index', $data);
    }
    

}
