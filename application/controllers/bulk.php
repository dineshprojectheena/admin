<?php

class Bulk extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function bulkaccessory() {
        $data['title'] = 'Bulk Accessory';
        $this->load->view('bulkaccessory', $data);
    }

    function bulkaccessory_hotdeal() {
        $data['title'] = 'Bulk Accessory';
        $this->load->view('bulkaccessory_hotdeal', $data);
    }

    function bulk_hotdeal_accessory_upload() {


        if (isset($_POST['submit'])) {

            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            if (!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }
            $i = 1;
            $j = 1;
            $res_var = array();
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                $i++;
                if ($i != '1') {

                    $acc_part_no = $data[0];
                    $acc_name = $data[1];
                    $acc_percent = $data[2];
                    $acc_valid_date = $data[3];
                    $date = date("Y-m-d");
                    $acc_res = $this->db->query("select car_accessory_id from car_accessory where name='$acc_name' and accessory_part_no='$acc_part_no'")->row_array();
                    if (count($acc_res) != 0) {
                        $car_accessory_id = $acc_res['car_accessory_id'];
                        $acc_hot_res = $this->db->query("select acc_hot_deal_id from accessory_hot_deals where accessory_id='$car_accessory_id'")->row_array();
                        if (count($acc_hot_res) == 0) {
                            $acc_hot_deal = $this->db->query("INSERT INTO `accessory_hot_deals`(`acc_hot_deal_id`, `accessory_id`, `percent`, `valid_until`, `added_date`) VALUES "
                                    . "('','$car_accessory_id','$acc_percent','$acc_valid_date','$date')");
                        } else {
                            $acc_hot_deal_id = $acc_hot_res['acc_hot_deal_id'];
                            $acc_hot_deal = $this->db->query("UPDATE `accessory_hot_deals` SET "
                                    . "`percent`='$acc_percent',`valid_until`='$acc_valid_date',`added_date`='$date' WHERE acc_hot_deal_id='$acc_hot_deal_id'");
                        }
                    }
                }
//                $i++;
            }
        }
        ?>
        <script>
            alert('Data Uploaded Successfully');
        </script>       

        <?php

        redirect(base_url('bulkaccessory_hotdeal'));
    }

    function bulkaccessoryupload() {
        if (isset($_POST['submit'])) {
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            if (!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }
            $i = 1;
            $j = 1;
            $k = 1;
            $res_var = array();
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                if ($i == 1) {
                    foreach ($data as $data_res) {
                        if ($j > 16) {
                            $res_var[] = $data_res;
                        }
                        $j++;
                    }
//                    print_r($res_var);
                } else {
                    $brand = $data[0];
                    $model = $data[1];
                    $acc_id = $data[2];
                    $acc_type = $data[3];
                    $acc_cat = $data[4];
                    $acc_sub_cat = $data[5];
                    $acc_part_no = $data[6];
                    $package_name = $data[7];
                    $acc_name = $data[8];
                    $acc_info = addslashes($data[9]);
                    $acc_price = $data[10];
                    $acc_discount = $data[11];
                    if (!empty($acc_discount)) {
                        $acc_discount = $acc_price - $acc_discount;
                    }

                    $acc_img = $data[12];
                    $type = $data[13];
//                    if($type=='AFTERMARKET')
//                    {
//                    $type=1;    
//                    }    
//                    else
//                    {
//                    $type=2;    
//                    }    
                    $acc_status = strtoupper($data[14]);
                    if ($acc_status == 'ENABLE') {
                        $status = '1';
                    } else {
                        $status = '2';
                    }
//                    if($acc_status=='Enable')
                    $acc_added_date = $data[15];
                    if (!empty($brand)) {
                        $brand_res = $this->db->query("select brand_id from brand where brand_name='$brand'")->row_array();
                        $brand_id = $brand_res['brand_id'];
                    }
                    if (!empty($model)) {
                        $model_res = $this->db->query("select model_id from model where model_name='$model' and brand_id='$brand_id'")->row_array();
                        $model_id = $model_res['model_id'];
                    }
                    if (!empty($acc_type)) {
                        $acc_type_res = $this->db->query("select * from accessory_type where accessory_type='$acc_type' and status='1'")->row_array();
                        $accessory_type_id = $acc_type_res['accessory_type_id'];
                    }

                    if (!empty($acc_cat)) {

//                        $acc_cate_res= "select * from accessory_category where accessory_category_name='$acc_cat' and status='1'";
                        $acc_cate_res = $this->db->query("select * from accessory_category where accessory_category_name like '%$acc_cat%' and status='1'")->row_array();
                        $accessory_category_id = $acc_cate_res['accessory_category_id'];
                    }


                    if (!empty($acc_sub_cat)) {
//                        echo $accessory_sub_category_res ="select * from accessory_sub_category where accessory_sub_category_name='$acc_sub_cat' and category_id='$accessory_category_id' and status='1'";
                        $accessory_sub_category_res = $this->db->query("select * from accessory_sub_category where accessory_sub_category_name='$acc_sub_cat'  and category_id='$accessory_category_id' and status='1'")->row_array();
                        echo $accessory_sub_category_id = $accessory_sub_category_res['accessory_sub_category_id'];
                    }
//                    if($acc_sub_cat=='Others' && $acc_cat=='Safety')
//                    {
//                    exit;    
//                    }
//                    exit;
//                    $acc_status="select * from car_accessory where name='$acc_name' and accessory_part_no='$acc_part_no'";
                    $acc_status = $this->db->query("select * from car_accessory where accessory_part_no='$acc_part_no'");

                    $acc_res = $acc_status->result_array();
                    $acc_count = $acc_status->num_rows();

                    if ($acc_count == 0) {
                        $acc_status = $this->db->query("INSERT INTO `car_accessory`(`car_accessory_id`, "
                                . "`category`, `image`, `name`,`discount_price`, `price`, `description`, `stock`, `status`,"
                                . " `all_product_name`, `brand_id`, `accessory_id`, `accessory_type`, "
                                . "`accessory_cat`, `accessory_sub_cat`, `accessory_part_no`, `package_name`, "
                                . "`discount`, `model_id`,`type`, `added_date`) "
                                . "VALUES ('','$accessory_category_id','$acc_img','$acc_name','$acc_discount','$acc_price'"
                                . ",'$acc_info','','$status','$status','$brand_id','$accessory_category_id',"
                                . "'$accessory_type_id','$accessory_category_id'"
                                . ",'$accessory_sub_category_id','$acc_part_no','$package_name','$acc_discount'"
                                . ",'$model_id','$type',NOW())");
                        $car_accessory_id = $this->db->insert_id();
                    } else {
                        $acc_status;
                        $car_accessory_id = $acc_res[0]['car_accessory_id'];
                        $acc_update = "UPDATE `car_accessory` SET "
                                . "`category`='$accessory_category_id',`image`='$acc_img',`name`='$acc_name',`discount_price`='$acc_discount',`price`='$acc_price',"
                                . "`description`='$acc_info',`stock`='',`status`='$status',`all_product_name`='',"
                                . "`brand_id`='$brand_id',`accessory_id`='$accessory_category_id',`accessory_type`='$accessory_type_id',`accessory_cat`='$accessory_category_id',"
                                . "`accessory_sub_cat`='$accessory_sub_category_id',`accessory_part_no`='$acc_part_no',`package_name`='$package_name',"
                                . "`discount`='$acc_discount',`model_id`='$model_id',`type`='$type',`added_date`=NOW() WHERE `car_accessory_id`='$car_accessory_id'";
                        $acc_update = $this->db->query($acc_update);
                    }

                    foreach ($data as $data_res) {
//                        print_r($data_res);
                        if ($k > 15) {
                            $res_data[] = $data_res;
                        }
                        $k++;
                    }
//                    echo $acc_part_no;

                    unset($k);
//                    echo '<pre>';
//                    print_r($data);
//                    print_r($res_var);

                    $variant_names = array();
                    $cnt = 0;
                    foreach ($res_var as $res_var_id) {
                        $res_var_id . '-';
                        $status_variant = strtoupper($res_data[$cnt]);
//                        echo '<br>';
                        if ($status_variant == 'Y') {
                            $variant_name[] = $res_var_id;
//                            $variant_name= "'" . $res_var_id . "'," . $variant_name;
                        }
                        $cnt++;
                    }
                    $variant_names = "";
                    foreach ($variant_name as $variant_name_data) {
                        $variant_names = "'" . $variant_name_data . "'," . $variant_names;
                    }
//                    print_r($variant_names);
                    $variant_names = rtrim($variant_names, ',');


                    if (!empty($variant_names)) {
                        $all_variant_depends = $this->db->query("select `variant_id` from `variant` where `pro_name` IN ($variant_names) and `brand_id`='$brand_id' and `model_id`='$model_id'")->result_array();
//                    $del_car_acc_depend = "delete from accessory_variant_dependency where accessory_id='$car_accessory_id' and brand_id='$brand_id' and model_id='$model_id'";
                        $del_car_acc_depend = $this->db->query("delete from accessory_variant_dependency where accessory_id='$car_accessory_id' and brand_id='$brand_id' and model_id='$model_id'");
                        foreach ($all_variant_depends as $all_variant_depend_data) {
                            $variant_id = $all_variant_depend_data['variant_id'];

                            $variant_dependency_insert = $this->db->query("INSERT INTO `accessory_variant_dependency`(`id`, `accessory_id`, `variant_id`, `brand_id`, `model_id`) "
                                    . "VALUES ('','$car_accessory_id','$variant_id','$brand_id','$model_id')");

                            $variant_dependency_insert = "INSERT INTO `accessory_variant_dependency`(`id`, `accessory_id`, `variant_id`, `brand_id`, `model_id`) "
                                    . "VALUES ('','$car_accessory_id','$variant_id','$brand_id','$model_id')";
                        }
                    }

                    unset($res_data);
                    unset($variant_name);
//                    echo '</pre>';
//                    if($car_accessory_id==11)
//                    {
//                        
//                    exit;    
//                    }    
//                    echo $del_car_acc_depend = "delete from accessory_variant_dependency where accessory_id='$car_accessory_id'";
//                    $del_car_acc_depend = $this->db->query("delete from accessory_variant_dependency where accessory_id='$car_accessory_id'");
//                    
//                    $car_accessory_id;
//                    $d = 0;
//                    $e = 0;
//                    foreach ($data as $data_variant_res) {
//                        if ($d > 14) {
//                            $head_pro = $res_var[$e];
//                            $variant_res = $this->db->query("select variant_id from variant where brand_id='$brand_id' and model_id='$model_id' and pro_name='$head_pro'")->row_array();
//                            $variant_id = $variant_res['variant_id'];
//                            if (strtoupper($data_variant_res) == 'Y') {
//                                $variant_dependency_insert = $this->db->query("INSERT INTO `accessory_variant_dependency`(`id`, `accessory_id`, `variant_id`, `brand_id`, `model_id`) "
//                                        . "VALUES ('','$car_accessory_id','$variant_id','$brand_id','$model_id')");
//                            }
//                            $e++;
//                        }
//                        $d++;
//                    }
                }
                echo '<br>';

                $i++;
                unset($j);
            }
//            exit;
            ?>
            <script>
                alert("Uploaded Successfully");
                history.go(-1);
            </script>        

            <?php

//        redirect(base_url() . 'bulkaccessory');
        }
    }

    function bulkseo() {
        $data['title'] = 'Bulk SEO';
        $this->load->view('bulkseo', $data);
    }

    function bulk_seo_data() {
        if (isset($_POST['submit'])) {
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            $del_record = $_POST['del_record'];
            if ($del_record == 1) {
                $truncate = $this->db->query("truncate seo_tag");
            }
            if(!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }
            $i = 0;
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                if ($i > 0) {
                    $id = trim($data[0]);
                    $page = addslashes(trim($data[1]));
                    $keywords = addslashes(trim($data[2]));
                    $meta_description = addslashes(trim($data[3]));
                    $meta_title = addslashes(trim($data[4]));
                    $meta_tags = addslashes(trim($data[5]));
                    $h2 = addslashes(trim($data[6]));
                    $h3 = addslashes(trim($data[7]));
                    $h4 = addslashes(trim($data[8]));
                    
                    
                    $check = $this->db->query("select `seo_tag_id` from `seo_tag` where `page`='$page'")->row_array();
                    if (count($check) == 0) {
                        $insert = $this->db->query("INSERT INTO `seo_tag`(`seo_tag_id`, `page`, `keywords`, `meta_description`, `title`, `meta_tags`,`h2`,`h3`,`h4`) "
                                . "VALUES ('','$page','$keywords','$meta_description','$meta_title','$meta_tags','$h2','$h3','$h4')");
                    } else {
                        $page = $check['page'];
                        $seo_tag_id = $check['seo_tag_id'];
                        $upd = $this->db->query("UPDATE `seo_tag` SET `keywords`='$keywords',"
                                . "`meta_description`='$meta_description',`title`='$meta_title',"
                                . "`meta_tags`='$meta_tags',`h2`='$h2',`h3`='$h3',`h4`='$h4' WHERE `page`='$page' and `seo_tag_id`='$seo_tag_id'");
                    }
                }
                $i++;
            }
            ?>
            <script>
                alert('Uploaded Successfully');
                history.go(-1);
            </script>             
            <?php
        }
    }

    function bulkrating() {
        $data['title'] = 'Product Gallery';
        $this->load->view('bulk_category', $data);
    }

    function bulk_upload_data() {
        if (isset($_POST['submit'])) {
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            $del_record = $_POST['del_record'];
            if ($del_record == 1) {
                $truncate = $this->db->query("truncate expert_rating");
            }

            if (!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }



            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                $added_date = date('Y-m-d h:i:s');
                if ($i > 2) {
                    $brand_name = trim($data[0]);
                    $model_name = trim($data[1]);
                    $variant_name = trim($data[2]);
                    $fuel = trim($data[3]);
                    $rating = trim($data[4]);
                    $profile = trim($data[5]);
                    $source = trim($data[6]);
                    $jornalist_name = trim($data[7]);
                    $heading = trim($data[8]);
                    $what_i_like = trim($data[9]);
                    $what_i_dont_like = trim($data[10]);
                    $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->result_array();
                    $brand_id = $brand[0]['brand_id'];
                    $model = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->result_array();
                    $model_id = $model[0]['model_id'];
                    $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->result_array();
                    $fuel_type_id = $fuel_data[0]['fuel_type_id'];
                    $added_date = date('Y-m-d h:i:s');

                    if ($brand_id != '' && $model_id != "") {
                        $variant = $this->db->query("SELECT `variant_id` FROM `variant` WHERE `model_id`='$model_id'"
                                        . " and `brand_id`='$brand_id' and pro_name='$variant_name' and fuel_type='$fuel_type_id'")->result_array();
                        $ids = '';
                        foreach ($variant as $variant_type_data) {
                            $variant_id = $variant_type_data['variant_id'];
                            $succ = $i;
                            $expert_add = $this->db->query("INSERT INTO `expert_rating`(`expert_rating_id`,`pro_id`,`source`,`profile_id`, "
                                    . "`user_id`, `journilist_name`, `heading`, `what_i_like`, `what_i_dont_like`, `total`, `added_date`, `date`) "
                                    . "VALUES ('','$variant_id','$source','$profile','','$jornalist_name','$heading','$what_i_like','$what_i_dont_like','$rating','$added_date','$added_date')");

                            $insert_id = $this->db->insert_id();
                        }
                        $sucess = $succ . ',' . $sucess;
                    } else {
                        $error = $i . ',' . $error;
                    }
                }
                $i++;
            }
            $error_exp = explode(',', $error);
            $error_exp = array_unique($error_exp);
            $error = implode(",", $error_exp);

            $sucess_exp = explode(',', $sucess);
            $sucess_exp = array_unique($sucess_exp);
            $sucess = implode(",", $sucess_exp);

//            $sucess_exp=explode(',',$sucess);

            $this->session->set_flashdata('Error', "Following Rows of data can't inserted: " . $error);
            $this->session->set_flashdata('Success', "Following Rows of data inserted successfully: " . $sucess);
//            exit;
            fclose($handle);
            ?>
            <script>
                alert('Rating Uploaded Successfully');
            </script>
            <?php

        }
        redirect(base_url() . 'bulkrating');
    }

    function bulkprice() {
        $data['title'] = 'Bulk Price';
        $this->load->view('bulkprice', $data);
    }

    function bulk_upload_price_data() {
        if (isset($_POST['submit'])) {
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            // 
            if (!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }



            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            $chck = 0;
            $msg = 'Results STatus:-<br>';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                if ($i > 0) {
                    $brand_name = trim($data[0]);
                    $model_name = trim($data[1]);
                    $variant_name = trim($data[2]);
                    $fuel = trim($data[3]);
                    $exshowroom_price = trim($data[4]);
                    $discount_type = 1;

                    $discount_amount = trim($data[5]);

                    $mnc_price = trim($data[6]);
                    $insuranc_price = trim($data[7]);

                    $insuranc_dis_price = trim($data[8]);

                    $lbt_tax = trim($data[9]);
                    $rto = trim($data[10]);
                    $pro_name_comp = $brand_name . ' ' . $model_name . ' ' . $variant_name;
                    $handling_cost = trim($data[11]);
                    $onroad_price = trim($data[12]);
                    $grp_discount = trim($data[13]);
//                  $special_rewards = trim($data[14]);
                    $car_exchange_price = trim($data[14]);
//                  $loyalty_bonus = trim($data[16]);
                    $amc = trim($data[15]);
//                  $extened_warranty_cost = trim($data[18]);
                    $delivery_time = trim($data[16]);
                    $sts = 1;
                    $city = trim($data[18]);

//                  $cash_discount=trim($data[22]);                    
                    $finance_benefit = trim($data[19]);
                    $exchange_benefit = trim($data[20]);

                    $loyality_bonus = trim($data[21]);

                    $corporate_discount = trim($data[22]);

                    $waivier_handling_charges = trim($data[23]);
                    $accessory_package = trim($data[24]);

                    $accessory_discount = trim($data[25]);

                    $insurance_benefits = trim($data[26]);
                    $extended_warranty = trim($data[27]);
                    $liquidation_support = trim($data[28]);
                    $additional_liquidation_support = trim($data[29]);
                    $zero_depreciation = trim($data[30]);
                    $special_reward = trim($data[31]);
                    $acc_benefits = trim($data[32]);
                    $total_benefits_per = trim($data[33]);
                    $total_savings = trim($data[34]);
                    $mnc_on_road_price = trim($data[35]);
                    $road_side_assistant = trim($data[36]);
                    $standard_accessory_kit = trim($data[37]);
                    $extended_warranty_beneits = trim($data[38]);
                    $remarks = trim($data[39]);
                    $add_upd = trim($data[40]);
                    
                    $total_benefits = $finance_benefit + $exchange_benefit + $loyality_bonus + $corporate_discount + $waivier_handling_charges + $accessory_package + $accessory_discount + $insurance_benefits + $extended_warranty + $liquidation_support + $additional_liquidation_support;
                    $new_mnc_price = $exshowroom_price - $discount_amount;

                    $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->row_array();

                    $brand_id = $brand['brand_id'];

                    $model = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->row_array();
                    $model_id = $model['model_id'];

                    $pro_name_comp = $brand_name . ' ' . $model_name . ' ' . $variant_name;
                    
                    $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->row_array();
                    $fuel_type_id = $fuel_data['fuel_type_id'];

                    $que = "select variant_id from variant where fuel_type='$fuel_type_id' and pro_name_comp='$pro_name_comp' and status='1'";
                    $variant_res = $this->db->query($que)->row_array();
                    $variant_id = $variant_res['variant_id'];
                    $added_date = date('Y-m-d h:i:s');
                    if ($add_upd == 'a') {

                        if (!empty($variant_id)) {
                            $check_city = $this->db->query("SELECT `city_id` FROM `city` WHERE `name`='$city'")->row_array();
                            $city_id = $check_city['city_id'];
                            if ($fuel == 'LPG') {
                                
                            }
                            $check_mnc_price_upd = $this->db->query("SELECT `city_id` FROM `mnc_exshowroom_price` WHERE `product_id`='$variant_id' and city_id='$city_id'")->num_rows();
                            if ($check_mnc_price_upd == 0 && !is_null($variant_id)) {
                                $temp_array = array(
                                    'product_id' => $variant_id,
                                    'city_id' => $city_id,
                                    'mnc_exshowroom_price' => $mnc_price,
                                    'discount_rs' => $discount_amount,
                                    'status' => $sts,
                                    'row_id' => $i,
                                    'road_side_assistant' => $road_side_assistant,
                                    'standard_accessory_kit' => $standard_accessory_kit,
                                    'cash_discount' => $discount_amount,
                                    'finance_benefit' => $finance_benefit,
                                    'exchange_benefit' => $exchange_benefit,
                                    'exchange_bonus' => $car_exchange_price,
                                    'loyality_bonus' => $loyality_bonus,
                                    'corporate_discount' => $corporate_discount,
                                    'waivier_handling_charges' => $waivier_handling_charges,
                                    'accessory_package' => $accessory_package,
                                    'accessory_discount' => $accessory_discount,
                                    'insurance_benefits' => $insurance_benefits,
                                    'extended_warranty' => $extended_warranty,
                                    'extended_warranty_benefits' => $extended_warranty_beneits,
                                    'liquidation_support' => $liquidation_support,
                                    'additional_liquidation_support' => $additional_liquidation_support,
                                    'total_benefits' => $total_benefits,
                                    'total_benefits_perc' => $total_benefits_per,
                                    'zero_depreciation' => $zero_depreciation,
                                    'special_reward' => $special_reward,
                                    'acc_benefits' => $acc_benefits,
                                    'total_savings' => $total_savings,
                                    'remarks' => $remarks,
                                    'insurance' => $insuranc_price,
                                    'insurance_discount' => $insuranc_dis_price,
                                    'row_id' => $i,
                                    'added_date' => date('Y-m-d h:i:s'),
                                    'delivery_date' => $delivery_time,
                                );

                                $this->db->insert('mnc_exshowroom_price', $temp_array);
                            }
                            $msg.='<div style="color:green;">Insert: Success Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';

                            $check_exprice_upd = $this->db->query("SELECT `city_id` FROM `exshowroom_price` WHERE `product_id`='$variant_id' and city_id='$city_id'")->num_rows();
                            if ($check_exprice_upd == 0 && !is_null($variant_id)) {
                                $temp_array2 = array(
                                    'product_id' => $variant_id,
                                    'city_id' => $city_id,
                                    'exshowroom_price' => $exshowroom_price,
                                    'status' => $sts,
                                    'last_update' => date('Y-m-d h:i:s'),
                                    'added_date' => date('Y-m-d h:i:s'),
                                    'delivery_date' => $delivery_time,
                                );

                                $this->db->insert('exshowroom_price', $temp_array2);
                            }

                            $check_onroad_upd = $this->db->query("SELECT `city_id` FROM `onroad_price` WHERE `variant_id`='$variant_id' and city_id='$city_id'")->num_rows();
                            if ($check_onroad_upd == 0 && !is_null($variant_id)) {
                                $temp_array3 = array(
                                    'variant_id' => $variant_id,
                                    'city_id' => $city_id,
                                    'comp_insurance' => $insuranc_price,
                                    'lbt' => $lbt_tax,
                                    'registeration_charges' => $rto,
                                    'road_tax' => $rto,
                                    'handling_price' => $handling_cost,
                                    'onroad_price' => $onroad_price,
                                    'mnc_on_road_price' => $mnc_on_road_price,
                                    'added_date' => date('Y-m-d h:i:s')
                                );

                                $this->db->insert('onroad_price', $temp_array3);
                            }
                            $chck++;
                        } else {
                            $msg2.='<div style="color:red;">Insert: Error Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';
                        }
                    } else if ($add_upd == 'u') {


                        if (!empty($variant_id)) {
                            $check_city = $this->db->query("SELECT `city_id` FROM `city` WHERE `name`='$city'")->row_array();
                            $city_id = $check_city['city_id'];

                            $check_mnc_price_upd = $this->db->query("SELECT `mnc_exshowroom_price_id` FROM `mnc_exshowroom_price` WHERE `product_id`='$variant_id' and city_id='$city_id'")->row_array();
                            $mnc_exshowroom_price_id = $check_mnc_price_upd['mnc_exshowroom_price_id'];
                            if ($mnc_exshowroom_price_id != '') {
                                $upate_mnc_exshowroom_price = $this->db->query("UPDATE `mnc_exshowroom_price` SET
                                        `mnc_exshowroom_price` = '$mnc_price',
                                        `discount_rs` = '$discount_amount',
                                        `status` = '$sts',
                                        `row_id` = '$i',
                                         `road_side_assistant` = '$road_side_assistant',
                                         `standard_accessory_kit` = '$standard_accessory_kit',
                                        `cash_discount` = '$discount_amount',
                                        `finance_benefit` = '$finance_benefit',
                                        `exchange_benefit` = '$exchange_benefit',
                                        `exchange_bonus` = '$car_exchange_price',
                                        `loyality_bonus` = '$loyality_bonus',
                                        `corporate_discount` = '$corporate_discount',
                                        `waivier_handling_charges` = '$waivier_handling_charges',
                                        `accessory_package` = '$accessory_package',
                                        `accessory_discount` = '$accessory_discount',
                                        `insurance_benefits` = '$insurance_benefits',
                                        `extended_warranty` = '$extended_warranty',
                                        `extended_warranty_benefits` = '$extended_warranty_beneits',
                                        `liquidation_support` = '$liquidation_support',
                                        `additional_liquidation_support` = '$additional_liquidation_support',
                                        `total_benefits` = '$total_benefits',
                                        `total_benefits_perc` = '$total_benefits_per',
                                        `zero_depreciation` = '$zero_depreciation',
                                        `special_reward` = '$special_reward',
                                        `acc_benefits` = '$acc_benefits',
                                        `total_savings` = '$total_savings',
                                        `remarks` = '$remarks',
                                        `insurance` = '$insuranc_price',
                                        `insurance_discount` = '$insuranc_dis_price',
                                        `row_id` = '$i',
                                        `delivery_date` = '$delivery_time' where `mnc_exshowroom_price_id`='$mnc_exshowroom_price_id'");
                                $msg.='<div style="color:green;">Update: Success Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';
                            } else {
                                $msg2.='<div style="color:red;">Update: Error Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';
                            }

//                        $sucess_record[]=array('Success'=>$msg);
                            $check_exshowroom_price = $this->db->query("SELECT `exshowroom_price_id` FROM `exshowroom_price` WHERE `product_id`='$variant_id' and city_id='$city_id'")->row_array();
                            $exshowroom_price_id = $check_exshowroom_price['exshowroom_price_id'];
                            if ($exshowroom_price_id != '') {
                                $upate_exshowroom_price = $this->db->query("UPDATE `exshowroom_price` SET `exshowroom_price` = '$exshowroom_price',"
                                        . "`status` = '$sts',`delivery_date` = '$delivery_time',`delivery_date` = '$delivery_time' "
                                        . "where `exshowroom_price_id`='$exshowroom_price_id'");
                            }

//                        echo $mnc_on_road_price.'---'.$variant_id.'<br>';


                            $check_onroad_price = $this->db->query("SELECT `onroad_price_id` FROM `onroad_price` WHERE `variant_id`='$variant_id' and city_id='$city_id'")->row_array();
                            $onroad_price_id = $check_onroad_price['onroad_price_id'];
                            if ($onroad_price_id != '') {
                                $upate_onroad_price = $this->db->query("UPDATE `onroad_price` SET
                                        `city_id` = '$city_id',
                                        `comp_insurance` = '$insuranc_price',
                                        `lbt` = '$lbt_tax',
                                        `registeration_charges` = '$rto',
                                        `road_tax` = '$rto',
                                        `handling_price` = '$handling_cost',
                                        `onroad_price` = '$onroad_price',
                                        `mnc_on_road_price` = '$mnc_on_road_price'
                                        where `onroad_price_id`='$onroad_price_id'");
                            }
                        } else {

                            $msg2.='<div style="color:red;">Update: Error Record:- Sr No. ' . $i . '........Product Name ' . $brand_name . ' ' . $model_name . ' ' . $variant_name . '</div><br>';
//                        $error_record[]=array('Error'=>$msg2);
                        }
                    }
                }
                $i++;
            }
//            echo $msg; 
            echo '<pre>';
            print_r($msg);
            echo '</pre>';

            echo '<pre>';
            print_r($msg2);
            echo '</pre>';

            exit;
            $error_exp = explode(',', $error);
            $tot_error = count($error_exp);
            $error_exp = array_unique($error_exp);
            $error = implode(",", $error_exp);

            $sucess_exp = explode(',', $sucess);
            $tot_sucess = count($sucess_exp);
            $sucess_exp = array_unique($sucess_exp);
            $sucess = implode(",", $sucess_exp);

//            echo $msg;

            $this->session->set_flashdata('Error', "Following Rows of data can't inserted:<br> " . $msg);
            fclose($handle);
        }
        redirect(base_url() . 'bulkprice');
    }

    function bulk_mosf() {
        $data['title'] = 'Bulk MOSF';
        $this->load->view('bulkmosf', $data);
    }

    function bulkmosfupload() {
        if (isset($_POST['submit'])) {
            $del_record = $_POST['del_record'];
            if ($del_record == 1) {
                $truncate = $this->db->query("truncate mosf_deals");
            }

            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");

            if (!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }


            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                $brand_name = trim($data[0]);
                $model_name = trim($data[1]);
                $variant_name = trim($data[2]);
                $fuel = trim($data[3]);
                $city = trim($data[4]);
                $sort = trim($data[5]);
                $percent = trim($data[6]);
                $valid_until = trim($data[7]);
                $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->row_array();
                $brand_id = $brand['brand_id'];
                $models = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->row_array();
                $model_id = $models['model_id'];
                $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->row_array();
                $fuel_type_id = $fuel_data['fuel_type_id'];
                $check_city = $this->db->query("SELECT `city_id` FROM `city` WHERE `name`='$city'")->row_array();
                $city_id = $check_city['city_id'];
                if ((!empty($brand_id) && !empty($model_id) && !empty($variant_name))) {
                    $variant_name = $variant_name;
                    // echo $variant_name = htmlspecialchars(str_replace('+', '-', $variant_name));
                    $variant = $this->db->query("SELECT `variant_id` FROM `variant` WHERE `fuel_type`='$fuel_type_id' and `pro_name`='$variant_name' and `model_id`='$model_id'"
                                    . " and `brand_id`='$brand_id'")->row_array();
                    $variant_id = $variant['variant_id'];
//                    echo '<br>';
                    $insert_hot_deals = $this->db->query("INSERT INTO `mosf_deals`(`hot_deal_id`, `variant_id`, `city_id`, `added_date`,`sort`,`percent`,`valid_until`) "
                            . "VALUES ('','$variant_id','$city_id',NOW(),'$sort','$percent','$valid_until')");
                }
            }
        }
        redirect(base_url() . 'mosf');
    }

    function bulkhotdeal() {
        $data['title'] = 'Bulk hot Deals';
        $this->load->view('bulkhotdeals', $data);
    }

    function bulkhotdealupload() {

        if (isset($_POST['submit'])) {


            $del_record = $_POST['del_record'];
            if ($del_record == 1) {
                $truncate = $this->db->query("truncate hot_deals");
            }

            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");

            if (!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }


            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                $brand_name = trim($data[0]);
                $model_name = trim($data[1]);
                $variant_name = trim($data[2]);
                $fuel = trim($data[3]);
                $city = trim($data[4]);
                $sort = trim($data[5]);
                $percent = trim($data[6]);
                $valid_until = trim($data[7]);
                $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->row_array();
                $brand_id = $brand['brand_id'];
                $models = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->row_array();
                $model_id = $models['model_id'];
                $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->row_array();
                $fuel_type_id = $fuel_data['fuel_type_id'];
                $check_city = $this->db->query("SELECT `city_id` FROM `city` WHERE `name`='$city'")->row_array();
                $city_id = $check_city['city_id'];
                if ((!empty($brand_id) && !empty($model_id) && !empty($variant_name))) {
                    echo $variant_name = $variant_name;
                    // echo $variant_name = htmlspecialchars(str_replace('+', '-', $variant_name));

                    $variant = $this->db->query("SELECT `variant_id` FROM `variant` WHERE `fuel_type`='$fuel_type_id' and `pro_name`='$variant_name' and `model_id`='$model_id'"
                                    . " and `brand_id`='$brand_id'")->row_array();
                    $variant_id = $variant['variant_id'];
                    $insert_hot_deals = $this->db->query("INSERT INTO `hot_deals`(`hot_deal_id`, `variant_id`, `city_id`, `added_date`,`sort`,`percent`,`valid_until`) "
                            . "VALUES ('','$variant_id','$city_id',NOW(),'$sort','$percent','$valid_until')");
                }
            }
        }
        redirect(base_url() . 'bulkhotdeal');
    }

    function bulkcoupon() {
        $data['title'] = 'Bulk Coupon';
        $this->load->view('bulkcoupon', $data);
    }

    function bulkcouponupload() {
        if (isset($_POST['submit'])) {
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");

            if (!empty($_FILES['pro_images']['name'])) {
                $path_type = pathinfo($_FILES['pro_images']['name'], PATHINFO_EXTENSION);
                if ($path_type != 'csv') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload csv file!');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }

            $i = 0;

            $success.='';
            $updates.='';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                if ($i > 0) {
                    
                    $coupon_id = trim($data[0]);

                    $city = trim($data[2]);
                    if ($city != '') {
                        $type.=$type . '1-';
                    }
                    $product_name = trim($data[1]);
                    if ($product_name != '') {
                        $type.='2-';
                    }


                    $start_date = trim($data[3]);
                    $end_date = trim($data[4]);
                    $fuel = trim($data[5]);
                    $no_of_coupon = trim($data[6]);
                    $coupon_type = trim($data[7]);
                    $coupon_material = addslashes(trim($data[8]));
                    $amount_percent = trim($data[9]);
                    $coupon_used = trim($data[10]);
//                    $type = trim($data[9]);
                    $brand = trim($data[12]);
                    if ($brand != '') {
                        $type.=$type . '3-';
                    }
                    $color = trim($data[13]);
                    if ($color != '') {
                        $type.=$type . '4-';
                    }
                    $coupon_code = trim($data[14]);
                    $desc = addslashes(trim($data[15]));
                    $comm = addslashes(trim($data[16]));
                    $faq = addslashes(trim($data[17]));
                    $added_date = trim($data[18]);
                    $exp_type = explode('-', $type);
                    $type = implode("-", (array_unique($exp_type)));

                    $type = rtrim($type, "-");
//                    exit;
                    
                    $brand_data = $this->db->query("select brand_id from brand where brand_name='$brand'")->row_array();
                    $brand_id = $brand_data['brand_id'];
                    
                    $city_data = $this->db->query("select city_id from city where name='$city'")->row_array();
                    $city_id = $city_data['city_id'];
                    
                    if (is_numeric($product_name)) {
                        $variant_main_id = $product_name;
                    } else {
                        $que = "select variant_id from variant where pro_name_comp='$product_name'";
                        $var_res = $this->db->query($que)->row_array();
                        $variant_main_id = $var_res['variant_id'];
                    }

                    $variant_main_id;
                    
                    $main_color = $color;
//                    $check = $this->db->query("select coupon_id from coupon where coupn_code='$coupon_code'")->result_array();
//                    echo $type;
//                    



//                    if (count($check) == 0) {
                        $insert = $this->db->query("INSERT INTO `coupon` SET `coupon_id` = '',`variant_id` = '$variant_main_id',`city_id` = '$city_id',
                    `start_date` = '$start_date',`end_date` = '$end_date',`fuel` = '$fuel',`no_of_coupon` = '$no_of_coupon',coupon_type='$coupon_type',coupon_material='$coupon_material',`amount_percent` = '$amount_percent',`coupon_used` = '$coupon_used',
                    `type` = '$type',`brand` = '$brand',`color` = '$color',`coupn_code` = '$coupon_code',
                     `description` = '$desc',`comment` = '$comm',`faq` = '$faq',`added_date` = CURRENT_TIMESTAMP");
                        $success.='<div style="color:blue;">Inserted Data: ' . $start_date . ' || ' . $end_date . ' || ' . $coupon_code . '</div>';
//                    } else {
//
//                        $coupon_id = $check[0]['coupon_id'];
//                        $update = $this->db->query("UPDATE `coupon` SET `variant_id` = '$variant_main_id',`city_id` = '$city_id',
//                    `start_date` = '$start_date',`end_date` = '$end_date',`fuel` = '$fuel',`no_of_coupon` = '$no_of_coupon',coupon_type='$coupon_type',coupon_material='$coupon_material',`amount_percent` = '$amount_percent',`coupon_used` = '$coupon_used',
//                    `type` = '$type',`brand` = '$brand',`color` = '$color',`coupn_code` = '$coupon_code',
//                     `description` = '$desc',`comment` = '$comm',`faq` = '$faq',`added_date` = CURRENT_TIMESTAMP WHERE `coupon_id`='$coupon_id'");
//                        $updates.='<div style="color:green;">Updated Data: ' . $start_date . ' || ' . $end_date . ' || ' . $coupon_code . '</div>';
//                    }
                    unset($type);
//                    exit;
//                if($app_type==1)
//                {
//                echo '1';    
//                }    
//                else if($app_type==2)
//                {
//                $brand=$this->db->query("select brand_id from brand where brand_name='$brand'")->result_array();                          
//                echo $brand_id=$brand[0]['brand_id'];
//                
//                exit;
//                }    
//                else if($app_type==3)
//                {
//                echo '3';    
//                }    
//                else if($app_type==4)
//                {
//                echo '4';    
//                }    
                }
                $i++;
            }
        } else {
            $select = "SELECT coupon_id,(select pro_name_comp from variant where variant.variant_id=coupon.variant_id) as product_name,(select name from city where city.city_id=coupon.city_id) as city_name,start_date,end_date,fuel,no_of_coupon,coupon_type,coupon_material,amount_percent,coupon_used,type,(select brand_name from brand where brand.brand_id=coupon.brand) as brand_name,color,coupn_code,description,comment,faq,added_date  FROM coupon";
            $export = mysql_query($select) or die("Sql error : " . mysql_error());

            $fields = mysql_num_fields($export);

            for ($i = 0; $i < $fields; $i++) {
                $header .= mysql_field_name($export, $i) . "\t";
            }

            while ($row = mysql_fetch_row($export)) {
                $line = '';
                foreach ($row as $value) {
                    if ((!isset($value) ) || ( $value == "" )) {
                        $value = "\t";
                    } else {
                        $value = str_replace('"', '""', $value);
                        $value = '"' . $value . '"' . "\t";
                    }
                    $line .= $value;
                }
                $data .= trim($line) . "\n";
            }
            $data = str_replace("\r", "", $data);

            if ($data == "") {
                $data = "\n(0) Records Found!\n";
            }

            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=coupon_list.xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            print "$header\n$data";
//            redirect(base_url() . 'bulkcoupon');
        }
        echo $success;
        echo $updates;
        exit;
    }

    function bulkgroupdeal() {
        $data['title'] = 'Bulk Group Deals';
        $this->load->view('bulkgrouodeals', $data);
    }

    function bulkgroupdealupload() {
        if (isset($_POST['submit'])) {
            $truncate = $this->db->query("truncate deal");
            $truncate = $this->db->query("truncate brand_deal");
            $handle = fopen($_FILES['pro_images']['tmp_name'], "r");
            $i = 0;
            $category = "";
            $heading = "";
            $j = 1;
            $error = '';
            $sucess = '';
            while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                echo '<pre>';
//              print_r($data);                
                $brand_name = trim($data[0]);
                $brand = $this->db->query("select brand_id from brand where brand_name='$brand_name'")->row_array();
                $brand_id = $brand['brand_id'];

                $model_name = trim($data[1]);
                $models = $this->db->query("select model_id from model where model_name='$model_name' and brand_id='$brand_id'")->row_array();
                $model_id = $models['model_id'];


                $variant_name = trim($data[2]);
                $fuel = trim($data[3]);
                $fuel_data = $this->db->query("select fuel_type_id from fuel_type where fuel_type='$fuel'")->row_array();
                $fuel_type_id = $fuel_data['fuel_type_id'];


                $city = trim($data[4]);
                $check_city = $this->db->query("SELECT `city_id` FROM `city` WHERE `name`='$city'")->row_array();
                $city_id = $check_city['city_id'];


                $brand_disc = trim($data[5]);
                $start_date = trim($data[6]);
                $end_date = trim($data[7]);
                $variant_disc = trim($data[8]);
                $disc_heading = trim($data[9]);
                $variant_start_data = trim($data[10]);
                $variant_end_data = trim($data[11]);
                $term_condition = trim($data[12]);
                $date_added = date('Y-m-d');
//                
                if ((!empty($brand_id) && !empty($model_id) && !empty($variant_name))) {
                    $variant_name = htmlspecialchars(str_replace('+', '-', $variant_name));


                    $variant = $this->db->query("SELECT `variant_id` FROM `variant` WHERE `fuel_type`='$fuel_type_id' and `pro_name`='$variant_name' and `model_id`='$model_id'"
                            . " and `brand_id`='$brand_id'");

                    $variant_result = $variant->row_array();
                    $variant_count = $variant->num_rows();
                    $variant_id = $variant_result['variant_id'];


                    $check_variant_deal = $this->db->query("select deal_id from deal where brand_id='$brand_id' and variant_id='$variant_id' and city_id='$city_id'")->num_rows();
                    if ($check_variant_deal == 0) {
                        if (strpos($variant_disc, '%') !== false) {
                            $type = '1';
                        } else {
                            $type = '2';
                        }


                        $v_g_d = $this->db->query("INSERT INTO `deal`"
                                . "(`deal_id`, `variant_id`, `brand_id`, `city_id`, `discount_type`, `deals_heading`, `amount_per`,"
                                . " `start_date`, `end_date`, `status`, `term_condition`, `added_date`) VALUES "
                                . "('','$variant_id','$brand_id','$city_id','$type','$disc_heading','$variant_disc','$variant_start_data',"
                                . "'$variant_end_data','1','$term_condition','$date_added')");
                    }

                    $brand_count = $this->db->query("select brand_id from brand_deal where brand_id='$brand_id'")->num_rows();

                    if ($brand_count == 0) {
                        if (strpos($brand_disc, '%') !== false) {
                            $brand_type = '1';
                        } else {
                            $brand_type = '2';
                        }
                        $brand_group_deal = $this->db->query("INSERT INTO `brand_deal`(`deal_id`, `brand_id`, `city_id`, `discount_type`, `deals_heading`, `amount_per`,"
                                . " `start_date`, `end_date`, `status`, `term_condition`, `added_date`) "
                                . "VALUES ('','$brand_id','$city_id','$brand_type','$disc_heading','$brand_disc','$start_date','$end_date','1','$term_condition','$date_added')");
                    }
                }
            }
            echo '</pre>';
        }

        redirect(base_url() . 'bulkgroupdeal');
    }

}
