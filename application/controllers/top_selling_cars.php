<?php
class Top_selling_cars extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('home_struture_data');
        $this->load->library('form_validation');
    }
    
    function index() {
        $data['content'] = 'Top Selling Product';
        $type='top_selling_cars';
//        $data['load_res']=$this->home_struture_data->load_featured_pro($type,$featured_id='');
        $this->load->view('top_selling_cars',$data);
    }
    
    
    function top_selling_result(){
        $this->load->library("datatables");
        $this->load->helper("datatables");
//        $a='car_comparison';
        $actionLinkBar=$this->load->view("content/report/datatable/top_selling_action",array(), TRUE);
        $this->datatables
                ->select("featured_id,(select pro_name from variant where variant_id=featured.prd_id_1)  as pro_name,status")                       
                ->from("featured")
//                ->where("featured_type='car_comparison'");
//              ->where("featured_id=car_comparison")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action",$actionLinkBar,'featured_id');
        echo $this->datatables->generate();
    }
    
    
    
    
    function top_selling_cars_upload() {  
        if (isset($_POST['top_selling_cars_submit'])) {
            $this->form_validation->set_rules('searchid', 'first product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'first product', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->index();                
            } else {
                $type = 'top_selling_cars';
                $data = array(
                    'prd_id_1' => $this->input->post('pro_1'),
                    'status' => $this->input->post('status'),
                    'featured_type' => $type
                );
                $query = $this->home_struture_data->upload_top_selling_cars($data);
                redirect('top_selling_cars');
            }
        } else if (isset($_POST['top_selling_cars_update'])) {
            $this->form_validation->set_rules('searchid', 'first product', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');            
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                 $type = '';
            $featured_id=$this->input->post('featured_id');
            $data['featured_res']= $this->home_struture_data->load_featured_pro($type,$featured_id);
            $this->load->view('top_selling_cars',$data);
            } else {
                $type = 'top_selling_cars';
                $featured_id = $this->input->post('featured_id');
                $data = array(
                    'prd_id_1' => $this->input->post('pro_1'),
                    'status' => $this->input->post('status'),
                    'featured_type' => $type
                );
                
//                exit;
                $query=$this->home_struture_data->update_top_selling_cars($data,$featured_id);
                redirect('top_selling_cars');
            }
        }
        else if (isset($_POST['delete']))
        {
            $featured_id = $this->input->post('featured_id');
            $query = $this->home_struture_data->del_top_selling_cars($featured_id);
            redirect('top_selling_cars');
        }
        else if(isset($_POST['get']))
        {
            $type = '';
            $featured_id=$this->input->post('featured_id');
            $data['featured_res']= $this->home_struture_data->load_featured_pro($type,$featured_id);
            $this->load->view('top_selling_cars',$data);
        }
        else {
            redirect('top_selling_cars');
        }
    }

    function get_my_new_car_works() {
        $featured_id = $this->input->post('featured_id');
        $type = 'my_new_car_works';
        $data = $this->home_struture_data->load_featured($featured_id,$type);
        foreach ($data as $res)
        $data['featured_id'] = array('featured_id' => $featured_id, 'status' => $res['status']);
        $pro1 = $res['prd_id_1'];
        $data['prd_id_name1'] = $this->home_struture_data->get_product_detail($pro1);
        $this->load->view('my_new_car_works', $data);
    }

}
