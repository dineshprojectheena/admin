<?php

class Dealer_prices extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('dealer_prices_content');
        $this->load->model('masters');
        $this->load->model('product_data');
        $this->load->model('dashboard');
        $this->load->model('common');
        $this->load->model('report');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function dis_approve() {
        $this->load->library('Mandrill');
        $join_us_id = $_GET['join_us'];
        $status = $_GET['status'];
        $get_dealer_login = $this->db->query("select dealer_id from  `dealer_login` WHERE `join_us`='$join_us_id'")->row_array();
        $dealer_id = $get_dealer_login['dealer_id'];

        $update_dealer_login = $this->db->query("update `dealer_login` set status='$status' WHERE `join_us`='$join_us_id'");
        $update_join_us = $this->db->query("update `dealer_join_us` set status='$status' WHERE `dealer_join_us_id`='$join_us_id'");
        $update_dealer = $this->db->query("update `dealer` set status='$status' WHERE `dealer_id`='$dealer_id'");
        $this->session->set_flashdata('success_msg', 'Dealer Disapproved Successfully');
        redirect('dealer-join-us');
        exit;
    }

    function approve() {
        $this->load->library('Mandrill');
        $join_us_id = $_GET['join_us'];
        $status = $_GET['status'];
        
        $get_dealer_login = $this->db->query("select * from  `dealer_login` WHERE `join_us`='$join_us_id'")->row_array();
        $dealer_id = $get_dealer_login['dealer_id'];

        if (!empty($dealer_id)) {
            $update_dealer_login = $this->db->query("update `dealer_login` set status='$status' WHERE `join_us`='$join_us_id'");
            $update_join_us = $this->db->query("update `dealer_join_us` set status='$status' WHERE `dealer_join_us_id`='$join_us_id'");
            $update_dealer = $this->db->query("update `dealer` set status='$status' WHERE `dealer_id`='$dealer_id'");
            $this->session->set_flashdata('success_msg', 'Dealer Approved Successfully');
            redirect('dealer-join-us');
        }
        $join_us = $this->masters->dealer_join_us($join_us_id);
        
        $added_date = $join_us['added_date'];
        $email = $join_us['email'];
        $status = $join_us['status'];
        $phone = $join_us['phone'];
        $name = $join_us['name'];
        $brand = $join_us['brand'];
        $city = $join_us['city'];
        $dealer_join_us_id = $join_us['dealer_join_us_id'];

        if (!empty($name)) {
            $dealer_name.=substr(preg_replace('/\s+/', '', (strtolower($name))), 0, 5) . '';
        }

        $count_user = $this->db->query("select dealer_username from dealer_login")->num_rows();
        $dealer_name=$dealer_name.''.($count_user+1);
        
        $select_user = $this->db->query("select dealer_username from dealer_login where dealer_username='$dealer_name' and dealer_email='$email'")->num_rows();
        $contact_no = $join_us['phone'];
        $user_password = md5(md5($dealer_name));
        $data = array('dealer_name' => $name, 'contact_no' => $phone, 'email' => $email, 'user_email' => $email, 'cont_person' => $name, 'username' => $dealer_name, 'user_password' => $user_password, 'status' => '1');
        if ($select_user == 0) {
            $insert = $this->db->insert('dealer', $data);
            $insert_id = $this->db->insert_id();
            $dealer_login_insert = $this->db->query("INSERT INTO `dealer_login`(`dealer_login_id`, `dealer_id`,"
                    . " `dealer_username`, `dealer_email`, `dealer_password`, `dealer_shown_password`,"
                    . " `first_name`, `last_name`, `dob`, `phoneno`, `status`, `added_date`,`join_us`)"
                    . " VALUES ('','$insert_id','$dealer_name','$email','$user_password','$dealer_name','','','','$phone','1','$added_date','$join_us_id')");
            $dealer_update = $this->db->query("UPDATE `dealer_join_us` SET `status`='1' WHERE `dealer_join_us_id`='$join_us_id'");
        }
        $logo = base_url() . 'uploads/logo.png';
        $user_email = $email;
        $html_data = "";
        $html_data = <<<END
            <p>Dear $name,</p>
 
<p>Congratulations on Signing-Up with mynewcar.in. Your username is now activated.</p>
 
<p>For your reference, below are your access credentials:</p>
<p>Username: $dealer_name</p>
<p>Password: $dealer_name</p>
 
<p>You can logon to the dealer portal using the link: <a href="https://mynewcar.in/dealer_panel">https://mynewcar.in/dealer_panel</a></p>
 
<p>For any clarifications, you can contact us at contact@mynewcar.in or call us at 022-45020304.</p>
 
<p><b>Best Regards,</b></p>
<p>Mynewcar.in Admin</p>
<img src="$logo" style="width:30%;"/><br>

Phone: 022 45 02 03 04 | Email: contact@mynewcar.in<br>
www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br><br>
---------------------------------------------------------------------------------------------------------------------------------
<br><br>
DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>
Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>
Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>        
                          
END;
        $data = array(
            "recipients" => array(
                array('address' => array('email' => $user_email)),
            ),
            "content" => array(
                "from" => 'account@mynewcar.in',
                "return_path" => 'account@mynewcar.in',
                'subject' => 'Welcome to mynewcar.in Dealer Portal.',
                'html' => $html_data,
                'text' => strip_tags($html_data)
            )
        );
        $status = sendMailViaSparkPost(json_encode($data));
        $this->session->set_flashdata('success_msg', 'Dealer Approved Successfully');
        redirect('dealer-join-us');
    }

    function dealer() {
        $data['city'] = $this->masters->get_City();
        $data['state'] = $this->masters->get_state();
        $data['country'] = $this->masters->get_country();

//        $join_id=$_GET['join_id'];

        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }
        $count = count($data['city']);
        if ($count > 0) {
            $this->load->view('dealer', $data);
        } else {
            $this->load->view('dealer');
        }
    }

    function dealer_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/dealer_all_action", array(), TRUE);
        $this->datatables
                ->select("dealer_id,dealer_name,email,contact_no,"
                        . "(select name from city where city_id=dealer.city) as name,status")
                ->from("dealer")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action", $actionLinkBar, 'dealer_id');

        echo $this->datatables->generate();
    }

    function dealer_upload() {
        $this->load->library('Mandrill');
        $dealer_id = $this->input->post('dealer_id');
        $data['city'] = $this->masters->get_City();
        $data['state'] = $this->masters->get_state();
        $data['country'] = $this->masters->get_country();
        if (isset($_POST['submit'])) {
            $all_data = $this->input->post();
            $profile_images = '';
            if (!empty($_FILES['profiles']['name'])) {
                $_FILES['profiles']['name'];
                $config2['upload_path'] = dealer_url() . 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $image = $this->upload->do_uploads('profiles');
                $profile_images = $image;
            }

            if (!empty($_FILES['mou_copy']['name'])) {
                $config2['upload_path'] = dealer_url() . 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $mou_copy = $this->upload->do_uploads('mou_copy');
            }

            if (!empty($_FILES['agreement_copy']['name'])) {
                $config2['upload_path'] = dealer_url() . 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $agreement_copy = $this->upload->do_uploads('agreement_copy');
            }

            if (!empty($_FILES['commision_slab']['name'])) {
                $config2['upload_path'] = dealer_url() . 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $commision_slab = $this->upload->do_uploads('commision_slab');
            }

            if (!empty($_FILES['process_flow_Chart']['name'])) {
                $config2['upload_path'] = dealer_url() . 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $process_flow_Chart = $this->upload->do_uploads('process_flow_Chart');
            }

            if (!empty($_FILES['dealer_location_map']['name'])) {
                $config2['upload_path'] = dealer_url() . 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $dealer_location_map = $this->upload->do_uploads('dealer_location_map');
            }


            $added_date = date('y-m-d h:i:s');
            $all_data = array_merge($all_data, array('added_date' => $added_date));
            $query = $this->masters->dealer_insert($all_data);

//            $image_data = array('dealer_id' => $query, 'profile_pic' => $profile_images, 'mou_pic' => $mou_copy, 'agreement_pic' => $agreement_copy, 'comission_pic' => $commision_slab, 'process_flow_chart_pic' => $process_flow_Chart, 'dealer_location_map_pic' => $dealer_location_map);
//            $upload_data = $this->masters->dealer_profile_upload($image_data);

            if (!empty($upload_data)) {
                $mandrill_api_key = Mandrill_key;
                $mandrill_ready = NULL;
                try {
                    $this->mandrill->init($mandrill_api_key);
                    $mandrill_ready = TRUE;
                } catch (Mandrill_Exception $e) {
                    $mandrill_ready = FALSE;
                }
                $username = $_POST['username'];
                $user_password = $_POST['user_password'];
                $user_email = $_POST['user_email'];
                $html_data = "";
                $html_data.=" 
                        Hi $username,
                        <br>    
                        Your account created sucessfully,
                        <br>
                        Your username: $username,<br>
                        Your password: $user_password
                        <br>    
                        Thank You,
                        <br>The Mynewcar.in Team";
                if ($mandrill_ready) {
                    $email = array(
                        'html' => $html_data,
                        'text' => 'Account Changed..!',
                        'subject' => $username . ' account created sucessfully..!',
                        'from_email' => 'account@mnc.com',
                        'from_name' => 'Account created',
                        'to' => array(array('email' => $user_email))
                    );
                    $result = $this->mandrill->messages_send($email);
                }
                $this->session->set_flashdata('success_msg', 'Dealer Successfully Created');
            }
            redirect('dealer');
        } else if (isset($_POST['update'])) {

            $all_data = $this->input->post();
            $dealer_id = $this->input->post('dealer_login_id');

            $data['user_images'] = $this->masters->get_edit_dealer($dealer_id, '3');


            $profile_images = $data['user_images']['profile_pic'];
            if (!empty($_FILES['profiles']['name'])) {
                $config2['upload_path'] = 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $image = $this->upload->do_uploads('profiles');
                $profile_images = $image;
            }

            $mou_copy = $data['user_images']['mou_pic'];
            if (!empty($_FILES['mou_copy']['name'])) {
                $config2['upload_path'] = 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $mou_copy = $this->upload->do_uploads('mou_copy');
            }

            $agreement_copy = $data['user_images']['agreement_pic'];
            if (!empty($_FILES['agreement_copy']['name'])) {
                $config2['upload_path'] = 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $agreement_copy = $this->upload->do_uploads('agreement_copy');
            }

            $commision_slab = $data['user_images']['comission_pic'];
            if (!empty($_FILES['commision_slab']['name'])) {
                $config2['upload_path'] = 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $commision_slab = $this->upload->do_uploads('commision_slab');
            }
            $process_flow_Chart = $data['user_images']['process_flow_chart_pic'];
            if (!empty($_FILES['process_flow_Chart']['name'])) {
                $config2['upload_path'] = 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $process_flow_Chart = $this->upload->do_uploads('process_flow_Chart');
            }

            $dealer_location_map = $data['user_images']['dealer_location_map_pic'];
            if (!empty($_FILES['dealer_location_map']['name'])) {
                $config2['upload_path'] = 'uploads/dealer/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $dealer_location_map = $this->upload->do_uploads('dealer_location_map');
            }
            $added_date = date('y-m-d h:i:s');
            $image_data = array('dealer_id' => $dealer_id, 'profile_pic' => $profile_images, 'mou_pic' => $mou_copy, 'agreement_pic' => $agreement_copy, 'comission_pic' => $commision_slab, 'process_flow_chart_pic' => $process_flow_Chart, 'dealer_location_map_pic' => $dealer_location_map, 'added_date' => $added_date);

            $all_data = array_merge($all_data, array('added_date' => $added_date));

            $query = $this->masters->dealer_update($all_data, $image_data);
            $this->session->set_flashdata('success_msg', 'Dealer Successfully updated');
            redirect('dealer');
        } else if (isset($_POST['get'])) {
            $data['results_up'] = $this->masters->get_edit_dealer($dealer_id, '1');
            $data['user_details'] = $this->masters->get_edit_dealer($dealer_id, '2');
            $data['user_images'] = $this->masters->get_edit_dealer($dealer_id, '3');
            $data['ext_brands'] = $this->masters->ext_brand($dealer_id);
            $data['document_status'] = $this->masters->document_status($dealer_id);

//            echo '<pre>';
//            print_r( $data['document_status']);
//            echo '</pre>';
            $this->load->view('dealer', $data);
        } else if (isset($_POST['delete'])) {
            $this->masters->delete_dealer();
            if ($this->db->trans_status()) {
                $this->session->set_flashdata('success_msg', 'Dealer Deleted Successfully');
            } else {
                $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
            }
            redirect('dealer');
        } else {
            redirect('dealer');
        }
    }

    function dealer_price() {
        $data['dealer_data'] = $this->dealer_prices_content->dealer_prices_data();
        $data['content'] = 'dealer_prices';
        $this->load->view('index', $data);
    }

    function dealer_send_credientials() {
        $join_us = $this->input->get_post('join_us');
        $sel_username = $this->db->query("SELECT `email`,`dealer_join_us_id` FROM `dealer_join_us` WHERE dealer_join_us_id='$join_us'")->row_array();
        $dealer_join_us_id = $sel_username['dealer_join_us_id'];
        if (!empty($dealer_join_us_id)) {
            $sel_dealer = $this->db->query("SELECT * FROM `dealer_login` WHERE `join_us`='$dealer_join_us_id'")->row_array();

            if (!empty($sel_dealer)) {
                $dealer_email = $sel_dealer['dealer_email'];
                $dealer_username = $sel_dealer['dealer_username'];
                $dealer_shown_password = $sel_dealer['dealer_shown_password'];
            } else {
                $this->session->set_flashdata('error_msg', 'First approve/activate dealer,than password credientails sent!');
                redirect('dealer-join-us');
            }
        }

        $html_data = <<<END
            <h3>Your password credientials has been sent!</h3>
            <p>To log in to site, use the following credentials</p>
            <p><b>Username: </b> $dealer_username</p>
            <p><b>Password: </b> $dealer_shown_password</p>
            <p></p>
            <p>If you have any questions or encounter any problems logging in,please contact a site administrator</p>
END;
        $data = array(
            "recipients" => array(
                array('address' => array('email' => $dealer_email))
            ),
            "content" => array(
                "from" => 'account@mynewcar.in',
                "return_path" => 'account@mynewcar.in',
                'subject' => 'Password Credientials Sent Successfully',
                'html' => $html_data,
                'text' => strip_tags($html_data)
            )
        );
        $status = sendMailViaSparkPost(json_encode($data));
        $this->session->set_flashdata('success_msg', 'Dealer login credientails sent successfully!');
        redirect('dealer-join-us');
    }

    function dealer_join_us() {
//        $data['dealer_data'] = $this->dealer_prices_content->dealer_prices_data();
        $data['content'] = 'dealer_join_us';
        $this->load->view('index', $data);
    }

    function approve_document() {
        $id = $this->input->get_post('id');
        if ($id > 0) {
            $dealer_id = $this->input->get_post('dealer_id');
            $file = $this->input->get_post('file');
            $status = $this->db->query("select * from dealer_document_status where dealer_id='$dealer_id' and file_type='$file'");
            $data = $status->row_array();
            $dealer_document_status_id = $data['dealer_document_status_id'];
            $count = $status->num_rows();
            $date = date("Y-m-d h:i:s");
            if ($count == 0) {
                $insert = "INSERT INTO `dealer_document_status`(`dealer_document_status_id`, `status`, `dealer_id`, `file_type`, `added_date`) "
                        . "VALUES ('','$id','$dealer_id','$file','$date')";
                $insert = $this->db->query($insert);
            } else {
                $update = $this->db->query("UPDATE `dealer_document_status` "
                        . "SET `status`='$id',`dealer_id`='$dealer_id',`file_type`='$file' WHERE dealer_document_status_id='$dealer_document_status_id'");
            }
            echo 'Updated Status';
        } else {
            echo 'Please Select Status';
        }
    }

    function dealer_verify_documents() {
        $id = $_GET['join_us'];
        $data['document'] = $this->db->query("select dealer_id,"
                        . "(select mou_pic from dealer_files where dealer_id=dealer_login.dealer_id) as mou_pic,"
                        . "(select agreement_pic from dealer_files where dealer_id=dealer_login.dealer_id) as agreement_pic,"
                        . "(select comission_pic from dealer_files where dealer_id=dealer_login.dealer_id) as comission_pic,"
                        . "(select process_flow_chart_pic from dealer_files where dealer_id=dealer_login.dealer_id) as process_flow_chart_pic,"
                        . "(select dealer_location_map_pic from dealer_files where dealer_id=dealer_login.dealer_id) as dealer_location_map_pic"
                        . " from dealer_login where join_us='$id'")->row_array();
        $dealer_id = $data['document']['dealer_id'];
        $data['document_status'] = $this->db->query("select * from `dealer_document_status` where dealer_id='$dealer_id'")->result_array();
//        print_r($data['document_status']);
////        $dealer_id=$dealer['dealer_id']);
//        exit;
//        print_r($dealer);
        $data['content'] = 'dealer_verify_documents';
        $this->load->view('index', $data);
    }

    function check_dealer() {
        $username = $_POST['username'];
        echo $res = $this->db->query("select * from `dealer_login` where `dealer_username`='$username'")->num_rows();
    }

    function dealer_list_prices() {
        $data['dealer_data'] = $this->dealer_prices_content->dealer_uploads_count();
        $data['content'] = 'dealer_list_prices';
        $this->load->view('index', $data);
    }

    function sync_hot_deal() {
        $data = $_GET['data'];
        $data_exp = explode(',', $data);
        print_r($data_exp);
        exit;
        ?>
        Live Successful Sync Product Hot List:-<br><br>
        <?php
        foreach ($data_exp as $data_res) {
            $res = $this->db->query("select `dealer_hot_deals`.*,(select `pro_name_comp` from `variant` where `variant_id`=`dealer_hot_deals`.`variant_id`) as `pro_name_comp` from `dealer_hot_deals`  where `hot_deal_id`='$data_res'")->row_array();
            $product_id = $res['variant_id'];
            $city_id = $res['city_id'];

            $del = $this->db->query("delete from hot_deals where "
                    . "variant_id='$product_id' and city_id='$city_id'");



            $temp_array = array(
                'variant_id' => $res['variant_id'],
                'city_id' => $res['city_id'],
                'sort' => $res['sort'],
                'sort' => $res['sort'],
                'percent' => $res['percent'],
                'valid_until' => $res['valid_until'],
                'added_date' => date('Y-m-d h:i:s'),
                'dealer_id' => $res['dealer_id']
//                'admin_status' => '1'
            );
            $this->db->insert('hot_deals', $temp_array);

//            print_r($res);
//            exit;
            ?>
            <table border="1">
                <thead>
                    <tr>
                        <th >Id</th>
                        <th >Product Name</th>
                        <th >Hot Deal Percent</th>
                        <th >Valid Until</th>                        
                </thead>
                <tbody>
                    <tr class="odd">
                        <td ><?php echo $res['hot_deal_id'] ?></td>
                        <td ><?php echo $res['pro_name_comp'] ?></td>
                        <td ><?php echo $res['percent'] ?></td>
                        <td ><?php echo $res['valid_until'] ?></td>
                    </tr>
                </tbody>
            </table>

            <br>
            <?php
        }
        ?>
        <button onclick="history.go(-1);">Back to result</button>            
        <?php
    }

    function reject_hot_deal() {
        $data = $_GET['data'];
        $data_exp = explode(',', $data);
        ?>
        Rejected Hot Deal List:-<br>    
        <?php
        foreach ($data_exp as $data_res) {
            $data_res;
            $upd = $this->db->query("update `dealer_hot_deals` set admin_status='2' where hot_deal_id='$data_res'");
            $res = "select `dealer_hot_deals`.*,(select `pro_name_comp` from `variant` where `variant_id`=`dealer_hot_deals`.`variant_id`) as `pro_name_comp` from `dealer_hot_deals`  where `hot_deal_id`='$data_res'";
            $result = $this->db->query($res)->result_array();
            ?>
            <table border="1">
                <thead>
                    <tr>
                        <th >Id</th>
                        <th >Product Name</th>
                        <th >Percent</th>
                        <th >Valid Until</th>                        
                </thead>
                <tbody>
                    <tr class="odd">
                        <td ><?php echo $result[0]['hot_deal_id'] ?></td>
                        <td ><?php echo $result[0]['pro_name_comp'] ?></td>
                        <td ><?php echo $result[0]['percent'] ?></td>
                        <td ><?php echo $result[0]['valid_until'] ?></td>
                    </tr>
                </tbody>
            </table>

            <br>
            <?php
        }
        ?>
        <button onclick="history.go(-1);">Back to result</button>            
        <?php
    }

    function reject_price() {
        $data = $_GET['data'];
        $data_exp = explode(',', $data);
        ?>
        Rejected Product List:-<br>    
        <?php
        foreach ($data_exp as $data_res) {
            $data_res;
            $upd = $this->db->query("update `dealer_inventory` set admin_status='2',`by_whom`='admin',`status_date`=NOW() where dealer_inventory_id='$data_res'");

            $res = "select `dealer_inventory_id`,"
                    . "(select pro_name_comp from variant where variant_id=dealer_inventory.variant_id) as pro_name_comp,"
                    . "exshowroom_price,"
                    . "(select name from city where city.city_id=dealer_inventory.city_id) as city_name "
                    . "from dealer_inventory where dealer_inventory_id='$data_res'";
            $result = $this->db->query($res)->result_array();
            ?>
            <table border="1">
                <thead>
                    <tr>
                        <th >Id</th>
                        <th >Product Name</th>
                        <th >City</th>
                        <th >MNC Ex-showroom Price</th>                        
                </thead>
                <tbody>
                    <tr class="odd">
                        <td ><?php echo $result[0]['dealer_inventory_id'] ?></td>
                        <td ><?php echo $result[0]['pro_name_comp'] ?></td>
                        <td ><?php echo $result[0]['city_name'] ?></td>
                        <td ><?php echo $result[0]['exshowroom_price'] ?></td>

                    </tr>
                </tbody>
            </table>

            <br>
            <?php
        }
        ?>
        <button onclick="history.go(-1);">Back to result</button>            
        <?php
    }

    function download() {
        if (isset($_POST['download_all'])) {
            $select = "SELECT (select brand_name from brand where brand.brand_id=d.brand_id) as brand_name,"
                    . "(select model_name from model where model.model_id=d.model_id) as model_name,"
                    . "v.pro_name as variant_name,(select name from city where city.city_id=d.city_id) as city_name,"
                    . "(select fuel_type from fuel_type where fuel_type.fuel_type_id=v.fuel_type) as fuel,"
                    . " d.`colors`, d.`stock`, d.`exshowroom_price`, d.`comp_insurance`, d.`rto`, d.`handling_charges`, d.`amc`, "
                    . "d.`extended_warranty`, d.`zero_depreciation_add_on`, d.`lbt_tax`, d.`doorstep_delivery`, d.`booking_amount`,"
                    . " d.`cash_discount`, d.`exchange_bonus`, d.`loyalty_bonus`, d.`insurance_discount`, d.`corporate_discount`, "
                    . "d.`accessory_discount`, d.`special_reward`, d.`onroad_price`, d.`total_discount`, d.`additional_liqidation_support_dealer`,"
                    . " d.`accessory_package`, d.`hot_deal`, d.`percent`, d.`valid_until`, d.`dealer_id`, d.`added_date`, `by_whom`, `status_date`,"
                    . " CASE admin_status WHEN '2' THEN 'Rejected' WHEN '1' THEN 'Synced' ELSE 'Pending' END AS admin_status"
                    . " FROM dealer_inventory d,variant v where v.variant_id=d.variant_id";
        }

        $export = mysql_query($select);

        $fields = mysql_num_fields($export);

        for ($i = 0; $i < $fields; $i++) {
            $header .= mysql_field_name($export, $i) . "\t";
        }

        while ($row = mysql_fetch_row($export)) {
            $line = '';
            foreach ($row as $value) {
                if ((!isset($value) ) || ( $value == "" )) {
                    $value = "\t";
                } else {
                    $value = str_replace('"', '""', $value);
                    $value = '"' . $value . '"' . "\t";
                }
                $line .= $value;
            }
            $data .= trim($line) . "\n";
        }
        $data = str_replace("\r", "", $data);

        if ($data == "") {
            //$data = "\n(0) Records Found!\n";
        }

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=Dealer_bulk_price.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        print "$header\n$data";
        exit;
    }

    function sync_price() {
        $data = $_GET['data'];
        if (!empty($data)) {
            $data_exp = explode(',', $data);
            ?>
            Sync Successful:-<br><br>    
            <?php
            foreach ($data_exp as $data_res) {
                $res = $this->db->query("select * from dealer_inventory where dealer_inventory_id='$data_res'")->row_array();
                $product_id = $res['variant_id'];
                $dealer_id = $res['dealer_id'];
                $dealer_inventory_id = $res['dealer_inventory_id'];
                $city_id = $res['city_id'];
                $booking_amount = $res['booking_amount'];
                $del = $this->db->query("delete from mnc_exshowroom_price where "
                        . "product_id='$product_id' and city_id='$city_id'");
                $del2 = $this->db->query("delete from onroad_price where "
                        . "variant_id='$product_id' and city_id='$city_id'");
                $del3 = $this->db->query("delete from exshowroom_price where "
                        . "product_id='$product_id' and city_id='$city_id'");
                $del4 = $this->db->query("delete from dealer_booking_amount where "
                        . "product_id='$product_id' and city_id='$city_id' and dealer_inventory_id='$dealer_inventory_id' and dealer_id='$dealer_id'");
                $temp_array = array(
                    'product_id' => $res['variant_id'],
                    'city_id' => $res['city_id'],
                    'mnc_exshowroom_price' => $res['exshowroom_price'],
                    'discount_rs' => '',
                    'status' => $res['status'],
                    'cash_discount' => $res['cash_discount'],
                    'exchange_bonus' => $res['exchange_bonus'],
                    'loyality_bonus' => $res['loyalty_bonus'],
                    'corporate_discount' => $res['corporate_discount'],
                    'accessory_package' => $res['special_gift'],
                    'special_reward' => $res['additional_liqidation_support_dealer'],
                    'accessory_discount' => $res['accessory_discount'],
                    'insurance_benefits' => $res['insurance_discount'],
                    'extended_warranty' => $res['extended_warranty'],
                    'additional_liquidation_support' => $res['additional_liqidation_support_dealer'],
                    'zero_depreciation' => $res['zero_depreciation_add_on'],
                    'total_benefits_perc' => $res['percent'],
                    'insurance_discount' => $res['insurance_discount'],
                    'total_savings' => $res['total_discount'],
                    'dealer_inventory_id' => $res['dealer_inventory_id'],
                    'added_date' => date('Y-m-d h:i:s'),
                    'dealer_id' => $dealer_id
                );


                $this->db->insert('mnc_exshowroom_price', $temp_array);

                $temp_array2 = array(
                    'product_id' => $res['variant_id'],
                    'city_id' => $res['city_id'],
                    'exshowroom_price' => $res['exshowroom_price'],
                    'status' => $res['status'],
                    'last_update' => date('Y-m-d h:i:s'),
                    'added_date' => date('Y-m-d h:i:s'),
                    'dealer_inventory_id' => $res['dealer_inventory_id'],
                    'dealer_id' => $dealer_id
                );

                $this->db->insert('exshowroom_price', $temp_array2);


                $temp_array3 = array(
                    'variant_id' => $res['variant_id'],
                    'city_id' => $res['city_id'],
                    'comp_insurance' => $res['comp_insurance'],
                    'lbt' => $res['lbt_tax'],
                    'handling_price' => $res['handling_charges'],
                    'onroad_price' => $res['onroad_price'],
                    'mnc_on_road_price' => $res['onroad_price'],
                    'home_delievery_charge' => $res['doorstep_delivery'],
                    'road_tax' => $res['rto'],
                    'added_date' => date('Y-m-d h:i:s'),
                    'dealer_inventory_id' => $res['dealer_inventory_id'],
                    'dealer_id' => $dealer_id
                );

                $this->db->insert('onroad_price', $temp_array3);


                $temp_array4 = array(
                    'dealer_id' => $dealer_id,
                    'dealer_inventory_id' => $res['dealer_inventory_id'],
                    'product_id' => $res['variant_id'],
                    'city_id' => $res['city_id'],
                    'booking_amount' => $res['booking_amount'],
                    'status' => $res['status'],                    
                    'added_date' => date('Y-m-d h:i:s')                    
                );

                $this->db->insert('dealer_booking_amount', $temp_array4);

                $res = "select *,"
                        . "(select pro_name_comp from variant where variant_id=dealer_inventory.variant_id) as pro_name_comp,"
                        . ""
                        . "(select name from city where city.city_id=dealer_inventory.city_id) as city_name "
                        . "from dealer_inventory where dealer_inventory_id='$data_res'";
                $result = $this->db->query($res)->result_array();

                $update = $this->db->query("UPDATE `dealer_inventory` SET `admin_status`='1' WHERE `dealer_inventory_id`='$dealer_inventory_id'");
                ?>
                <table border="1">
                    <thead>
                        <tr>
                            <th >Id</th>
                            <th >Product Name</th>
                            <th >City</th>
                            <th >MNC Ex-showroom Price</th>                        
                            <th >Total Savings</th>                        
                    </thead>
                    <tbody>
                        <tr class="odd">
                            <td ><?php echo $result[0]['dealer_inventory_id'] ?></td>
                            <td ><?php echo $result[0]['pro_name_comp'] ?></td>
                            <td ><?php echo $result[0]['city_name'] ?></td>
                            <td ><?php echo $result[0]['exshowroom_price'] ?></td>
                            <td ><?php echo $result[0]['total_discount'] ?></td>

                        </tr>
                    </tbody>
                </table>

                <br>
                <?php
            }
        }
        ?>
        <button onclick="history.go(-1);">Back to result</button>            
        <?php
    }

    function sync_hot_price() {
        $data = $_GET['data'];

        $data_exp = explode(',', $data);
//        print_r($data_exp);
//        exit;
        ?>
        Sync Successful:-<br><br>    
        <?php
        foreach ($data_exp as $data_res) {
            $res = $this->db->query("select * from dealer_inventory where dealer_inventory_id='$data_res'")->row_array();
            echo $product_id = $res['variant_id'];
            $city_id = $res['city_id'];
            $dealer_id = $res['dealer_id'];
            $valid_until = $res['valid_until'];
            $percent = $res['percent'];
            $del = $this->db->query("delete from hot_deals where variant_id='$product_id' and city_id='$city_id'");

            $upd = $this->db->query("INSERT `hot_deals` SET `variant_id`='$product_id',`city_id`='$city_id',"
                    . "`percent`='$percent',`valid_until`='$valid_until',sort='1',`added_date`=NOW(),"
                    . "`dealer_id`='$dealer_id'");
            $insert_id = $this->db->insert_id();
            $res = $this->db->query("select *,"
                            . "(select pro_name_comp from variant where variant_id=hot_deals.variant_id) as `pro_name_comp` from `hot_deals` where `hot_deal_id`='$insert_id'")->row_array();
//            print_r($res);
            ?>

            <table border="1">
                <thead>
                    <tr>
                        <th >Id</th>
                        <th >Product Name</th>
                        <th >Hot Deal Percent</th>
                        <th >Valid Until</th>                        
                </thead>
                <tbody>
                    <tr class="odd">
                        <td ><?php echo $res['hot_deal_id'] ?></td>
                        <td ><?php echo $res['pro_name_comp'] ?></td>
                        <td ><?php echo $res['percent'] ?></td>
                        <td ><?php echo $res['valid_until'] ?></td>
                    </tr>
                </tbody>
            </table>

            <br>
            <?php
        }
        ?>
        <button onclick="history.go(-1);">Back to result</button>            
        <?php
    }

    function single_price_data() {
        $res_id = $_POST['res_id'];
        $result = $this->db->query("select `dealer_inventory_id`,`variant_id`,city_id,dealer_id from `dealer_inventory` where `dealer_inventory_id`='$res_id'")->row_array();
        $product_id = $result['variant_id'];
        $city_id = $result['city_id'];
        $dealer_id = $result['dealer_id'];
        $dealer_exshowroom_price_id = $result['dealer_inventory_id'];
        $res = "select `dealer_inventory_id`,variant_id,"
                . "(select pro_name_comp from variant where variant_id=dealer_inventory.variant_id) as pro_name_comp,"
                . "(select dealer_username from dealer_login where dealer_login_id=dealer_inventory.dealer_id) as username,"
                . "exshowroom_price,onroad_price,"
                . "(select name from city where city.city_id=dealer_inventory.city_id) as city_name "
                . "from dealer_inventory where variant_id='$product_id' and city_id='$city_id'";

        $results = $this->db->query($res)->result_array();
        ?>
        <h6>All Dealer Price</h6>
        <table class="table table-striped table-bordered table-hover dataTable" width="100%">
            <thead>
                <tr>
                    <th style="width:10%;">Id</th>
                    <th  style="width:20%;">Product Name</th>
                    <th  style="width:10%;">City</th>
                    <th  style="width:20%;">MNC Ex-showroom Price</th>                        
                    <th  style="width:20%;">Onroad Price</th>                        
                    <th  style="width:20%;">Dealer</th>                        
            </thead>
            <tbody>
        <?php
        foreach ($results as $results_data) {
            ?>
                    <tr class="odd">
                        <td ><?php echo $results_data['dealer_inventory_id'] ?></td>
                        <td ><?php echo $results_data['pro_name_comp'] ?></td>
                        <td ><?php echo $results_data['city_name'] ?></td>
                        <td ><?php echo $results_data['exshowroom_price'] ?></td>
                        <td ><?php echo $results_data['onroad_price'] ?></td>
                        <td ><?php echo $results_data['username'] ?></td>
                    </tr>
            <?php
        }
        ?>
            </tbody>
        </table>

        <h6>Live Price</h6>
                <?php
//        $res2 = "select `dealer_exshowroom_price_id`,product_id,"
//                . "(select pro_name_comp from variant where variant_id=dealer_exshowroom_price.product_id) as pro_name_comp,"
//                . "(select name from city where city.city_id=dealer_exshowroom_price.city_id) as city_name,"
//                . "(select `mnc_exshowroom_price` from `mnc_exshowroom_price` where `product_id`=`dealer_exshowroom_price`.`product_id` and `city_id`=`dealer_exshowroom_price`.`city_id`) as `mnc_exshowroom_price`, "
//                . "(select `total_savings` from `mnc_exshowroom_price` where `product_id`=`dealer_exshowroom_price`.`product_id` and `city_id`=`dealer_exshowroom_price`.`city_id`) as `total_savings`, "
//                . "(select `onroad_price` from `onroad_price` where `variant_id`=`dealer_exshowroom_price`.`product_id` and `city_id`=`dealer_exshowroom_price`.`city_id`) as `onroad_price` "
//                . "from dealer_exshowroom_price where "
//                . "product_id='$product_id' and city_id='$city_id'";
                $res2 = "select *,(select dealer_username from dealer_login where dealer_login_id=mnc_exshowroom_price.dealer_id) as dealer_username,"
                        . "(select pro_name_comp from variant where variant_id='$product_id') as pro_name_comp,"
                        . "(select name from city where city_id='$city_id') as city_name, "
                        . "(select `onroad_price` from `onroad_price` where `variant_id`='$product_id' and `city_id`='$city_id') as `onroad_price` "
                        . " from mnc_exshowroom_price where product_id='$product_id' and city_id='$city_id'";
//        exit;
                $live_results = $this->db->query($res2)->result_array();
                ?>
        <table class="table table-striped table-bordered table-hover dataTable" border="1" cellpadding="5" width="100%">
            <thead>
                <tr>
                    <th style="width:10%;">Id</th>
                    <th  style="width:20%;">Product Name</th>
                    <th  style="width:10%;">City</th>
                    <th  style="width:20%;">MNC Ex-showroom Price</th>                        
                    <th  style="width:20%;">Onroad Price</th>                        
                    <th  style="width:20%;">Dealer</th>                     
            </thead>
            <tbody>
        <?php
        foreach ($live_results as $results_data) {
            ?>
                    <tr class="odd">
                        <td ><?php echo $dealer_exshowroom_price_id; ?></td>
                        <td ><?php echo $results_data['pro_name_comp'] ?></td>
                        <td ><?php echo $results_data['city_name'] ?></td>
                        <td ><?php echo $results_data['mnc_exshowroom_price'] ?></td>
                        <td ><?php echo $results_data['onroad_price'] ?></td>
                        <td ><?php echo $results_data['dealer_username'] ?></td>
                    </tr>
            <?php
        }
        ?>
            </tbody>
        </table>

        <?php
    }

    function datatables_alldealer() {
        $this->load->library("datatables");
        $dealer = $_GET['dealer'];
        $sdate = $_GET['sdate'];
        $edate = $_GET['edate'];
        $sync_status = $_GET['sync_status'];

        $actionLinkBar = $this->load->view("content/report/datatable/alldealeraction", array(), TRUE);
        $this->datatables->select("dealer_inventory_id,"
                . "(select pro_name_comp from variant where variant_id=dealer_inventory.variant_id) as pro_name_comp,"
                . "exshowroom_price,onroad_price,added_date,(select dealer_name from dealer where dealer_id=dealer_inventory.dealer_id) as dealer_name");
        $this->datatables->from("dealer_inventory");
        if ($dealer != '') {
            $this->datatables->where('dealer_id', $dealer);
        }
        if ($sync_status != '') {
            $this->datatables->where('admin_status', $sync_status);
        }
        $this->datatables->add_column("Action", $actionLinkBar, 'dealer_inventory_id');
        echo $this->datatables->generate();
    }

    function datatables_dealer_join_us() {
        $this->load->library("datatables");
//        $actionLinkBar = $this->load->view("content/report/datatable/dealer_join_us", array(), TRUE);
        $this->datatables
                ->select("dealer_join_us_id,name, dealership, job_title, phone, email, hear_about_us, message, status")
                ->from("dealer_join_us")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column('Action', '$1', 'action(status,1,dealer_join_us_id)')
                ->add_column('Document', '$1', 'dealer_document_verification(dealer_join_us_id,1)')
                ->add_column('Login Crediential', '$1', 'dealer_credential(dealer_join_us_id,1)');
//               ->add_column("Action", $actionLinkBar, 'dealer_join_us_id');
        echo $this->datatables->generate();
    }

    function datatables_dealer_verify_documents() {
        $id = $_GET['join_us'];
//        $dealer=  $this->db->query("select `dealer_id` from `dealer_login` where `join_us`='$id'")->row_array();
//        $dealer_id=$dealer['dealer_id'];
        $this->load->library("datatables");

//        $actionLinkBar = $this->load->view("content/report/datatable/dealer_join_us", array(), TRUE);
        $this->datatables
                ->select("dealer_login_id,"
                        . "(select mou_pic from dealer_files where dealer_id=dealer_login.dealer_id) as mou_pic,"
                        . "(select agreement_pic from dealer_files where dealer_id=dealer_login.dealer_id) as agreement_pic,"
                        . "(select comission_pic from dealer_files where dealer_id=dealer_login.dealer_id) as comission_pic,"
                        . "(select process_flow_chart_pic from dealer_files where dealer_id=dealer_login.dealer_id) as process_flow_chart_pic,"
                        . "(select dealer_location_map_pic from dealer_files where dealer_id=dealer_login.dealer_id) as dealer_location_map_pic"
                        . "");
        $this->datatables->from("dealer_login");
        if ($id != '') {
            $this->datatables->where('join_us', $id);
        }
//                ->from("dealer_login")
//                ->where('dealer_id', $dealer_id);
//                ->edit_column('status', '$1', 'check_status(status)')
//                ->add_column('Action', '$1', 'action(status,1,dealer_join_us_id)')
//                ->add_column('Document', '$1', 'dealer_document_verification(dealer_join_us_id,1)');
//               ->add_column("Action", $actionLinkBar, 'dealer_join_us_id');
        echo $this->datatables->generate();
    }

    function dealer_hot_deal() {
        $data['dealer_data'] = $this->dealer_prices_content->dealer_prices_data();

        $data['content'] = 'dealer_hot_deal';
        $this->load->view('index', $data);
    }

    function dealer_hot_deals() {
        $data['dealer_data'] = $this->dealer_prices_content->dealer_hot_deals_count();
//        print_r($data['dealer_data']);


        $data['content'] = 'dealer_list_hotdeals';
        $this->load->view('index', $data);
    }

    function datatables_allhotdeal() {
        $this->load->library("datatables");
        $dealer = $_GET['dealer'];
        $sdate = $_GET['sdate'];
        $edate = $_GET['edate'];
        $sync_status = $_GET['sync_status'];

        $actionLinkBar = $this->load->view("content/report/datatable/allhotdealeraction", array(), TRUE);
        $this->datatables->select("hot_deal_id,(select pro_name_comp from variant where variant_id=dealer_hot_deals.variant_id) as pro_name_comp,percent,valid_until,added_date");
        $this->datatables->from("dealer_hot_deals");
        if ($dealer != '') {
            $this->datatables->where('dealer_id', $dealer);
        }
        if ($sync_status != '') {
            $this->datatables->where('admin_status', $sync_status);
        }
//        if($sdate!='' && $edate!='')
//        {    
//        $this->datatables->where("added_date>=',$sdate");
//        $this->datatables->where("added_date<=',$edate");
//        }
        $this->datatables->add_column("Action", $actionLinkBar, 'hot_deal_id');

        echo $this->datatables->generate();
    }

    function download_hot_deal() {
        if (isset($_POST['download_all'])) {
            $select = "select h.hot_deal_id,(select `brand_name` from `brand` where `brand`.`brand_id`=v.`brand_id`) as brand_name,"
                    . "(select `model_name` from `model` where `model`.`model_id`=v.`model_id`) as model_name,"
                    . " (select `name` from `city` where `city_id`=h.city_id) as city_name,"
                    . " pro_name,(select `fuel_type` from `fuel_type` where v.`fuel_type`=`fuel_type_id`) as fuel_type,"
                    . "h.sort,h.percent,h.added_date,(select `dealer_username` from `dealer_login` where `dealer_login_id`=h.`dealer_id`) as `username`,`comment` from `variant` v,`dealer_hot_deals` h where h.`variant_id`=v.`variant_id`";
        } else if (isset($_POST['download_rejected'])) {
            $select = "select h.hot_deal_id,(select `brand_name` from `brand` where `brand`.`brand_id`=v.`brand_id`) as brand_name,"
                    . "(select `model_name` from `model` where `model`.`model_id`=v.`model_id`) as model_name,"
                    . " (select `name` from `city` where `city_id`=h.city_id) as city_name,"
                    . " pro_name,(select `fuel_type` from `fuel_type` where v.`fuel_type`=`fuel_type_id`) as fuel_type,"
                    . "h.sort,h.percent,h.added_date,(select `dealer_username` from `dealer_login` where `dealer_login_id`=h.`dealer_id`) as `username`,`comment` from `variant` v,`dealer_hot_deals` h where h.`variant_id`=v.`variant_id` and h.admin_status='2'";
        } else if (isset($_POST['download_active'])) {
            $select = "select h.hot_deal_id,(select `brand_name` from `brand` where `brand`.`brand_id`=v.`brand_id`) as brand_name,"
                    . "(select `model_name` from `model` where `model`.`model_id`=v.`model_id`) as model_name,"
                    . " (select `name` from `city` where `city_id`=h.city_id) as city_name,"
                    . " pro_name,(select `fuel_type` from `fuel_type` where v.`fuel_type`=`fuel_type_id`) as fuel_type,"
                    . "h.sort,h.percent,h.added_date,(select `dealer_username` from `dealer_login` where `dealer_login_id`=h.`dealer_id`) as `username`,`comment` from `variant` v,`dealer_hot_deals` h where h.`variant_id`=v.`variant_id` and h.admin_status='1'";
        }

//        echo $select;
//        
//        exit;
        $export = mysql_query($select);

        $fields = mysql_num_fields($export);

        for ($i = 0; $i < $fields; $i++) {
            $header .= mysql_field_name($export, $i) . "\t";
        }

        while ($row = mysql_fetch_row($export)) {
            $line = '';
            foreach ($row as $value) {
                if ((!isset($value) ) || ( $value == "" )) {
                    $value = "\t";
                } else {
                    $value = str_replace('"', '""', $value);
                    $value = '"' . $value . '"' . "\t";
                }
                $line .= $value;
            }
            $data .= trim($line) . "\n";
        }
        $data = str_replace("\r", "", $data);

        if ($data == "") {
            //$data = "\n(0) Records Found!\n";
        }

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=Dealer_hot_deal.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        print "$header\n$data";
        exit;
    }

}
