<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Authorization extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('login_authorization');
//        $this->output->nocache();
        parse_str($_SERVER['QUERY_STRING'], $_REQUEST);
        $this->load->library('image_uploads');
    }

    function onlyFbLogin() {
//        echo $res==$data.'-|-'.$data2;
//        $session_id=$this->session->userdata($res);
        if ($this->input->get('req_uri')) {
            $request_uri = $this->input->get('req_uri');
            $this->session->set_flashdata('req_uri', $request_uri);
        }
        $this->load->library('facebook');
        /* Callback url of Facebook is set in constants.php */
        $data['fb_url'] = $this->facebook->getLoginUrl(array('scope' => 'email, user_birthday, user_work_history'));
        redirect($data['fb_url']);
    }

    /* actions to do after user has logged in from fb */

    public function fblogin() {
        $this->load->library('facebook');
        $this->session->unset_userdata('oauth_request_token_secret');
        $this->session->unset_userdata('oauth_request_token');
        if ($this->input->get('error_reason') == 'error_reason' || $this->input->get('error') == 'access_denied') {
            $this->session->set_flashdata('login_error', FB_LOGIN_ERROR_MSG);
            redirect(base_url());
        }
        $userId = $this->facebook->getUser();

        if ($userId != 0) {
            $user = $this->facebook->api('/me');

            $img_url = 'https://graph.facebook.com/' . $user['id'] . '/picture?width=180&height=180';
//            $this->uploadProfileImage($user['id'], $img_url);

            $dob = date("Y-m-d", strtotime($user['birthday']));
            if (!isset($user['email'])) {
                log_message('Error', 'Email not received for facebook user - ' . $user['id']);
                $this->session->set_flashdata('login_error', '<span class="error">Seems we couldnt retrieve your Email from your Facebook. We recommend to try logging in using LinkedIn</span>');
                redirect(base_url());
            } else {
                if (is_array($user['location'])) {
                    $address = $user['location']['name'];
                } else {
                    $address = '';
                }

                if (!isset($user['location'])) {
                    $user['location'] = '-';
                }
                $res_data = array(
                    'dob' => $dob,
                    'firstname' => $user['first_name'],
                    'username' => $user['first_name'] . '' . $user['last_name'] . '' . date("ymdHis"),
                    'gender' => $user['gender'],
                    'fb_id' => $user['id'],
                    'fb_link' => $user['link'],
                    'email' => $user['email'],
                    'date_added' => date('Y-m-d'),
                    'address' => $address
                );
                $this->image_uploads->uploadProfileImage($user['id'], $img_url);
                $res = $this->login_authorization->facebook_social_auth($res_data);
                // exit;
                $cond = array('logged_in' => 'TRUE');
                $res_status = array_merge($res, $cond);

                $this->session->set_userdata("logged_in_user", $res_status);
                $session = $this->session->userdata("logged_in_user");

                if ($this->session->flashdata('req_uri')) {
                    redirect($this->session->flashdata('req_uri'));
                } else {
                    redirect('dashboard');
                }
            }
        } else {
            ?>
        <script>
        alert('Sorry Some issue occured with facebook login,please try again with some other social..');    
        window.location.href="<?php echo $this->session->flashdata('req_uri');?>";
        </script>
        <?php  
        }
    }

    function dashboard() {
        $data['content'] = 'dashboard';
        $this->load->view('index', $data);
    }

    function logout() {
        $this->session->set_userdata('logged_in', FALSE);
        $this->session->unset_userdata('logged_in_user');
        $this->session->sess_destroy();
        redirect(base_url());
    }

    function join_group() {
        $brand_id = $this->input->post('brand_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status');
        $status_return = $this->login_authorization->join_brand_group($brand_id, $user_id, $status);
        print_r($status_return);
    }
    
    function linkdin_url()
    {
    if ($this->input->get('req_uri')) {
            $request_uri = $this->input->get('req_uri');
            $this->session->set_flashdata('req_uri', $request_uri);
        }    
    $this->input->get('req_uri');
    
    $this->config->load("linkedin", TRUE);
        $config = $this->config->item('linkedin');
        $this->load->library('linkedin', $config);
        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);

        /* Callback url of Facebook is set in constants.php */
        $token = $this->linkedin->retrieveTokenRequest();
        $this->session->set_userdata('oauth_request_token_secret', $token['linkedin']['oauth_token_secret']);
        $this->session->set_userdata('oauth_request_token', $token['linkedin']['oauth_token']);
        $data = array(
            'oauth_request_token_secret' => $token['linkedin']['oauth_token_secret'],
            'oauth_request_token' => $token['linkedin']['oauth_token']
        );

        if($this->session->flashdata('login_error')) {
            $data['login_error_msg'] = $this->session->flashdata('login_error');
        }
        $data['linkedin_url'] = "https://api.linkedin.com/uas/oauth/authorize?oauth_token=" . $token['linkedin']['oauth_token'];
        redirect($data['linkedin_url']);    
    }
    

    function linkedLogin() {
        $this->config->load("linkedin", TRUE);
        exzt
        $config = $this->config->item('linkedin');
        $this->load->library('linkedin', $config);
        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        $this->load->library('ph_utils');

        $oauth_token = $this->session->userdata('oauth_request_token');

        $oauth_token_secret = $this->session->userdata('oauth_request_token_secret');
        $oauth_verifier = $this->input->get('oauth_verifier');
        $response = $this->linkedin->retrieveTokenAccess($oauth_token, $oauth_token_secret, $oauth_verifier);
        $profile = $this->linkedin->profile('~:(id,site-standard-profile-request,first-name,last-name,picture-url,headline,location,date-of-birth,email-address,summary,primary-twitter-account)');
        $response = $this->linkedin->setTokenAccess($response['linkedin']);
        
        
        $user = json_decode($profile['linkedin']);
                $lindin_id=$user->id;
                $firstName=$user->firstName;
                $lastName=$user->lastName;
                $emailAddress=$user->emailAddress;
                $lindin_link=$user->siteStandardProfileRequest->url;
                $user_data=array(
                    'linkdin' => $lindin_id,
                    'firstname' => $firstName,
                    'username' => $firstName . '' . $lastName . '' . date("ymdHis"),
                    'lastname' => $lastName,
                    'email' => $emailAddress,
//                    'email_id' => $user->emailAddress,
                    'linkedin_link' => $lindin_link
                );
        exit;
        if(!empty($emailAddress))
        {    
        $res = $this->login_authorization->linkdin_social_auth($user_data);
        ################ Email Send When User Register
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            $email=$user_data['email'];
            $html_data = "";
            $html_data.=" 
                Welcome To Mynewcar.in<br>
                Hi, $email
                <br>    
                Welcome to www.Mynewcar.in you have successfully registered.
                <br>                
                Thank You,
                <br>The Mynewcar.in Team.";

            $html_data;
            if ($mandrill_ready) {
                $email = array(
//            'html' => $this->load->view('emailers/download_config', TRUE), //Consider using a view file
                    'html' => $html_data, //Consider using a view file
                    'text' => 'Account created successully',
                    'subject' => 'Welcome to Mynewcar.in',
                    'from_email' => 'account@mnc.com',
                    'from_name' => 'MNC Account',
                    'to' => array(array('email' => $email))
                );
                $result = $this->mandrill->messages_send($email);
            }   
            ################ Email Send When User Register   
        
        
        
        
        
        
        $cond = array('logged_in' => 'TRUE');
                $res_status = array_merge($res, $cond);

                $this->session->set_userdata("logged_in_user", $res_status);
                $session = $this->session->userdata("logged_in_user");

               if ($this->session->flashdata('req_uri')) {
                    redirect($this->session->flashdata('req_uri'));
                } else {
                    redirect('');
                }
        }
        else {
        ?>
        <script>
        alert('Sorry Some issue occured with linkdin login,please try again with some other social..');    
        window.location.href="<?php echo $this->session->flashdata('req_uri');?>";
        </script>
        <?php        
//        redirect('');    
        }
    }

    public function linkedinLoginCancelled() {
        $this->session->set_flashdata('login_error', LINKEDIN_LOGIN_ERROR_MSG);
        $this->session->unset_userdata('oauth_request_token_secret');
        $this->session->unset_userdata('oauth_request_token');
        redirect('login');
    }
    
    function login()
    {
    $signin_email=$this->input->post('signin_email');
    $url=$this->input->post('url');
    $signin_password=$this->input->post('signin_password');
    $res= $this->login_authorization->custom_login($signin_email,$signin_password);
    print_r($res);    
    exit;
    if(!empty($res))
    {    
    $cond = array('logged_in' => 'TRUE');
                $res_status = array_merge($res, $cond);
                $this->session->set_userdata("logged_in_user", $res_status);
                $session = $this->session->userdata("logged_in_user");
                redirect($url);                
    }
    else
    {
    ?>   
    <script>
    alert("Login Crediential can't match try again");    
    </script>
    <?php
     redirect($url);   
    }    
    }
    
    function forget()
    {
    $forget_email=$this->input->post('forget_email');
    $res = $this->login_authorization->check_email($forget_email);
    $user_id=$res[0]['user_id'];
    if(empty($res))
    {
    ?>   
    <script>
    alert('Email id not exist...');    
    </script>
    <?php    
    }
    else {
    $uniue_id=md5(uniqid($forget_email,true));
    $res=$this->login_authorization->update_unq_forget($forget_email,$uniue_id,$user_id);
    $mandrill_api_key=Mandrill_key;
        $this->load->library('Mandrill');
        $mandrill_ready = NULL;
        try{
            $this->mandrill->init($mandrill_api_key);
            $mandrill_ready = TRUE;
        } catch (Mandrill_Exception $e) {
            $mandrill_ready = FALSE;
        }
        $html_data="";
        $html_data.="Hello,<br><b>Please click the below given link to change the account</b><br>";
        $html_data.=base_url()."stitcher/change_password/".$uniue_id;
        $html_data.="<br>";
        $html_data.="Thanks & Best Regards,<br>";
        $html_data.="MNC Team";
        $html_data;
        if ($mandrill_ready){
            $email = array(
//            'html' => $this->load->view('emailers/download_config', TRUE), //Consider using a view file
            'html' => $html_data, //Consider using a view file
            'text' => 'You have a change password link',
            'subject' => 'please change your mnc account',
            'from_email' => 'account@mnc.com',
            'from_name' => 'MNC Account',
            'to' => array(array('email' => $forget_email))
            );
            $result = $this->mandrill->messages_send($email);
        }
    ?>
    <script>
    alert('Password Send,please check your mail box..');    
    </script>
    <?php    
    redirect(base_url());    
    }    
    }
    function update_password()
    {
    $user_id=$this->input->post('user_id');    
    $new_password=$this->input->post('new_password');    
    $re_new_password=$this->input->post('re_new_password');    
    $res=$this->login_authorization->update_password($user_id,$new_password,$re_new_password);    
//    if($res==1)
//    {    
    ?>
    <script>
    alert('Password updated Sucessfully');    
    </script>
    <?php
//    }
    redirect(base_url());
    }
}

?>
