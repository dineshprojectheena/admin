<?php

class Stitcher extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('masters');
        $this->load->model('product_data');
        $this->load->model('dashboard');
        $this->load->model('common');
        $this->load->model('report');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library("datatables");
    }

    function index() {
        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        $this->load->view('login', $data);
    }

    function datatables_affiliates_count() {
        $this->load->library("datatables");
        $this->datatables
                ->select("user,code,(select COUNT(*) from `order` where ref_id=affiliate.affiliate_id) as total")
//                        . "(select COUNT(*) from order where order.ref_id=affiliate.affiliate_id) as total")
                ->from("affiliate");
        echo $this->datatables->generate();
    }

    function change_password() {
        $data['content'] = 'change_password';
        $this->load->view('index', $data);
    }

    function top_affiliate() {
        $data['test_drive'] = $this->dashboard->dashboard_test_drive('1');
        $data['content'] = 'top_affiliate';
        $this->load->view('index', $data);
    }

    function home() {
        $id=$_GET['id'];
        $data['test_drive'] = $this->dashboard->dashboard_test_drive('1');
        $data['test_success'] = $this->dashboard->dashboard_test_drive('2');
        $data['test_fail'] = $this->dashboard->dashboard_test_drive('3');
        $data['new_order'] = $this->dashboard->new_orders();
        $data['total_order'] = $this->dashboard->total_orders();
        $data['new_testdrive'] = $this->dashboard->new_test_drive_enquiry();
        $data['new_user'] = $this->dashboard->new_users();
        $data['new_accessory'] = $this->dashboard->new_accessory();
        $data['car_confiure'] = $this->dashboard->car_confiured();
        $data['car_exchange'] = $this->dashboard->car_exchange();
        $data['car_lease'] = $this->dashboard->car_lease();
        $data['car_upcoming_car_enquiry'] = $this->dashboard->car_upcoming_car();
        $data['car_upcoming_sale'] = $this->dashboard->car_upcoming_sale();
        $data['contact_us'] = $this->dashboard->contact_us();
//        print_r($data['new_accessory']);
        $data['content'] = 'home';
        $this->load->view('index', $data);
    }

    function datatables_allUsers() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/allUsersAction", array(), TRUE);
        $this->datatables
                ->select("order.order_id,"
                        . "(select pro_name_comp from variant where variant_id=order.product_id) as pro_name,"
                        . "(select firstname from user where user_id=order.user_id) as firstname,"
                        . "(select email from user where user_id=order.user_id) as email,"
                        . "(select code from affiliate where affiliate_id=order.ref_id) as code,"
                        . "order.added_date")
                ->from("order")
                ->add_column("Action", $actionLinkBar, 'order.order_id');

        echo $this->datatables->generate();
    }

    function datatables_accessory() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/allaccessory", array(), TRUE);
        $this->datatables
                ->select("accessory_configuration.accessory_configuration_id,accessory_configuration.invoice_no,"
                        . "(select firstname from user where user_id=accessory_configuration.user_id) as firstname,"
                        . "(select email from user where user_id=accessory_configuration.user_id) as email,"
                        . "accessory_configuration.added_date")
//                        . "accessory_configuration.added_date")
                ->from("accessory_configuration")
                ->where('status', '2')
                ->add_column("Action", $actionLinkBar, 'accessory_configuration.accessory_configuration_id');

        echo $this->datatables->generate();
    }

    function datatables_upcoming() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/upcominglusersaction", array(), TRUE);
        $this->datatables
                ->select("upcoming_configuration_id,invoice_no,(select firstname from user where user_id=upcoming_configuration.user_id) as firstname,(select email from user where user_id=upcoming_configuration.user_id) as email,added_date")
                ->from("upcoming_configuration")
                ->where('status', '2')
                ->add_column("Action", $actionLinkBar, 'upcoming_configuration_id');
        echo $this->datatables->generate();
    }

    /* Banner data section start here */

    function banner() {

        $data['results'] = $this->masters->uploaded_banner_data();
        $count = count($data['results']);
        if ($count > 0) {
            $this->load->view('banner', $data);
        } else {
            $this->load->view('banner');
        }
    }

    function banner_upload() {
        #### used for update section here
        if (isset($_POST['update'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('banner_name', 'Banner Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('link', 'Link', 'trim|required|xss_clean');
            $this->form_validation->set_rules('banner_desc', 'Banner Desc', 'trim|required|xss_clean');


            $config['upload_path'] = 'application/views/uploads/banner/';
            $config['allowed_types'] = 'gif|jpg|png';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->set_allowed_types('*');
            $data['upload_data'] = '';
            $this->upload->do_upload('accessory_pic');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->banner();
            } else {
                $query = $this->masters->update_data();
                redirect('banner');
            }
        }

        #### used for delete section here
        else if (isset($_POST['delete'])) {

            $del_id = $this->input->post('count');
            $this->masters->del_banner($del_id);
            redirect('banner');
        }

        #### used for insert section here
        else if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('banner_name', 'Banner Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'required');
            //$this->form_validation->set_rules('accessory_pic', 'Accessory Pic', 'required');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('link', 'Link', 'trim|required|xss_clean');
            $this->form_validation->set_rules('banner_desc', 'Banner Desc', 'trim|required|xss_clean');


            $config['upload_path'] = 'application/views/uploads/banner/';
            $config['allowed_types'] = 'gif|jpg|png';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->set_allowed_types('*');
            $data['upload_data'] = '';
            $this->upload->do_upload('accessory_pic');


            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->banner();
            } else {

                if ($query = $this->masters->banner_upload_data()) {
                    redirect('banner');
                }
            }
        } else if (isset($_POST['edit'])) {
            $brandgrp_id = $this->input->post('count2');
            $data['banner_get'] = $this->masters->get_banner($brandgrp_id);
            $this->load->view('banner', $data);
        }
        #### used for else redirect to banner page
        else {
            redirect('banner');
        }
    }

    /* Banner data section End here */





    /* Faq data section start here */

    function faq() {
//        $data['results'] = $this->masters->faq_data_res();
        $count = count($data['results']);
        if ($count > 0) {
            $this->load->view('faq', $data);
        } else {
            $this->load->view('faq');
        }
    }

    function faq_result() {
        $this->load->library("datatables");
        $this->load->helper("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/faq_action", array(), TRUE);
        $this->datatables
                ->select("faq_id,main_heading,descrtiption,type,status")
                ->from("faq")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action", $actionLinkBar, 'faq_id');

        echo $this->datatables->generate();
    }

    function faq_upload() {

        #### used for update section here
        if (isset($_POST['update'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('main_heading', 'Top Heading', 'trim|required|xss_clean');
            $this->form_validation->set_rules('banner_desc', 'Description', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('type', 'Status', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );

                $faq_id = $this->input->post('count2');
                $data['faq_get'] = $this->masters->get_faq($faq_id);
                $this->load->view('faq', $data);
            } else {
                $this->masters->update_faq();
                redirect('faq');
            }
        }
        #### used for delete section here
        else if (isset($_POST['delete'])) {

            $del_id = $this->input->post('count');
            $this->masters->del_faq($del_id);
            redirect('faq');
        }

        #### used for insert section here
        else if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('main_heading', 'Top Heading', 'trim|required|xss_clean');
            $this->form_validation->set_rules('banner_desc', 'Description', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('type', 'type', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->faq();
            } else {
                if ($query = $this->masters->faq_insert_data()) {
                    redirect('faq');
                }
            }
        } else if (isset($_POST['edit'])) {
            $faq_id = $this->input->post('count2');
            $data['faq_get'] = $this->masters->get_faq($faq_id);
            $this->load->view('faq', $data);
        }
        #### used for else redirect to banner page
        else {
            redirect('faq');
        }
    }

    /* Faq data section End here */


    /* News data section start here */

    function news() {
//        $data['results'] = $this->masters->news_data_res();
        $count = count($data['results']);
        if ($count > 0) {
            $this->load->view('news', $data);
        } else {
            $this->load->view('news');
        }
    }

    function news_result() {
        $this->load->library("datatables");
        $this->load->helper("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/news_action", array(), TRUE);
        $this->datatables
                ->select("latest_news_id,news_title,author,publish_date,status")
                ->from("news")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action", $actionLinkBar, 'latest_news_id');

        echo $this->datatables->generate();
    }

    function news_upload() {
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('news_title', 'News Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('news_titles', 'Details', 'trim|required|xss_clean');
            $this->form_validation->set_rules('id-date-picker-1', 'Publish Date', 'trim|required|xss_clean');
            $this->form_validation->set_rules('Author', 'Author', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('news_pic', 'Pic', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->news();
            } else {
                $config['upload_path'] = 'uploads/news/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';

                $img_name = $this->upload->do_uploads('news_pic');
                $added_date = date('y-m-d H:i:s');
                $data = array(
                    'news_title' => $_POST['news_title'],
                    'details' => $_POST['news_titles'],
                    'publish_date' => $_POST['id-date-picker-1'],
                    'author' => $_POST['Author'],
                    'image' => $img_name,
                    'author' => $_POST['Author'],
                    'status' => $_POST['status'],
                    'cust_title' => $_POST['title'],
                    'meta_description' => $_POST['description'],
                    'meta_keyword' => $_POST['keyword'],
                    'added_date' => $added_date
                );


                if ($query = $this->masters->news_insert_data($data)) {
                    redirect('news');
                }
            }
        } else {
            redirect('news');
        }
    }

    function delete_news() {
        if (isset($_POST['delete'])) {
            $del_id = $this->input->post('count');
            $this->masters->del_news($del_id);
            redirect('news');
        }
    }

    function get_update_news() {
        if (isset($_POST['update'])) {
            $datanews['results_news'] = $this->masters->sear_news_data_res();
            $count = count($datanews['results_news']);
            if ($count > 0) {
                $this->load->view('news_update', $datanews);
            } else {
                redirect('news');
            }
        } else {
            redirect('news');
        }
    }

    function news_update() {
        if (isset($_POST['Update'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('news_title', 'News Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('news_titles', 'Details', 'trim|required|xss_clean');
            $this->form_validation->set_rules('id-date-picker-1', 'Publish Date', 'trim|required|xss_clean');
            $this->form_validation->set_rules('Author', 'Author', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->news();
            } else {
                $config['upload_path'] = 'uploads/news/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                if (!empty($_FILES['news_pic']['name'])) {
                    $img_name = $this->upload->do_uploads('news_pic');
                } else {
                    $img_name = $this->input->post('news_pic2');
                }
//                echo $img_name;
                $added_date = date('y-m-d H:i:s');
                $data = array(
                    'news_title' => $_POST['news_title'],
                    'details' => $_POST['news_titles'],
                    'publish_date' => $_POST['id-date-picker-1'],
                    'author' => $_POST['Author'],
                    'image' => $img_name,
                    'author' => $_POST['Author'],
                    'status' => $_POST['status'],
                    'cust_title' => $_POST['title'],
                    'meta_description' => $_POST['description'],
                    'meta_keyword' => $_POST['keyword'],
                    'added_date' => $added_date
                );
                if ($query = $this->masters->news_update_data($data)) {
                    redirect('news');
                }
            }
        } else {
            redirect('news');
        }
    }

    /* News data section end here */



    /* Brand group master data section start here */

    function brand_group() {
        $data['title'] = 'Brand Group';
        $data['results'] = $this->masters->brand_group_data_res();
        $this->load->view('brand_group', $data);
    }

    function group_upload() {
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('group_name', 'Group Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('group_size', 'Group Size', 'trim|required|numeric|max_length[10]');
            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->brand_group();
            } else {
                if ($query = $this->masters->brand_group_insert()) {
                    redirect('brand_group');
                }
            }
        } else if (isset($_POST['update'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('group_name', 'Group Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('group_size', 'Group Size', 'trim|required|numeric|max_length[10]');
            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            $rec_data = array(
                'group_name' => $this->input->post('group_name'),
                'group_size' => $this->input->post('group_size'),
                'status' => $this->input->post('status'),
            );
            $postcode = array('brand_group_id' => $this->input->post('group_id'));
            $data['results_up'] = array_merge($postcode, $rec_data);

            if (!$this->form_validation->run()) {
                $this->load->view('brand_group', $data);
//                $this->load->view('brand_group');                
            } else {
                if ($query = $this->masters->brand_group_update()) {
                    redirect('brand_group');
                }
            }
        } else {
            redirect('brand_group');
        }
    }

    function del_brand_group() {
        if (isset($_POST['delete'])) {
            $brandgrp_id = $this->input->post('brandgrp_id');
            $this->masters->del_brand_group($brandgrp_id);
            redirect('brand_group');
        }
    }

    function get_brand_group() {
        if (isset($_POST['get'])) {
            $brandgrp_id = $this->input->post('get_brandgrp_id');
            $data['cond'] = '1';
            $data['results_up'] = $this->masters->get_brand_group($brandgrp_id);
            $this->load->view('brand_group', $data);
        } else {
            redirect('brand_group');
        }
    }

    /* Brand group master data section start here */





    /* Brand master data section start here */

    function brand() {
//        $data['results'] = $this->masters->brand_res();
        $data['results_brand'] = $this->masters->get_brand_group_res();
        $this->load->view('brand', $data);
    }

    function brand_result() {
        $actionLinkBar = $this->load->view("content/report/datatable/brand_all_action", array(), TRUE);
        $this->datatables
                ->select("brand_id,brand_name,details,status")
                ->from("brand")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action", $actionLinkBar, 'brand_id');

        echo $this->datatables->generate();
    }

    function get_buttons($id) {
        $ci = & get_instance();
//        if($id==1)
//        {
//        $status='Enable';    
//        }    
//        else
//        {
//        $status='Disbale';    
//        }    
//        $ci = & get_instance();
//        $html .=$status;
        return $id;
    }

    function upload_brand_group() {
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('brand_name', 'Brand Name', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('details', 'Brand Details', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('description', 'Status', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('details', 'Status', 'trim|required|xss_clean');
            $brand_name = preg_replace('/[^A-Za-z0-9\-]/', '-', $this->input->post('brand_name'));
//            ;
            if (!$this->form_validation->run()) {
                $this->brand();
            } else {
                $config['upload_path'] = 'uploads/brand/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG|JPEG|GIF|';
                $config['max_size'] = '5200';
                $this->load->library('upload', $config);
                $img_name = $this->upload->do_uploads('brand_images');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php
                    exit;
                }
                $added_date = date('Y-m-d h:i:s');
                $data = array(
                    'brand_name' => $brand_name,
                    'description' => $this->input->post('brand_description'),
                    'brand_image' => $img_name,
                    'details' => $this->input->post('details'),
                    'status' => $this->input->post('status'),
//                    'status' => $_POST['status'],
                    'added_date' => $added_date
                );
                $query = $this->masters->brand_insert($data, $brand_name);
                $this->session->set_flashdata('status', "Brand Created Successfully");
                redirect('brand');
            }
        } else if (isset($_POST['update'])) {
            $brandgrp_id = $this->input->post('brand_id');
            $data['results_ups'] = $this->masters->get_brand($brandgrp_id);
            $this->load->library('form_validation');
            $this->form_validation->set_rules('brand_name', 'Brand Name', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('details', 'Brand Details', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('description', 'Status', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('details', 'Status', 'trim|required|xss_clean');

            $brand_name = preg_replace('/[^A-Za-z0-9\-]/', '-', $this->input->post('brand_name'));

            $ids = $this->input->post('brand_id');
            if (!$this->form_validation->run()) {
                $this->load->view('brand', $data);
            } else {

                $config['upload_path'] = 'uploads/brand/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '5200';
                $this->load->library('upload', $config);
                if (!empty($_FILES['brand_images']['name'])) {
                    $img_name = $this->upload->do_uploads('brand_images');

                    if ($img_name == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php
                        exit;
                    }
                }
                if (!empty($img_name)) {
                    $img_brand = $img_name;
                } else {
                    $img_brand = $this->input->post('brand_image2');
                }
                $added_date = date('Y-m-d h:i:s');

                $data = array(
                    'brand_name' => $brand_name,
                    'description' => $this->input->post('brand_description'),
                    'brand_image' => $img_name,
                    'details' => $this->input->post('details'),
                    'status' => $this->input->post('status'),
//                    'status' => $_POST['status'],
                    'added_date' => $added_date
                );

                $query = $this->masters->brand_update($data, $ids, $brand_name);
                $this->session->set_flashdata('status', "Brand Updated Successfully");
                redirect('brand');
            }
        } else {
            redirect('brand');
        }
    }

    function accessory_master_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/accessory_master", array(), TRUE);
        $this->datatables
                ->select("`car_accessory_id`,`name`, `price`, `description`,`added_date`")
                ->from("car_accessory")
                ->where("status", "1")
                ->add_column("Action", $actionLinkBar, '`car_accessory_id`');

        echo $this->datatables->generate();
    }

    function accessory_master() {
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();
        $data['variant'] = $this->product_data->get_all_variant();
//        print_r($data['variant']);
        $data['accessory_category'] = $this->common->get_accessory_category();
        $data['accessory_sub_category'] = $this->common->get_accessory_sub_category();
        $data['accessory_type'] = $this->common->get_accessory_type();
        $this->load->view('accessory', $data);
    }

    function upload_accessory_master() {
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();
//        print_r($data['brand']);
        $data['variant'] = $this->product_data->get_all_variant();
        $data['accessory_category'] = $this->common->get_accessory_category();
        $data['accessory_sub_category'] = $this->common->get_accessory_sub_category();
        $data['accessory_type'] = $this->common->get_accessory_type();
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('category', 'Category Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('price', 'Price', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('message', "validate");
                $this->session->set_flashdata('res', $_POST);
                $data['data_set'] = $_POST;
                $all_product_name = $_POST['all_product_name'];
                $res = '';
                foreach ($all_product_name as $all_product_name_data) {
                    $res = "'" . $all_product_name_data . "'," . $res;
                }
                $id = rtrim($res, ',');
                $data['acc_variant_data'] = $this->db->query("select pro_name_comp,variant_id from variant where variant_id IN ($id)")->result_array();
                $this->session->set_flashdata('acc_variant_data', $data['acc_variant_data']);
//                print_r($data['acc_variant_data']);
                $this->load->view('accessory', $data);
            } else {
                $image = '';
                if (!empty($_FILES['image']['name'])) {
                    $config2['upload_path'] = 'uploads/accessory/';
                    $config2['allowed_types'] = 'gif|jpg|png';
                    $config2['max_size'] = '5024';
                    $this->load->library('upload', $config2);
                    $image = $this->upload->do_uploads('image');
                    if ($image == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php
                        exit;
                    }
                }
                $data = array(
                    'category' => $_POST['category'],
                    'accessory_sub_cat' => $_POST['accessory_sub_cat'],
                    'accessory_type' => $_POST['cat_type'],
                    'image' => $image,
                    'name' => $_POST['name'],
                    'price' => $_POST['price'],
                    'discount_price' => $_POST['discount_price'],
                    'description' => $_POST['description'],
                    'stock' => $_POST['stock'],
                    'status' => $_POST['status'],
                    'type' => $_POST['product_type'],
                    'accessory_part_no' => $_POST['accessory_part_no'],
                    'added_date' => date('Y-m-d h:i:s')
                );

                $all_product_name = $_POST['all_product_name'];
                $name = addslashes($_POST['name']);
                $price = $_POST['price'];
                $brand_id = $_POST['brand_id'];
                $model_id = $_POST['model_id'];
                $percent = $_POST['percent'];
                $valid_until = $_POST['valid_until'];
                $product_type = $_POST['product_type'];
                $accessory_part_no = $_POST['accessory_part_no'];
                $res = $this->db->query("select car_accessory_id from `car_accessory` WHERE name='$name' and category='$category'")->result_array();
                if (count($res) == 0) {
                    $get_data = $this->db->insert('car_accessory', $data);
                    $this->session->set_flashdata('message', "success");
                    $insert_id = $this->db->insert_id();

//                    print_r($_POST['all_product_name']);
                    $final_variant = '';
                    foreach ($_POST['all_product_name'] as $all_product_name) {
                        $final_variant = "'" . $all_product_name . "'," . $final_variant;
                    }
                    $all_variant = rtrim($final_variant, ",");
                    $get_variant_data = $this->db->query("select variant_id,brand_id,model_id from variant where variant_id IN($all_variant)")->result_array();

                    foreach ($get_variant_data as $get_variant_data_res) {
//                        print_r($get_variant_data_res);
//                        exit;
                        $variant_id = $get_variant_data_res['variant_id'];
                        $model_id = $get_variant_data_res['model_id'];
                        $brand_id = $get_variant_data_res['brand_id'];
                        if (!empty($variant_id)) {
                            $all_product_name;
                            $get_data = $this->db->query("INSERT INTO `accessory_variant_dependency` VALUES "
                                    . "('','$insert_id','$variant_id','$brand_id','$model_id')");
                        }
                    }

                    $get_hotdeal = $this->db->query("INSERT INTO `accessory_hot_deals`"
                            . "(`acc_hot_deal_id`, `accessory_id`, `percent`, `valid_until`, `added_date`) "
                            . "VALUES ('','$insert_id','$percent','$valid_until',NOW())");
                } else {

                    $this->session->set_flashdata('message', "exist");
                }
            }
            redirect('accessory_master');
        } else if (isset($_POST['delete'])) {
            $id = $_POST['id'];
            $get_data = $this->db->query("update car_accessory set status='2' where car_accessory_id='$id'");
            $this->session->set_flashdata('message', "delete");
            redirect('accessory_master');
        } else if (isset($_POST['edit'])) {
            $id = $_POST['id'];
            $data['acc_data'] = $this->db->query("select * from `car_accessory` WHERE car_accessory_id='$id'")->row_array();
            $data['acc_variant_data'] = $this->db->query("select *,(select pro_name_comp from variant where variant_id=accessory_variant_dependency.variant_id) as pro_name_comp from `accessory_variant_dependency` WHERE accessory_id='$id'")->result_array();
            $data['acc_hot_deals'] = $this->db->query("select * from `accessory_hot_deals` WHERE accessory_id='$id'")->row_array();
//            print_r($data['acc_variant_data']);
//            redirect('affiliate');
            $this->load->view('accessory', $data);
        } else if (isset($_POST['Update'])) {
            $this->load->library('form_validation');
//            $this->form_validation->set_rules('brand_id', 'Brand Name', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('model_id', 'Model Name', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('all_product_name[]', 'Variant Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('category', 'Category Name', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('image', 'Image', 'trim|required|xss_clean');
            $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('price', 'Price', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('stock', 'Stock', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('message', "validate");
                $id = $_POST['acc_id'];
                $data['acc_data'] = $this->db->query("select * from `car_accessory` WHERE car_accessory_id='$id'")->row_array();
                $data['acc_variant_data'] = $this->db->query("select *,(select pro_name_comp from variant where variant_id=accessory_variant_dependency.variant_id) as pro_name_comp from `accessory_variant_dependency` WHERE accessory_id='$id'")->result_array();
                $this->load->view('accessory', $data);
            } else {

                if (!empty($_FILES['image']['name'])) {
//                    echo '1';
                    $config2['upload_path'] = 'uploads/accessory/';
                    $config2['allowed_types'] = 'gif|jpg|png';
                    $config2['max_size'] = '5024';
                    $this->load->library('upload', $config2);
                    $image = $this->upload->do_uploads('image');
                    if ($image == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php
                        exit;
                    }
                } else {
//                echo '2';    
                    $image = $_POST['image_name'];
                }
                $data = array(
                    'category' => $_POST['category'],
                    'accessory_sub_cat' => $_POST['accessory_sub_cat'],
                    'accessory_type' => $_POST['cat_type'],
                    'image' => $image,
                    'name' => $_POST['name'],
                    'price' => $_POST['price'],
                    'discount_price' => $_POST['discount_price'],
                    'description' => $_POST['description'],
                    'stock' => $_POST['stock'],
                    'status' => $_POST['status'],
                    'type' => $_POST['product_type'],
                    'accessory_part_no' => $_POST['accessory_part_no'],
                    'added_date' => date('Y-m-d h:i:s')
                );
                $id = $_POST['acc_id'];
                $all_product_name = $_POST['all_product_name'];
                $name = addslashes($_POST['name']);
                $price = $_POST['price'];
                $brand_id = $_POST['brand_id'];
                $model_id = $_POST['model_id'];
                $percent = $_POST['percent'];
                $valid_until = $_POST['valid_until'];
                $product_type = $_POST['product_type'];

                $res = $this->db->query("select car_accessory_id from `car_accessory` WHERE car_accessory_id='$id'")->result_array();
                if (count($res) > 0) {
                    $this->db->where('car_accessory_id', $id);
                    $this->db->update('car_accessory', $data);
                    $this->session->set_flashdata('message', "updated");
                    $car_accessory_id = $res[0]['car_accessory_id'];
                    $del = $this->db->query("delete from accessory_variant_dependency where accessory_id='$car_accessory_id'");
                    $final_variant = '';
                    foreach ($_POST['all_product_name'] as $all_product_name) {
                        $final_variant = "'" . $all_product_name . "'," . $final_variant;
                    }
                    $all_variant = rtrim($final_variant, ",");
                    $get_variant_data = $this->db->query("select variant_id,brand_id,model_id from variant where variant_id IN($all_variant)")->result_array();
//                    print_r($get_variant_data);
                    foreach ($get_variant_data as $get_variant_data_res) {
                        $variant_id = $get_variant_data_res['variant_id'];
                        $model_id = $get_variant_data_res['model_id'];
                        $brand_id = $get_variant_data_res['brand_id'];
                        if (!empty($variant_id)) {
                            $all_product_name;
                            $get_data = $this->db->query("INSERT INTO `accessory_variant_dependency` VALUES "
                                    . "('','$car_accessory_id','$variant_id','$brand_id','$model_id')");
                        }
                    }

                    $res_hot_deal = $this->db->query("SELECT `acc_hot_deal_id` FROM `accessory_hot_deals` WHERE `accessory_id`='$id'")->result_array();
                    if (count($res_hot_deal) == 0) {
                        $date = date('Y-m-d');
                        $upd_hotdeal = $this->db->query("INSERT INTO `accessory_hot_deals`"
                                . "(`acc_hot_deal_id`, `accessory_id`, `percent`, `valid_until`, `added_date`) VALUES "
                                . "('','$id','$percent','$valid_until','$date')");
                    } else {
                        $acc_hot_deal_id = $res_hot_deal[0]['acc_hot_deal_id'];
                        $upd_hotdeal = $this->db->query("UPDATE `accessory_hot_deals` SET "
                                . "`percent`='$percent',`valid_until`='$valid_until' WHERE `acc_hot_deal_id`='$acc_hot_deal_id'");
                    }
                } else {
                    $this->session->set_flashdata('message', "exist");
                }
            }

//            exit;

            redirect('accessory_master');
        } else {
            redirect('accessory_master');
        }
    }

    function affiliate() {
        $this->load->view('affiliate', $data);
    }

    function affiliate_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/affiliate_page_action", array(), TRUE);
        $this->datatables
                ->select("affiliate_id,user,code,email_id,comments,status,added_date")
                ->from("affiliate")
                ->add_column("Action", $actionLinkBar, 'affiliate_id');

        echo $this->datatables->generate();
    }

    function upload_affiliate() {
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('user', 'user', 'trim|required|xss_clean');
            $this->form_validation->set_rules('code', 'code', 'trim|required|xss_clean');
            $this->form_validation->set_rules('comments', 'comments', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status_data', 'status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email_id', 'email id', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('message', "validate");
                $id = $_POST['id'];
                if (!empty($id)) {
                    $data['affi_data'] = $this->db->query("select * from `affiliate` WHERE affiliate_id='$id'")->row_array();
                }
                $this->affiliate();
            } else {
                $data = array(
                    'user' => $_POST['user'],
                    'code' => $_POST['code'],
                    'status' => $_POST['status_data'],
                    'email_id' => $_POST['email_id'],
                    'comments' => $_POST['comments'],
                    'added_date' => date('Y-m-d h:i:s')
                );

                $code = $_POST['code'];
                $res = $this->db->query("select code from `affiliate` WHERE code='$code'")->result_array();
                if (count($res) == 0) {
                    $get_data = $this->db->insert('affiliate', $data);
                    $this->session->set_flashdata('message', "success");
                } else {
                    $this->session->set_flashdata('message', "exist");
                }
            }
            redirect('affiliate');
        } else if (isset($_POST['delete'])) {
            $id = $_POST['id'];
            $get_data = $this->db->query("DELETE FROM `affiliate` WHERE affiliate_id='$id'");
            $this->session->set_flashdata('message', "delete");
            redirect('affiliate');
        } else if (isset($_POST['edit'])) {
            $id = $_POST['id'];
            $data['affi_data'] = $this->db->query("select * from `affiliate` WHERE affiliate_id='$id'")->row_array();
//            print_r($data['affi_data']);
//            redirect('affiliate');
            $this->load->view('affiliate', $data);
        } else if (isset($_POST['Update'])) {
            $this->load->library('form_validation');
            $id = $_POST['affiliate_id'];
            $this->load->library('form_validation');
            $this->form_validation->set_rules('user', 'user', 'trim|required|xss_clean');
            $this->form_validation->set_rules('code', 'code', 'trim|required|xss_clean');
            $this->form_validation->set_rules('comments', 'comments', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status_data', 'status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email_id', 'email id', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('message', "validate");
                $this->affiliate();
//                $this->load->view('affiliate', $data);
            } else {

                $affiliate_id = $_POST['affiliate_id'];
                $user = $_POST['user'];
                $code = $_POST['code'];
                $comments = $_POST['comments'];
                $status = $_POST['status_data'];
                $email_id = $_POST['email_id'];
                $added_date = date('Y-m-d h:i:s');
                $this->session->set_flashdata('message', "updated");
                $get_data = $this->db->query("update affiliate set user='$user',comments='$comments',code='$code',status='$status',email_id='$email_id',added_date='$added_date' where affiliate_id='$affiliate_id'");
            }
            redirect('affiliate');
        } else {
            redirect('affiliate');
        }
    }

    function coupon_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/coupon_page_action", array(), TRUE);
        $this->datatables
                ->select("coupon_id,start_date,end_date,no_of_coupon,coupon_used,amount_percent,coupon_material,coupn_code,description,comment")
                ->from("coupon")
                ->add_column("Action", $actionLinkBar, 'coupon_id');

        echo $this->datatables->generate();
    }

    function coupon() {
        $data['city'] = $this->common->get_city();
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();
        $data['color'] = $this->product_data->get_color();
        $data['coupon'] = $this->product_data->get_coupon();
        $this->load->view('coupon', $data);
    }

    function upload_coupon_data() {
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
            $this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
            $this->form_validation->set_rules('no_of_coupon', 'No of coupon', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('amount_percent', 'Amount Percent', 'trim|required|xss_clean');
            $this->form_validation->set_rules('coupon_code', 'Coupon Code', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('apply_for', 'Apply For', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('message', "error");
                $this->coupon();
            } else {

                $exp_type = explode(',', $_POST['apply_type']);
                $type = implode("-", (array_unique($exp_type)));
                $type = rtrim($type, "-");
                $model_id = '';
                if ($_POST['all_product_name'] == '' && $_POST['model_id'] != '') {
                    $model_id = $_POST['model_id'];
                    $type = str_replace("2", "5", $type);
                }
                if (empty($_POST['city'])) {
                    $city = '';
                } else {
                    $city = $_POST['city'];
                }
                $data = array(
                    'start_date' => $_POST['start_date'],
                    'end_date' => $_POST['end_date'],
                    'no_of_coupon' => $_POST['no_of_coupon'],
                    'coupon_type' => $_POST['coupon_type'],
                    'coupon_material' => $_POST['material'],
                    'amount_percent' => $_POST['amount_percent'],
                    'coupn_code' => $_POST['coupon_code'],
                    'description' => $_POST['description'],
                    'comment' => $_POST['comment'],
                    'faq' => $_POST['faq'],
                    'city_id' => $city,
                    'variant_id' => $_POST['all_product_name'][0],
                    'brand' => $_POST['brand_id2'],
                    'model' => $model_id,
                    'type' => $type,
                    'added_date' => date('Y-m-d h:i:s'),
                    'color' => $_POST['color']
                );

                $get_data = $this->db->insert('coupon', $data);
//                print_r($data);
//                
//                exit;
                $this->session->set_flashdata('message', "Submited");
                redirect('coupon');
            }
        } else if (isset($_POST['delete'])) {
            $id = $_POST['id'];
            $get_data = $this->db->query("DELETE FROM `coupon` WHERE coupon_id='$id'");
            redirect('coupon');
        } else {
            redirect('coupon');
        }
    }

    function landing_page() {
        $data['model'] = $this->product_data->get_model();
        $data['brand'] = $this->product_data->get_brand();
        $data['city'] = $this->common->get_city();
        $this->load->view('landing_page', $data);
    }

    function landing_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/landing_page_action", array(), TRUE);
        $this->datatables
                ->select("landing_page_id,name,title,background_color,background_image,Description")
                ->from("landing_page")
                ->add_column("Action", $actionLinkBar, 'landing_page_id');

        echo $this->datatables->generate();
    }

    function delete_landing_data($id) {


        $get_data = $this->db->query("DELETE FROM `landing_page` WHERE landing_page_id='$id'");
        redirect('landing_page');
    }

    function upload_landing_data() {
//        $data['model'] = $this->product_data->get_model();
//        $data['brand'] = $this->product_data->get_brand();
//        $data['city'] = $this->masters->get_City();
        if (isset($_POST['submit'])) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('page_heading', 'page_heading', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('Email', 'Email', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('City', 'City', 'trim|required|xss_clean');
            $this->form_validation->set_rules('menu', 'menu', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('message', "error");
                $this->landing_page();
            } else {
                $image = '';
                if (!empty($_FILES['back_image']['name'])) {

                    $config2['upload_path'] = 'uploads/back_image/';
                    $config2['allowed_types'] = 'gif|jpg|png';
                    $config2['max_size'] = '2048';
                    $this->load->library('upload', $config2);
                    $image = $this->upload->do_uploads('back_image');
                    if ($image == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php
                        exit;
                    }
                }


                $main_image = '';
                if (!empty($_FILES['main_image']['name'])) {

                    $config2['upload_path'] = 'uploads/landing_main_image/';
                    $config2['allowed_types'] = 'gif|jpg|png';
                    $config2['max_size'] = '2048';
                    $this->load->library('upload', $config2);
                    $main_image = $this->upload->do_uploads('main_image');
                    if ($main_image == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php
                        exit;
                    }
                }
                $added_date = date('Y-m-d h:i:s');
                $text_fields = '';
                $button_fields = '';
                $icons = '';
                foreach ($_POST['text_fields'] as $selectedOption) {
                    $text_fields = $selectedOption . ',' . $text_fields;
                    $data2[] = $selectedOption;
                }
//                print_r($data2);
                $text_fields = implode(",", $data2);



                foreach ($_POST['button_fields'] as $selectedOption2) {
                    $button_fields = $selectedOption2 . '--' . $button_fields;
                }
//                print_r($button_fields);




                foreach ($_POST['icons'] as $selectedOption3) {
                    $icons = $selectedOption3 . ',' . $icons;
                }
                $data = array(
                    'name' => $_POST['name'],
                    'title' => $_POST['page_heading'],
                    'page_heading_color' => $_POST['page_heading_color'],
                    'page_heading_Size' => $_POST['page_heading_Size'],
                    'timmer' => $_POST['timmer'],
                    'background_color' => $_POST['back_color'],
                    'background_image' => $image,
                    'main_image' => $main_image,
                    'menu_status' => $_POST['menu'],
                    'main_video' => $_POST['main_video'],
                    'video_status' => $_POST['video_status'],
                    'Description' => $_POST['Description'],
                    'form_heading' => $_POST['form_heading'],
                    'testimonials_status' => $_POST['testimonials'],
                    'end_date' => $_POST['end_date'],
                    'text_fields' => $text_fields,
                    'button_fields' => $button_fields,
                    'icons' => $icons,
                    'added_date' => $added_date
                );
//                echo '<pre>';
//                print_r($data);
//                echo '</pre>';
//                exit;
                $get_data = $this->db->insert('landing_page', $data);
//                $data['results'] = $this->home_section->landing_data($data);
//                $query=$this->home_section->landing_data($data);
                $this->session->set_flashdata('message', "Submited");
                redirect('landing_page');
            }
        } else if (isset($_POST['Update'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('page_heading', 'page_heading', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('Email', 'Email', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('City', 'City', 'trim|required|xss_clean');
            $this->form_validation->set_rules('menu', 'menu', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->landing_page();
            } else {

                $image = '';
                if (!empty($_FILES['back_image']['name'])) {

                    $config2['upload_path'] = 'uploads/back_image/';
                    $config2['allowed_types'] = 'gif|jpg|png';
                    $config2['max_size'] = '2048';
                    $this->load->library('upload', $config2);
                    $image = $this->upload->do_uploads('back_image');
                    if ($image == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php
                        exit;
                    }
                } else {
                    $image = $_POST['back_image1'];
                }


                $main_image = '';
                if (!empty($_FILES['main_image']['name'])) {

                    $config2['upload_path'] = 'uploads/landing_main_image/';
                    $config2['allowed_types'] = 'gif|jpg|png';
                    $config2['max_size'] = '2048';
                    $this->load->library('upload', $config2);
                    $main_image = $this->upload->do_uploads('main_image');
                    if ($main_image == '') {
                        ?>
                        <script>
                            alert('wrong file type upload,please upload image');
                            history.go(-1);
                        </script>
                        <?php
                        exit;
                    }
                } else {
                    $main_image = $_POST['main_image1'];
                }


                $added_date = date('Y-m-d h:i:s');
                $text_fields = '';
                $button_fields = '';
                $icons = '';
                $data2 = array();
                foreach ($_POST['text_fields'] as $selectedOption) {
                    $text_fields = $selectedOption . ',' . $text_fields;
                    $data2[] = $selectedOption;
                }
//                print_r($data2);
                $text_fields = implode(",", $data2);
//                echo $text_fields;
//                exit;

                foreach ($_POST['button_fields'] as $selectedOption2) {
                    $button_fields = $selectedOption2 . '--' . $button_fields;
                }


                foreach ($_POST['icons'] as $selectedOption3) {
                    $icons = $selectedOption3 . ',' . $icons;
                }
                $main_id = $_POST['main_id'];

                $data = array(
                    'name' => $_POST['name'],
                    'title' => $_POST['page_heading'],
                    'page_heading_color' => $_POST['page_heading_color'],
                    'page_heading_Size' => $_POST['page_heading_Size'],
                    'timmer' => $_POST['timmer'],
//                    'email'=>$_POST['Email'],
//                    'city'=>$_POST['City'],
                    'background_color' => $_POST['back_color'],
                    'background_image' => $image,
                    'main_image' => $main_image,
                    'menu_status' => $_POST['menu'],
                    'main_video' => $_POST['main_video'],
                    'end_date' => $_POST['end_date'],
                    'video_status' => $_POST['video_status'],
                    'Description' => $_POST['Description'],
                    'form_heading' => $_POST['form_heading'],
                    'testimonials_status' => $_POST['testimonials'],
                    'text_fields' => $text_fields,
                    'button_fields' => $button_fields,
                    'icons' => $icons,
                    'added_date' => $added_date
                );


                $this->db->where('landing_page_id', $main_id);
                $this->db->update('landing_page', $data);
                $this->session->set_flashdata('message', "Updated");
                redirect('landing_page');
            }
        } else if (isset($_POST['edit'])) {
            $id = $_POST['id'];
            $data['query'] = $this->db->query("SELECT * FROM `landing_page` where landing_page_id='$id'")->row_array();
            $this->load->view('landing_page', $data);
//            redirect('landing_page');
        } else if (isset($_POST['delete'])) {

            $id = $_POST['id'];
            $get_data = $this->db->query("DELETE FROM `landing_page` WHERE landing_page_id='$id'");
            redirect('landing_page');
            $data['query'] = $this->db->query("SELECT * FROM `landing_page` where landing_page_id='$id'")->row_array();
            $this->load->view('landing_page', $data);
        } else {
            redirect('landing_page');
        }
    }

    function del_brand() {
        if (isset($_POST['delete'])) {
            $brandgrp_id = $this->input->post('brandgrp_id');
            $this->masters->del_brand($brandgrp_id);
            $this->session->set_flashdata('status', "Brand Deleted Successfully");
            redirect('brand');
        }
    }

    function get_brand() {
        if (isset($_POST['get'])) {
            $data['results_brand'] = $this->masters->get_brand_group_res();
            $brandgrp_id = $this->input->post('get_brandgrp_id');
            $data['cond'] = '1';
            $data['results_ups'] = $this->masters->get_brand($brandgrp_id);

            $this->load->view('brand', $data);
        } else {
            redirect('brand');
        }
    }

    /* Brand master data section start here */



    /* Information Page master data section start here */

    function information() {
        $data['results'] = $this->masters->info_data();
        $count = count($data['results']);
        if ($count > 0) {
            $this->load->view('information', $data);
        } else {
            $this->load->view('information');
        }
    }

    function info_upload() {

        #### used for update section here
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('info_title', 'Information Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('details', 'Description', 'trim|required');
            $this->form_validation->set_rules('sort_order', 'Sort Order', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->information();
            } else {

                if ($query = $this->masters->info_upload()) {
                    redirect('information');
                }
            }
        } else if (isset($_POST['delete'])) {
            $this->masters->del_info();
            redirect('information');
        } else if (isset($_POST['get'])) {
            $data['info_get'] = $this->masters->get_info();
            $this->load->view('information', $data);
        } else if (isset($_POST['update'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('info_title', 'Information Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('details', 'Description', 'trim|required');
            $this->form_validation->set_rules('sort_order', 'Sort Order', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {

                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->banner();
            } else {
                $query = $this->masters->info_update();
                redirect('information');
            }
        }


        #### used for else redirect to banner page
        else {
            redirect('information');
        }
    }

    /* Information Page master data section End here */


    /* Dealer Page master data section start here */



    /* Dealer Page master data section start here */


    /* Brand wise model record start here */

    function get_brand_wise_model() {
        $brand_id = $this->input->post('brand_id');
        $model_id = $this->input->post('model_id');
        $brand_wise_model = $this->masters->get_brand_wise_model($brand_id);
        ?>
        <select name="model_id" id="model_id" style="width:180px;" onchange="get_variant();">
            <option  value="">Select model</option>
            <?php
            foreach ($brand_wise_model as $brand_wise_model_data) {
                ?>
                <option <?php
                if ($model_id == $brand_wise_model_data['model_id']) {
                    echo 'selected';
                }
                ?>  value="<?php echo $brand_wise_model_data['model_id']; ?>"><?php echo $brand_wise_model_data['model_name']; ?></option>    
                    <?php
                }
                ?>
        </select>            
        <?php
    }

    function get_exp_brand_wise_model() {
        $brand_id = $this->input->post('brand_id');
        $model_id = $this->input->post('model_id');
        $brand_wise_model = $this->masters->get_brand_wise_model($brand_id);
        ?>
        <select name="model_id" id="model_id"  style="width: 340px;" onchange="get_exp_variant();">
            <option  value="">Select model</option>
            <?php
            if ($brand_id != '') {
                foreach ($brand_wise_model as $brand_wise_model_data) {
                    ?>
                    <option <?php
                    if ($model_id == $brand_wise_model_data['model_id']) {
                        echo 'selected';
                    }
                    ?>  value="<?php echo $brand_wise_model_data['model_id']; ?>"><?php echo $brand_wise_model_data['model_name']; ?></option>    
                        <?php
                    }
                }
                ?>
        </select>            
        <?php
    }

    function get_exp_model_wise_variant() {
        $model_id = $this->input->post('model_id');
        $brand_id = $this->input->post('brand_id');
        $variant_id = $this->input->post('variant_id');
        $brand_wise_model = $this->masters->get_model_wise_variant($model_id, $brand_id);
        ?>
        <select name="all_product_name" id="all_product_name"  style="width: 340px;">
            <option  value="">Select Variant</option>
            <option  value="All">All Variant</option>
            <?php
            foreach ($brand_wise_model as $brand_wise_model_data) {
                ?>
                <option <?php
                if ($brand_wise_model_data['variant_id'] == $variant_id) {
                    echo 'selected';
                }
                ?> value="<?php echo $brand_wise_model_data['variant_id']; ?>"><?php echo $brand_wise_model_data['pro_name']; ?></option>    
                    <?php
                }
                ?>
        </select>  


        <?php
    }

    function get_acc_brand_wise_model() {
        $brand_id = $this->input->post('brand_id');
        $model_id = $this->input->post('model_id');
        if ($brand_id > 0) {
            $brand_wise_model = $this->masters->get_brand_wise_model($brand_id);
        }
        ?>
        <select name="model_id" id="model_id" style="width: 340px;" onchange="get_acc_variant();">
            <option  value="">Select model</option>

            <?php
            $i = 1;
            foreach ($brand_wise_model as $brand_wise_model_data) {
                if ($i == 1) {
                    ?>
                    <option <?php
                    if ($model_id == 0) {
                        echo 'selected';
                    }
                    ?> value="0">All Model</option>
                        <?php
                    }
                    ?>

                <option <?php
                if ($model_id == $brand_wise_model_data['model_id']) {
                    echo 'selected';
                }
                ?>  value="<?php echo $brand_wise_model_data['model_id']; ?>"><?php echo $brand_wise_model_data['model_name']; ?></option>    
                    <?php
                    $i++;
                }
                ?>
        </select>            
        <?php
    }

    function get_model_wise_variant() {
        $model_id = $this->input->post('model_id');
        $brand_id = $this->input->post('brand_id');
        $variant_id = $this->input->post('variant_id');
        $brand_wise_model = $this->masters->get_model_wise_variant($model_id, $brand_id);
//        print_r($brand_wise_model);
        ?>
        <select name="all_product_name[]" id="all_product_name" style="width:180px;">
            <option  value="">Select Variant</option>
            <?php
            foreach ($brand_wise_model as $brand_wise_model_data) {
                ?>
                <option <?php
                if ($brand_wise_model_data['variant_id'] == $variant_id) {
                    echo 'selected';
                }
                ?> value="<?php echo $brand_wise_model_data['variant_id']; ?>"><?php echo $brand_wise_model_data['pro_name']; ?></option>    
                    <?php
                }
                ?>
        </select>  


        <?php
    }

    function get_acc_model_wise_variant() {
        $model_id = $this->input->post('model_id');
        $brand_id = $this->input->post('brand_id');
        $allvariants = explode("-", $this->input->post('allvariants'));
//        print_r($allvariants);
//        exit;
        $brand_wise_model = $this->masters->get_model_wise_variant($model_id, $brand_id);
        ?>
        <select name="all_product_name[]" id="s" style="width:170px;" multiple="multiple">
            <!--<option  value="">Select Variant</option>-->
            <?php
            foreach ($brand_wise_model as $brand_wise_model_data) {
                if (in_array($brand_wise_model_data['variant_id'], $allvariants)) {
                    
                } else {
                    ?>
                    <option value="<?php echo $brand_wise_model_data['variant_id']; ?>"><?php echo $brand_wise_model_data['pro_name_comp']; ?></option>    
                    <?php
                }
            }
            ?>
        </select>  


        <?php
    }

    /* Brand wise model record end here */

    function admin_login() {
        $this->load->model('common');
        $username = trim($this->input->post('username'));
        $password = trim($this->input->post('password'));
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
            if (!$this->form_validation->run()) {
                $this->index();
            } else {
                $query = $this->common->admin_authentication($username, $password, $cond = '1', '');
                if (count($query) > 0) {
                    $cond = array('logged_in' => 'TRUE');
                    $res_status = array_merge($query, $cond);
                    $this->session->set_userdata("logged_in_admin_user", $res_status);
                    $session = $this->session->userdata("logged_in_user");

                    $query = $this->common->admin_authentication($username, $password, $cond = '2', $query['login_id']);
                    $this->session->set_flashdata('user_login_sucess', "login_sucess");
                    redirect(base_url('home'));
                } else {
                    $this->session->set_flashdata('login_failed', "login_failed");
                    redirect(base_url());
                }
            }
        }
    }

    function log_out() {
        $userdata = $this->session->userdata('logged_in_admin_user');
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('logged_in_admin_user');
        $this->session->sess_destroy();
        redirect(base_url());
    }

    function download_pdf($order_id = '', $cond_id) {
        $this->load->model('home_section');
        $order_wise_detail = $this->home_section->order_wise_detail($order_id);
        $id = $order_wise_detail[0]['product_id'];
        $data['id'] = $order_wise_detail[0]['product_id'];
        $data['main_id'] = $order_wise_detail[0]['product_id'];
        $data['city_name'] = $order_wise_detail[0]['city_name'];
        $user_id = $order_wise_detail[0]['user_id'];
        $on_road_price = $order_wise_detail[0]['on_road_price'];
        $dis_price = $order_wise_detail[0]['dis_price'];
        $data['order_result'] = array('city_name' => $data['city_name'], 'on_road_price' => $on_road_price, 'dis_price' => $dis_price);
        $data['car_into_details'] = $this->home_section->car_intro_detail($data['id']);

        $pro_name_comp = $data['car_into_details'][0]['pro_name_comp'];
        $city_name = $data['order_result']['city_name'];

        $data['customer_details'] = $this->home_section->customer_details($user_id);
        $email = $data['customer_details'][0]['email'];
        $firstname = $data['customer_details'][0]['firstname'];
        $lastname = $data['customer_details'][0]['lastname'];
        $data['registeration_details'] = $this->home_section->registeration_details($user_id);
        $data['car_configuration'] = $this->home_section->car_configuration($user_id, $id, $cond = '2');
        $invoice_no = $data['car_configuration'][0]['invoice_no'];

        $data['emi'] = $this->home_section->car_emi($id, $user_id);
        $data['booking_varaint'] = $this->home_section->booking_detail($data['id'], $user_id, $cond = '1');
        $data['booking_exterior'] = $this->home_section->booking_detail($data['id'], $user_id, $cond = '2');
        $data['booking_interior'] = $this->home_section->booking_detail($data['id'], $user_id, $cond = '3');
        $data['booking_accessory'] = $this->home_section->booking_detail($data['id'], $user_id, $cond = '4');
        require_once APPPATH . '/third_party/Mpdf/mpdf.php';
        $mpdf = new mPDF('c', array(220, 150), '', '', 0, 0, 0, 0, 0, 0);
        $html = $this->load->view('test', $data, TRUE);
        if ($cond_id == 3) {
            $mpdf = new mPDF('c', 'A4', '', '', 0, 0, 0, 0, 0, 0);
            $mpdf->WriteHTML($html);
            $pdf_name = 'MyNewCar-Invoice' . date('Y-m-d-H-i-s') . '-' . $user_id . '-' . $data['id'] . '.pdf';
            $pdf_name = 'MyNewCar-Invoice' . date('Y-m-d-H-i-s') . '-' . $user_id . '-' . $data['id'] . '.pdf';
            $pdfFilePath = FCPATH . "uploads/car_con_download/$pdf_name";
            $link = APPPATH . "../uploads/car_con_download/$pdf_name";
            $uploadPath = $pdf_name;
            $mpdf->Output($pdfFilePath, 'F');
//                $datas = $this->input->post();
            $fileContent = @file_get_contents($link);
            $encodedContent = @base64_encode($fileContent);
            $mandrill_api_key = Mandrill_key;
            $logo = base_url() . 'uploads/logo.png';

            $html_data = <<<END
                        <div align="left" style="width:100%;">
                        <div style="float:left;">   
                        Dear Mr/Ms. $firstname,<br><br>

                        Greetings of the Day!!<br><br>

                        Thank you for booking your new $pro_name_comp in $city_name on MYNEWCAR.in. We hereby acknowledge your Booking Order No. $invoice_no.<br><br>
                        
			Please find attached the booking confirmation for your reference.<br><br>

                        Our Customer Care will get in touch with you within the next 48 hours to confirm the delivery details along with offers on other related services and sories requested by you.<br><br>    
                            
                        Regards,<br>
Team MYNEWCAR.in<br>
<img src="$logo" style="width:30%;"/><br>

Phone: 022 45 02 03 04 | Email: contact@mynewcar.in<br>
www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br><br>
---------------------------------------------------------------------------------------------------------------------------------
<br><br>
DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>
Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>
Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>        
                                
                                
                        </div>        
                        </div> 
                        <br><br>
    

END;

            $datas = array(
                "recipients" => array(
                    array('address' => array('email' => $email)),
                    array('address' => array('email' => 'contact@mynewcar.in')),
                    array('address' => array('email' => 'sbhuia@mynewcar.in')),
                    array('address' => array('email' => 'contact.mynewcar@gmail.com')),
                    array('address' => array('email' => 'dinesh@projectheena.com')),
                    array('address' => array('email' => 'mvasudev@mynewcar.in'))
                ),
                "content" => array(
                    "from" => 'account@mynewcar.in',
                    "return_path" => 'account@mynewcar.in',
                    'subject' => 'MYNEWCAR.IN Booking Confirmation - ' . $firstname . ' ' . $lastname,
                    'html' => $html_data,
                    'text' => strip_tags($html_data),
                    'attachments' => array(
                                array(
                                    'type' => "application/pdf",
                                    'data' => $encodedContent,
                                    'name' => ' Booking Confirmation ' . $invoice_no . '.pdf',
                                )
                            )
                )
            );
//            print_r($datas);
            $status = sendMailViaSparkPost(json_encode($datas));
            ?>
            <script>
                alert("Invoice Send Successfully");
                history.go(-1);
            </script>            
            <?php
        } else if ($cond_id == 2) {
            echo $html;
            exit;
        } else {
            $mpdf->WriteHTML($html);
            $pdf_name = 'config_' . date('Y-m-d') . '-' . $user_id . '-' . $data['id'] . '.pdf';
            $path = base_url() . 'config_download/';
            $uploadPath = $pdf_name;
            $mpdf->Output($uploadPath, 'D');
        }
    }

    function download_upcoming_pdf($order_id = '', $cond_id) {
        $this->load->model('home_section');
        if (!empty($order_id)) {
            $configurator_id = $order_id;
            $upcoming_details = $this->home_section->upcoming_configuration_details($configurator_id);
            $data['upcoming_details'] = $upcoming_details;
            $user_id = $upcoming_details['user_id'];
            $data['invoice_no'] = $upcoming_details["invoice_no"];
            $data['customer_details'] = $this->home_section->customer_details($user_id);
            $order_email = $data['customer_details'][0]['email'];
            $data['registeration_details'] = $this->home_section->registeration_details($user_id);
            $name = $data['customer_details'][0]['firstname'] . ' ' . $data['customer_details'][0]['lastname'];
            if (!empty($data['invoice_no'])) {
                $logo = base_url() . 'uploads/logo.png';
                $html_data = <<<END
                        <div align="left" style="width:100%;">
                        <div style="float:left;">   
                        Dear Mr/Ms. $name,<br><br>

                        Greetings of the Day!!<br><br>

                        Thank you for booking your new $upcoming_details[product_name] in $upcoming_details[city_name] on MYNEWCAR.in. We hereby acknowledge your Booking Order No. $upcoming_details[invoice_no].<br><br>
                        
			Please find attached the booking confirmation for your reference.<br><br>

                        Our Customer Care will get in touch with you within the next 48 hours to confirm the delivery details along with offers on other related services and sories requested by you.<br><br>    
                            
                        Regards,<br>
Team MYNEWCAR.in<br>
<img src="$logo" style="width:30%;"/><br>

Phone: 022 45 02 03 04 | Email: contact@mynewcar.in<br>
www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br><br>
---------------------------------------------------------------------------------------------------------------------------------
<br><br>
DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>
Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>
Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>        
                                
                                
                        </div>        
                        </div> 
                        <br><br>
    

END;

                $html = $this->load->view('upcoming_invoice', $data, TRUE);

                require_once APPPATH . '/third_party/Mpdf/mpdf.php';
                $mpdf = new mPDF('c', 'A4', '', '', 0, 0, 0, 0, 0, 0);
                $mpdf->WriteHTML($html);
                $pdf_name = 'MyNewCar-Upcoming-Invoice' . date('Y-m-d-H-i-s') . '.pdf';
                $pdf_name = 'MyNewCar-Upcoming-Invoice' . date('Y-m-d-H-i-s') . '.pdf';
                $pdfFilePath = FCPATH . "uploads/upcoming/$pdf_name";
                $link = APPPATH . "../uploads/upcoming/$pdf_name";
                $uploadPath = $pdf_name;
                $mpdf->Output($pdfFilePath, 'F');
                $datas = $this->input->post();
                $fileContent = @file_get_contents($link);
                $encodedContent = @base64_encode($fileContent);
                $id = $_GET['id'];
                if ($cond_id == 1) {
                    $pdf_name = 'MyNewCar-Upcoming-Invoice' . date('Y-m-d') . '.pdf';
                    $path = base_url() . 'config_download/';
                    $uploadPath = $pdf_name;
                    $mpdf->Output($uploadPath, 'D');
                } else {
                    $data = array(
                        "recipients" => array(
                            array('address' => array('email' => $order_email)),
                            array('address' => array('email' => 'contact@mynewcar.in')),
                            array('address' => array('email' => 'sbhuia@mynewcar.in')),
                            array('address' => array('email' => 'contact.mynewcar@gmail.com')),
                            array('address' => array('email' => 'dinesh@projectheena.com')),
                            array('address' => array('email' => 'mvasudev@mynewcar.in'))
                        ),
                        "content" => array(
                            "from" => 'account@mynewcar.in',
                            "return_path" => 'account@mynewcar.in',
                            'subject' => 'MYNEWCAR.IN Booking Confirmation - ' . $firstname . ' ' . $lastname,
                            'html' => $html_data,
                            'text' => strip_tags($html_data),
                            'attachments' => array(
                                array(
                                    'type' => "application/pdf",
                                    'data' => $encodedContent,
                                    'name' => ' Booking Confirmation ' . $invoice_no . '.pdf',
                                )
                            )
                        )
                    );
                    $status = sendMailViaSparkPost(json_encode($data));
                    ?>
                    <script>
                        alert("Invoice Sent Sucessfully");
                        history.go(-1);
                    </script>
                    <?php
                }
            }
        }
    }
    
    function download_accessory_pdf($order_id = '', $cond_id) {
        $this->load->model('home_section');
        if (!empty($order_id)) {
            $configurator_id = $order_id;
            $data['accessory_details']=$this->home_section->accessory_configuration_details($configurator_id);
            
            $config_detail = "select * from accessory_configuration where accessory_configuration_id='$configurator_id'";
            $config_details = $this->db->query($config_detail)->row_array();
            
            $data['acc_config'] = $config_details;
            $user_id = $config_details['user_id'];
            $data['invoice_no'] = $config_details["invoice_no"];
            $data['customer_details'] = $this->home_section->acc_customer_details($user_id, $configurator_id);
            
            $data['user_details'] = $this->home_section->customer_details($user_id);
            $name = $data['user_details'][0]['firstname'];
            $order_email = $data['customer_details'][0]['email'];
            if(!empty($data['invoice_no'])) {
                $logo = base_url() . 'uploads/logo.png';
                $html_data = <<<END
                        <div align="left" style="width:100%;">
                        <div style="float:left;">   
                        Dear Mr/Ms. $name,<br><br>

                        Greetings of the Day!!<br><br>

                        Thank you for booking your new accessory  on MYNEWCAR.in. We hereby acknowledge your Booking Order No. $data[invoice_no].<br><br>
                        
			Please find attached the booking confirmation for your reference.<br><br>

                        Our Customer Care will get in touch with you within the next 48 hours to confirm the delivery details along with offers on other related services and accessories requested by you.<br><br>    
                            
                        Regards,<br>
Team MYNEWCAR.in<br>
<img src="$logo" style="width:30%;"/><br>

Phone: 022 45 02 03 04 | Email: contact@mynewcar.in<br>
www.mynewcar.in | Visit Us on Facebook or Follow Us on Twitter<br><br>
---------------------------------------------------------------------------------------------------------------------------------
<br><br>
DREAMZ MYNEWCAR INDIA Pvt. Ltd. <br>
Registered & Corp. Office: Plot 669, Sector 23, Gurgaon, NCR - Delhi 122017, India.<br>
Proprietary and confidential. Distribution only by expressed authority of DREAMZ MYNEWCAR INDIA Pvt. Ltd.<br>        
                                
                                
                        </div>        
                        </div> 
                        <br><br>
    

END;
                
                $html_data;
                $acc_invoice = $this->input->cookie('acc_invoice', TRUE);
                
                
                $html = $this->load->view('accessory_invoice', $data, TRUE);
                
                require_once APPPATH . '/third_party/Mpdf/mpdf.php';
                $mpdf = new mPDF('c', 'A4', '', '', 0, 0, 0, 0, 0, 0);
                $mpdf->WriteHTML($html);
                $pdf_name = 'MyNewCar-Accessory-Invoice' . date('Y-m-d-H-i-s') . '.pdf';
                $pdf_name = 'MyNewCar-Accessory-Invoice' . date('Y-m-d-H-i-s') . '.pdf';
                $pdfFilePath = FCPATH . "uploads/accessory/$pdf_name";
                $link = APPPATH . "../uploads/accessory/$pdf_name";
                $uploadPath = $pdf_name;
                $mpdf->Output($pdfFilePath, 'F');
                $datas = $this->input->post();
                $fileContent = @file_get_contents($link);
                $encodedContent = @base64_encode($fileContent);

                if ($cond_id==1) {
                    $pdf_name = 'MyNewCar-Accessory-Invoice' . date('Y-m-d') . '.pdf';
                    $path = base_url() . 'config_download/';
                    $uploadPath = $pdf_name;
                    $mpdf->Output($uploadPath, 'D');
                } else {
                    $data = array(
                        "recipients" => array(
                            array('address' => array('email' => $order_email)),
                        ),
                        "content" => array(
                            "from" => 'account@mynewcar.in',
                            "return_path" => 'account@mynewcar.in',
                            'subject' => 'MYNEWCAR.IN Booking Confirmation - ' . $firstname . ' ' . $lastname,
                            'html' => $html_data,
                            'text' => strip_tags($html_data),
                            'attachments' => array(
                                array(
                                    'type' => "application/pdf",
                                    'data' => $encodedContent,
                                    'name' => ' Booking Confirmation ' . $invoice_no . '.pdf',
                                )
                            )
                        )
                    );
                    $status = sendMailViaSparkPost(json_encode($data));
                    ?>
                    <script>
                        alert("Invoice Sent Sucessfully");
                        history.go(-1);
                    </script>
                    <?php

                }
                $ii = $ii + 1;
            }
            
        }
    }

    function onroad_calculation($pro_id, $user_id) {

        $onroad_price_calculation = $this->home_section->onroad_price_calculation($pro_id, $user_id, $cond = '1');
        $accessory_price_calculation = $this->home_section->onroad_price_calculation($pro_id, $user_id, $cond = '2');
        return $onroad_price_calculation . '-' . $accessory_price_calculation;
    }

    function pro_price_calculation($id, $user_id) {

        $data['exshowroom_price'] = $this->home_section->car_intro_exshowroom_price($id);
        $data['additional_info_details'] = $this->home_section->additional_info($id, $user_id);
        $data['accessory_details'] = $this->home_section->accessory_details_res($id, $user_id);
        $data['onroad_calculation'] = $this->home_section->onraod_calculation($user_id);
        return $exp_show_price = array("exshowroom_price" => $data['exshowroom_price'], "additional_config" => $data['additional_info_details'], "accessory_details" => $data['accessory_details'], "onroad_calculation" => $data['onroad_calculation']);
    }

    function getcity() {
        $state_data = trim($this->input->post('state_data'));
        $city_data = $this->db->query("SELECT * FROM `city` where `state_id`='$state_data'")->result_array();
        ?>
        <select style="width:335px;" id="city" name="city">
            <option value="">Select City</option>    
            <?php
            foreach ($city_data as $city_data_res) {
                ?>
                <option value="<?php echo $city_data_res['city_id']; ?>"><?php echo $city_data_res['name']; ?></option>    
                <?php
            }
            ?>
        </select>
        <?php
    }

    function get_feature_name() {
        $id_data = $this->input->post('id_data');
        $q = "SELECT * FROM `feature_types` where `feature_category_id`='$id_data'";
        $city_data = $this->db->query($q)->result_array();
        ?>
        <select data-placeholder="Click to Choose..." name="feature_name" id="feature_name" style="width:230px;">
            <option value="">Select Feature</option>                                                                                                                                                                        
            <?php
            foreach ($city_data as $city_data_res) {
                ?>
                <option value="<?php print_r($city_data_res['featured_sub_type']); ?>"><?php print_r($city_data_res['featured_sub_type']); ?></option>    
                <?php
            }
            ?>
        </select>                            
        <?php
    }

    function slider() {
        $data['city'] = $this->masters->get_City();
        $data['brand'] = $this->product_data->get_brand();
        $data['all_car_type'] = $this->masters->all_car_type();
//            print_r($data['all_car_type']);
        $this->load->view('slider', $data);
    }

    function slider_delete_data() {
        $mul_slider_id = explode(",", $this->input->get('id'));

        foreach ($mul_slider_id as $mul_slider_id_data) {
            $slider_id = $mul_slider_id_data;
            $insert = $this->db->query("delete from slider where slider_id='$slider_id'");
        }
        redirect('slider_data');
    }

    function slider_upload_data() {

        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('profiles', 'profile', 'callback_validate_image');
            $this->form_validation->set_rules('city', 'city', 'required|trim');
//                $this->form_validation->set_rules('link', 'link', 'required|trim');
            $this->form_validation->set_rules('sort', 'sort', 'required|trim');
            if (!$this->form_validation->run()) {
                $this->slider();
            } else {

                $config2['upload_path'] = 'uploads/slider/';
                $config2['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                $config2['max_size'] = '5200';
                $this->load->library('upload', $config2);
                $image = $this->upload->do_uploads('profiles');

                if (empty($image)) {
                    ?>
                    <script>
                        alert("File Can't be uploaded,file size less than 500KB..!");
                        window.location.href = "<?php base_url() . 'slider_data'; ?>";
                    </script>
                    <?php
                } else {
                    $added_date = date('Y-m-d h:i:s');
                    $city = $_POST['city'];
                    if ($city == 'All') {
                        $select = $this->db->query('select name from city')->result_array();
                    } else {
                        $select = array(array("name" => $city));
                    }

                    foreach ($select as $select_city) {
                        $city = $select_city['name'];
                        $link = $_POST['link'];
                        $sort = $_POST['sort'];
                        $app_type = $_POST['app_type'];
                        $link_type = $_POST['link_type'];
                        if ($link_type == 'product') {
                            $app_data = $_POST['all_product_name'];
                            $app_data = $app_data[0];
                        } else if ($link_type == 'href') {
                            $app_data = $_POST['app_data'];
                        } else if ($link_type == 'hot_deal') {
                            $app_data = '';
                            $hot_deal_brand_id = $_POST['hot_deal_brand_id'];
                            $body_type = $_POST['body_type'];
                            if ($hot_deal_brand_id != '') {
                                $app_data.='brand-' . $hot_deal_brand_id . '|';
                            }
                            if ($body_type != '') {
                                $app_data.='body_type-' . $body_type . '|';
                            }
                            $app_data = rtrim($app_data, "|");
                        }
//                       print_r($app_data); 
//                        exit;

                        $insert = $this->db->query("INSERT INTO `slider`(`slider_id`, `city`, `image_name`, `added_date`, `link`, `sort`, `type`, `app_type`,"
                                . " `link_type`, `app_data`, `status`) "
                                . "VALUES ('','$city','$image','$added_date','$link',"
                                . "'$sort','','$app_type','$link_type','$app_data','1')");
                    }
                    redirect('slider_data');
                }
            }
        } else if (isset($_POST['delete'])) {
            $slider_id = $_POST['slider_id'];
            $insert = $this->db->query("delete from slider where slider_id='$slider_id'");
            redirect('slider_data');
        } else {
            redirect('slider_data');
        }
    }

    function slider_result_data() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/slider_all_action", array(), TRUE);
        $this->datatables
                ->select("slider_id,city,image_name,link,app_type,sort,added_date")
                ->from("slider")
                ->add_column("Action", $actionLinkBar, 'slider_id');
        echo $this->datatables->generate();
    }

}
