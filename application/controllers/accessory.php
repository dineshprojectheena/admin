<?php

/**
 * @author Nikhil Kataria <nikhil@projectheena.com>
 * @desc contains all accessory related functionalities
 * 
 */
class Accessory extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('accessory_model');
        $this->load->model('product_data');
    }

    function index() {
        $this->load->library('form_validation');
        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }

        $data['all_accessories'] = $this->accessory_model->get_all_accessory();
        $data['content'] = 'accessory/accessory';
        $this->load->view('index', $data);
    }

    function accessory_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/accessory_all_action", array(), TRUE);
        $this->datatables
                ->select("accessory_seller.accessory_seller_id,accessory_seller.name,accessory_seller.contact_person_emailid,accessory_seller.contact_person_telephoneno,"
                        . "(select brand_name from brand where brand_id=accessory_seller.accessory_brand) as brand_name,(select name from city where city_id=accessory_seller.city_id) as city_name")
                ->from("accessory_seller")
                ->add_column("Action", $actionLinkBar, 'accessory_seller.accessory_seller_id');

        echo $this->datatables->generate();
    }

    function all_accessory_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/accessory_action", array(), TRUE);
        $this->datatables
                ->select("accessory_id,accessory_name,accessory_information")
                ->from("accessory")
                ->add_column("Action", $actionLinkBar, 'accessory_id');

        echo $this->datatables->generate();
    }

    function create_accessory() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="help-block">', '</div>');
        $this->form_validation->set_rules('accessory_name', 'Accessory Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('accessory_cat', 'Accessory Category', 'trim|required|xss_clean|integer|greater_than[0]|less_than[9]');
        $this->form_validation->set_rules('accessory_sub_cat', 'Accessory Sub-Category', 'trim|required|xss_clean|integer|greater_than[0]|less_than[18]');
        $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean|integer|exact_length[1]');
        $this->form_validation->set_rules('accessory_information', 'Accessory Information', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (!empty($_FILES['accessory_img']['name'])) {
                $config2['upload_path'] = 'uploads/accessory/';
                $config2['allowed_types'] = 'gif|jpg|png';
                $config2['max_size'] = '2048';
                $this->load->library('upload', $config2);
                $img_name = $this->upload->do_uploads('accessory_img');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }





            $data = array(
                'accessory_name' => $this->input->post('accessory_name'),
                'accessory_img' => $img_name,
                'accessory_cat' => $this->input->post('accessory_cat'),
                'accessory_sub_cat' => $this->input->post('accessory_sub_cat'),
                'status' => $this->input->post('status'),
                'accessory_information' => $this->input->post('accessory_information'),
                'added_date' => date('Y-m-d H:i:s')
            );

            $this->accessory_model->create_accessory($data);

            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                $this->session->set_flashdata('success_msg', 'Accessory Successfully Created');
            } else {
                $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
            }
            redirect('accessory');
        } else {
            $this->index();
        }
    }

    function _get_dependency_array($postive_dependencies, $negative_dependencies, $accessory_id) {
        $dependent_array = array();
        if (!empty($postive_dependencies)) {

            $postive_dependencies = explode(',', $postive_dependencies);
            foreach ($postive_dependencies as $p) {
                $temp_array = array(
                    'accessory_id' => $accessory_id,
                    'dependent_accessory_id' => $p,
                    'dependency_type' => 1
                );

                array_push($dependent_array, $temp_array);
            }
        }
        if (!empty($negative_dependencies)) {
            $negative_dependencies = explode(',', $negative_dependencies);
            foreach ($negative_dependencies as $n) {
                $temp_array = array(
                    'accessory_id' => $accessory_id,
                    'dependent_accessory_id' => $n,
                    'dependency_type' => 0
                );

                array_push($dependent_array, $temp_array);
            }
        }

        return $dependent_array;
    }

    function delete_accessory() {
        $accessory_id = $this->input->post('accessory_id');
        $result_json;
        $this->db->trans_start();
        $this->accessory_model->delete_accessory($accessory_id);
        $this->accessory_model->delete_accessory_dependencies($accessory_id);
        $this->db->trans_complete();
        if ($this->db->trans_status()) {
            $result_json = array(
                'sts' => 1,
                'msg' => ''
            );
        } else {
            $result_json = array(
                'sts' => 0,
                'msg' => TRANSACTION_ERROR_MSG
            );
        }
        echo json_encode($result_json);
    }

    function update_accessory() {
        $this->load->library('form_validation');
        $accessory_id = $this->input->post('accessory_id');
        $data['accessory_details'] = $this->accessory_model->get_accessory_details($accessory_id);
        $data['positive_dependencies'] = json_encode($this->accessory_model->get_accessory_dependencies($accessory_id, 1));
        $data['negative_dependencies'] = json_encode($this->accessory_model->get_accessory_dependencies($accessory_id, 0));

        $data['content'] = 'accessory/edit_accessory';
        $this->load->view('index', $data);
    }

    function edit_accessory() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="help-block">', '</div>');
        $this->form_validation->set_rules('accessory_name', 'Accessory Name', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('accessory_type', 'Accessory Type', 'trim|required|xss_clean|integer|greater_than[0]|less_than[3]');
        $this->form_validation->set_rules('accessory_cat', 'Accessory Category', 'trim|required|xss_clean|integer|greater_than[0]|less_than[7]');
//        $this->form_validation->set_rules('accessory_sub_cat', 'Accessory Sub-Category', 'trim|required|xss_clean|integer|greater_than[0]|less_than[16]');
        $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean|integer|exact_length[1]');
//        $this->form_validation->set_rules('accessory_price', 'Accessory Price', 'trim|required|integer|is_natural_no_zero|xss_clean');
        $this->form_validation->set_rules('accessory_information', 'Accessory Information', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (!empty($_FILES['accessory_img']['name'])) {
                $config2['upload_path'] = 'uploads/accessory/';
                $config2['allowed_types'] = 'gif|jpg|png';
                $config2['max_size'] = '2048';
                $this->load->library('upload', $config2);
                $img_name = $this->upload->do_uploads('accessory_img');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
            }
            if ($img_name == '') {
                $img_name = $this->input->post('feature_img2');
            }


            $data = array('accessory_id' => $this->input->post('accessory_id'),
                'accessory_name' => $this->input->post('accessory_name'),
                'accessory_img' => $img_name,
                'accessory_cat' => $this->input->post('accessory_cat'),
                'accessory_sub_cat' => $this->input->post('accessory_sub_cat'),
                'status' => $this->input->post('status'),
                'accessory_price' => $this->input->post('accessory_price'),
                'accessory_information' => $this->input->post('accessory_information'),
                'added_date' => date('Y-m-d H:i:s'));

//            $postive_dependencies = $this->input->post('positive_dependencies');
//            $negative_dependencies = $this->input->post('negative_dependencies');
//            
//            $dependent_array = $this->_get_dependency_array($postive_dependencies, $negative_dependencies, $data['accessory_id']);
//            
//            $this->db->trans_start();

            $this->accessory_model->update_accessory($data, $data['accessory_id']);
//            $this->accessory_model->delete_previous_accessory_dependency($data['accessory_id']);
//            if (!empty($dependent_array)) {
//                $this->accessory_model->insert_accessory_dependency($dependent_array);
//            }
//            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                $this->session->set_flashdata('success_msg', 'Accessory Updated Successfully');
            } else {
                $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
            }
            redirect('accessory');
        } else {
            $this->update_accessory();
        }
    }

    function get_accessory() {
        $query = $_GET['q'];
        $result = $this->accessory_model->get_accessory($query);
        echo json_encode($result);
    }

    function map_accessory() {
        $this->load->library('form_validation');
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();
        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }

//        $data['all_features'] = $this->accessory_model->get_all_map_accessory();

        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }

        $data['content'] = 'accessory/map_accessory';
        $this->load->view('index', $data);
    }

    function all_map_accessory_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/accessory_all_map_action", array(), TRUE);
        $this->datatables
                ->select("id,(select pro_name from variant where variant_id=accessory_variant_mapping.variant_id) as pro_name,(select accessory_name from accessory where accessory_id=accessory_variant_mapping.accessory_id) as accessory_name,price")
                ->from("accessory_variant_mapping")
                ->add_column("Action", $actionLinkBar, 'id');

        echo $this->datatables->generate();
    }

    function get_all_accessory() {
        $variant_id = $this->input->post('product_id');
        $data['exterior_accessory'] = $this->accessory_model->get_all_accessory_features($cate1 = '1');
        $data['interior_accessory'] = $this->accessory_model->get_all_accessory_features($cate1 = '2');
        $data['safety_accessory'] = $this->accessory_model->get_all_accessory_features($cate1 = '3');
        $data['audio_accessory'] = $this->accessory_model->get_all_accessory_features($cate1 = '4');
        $data['services_accessory'] = $this->accessory_model->get_all_accessory_features($cate1 = '5');
        $data['equipment_accessory'] = $this->accessory_model->get_all_accessory_features($cate1 = '6');
        $data['ser_accessory'] = $this->accessory_model->get_all_accessory_features($cate1 = '7');
//        $data['variant_features']=$this->accessory_model->get_variant_features($variant_id);
//        $data['features'] = $this->feature_model->get_all_features();
        echo $this->load->view('ajaxfiles/all_accessory', $data, TRUE);
    }

    function insert_accessory() {

        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="help-block">', '</div>');
        $this->form_validation->set_rules('all_product_name', 'Variant', 'trim|required|xss_clean');
        $this->form_validation->set_rules('feature_names', 'Feature Names', 'trim|required|xss_clean');
        $this->form_validation->set_rules('feature_ids', '', 'trim|required|xss_clean');
//         print_r($insert_array);
//            exit;

        if ($this->form_validation->run()) {
            $variant_id = $this->input->post('all_product_name');
            $feature_ids = $this->input->post('feature_ids');
            $feature_id_array = explode(',', $feature_ids);
            $last_element_index = count($feature_id_array) - 1;

            if (empty($feature_id_array[$last_element_index])) {
                unset($feature_id_array[$last_element_index]);
            }

            $insert_array = array();
            $update_array = array();
            $temp_array = array();
            foreach ($feature_id_array as $f) {
                $temp_array = array(
                    'accessory_id' => $f,
                    'status' => 1,
                    'price' => $this->input->post('price'),
                    'variant_id' => $variant_id
                );
                array_push($insert_array, $temp_array);
                $temp_array['status'] = 0;
                array_push($update_array, $temp_array);
            }

            $this->accessory_model->create_accessory_variant_mapping($insert_array);

            $this->db->trans_complete();
            if ($this->db->trans_status()) {
                $this->session->set_flashdata('success_msg', 'Features Assigned Successfully Created');
            } else {
                log_message('Error', 'Transaction failed in controllers/feature/insert_variant_features for feature');
                $this->session->set_flashdata('error_msg', TRANSACTION_ERROR_MSG);
            }

            redirect('accessory/map_accessory');
        } else {
            $this->map_accessory();
        }
    }

    function accessory_group() {
        $this->load->library('form_validation');
        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }

        $data['content'] = 'accessory/group_accessory';
        $this->load->view('index', $data);
    }

    function check_accessory() {
        $result = $this->input->post('result');
        $exp_1 = explode('-', $result);
        $res = '';
        for ($i = 0; $i <= count($exp_1) - 1; $i++) {
            $res = $res . "'" . $exp_1[$i] . "',";
        }
        echo $select_acc = rtrim($res, ",");
        $pos_dep = $this->accessory_model->get_pos_neg_res($select_acc, $i = 0);
        $neg_dep = $this->accessory_model->get_pos_neg_res($select_acc, $i = 1);
        print_r($pos_dep);
        print_r($neg_dep);
        exit;
    }

    function accessory_depencies() {
        $this->load->library('form_validation');
        if ($this->session->flashdata('error_msg')) {
            $data['error_msg'] = $this->session->flashdata('error_msg');
        }
        if ($this->session->flashdata('success_msg')) {
            $data['success_msg'] = $this->session->flashdata('success_msg');
        }
        $data['all_accessories'] = $this->accessory_model->get_variant_dependency($id = 1);
        $data['content'] = 'accessory/accessory_depencies';
        $this->load->view('index', $data);
    }

    function upload_acc_dependency() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="help-block">', '</div>');
        $this->form_validation->set_rules('accessory_id', 'Accessory Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('dependent_accessory', 'Dependent Accessory', 'trim|required|xss_clean');
        $this->form_validation->set_rules('type', 'type', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            $accessory_id = $this->input->post('accessory_id');
            $dependent_accessory_id = $this->input->post('dependent_accessory');
            $acc_name = $this->accessory_model->get_accessory_name($dependent_accessory_id);
            $accessory_name = $acc_name[0]['accessory_name'];
            $dependency_type = $this->input->post('type');

            $data = array(
                'accessory_id' => $accessory_id,
                'dependent_accessory_id' => $dependent_accessory_id,
                'dependent_accessory_name' => $accessory_name,
                'dependency_type' => $dependency_type
            );
            $this->accessory_model->create_variant_dependency($data);
            redirect('accessory/accessory_depencies');
        } else {
            $this->map_accessory();
        }
    }

    function delete_acc_dependency() {
        $accessory_id = $this->input->post('accessory_id');
        $this->accessory_model->delete_acc_dependencies($accessory_id);
        redirect('accessory/accessory_depencies');
    }

    function del_accessory_mapping() {
        $accessory_id = $this->input->post('accessory_id');
        $this->db->query("delete from accessory_variant_mapping where id='$accessory_id'");
        redirect('accessory/map_accessory');
    }
    
    
    function accessory_master()
    {
        $this->load->view('content/accessory_ecommerce/accessory', $data);
//        $data['content'] = 'accessory_ecommerce/accessory';
//        $this->load->view('index', $data);    
    }

}
