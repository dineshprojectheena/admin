<?php

class Module extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('modules');

        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    function index() {
        $data['content'] = 'home';
        $this->load->view('index', $data);
    }

    ########################## accessory seller start  ##########################

    function accessory_seller() {
        $id = '';
        $data['city'] = $this->modules->get_city();
        $data['ress'] = $this->modules->get_accessory_seller($id);
        $this->load->view('accessory_seller', $data);
    }

    function acc_sell_upload() {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('acc_name', 'Brand Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('post_code', 'Post Code', 'trim|required|numeric|max_length[15]');
            $this->form_validation->set_rules('street', 'street', 'trim|required|xss_clean');

            $this->form_validation->set_rules('accessory_brand', 'Accessory brand', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('picture', 'picture', 'trim|required|xss_clean');
            $this->form_validation->set_rules('contact_person_email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('contact_person_tel', 'Contact person tele.', 'required|numeric|max_length[15]');
            $this->form_validation->set_rules('fax', 'fax', 'required|numeric|max_length[15]');


            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->accessory_seller();
            } else {
                $config['upload_path'] = 'uploads/acc_supplier/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                $img_name = $this->upload->do_uploads('picture');

                $added_date = date('y-m-d H:i:s');
                $rec_data = array(
                    'name' => $this->input->post('acc_name'),
                    'city_id' => $this->input->post('city'),
                    'postcode' => $this->input->post('post_code'),
                    'street' => $this->input->post('street'),
                    'picture' => $img_name,
                    'contact_person_firstname' => $this->input->post('contact_person_firstname'),
                    'contact_person_lastname' => $this->input->post('contact_person_lastname'),
                    'contact_person_telephoneno' => $this->input->post('contact_person_tel'),
                    'contact_person_emailid' => $this->input->post('contact_person_email'),
                    'fax' => $this->input->post('fax'),
                    'accessory_brand' => $this->input->post('accessory_brand'),
                    'added_date' => $added_date
                );

                $query = $this->modules->acc_sell_upload($rec_data);
                redirect('accessory_seller');
            }
        } else if (isset($_POST['get'])) {
            $data['city'] = $this->modules->get_city();
            $accessory_seller_id = $this->input->post('accessory_seller_id');
            $data['ress2'] = $this->modules->get_accessory_seller($accessory_seller_id);
            //print_r($data['ress2']);	
            $this->load->view('accessory_seller', $data);
        } else if (isset($_POST['update'])) {

            $this->form_validation->set_rules('acc_name', 'Brand Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('post_code', 'Post Code', 'trim|required|numeric|max_length[15]');
            $this->form_validation->set_rules('street', 'street', 'trim|required|xss_clean');

            $this->form_validation->set_rules('accessory_brand', 'Accessory brand', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('pics', 'pic', 'trim|required|xss_clean');
            $this->form_validation->set_rules('contact_person_email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('contact_person_tel', 'Contact person tele.', 'required|numeric|max_length[15]');
            $this->form_validation->set_rules('fax', 'fax', 'required|numeric|max_length[15]');
            if (!$this->form_validation->run()) {

                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $data['city'] = $this->modules->get_city();
                $accessory_seller_id = $this->input->post('accessory_seller_id');
                $data['ress2'] = $this->modules->get_accessory_seller($accessory_seller_id);
                //print_r($data['ress2']);	
                $this->load->view('accessory_seller', $data);
//                $data['ress2'] = array_merge($rec_data2, $rec_data);
//                $data['city'] = $this->modules->get_city();
//                $this->load->view('accessory_seller', $data);
            } else {
                $img_name = $_POST['profile_pic'];
                if (!empty($_FILES['picture']['name'])) {
                    $config['upload_path'] = 'uploads/acc_supplier/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->set_allowed_types('*');
                    $data['upload_data'] = '';
                    $img_name = $this->upload->do_uploads('picture');
                }
                $added_date = date('y-m-d H:i:s');
                $rec_data = array(
                    'name' => $this->input->post('acc_name'),
                    'city_id' => $this->input->post('city'),
                    'postcode' => $this->input->post('post_code'),
                    'street' => $this->input->post('street'),
                    'picture' => $img_name,
                    'contact_person_firstname' => $this->input->post('contact_person_firstname'),
                    'contact_person_lastname' => $this->input->post('contact_person_lastname'),
                    'contact_person_telephoneno' => $this->input->post('contact_person_tel'),
                    'contact_person_emailid' => $this->input->post('contact_person_email'),
                    'fax' => $this->input->post('fax'),
                    'accessory_brand' => $this->input->post('accessory_brand'),
                    'added_date' => $added_date
                );
                $accessory_seller_id = $this->input->post('accessory_seller_id');
                $rec_data2 = array('accessory_seller_id' => $accessory_seller_id);


                $query = $this->modules->acc_sell_update($rec_data, $accessory_seller_id);
                redirect('accessory_seller');
            }
        } else if (isset($_POST['delete'])) {
            $accessory_seller_id = $this->input->post('accessory_seller_id');
            $query = $this->modules->acc_sell_del($accessory_seller_id);
            $this->accessory_seller();
        } else {
            redirect('accessory_seller');
        }
    }

    ########################## accessory seller start  ##########################
    ########################## service seller start  ##########################

    function service_seller() {
        $id = '';
        $data['city'] = $this->modules->get_city();
        $data['ress'] = $this->modules->get_service_seller($id);
        $this->load->view('service_seller', $data);
    }

    function service_seller_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/service_seller_action", array(), TRUE);
        $this->datatables
                ->select("service_seller_id,name,(select service_category from service_category where service_category_id=service_seller.service_category) as service_category,contact_person_emailid")
                ->from("service_seller")
                ->add_column("Action", $actionLinkBar, 'service_seller_id');

        echo $this->datatables->generate();
    }

    function ser_sell_upload() {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('acc_name', 'Brand Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');
            $this->form_validation->set_rules('post_code', 'Post Code', 'trim|required|numeric|max_length[15]');
            $this->form_validation->set_rules('street', 'street', 'trim|required|xss_clean');

            $this->form_validation->set_rules('accessory_brand', 'Service Category', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('picture', 'pic', 'trim|required|callback_handle_upload');
            $this->form_validation->set_rules('contact_person_email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('contact_person_tel', 'Email', 'required|numeric|max_length[15]');
            $this->form_validation->set_rules('fax', 'fax', 'required|numeric|max_length[15]');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->service_seller();
            } else {
                $config2['upload_path'] = 'uploads/ser_seller/';
                $config2['allowed_types'] = 'gif|jpg|png';
                $config2['max_size'] = '2048';
                $this->load->library('upload', $config2);
                $img_name = $this->upload->do_uploads('picture');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }





                $added_date = date('y-m-d H:i:s');
                $rec_data = array(
                    'name' => $this->input->post('acc_name'),
                    'city_id' => $this->input->post('city'),
                    'postcode' => $this->input->post('post_code'),
                    'street' => $this->input->post('street'),
                    'picture' => $img_name,
                    'contact_person_firstname' => $this->input->post('contact_person_firstname'),
                    'contact_person_lastname' => $this->input->post('contact_person_lastname'),
                    'contact_person_telephoneno' => $this->input->post('contact_person_tel'),
                    'contact_person_emailid' => $this->input->post('contact_person_email'),
                    'fax' => $this->input->post('fax'),
                    'service_category' => $this->input->post('accessory_brand'),
                    'added_date' => $added_date
                );
                $query = $this->modules->ser_sell_upload($rec_data);
                redirect('service_seller');
            }
        } else if (isset($_POST['get'])) {
            $data['city'] = $this->modules->get_city();
            $id = $this->input->post('id');
            $data['ress2'] = $this->modules->get_service_seller($id);
            $this->load->view('service_seller', $data);
        } else if (isset($_POST['update'])) {
            $this->form_validation->set_rules('acc_name', 'Brand Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');
            $this->form_validation->set_rules('post_code', 'Post Code', 'trim|required|numeric|max_length[15]');
            $this->form_validation->set_rules('street', 'street', 'trim|required|xss_clean');
            $this->form_validation->set_rules('accessory_brand', 'Service Category', 'trim|required|xss_clean');
            $this->form_validation->set_rules('contact_person_email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('contact_person_tel', 'Email', 'required|numeric|max_length[15]');
            $this->form_validation->set_rules('fax', 'fax', 'required|numeric|max_length[15]');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $data['ress2'] = array_merge($rec_data2, $rec_data);
                $data['city'] = $this->modules->get_city();
                $this->load->view('service_seller', $data);
            } else {
                $config2['upload_path'] = 'uploads/ser_seller/';
                $config2['allowed_types'] = 'gif|jpg|png';
                $config2['max_size'] = '2048';
                $this->load->library('upload', $config2);
                $img_name = $this->upload->do_uploads('picture');
                if ($img_name == '') {
                    ?>
                    <script>
                        alert('wrong file type upload,please upload image');
                        history.go(-1);
                    </script>
                    <?php

                    exit;
                }
                if ($img_name == '') {
                    $img_name = $_POST['profile_pic'];
                }
                $accessory_seller_id = $this->input->post('accessory_seller_id');

                $added_date = date('y-m-d H:i:s');
                $rec_data = array(
                    'name' => $this->input->post('acc_name'),
                    'city_id' => $this->input->post('city'),
                    'postcode' => $this->input->post('post_code'),
                    'street' => $this->input->post('street'),
                    'picture' => $img_name,
                    'contact_person_firstname' => $this->input->post('contact_person_firstname'),
                    'contact_person_lastname' => $this->input->post('contact_person_lastname'),
                    'contact_person_telephoneno' => $this->input->post('contact_person_tel'),
                    'contact_person_emailid' => $this->input->post('contact_person_email'),
                    'service_category' => $this->input->post('accessory_brand'),
                    'fax' => $this->input->post('fax'),
                    'added_date' => $added_date
                );
//                print_r($rec_data);
//                exit;                
                $query = $this->modules->ser_sell_update($rec_data, $accessory_seller_id);
                redirect('service_seller');
            }
        } else if (isset($_POST['delete'])) {
            $id = $this->input->post('id');

            $query = $this->modules->ser_sell_del($id);
            $this->service_seller();
        } else {
            redirect('service_seller');
        }
    }

    ########################## service seller start  ##########################
    ########################## model start  ##########################

    function model() {
        $id = '';
        $data['brand'] = $this->modules->get_brand();
//        $data['ress'] = $this->modules->get_model($id);
//        $data['ress'] = $this->modules->get_service_seller($id);
        $this->load->view('model', $data);
    }

    function model_result() {
        $this->load->library("datatables");
        $this->load->helper("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/model_all_action", array(), TRUE);
        $this->datatables
                ->select("model_id,model_name,"
                        . "(select brand_name from brand where brand_id=model.brand_id) as brand_name,status")
                ->from("model")
                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action", $actionLinkBar, 'model_id');

        echo $this->datatables->generate();
    }

    function model_upload() {
        if (isset($_POST['submit'])) {
            $id = '';
            $data['ress'] = $this->modules->get_model($id);
            $data['brand'] = $this->modules->get_brand();
            $this->form_validation->set_rules('brand', 'brand', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('car_type', 'Car Type', 'required');
            $this->form_validation->set_rules('model_name', 'Model Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            $added_date = date('y-m-d h:i:s');
            $rec_data = array(
                'brand_id' => $this->input->post('brand'),
                'car_type' => $this->input->post('car_type'),
                'model_name' => $this->input->post('model_name'),
                'status' => $this->input->post('status'),
                'added_date' => $added_date
            );
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->load->view('model', $data);
            } else {
                $query = $this->modules->model_upload($rec_data);
                redirect('model');
            }
        } else if (isset($_POST['update'])) {
            $data['brand'] = $this->modules->get_brand();
            $this->form_validation->set_rules('brand', 'brand', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('car_type', 'Car Type', 'required');
            $this->form_validation->set_rules('model_name', 'Model Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            $added_date = date('y-m-d h:i:s');
            $rec_data = array(
                'brand_id' => $this->input->post('brand'),
                'car_type' => $this->input->post('car_type'),
                'model_name' => $this->input->post('model_name'),
                'status' => $this->input->post('status'),
                'added_date' => $added_date
            );
            $id = $this->input->post('model_id');
            $data['ress2'] = $this->modules->get_model($id);
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->load->view('model', $data);
            } else {
                $query = $this->modules->model_updates($rec_data, $id);
                redirect('model');
            }
        } else if (isset($_POST['delete'])) {
            echo $model_id = $this->input->post('model_id');
            exit;
            $query = $this->modules->model_del($model_id);
            redirect('model');
        } else if (isset($_POST['get'])) {
            $data['brand'] = $this->modules->get_brand();
            $model_id = $this->input->post('model_id');
            $data['ress2'] = $this->modules->get_model($model_id);
            $this->load->view('model', $data);
        } else {
            redirect('model');
        }
    }

    ########################## model end  ##########################
    ########################## body color start  ##########################

    function body_color() {
        $id = '';
        $data['ress'] = $this->modules->get_color($id);
//        exit;
        $this->load->view('body_color', $data);
    }

    function body_color_upload() {
        if (isset($_POST['submit'])) {
            $id = '';
            $this->form_validation->set_rules('color', 'color', 'required');
            $this->form_validation->set_rules('body_icon', 'body icon', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('body_price', 'body_price', 'trim|required|xss_clean|numeric');
            $added_date = date('y-m-d H:i:s');
            if ($this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->load->view('body_color');
            } else {
                $config['upload_path'] = 'uploads/body_color/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                $img_name = $this->upload->do_uploads('body_icon');
                $rec_data = array(
                    'color' => $this->input->post('color'),
                    'body_icon' => $img_name,
                    'status' => $this->input->post('status'),
                    'body_price' => $this->input->post('body_price'),
                    'added_date' => $added_date
                );
                $query = $this->modules->color_upload($rec_data);
                redirect('body_color');
            }
        } else if (isset($_POST['update'])) {
            $this->form_validation->set_rules('color', 'color', 'required');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('body_price', 'body_price', 'trim|required|xss_clean|numeric');

            $added_date = date('y-m-d H:i:s');
            $body_color_id = $this->input->post('body_color_id');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $data['ress2'] = $this->modules->get_color($body_color_id);
                $this->load->view('body_color', $data);
            } else {


                $config['upload_path'] = 'uploads/body_color/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                if (!empty($_FILES['body_icon']['name'])) {
                    $img_name = $this->upload->do_uploads('body_icon');
                } else {
                    $img_name = $this->input->post('body_icon2');
                }
                echo $img_name;
//                exit;
                $rec_data = array(
                    'color' => $this->input->post('color'),
                    'body_icon' => $img_name,
                    'status' => $this->input->post('status'),
                    'body_price' => $this->input->post('body_price'),
                    'added_date' => $added_date
                );
                $query = $this->modules->color_update($rec_data, $body_color_id);
                redirect('body_color');
            }
        } else if (isset($_POST['delete'])) {
            $body_color_id = $this->input->post('body_color_id');

            $query = $this->modules->body_color_del($body_color_id);
            redirect('body_color');
        } else if (isset($_POST['get'])) {
            $body_color_id = $this->input->post('body_color_id');
            $data['ress2'] = $this->modules->get_color($body_color_id);
            $this->load->view('body_color', $data);
        } else {
            redirect('body_color');
        }
    }

    ########################## Body Dashboard  ##########################

    function body_dashboard() {
        $id = '';
        $data['res1'] = $this->modules->get_wheel($id);
        $this->load->view('body_dashboard', $data);
    }

    function body_dashboard_upload() {
        if (isset($_POST['submit'])) {
            $id = '';
            $this->form_validation->set_rules('color', 'color', 'required');
            $this->form_validation->set_rules('body_icon', 'body icon', 'trim|required|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('body_price', 'body_price', 'trim|required|xss_clean|numeric');

            $added_date = date('y-m-d H:i:s');
            if ($this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->load->view('body_dashboard');
            } else {
                $config['upload_path'] = 'uploads/body_dashboard/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                $img_name = $this->upload->do_uploads('body_icon');


                $rec_data = array(
                    'color' => $this->input->post('color'),
                    'body_dashboard_icon' => $img_name,
                    'status' => $this->input->post('status'),
                    'body_price' => $this->input->post('body_price'),
                    'added_date' => $added_date
                );
                print_r($rec_data);
                exit;

                $query = $this->modules->color_upload($rec_data);
                redirect('body_color');
            }
        } else if (isset($_POST['update'])) {
            $this->form_validation->set_rules('color', 'color', 'required');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
            $this->form_validation->set_rules('body_price', 'body_price', 'trim|required|xss_clean|numeric');

            $added_date = date('y-m-d H:i:s');
            $body_color_id = $this->input->post('body_color_id');
            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $data['ress2'] = $this->modules->get_color($body_color_id);
                $this->load->view('body_color', $data);
            } else {


                $config['upload_path'] = 'uploads/body_color/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                if (!empty($_FILES['body_icon']['name'])) {
                    $img_name = $this->upload->do_uploads('body_icon');
                } else {
                    $img_name = $this->input->post('body_icon2');
                }
                echo $img_name;
//                exit;
                $rec_data = array(
                    'color' => $this->input->post('color'),
                    'body_icon' => $img_name,
                    'status' => $this->input->post('status'),
                    'body_price' => $this->input->post('body_price'),
                    'added_date' => $added_date
                );
                $query = $this->modules->color_update($rec_data, $body_color_id);
                redirect('body_color');
            }
        } else if (isset($_POST['delete'])) {
            $body_color_id = $this->input->post('body_color_id');

            $query = $this->modules->body_color_del($body_color_id);
            redirect('body_color');
        } else if (isset($_POST['get'])) {
            $body_color_id = $this->input->post('body_color_id');
            $data['ress2'] = $this->modules->get_color($body_color_id);
            $this->load->view('body_color', $data);
        } else {
            redirect('body_color');
        }
    }

    ########################## Body Dashboard  ##########################
    #
    ########################### model end  ##########################
    ########################## wheel start  ##########################

    function wheel() {
        $id = '';
        $data['res1'] = $this->modules->get_wheel($id);
        $this->load->view('wheel', $data);
    }

    function wheel_upload() {
        $wheel_id = $this->input->post('wheel_id');
        if (isset($_POST['submit'])) {
            $id = '';
            $data['res1'] = $this->modules->get_wheel($id);
            $this->form_validation->set_rules('tire_size', 'tire size', 'required|numeric');
            $this->form_validation->set_rules('wheel_size', 'wheel size', 'trim|required|xss_clean|numeric');
            $this->form_validation->set_rules('wheel_description', 'wheel description', 'trim|required|xss_clean');
            $this->form_validation->set_rules('wheel_price', 'wheel price', 'trim|required|xss_clean|numeric');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean|numeric');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );

                $this->load->view('wheel', $data);
            } else {
                $config['upload_path'] = 'uploads/wheel/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';

                $img_name = $this->upload->do_uploads('wheel_design');
                $added_date = date('y-m-d H:i:s');
                $rec_data = array(
                    'tire_size' => $this->input->post('tire_size'),
                    'wheel_size' => $this->input->post('wheel_size'),
                    'wheel_description' => $this->input->post('wheel_description'),
                    'wheel_price' => $this->input->post('wheel_price'),
                    'wheel_design' => $img_name,
                    'status' => $this->input->post('status'),
                    'added_date' => $added_date
                );
                $query = $this->modules->wheel_upload($rec_data);
                redirect('wheel');
            }
        } else if (isset($_POST['update'])) {
            $id = '';
            $data['ress2'] = $this->modules->get_wheel($wheel_id);
            $this->form_validation->set_rules('tire_size', 'tire size', 'required|numeric');
            $this->form_validation->set_rules('wheel_size', 'wheel size', 'trim|required|xss_clean|numeric');
            $this->form_validation->set_rules('wheel_description', 'wheel description', 'trim|required|xss_clean');
            $this->form_validation->set_rules('wheel_price', 'wheel price', 'trim|required|xss_clean|numeric');
            $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean|numeric');
            $added_date = date('y-m-d H:i:s');
            $rec_data = array(
                'tire_size' => $this->input->post('tire_size'),
                'wheel_size' => $this->input->post('wheel_size'),
                'wheel_price' => $this->input->post('wheel_price'),
                'wheel_description' => $this->input->post('wheel_description'),
                'wheel_design' => $_FILES['wheel_design']['name'],
                'status' => $this->input->post('status'),
                'added_date' => $added_date
            );
//            print_r($rec_data);
//            exit;       


            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );

                $this->load->view('wheel', $data);
            } else {

                $config['upload_path'] = 'uploads/wheel/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->set_allowed_types('*');
                $data['upload_data'] = '';
                if (!empty($_FILES['wheel_design']['name'])) {
                    $img_name = $this->upload->do_uploads('wheel_design');
                } else {
                    $img_name = $this->input->post('wheel_design2');
                }
                $added_date = date('y-m-d H:i:s');
                $rec_data = array(
                    'tire_size' => $this->input->post('tire_size'),
                    'wheel_size' => $this->input->post('wheel_size'),
                    'wheel_price' => $this->input->post('wheel_price'),
                    'wheel_description' => $this->input->post('wheel_description'),
                    'wheel_design' => $img_name,
                    'status' => $this->input->post('status'),
                    'added_date' => $added_date
                );
                $query = $this->modules->wheel_update($rec_data, $wheel_id);

                redirect('wheel');
            }
        } else if (isset($_POST['delete'])) {
            $query = $this->modules->wheel_del($wheel_id);
            redirect('wheel');
        } else if (isset($_POST['get'])) {
            $data['ress2'] = $this->modules->get_wheel($wheel_id);
            $this->load->view('wheel', $data);
        } else {
            redirect('wheel');
        }
    }

    function uploadTaskImages($folder_name) {
        $CI = & get_instance();

        /*
         *  Check whether the upload path already exist ?
         * If not, then create a new folder else
         * upload file in exisiting folder
         */
        $pathToUpload = './uploads/projects/' . $folder_name;
        if (!file_exists($pathToUpload)) {
            mkdir($pathToUpload, 0777, TRUE);
            chmod($pathToUpload, 0777);
            mkdir($pathToUpload . '/images', 0777, TRUE);
            chmod($pathToUpload . '/images', 0777);
            mkdir($pathToUpload . '/thb', 0777, TRUE);
            chmod($pathToUpload . '/thb', 0777);
        }

        /*
         * Count how many files have user selected to upload
         */
        $total_files_to_upload = count($_FILES['filename']['name']);
        $length = strlen($_FILES['filename']['name'][0]);
        /*
         * $length != 0 => if user has selected some files; (if condition)
         * $length == 0 => if user has selected no files; (else condition)
         */

        if ($length != 0) {
            $image_name = time();
            for ($i = 0; $i < $total_files_to_upload; $i++) {
                $_FILES['userfile']['name'] = $_FILES['filename']['name'][$i];
                $_FILES['userfile']['type'] = $_FILES['filename']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $_FILES['filename']['error'][$i];
                $_FILES['userfile']['size'] = $_FILES['filename']['size'][$i];
                $image_name = $image_name + 1;
                $config['file_name'] = $image_name;
                $config['overwrite'] = FALSE;
                $config['upload_path'] = $pathToUpload . '/images/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = '1024';
                $config['max_width'] = '0';
                $config['max_height'] = '0';
                $CI->upload->initialize($config);
                if ($CI->upload->do_upload()) {
                    $data = $CI->upload->data();
                    chmod($data['full_path'], 0777);
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $data['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['thumb_marker'] = '';
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 600;
                    $config['height'] = 400;
                    $CI->image_lib->initialize($config);
                    $CI->image_lib->resize();

                    unset($config);

                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $data['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['thumb_marker'] = '';
                    $config['new_image'] = $pathToUpload . '/thb/' . $image_name . $data['file_ext'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150;
                    $config['height'] = 150;
                    $CI->image_lib->initialize($config);
                    $CI->image_lib->resize();
                } else {
                    $error = $CI->upload->display_errors();
                    $CI->session->set_flashdata('error_msg', $error);
                    redirect('create-task');
                }
            }
        } else {
            /*
             * Check if User had already Uploaded Any files earlier
             */
            $files = $this->getTaskImages($folder_name);
            if (count($files) >= 1) {
                
            } else {
                $this->default_task_image($pathToUpload);
            }
        }
    }

    ########################## wheel end  ##########################
}
