<?php

class Expert_rating extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('experts_rating');
        $this->load->model('product_data');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function rating() {
        $id = '';
//      $data['show_res'] = $this->experts_rating->show_rating($id);
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();
        $data['msg'] = $this->load->view('CKEditorForm', array(
            'ckeditor' => $this->_setup_ckeditor('content'),
            // HTML for textarea, populate using your model's property
            'content_html' => ''
                ), true);
//        echo $data['msg'];
//        $data['datas'] = $this->load_editor();

        $this->load->view('expert_rating', $data);
    }

    function load_editor_data() {
        $CI = & get_instance();
        return $CI->load_editor();
    }

    function load_editor() {
        $this->load->view('CKEditorForm', array(
            'ckeditor' => $this->_setup_ckeditor('content'),
            // HTML for textarea, populate using your model's property
            'content_html' => ''
        ));
    }

    function expert_rating_delete() {
        $exp_id=explode(",",$_GET['id']);
        
        foreach($exp_id as $id)
        {    
        $query = $this->experts_rating->rating_del($id);
        }
        $this->session->set_flashdata('status', "Data Deleted Successfully");
        redirect('rating');
    }

    public function save() {
        if (FALSE !== $this->input->post('content')) {
            // TODO persist model for 'content' textarea HTML containing uploaded
            // file's img reference.
        }

        header('Location: /CKEeditorForm/');
        exit();
    }

    /**
     * Output CKEditor Javascript callback function for image file uploaded
     * in $_FILES['upload']. The GET parameters must also contain the
     * CKEditorFuncNum parameter so the JavaScript callback will reference
     * the correct CKEditor instance.
     */
    public function upload() {
        $config['upload_path'] = 'assets/upload/images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->set_allowed_types('*');
        $data['upload_data'] = '';



        $callback = 'null';
        $url = '';
        $get = array();

        // for form action, pull CKEditorFuncNum from GET string. e.g., 4 from
        // /ckeditor-form/upload?CKEditor=content&CKEditorFuncNum=4&langCode=en
        // Convert GET parameters to PHP variables
        $qry = $_SERVER['REQUEST_URI'];
        parse_str(substr($qry, strpos($qry, '?') + 1), $get);

        if (!isset($_POST) || !isset($get['CKEditorFuncNum'])) {
            $msg = 'CKEditor instance not defined. Cannot upload image.';
        } else {
            $callback = $get['CKEditorFuncNum'];

            $img_name = $this->upload->do_uploads('upload');
//            var_dump($img_name);
            $url = base_url() . "assets/upload/images/" . $img_name;
            $msg = "File uploaded successfully to: {$url}";
        }

        // Callback function that inserts image into correct CKEditor instance
        $output = '<html><body><script type="text/javascript">' .
                'window.parent.CKEDITOR.tools.callFunction(' .
                $callback .
                ', "' .
                $url .
                '", "' .
                $msg .
                '");</script></body></html>';

        echo $output;
    }

    /**
     * Retrieve configuration properties for CKEditor instance. Ensure the
     * CodeIgniter helper has been copied to CI's system directory.
     *
     * @param $id HTML id="" attribute CKEditor instance is enabled for.
     *
     * @return array First parameter for display_ckeditor() function invoked
     *         in the CI view.
     */
    private function _setup_ckeditor($id) {
        $this->load->helper('url');
        $this->load->helper('ckeditor');

        $ckeditor = array(
            'id' => $id,
            'path' => 'assets/js/ckeditor',
            'config' => array(
                'toolbar' => 'Full',
                'width' => '100%',
                'height' => 'auto',
                // path to submit image upload form to. i.e., upload() above
                'filebrowserImageUploadUrl' => base_url() . 'CKEditorForm/upload')

                // add additional CKEditor properties here
        );

        return $ckeditor;
    }

    function rating_result() {
        $this->load->library("datatables");
        $actionLinkBar = $this->load->view("content/report/datatable/expert_rating_action", array(), TRUE);
        $this->datatables
                ->select("expert_rating_id,(select pro_name_comp from variant where variant_id=expert_rating.pro_id) as pro_name,source,total,journilist_name,heading,added_date")
                ->from("expert_rating")
                ->add_column("Action", $actionLinkBar, 'expert_rating_id');
        echo $this->datatables->generate();
    }

    function rating_upload() {
        $data['brand'] = $this->product_data->get_brand();
        $data['model'] = $this->product_data->get_model();

        #### used for insert section here
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('brand_id', 'Brand', 'required');
            $this->form_validation->set_rules('model_id', 'Model', 'required');
            $this->form_validation->set_rules('all_product_name', 'Product', 'required');
//            $this->form_validation->set_rules('expert_pic', 'Profile Pic', 'required');
//            $this->form_validation->set_rules('details', '', 'required');
//            $this->form_validation->set_rules('com_name', 'Journilist name', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('total', 'Product Rating', 'trim|required|xss_clean|numeric');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->session->set_flashdata('status', "Please Fill Validated Data");
                $this->rating();
            } else {
                
                if (!empty($_FILES['expert_pic']['name'])) {
                        $config2['upload_path'] = 'uploads/expert/';
                        $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config2['max_size'] = '2400';
                        $this->load->library('upload', $config2);
                        $image = $this->upload->do_uploads('expert_pic');
                        if ($image == '') {
                            ?>
                            <script>
                                alert('Please upload Profile Picture');
                                //                                history.go(-1);
                            </script>
                            <?php

                            $this->rating();
                        }
                                        
                }
                else
                {
                    $image='';
                }    
                
                
                /*
                $size = number_format($_FILES['expert_pic']['size'] / 1048576, 2);
                if ($size > 2) {
                    ?>
                    <script>
                        alert('Profile Picture should be less than 2MB');
                    </script>
                    <?php

                    $this->rating();
                } else {
                    $config2['upload_path'] = 'uploads/expert/';
                    $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config2['max_size'] = '2400';
                    $this->load->library('upload', $config2);
                    $image = $this->upload->do_uploads('expert_pic');
                    
                    if ($image == '') {
                        ?>
                        <script>
                            alert('Please upload Profile Picture');
                        </script>
                        <?php
                        $this->session->set_flashdata('status', "Please Upload Profile Picture");
                        $this->rating();
                    }
                    
                    
                }
                */
//                if ($image != '') {
                    $brand_id = $this->input->post('brand_id');
                    $model_id = $this->input->post('model_id');
                    $all_product_name = $this->input->post('all_product_name');
                    if ($all_product_name == 'All') {
                        $get_data = $this->db->query("select `variant_id`,`pro_name_comp` from `variant` where `brand_id`='$brand_id' and `model_id`='$model_id'")->result_array();
                        foreach ($get_data as $get_data_res) {
                            $variant_id = $get_data_res['variant_id'];
                            $rec_data = array(
                                'pro_id' => $variant_id,
                                'source' => $this->input->post('source'),
                                'journilist_name' => $this->input->post('com_name'),
                                'heading' => $this->input->post('heading'),
                                'what_i_like' => $this->input->post('content'),
                                'total' => $this->input->post('total'),
                                'profile_id' => $image,
                                'date' => date('Y-m-d')
                            );
                            $query = $this->experts_rating->rating_upload($rec_data);
                        }
                    } else {
                        $rec_data = array(
                            'pro_id' => $all_product_name,
                            'source' => $this->input->post('source'),
                            'journilist_name' => $this->input->post('com_name'),
                            'heading' => $this->input->post('heading'),
                            'what_i_like' => $this->input->post('content'),
                            'total' => $this->input->post('total'),
                            'profile_id' => $image,
                            'date' => date('Y-m-d')
                        );
                        $query = $this->experts_rating->rating_upload($rec_data);
                    }
                    
                    $this->session->set_flashdata('status', "Data Uploded Successfully");
                    redirect('rating');
//                }
            }
        } else if (isset($_POST['update'])) {
            $id = $this->input->post('expert_rating_ids');
            if (empty($id)) {
                $id = $this->input->post('expert_rating_id');
            }
            $data['single_res'] = $this->experts_rating->show_rating($id);
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            $this->form_validation->set_rules('brand_id', 'Brand', 'required');
            $this->form_validation->set_rules('model_id', 'Model', 'required');
            $this->form_validation->set_rules('all_product_name', 'Product', 'required');
//            $this->form_validation->set_rules('expert_pic', 'Profile Pic', 'required');
//            $this->form_validation->set_rules('details', '', 'required');
//            $this->form_validation->set_rules('com_name', 'Journilist name', 'trim|required|xss_clean');
//            $this->form_validation->set_rules('total', 'Product Rating', 'trim|required|xss_clean|numeric');

            if (!$this->form_validation->run()) {
                $resp = array(
                    'code' => 0,
                    'msg' => validation_errors()
                );
                $this->session->set_flashdata('status', "Please Fill Validated Data");
                $this->load->view('expert_rating', $data);
            } else {
                
                if (!empty($_FILES['expert_pic']['name'])) {
                        $config2['upload_path'] = 'uploads/expert/';
                        $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config2['max_size'] = '2400';
                        $this->load->library('upload', $config2);
                        $image = $this->upload->do_uploads('expert_pic');
                        if ($image == '') {
                            ?>
                            <script>
                                alert('Please upload Profile Picture');
                                //                                history.go(-1);
                            </script>
                            <?php

                            $this->rating();
                        }
                                        
                } else {
                    $image = $_POST['pro_image2'];
                }
                if(!isset($image))
                {
                    $image='';
                }
                
                
//                if ($image != '') {
                    $all_product_name = $this->input->post('all_product_name');
                    $rec_data = array(
                        'pro_id' => $all_product_name,
//                        'content' => $this->input->post('content'),
                        'source' => $this->input->post('source'),
                        'journilist_name' => $this->input->post('com_name'),
                        'heading' => $this->input->post('heading'),
                        'what_i_like' => $this->input->post('content'),
//                    'what_i_dont_like' => $this->input->post('what_i_dont_like'),
                        'total' => $this->input->post('total'),
                        'profile_id' => $image,
                        'date' => date('Y-m-d')
                    );

                    $query = $this->experts_rating->rating_update($rec_data, $id);
                    $this->session->set_flashdata('status', "Data Updated Successfully");
                    redirect('rating');
//                }
            }
        } else if (isset($_POST['delete'])) {
            $id = $this->input->post('expert_rating_id');

            $query = $this->experts_rating->rating_del($id);
            $this->session->set_flashdata('status', "Data Deleted SUccessfully");
            redirect('rating');
        } else if (isset($_POST['get'])) {
            $id = $this->input->post('expert_rating_id');
            $data['single_res'] = $this->experts_rating->show_rating($id);
            foreach ($data['single_res'] as $get_res)
            $pro_id = $get_res->pro_id;
            $data['single_res_detail'] = $this->experts_rating->show_rating_data($pro_id);
            $data['msg'] = $this->load->view('CKEditorForm', array(
                'data_res'=>$data['single_res'],
                'ckeditor' => $this->_setup_ckeditor('content'),
                // HTML for textarea, populate using your model's property
                'content_html' => ''
                    ), true);
//            echo $data['msg'];
//            exit;
            
//            print_r($query['single_res_detail']);
            $this->load->view('expert_rating', $data);
        } else {

            redirect('rating');
        }
    }

    function uploadCauseImages() {
        $CI = & get_instance();
        /*
         *  Check whether the upload path already exist ?
         * If not, then create a new folder else
         * upload file in exisiting folder
         */
        $pathToUpload = './uploads/expert/';
        if (!file_exists($pathToUpload)) {
            mkdir($pathToUpload, 0777, TRUE);
            chmod($pathToUpload, 0777);
            mkdir($pathToUpload . '/images', 0777, TRUE);
            chmod($pathToUpload . '/images', 0777);
            mkdir($pathToUpload . '/thb', 0777, TRUE);
            chmod($pathToUpload . '/thb', 0777);
        }

        /*
         * Count how many files have user selected to upload
         */

        echo $total_files_to_upload = count($_FILES['expert_pic']['name']);

        $length = strlen($_FILES['expert_pic']['name'][0]);

        exit;

        /*
         * $length != 0 => if user has selected some files; (if condition)
         * $length == 0 => if user has selected no files; (else condition)
         */

        if ($length != 0) {
            $image_name = time();
            for ($i = 0; $i < $total_files_to_upload; $i++) {
                $_FILES['userfile']['name'] = $_FILES['expert_pic']['name'][$i];
                $_FILES['userfile']['type'] = $_FILES['expert_pic']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['expert_pic']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $_FILES['expert_pic']['error'][$i];
                $_FILES['userfile']['size'] = $_FILES['expert_pic']['size'][$i];
                $image_name = $image_name + 1;
                $config['file_name'] = $image_name;
                $config['overwrite'] = FALSE;
                $config['upload_path'] = $pathToUpload;
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['max_size'] = '1024';
                $config['max_width'] = '0';
                $config['max_height'] = '0';
                $CI->upload->initialize($config);
                if ($CI->upload->do_upload()) {
                    $data = $CI->upload->data();
                    chmod($data['full_path'], 0777);
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $data['full_path'];
                    $config['create_thumb'] = TRUE;
                    $config['thumb_marker'] = '';
                    $config['new_image'] = $pathToUpload . '/thb/' . $image_name . $data['file_ext'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 150;
                    $config['height'] = 150;
                    $CI->image_lib->initialize($config);
                    $CI->image_lib->resize();
                } else {
                    $error = $CI->upload->display_errors();
                    $CI->session->set_flashdata('error_msg', $error);
                }
            }
        } else {
            /*
             * Check if User had already Uploaded Any files earlier
             */
            $files = $this->getCauseImages($folder_name);
            if (count($files) >= 1) {
                
            } else {
                $this->default_cause_image($pathToUpload);
            }
        }
    }

}
