<?php

class Order extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("report");
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library("datatables");
    }

    function order_details($id) {
        $data['id'] = $id;
        $data['order_details'] = $this->report->order_reports($id, $i = 1,$added_by);
        $data['pro_details'] = $this->report->order_reports($id, $i = 2,$added_by);
        $data['order_history'] = $this->report->order_reports($id, $i = 3,$added_by='customer');
        $data['order_status'] = $this->report->order_status();
        $data['content'] = 'report/order_detail';
        $this->load->view('index', $data);
    }
    function upcoming_variant_order($id) {
        $data['id'] = $id;
        $data['content'] = 'report/upcoming_orders';
        $this->load->view('index', $data);
    }
    
    
    function brand_result() {
        $actionLinkBar = $this->load->view("content/report/datatable/upcomingorder", array(), TRUE);
        $this->datatables
                ->select("upcoming_configuration_id, `invoice_no`,(select pro_name_comp from variant where `variant_id`=upcoming_configuration.variant_id) as variant_name,(select firstname from user where `user_id`=upcoming_configuration.user_id) as first_name,(select lastname from user where `user_id`=upcoming_configuration.user_id) as last_name,(select email from user where `user_id`=upcoming_configuration.user_id) as email,`booking_amount`,(select name from city where city_id=upcoming_configuration.city_id) as city_name,`added_date`")
                ->from("upcoming_configuration")
                ->where("status","2")
//                ->edit_column('status', '$1', 'check_status(status)')
                ->add_column("Action", $actionLinkBar, 'upcoming_configuration_id');
        echo $this->datatables->generate();
    }

    function status_update() {
        $status = $this->input->post('status');
        $order_id = $this->input->post('order_id');
        $data['get_order_email'] = $this->report->order_email($order_id);
        $notify = $this->input->post('notify');
        $comments = $this->input->post('comments');
        if ($notify[0] == 1) {
            $get_order_email = $this->report->order_email($order_id);
            $user_email = $get_order_email[0]['email'];
            $data = $this->input->post();
            $mandrill_api_key = Mandrill_key;
            $this->load->library('Mandrill');

            $mandrill_ready = NULL;
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready = TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            if ($mandrill_ready) {
                $email = array(
                    'html' => $this->load->view('emailers/contact_email', $data, TRUE), //Consider using a view file
                    'text' => 'You have a order history Email',
                    'subject' => 'Email Via MNC order status',
                    'from_email' => 'webdeveloperdinesh@gmail.com',
                    'from_name' => 'support',
                    'to' => array(array('email' => $user_email))
//                        'to'=>array(array('email' => 'madhukarmanpuria@gmail.com')),
//                        'bcc'=>array(array('email' => 'webdeveloperdinesh@gmail.com'))
                );
                $result = $this->mandrill->messages_send($email);
            }
//                $msg = 'Email sent successfully. You will hear from us soon';
//                $this->session->set_flashdata('alert_msg', $msg);
//                redirect('contact-us');
//            }
//            echo 'sdsdsdsd';
        }
//        exit;
        $notify = $notify[0];
        $data['order_status'] = $this->report->order_status_update($status, $order_id, $notify, $comments,$dealer_id='',$added_by='customer');
        redirect(base_url().'order-detail/'.$order_id);
        
    }

    function lastestorder() {
        $data['report'] = $this->report->dashborad_reports();
        $data['content'] = 'report/orders';
        $this->load->view('index', $data);
    }

    function order_to_dealer() {
        $data['report'] = $this->report->dashborad_reports();
        $data['content'] = 'report/orders_to_dealer';
        $this->load->view('index', $data);
    }
    
    function orderdownload()
    {
//    if (isset($_POST['download'])) {
            $select = "select o.order_id,o.paid_amount,o.invoice_no,o.on_road_price,c.total_saving,c.finance_id,c.used_id,c.home_delivery,c.insurance_id,c.extended_id,c.corporate_discount,c.loyality_bonus,"
                    . "(select name from `city` where city_id=o.city_id), "
                    . "(select pro_name_comp from `variant` where variant_id=o.product_id), "
                    . "(select firstname from `user` where user_id=o.user_id), "
                    . "(select lastname from `user` where user_id=o.user_id), "
                    . "(select email from `user` where user_id=o.user_id), "
                    . "(select telephone from `user` where user_id=o.user_id), "
                    . "(select address from `user` where user_id=o.user_id) "
                    . "from `order` o,`car_configuration` c where c.car_configuration_id=o.configuration_id";
            $export = mysql_query($select);
            $fields = mysql_num_fields($export);
                     
            
//            for ($i = 0; $i < $fields; $i++) {
//                 $header.= mysql_field_name($export, $i) . ",\t";
//            }
            
//            echo $header; 
//            echo '<br>';
//            echo '<br>';
//            
//            $header.='Order Id';
//            $header.='\t';
//            $header.='Booking Amount';
            $header.='Order Id '."\t".' Booking Amount'."\t".'Invoice No'."\t".'Onroad Price'."\t".'Total Saving'."\t".'Finanace'."\t".'Car Exchange'."\t".'Home Delivery'."\t".'Zero Depreciation Insurance'."\t".' Extended Warranty   '."\t".' Corporate Discount'."\t".'Loyalty Bonus'."\t".'City'."\t".'Product Name'."\t".'Customer Firstname'."\t".'Customer Lastname'."\t".' Customer Email'."\t".'Telephone'."\t".'Address ';
//            exit;
            
            while ($row = mysql_fetch_row($export)) {
                $line = '';
                foreach ($row as $value) {
                    if ((!isset($value) ) || ( $value == "" )) {
                        $value = "\t";
                    } else {
                        $value = str_replace('"', '""', $value);
                        $value = '"' . $value . '"' . "\t";
                    }
                    $line .= $value;
                }
                $data .= trim($line) . "\n";
            }
            $data = str_replace("\r", "", $data);

            if ($data == "") {
                //$data = "\n(0) Records Found!\n";
            }

            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=order_data.xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            print "$header\n$data";
            exit;
//        }    
    }
    

    function order_dealer_map($id) {
        $data['id'] = $id;
        $data['order_details'] = $this->report->order_reports($id, $i = 1,$added_by);
        $data['pro_details'] = $this->report->order_reports($id, $i = 2,$added_by);
        $data['order_history'] = $this->report->order_reports($id, $i = 3,$added_by='dealer');
        $data['order_status'] = $this->report->order_status();
        $data['report'] = $this->report->dashborad_reports();
//        print_r()
//        exit;
        $data['content'] = 'report/orders_to_dealer_map';
        $this->load->view('index', $data);
    }
    
    function search_dealer()
    {
     $query = $_GET['q'];
        $result = $this->report->get_dealer($query);
        echo json_encode($result);    
    }
    
    function update_dealer_status()
    {
        $status = $this->input->post('status');
        $order_id = $this->input->post('order_id');
        $dealer_id = $this->input->post('dealer_id');
        
        $notify=$this->input->post('notify');
        $comments = $this->input->post('comments');        
        if ($notify[0] == 1){
            $get_dealer_email=$this->report->dealer_email($dealer_id);
            $user_details=$this->report->user_details($order_id);
            $email=$get_dealer_email[0]['email'];
            $data['firstname']=$user_details[0]['firstname'];
            $data['lastname']=$user_details[0]['lastname'];
            $data['invoice_no']=$user_details[0]['invoice_no'];
            $mandrill_api_key='1kRJ5flBFEw6jQEmJ1gazQ';
            $this->load->library('Mandrill');
            $mandrill_ready = NULL;
            $data['order_id']='1111';
            $data_page=$this->load->view('emailers/contact_email', $data, TRUE);
            try {
                $this->mandrill->init($mandrill_api_key);
                $mandrill_ready=TRUE;
            } catch (Mandrill_Exception $e) {
                $mandrill_ready = FALSE;
            }
            if ($mandrill_ready){
                $email=array(
                    'html'=>$data_page, //Consider using a view file
                    'text' => 'You have a order history Email',
                    'subject' => 'Email Via MNC order status',
                    'from_email' => 'webdeveloperdinesh@gmail.com',
                    'from_name' => 'support',
                    'to' => array(array('email'=>$email))
            );
            $result = $this->mandrill->messages_send($email);
            }
        }        
        $notify = $notify[0];
        $data['order_status'] = $this->report->order_status_update($status, $order_id, $notify, $comments,$dealer_id,$added_by='dealer');
        redirect(base_url().'order-to-dealer-map/'.$order_id);
    }
}
