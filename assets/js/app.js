
jQuery(document).ready(function($){
     if( $.fn.datepicker ){
       $( ".date-picker" ).datepicker();
   }
    if( $.fn.dataTableExt ){
        var oTable1 = $('#sample-table-2').dataTable( {
            "aoColumnDefs": [
            {
                'bSortable': false, 
                'aTargets': [ 0,5 ]
            }
            ]
        } );
    //        var oTable1 = $('#sample-table-2').dataTable( {
    //            "aoColumns": [
    //            {
    //                "bSortable": false
    //            },
    //            null, null,null, null, null,
    //            {
    //                "bSortable": false
    //            }
    //            ]
    //        } );
    }
    
				
				
    $('table th input:checkbox').on('click' , function(){
        var that = this;
        $(this).closest('table').find('tr > td:first-child input:checkbox')
        .each(function(){
            this.checked = that.checked;
            $(this).closest('tr').toggleClass('selected');
        });
						
    });
});