jQuery(document).ready(function(){
    
        
    //    $('#validation-form').validate({
    //        errorElement: 'div',
    //        errorClass: 'help-block',
    //        focusInvalid: false,
    //        rules: {
    //            email: {
    //                required: true,
    //                email:true
    //            },
    //            password: {
    //                required: true,
    //                minlength: 5
    //            },
    //            password2: {
    //                required: true,
    //                minlength: 5,
    //                equalTo: "#password"
    //            },
    //            name: {
    //                required: true
    //            },
    //            phone: {
    //                required: true,
    //                phone: 'required'
    //            },
    //            url: {
    //                required: true,
    //                url: true
    //            },
    //            comment: {
    //                required: true
    //            },
    //            state: {
    //                required: true
    //            },
    //            platform: {
    //                required: true
    //            },
    //            subscription: {
    //                required: true
    //            },
    //            gender: 'required',
    //            agree: 'required'
    //        },
    //			
    //        messages: {
    //            email: {
    //                required: "Please provide a valid email.",
    //                email: "Please provide a valid email."
    //            },
    //            password: {
    //                required: "Please specify a password.",
    //                minlength: "Please specify a secure password."
    //            },
    //            subscription: "Please choose at least one option",
    //            gender: "Please choose gender",
    //            agree: "Please accept our policy"
    //        },
    //			
    //        invalidHandler: function (event, validator) { //display error alert on form submit   
    //            $('.alert-danger', $('.login-form')).show();
    //        },
    //			
    //        highlight: function (e) {
    //            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
    //        },
    //			
    //        success: function (e) {
    //            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
    //            $(e).remove();
    //        },
    //			
    //        errorPlacement: function (error, element) {
    //            if(element.is(':checkbox') || element.is(':radio')) {
    //                var controls = element.closest('div[class*="col-"]');
    //                if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
    //                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
    //            }
    //            else if(element.is('.select2')) {
    //                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
    //            }
    //            else if(element.is('.chosen-select')) {
    //                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
    //            }
    //            else error.insertAfter(element.parent());
    //        },
    //			
    //        submitHandler: function (form) {
    //        },
    //        invalidHandler: function (form) {
    //        }
    //    });
    
    if($.fn.validate){
        $('#create-car-brand').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                brand_name: {
                    required: true
                },
                brand_desc: {
                    required: true
                },
                brand_logo: {
                    required: true,
                    accept: "image/*"
                }
            },
			
            messages: {
                brand_name: {
                    required: "Please provide brand name."
                },
                brand_desc: "Please provide brand description",
                brand_logo: "Provide Brand Logo"
            },
			
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },
			
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
			
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },
			
            errorPlacement: function (error, element) {
                if(element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },
			
            submitHandler: function (form) {
            },
            invalidHandler: function (form) {
            }
        });
    }
    
    $('#id-input-file-2').ace_file_input({
        no_file:'No File ...',
        btn_choose:'Choose',
        btn_change:'Change',
        droppable:false,
        onchange:null,
        thumbnail:false, //| true | large
        whitelist:'gif|png|jpg|jpeg'
//        blacklist:'exe|php'
        //onchange:''
        //
    });
    
    
    if($.fn.select2){
        $(".select2").css('width','200px').select2({
            allowClear:true
        })
        .on('change', function(){
            $(this).closest('form').validate().element($(this));
        }); 
    }
    
    $("#add-variant").click(function(){
        var input = '<p style="line-height:32px"><input type="text" name="car_variant" placeholder="Variant" class="col-xs-10 col-sm-5" /><span class="help-inline"><i class="icon-minus-sign remove-varaints"> Remove</i></span></p>'
        $(".additional-variants").append(input);
    });
    
    $('.additional-variants').on('click', '.remove-varaints', function(){
        $(this).closest('p').remove();
    });
    
    
    if($.fn.validate){
        $('#create-car').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                brand: {
                    required: true
                },
                car_model: {
                    required: true
                },
                car_variant: {
                    required: true
                },
                car_type: {
                    required: true
                },
                car_img: {
                    required: true
                },
                car_desc: {
                    required: true
                }
            },
			
            messages: {
                brand: {
                    required: "Select Brand."
                },
                car_model: "Specify Car Model",
                car_variant: "Specify Car Variant",
                car_type: "Select Car Type",
                car_desc: "Provide Car Description",
                car_img: "Provide Car Image"
            },
			
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },
			
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
			
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },
			
            errorPlacement: function (error, element) {
                if(element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },
			
            submitHandler: function (form) {
            },
            invalidHandler: function (form) {
            }
        });
    }
    
    /* 
    if($.fn.validate){
        $('#create-accessory').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                accessory_category: 'required',
                accessory_name: 'required',
                accessory_cost: {
                    required: true,
                    number: true
                },
                accessory_pic: {
                    required: true,
                    accept: "image/*"
                },
                accessory_desc: 'required',
                accessory_type: 'required'
            },
			
            messages: {
                accessory_category: {
                    required: "Select Category."
                },
                accessory_name: "Specify Accessory Name",
                accessory_cost: {
                    required: "Provide Accessory Cost",
                    number: "Provide Valid Accessory Cost"
                },
                accessory_pic: {
                    required: "Provide Accessory Image",
                    number: "Provide Valid Accessory Image"
                },
                accessory_desc: "Provide Accessory Description",
                accessory_type: "Provide Accessory Field"
            },
			
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },
			
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
			
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },
			
            errorPlacement: function (error, element) {
                if(element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },
			
            submitHandler: function (form) {
            },
            invalidHandler: function (form) {
            }
        });
    }
    */
    
    if($.fn.validate){
        $('#create-car-color').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                color_name: 'required',
                color_img: {
                    required: true,
                    accept: "image/*"
                }
            },
			
            messages: {
                color_name: "Provide Color Name",
                color_img: {
                    required: "Provide Color Image",
                    number: "Provide Valid Color Image"
                }
            },
			
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },
			
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
			
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },
			
            errorPlacement: function (error, element) {
                if(element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },
			
            submitHandler: function (form) {
            },
            invalidHandler: function (form) {
            }
        });
    }
    
    if($.fn.validate){
        $('#configure-car').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                brand: 'required',
                car_name: 'required',
                car_variant: 'required',
                car_wheels: 'required',
                car_colors: 'required',
                car_seats: 'required',
                car_dashboards: 'required',
                ex_showroom_cost: {
                    required: true,
                    number: true
                },
                mnc_cost: {
                    required: true,
                    number: true
                }
            },
			
            messages: {
                brand: "Select Car Brand",
                car_name: "Select Car",
                car_variant: "Select Variant",
                car_wheels: "Provide Car Wheels",
                car_colors: "Provide Car Colors",
                car_seats: "Provide Car Seats",
                car_dashboards: "Provide Car Dashboards",
                ex_showroom_cost: {
                    required: "Provide Car Ex-showroom Price",
                    number: "Provide Valid Car Ex-showroom Price"
                },
                mnc_cost: {
                    required: "Provide Car MNC Price",
                    number: "Provide Valid Car MNC Price"
                }
            },
			
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },
			
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },
			
            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },
			
            errorPlacement: function (error, element) {
                if(element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },
			
            submitHandler: function (form) {
            },
            invalidHandler: function (form) {
            }
        });
    }
    
    
    $(".chosen-select").chosen();
    
});