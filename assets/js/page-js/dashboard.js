
jQuery(document).ready(function($){
    //jQuery(function($) {
    $('.easy-pie-chart.percentage').each(function(){
        var $box = $(this).closest('.infobox');
        var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
        var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
        var size = parseInt($(this).data('size')) || 50;
        $(this).easyPieChart({
            barColor: barColor,
            trackColor: trackColor,
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: parseInt(size/10),
            animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
            size: size
        });
    })
			
    $('.sparkline').each(function(){
        var $box = $(this).closest('.infobox');
        var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
        $(this).sparkline('html', {
            tagValuesAttribute:'data-values', 
            type: 'bar', 
            barColor: barColor , 
            chartRangeMin:$(this).data('min') || 0
        } );
    });
			
			
			
			
    var placeholder = $('#piechart-placeholder').css({
        'width':'90%' , 
        'min-height':'150px'
    });
    var data = [
    {
        label: "social networks",  
        data: 38.7, 
        color: "#68BC31"
    },

    {
        label: "search engines",  
        data: 24.5, 
        color: "#2091CF"
    },

    {
        label: "ad campaigns",  
        data: 8.2, 
        color: "#AF4E96"
    },

    {
        label: "direct traffic",  
        data: 18.6, 
        color: "#DA5430"
    },

    {
        label: "other",  
        data: 10, 
        color: "#FEE074"
    }
    ]
    function drawPieChart(placeholder, data, position) {
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    tilt:0.8,
                    highlight: {
                        opacity: 0.25
                    },
                    stroke: {
                        color: '#fff',
                        width: 2
                    },
                    startAngle: 2
                }
            },
            legend: {
                show: true,
                position: position || "ne", 
                labelBoxBorderColor: null,
                margin:[-30,15]
            }
            ,
            grid: {
                hoverable: true,
                clickable: true
            }
        })
    }
    drawPieChart(placeholder, data);
			
    /**
                 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
                 so that's not needed actually.
                 */
    placeholder.data('chart', data);
    placeholder.data('draw', drawPieChart);
			
			
			
    var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
    var previousPoint = null;
			
    placeholder.on('plothover', function (event, pos, item) {
        if(item) {
            if (previousPoint != item.seriesIndex) {
                previousPoint = item.seriesIndex;
                var tip = item.series['label'] + " : " + item.series['percent']+'%';
                $tooltip.show().children(0).text(tip);
            }
            $tooltip.css({
                top:pos.pageY + 10, 
                left:pos.pageX + 10
            });
        } else {
            $tooltip.hide();
            previousPoint = null;
        }			
    });	
    
    //Group Statistics
    
    var datasets = {
        "audi": {
            label: "Audi",
            data: [[1988, 483994], [1989, 479060], [1990, 457648], [1991, 401949], [1992, 424705], [1993, 402375], [1994, 377867], [1995, 357382], [1996, 337946], [1997, 336185], [1998, 328611], [1999, 329421], [2000, 342172], [2001, 344932], [2002, 387303], [2003, 440813], [2004, 480451], [2005, 504638], [2006, 528692]]
        },        
        "chevrolet": {
            label: "Chevrolet",
            data: [[1988, 478000], [1989, 453000], [1990, 431000], [1992, 420500], [1993, 420600], [1994, 400600], [1995, 365700], [1996, 346200], [1997, 327300], [1998, 336600], [1999, 318000], [2000, 319100], [2001, 321300], [2002, 323600], [2003, 325100], [2004, 426100], [2005, 431100], [2006, 534700]]
        },
        "fiat": {
            label: "Fiat",
            data: [[1988, 462982], [1989, 462027], [1990, 420696], [1991, 412348], [1992, 418560], [1993, 456393], [1994, 354579], [1995, 350818], [1996, 350554], [1997, 348276], [1998, 347691], [1999, 347529], [2000, 347778], [2001, 348760], [2002, 350949], [2003, 457452], [2004, 460234], [2005, 560076], [2006, 459213]]
        },
        "ford": {
            label: "Ford",
            data: [[1988, 460627], [1989, 455475], [1990, 458464], [1991, 455134], [1992, 452436], [1993, 447139], [1994, 443962], [1995, 343238], [1996, 342395], [1997, 340854], [1998, 340993], [1999, 341822], [2000, 341147], [2001, 340474], [2002, 340604], [2003, 340044], [2004, 438816], [2005, 538060], [2006, 536984]]
        },
        "mercedes": {
            label: "Mercedes",
            data: [[1988, 453813], [1989, 453719], [1990, 443722], [1991, 443789], [1992, 443720], [1993, 443730], [1994, 443636], [1995, 333598], [1996, 333610], [1997, 333655], [1998, 333695], [1999, 333673], [2000, 333553], [2001, 333774], [2002, 333728], [2003, 333618], [2004, 333638], [2005, 333467], [2006, 553770]]
        },
        "renault": {
            label: "Renault",
            data: [[1988, 456402], [1989, 456474], [1990, 446605], [1991, 446209], [1992, 446035], [1993, 446020], [1994, 446000], [1995, 326018], [1996, 333958], [1997, 325780], [1998, 325954], [1999, 346178], [2000, 346411], [2001, 345993], [2002, 345833], [2003, 445791], [2004, 445450], [2005, 535521], [2006, 555271]]
        },
        "toyota": {
            label: "Toyota",
            data: [[1988, 444382], [1989, 444498], [1990, 444535], [1991, 444398], [1992, 444766], [1993, 444441], [1994, 454670], [1995, 234217], [1996, 334275], [1997, 234203], [1998, 324482], [1999, 344506], [2000, 344358], [2001, 344385], [2002, 345269], [2003, 445066], [2004, 445194], [2005, 354887], [2006, 554891]]
        },
        "volkswagen": {
            label: "Volkswagen",
            data: [[1988, 454382], [1989, 434498], [1990, 444535], [1991, 444398], [1992, 444766], [1993, 444441], [1994, 445670], [1995, 324217], [1996, 334275], [1997, 234203], [1998, 324482], [1999, 344506], [2000, 434358], [2001, 344385], [2002, 345269], [2003, 445066], [2004, 445194], [2005, 354887], [2006, 554891]]
        }
    };

    // hard-code color indices to prevent them from shifting as
    // countries are turned on/off

    var i = 0;
    $.each(datasets, function(key, val) {
        val.color = i;
        ++i;
    });

    // insert checkboxes 
    var choiceContainer = $("#choices");
    $.each(datasets, function(key, val) {
        choiceContainer.append("<br/><input type='checkbox' name='" + key +
            "' checked='checked' id='id" + key + "'></input>" +
            "<label for='id" + key + "'>"
            + val.label + "</label>");
    });

    choiceContainer.find("input").click(plotAccordingToChoices);

    function plotAccordingToChoices() {

        var data = [];

        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && datasets[key]) {
                data.push(datasets[key]);
            }
        });

        if (data.length > 0) {
            $.plot("#grp-statics", data, {
                yaxis: {
                    min: 0
                },
                xaxis: {
                    tickDecimals: 0
                }
            });
        }
    }

    plotAccordingToChoices();
    
    //Bar Chart
    
    var data1 = [ ["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9] ];

    $.plot("#bar-chart", [ data1 ], {
        series: {
            bars: {
                show: true,
                barWidth: 0.6,
                align: "center"
            }
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        }
    });
    
    //
    $('.dialogs,.comments').slimScroll({
					height: '300px'
			    });
});