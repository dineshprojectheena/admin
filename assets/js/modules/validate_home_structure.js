$(document).ready(function() {
    $('#validation-form').validate({
        errorElement: 'span',
        errorClass: 'help-inline',
        focusInvalid: false,
        rules: {
//            email: {
//                required: true,
//                email: true
//            },
            status: {
                required: true
            },
            searchid: {
                required: true
            },
            searchid2: {
                required: true
            }
//            details: {
//                required: true
//            }
//            ,
//            password2: {
//                required: true,
//                minlength: 5,
//                equalTo: "#password"
//            },
//            name: {
//                required: true
//            },
//            phone: {
//                required: true,
//                phone: 'required'
//            },
//            url: {
//                required: true,
//                url: true
//            },
//            comment: {
//                required: true
//            },
//            state: {
//                required: true
//            },
//            platform: {
//                required: true
//            },
//            subscription: {
//                required: true
//            },
//            gender: 'required',
//            agree: 'required'
        },
        messages: {
//            email: {
//                required: "Please provide a valid email.",
//                email: "Please provide a valid email."
//            },
            brand_name: {
                required: "Please specify a brand name."
            },
            brand_images: {
                required: "Please select a image."
            },
            status: {
                required: "Please select a status."
            },
            details: {
                required: "Please specify a brand details."
            }
//            subscription: "Please choose at least one option",
//            gender: "Please choose gender",
//            agree: "Please accept our policy"
     },
        highlight: function(e) {
            $(e).closest('.control-group').removeClass('info').addClass('error');
        },
        success: function(e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('.controls');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chzn-select')) {
                error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
            }
            else
                error.insertAfter(element);
        },
//        submitHandler: function(form) {
//        },
        invalidHandler: function(form) {
        }
    });
});