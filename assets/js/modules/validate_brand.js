$(document).ready(function() {
    if($('#brand_image2').val()=='')
    {
    $('#validation-form').validate({
        errorElement: 'span',
        errorClass: 'help-inline',
        focusInvalid: false,
        rules: {
            brand_name: {
                required: true
            },
            status: {
                required: true
            },
            brand_images: {
                required: true
            },
            details: {
                required: true
            }
        },
        messages: {
            brand_name: {
                required: "Please specify a brand name."
            },
            brand_images: {
                required: "Please select a image."
            },
            status: {
                required: "Please select a status."
            },
            details: {
                required: "Please specify a brand details."
            }
     },
        highlight: function(e) {
            $(e).closest('.control-group').removeClass('info').addClass('error');
        },
        success: function(e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('.controls');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chzn-select')) {
                error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
            }
            else
                error.insertAfter(element);
        },
//        submitHandler: function(form) {
//        },
        invalidHandler: function(form) {
        }
    });
    }
    else
    {
    $('#validation-form').validate({
        errorElement: 'span',
        errorClass: 'help-inline',
        focusInvalid: false,
        rules: {
            brand_name: {
                required: true
            },
            status: {
                required: true
            },
            details: {
                required: true
            }
        },
        messages: {
            brand_name: {
                required: "Please specify a brand name."
            },
            status: {
                required: "Please select a status."
            },
            details: {
                required: "Please specify a brand details."
            }
     },
        highlight: function(e) {
            $(e).closest('.control-group').removeClass('info').addClass('error');
        },
        success: function(e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('.controls');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chzn-select')) {
                error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
            }
            else
                error.insertAfter(element);
        },
//        submitHandler: function(form) {
//        },
        invalidHandler: function(form) {
        }
    });
    }
    
    
});