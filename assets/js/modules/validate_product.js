$(document).ready(function() {
    if($('#profile_pic').val()=='')
    {
    $('#validation-form').validate({
        errorElement: 'span',
        errorClass: 'help-inline',
        focusInvalid: false,
        rules: {
            brand_id: {
                required: true
            },            
            model_id: {
                required: true
            },            
            pro_name: {
                required: true
            },            
            pro_type: {
                required: true
            },            
            engine_name: {
                required: true
            },            
            status: {
                required: true
            },            
            product_images: {
                required: true
            },
            
        },
        messages:{
         brand: {
                required: "Please specify a brand name."
            },
            model_name: {
                required: "Please select a image."
            },
            status: {
                required: "Please select a status."
            }
        },
        highlight: function(e) {
            $(e).closest('.control-group').removeClass('info').addClass('error');
        },
        success: function(e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('.controls');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chzn-select')) {
                error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
            }
            else
                error.insertAfter(element);
        },
//        submitHandler: function(form) {
//        },
        invalidHandler: function(form) {
        }
    });
    }
    else
    {
    $('#validation-form').validate({
        errorElement: 'span',
        errorClass: 'help-inline',
        focusInvalid: false,
        rules: {
            brand_id: {
                required: true
            },            
            model_id: {
                required: true
            },            
            pro_name: {
                required: true
            },            
            pro_type: {
                required: true
            },            
            engine_name: {
                required: true
            },            
            status: {
                required: true
            },            
            
        },
        messages:{
         brand: {
                required: "Please specify a brand name."
            },
            model_name: {
                required: "Please select a image."
            },
            status: {
                required: "Please select a status."
            }
        },
        highlight: function(e) {
            $(e).closest('.control-group').removeClass('info').addClass('error');
        },
        success: function(e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('.controls');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chzn-select')) {
                error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
            }
            else
                error.insertAfter(element);
        },
//        submitHandler: function(form) {
//        },
        invalidHandler: function(form) {
        }
    });
    }
});