$(document).ready(function() {
    if($('#profile_pic').val()=='')
    {
    $('#validation-form').validate({
        errorElement: 'span',
        errorClass: 'help-inline',
        focusInvalid: false,
        rules: {
            dealer_name: {
                required: true
            },
//            profiles:{
//                required:true
//            },
            status: {
                required: true
            },
//            contact_no: {
//                required: true
//            },
//            email:{
//                required: true,
//                email: true
//            },
//             no_brands: {
//                required: true
//            },
//            brand_data:{
//                required: true
//            },
//            others_brand:{
//                required: true
//            },
//            cont_person:{
//                required: true
//            },
//            spoc_number:{
//                required: true
//            },
//            fax:{
//                required: true
//            },
//            address:{
//                required: true
//            },
//            state:{
//                required: true
//            },
//            city:{
//                required: true
//            },
//            post_code:{
//                required: true
//            },
//            country:{
//                required: true
//            },
//            spoc_email:{
//                required: true
//            },

        },
        messages: {
            brand: {
                required: "Please specify a brand name."
            },
            model_name: {
                required: "Please select a image."
            },
            status: {
                required: "Please select a status."
            }
     },
        highlight: function(e) {
            $(e).closest('.control-group').removeClass('info').addClass('error');
        },
        success: function(e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('.controls');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chzn-select')) {
                error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
            }
            else
                error.insertAfter(element);
        },
        invalidHandler: function(form) {
        }
    });
    }
    else
    {
    $('#validation-form').validate({
        errorElement: 'span',
        errorClass: 'help-inline',
        focusInvalid: false,
        rules: {
            dealer_name: {
                required: true
            },
            status: {
                required: true
            },
//            contact_no: {
//                required: true
//            },
//            email:{
//                required: true,
//                email: true
//            },
//             no_brands: {
//                required: true
//            },
//            brand_data:{
//                required: true
//            },
//            others_brand:{
//                required: true
//            },
//            cont_person:{
//                required: true
//            },
//            spoc_number:{
//                required: true
//            },
//            fax:{
//                required: true
//            },
//            address:{
//                required: true
//            },
//            state:{
//                required: true
//            },
//            city:{
//                required: true
//            },
//            post_code:{
//                required: true
//            },
//            country:{
//                required: true
//            },
//            spoc_email:{
//                required: true
//            },

        },
        messages: {
            brand: {
                required: "Please specify a brand name."
            },
            model_name: {
                required: "Please select a image."
            },
            status: {
                required: "Please select a status."
            }
     },
        highlight: function(e) {
            $(e).closest('.control-group').removeClass('info').addClass('error');
        },
        success: function(e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
        errorPlacement: function(error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('.controls');
                if (controls.find(':checkbox,:radio').length > 1)
                    controls.append(error);
                else
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chzn-select')) {
                error.insertAfter(element.siblings('[class*="chzn-container"]:eq(0)'));
            }
            else
                error.insertAfter(element);
        },
        invalidHandler: function(form) {
        }
    });
    }
});